﻿using BLL;
using Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RestSharp;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace ScheduleAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private NotificationServices _notificationServices;
        private readonly IConfiguration _config;

        public NotificationController(NotificationServices notificationServices , IConfiguration config)
        {
            _notificationServices = notificationServices;
            _config = config;
        }

        /// <summary>
        /// GetPostFTPXMLFileJob
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> SendJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData resData = new ResData();
            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            await _notificationServices.Send();
            return Ok(resData);
        }

        /// <summary>
        /// GetPostFTPXMLFileJob
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> SMSSendJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData resData = new ResData();

            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }

            await _notificationServices.SMSSend();


            return Ok(resData);
        }

 

    }
}
