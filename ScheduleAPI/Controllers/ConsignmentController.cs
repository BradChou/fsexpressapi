﻿using BLL;
using BLL.Model.ScheduleAPI.Res.Consignment;
using Common;
using Common.Attribute;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ConsignmentController : ControllerBase
    {
        private ConsignmentServices _consignmentServices;


        public ConsignmentController(ConsignmentServices consignmentServices)
        {
            _consignmentServices = consignmentServices;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]

        public async Task<ActionResult<ResData>> FilterDeliveryStatusPublicJob([FromHeader(Name = "apikey")] string apikey)
        {

            ResData<FilterDeliveryStatusPublicJob_Res> resData = new ResData<FilterDeliveryStatusPublicJob_Res>();

            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }

            resData.Data = await _consignmentServices.FilterDeliveryStatusPublic();


            return Ok(resData);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]

        public async Task<ActionResult<ResData>> UpdateDeliveryStatusPublicToDeliveryScanLogJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<UpdateDeliveryStatusPublicToDeliveryScanLogJob_Res> resData = new ResData<UpdateDeliveryStatusPublicToDeliveryScanLogJob_Res>();

            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }

            resData.Data = await _consignmentServices.UpdateDeliveryStatusPublicToDeliveryScanLog();


            return Ok(resData);
        }
    }
}
