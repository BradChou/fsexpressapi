﻿using BLL;
using BLL.Model.ScheduleAPI.Res.Contract;
using Common;
using Common.Attribute;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace ScheduleAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContractController : ControllerBase
    {
        private ContractServices _contractServices;
        public ContractController(ContractServices contractServices)
        {
            _contractServices = contractServices;
        }
        /// <summary>
        /// SettleCaculate結算計算
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        ///[Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> SettleCaculateJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<List<SettleCaculate_Res>> resData = new ResData<List<SettleCaculate_Res>>();

            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            //using (TransactionScope ntx = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            //{
            //    resData.Data = await _measureServices.MesureScanLogFilter();
            //    ntx.Complete();
            //}
            await _contractServices.SettleCaculate();
            return Ok(resData);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<ResData>> SettleCaculateJob_new()
        {
            ResData<List<SettleCaculate_Res>> resData = new ResData<List<SettleCaculate_Res>>();

            //var header = HttpContext.Request.Headers["apikey"].ToString();

            //if (!header.Equals("fsejob"))
            //{
            //    throw new Exception();
            //}
            ////using (TransactionScope ntx = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            ////{
            ////    resData.Data = await _measureServices.MesureScanLogFilter();
            ////    ntx.Complete();
            ////}
            await _contractServices.SettleCaculate_new();
            return Ok(resData);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<ResData>> SettleCaculateJob_daily()
        {
            ResData<List<SettleCaculate_Res>> resData = new ResData<List<SettleCaculate_Res>>();

            //var header = HttpContext.Request.Headers["apikey"].ToString();

            //if (!header.Equals("fsejob"))
            //{
            //    throw new Exception();
            //}
            ////using (TransactionScope ntx = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            ////{
            ////    resData.Data = await _measureServices.MesureScanLogFilter();
            ////    ntx.Complete();
            ////}
            await _contractServices.SettleCaculate_daily();
            return Ok(resData);
        }


    }
}
