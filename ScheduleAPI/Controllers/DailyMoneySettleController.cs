﻿using BLL;
using BLL.Model.ScheduleAPI.Req.DailySettlement;
using BLL.Model.ScheduleAPI.Res.Consignment;
using Common;
using Common.Attribute;
using DAL.Model.Condition;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DailyMoneySettleController : ControllerBase
    {
        private DailyMoneySettleServices _DailyMoneySettleServices;


        public DailyMoneySettleController(DailyMoneySettleServices DailyMoneySettleServices)
        {
            _DailyMoneySettleServices = DailyMoneySettleServices;
        }

        

        [HttpGet("[action]")]
        public async Task<ActionResult<ResData>> InsertDailyMoneySettleDetail_new(DailySettleToDoListModel DailySettleToDoList)
        {
            ResData resData = new ResData();

            await _DailyMoneySettleServices.InsertMoneySettleDetail_new(DailySettleToDoList);

            return Ok(resData);
        }

    }
}
