﻿using BLL;
using BLL.Model.ScheduleAPI.Res.DeliveryRequests;
using Common;
using Common.Attribute;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DeliveryRequestsController : ControllerBase
    {

        private ConsignmentServices _consignmentServices;
        private DeliveryRequestsServices _DeliveryRequestsServices;

        public DeliveryRequestsController(DeliveryRequestsServices DeliveryRequestsServices)
        {
            _DeliveryRequestsServices = DeliveryRequestsServices;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>

        [HttpPost("[action]")]

        public async Task<ActionResult<ResData>> DeliveryRequestsUpdateSDMDJob([FromHeader(Name = "apikey")] string apikey)
        {

            ResData<DeliveryRequestsUpdateSDMDJob_Res> resData = new ResData<DeliveryRequestsUpdateSDMDJob_Res>();

            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }

            resData.Data = await _DeliveryRequestsServices.DeliveryRequestsUpdateSDMD();


            return Ok(resData);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>

        [HttpGet("[action]")]

        public async Task<ActionResult<ResData>> DeliveryRequestsUpdateSDMD(string check_number)
        {

            ResData<DeliveryRequestsUpdateSDMDJob_Res> resData = new ResData<DeliveryRequestsUpdateSDMDJob_Res>();


            await _DeliveryRequestsServices.DeliveryRequestsUpdateSDMDBYcheck_number(check_number);


            return Ok(resData);

        }
    }
}
