﻿using BLL;
using BLL.Model.ScheduleAPI.Res.Consignment;
using Common;
using Common.Attribute;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MoneySettleController : ControllerBase
    {
        private MoneySettleServices _MoneySettleServices;


        public MoneySettleController(MoneySettleServices MoneySettleServices)
        {
            _MoneySettleServices = MoneySettleServices;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        //[Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> InsertMoneySettleDetail([FromHeader(Name = "apikey")] string apikey)
        {
            ResData resData = new ResData();

            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }

            await _MoneySettleServices.InsertMoneySettleDetail();

            return Ok(resData);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<ResData>> InsertMoneySettleDetail_new()
        {
            ResData resData = new ResData();

            //var header = HttpContext.Request.Headers["apikey"].ToString();

            //if (!header.Equals("fsejob"))
            //{
            //    throw new Exception();
            //}

            await _MoneySettleServices.InsertMoneySettleDetail_new();

            return Ok(resData);
        }

    }
}
