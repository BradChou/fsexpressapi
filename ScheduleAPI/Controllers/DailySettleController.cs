﻿using BLL;
using BLL.Model.ScheduleAPI.Res.DailySettle;
using Common;
using Common.Attribute;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleAPI.Controllers
{
    public class DailySettleController : Controller
    {

        private DailySettleTodoListServices _DailySettleTodoListServices;


        public DailySettleController(DailySettleTodoListServices DailySettleTodoListServices)
        {
            _DailySettleTodoListServices = DailySettleTodoListServices;
        }

        /// <summary>
        /// TODOLIST
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> TodoListUpdateMainTempJob()
        {
            ResData<TodoListUpdateMainTempJob_Res> resData = new ResData<TodoListUpdateMainTempJob_Res>();
         
            //resData.Data = await _DailySettleTodoListServices.TodoListUpdateMainTempJob(null,DateTime.Now);
            return Ok(resData);
        }
    }
}
