﻿using BLL;
using BLL.Model.ScheduleAPI.Req.Measure;
using BLL.Model.ScheduleAPI.Res.Measure;
using Common;
using Common.Attribute;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace ScheduleAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MeasureController : ControllerBase
    {
        private MeasureServices _measureServices;

        public MeasureController(MeasureServices measureServices)
        {
            _measureServices = measureServices;
        }

        //[HttpPost("[action]")]
        //public async Task<ActionResult<ResData>> GetScheduleTaskLog(GetScheduleTaskLog_Req req)
        //{
        //    ResData<GetScheduleTaskLog_Res> resData = new ResData<GetScheduleTaskLog_Res>();
        //    resData.Data = await _measureServices.GetScheduleTaskLogByTaskAndApi(req.TaskName, req.ApiName);
        //    LastDate date = JsonConvert.DeserializeObject<LastDate>(resData.Data.Parameter);
        //    return Ok(date.cdate);
        //}

        /// <summary>
        /// MeasureScanLog分類
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> MesureScanLogFilterJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<MesureScanLogFilterJob_Res> resData = new ResData<MesureScanLogFilterJob_Res>();

            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            //using (TransactionScope ntx = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            //{
            //    resData.Data = await _measureServices.MesureScanLogFilter();
            //    ntx.Complete();
            //}
            resData.Data = await _measureServices.MesureScanLogFilter();
            return Ok(resData);
        }

        /// <summary>
        /// 將Filter後資料Insert進貨態表&Update主表
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> UpdateMesureScanLogToMainTable([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<UpdateMesureScanLogToMainTable_Res> resData = new ResData<UpdateMesureScanLogToMainTable_Res>();

            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }

            resData.Data = await _measureServices.UpdateMesureScanLogToMainTable();
            return Ok(resData);
        }

        //public async Task<ActionResult<ResData>> UploadMeasureImage(UploadMeasureImage_Req req)
        /// <summary>
        /// 上傳丈量機圖檔取時間
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> UploadMeasureImageGetStartTIme([FromHeader(Name = "apikey")] string apikey, UploadMeasureImageStartTIme_Req req)
        {
            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            ResData<UploadMeasureImageStartTime_Res> resData = new ResData<UploadMeasureImageStartTime_Res>();
            resData.Data = await _measureServices.UploadMeasureImageStartTIme(req);

            return Ok(resData);
        }

        /// <summary>
        /// 上傳丈量機圖檔回傳log
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> UploadMeasureImageUpdateLog([FromHeader(Name = "apikey")] string apikey, UploadMeasureImageUpdateLog_Req req)
        {
            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            ResData<UploadMeasureImageUpdateLog_Res> resData = new ResData<UploadMeasureImageUpdateLog_Res>();
            resData.Data = await _measureServices.UploadMeasureImageUpdateLog(req);

            return Ok(resData);
        }

        /// <summary>
        /// MeasureScanLog INSERT 補救表 [cbm_data_log]
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> MesureScanLogAbnormalJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<MesureScanLogAbnormalJob_Res> resData = new ResData<MesureScanLogAbnormalJob_Res>();

            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            //using (TransactionScope ntx = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            //{
            //    resData.Data = await _measureServices.MesureScanLogFilter();
            //    ntx.Complete();
            //}
            resData.Data = await _measureServices.MesureScanLogAbnormal();
            return Ok(resData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> CBMDetailFilterJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<CBMDetailFilterJob_Res> resData = new ResData<CBMDetailFilterJob_Res>();

            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }

            resData.Data= await _measureServices.CBMDetailFilter();
            return Ok(resData);
        }

       


    }

    internal class LastDate
    {
        public DateTime cdate { get; set; }
    }
}
