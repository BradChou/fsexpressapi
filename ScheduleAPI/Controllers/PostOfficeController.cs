﻿using BLL;
using BLL.Model.ScheduleAPI.Res.PostOffice;
using Common;
using Common.Attribute;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PostOfficeController : ControllerBase
    {

        private PostOfficeServices _postOfficeServices;


        public PostOfficeController(PostOfficeServices postOfficeServices)
        {
            _postOfficeServices = postOfficeServices;
        }

        /// <summary>
        /// 郵局上傳下載FTP
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> TransportPostFTPJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<TransportPostFTPJob_Res> resData = new ResData<TransportPostFTPJob_Res>();
            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.TransportPostFTP();
            return Ok(resData);
        }


        /// <summary>
        /// 上傳FTP 給郵局的出貨檔JOB
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> UploadPostOrderJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<UploadPostOrderJob_Res> resData = new ResData<UploadPostOrderJob_Res>();
            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.UploadPostOrderJob();
            return Ok(resData);
        }


        /// <summary>
        /// 抓郵局的FTP到NAS
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> DownloadPostStatusFTPToNASJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<DownloadPostStatusFTPToNASJob_Res> resData = new ResData<DownloadPostStatusFTPToNASJob_Res>();
            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.DownloadPostStatusFTPToNAS();
            return Ok(resData);
        }


        /// <summary>
        /// 抓郵局結果檔
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetPostResultNASToDBJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<GetPostResultNASToDBJob_Res> resData = new ResData<GetPostResultNASToDBJob_Res>();
            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.GetPostResultNASToDB();
            return Ok(resData);
        }

        /// <summary>
        /// 抓郵局異常檔
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetPostIrregularNASToDBJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<GetPostIrregularNASToDBJob_Res> resData = new ResData<GetPostIrregularNASToDBJob_Res>();
            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.GetPostIrregularNASToDB();
            return Ok(resData);
        }

        /// <summary>
        /// 抓郵局貨態 NAS 到 DB
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetPostStatusNASToDBJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<GetPostStatusNASToDBJob_Res> resData = new ResData<GetPostStatusNASToDBJob_Res>();
            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.GetPostStatusNASToDB();
            return Ok(resData);
        }

        /// <summary>
        /// 解析郵局狀態JOB
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> ParsingPostStatusJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<ParsingPostStatusJob_Res> resData = new ResData<ParsingPostStatusJob_Res>();
            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.ParsingPostStatus();
            return Ok(resData);
        }

        /// <summary>
        /// 分類郵局狀態JOB
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> FilterPostStatusJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<FilterPostStatusJob_Res> resData = new ResData<FilterPostStatusJob_Res>();
            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.FilterPostStatus();
            return Ok(resData);
        }

        /// <summary>
        /// 寫入郵局狀態JOB
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> InsertPostStatusJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<InsertPostStatusJob_Res> resData = new ResData<InsertPostStatusJob_Res>();
            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.InsertPostStatus();
            return Ok(resData);
        }





        /// <summary>
        /// GetErroePostFTPXMLFileJob
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetErrorPostFTPXMLFileJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<GetErrorPostFTPXMLFileJob_Res> resData = new ResData<GetErrorPostFTPXMLFileJob_Res>();
            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.GetErrorPostFTPXMLFile();
            return Ok(resData);
        }

        /// <summary>
        /// GetPostFTPXMLFileJob
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetPostFTPXMLFileJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<GetPostFTPXMLFileJob_Res> resData = new ResData<GetPostFTPXMLFileJob_Res>();
            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.GetPostFTPXMLFile();
            return Ok(resData);
        }
        /// <summary>
        /// ParsingPostXmlJob
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> ParsingPostXmlJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<ParsingPostXmlJob_Res> resData = new ResData<ParsingPostXmlJob_Res>();

            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.ParsingPostXml();
            return Ok(resData);
        }
        /// <summary>
        /// ParsingPostXmlJob
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> FilterPostStateJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<FilterPostStateJob_Res> resData = new ResData<FilterPostStateJob_Res>();

            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.FilterPostState();
            return Ok(resData);
        }

        /// <summary>
        /// ParsingPostXmlJob
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> UpdatePostStateToDeliveryScanLogJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<UpdatePostStateToDeliveryScanLogJob_Res> resData = new ResData<UpdatePostStateToDeliveryScanLogJob_Res>();

            var header = HttpContext.Request.Headers["apikey"].ToString();

            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.UpdatePostStateToDeliveryScanLog();
            return Ok(resData);
        }

        /// <summary>
        /// 郵局Q100報表
        /// </summary>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Transactional]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> CreateXlsxPostQ100ReportJob([FromHeader(Name = "apikey")] string apikey)
        {
            ResData<CreateXlsxPostQ100ReportJob_Res> resData = new ResData<CreateXlsxPostQ100ReportJob_Res>();
            var header = HttpContext.Request.Headers["apikey"].ToString();
            if (!header.Equals("fsejob"))
            {
                throw new Exception();
            }
            resData.Data = await _postOfficeServices.CreateXlsxPostQ100Report();
            return Ok(resData);
        }

    }
}
