﻿using Common;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;

namespace ScheduleAPI.Handler
{
    public class CashExceptionHandler
    {
        private readonly RequestDelegate _next;
        public CashExceptionHandler(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                try
                {
                    await _next(context);
                }
                catch (MyException ex)
                {
                    ResData resData = new ResData();
                    resData.Status = ex.ResponseCode;
                    resData.Message = ex.Message;
                    context.Response.StatusCode = 200;
                    var result = JsonConvert.SerializeObject(resData);
                    await context.Response.WriteAsync(result);
                }
            }
            catch (Exception e)//不知道為什麼沒吃到多補一層
            {

                ResData resData = new ResData();
                resData.Status = ResponseCodeEnum.Error;
                //resData.Message = e.Message;
                resData.Message = e.Message;
                context.Response.StatusCode = 200;
                var result = JsonConvert.SerializeObject(resData);
                await context.Response.WriteAsync(result);
            }

        }
    }
}
