﻿
using BLL.Model.AddressParsing.Res.Address;
using BLL.Model.FSExpressAPI.Model;
using BLL.Model.FSExpressAPI.Req.Consignment;
using BLL.Model.FSExpressAPI.Res.Consignment;
using BLL.Model.ScheduleAPI.Res.Consignment;
using Common;
using Common.Model;
using Common.SSRS;
using DAL;
using DAL.Contract_DA;
using DAL.DA;
using DAL.JunFuTrans_DA;
using DAL.Model.Condition;
using DAL.Model.Contract_Condition;
using DAL.Model.JunFuAddress_Condition;
using DAL.Model.JunFuTrans_Condition;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.International.Converters.TraditionalChineseToSimplifiedConverter;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Transactions;
using static Common.Setting.EnumSetting;
using static Common.Setting.JobSetting;

namespace BLL
{
    public class ConsignmentServices
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ItcDeliveryRequests_DA _tcDeliveryRequests;
        private IDeliveryStatusPublic_DA _DeliveryStatusPublic;
        private IttDeliveryScanLog_DA _ttDeliveryScanLog_DA;
        private IScheduleTask_DA _ScheduleTask_DA;
        private IDeliveryStatusPublicFilter_DA _DeliveryStatusPublicFilter_DA;
        private IDeliveryStatusPublicError_DA _DeliveryStatusPublicError_DA;
        private ItbItemCodes_DA _tbItemCodes_DA;
        private IStoredProcedure_DA _storedProcedure_DA;
        private ItbCustomers_DA _tbCustomers_DA;
        private Icheck_number_prehead_DA _check_number_prehead_DA;
        private IorgArea_DA _orgArea_DA;
        private Icheck_number_sd_mapping_DA _check_number_sd_mapping_DA;
        private IttDeliveryRequestsRecord_DA _ttDeliveryRequestsRecord_DA;
        private IReportPrintService _reportPrintService;
        private Ipickup_request_for_apiuser_DA _pickup_request_for_apiuser_DA;
        private ICBMDetailLog_DA _CBMDetailLog_DA;
        private IProductValuationManage_DA _ProductValuationManage_DA;
        private IProductManage_DA _ProductManage_DA;
        private IItemCodes_DA _ItemCodes_DA;


        private string CustomerCode;

        private InsertDelivery_Req _insertDelivery_Req;
        //private LyAddrFormatReturnModel _SendLyAddrFormatReturnModel;
        //private LyAddrFormatReturnModel _ReceiveLyAddrFormatReturnModel;
        //private LyGetDistributorReturnModel _SendLyGetDistributorReturnModel;
        //private LyGetDistributorReturnModel _ReceiveLyGetDistributorReturnModel;
        private tbCustomers_Condition _CustomerInfo;

        private AddressParsingInfo _SendAddressParsingInfo;
        private AddressParsingInfo _ReceiveAddressParsingInfo;
        private SpecialAreaParsing _SpecialArea;
        private tcDeliveryRequests_Condition _tcDeliveryRequestsInfo;
        private decimal _id_D;
        private decimal _id_R;

        private string _checkNumber_D;
        private string _checkNumber_R;

        private tcDeliveryRequests_Condition DeliveryInfo_D = new tcDeliveryRequests_Condition();

        private tcDeliveryRequests_Condition DeliveryInfo_R = new tcDeliveryRequests_Condition();
        public ConsignmentServices(
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor,
            ItcDeliveryRequests_DA tcDeliveryRequests,
            IDeliveryStatusPublic_DA DeliveryStatusPublic,
            IttDeliveryScanLog_DA ttDeliveryScanLog,
            IDeliveryStatusPublicFilter_DA IDeliveryStatusPublicFilter_DA,
            IDeliveryStatusPublicError_DA DeliveryStatusPublicError_DA,
            IScheduleTask_DA ScheduleTask_DA,
            ItbItemCodes_DA tbItemCodes_DA,
            IStoredProcedure_DA storedProcedure_DA,
            ItbCustomers_DA tbCustomers_DA,
            Icheck_number_prehead_DA check_number_prehead_DA,
            IorgArea_DA orgArea_DA,
            Icheck_number_sd_mapping_DA check_number_sd_mapping_DA,
            IttDeliveryRequestsRecord_DA ttDeliveryRequestsRecord_DA,
            IReportPrintService reportPrintService,
             Ipickup_request_for_apiuser_DA pickup_request_for_apiuser_DA,
             ICBMDetailLog_DA CBMDetailLog_DA,
             IProductValuationManage_DA ProductValuationManage_DA,
              IProductManage_DA ProductManage_DA,
              IItemCodes_DA ItemCodes_DA
            )
        {
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _tcDeliveryRequests = tcDeliveryRequests;
            _DeliveryStatusPublic = DeliveryStatusPublic;
            _ttDeliveryScanLog_DA = ttDeliveryScanLog;
            _DeliveryStatusPublicFilter_DA = IDeliveryStatusPublicFilter_DA;
            _DeliveryStatusPublicError_DA = DeliveryStatusPublicError_DA;
            _ScheduleTask_DA = ScheduleTask_DA;
            _tbItemCodes_DA = tbItemCodes_DA;
            _storedProcedure_DA = storedProcedure_DA;
            _tbCustomers_DA = tbCustomers_DA;
            _check_number_prehead_DA = check_number_prehead_DA;
            _orgArea_DA = orgArea_DA;
            _check_number_sd_mapping_DA = check_number_sd_mapping_DA;
            _ttDeliveryRequestsRecord_DA = ttDeliveryRequestsRecord_DA;
            _reportPrintService = reportPrintService;
            _pickup_request_for_apiuser_DA = pickup_request_for_apiuser_DA;
            _CBMDetailLog_DA = CBMDetailLog_DA;
            _ProductValuationManage_DA = ProductValuationManage_DA;
            _ProductManage_DA = ProductManage_DA;
            _ItemCodes_DA = ItemCodes_DA;
            CustomerCode = _httpContextAccessor.HttpContext.Items["Account"] == null ? string.Empty : _httpContextAccessor.HttpContext.Items["Account"].ToString();

        }


        private GetDetail_Res GetAddressParsing(string Address, string CustomerCode, AddressParsingComeFromEnum ComeFrom, AddressParsingTypeEnum Type = AddressParsingTypeEnum.Lv1)
        {
            GetDetail_Res getDetail_Res = new GetDetail_Res();
            var AddressParsingURL = _configuration["AddressParsingURL"];

            //打API
            var client = new RestClient(AddressParsingURL);
            var request = new RestRequest(Method.POST);

            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body

            var response = client.Post<ResData<GetDetail_Res>>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                try
                {
                    getDetail_Res = response.Data.Data;


                }
                catch (Exception e)
                {


                }

            }
            else
            {

            }

            return getDetail_Res;
        }

        private async Task<AddressParsingInfo> ParsingSendaddress()
        {
            string fulladdress = this._insertDelivery_Req.SendAddress;
            try
            {
                var info = await Tool.GetAddressParsing(fulladdress, CustomerCode, AddressParsingComeFromEnum.PublicAPI);

                if (info == null)
                {
                    throw new MyException(ResponseCodeEnum.DeliverySendAddressError);
                }
                if (string.IsNullOrEmpty(info.City))
                {
                    throw new MyException(ResponseCodeEnum.DeliverySendAddressError);
                }
                if (string.IsNullOrEmpty(info.Town))
                {
                    throw new MyException(ResponseCodeEnum.DeliverySendAddressError);
                }
                //if (string.IsNullOrEmpty(info.Address))
                //{
                //    throw new MyException(ResponseCodeEnum.DeliverySendAddressError);
                //}
                if (string.IsNullOrEmpty(info.StationCode))
                {
                    throw new MyException(ResponseCodeEnum.DeliverySendAddressError);
                }

                return info;
            }
            catch (Exception)
            {
                throw new MyException(ResponseCodeEnum.DeliverySendAddressError);
            }
        }


        private async Task<SpecialAreaParsing> ParsingSpecialArea(string address)
        {
            string fulladdress = address;
            try
            {
                SpecialAreaParsing specialAreaParsing = new SpecialAreaParsing();


                var info = await Tool.GetSpecialAreaParsing(fulladdress, CustomerCode, AddressParsingComeFromEnum.PublicAPI);

                if (info != null)
                {

                    specialAreaParsing = info;
                    return specialAreaParsing;


                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private async Task<AddressParsingInfo> ParsingReceiveaddress()
        {
            string fulladdress = this._insertDelivery_Req.ReceiveAddress;
            try
            {
                var info = await Tool.GetAddressParsing(fulladdress, CustomerCode, AddressParsingComeFromEnum.PublicAPI);

                //if (info == null)
                //{
                //    throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
                //}
                //if (string.IsNullOrEmpty(info.City))
                //{
                //    throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
                //}
                //if (string.IsNullOrEmpty(info.Town))
                //{
                //    throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
                //}
                //if (string.IsNullOrEmpty(info.Address))
                //{
                //    throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
                //}
                //if (string.IsNullOrEmpty(info.StationCode))
                //{
                //    throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
                //}

                return info;
            }
            catch (Exception)
            {
                throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
            }
        }

        /// <summary>
        /// 確認寄件人地址
        /// </summary>
        /// <param name="fulladdress"></param>
        /// <returns></returns>
        private async Task<LyAddrFormatReturnModel> checkSendaddress()
        {
            string fulladdress = this._insertDelivery_Req.SendAddress;
            try
            {
                //去除空白換行
                fulladdress = Tool.StrTrimer(fulladdress);
                //全形轉半形
                fulladdress = Tool.ToDBC(fulladdress);
                //去頭郵遞區號
                fulladdress = Tool.ReplaceZipCode(fulladdress);
                //簡轉繁體
                string realAddress = ChineseConverter.Convert(fulladdress, ChineseConversionDirection.SimplifiedToTraditional);

                //特殊狀況
                realAddress = realAddress.Replace("樸子市", "朴子市");

                realAddress = realAddress.Replace("南莊鄉", "南庄鄉");

                var result = await _storedProcedure_DA.GetLY_AddrFormat(realAddress);

                if (realAddress.IndexOf(result.city) == 0)
                {
                    realAddress = realAddress.Replace(result.city, "");
                }
                else
                {
                    throw new MyException(ResponseCodeEnum.DeliverySendAddressError);
                }
                if (realAddress.IndexOf(result.area) == 0)
                {
                    realAddress = realAddress.Replace(result.area, "");
                }
                else
                {
                    throw new MyException(ResponseCodeEnum.DeliverySendAddressError);
                }
                result.address = realAddress;

                if (result == null)
                {
                    throw new MyException(ResponseCodeEnum.DeliverySendAddressError);

                }
                if (string.IsNullOrEmpty(result.city) || string.IsNullOrEmpty(result.area))
                {
                    throw new MyException(ResponseCodeEnum.DeliverySendAddressError);
                }

                return result;
            }
            catch (Exception)
            {
                throw new MyException(ResponseCodeEnum.DeliverySendAddressError);
            }
        }
        /// <summary>
        /// 確認收件人地址
        /// </summary>
        /// <param name="fulladdress"></param>
        /// <returns></returns>
        private async Task<LyAddrFormatReturnModel> checkReceiveaddress()
        {
            string fulladdress = this._insertDelivery_Req.ReceiveAddress;
            try
            {

                //去除空白換行
                fulladdress = Tool.StrTrimer(fulladdress);
                //全形轉半形
                fulladdress = Tool.ToDBC(fulladdress);
                //去頭郵遞區號
                fulladdress = Tool.ReplaceZipCode(fulladdress);
                //簡轉繁體
                string realAddress = ChineseConverter.Convert(fulladdress, ChineseConversionDirection.SimplifiedToTraditional);
                //特殊狀況
                realAddress = realAddress.Replace("樸子市", "朴子市");

                realAddress = realAddress.Replace("南莊鄉", "南庄鄉");

                var result = await _storedProcedure_DA.GetLY_AddrFormat(realAddress);

                if (realAddress.IndexOf(result.city) == 0)
                {
                    realAddress = realAddress.Replace(result.city, "");
                }
                else
                {
                    throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
                }
                if (realAddress.IndexOf(result.area) == 0)
                {
                    realAddress = realAddress.Replace(result.area, "");
                }
                else
                {
                    throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
                }
                result.address = realAddress;

                if (result == null)
                {
                    throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);

                }
                if (string.IsNullOrEmpty(result.city) || string.IsNullOrEmpty(result.area))
                {
                    throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
                }

                return result;
            }
            catch (Exception)
            {
                throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
            }
        }
        /// <summary>
        /// 拆地址
        /// </summary>
        /// <param name="fSEAddressEntity"></param>
        /// <returns></returns>
        private async Task<List<orgArea_Condition>> GetByFSEAddressEntity(FSEAddressEntity fSEAddressEntity)
        {
            try
            {
                var OrgAreasList = await _orgArea_DA.All();

                string postRoad = fSEAddressEntity.PostRoad;
                string postStreet = fSEAddressEntity.PostStreet;

                if (fSEAddressEntity.PostRoad != null && fSEAddressEntity.PostRoad.Length > 0 && fSEAddressEntity.PostRoad.Substring(fSEAddressEntity.PostRoad.Length - 1) == "里")
                {
                    postRoad = fSEAddressEntity.PostRoad[0..^1];
                }
                //排除所有NULL
                OrgAreasList = OrgAreasList.Where(x => !string.IsNullOrEmpty(x.CITY) && !string.IsNullOrEmpty(x.AREA) && !string.IsNullOrEmpty(x.ROAD) && !string.IsNullOrEmpty(x.EROAD)).ToList();

                var data = from orgArea in OrgAreasList where orgArea.CITY.Equals(fSEAddressEntity.City) && orgArea.AREA.Equals(fSEAddressEntity.District) && (orgArea.ROAD.Equals(postRoad) || orgArea.ROAD.Equals(postStreet)) select orgArea;

                List<orgArea_Condition> output = new List<orgArea_Condition>();

                List<orgArea_Condition> orgAreas = data.ToList();

                if (orgAreas.Count == 0)
                {
                    var data1 = from orgArea in OrgAreasList where orgArea.CITY.Equals(fSEAddressEntity.City) && (orgArea.ROAD.Equals(fSEAddressEntity.PostRoad) || orgArea.ROAD.Equals(fSEAddressEntity.PostStreet)) select orgArea;

                    orgAreas = data1.ToList();
                }

                if (orgAreas.Count == 0)
                {
                    var data1 = from orgArea in OrgAreasList where orgArea.CITY.Equals(fSEAddressEntity.City) && (orgArea.EROAD.Equals(fSEAddressEntity.PinYinRoad) || orgArea.EROAD.Equals(fSEAddressEntity.PinYinStreet)) select orgArea;

                    orgAreas = data1.ToList();
                }

                if (orgAreas.Count == 1)
                {
                    output = orgAreas;
                }
                else if (orgAreas.Count > 1)
                {
                    double findNo = 0;

                    try
                    {
                        findNo = Convert.ToDouble(fSEAddressEntity.FindNo);
                    }
                    catch
                    {

                    }

                    if (findNo == 0)
                    {
                        output.Add(orgAreas[0]);
                    }
                    else
                    {
                        bool even = true;

                        if ((findNo % 2) == 1)
                        {
                            even = false;
                        }

                        if (even)
                        {
                            var eachData = (from aa in orgAreas
                                            where aa.NO_BGN <= findNo && aa.NO_END >= findNo && aa.EVEN == 2
                                            select aa).ToList();

                            if (eachData.Count == 0)
                            {
                                eachData = (from aa in orgAreas
                                            where aa.NO_BGN <= findNo && aa.NO_END >= findNo && aa.EVEN == 0
                                            select aa).ToList();
                            }

                            if (eachData.Count == 0)
                            {
                                eachData = (from aa in orgAreas
                                            where aa.NO_BGN <= findNo && aa.NO_END >= findNo
                                            select aa).ToList();
                            }

                            if (eachData.Count == 0)
                            {
                                output.Add(orgAreas[0]);
                            }
                            else
                            {
                                output.Add(eachData[0]);
                            }
                        }
                        else
                        {
                            var eachData = (from aa in orgAreas
                                            where aa.NO_BGN <= findNo && aa.NO_END >= findNo && aa.EVEN == 1
                                            select aa).ToList();

                            if (eachData.Count == 0)
                            {
                                eachData = (from aa in orgAreas
                                            where aa.NO_BGN <= findNo && aa.NO_END >= findNo && aa.EVEN == 0
                                            select aa).ToList();
                            }

                            if (eachData.Count == 0)
                            {
                                eachData = (from aa in orgAreas
                                            where aa.NO_BGN <= findNo && aa.NO_END >= findNo
                                            select aa).ToList();
                            }

                            if (eachData.Count == 0)
                            {
                                output.Add(orgAreas[0]);
                            }
                            else
                            {
                                output.Add(eachData[0]);
                            }
                        }

                    }
                }

                return output;
            }
            catch (Exception)
            {

                return null;
            }

        }
        /// <summary>
        /// 確認帳號
        /// </summary>
        /// <returns></returns>
        private async Task CheckCustomerCode()
        {
            tbCustomers_Condition tbCustomers_Condition = new tbCustomers_Condition();
            //確認帳號
            var CustomerInfo = await _tbCustomers_DA.GetInfoByCustomerCode(CustomerCode);
            if (CustomerInfo == null)
            {
                throw new MyException(ResponseCodeEnum.CustomerNotFound);

            }
            _CustomerInfo = CustomerInfo;

            // return tbCustomers_Condition;
        }
        /// <summary>
        /// 確認託運單
        /// </summary>
        /// <returns></returns>
        private async Task CheckcheckNumber(string checkNumber)
        {
            tcDeliveryRequests_Condition tcDeliveryRequests_Condition = new tcDeliveryRequests_Condition();
            //檢查貨號 checknumber 

            var result = (await _tcDeliveryRequests.GetInfoByCheckNumber(checkNumber)).FirstOrDefault();

            if (result == null)
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            }
            _tcDeliveryRequestsInfo = result;

        }

        //private async Task<LyGetDistributorReturnModel> GetSendDistributor()
        //{
        //    LyGetDistributorReturnModel lyGetDistributorReturnModel = new LyGetDistributorReturnModel();

        //    string city = _SendLyAddrFormatReturnModel.city;
        //    string area = _SendLyAddrFormatReturnModel.area;

        //    var SendAddressSupplier = await _storedProcedure_DA.GetLY_GetDistributor(city, area);
        //    if (SendAddressSupplier == null)
        //    {
        //        throw new MyException(ResponseCodeEnum.DeliverySendAddressError);

        //    }
        //    if (string.IsNullOrEmpty(SendAddressSupplier.area_arrive_code))
        //    {
        //        throw new MyException(ResponseCodeEnum.DeliverySendAddressError);
        //    }
        //    lyGetDistributorReturnModel = SendAddressSupplier;
        //    return lyGetDistributorReturnModel;
        //}
        //private async Task<LyGetDistributorReturnModel> GetReceiveDistributor()
        //{
        //    LyGetDistributorReturnModel lyGetDistributorReturnModel = new LyGetDistributorReturnModel();

        //    string city = _ReceiveLyAddrFormatReturnModel.city;
        //    string area = _ReceiveLyAddrFormatReturnModel.area;

        //    var SendAddressSupplier = await _storedProcedure_DA.GetLY_GetDistributor(city, area);
        //    if (SendAddressSupplier == null)
        //    {
        //        throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);

        //    }
        //    if (string.IsNullOrEmpty(SendAddressSupplier.area_arrive_code))
        //    {
        //        throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
        //    }
        //    lyGetDistributorReturnModel = SendAddressSupplier;
        //    return lyGetDistributorReturnModel;
        //}

        private async Task CreateD()
        {
            var req = this._insertDelivery_Req;

            DeliveryInfo_D.customer_code = CustomerCode;
            DeliveryInfo_D.check_type = "1";
            DeliveryInfo_D.order_number = req.OrderNumber;
            if (req.CollectionMoney > 0)
            {
                DeliveryInfo_D.subpoena_category = "41";
            }
            else
            {
                DeliveryInfo_D.subpoena_category = "11";
            }
            DeliveryInfo_D.collection_money = req.CollectionMoney;
            DeliveryInfo_D.receive_tel1 = req.ReceiveTel1;
            DeliveryInfo_D.receive_tel2 = req.ReceiveTel2;
            DeliveryInfo_D.receive_contact = req.ReceiveContact;
            DeliveryInfo_D.receive_city = _ReceiveAddressParsingInfo.City;
            DeliveryInfo_D.receive_area = _ReceiveAddressParsingInfo.Town;
            DeliveryInfo_D.receive_address = _ReceiveAddressParsingInfo.Address.Replace(_ReceiveAddressParsingInfo.City, "").Replace(_ReceiveAddressParsingInfo.Town, "");
            DeliveryInfo_D.area_arrive_code = _ReceiveAddressParsingInfo.StationCode;
            DeliveryInfo_D.ShuttleStationCode = _ReceiveAddressParsingInfo.ShuttleStationCode;
            DeliveryInfo_D.send_contact = req.SendContact;
            DeliveryInfo_D.send_tel = req.SendTel;
            DeliveryInfo_D.send_city = _SendAddressParsingInfo.City;
            DeliveryInfo_D.send_area = _SendAddressParsingInfo.Town;
            DeliveryInfo_D.send_address = _SendAddressParsingInfo.Address.Replace(_SendAddressParsingInfo.City, "").Replace(_SendAddressParsingInfo.Town, "");
            DeliveryInfo_D.supplier_code = _CustomerInfo.supplier_code;
            DeliveryInfo_D.send_station_scode = _SendAddressParsingInfo.StationCode;
            DeliveryInfo_D.CbmSize = req.CbmSize;
            DeliveryInfo_D.invoice_desc = req.InvoiceDesc;
            DateTime? Arriveassigndate = string.IsNullOrEmpty(req.Arriveassigndate) ? null : DateTime.ParseExact(req.Arriveassigndate, "yyyyMMdd", null);
            DeliveryInfo_D.arrive_assign_date = Arriveassigndate;
            DeliveryInfo_D.print_date = Arriveassigndate == null ? DateTime.Now.Date : Arriveassigndate;
            DeliveryInfo_D.SendPlatform = req.SendPlatform;
            DeliveryInfo_D.ArticleNumber = req.ArticleNumber;
            DeliveryInfo_D.ArticleName = req.ArticleName;
            DeliveryInfo_D.cuser = CustomerCode;
            DeliveryInfo_D.cdate = DateTime.Now;
            DeliveryInfo_D.uuser = CustomerCode;
            DeliveryInfo_D.udate = DateTime.Now;
            DeliveryInfo_D.pricing_type = "02";
            DeliveryInfo_D.pieces = req.Pieces.HasValue ? req.Pieces : 1;
            DeliveryInfo_D.plates = 0;
            DeliveryInfo_D.receive_by_arrive_site_flag = false;
            DeliveryInfo_D.pallet_recycling_flag = false;
            DeliveryInfo_D.Less_than_truckload = true;
            DeliveryInfo_D.pallet_recycling_flag = false;
            DeliveryInfo_D.print_flag = false;
            DeliveryInfo_D.add_transfer = false;


            DeliveryInfo_D.SendCode = _SendAddressParsingInfo.StationCode;
            DeliveryInfo_D.SendSD = string.IsNullOrEmpty(_SendAddressParsingInfo.SendSalesDriverCode) ? string.Empty : _SendAddressParsingInfo.SendSalesDriverCode;
            DeliveryInfo_D.SendMD = string.IsNullOrEmpty(_SendAddressParsingInfo.SendMotorcycleDriverCode) ? string.Empty : _SendAddressParsingInfo.SendMotorcycleDriverCode;

            DeliveryInfo_D.ReceiveCode = _ReceiveAddressParsingInfo.StationCode;
            DeliveryInfo_D.ReceiveSD = string.IsNullOrEmpty(_ReceiveAddressParsingInfo.SalesDriverCode) ? string.Empty : _ReceiveAddressParsingInfo.SalesDriverCode;
            DeliveryInfo_D.ReceiveMD = string.IsNullOrEmpty(_ReceiveAddressParsingInfo.MotorcycleDriverCode) ? string.Empty : _ReceiveAddressParsingInfo.MotorcycleDriverCode;
            DeliveryInfo_D.ReportFee = req.Insurance;

            switch (req.DeliveryMethod)
            {
                case 1:
                    DeliveryInfo_D.DeliveryType = "D";
                    DeliveryInfo_D.round_trip = false;
                    break;
                case 2:
                    DeliveryInfo_D.DeliveryType = "R";
                    DeliveryInfo_D.round_trip = false;
                    break;
                default:
                    break;
            }

            DeliveryInfo_D.receipt_flag = req.receiptFlag.Value == 1 ? true : false;

            switch (req.TimePeriod)
            {
                case 0:
                    DeliveryInfo_D.time_period = "不指定";
                    break;
                case 1:
                    DeliveryInfo_D.time_period = "早";
                    break;
                case 2:
                    DeliveryInfo_D.time_period = "中";
                    break;
                case 3:
                    DeliveryInfo_D.time_period = "晚";
                    break;
                default:
                    DeliveryInfo_D.time_period = "不指定";
                    break;
            }


            if (_SpecialArea != null)
            {

                if (_SpecialArea.id > 0 && _SpecialArea.Fee > 0 && !string.IsNullOrEmpty(_SpecialArea.TimeLimit))
                {
                    DeliveryInfo_D.SpecialAreaFee = _SpecialArea.Fee;
                    DeliveryInfo_D.SpecialAreaId = (int)_SpecialArea.id;
                }
                else
                {
                    DeliveryInfo_D.SpecialAreaFee = 0;
                    DeliveryInfo_D.SpecialAreaId = 0;
                }

            }

            //保值
            if (req.BuyInsurance.Value || req.CollectionMoney > 10000)
            {
                if (req.CollectionMoney > req.Insurance)
                {

                    DeliveryInfo_D.ProductValue = Convert.ToInt16(Math.Ceiling((double)req.CollectionMoney.Value / 100));

                }
                else
                {
                    DeliveryInfo_D.ProductValue = Convert.ToInt16(Math.Ceiling((double)req.Insurance.Value / 100));
                }

            }


            _id_D = await _tcDeliveryRequests.InsertRetureId(DeliveryInfo_D);
        }

        private async Task CreateR()
        {
            //清空PK
            DeliveryInfo_R.request_id = 0;
            DeliveryInfo_R.check_number = null;
            //對應check_number
            DeliveryInfo_R.return_check_number = DeliveryInfo_D.check_number;
            //逆物流
            DeliveryInfo_R.DeliveryType = "R";
            //交換電話
            DeliveryInfo_R.receive_tel1 = DeliveryInfo_D.send_tel;
            DeliveryInfo_R.send_tel = DeliveryInfo_D.receive_tel1 + (string.IsNullOrEmpty(DeliveryInfo_D.receive_tel1_ext) ? string.Empty : "#" + DeliveryInfo_D.receive_tel1_ext);
            //交換名稱
            DeliveryInfo_R.receive_contact = DeliveryInfo_D.send_contact;
            DeliveryInfo_R.send_contact = DeliveryInfo_D.receive_contact;
            //交換地址
            DeliveryInfo_R.receive_city = DeliveryInfo_D.send_city;
            DeliveryInfo_R.receive_area = DeliveryInfo_D.send_area;
            DeliveryInfo_R.receive_address = DeliveryInfo_D.send_address;
            DeliveryInfo_R.send_city = DeliveryInfo_D.receive_city;
            DeliveryInfo_R.send_area = DeliveryInfo_D.receive_area;
            DeliveryInfo_R.send_address = DeliveryInfo_D.receive_address;
            //站所交換
            DeliveryInfo_R.area_arrive_code = DeliveryInfo_D.send_station_scode;
            DeliveryInfo_R.send_station_scode = DeliveryInfo_D.area_arrive_code;

            //其餘
            DeliveryInfo_R.pricing_type = DeliveryInfo_D.pricing_type;
            DeliveryInfo_R.customer_code = DeliveryInfo_D.customer_code;
            DeliveryInfo_R.check_type = DeliveryInfo_D.check_type;
            DeliveryInfo_R.order_number = DeliveryInfo_D.order_number;
            DeliveryInfo_R.receive_customer_code = DeliveryInfo_D.receive_customer_code;
            DeliveryInfo_R.subpoena_category = DeliveryInfo_D.subpoena_category;
            DeliveryInfo_R.receive_by_arrive_site_flag = DeliveryInfo_D.receive_by_arrive_site_flag;
            DeliveryInfo_R.arrive_address = DeliveryInfo_D.arrive_address;
            DeliveryInfo_R.pieces = DeliveryInfo_D.pieces;
            DeliveryInfo_R.plates = DeliveryInfo_D.plates;
            DeliveryInfo_R.cbm = DeliveryInfo_D.cbm;
            DeliveryInfo_R.CbmSize = DeliveryInfo_D.CbmSize;
            DeliveryInfo_R.collection_money = DeliveryInfo_D.collection_money;
            DeliveryInfo_R.arrive_to_pay_freight = DeliveryInfo_D.arrive_to_pay_freight;
            DeliveryInfo_R.arrive_to_pay_append = DeliveryInfo_D.arrive_to_pay_append;
            DeliveryInfo_R.donate_invoice_flag = DeliveryInfo_D.donate_invoice_flag;
            DeliveryInfo_R.electronic_invoice_flag = DeliveryInfo_D.electronic_invoice_flag;
            DeliveryInfo_R.uniform_numbers = DeliveryInfo_D.uniform_numbers;
            DeliveryInfo_R.arrive_mobile = DeliveryInfo_D.arrive_mobile;
            DeliveryInfo_R.arrive_email = DeliveryInfo_D.arrive_email;
            DeliveryInfo_R.invoice_memo = DeliveryInfo_D.invoice_memo;
            DeliveryInfo_R.invoice_desc = DeliveryInfo_D.invoice_desc;
            DeliveryInfo_R.product_category = DeliveryInfo_D.product_category;
            DeliveryInfo_R.special_send = DeliveryInfo_D.special_send;
            DeliveryInfo_R.arrive_assign_date = DeliveryInfo_D.arrive_assign_date;
            DeliveryInfo_R.time_period = DeliveryInfo_D.time_period;
            DeliveryInfo_R.receipt_flag = DeliveryInfo_D.receipt_flag;
            DeliveryInfo_R.pallet_recycling_flag = DeliveryInfo_D.pallet_recycling_flag;
            DeliveryInfo_R.Pallet_type = DeliveryInfo_D.Pallet_type;
            DeliveryInfo_R.supplier_code = DeliveryInfo_D.supplier_code;
            DeliveryInfo_R.ShuttleStationCode = DeliveryInfo_D.ShuttleStationCode;
            DeliveryInfo_R.supplier_name = DeliveryInfo_D.supplier_name;
            DeliveryInfo_R.supplier_date = DeliveryInfo_D.supplier_date;
            DeliveryInfo_R.receipt_numbe = DeliveryInfo_D.receipt_numbe;
            DeliveryInfo_R.supplier_fee = DeliveryInfo_D.supplier_fee;
            DeliveryInfo_R.csection_fee = DeliveryInfo_D.csection_fee;
            DeliveryInfo_R.remote_fee = DeliveryInfo_D.remote_fee;
            DeliveryInfo_R.total_fee = DeliveryInfo_D.total_fee;
            DeliveryInfo_R.print_date = DateTime.Now.Date;
            DeliveryInfo_R.print_flag = DeliveryInfo_D.print_flag;
            DeliveryInfo_R.checkout_close_date = DeliveryInfo_D.checkout_close_date;
            DeliveryInfo_R.add_transfer = DeliveryInfo_D.add_transfer;
            DeliveryInfo_R.sub_check_number = DeliveryInfo_D.sub_check_number;
            DeliveryInfo_R.import_randomCode = DeliveryInfo_D.import_randomCode;
            DeliveryInfo_R.close_randomCode = DeliveryInfo_D.close_randomCode;
            DeliveryInfo_R.turn_board = DeliveryInfo_D.turn_board;
            DeliveryInfo_R.upstairs = DeliveryInfo_D.upstairs;
            DeliveryInfo_R.difficult_delivery = DeliveryInfo_D.difficult_delivery;
            DeliveryInfo_R.turn_board_fee = DeliveryInfo_D.turn_board_fee;
            DeliveryInfo_R.upstairs_fee = DeliveryInfo_D.upstairs_fee;
            DeliveryInfo_R.difficult_fee = DeliveryInfo_D.difficult_fee;
            // DeliveryInfo_R.cancel_date = DeliveryInfo_D.cancel_date;
            DeliveryInfo_R.cuser = CustomerCode;
            DeliveryInfo_R.cdate = DateTime.Now;
            DeliveryInfo_R.uuser = CustomerCode;
            DeliveryInfo_R.udate = DateTime.Now;
            DeliveryInfo_R.HCTstatus = DeliveryInfo_D.HCTstatus;
            DeliveryInfo_R.is_pallet = DeliveryInfo_D.is_pallet;
            DeliveryInfo_R.pallet_request_id = DeliveryInfo_D.pallet_request_id;
            DeliveryInfo_R.Less_than_truckload = DeliveryInfo_D.Less_than_truckload;
            DeliveryInfo_R.Distributor = DeliveryInfo_D.Distributor;
            DeliveryInfo_R.temperate = DeliveryInfo_D.temperate;
            DeliveryInfo_R.cbmLength = DeliveryInfo_D.cbmLength;
            DeliveryInfo_R.cbmWidth = DeliveryInfo_D.cbmWidth;
            DeliveryInfo_R.cbmHeight = DeliveryInfo_D.cbmHeight;
            DeliveryInfo_R.cbmWeight = DeliveryInfo_D.cbmWeight;
            DeliveryInfo_R.cbmCont = DeliveryInfo_D.cbmCont;
            DeliveryInfo_R.bagno = DeliveryInfo_D.bagno;
            DeliveryInfo_R.ArticleNumber = DeliveryInfo_D.ArticleNumber;
            DeliveryInfo_R.SendPlatform = DeliveryInfo_D.SendPlatform;
            DeliveryInfo_R.ArticleName = DeliveryInfo_D.ArticleName;
            DeliveryInfo_R.round_trip = DeliveryInfo_D.round_trip;
            DeliveryInfo_R.ship_date = DeliveryInfo_D.ship_date;
            DeliveryInfo_R.VoucherMoney = DeliveryInfo_D.VoucherMoney;
            DeliveryInfo_R.CashMoney = DeliveryInfo_D.CashMoney;
            DeliveryInfo_R.pick_up_good_type = DeliveryInfo_D.pick_up_good_type;
            DeliveryInfo_R.latest_scan_date = DeliveryInfo_D.latest_scan_date;
            DeliveryInfo_R.delivery_complete_date = DeliveryInfo_D.delivery_complete_date;
            DeliveryInfo_R.pic_path = DeliveryInfo_D.pic_path;

            DeliveryInfo_R.Is_write_off = DeliveryInfo_D.Is_write_off;
            DeliveryInfo_R.freight = DeliveryInfo_D.freight;
            DeliveryInfo_R.latest_dest_date = DeliveryInfo_D.latest_dest_date;
            DeliveryInfo_R.latest_delivery_date = DeliveryInfo_D.latest_delivery_date;
            DeliveryInfo_R.latest_arrive_option_date = DeliveryInfo_D.latest_arrive_option_date;
            DeliveryInfo_R.latest_dest_driver = DeliveryInfo_D.latest_dest_driver;
            DeliveryInfo_R.latest_delivery_driver = DeliveryInfo_D.latest_delivery_driver;
            DeliveryInfo_R.latest_arrive_option_driver = DeliveryInfo_D.latest_arrive_option_driver;
            DeliveryInfo_R.latest_arrive_option = DeliveryInfo_D.latest_arrive_option;
            DeliveryInfo_R.delivery_date = DeliveryInfo_D.delivery_date;
            DeliveryInfo_R.payment_method = DeliveryInfo_D.payment_method;
            DeliveryInfo_R.catch_cbm_pic_path_from_S3 = DeliveryInfo_D.catch_cbm_pic_path_from_S3;
            DeliveryInfo_R.catch_taichung_cbm_pic_path_from_S3 = DeliveryInfo_D.catch_taichung_cbm_pic_path_from_S3;
            DeliveryInfo_R.latest_scan_item = DeliveryInfo_D.latest_scan_item;
            DeliveryInfo_R.latest_scan_arrive_option = DeliveryInfo_D.latest_scan_arrive_option;
            DeliveryInfo_R.latest_scan_driver_code = DeliveryInfo_D.latest_scan_driver_code;
            DeliveryInfo_R.latest_dest_exception_option = DeliveryInfo_D.latest_dest_exception_option;
            DeliveryInfo_R.holiday_delivery = DeliveryInfo_D.holiday_delivery;
            DeliveryInfo_R.roundtrip_checknumber = DeliveryInfo_D.roundtrip_checknumber;
            //DeliveryInfo_R.closing_date = DeliveryInfo_D.closing_date;
            DeliveryInfo_R.custom_label = DeliveryInfo_D.custom_label;
            DeliveryInfo_R.latest_pick_up_scan_log_id = DeliveryInfo_D.latest_pick_up_scan_log_id;
            DeliveryInfo_R.orange_r1_1_uuser = DeliveryInfo_D.orange_r1_1_uuser;
            DeliveryInfo_R.sign_paper_print_flag = DeliveryInfo_D.sign_paper_print_flag;
            DeliveryInfo_R.ProductId = DeliveryInfo_D.ProductId;
            DeliveryInfo_R.SpecCodeId = DeliveryInfo_D.SpecCodeId;
            DeliveryInfo_R.ShuttleStationCode = DeliveryInfo_D.ShuttleStationCode;
            DeliveryInfo_R.ProductValue = DeliveryInfo_D.ProductValue;
            DeliveryInfo_R.SpecialAreaFee = DeliveryInfo_D.SpecialAreaFee;
            DeliveryInfo_R.SpecialAreaId = DeliveryInfo_D.SpecialAreaId;

            DeliveryInfo_R.SendCode = DeliveryInfo_D.ReceiveCode;
            DeliveryInfo_R.SendSD = DeliveryInfo_D.ReceiveSD;
            DeliveryInfo_R.SendMD = DeliveryInfo_D.ReceiveMD;

            DeliveryInfo_R.ReceiveCode = DeliveryInfo_D.SendCode;
            DeliveryInfo_R.ReceiveSD = DeliveryInfo_D.SendSD;
            DeliveryInfo_R.ReceiveMD = DeliveryInfo_D.SendMD;

            DeliveryInfo_R.ReportFee = DeliveryInfo_D.ReportFee;


            _id_R = await _tcDeliveryRequests.InsertRetureId(DeliveryInfo_R);
        }
        /// <summary>
        /// 新增託運單
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<List<InsertDeliveryForBusiness_Res>> InsertDeliveryForBusiness(InsertDeliveryForBusiness_Req req)
        {

            List<InsertDeliveryForBusiness_Res> list = new List<InsertDeliveryForBusiness_Res>();

            InsertDelivery_Req insertDelivery_Req = new InsertDelivery_Req();
            insertDelivery_Req.OrderNumber = req.OrderNumber;
            insertDelivery_Req.ReceiveContact = req.ReceiveContact;
            insertDelivery_Req.ReceiveTel1 = req.ReceiveTel1;
            insertDelivery_Req.ReceiveTel2 = req.ReceiveTel2;
            insertDelivery_Req.ReceiveAddress = req.ReceiveAddress;
            insertDelivery_Req.Weight = req.Weight;
            insertDelivery_Req.PaymentMethod = req.PaymentMethod;
            insertDelivery_Req.CollectionMoney = req.CollectionMoney;
            insertDelivery_Req.CbmSize = req.CbmSize;
            insertDelivery_Req.Arriveassigndate = req.Arriveassigndate;
            insertDelivery_Req.TimePeriod = req.TimePeriod;
            insertDelivery_Req.InvoiceDesc = req.InvoiceDesc;
            insertDelivery_Req.ArticleNumber = req.ArticleNumber;
            insertDelivery_Req.SendPlatform = req.SendPlatform;
            insertDelivery_Req.ArticleName = req.ArticleName;
            insertDelivery_Req.DeliveryMethod = req.DeliveryMethod;
            insertDelivery_Req.receiptFlag = req.receiptFlag;

            if (req.ProductId=="CS000029")
            {
                req.ProductId = "CM000030";
            }

            insertDelivery_Req.ProductId = req.ProductId;
            insertDelivery_Req.Spec = req.Spec;
            insertDelivery_Req.Pieces = req.Pieces;
            insertDelivery_Req.MeasurementInfos = req.MeasurementInfos;
            insertDelivery_Req.Insurance = req.Insurance;
            insertDelivery_Req.BuyInsurance = req.BuyInsurance;
            //確認帳號
            await CheckCustomerCode();


            //確認產品無誤
            DateTime dateTime = DateTime.Now;

            ProductValuationManage_Condition productValuationManage_Condition = new ProductValuationManage_Condition();
            productValuationManage_Condition.CustomerCode = this.CustomerCode;
            productValuationManage_Condition.StartDate = dateTime;
            productValuationManage_Condition.EndDate = dateTime;
            var ProductList = await _ProductValuationManage_DA.SearchInfo(productValuationManage_Condition);

            //看客代 
            bool isNewCustomer = _CustomerInfo.is_new_customer.HasValue ? _CustomerInfo.is_new_customer.Value : false;
            //新客代 沒有代參數
            if (isNewCustomer && req.ProductId != null && req.Spec != null)
            {
                //判斷正逆物流
                var proD = ProductList.Where(x => x.ProductId.Equals(req.ProductId) && x.Spec.Equals(req.Spec) && x.DeliveryType.Equals("D")).FirstOrDefault();
                var proR = ProductList.Where(x => x.ProductId.Equals(req.ProductId) && x.Spec.Equals(req.Spec) && x.DeliveryType.Equals("R")).FirstOrDefault();
                switch (req.DeliveryMethod)
                {
                    case 1:
                        if (proD == null)
                        {
                            throw new MyException(ResponseCodeEnum.ParameterError);
                        }
                        break;
                    case 2:
                        if (proR == null)
                        {
                            throw new MyException(ResponseCodeEnum.ParameterError);
                        }
                        break;
                    default:

                        if (proD == null || proR == null)
                        {
                            throw new MyException(ResponseCodeEnum.ParameterError);
                        }
                        break;
                }

                DeliveryInfo_D.ProductId = req.ProductId;
                DeliveryInfo_D.SpecCodeId = req.Spec;
            }
            else if (!isNewCustomer)
            {
                //預設產品項目

                //預購代
                if (_CustomerInfo.product_type == 2)
                {
                    //B003 3號
                    DeliveryInfo_D.ProductId = "PS000031";
                    DeliveryInfo_D.SpecCodeId = "B003";

                }//預購箱
                else if (_CustomerInfo.product_type == 3)
                {
                    //X003 3號
                    DeliveryInfo_D.ProductId = "PS000034";
                    DeliveryInfo_D.SpecCodeId = "X003";
                }
                else
                {
                    DeliveryInfo_D.ProductId = "CS000035";

                    if (req.CbmSize.Equals(null))
                    {
                        req.CbmSize = 1;
                    }

                    switch (req.CbmSize.Value)
                    {
                        case 1:
                            DeliveryInfo_D.SpecCodeId = "S060";
                            break;
                        case 2:
                            DeliveryInfo_D.SpecCodeId = "S090";
                            break;
                        case 3:
                            DeliveryInfo_D.SpecCodeId = "S120";
                            break;
                        case 4:
                            DeliveryInfo_D.SpecCodeId = "S150";
                            break;
                        case 6:
                            DeliveryInfo_D.SpecCodeId = "S110";
                            break;
                        default:
                            DeliveryInfo_D.SpecCodeId = "N001";
                            break;
                    }
                }
            }
            else
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            //檢查CBM參數是否都對
            var ProductSpec = await _ItemCodes_DA.GetInfo("ProductSpec");
            if (req.MeasurementInfos != null && req.MeasurementInfos.Any())
            {
                foreach (var item in req.MeasurementInfos)
                {
                    if (string.IsNullOrEmpty(item.MeasurementCode))
                    {
                        throw new MyException(ResponseCodeEnum.ParameterError);
                    }
                    else
                    {
                        var _ProductSpec = ProductSpec.Where(x => x.CodeId.Equals(item.MeasurementCode)).FirstOrDefault();
                        if (_ProductSpec == null)
                        {
                            throw new MyException(ResponseCodeEnum.ParameterError);

                        }
                    }
                }
            }

            //補剩下的
            insertDelivery_Req.SendContact = _CustomerInfo.customer_name;
            insertDelivery_Req.SendTel = _CustomerInfo.telephone;
            insertDelivery_Req.SendAddress = _CustomerInfo.shipments_city + _CustomerInfo.shipments_area + _CustomerInfo.shipments_road;

            this._insertDelivery_Req = insertDelivery_Req;

            //代收貨款是否超過2萬
            if (req.CollectionMoney > 20000)
            {
                throw new MyException(ResponseCodeEnum.DeliveryCollectionMoneyOver);
            }

            ////檢查寄件人地址
            //_SendLyAddrFormatReturnModel = await checkSendaddress();
            //_SendLyGetDistributorReturnModel = await GetSendDistributor();

            ////檢查收件人地址
            //_ReceiveLyAddrFormatReturnModel = await checkReceiveaddress();
            //_ReceiveLyGetDistributorReturnModel = await GetReceiveDistributor();

            //地址解析
            _SendAddressParsingInfo = await ParsingSendaddress();
            //_ReceiveAddressParsingInfo = await ParsingReceiveaddress();
            //_SpecialArea = await ParsingSpecialArea(this._insertDelivery_Req.ReceiveAddress);

            //if (_SpecialArea != null)
            //{
            //    _ReceiveAddressParsingInfo.City = _SpecialArea.City;
            //    _ReceiveAddressParsingInfo.Town = _SpecialArea.Town;
            //    _ReceiveAddressParsingInfo.Address = _SpecialArea.Road;
            //    _ReceiveAddressParsingInfo.StationCode = _SpecialArea.StationCode;
            //    _ReceiveAddressParsingInfo.ShuttleStationCode = _SpecialArea.ShuttleStationCode;
            //    _ReceiveAddressParsingInfo.SalesDriverCode = _SpecialArea.SalesDriverCode;
            //    _ReceiveAddressParsingInfo.MotorcycleDriverCode = _SpecialArea.MotorcycleDriverCode;
            //    _ReceiveAddressParsingInfo.StackCode = _SpecialArea.StackCode;
            //}


            List<Task> ss = new List<Task>();

            SpecialAreaParsing S1 = new SpecialAreaParsing();
            SpecialAreaParsing S2 = new SpecialAreaParsing();
            var t1 = Task.Run(async () =>
            {
                try
                {
                    //特服
                    S1 = await ParsingSpecialArea(this._insertDelivery_Req.ReceiveAddress);
                }
                catch (Exception e)
                {


                }
            });
            ss.Add(t1);
            var t2 = Task.Run(async () =>
            {
                try
                {
                    _ReceiveAddressParsingInfo = await ParsingReceiveaddress();
                    if (_ReceiveAddressParsingInfo != null)
                    {
                        var c = _ReceiveAddressParsingInfo.City ?? string.Empty;
                        var t = _ReceiveAddressParsingInfo.Town ?? string.Empty;
                        var r = _ReceiveAddressParsingInfo.Road ?? string.Empty;
                        S2 = await ParsingSpecialArea(c + t + r);
                    }

                }
                catch (Exception e)
                {

                }

            });
            ss.Add(t2);

            var aaaaa = Task.WaitAll(ss.ToArray(), 3000);


            //如果兩個特服都有
            if (S1 != null && S2 != null)
            {
                if (S1.Fee >= S2.Fee)
                {
                    _SpecialArea = S1;
                }
                else
                {
                    _SpecialArea = S2;
                }

            }
            else if (S1 != null)
            {
                _SpecialArea = S1;
            }
            else if (S2 != null)
            {
                _SpecialArea = S2;
            }
            else
            {
                _SpecialArea.id = -1;
                _SpecialArea.Fee = 0;
            }
            if (_SpecialArea != null && _SpecialArea.id > 0)
            {

                _ReceiveAddressParsingInfo.City = _SpecialArea.City;
                _ReceiveAddressParsingInfo.Town = _SpecialArea.Town;
                _ReceiveAddressParsingInfo.Address = _SpecialArea.Road;
                _ReceiveAddressParsingInfo.StationCode = _SpecialArea.StationCode;
                _ReceiveAddressParsingInfo.ShuttleStationCode = _SpecialArea.ShuttleStationCode;
                _ReceiveAddressParsingInfo.SalesDriverCode = _SpecialArea.SalesDriverCode;
                _ReceiveAddressParsingInfo.MotorcycleDriverCode = _SpecialArea.MotorcycleDriverCode;
                _ReceiveAddressParsingInfo.StackCode = _SpecialArea.StackCode;
            }


            try
            {
                using (var _transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {

                    string CBM = string.Empty;

                    if (req.CbmSize.HasValue)
                    {
                        if (req.CbmSize.Equals(null))
                        {
                            req.CbmSize = 1;
                        }



                        switch (req.CbmSize.Value)
                        {
                            case 1:
                                CBM = "S060";
                                break;
                            case 2:
                                CBM = "S090";
                                break;
                            case 3:
                                CBM = "S120";
                                break;
                            case 4:
                                CBM = "S150";
                                break;
                            case 6:
                                CBM = "S110";
                                break;
                            default:
                                CBM = "S090";
                                break;
                        }
                    }
                    List<CBMDetailLog_Condition> cBMDetailLog_Conditions = new List<CBMDetailLog_Condition>();


                    switch (req.DeliveryMethod)
                    {
                        case 1:
                            DeliveryInfo_D.DeliveryType = "D";
                            DeliveryInfo_D.round_trip = false;

                            await CreateD();
                            _checkNumber_D = await CreateCheckNumber(_id_D, (DeliveryMethod)req.DeliveryMethod);
                            DeliveryInfo_D.request_id = _id_D;
                            DeliveryInfo_D.check_number = _checkNumber_D;
                            await _tcDeliveryRequests.Update(DeliveryInfo_D);

                            if (req.MeasurementInfos == null)
                            {
                                if (!string.IsNullOrEmpty(CBM))
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = CBM;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                                else
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = "S090";
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            else if (req.MeasurementInfos.Any())
                            {
                                foreach (var cbm in req.MeasurementInfos)
                                {

                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = cbm.MeasurementCode;
                                        cBMDetailLog_Condition.Width = cbm.Width;
                                        cBMDetailLog_Condition.Height = cbm.Height;
                                        cBMDetailLog_Condition.Length = cbm.Length;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            break;
                        case 2:
                            DeliveryInfo_D.DeliveryType = "R";
                            DeliveryInfo_D.round_trip = false;

                            await CreateD();
                            _checkNumber_D = await CreateCheckNumber(_id_D, (DeliveryMethod)req.DeliveryMethod);
                            DeliveryInfo_D.request_id = _id_D;
                            DeliveryInfo_D.check_number = _checkNumber_D;
                            // DeliveryInfo_R.return_check_number = _checkNumber_D;
                            await _tcDeliveryRequests.Update(DeliveryInfo_R);
                            DeliveryInfo_D.return_check_number = _checkNumber_R;
                            await _tcDeliveryRequests.Update(DeliveryInfo_D);
                            if (req.MeasurementInfos == null)
                            {
                                if (!string.IsNullOrEmpty(CBM))
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = CBM;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                                else
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = "S090";
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            else if (req.MeasurementInfos.Any())
                            {
                                foreach (var cbm in req.MeasurementInfos)
                                {

                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = cbm.MeasurementCode;
                                        cBMDetailLog_Condition.Width = cbm.Width;
                                        cBMDetailLog_Condition.Height = cbm.Height;
                                        cBMDetailLog_Condition.Length = cbm.Length;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }


                            break;
                        case 3://來回
                            //正
                            DeliveryInfo_D.DeliveryType = "D";
                            DeliveryInfo_D.round_trip = true;
                            await CreateD();
                            _checkNumber_D = await CreateCheckNumber(_id_D, DeliveryMethod.Forward);
                            DeliveryInfo_D.request_id = _id_D;
                            DeliveryInfo_D.check_number = _checkNumber_D;
                            await _tcDeliveryRequests.Update(DeliveryInfo_D);
                            if (req.MeasurementInfos == null)
                            {
                                if (!string.IsNullOrEmpty(CBM))
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = CBM;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                                else
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = "S090";
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            else if (req.MeasurementInfos.Any())
                            {
                                foreach (var cbm in req.MeasurementInfos)
                                {

                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = cbm.MeasurementCode;
                                        cBMDetailLog_Condition.Width = cbm.Width;
                                        cBMDetailLog_Condition.Height = cbm.Height;
                                        cBMDetailLog_Condition.Length = cbm.Length;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }


                            //逆
                            DeliveryInfo_R.DeliveryType = "R";
                            DeliveryInfo_R.round_trip = true;
                            await CreateR();
                            _checkNumber_R = await CreateCheckNumber(_id_R, DeliveryMethod.Returned);
                            DeliveryInfo_R.request_id = _id_R;
                            DeliveryInfo_R.check_number = _checkNumber_R;
                            DeliveryInfo_R.roundtrip_checknumber = _checkNumber_D;
                            await _tcDeliveryRequests.Update(DeliveryInfo_R);
                            DeliveryInfo_D.roundtrip_checknumber = _checkNumber_R;
                            await _tcDeliveryRequests.Update(DeliveryInfo_D);
                            if (req.MeasurementInfos == null)
                            {
                                if (!string.IsNullOrEmpty(CBM))
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_R;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = CBM;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                                else
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_R;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = "S090";
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            else if (req.MeasurementInfos.Any())
                            {
                                foreach (var cbm in req.MeasurementInfos)
                                {

                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_R;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = cbm.MeasurementCode;
                                        cBMDetailLog_Condition.Width = cbm.Width;
                                        cBMDetailLog_Condition.Height = cbm.Height;
                                        cBMDetailLog_Condition.Length = cbm.Length;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }



                            break;
                        default:
                            break;
                    }

                    InsertDeliveryForBusiness_Res data = new InsertDeliveryForBusiness_Res();
                    data.CheckNumber = _checkNumber_D;

                    data.MotorcycleDriverCode = _ReceiveAddressParsingInfo.MotorcycleDriverCode;
                    data.SalesDriverCode = _ReceiveAddressParsingInfo.SalesDriverCode;
                    data.StackCode = _ReceiveAddressParsingInfo.StackCode;
                    data.ShuttleStationCode = _ReceiveAddressParsingInfo.ShuttleStationCode;
                    data.AreaCode = _ReceiveAddressParsingInfo.StationCode;
                    data.SupplierCode = _SendAddressParsingInfo.StationCode;
                    data.DeliveryType = DeliveryInfo_D.DeliveryType;
                    data.SendMotorcycleDriverCode = _ReceiveAddressParsingInfo.SendMotorcycleDriverCode;
                    data.SendSalesDriverCode = _ReceiveAddressParsingInfo.SendSalesDriverCode;

                    list.Add(data);

                    await _tcDeliveryRequests.InsertAPIorigin(CustomerCode, "InsertDeliveryForBusiness");

                    if (req.DeliveryMethod == 3)
                    {
                        //SDMD-R

                        InsertDeliveryForBusiness_Res data_r = new InsertDeliveryForBusiness_Res();
                        data_r.CheckNumber = _checkNumber_R;

                        data.MotorcycleDriverCode = _SendAddressParsingInfo.MotorcycleDriverCode;
                        data.SalesDriverCode = _SendAddressParsingInfo.SalesDriverCode;
                        data.StackCode = _SendAddressParsingInfo.StackCode;
                        data.ShuttleStationCode = _SendAddressParsingInfo.ShuttleStationCode;
                        data_r.AreaCode = _SendAddressParsingInfo.StationCode;
                        data_r.SupplierCode = _ReceiveAddressParsingInfo.StationCode;
                        data_r.DeliveryType = DeliveryInfo_R.DeliveryType;
                        data.SendSalesDriverCode = _SendAddressParsingInfo.SendSalesDriverCode;
                        data.SendMotorcycleDriverCode = _SendAddressParsingInfo.SendMotorcycleDriverCode;
                        list.Add(data_r);
                    }

                    _transactionScope.Complete();

                }

            }
            catch (Exception e)
            {
                throw e;
            }

            return list;
        }
        /// <summary>
        /// 新增託運單
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<List<InsertDelivery_Res>> InsertDelivery(InsertDelivery_Req req)
        {


            List<InsertDelivery_Res> list = new List<InsertDelivery_Res>();

            this._insertDelivery_Req = req;
            //確認帳號
            await CheckCustomerCode();

            //確認產品無誤
            DateTime dateTime = DateTime.Now;

            ProductValuationManage_Condition productValuationManage_Condition = new ProductValuationManage_Condition();

            productValuationManage_Condition.CustomerCode = this.CustomerCode;
            productValuationManage_Condition.StartDate = dateTime;
            productValuationManage_Condition.EndDate = dateTime;
            var ProductList = await _ProductValuationManage_DA.SearchInfo(productValuationManage_Condition);

            //看客代 
            bool isNewCustomer = _CustomerInfo.is_new_customer.HasValue ? _CustomerInfo.is_new_customer.Value : false;
            //新客代 沒有代參數
            if (isNewCustomer && req.ProductId != null && req.Spec != null)
            {
                //判斷正逆物流
                var proD = ProductList.Where(x => x.ProductId.Equals(req.ProductId) && x.Spec.Equals(req.Spec) && x.DeliveryType.Equals("D")).FirstOrDefault();
                var proR = ProductList.Where(x => x.ProductId.Equals(req.ProductId) && x.Spec.Equals(req.Spec) && x.DeliveryType.Equals("R")).FirstOrDefault();
                switch (req.DeliveryMethod)
                {
                    case 1:
                        if (proD == null)
                        {
                            throw new MyException(ResponseCodeEnum.ParameterError);
                        }
                        break;
                    case 2:
                        if (proR == null)
                        {
                            throw new MyException(ResponseCodeEnum.ParameterError);
                        }
                        break;
                    default:

                        if (proD == null || proR == null)
                        {
                            throw new MyException(ResponseCodeEnum.ParameterError);
                        }
                        break;
                }

                if (req.ProductId=="CS000029")
                {
                    req.ProductId = "CM000030";
                }

                DeliveryInfo_D.ProductId = req.ProductId;
                DeliveryInfo_D.SpecCodeId = req.Spec;
            }
            else if (!isNewCustomer)
            {
                //預設產品項目

                //預購代
                if (_CustomerInfo.product_type == 2)
                {
                    //B003 3號
                    DeliveryInfo_D.ProductId = "PS000031";
                    DeliveryInfo_D.SpecCodeId = "B003";

                }//預購箱
                else if (_CustomerInfo.product_type == 3)
                {
                    //X003 3號
                    DeliveryInfo_D.ProductId = "PS000034";
                    DeliveryInfo_D.SpecCodeId = "X003";
                }
                else
                {
                    DeliveryInfo_D.ProductId = "CS000035";

                    switch (req.CbmSize.Value)
                    {
                        case 1:
                            DeliveryInfo_D.SpecCodeId = "S060";
                            break;
                        case 2:
                            DeliveryInfo_D.SpecCodeId = "S090";
                            break;
                        case 3:
                            DeliveryInfo_D.SpecCodeId = "S120";
                            break;
                        case 4:
                            DeliveryInfo_D.SpecCodeId = "S150";
                            break;
                        case 6:
                            DeliveryInfo_D.SpecCodeId = "S110";
                            break;
                        default:
                            DeliveryInfo_D.SpecCodeId = "N001";
                            break;
                    }
                }
            }
            else
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            //檢查CBM參數是否都對

            var ProductSpec = await _ItemCodes_DA.GetInfo("ProductSpec");


            if (req.MeasurementInfos != null && req.MeasurementInfos.Any())
            {
                foreach (var item in req.MeasurementInfos)
                {
                    if (string.IsNullOrEmpty(item.MeasurementCode))
                    {
                        throw new MyException(ResponseCodeEnum.ParameterError);

                    }
                    else
                    {
                        var _ProductSpec = ProductSpec.Where(x => x.CodeId.Equals(item.MeasurementCode)).FirstOrDefault();

                        if (_ProductSpec == null)
                        {
                            throw new MyException(ResponseCodeEnum.ParameterError);

                        }
                    }
                }
            }

            //代收貨款是否超過2萬
            if (req.CollectionMoney > 20000)
            {
                throw new MyException(ResponseCodeEnum.DeliveryCollectionMoneyOver);
            }


            //地址解析
            _SendAddressParsingInfo = await ParsingSendaddress();

            //寄件人轉聯運 不收

            if (_SendAddressParsingInfo.StationCode.LastIndexOf("9") > -1 || _SendAddressParsingInfo.StationCode.Equals("95") || _SendAddressParsingInfo.StationCode.Equals("92"))
            {
                throw new MyException(ResponseCodeEnum.DeliveryReceiveTooFar);
            }

            List<Task> ss = new List<Task>();

            SpecialAreaParsing S1 = new SpecialAreaParsing();
            SpecialAreaParsing S2 = new SpecialAreaParsing();
            var t1 = Task.Run(async () =>
            {
                try
                {
                    //特服
                    S1 = await ParsingSpecialArea(this._insertDelivery_Req.ReceiveAddress);
                }
                catch (Exception e)
                {


                }
            });
            ss.Add(t1);
            var t2 = Task.Run(async () =>
            {
                try
                {
                    _ReceiveAddressParsingInfo = await ParsingReceiveaddress();
                    if (_ReceiveAddressParsingInfo != null)
                    {
                        var c = _ReceiveAddressParsingInfo.City ?? string.Empty;
                        var t = _ReceiveAddressParsingInfo.Town ?? string.Empty;
                        var r = _ReceiveAddressParsingInfo.Road ?? string.Empty;
                        S2 = await ParsingSpecialArea(c + t + r);
                    }

                }
                catch (Exception e)
                {

                }

            });
            ss.Add(t2);

            Task.WaitAll(ss.ToArray());


            //如果兩個特服都有
            if (S1 != null && S2 != null)
            {
                if (S1.Fee >= S2.Fee)
                {
                    _SpecialArea = S1;
                }
                else
                {
                    _SpecialArea = S2;
                }

            }
            else if (S1 != null)
            {
                _SpecialArea = S1;
            }
            else if (S2 != null)
            {
                _SpecialArea = S2;
            }
            else
            {
                _SpecialArea.id = -1;
                _SpecialArea.Fee = 0;
            }
            if (_SpecialArea != null && _SpecialArea.id > 0)
            {

                _ReceiveAddressParsingInfo.City = _SpecialArea.City;
                _ReceiveAddressParsingInfo.Town = _SpecialArea.Town;
                _ReceiveAddressParsingInfo.Address = _SpecialArea.Road;
                _ReceiveAddressParsingInfo.StationCode = _SpecialArea.StationCode;
                _ReceiveAddressParsingInfo.ShuttleStationCode = _SpecialArea.ShuttleStationCode;
                _ReceiveAddressParsingInfo.SalesDriverCode = _SpecialArea.SalesDriverCode;
                _ReceiveAddressParsingInfo.MotorcycleDriverCode = _SpecialArea.MotorcycleDriverCode;
                _ReceiveAddressParsingInfo.StackCode = _SpecialArea.StackCode;
            }
            if (_ReceiveAddressParsingInfo == null) 
            {
                throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
            }
            if (string.IsNullOrEmpty(_ReceiveAddressParsingInfo.StationCode))
            {
                throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
            }
            try
            {
                using (var _transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {

                    string CBM = string.Empty;

                    if (req.CbmSize.HasValue)
                    {

                        switch (req.CbmSize.Value)
                        {
                            case 1:
                                CBM = "S060";
                                break;
                            case 2:
                                CBM = "S090";
                                break;
                            case 3:
                                CBM = "S120";
                                break;
                            case 4:
                                CBM = "S150";
                                break;
                            case 6:
                                CBM = "S110";
                                break;
                            default:
                                CBM = "S090";
                                break;
                        }
                    }
                    List<CBMDetailLog_Condition> cBMDetailLog_Conditions = new List<CBMDetailLog_Condition>();


                    switch (req.DeliveryMethod)
                    {
                        case 1:
                            DeliveryInfo_D.DeliveryType = "D";
                            DeliveryInfo_D.round_trip = false;

                            await CreateD();
                            _checkNumber_D = await CreateCheckNumber(_id_D, (DeliveryMethod)req.DeliveryMethod);
                            DeliveryInfo_D.request_id = _id_D;
                            DeliveryInfo_D.check_number = _checkNumber_D;
                            await _tcDeliveryRequests.Update(DeliveryInfo_D);
                            if (req.MeasurementInfos == null)
                            {
                                if (!string.IsNullOrEmpty(CBM))
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = CBM;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                                else
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = "S090";
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            else if (req.MeasurementInfos.Any())
                            {
                                foreach (var cbm in req.MeasurementInfos)
                                {

                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = cbm.MeasurementCode;
                                        cBMDetailLog_Condition.Width = cbm.Width;
                                        cBMDetailLog_Condition.Height = cbm.Height;
                                        cBMDetailLog_Condition.Length = cbm.Length;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            break;
                        case 2:
                            DeliveryInfo_D.DeliveryType = "R";
                            DeliveryInfo_D.round_trip = false;
                            await CreateD();
                            _checkNumber_D = await CreateCheckNumber(_id_D, (DeliveryMethod)req.DeliveryMethod);
                            DeliveryInfo_D.request_id = _id_D;
                            DeliveryInfo_D.check_number = _checkNumber_D;
                            await _tcDeliveryRequests.Update(DeliveryInfo_D);
                            if (req.MeasurementInfos == null)
                            {
                                if (!string.IsNullOrEmpty(CBM))
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = CBM;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                                else
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = "S090";
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            else if (req.MeasurementInfos.Any())
                            {
                                foreach (var cbm in req.MeasurementInfos)
                                {

                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = cbm.MeasurementCode;
                                        cBMDetailLog_Condition.Width = cbm.Width;
                                        cBMDetailLog_Condition.Height = cbm.Height;
                                        cBMDetailLog_Condition.Length = cbm.Length;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            break;
                        case 3://來回
                            //正
                            DeliveryInfo_D.DeliveryType = "D";
                            DeliveryInfo_D.round_trip = true;
                            await CreateD();
                            _checkNumber_D = await CreateCheckNumber(_id_D, DeliveryMethod.Forward);
                            DeliveryInfo_D.request_id = _id_D;
                            DeliveryInfo_D.check_number = _checkNumber_D;
                            await _tcDeliveryRequests.Update(DeliveryInfo_D);
                            if (req.MeasurementInfos == null)
                            {
                                if (!string.IsNullOrEmpty(CBM))
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = CBM;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                                else
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = "S090";
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            else if (req.MeasurementInfos.Any())
                            {
                                foreach (var cbm in req.MeasurementInfos)
                                {

                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_D;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = cbm.MeasurementCode;
                                        cBMDetailLog_Condition.Width = cbm.Width;
                                        cBMDetailLog_Condition.Height = cbm.Height;
                                        cBMDetailLog_Condition.Length = cbm.Length;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            //逆
                            DeliveryInfo_R.DeliveryType = "R";
                            DeliveryInfo_R.round_trip = true;
                            await CreateR();
                            _checkNumber_R = await CreateCheckNumber(_id_R, DeliveryMethod.Returned);
                            DeliveryInfo_R.request_id = _id_R;
                            DeliveryInfo_R.check_number = _checkNumber_R;
                            DeliveryInfo_R.roundtrip_checknumber = _checkNumber_D;
                            //更新關聯
                            await _tcDeliveryRequests.Update(DeliveryInfo_R);
                            DeliveryInfo_D.roundtrip_checknumber = _checkNumber_R;
                            await _tcDeliveryRequests.Update(DeliveryInfo_D);
                            if (req.MeasurementInfos == null)
                            {
                                if (!string.IsNullOrEmpty(CBM))
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_R;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = CBM;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                                else
                                {
                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_R;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = "S090";
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            else if (req.MeasurementInfos.Any())
                            {
                                foreach (var cbm in req.MeasurementInfos)
                                {

                                    //預設材積 要有託運單號才能關聯
                                    try
                                    {
                                        CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                        cBMDetailLog_Condition.CheckNumber = _checkNumber_R;
                                        cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                                        cBMDetailLog_Condition.CBM = cbm.MeasurementCode;
                                        cBMDetailLog_Condition.Width = cbm.Width;
                                        cBMDetailLog_Condition.Height = cbm.Height;
                                        cBMDetailLog_Condition.Length = cbm.Length;
                                        cBMDetailLog_Condition.CreateUser = CustomerCode;
                                        await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }


                    //SDMD-D

                    InsertDelivery_Res data = new InsertDelivery_Res();
                    data.CheckNumber = _checkNumber_D;

                    data.MotorcycleDriverCode = _ReceiveAddressParsingInfo.MotorcycleDriverCode;
                    data.SalesDriverCode = _ReceiveAddressParsingInfo.SalesDriverCode;
                    data.StackCode = _ReceiveAddressParsingInfo.StackCode;
                    data.ShuttleStationCode = _ReceiveAddressParsingInfo.ShuttleStationCode;
                    data.AreaCode = _ReceiveAddressParsingInfo.StationCode;
                    data.SupplierCode = _SendAddressParsingInfo.StationCode;
                    data.DeliveryType = DeliveryInfo_D.DeliveryType;
                    data.SendMotorcycleDriverCode = _ReceiveAddressParsingInfo.SendMotorcycleDriverCode;
                    data.SendSalesDriverCode = _ReceiveAddressParsingInfo.SendSalesDriverCode;
                    list.Add(data);


                    //紀錄集貨站所
                    pickup_request_for_apiuser_Condition pickup_Request_For_Apiuser_D = new pickup_request_for_apiuser_Condition();
                    pickup_Request_For_Apiuser_D.check_number = _checkNumber_D;
                    pickup_Request_For_Apiuser_D.customer_code = CustomerCode;
                    pickup_Request_For_Apiuser_D.pieces = 1;
                    pickup_Request_For_Apiuser_D.request_date = DateTime.Now;
                    pickup_Request_For_Apiuser_D.send_city = _SendAddressParsingInfo.City;
                    pickup_Request_For_Apiuser_D.send_area = _SendAddressParsingInfo.Town;
                    pickup_Request_For_Apiuser_D.send_road = req.SendAddress;
                    pickup_Request_For_Apiuser_D.supplier_code = "F" + _SendAddressParsingInfo.StationCode;//配合就系統+F
                    pickup_Request_For_Apiuser_D.ShuttleStationCode = _SendAddressParsingInfo.ShuttleStationCode;
                    pickup_Request_For_Apiuser_D.send_tel = req.SendTel;
                    pickup_Request_For_Apiuser_D.sd = _SendAddressParsingInfo.SalesDriverCode;
                    pickup_Request_For_Apiuser_D.md = _SendAddressParsingInfo.MotorcycleDriverCode;
                    pickup_Request_For_Apiuser_D.putorder = _SendAddressParsingInfo.StackCode;
                    await _pickup_request_for_apiuser_DA.Insert(pickup_Request_For_Apiuser_D);

                    await _tcDeliveryRequests.InsertAPIorigin(CustomerCode, "InsertDelivery");
                    if (req.DeliveryMethod == 3)
                    {
                        //SDMD-R

                        InsertDelivery_Res data_r = new InsertDelivery_Res();
                        data_r.CheckNumber = _checkNumber_R;

                        data_r.MotorcycleDriverCode = _SendAddressParsingInfo.MotorcycleDriverCode;
                        data_r.SalesDriverCode = _SendAddressParsingInfo.SalesDriverCode;
                        data_r.StackCode = _SendAddressParsingInfo.StackCode;
                        data_r.ShuttleStationCode = _SendAddressParsingInfo.ShuttleStationCode;
                        data_r.AreaCode = _SendAddressParsingInfo.StationCode;
                        data_r.SupplierCode = _ReceiveAddressParsingInfo.StationCode;
                        data_r.DeliveryType = DeliveryInfo_R.DeliveryType;
                        data_r.SendMotorcycleDriverCode = _SendAddressParsingInfo.SendMotorcycleDriverCode;
                        data_r.SendSalesDriverCode = _SendAddressParsingInfo.SendSalesDriverCode;
                        list.Add(data_r);

                        //紀錄集貨站所
                        pickup_request_for_apiuser_Condition pickup_Request_For_Apiuser_R = new pickup_request_for_apiuser_Condition();
                        pickup_Request_For_Apiuser_R.check_number = _checkNumber_R;
                        pickup_Request_For_Apiuser_R.customer_code = CustomerCode;
                        pickup_Request_For_Apiuser_R.pieces = 1;
                        pickup_Request_For_Apiuser_R.request_date = DateTime.Now;
                        pickup_Request_For_Apiuser_R.send_city = _ReceiveAddressParsingInfo.City;
                        pickup_Request_For_Apiuser_R.send_area = _ReceiveAddressParsingInfo.Town;
                        pickup_Request_For_Apiuser_R.send_road = req.ReceiveAddress;
                        pickup_Request_For_Apiuser_R.supplier_code = "F" + _ReceiveAddressParsingInfo.StationCode;//配合就系統+F
                        pickup_Request_For_Apiuser_R.ShuttleStationCode = _ReceiveAddressParsingInfo.ShuttleStationCode;
                        pickup_Request_For_Apiuser_R.send_tel = req.ReceiveTel1;
                        pickup_Request_For_Apiuser_R.sd = _ReceiveAddressParsingInfo.SalesDriverCode;
                        pickup_Request_For_Apiuser_R.md = _ReceiveAddressParsingInfo.MotorcycleDriverCode;
                        pickup_Request_For_Apiuser_R.putorder = _ReceiveAddressParsingInfo.StackCode;
                        await _pickup_request_for_apiuser_DA.Insert(pickup_Request_For_Apiuser_R);
                    }
                    _transactionScope.Complete();

                }

            }
            catch (Exception e)
            {
                throw e;
            }

            return list;
        }
        /// <summary>
        /// 產生託運單號
        /// </summary>
        /// <param name="CustomerInfo"></param>
        /// <param name="request_id"></param>
        /// <param name="deliveryMethod"></param>
        /// <returns></returns>
        private async Task<string> CreateCheckNumber(decimal request_id, DeliveryMethod deliveryMethod)
        {
            tbCustomers_Condition CustomerInfo = _CustomerInfo;
            //如果沒使用區間，主動產生check_number

            long checkNumberTemp = 100000000 + Convert.ToInt64(request_id);
            int tail = Convert.ToInt32(request_id % 7);

            string checkNumberPrefix = "";
            //各種寫死開頭

            // 蝦皮 F1300600002
            if (CustomerInfo.customer_code.Equals("F1300600002"))
            {
                checkNumberPrefix = "700";
            }
            //momo F3500010002
            else if (CustomerInfo.customer_code.Equals("F3500010002"))
            {
                switch (deliveryMethod)
                {
                    //201 momo正物流
                    case DeliveryMethod.Forward:
                        checkNumberPrefix = "201";
                        break;
                    case DeliveryMethod.Returned:
                        checkNumberPrefix = "202";
                        break;

                    default:
                        break;
                }
            }
            else if (deliveryMethod == DeliveryMethod.Returned)
            {
                checkNumberPrefix = "990";
            }
            else
            {
                checkNumberPrefix = (await _check_number_prehead_DA.GetInfoByProductType(CustomerInfo.product_type.ToString())).check_number_prehead;
            }


            //一般規則
            //103 一般件
            //880 預購袋
            //500 超值箱
            //100 內部文件
            //600 商城


            return string.Format("{0}{1}{2}", checkNumberPrefix, checkNumberTemp.ToString().Substring(1), tail.ToString());
        }
        /// <summary>
        /// 取得標籤(Base64)
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<string> GetTagImage(GetTagImage_Req req)
        {

            string checkNumber = req.CheckNumber;
            await CheckcheckNumber(checkNumber);
            await CheckCustomerCode();
            //此單不等於此客代
            if (!_tcDeliveryRequestsInfo.customer_code.Equals(_CustomerInfo.customer_code))
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            }
            //已消單
            if (_tcDeliveryRequestsInfo.cancel_date != null)
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            }
            //已配達
            //所有掃描貨態
            var scans = await _ttDeliveryScanLog_DA.GetInfoByCheckNumber(checkNumber);
            //正常配交無法列印
            if (scans != null && scans.Count() > 0)
            {
                scans = scans.Where(x => !string.IsNullOrEmpty(x.scan_item) && !string.IsNullOrEmpty(x.arrive_option));

                if (scans != null && scans.Count() > 0)
                {
                    scans = scans.Where(x => x.scan_item.Equals("3") && x.arrive_option.Equals("3"));

                    if (scans != null && scans.Count() > 0)
                    {
                        throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
                    }
                }
            }


            string base64 = string.Empty;

            try
            {
                NameValueCollection reportParams = new NameValueCollection
                {
                    ["request_ids"] = _tcDeliveryRequestsInfo.request_id.ToString()
                };
                ReportExportType reportExportType = ReportExportType.IMAGE;

                var pdfStream = _reportPrintService.GetNomalReport(reportParams, "LTReelLabelByIds_2_0", reportExportType);

                byte[] bytes;
                using (var memoryStream = new MemoryStream())
                {
                    pdfStream.CopyTo(memoryStream);
                    bytes = memoryStream.ToArray();
                }

                base64 = Convert.ToBase64String(bytes);
            }
            catch (Exception ex)
            {
                throw new MyException(ResponseCodeEnum.ReportError);
            }


            return base64;
        }

        /// <summary>
        /// 退貨(逆物流)
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<ReverseDelivery_Res> ReverseDelivery(ReverseDelivery_Req req)
        {
            ReverseDelivery_Res data = new ReverseDelivery_Res();

            try
            {
                string checkNumber = req.CheckNumber;
                await CheckcheckNumber(checkNumber);

                DeliveryInfo_D = _tcDeliveryRequestsInfo;

                //檢查寄件收件有誤
                //寄件電話
                if (string.IsNullOrEmpty(_tcDeliveryRequestsInfo.receive_tel1))
                {
                    if (string.IsNullOrEmpty(DeliveryInfo_D.receive_tel2))
                    {
                        throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
                    }
                    else
                    {
                        DeliveryInfo_D.receive_tel1 = DeliveryInfo_D.receive_tel2;
                    }
                }

                //確認帳號
                await CheckCustomerCode();
                //此單不等於此客代
                if (!DeliveryInfo_D.customer_code.Equals(CustomerCode))
                {
                    throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
                }
                //確認是否為正物流
                if (!DeliveryInfo_D.DeliveryType.Equals("D"))
                {
                    throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
                }


                using (var _transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    await CreateR();

                    _checkNumber_R = await CreateCheckNumber(_id_R, DeliveryMethod.Returned);
                    DeliveryInfo_R.request_id = _id_R;
                    DeliveryInfo_R.check_number = _checkNumber_R;
                    DeliveryInfo_R.return_check_number = DeliveryInfo_D.check_number;
                    await _tcDeliveryRequests.Update(DeliveryInfo_R);
                    DeliveryInfo_D.return_check_number = _checkNumber_R;
                    await _tcDeliveryRequests.Update(DeliveryInfo_D);


                    for (int i = 0; i < DeliveryInfo_R.pieces.Value; i++)
                    {
                        //預設材積 要有託運單號才能關聯
                        try
                        {
                            CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                            cBMDetailLog_Condition.CheckNumber = _checkNumber_R;
                            cBMDetailLog_Condition.ComeFrom = "1";//建立訂單代碼
                            cBMDetailLog_Condition.CBM = DeliveryInfo_R.SpecCodeId;
                            cBMDetailLog_Condition.CreateUser = CustomerCode;
                            await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);
                        }
                        catch (Exception e)
                        {

                        }
                    }

                    data.CheckNumber = _checkNumber_R;

                    data.MotorcycleDriverCode = DeliveryInfo_R.ReceiveMD;
                    data.SalesDriverCode = DeliveryInfo_R.ReceiveSD;
                    data.StackCode = String.Empty;
                    data.AreaCode = DeliveryInfo_R.area_arrive_code;
                    data.SupplierCode = DeliveryInfo_R.send_station_scode;
                    data.DeliveryType = DeliveryInfo_R.DeliveryType;
                    data.SendMotorcycleDriverCode = DeliveryInfo_R.SendMD;
                    data.SendSalesDriverCode = DeliveryInfo_R.SendSD;

                    _transactionScope.Complete();

                }
                //拆兩個transactionScope否則會出錯
                //using (var _transactionScope2 = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                //{
                //    //SDMD
                //    //var sdmd = await CreateSDMD(id, checkNumber, tcDeliveryRequests_Condition.receive_city, tcDeliveryRequests_Condition.receive_area, tcDeliveryRequests_Condition.receive_address);
                //    //SDMD
                //    var sdmd = await CreateSDMD(_id_R, _checkNumber_R, DeliveryInfo_R.receive_city, DeliveryInfo_R.receive_area, DeliveryInfo_R.receive_address);


                //    data.CheckNumber = _checkNumber_R;

                //    data.MotorcycleDriverCode = sdmd.md;
                //    data.SalesDriverCode = sdmd.sd;
                //    data.StackCode = sdmd.put_order;
                //    data.AreaCode = DeliveryInfo_R.area_arrive_code;
                //    data.SupplierCode = DeliveryInfo_R.send_station_scode;
                //    data.DeliveryType = DeliveryInfo_R.DeliveryType;
                //    _transactionScope2.Complete();

                //}
            }
            catch (Exception)
            {

                throw;
            }

            return data;
        }
        /// <summary>
        /// 建SDMD
        /// </summary>
        /// <param name="request_id"></param>
        /// <param name="checknumber"></param>
        /// <param name="city"></param>
        /// <param name="area"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        private async Task<check_number_sd_mapping_Condition> CreateSDMD(decimal request_id, string checknumber, string city, string area, string address)
        {
            check_number_sd_mapping_Condition check_Number_Sd_Mapping_Condition = new check_number_sd_mapping_Condition();
            try
            {
                //SDMD
                FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(address, city, area);

                var eachDatas = await GetByFSEAddressEntity(fSEAddressEntity);

                if (eachDatas != null && eachDatas.Count() > 0)
                {

                    check_Number_Sd_Mapping_Condition.check_number = checknumber;
                    check_Number_Sd_Mapping_Condition.request_id = (long)request_id;
                    check_Number_Sd_Mapping_Condition.org_area_id = eachDatas[0].id;
                    check_Number_Sd_Mapping_Condition.md = eachDatas[0].md_no;
                    check_Number_Sd_Mapping_Condition.sd = eachDatas[0].sd_no;
                    check_Number_Sd_Mapping_Condition.put_order = eachDatas[0].put_order;
                    check_Number_Sd_Mapping_Condition.cdate = DateTime.Now;
                    check_Number_Sd_Mapping_Condition.udate = DateTime.Now;
                    await _check_number_sd_mapping_DA.Insert(check_Number_Sd_Mapping_Condition);
                }
                else
                {


                }
                return check_Number_Sd_Mapping_Condition;
            }
            catch (Exception)
            {

                return null;
            }


        }

        /// <summary>
        /// 貨態
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<List<GetDeliveryTracking_Res>> GetDeliveryTracking(GetDeliveryTracking_Req req)
        {
            List<GetDeliveryTracking_Res> data = new List<GetDeliveryTracking_Res>();
            //檢查貨號 checknumber 
            string checkNumber = req.CheckNumber;
            await CheckcheckNumber(checkNumber);
            //確認帳號
            var CustomerInfo = await _tbCustomers_DA.GetInfoByCustomerCode(CustomerCode);
            if (CustomerInfo == null)
            {
                throw new MyException(ResponseCodeEnum.CustomerNotFound);

            }
            ////此單不等於此客代
            //if (!_tcDeliveryRequestsInfo.customer_code.Equals(CustomerInfo.customer_code))
            //{
            //    throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            //}
            //所有掃描貨態
            var scans = await _ttDeliveryScanLog_DA.GetInfoByCheckNumber(checkNumber);

            foreach (var _scan in scans)
            {
                GetDeliveryTracking_Res _getDeliveryTracking_Res = new GetDeliveryTracking_Res();
                _getDeliveryTracking_Res.State = _scan.scan_item;
                _getDeliveryTracking_Res.StateDetail = string.Empty;
                _getDeliveryTracking_Res.StationName = _scan.driver_station_name;
                _getDeliveryTracking_Res.HandleTime = _scan.scan_date.Value.ToString("yyyy-MM-dd HH:mm:ss");
                _getDeliveryTracking_Res.DeliveryType = _scan.DeliveryType;

                if (_getDeliveryTracking_Res.ReturnCheckNumber is not null && _getDeliveryTracking_Res.DeliveryType.Equals("R"))
                {
                    _getDeliveryTracking_Res.ReturnCheckNumber = _scan.ReturnCheckNumber;
                }


                switch (Tool.StrTrimer(_scan.scan_item))
                {
                    case "1":
                        break;
                    case "2":
                        break;
                    case "3":
                        _getDeliveryTracking_Res.StateDetail = _scan.arrive_option;
                        break;
                    case "5":
                        _getDeliveryTracking_Res.StateDetail = _scan.receive_option;
                        break;
                    case "6":
                        break;
                    case "7":
                        break;
                    default:
                        break;
                }

                data.Add(_getDeliveryTracking_Res);
            }

            return data.OrderBy(x => x.HandleTime).ToList();
        }
        /// <summary>
        /// 取消託運單
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task CancelDelivery(CancelDelivery_Req req)
        {
            string check_number = req.CheckNumber;

            //檢查貨號 checknumber 

            var result = (await _tcDeliveryRequests.GetInfoByCheckNumber(check_number)).FirstOrDefault();

            if (result == null)
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            }
            //檢查是否已經銷單
            var result1 = (await _tcDeliveryRequests.GetInfoByCheckNumberWithoutCancel(check_number)).FirstOrDefault();

            if (result1 == null)
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsCanceled);
            }

            //檢查此貨號是否屬於此客代

            var customer_code = CustomerCode;

            var result2 = (await _tcDeliveryRequests.GetInfoByCheckNumberAndCustomerCode(check_number, customer_code)).FirstOrDefault();

            if (result2 == null)
            {
                throw new MyException(ResponseCodeEnum.CheckNumberNotFoundInCustomerCode);
            }

            //檢查此貨號有沒有正常集貨

            var result3 = (await _tcDeliveryRequests.GetDeliveryInfoByCheckNumber(check_number)).FirstOrDefault();

            if (result3 != null)
            {
                throw new MyException(ResponseCodeEnum.HavingDeliveryItem);
            }
            //將此貨號進行銷單，cancel_date設為現在

            await _tcDeliveryRequests.CancelByCheckNumber(check_number);

            //寫入log
            string record_action = result.DeliveryType;
            string request_id = result.request_id.ToString();
            await _ttDeliveryRequestsRecord_DA.CancelLog(check_number, customer_code, request_id, record_action);

        }
        /// <summary>c
        /// 來回件
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<RoundShipment_Res> RoundShipment(RoundShipment_Req req)
        {
            RoundShipment_Res data = new RoundShipment_Res();
            string checkNumber = req.CheckNumber;
            await CheckcheckNumber(checkNumber);

            //確認帳號
            await CheckCustomerCode();
            //此單不等於此客代
            if (!DeliveryInfo_D.customer_code.Equals(CustomerCode))
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            }
            //確認是否為正物流
            if (!DeliveryInfo_D.DeliveryType.Equals("D"))
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            }



            return data;
        }


        /// <summary>
        /// 查詢託運單資訊
        /// </summary>
        /// <param name="check_number"></param>
        /// <returns></returns>
        public async Task<GetDeliveryInfo_Res> GetDeliveryInfoBycheck_number(string check_number)
        {
            if (string.IsNullOrEmpty(check_number))
            {
                throw new MyException(ResponseCodeEnum.Error);
            }

            GetDeliveryInfo_Res getDeliveryInfo_Res = new GetDeliveryInfo_Res();

            await CheckcheckNumber(check_number);
            string receive_tel = string.Empty;

            if (string.IsNullOrEmpty(_tcDeliveryRequestsInfo.receive_tel1))
            {
                if (!string.IsNullOrEmpty(_tcDeliveryRequestsInfo.receive_tel2))
                {
                    receive_tel = _tcDeliveryRequestsInfo.receive_tel2;
                }
            }
            else
            {

                if (string.IsNullOrEmpty(_tcDeliveryRequestsInfo.receive_tel1_ext))
                {
                    receive_tel = _tcDeliveryRequestsInfo.receive_tel1;
                }
                else
                {
                    receive_tel = _tcDeliveryRequestsInfo.receive_tel1 + "#" + _tcDeliveryRequestsInfo.receive_tel1_ext.Replace("#", "");
                }
            }

            //Tool.StrTrimer();
            getDeliveryInfo_Res.orderNumber = Tool.StrTrimer(_tcDeliveryRequestsInfo.order_number);
            getDeliveryInfo_Res.Ship_date = _tcDeliveryRequestsInfo.ship_date;
            getDeliveryInfo_Res.Send_contact = Tool.StrTrimer(_tcDeliveryRequestsInfo.send_contact);
            getDeliveryInfo_Res.Send_tell = Tool.StrTrimer(_tcDeliveryRequestsInfo.send_tel);
            getDeliveryInfo_Res.Send_Address = Tool.StrTrimer(_tcDeliveryRequestsInfo.send_city + _tcDeliveryRequestsInfo.send_area + _tcDeliveryRequestsInfo.send_address);
            getDeliveryInfo_Res.Area_arrive_code = Tool.StrTrimer(_tcDeliveryRequestsInfo.area_arrive_code);
            getDeliveryInfo_Res.Receive_contact = Tool.StrTrimer(_tcDeliveryRequestsInfo.receive_contact);
            getDeliveryInfo_Res.Receive_tell = Tool.StrTrimer(receive_tel);
            getDeliveryInfo_Res.receive_address = Tool.StrTrimer(_tcDeliveryRequestsInfo.receive_city + _tcDeliveryRequestsInfo.receive_area + _tcDeliveryRequestsInfo.receive_address);
            getDeliveryInfo_Res.Time_period = Tool.StrTrimer(_tcDeliveryRequestsInfo.time_period);
            getDeliveryInfo_Res.pieces = Tool.StrTrimer(_tcDeliveryRequestsInfo.pieces.ToString());
            getDeliveryInfo_Res.Collection_money = Tool.StrTrimer(_tcDeliveryRequestsInfo.collection_money.ToString());
            getDeliveryInfo_Res.Invoice_desc = Tool.StrTrimer(_tcDeliveryRequestsInfo.invoice_desc);
            getDeliveryInfo_Res.articlename = Tool.StrTrimer(_tcDeliveryRequestsInfo.ArticleName);

            return getDeliveryInfo_Res;
        }
        /// <summary>
        /// 加入貨態
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task InsertShipInfo(InsertShipInfo_Req req)
        {
            try
            {
                //寫入紀錄
                DeliveryStatusPublic_Condition _deliveryStatusPublic_Condition = new DeliveryStatusPublic_Condition();
                _deliveryStatusPublic_Condition.DataSource = this.CustomerCode;
                _deliveryStatusPublic_Condition.CheckNumber = req.checkNumber;
                _deliveryStatusPublic_Condition.DeliveryStatus = req.status;
                _deliveryStatusPublic_Condition.ScanDate = req.Scan_date.Value;
                _deliveryStatusPublic_Condition.CompanyName = req.CompanyName;
                _deliveryStatusPublic_Condition.TaxIDNumber = req.Company_no;
                _deliveryStatusPublic_Condition.DeliveryStatusCode = req.Status_delivery;
                _deliveryStatusPublic_Condition.WorkerName = req.Frontline_worker;
                _deliveryStatusPublic_Condition.StationName = req.Station_name;
                _deliveryStatusPublic_Condition.StationCode = req.StationCode;
                await _DeliveryStatusPublic.Insert(_deliveryStatusPublic_Condition);

            }
            catch (MyException e)
            {

                throw e;
            }

        }

        /// <summary>
        /// 取得產品
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<List<GetMeasurementInfo_Res>> GetMeasurementInfo()
        {


            List<GetMeasurementInfo_Res> getMeasurementInfo_Res = new List<GetMeasurementInfo_Res>();
            var ProductSpec = await _ItemCodes_DA.GetInfo("ProductSpec");

            foreach (var item in ProductSpec)
            {
                GetMeasurementInfo_Res res = new GetMeasurementInfo_Res();
                res.MeasurementCode = item.CodeId;

                res.Memo = item.Memo;
                getMeasurementInfo_Res.Add(res);
            }



            return getMeasurementInfo_Res;
        }

        /// <summary>
        /// 取得產品
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<List<GetProduct_Res>> GetProduct()
        {
            List<GetProduct_Res> getProduct_Res = new List<GetProduct_Res>();
            var ProductSpec = await _ItemCodes_DA.GetInfo("ProductSpec");

            try
            {
                var productList = await _ProductManage_DA.GetpublicAll();

                DateTime dateTime = DateTime.Now;

                ProductValuationManage_Condition productValuationManage_Condition = new ProductValuationManage_Condition();

                productValuationManage_Condition.CustomerCode = this.CustomerCode;
                productValuationManage_Condition.StartDate = dateTime;
                productValuationManage_Condition.EndDate = dateTime;
                var ProductList = await _ProductValuationManage_DA.SearchInfo(productValuationManage_Condition);
                foreach (var Product in ProductList)
                {
                    try
                    {
                        var info = getProduct_Res.Where(x => x.ProductId.Equals(Product.ProductId) && x.DeliveryType.Equals(Product.DeliveryType)).FirstOrDefault();

                        if (info == null)
                        {
                            GetProduct_Res res = new GetProduct_Res();
                            res.SpecList = new List<SpecInfo>();
                            res.DeliveryType = Product.DeliveryType;
                            res.ProductId = Product.ProductId;
                            res.ProductName = productList.Where(x => x.ProductId.Equals(res.ProductId)).FirstOrDefault().Name;
                            SpecInfo specInfo = new SpecInfo();
                            specInfo.SpecId = Product.Spec;
                            specInfo.SpecName = ProductSpec.Where(x => x.CodeId.Equals(specInfo.SpecId)).FirstOrDefault().CodeName;
                            res.SpecList.Add(specInfo);
                            getProduct_Res.Add(res);
                        }
                        else
                        {
                            SpecInfo specInfo = new SpecInfo();
                            specInfo.SpecId = Product.Spec;
                            specInfo.SpecName = ProductSpec.Where(x => x.CodeId.Equals(specInfo.SpecId)).FirstOrDefault().CodeName;
                            info.SpecList.Add(specInfo);
                        }
                    }
                    catch (Exception)
                    {


                    }
                }
            }
            catch (MyException e)
            {

                throw e;
            }
            return getProduct_Res;
        }



        /// <summary>
        /// 排成轉道主要Sanlog
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<FilterDeliveryStatusPublicJob_Res> FilterDeliveryStatusPublic()
        {
            FilterDeliveryStatusPublicJob_Res filterDeliveryStatusPublicJob = new FilterDeliveryStatusPublicJob_Res();

            int SuccessCount = 0;
            int ErrorCount = 0;

            try
            {
                //抓取
                var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.FilterDeliveryStatusPublicJob)).FirstOrDefault();

                if (info == null)
                {
                    throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
                }

                var GetInfoById = await _DeliveryStatusPublic.GetInfoById(info.StartId);

                if (GetInfoById != null && GetInfoById.Count() > 0)
                {
                    var lastId = GetInfoById.Max(p => p.id);

                    //所有貨態
                    var P3 = await _tbItemCodes_DA.GetInfosBycode_sclass("P3");

                    //所有配達貨態
                    var AO = await _tbItemCodes_DA.GetInfosBycode_sclass("AO");

                    foreach (var _info in GetInfoById)
                    {
                        DeliveryStatusPublicFilter_Condition deliveryStatusPublicFilter_Condition = _info;
                        DeliveryStatusPublicError_Condition deliveryStatusPublicError_Condition = _info;
                        try
                        {
                            //檢查有無託運單
                            var result = (await _tcDeliveryRequests.GetInfoByCheckNumber(_info.CheckNumber)).FirstOrDefault();

                            if (result == null)
                            {
                                //查無此貨態
                                deliveryStatusPublicError_Condition.ErrMsg = ResponseCodeEnum.DeliveryStatusPublicNotFoundCheckNumber.GetString();
                                deliveryStatusPublicError_Condition.Status = (int)ResponseCodeEnum.DeliveryStatusPublicNotFoundCheckNumber;
                                await _DeliveryStatusPublicError_DA.Insert(deliveryStatusPublicError_Condition);
                                ErrorCount++;
                                throw new MyException(ResponseCodeEnum.DeliveryStatusPublicNotFound);
                            }

                            //檢查貨態
                            var P3Info = P3.Where(x => x.code_id == _info.DeliveryStatus).FirstOrDefault();

                            if (P3Info == null)
                            {
                                //查無此貨態
                                deliveryStatusPublicError_Condition.ErrMsg = ResponseCodeEnum.DeliveryStatusPublicNotFound.GetString();
                                deliveryStatusPublicError_Condition.Status = (int)ResponseCodeEnum.DeliveryStatusPublicNotFound;
                                await _DeliveryStatusPublicError_DA.Insert(deliveryStatusPublicError_Condition);
                                ErrorCount++;
                                throw new MyException(ResponseCodeEnum.DeliveryStatusPublicNotFound);
                            }

                            if (P3Info.code_id.Equals("3")) //配達
                            {
                                var AOInfo = AO.Where(x => x.code_id == _info.DeliveryStatusCode).FirstOrDefault();

                                if (AOInfo == null)
                                {
                                    //查無此貨態
                                    deliveryStatusPublicError_Condition.ErrMsg = ResponseCodeEnum.DeliveryStatusPublicNotFound.GetString();
                                    deliveryStatusPublicError_Condition.Status = (int)ResponseCodeEnum.DeliveryStatusPublicNotFound;
                                    await _DeliveryStatusPublicError_DA.Insert(deliveryStatusPublicError_Condition);
                                    ErrorCount++;
                                    throw new MyException(ResponseCodeEnum.DeliveryStatusPublicNotFound);
                                }
                            }

                            //檢查單號重複
                            var DeliveryStatusPublicFilter = await _DeliveryStatusPublicFilter_DA.CheckInfoRepeat(deliveryStatusPublicFilter_Condition);
                            if (DeliveryStatusPublicFilter != null)
                            {
                                deliveryStatusPublicError_Condition.ErrMsg = ResponseCodeEnum.DeliveryStatusPublicStateRepeat.GetString();
                                deliveryStatusPublicError_Condition.Status = (int)ResponseCodeEnum.DeliveryStatusPublicStateRepeat;
                                await _DeliveryStatusPublicError_DA.Insert(deliveryStatusPublicError_Condition);
                                ErrorCount++;
                                throw new MyException(ResponseCodeEnum.DeliveryStatusPublicStateRepeat);
                            }
                            await _DeliveryStatusPublicFilter_DA.Insert(deliveryStatusPublicFilter_Condition);
                            SuccessCount++;
                        }
                        catch (MyException e)
                        {
                            if (!(e.ResponseCode == ResponseCodeEnum.DeliveryStatusPublicStateRepeat ||
                                e.ResponseCode == ResponseCodeEnum.DeliveryStatusPublicNotFound ||
                                  e.ResponseCode == ResponseCodeEnum.DeliveryStatusPublicNotFoundCheckNumber
                                ))
                            {
                                //未知錯誤
                                deliveryStatusPublicError_Condition.ErrMsg = e.Message;
                                deliveryStatusPublicError_Condition.Status = (int)ResponseCodeEnum.DeliveryStatusPublicError;
                                await _DeliveryStatusPublicError_DA.Insert(deliveryStatusPublicError_Condition);
                                ErrorCount++;
                            }

                        }

                    }
                    info.StartId = lastId;
                    await _ScheduleTask_DA.UpdateSetStartId(info);
                }

            }
            catch (Exception e)
            {
                throw;
            }
            return filterDeliveryStatusPublicJob;
        }

        /// <summary>
        /// 排成轉道主要Sanlog
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<UpdateDeliveryStatusPublicToDeliveryScanLogJob_Res> UpdateDeliveryStatusPublicToDeliveryScanLog()
        {
            UpdateDeliveryStatusPublicToDeliveryScanLogJob_Res updateDeliveryStatusPublicToDeliveryScanLogJob_Res = new UpdateDeliveryStatusPublicToDeliveryScanLogJob_Res();

            int SuccessCount = 0;
            int ErrorCount = 0;

            //抓取時間
            var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.UpdateDeliveryStatusPublicToDeliveryScanLogJob)).FirstOrDefault();

            if (info == null)
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            }

            //抓取資料
            var GetInfoById = await _DeliveryStatusPublicFilter_DA.GetInfoById(info.StartId);

            if (GetInfoById != null && GetInfoById.Count() > 0)
            {

                //所有貨態
                var P3 = await _tbItemCodes_DA.GetInfosBycode_sclass("P3");

                //所有配達貨態
                var AO = await _tbItemCodes_DA.GetInfosBycode_sclass("AO");


                var lastId = GetInfoById.Max(p => p.id);
                foreach (var _info in GetInfoById)
                {
                    DeliveryStatusPublicError_Condition deliveryStatusPublicError_Condition = _info;

                    string checknumber = _info.CheckNumber;
                    string scan_item = string.Empty;
                    string arrive_option = string.Empty;
                    string DataSource = _info.DataSource;
                    DateTime Scan_date = _info.ScanDate;


                    ttDeliveryScanLog_Condition _ttDeliveryScanLog_Condition = new ttDeliveryScanLog_Condition();
                    try
                    {
                        //檢查有無託運單
                        var result = (await _tcDeliveryRequests.GetInfoByCheckNumber(checknumber)).FirstOrDefault();

                        if (result == null)
                        {
                            //查無此貨號
                            deliveryStatusPublicError_Condition.ErrMsg = ResponseCodeEnum.DeliveryStatusPublicNotFoundCheckNumber.GetString();
                            deliveryStatusPublicError_Condition.Status = (int)ResponseCodeEnum.DeliveryStatusPublicNotFoundCheckNumber;
                            await _DeliveryStatusPublicError_DA.Insert(deliveryStatusPublicError_Condition);
                            ErrorCount++;
                            throw new MyException(ResponseCodeEnum.DeliveryStatusPublicNotFound);
                        }

                        //所有掃描貨態
                        var scans = await _ttDeliveryScanLog_DA.GetInfoByCheckNumber(_info.CheckNumber);

                        if (scans != null && scans.Count() > 0)
                        {
                            var P33AO3 = scans.Where(x => x.scan_item.Equals("3") && !string.IsNullOrEmpty(x.arrive_option) && x.arrive_option.Equals("3")).FirstOrDefault();

                            //已經正常配交
                            if (P33AO3 != null)
                            {
                                deliveryStatusPublicError_Condition.ErrMsg = ResponseCodeEnum.DeliveryStatusPublicHaveP33AO3.GetString();
                                deliveryStatusPublicError_Condition.Status = (int)ResponseCodeEnum.DeliveryStatusPublicHaveP33AO3;
                                await _DeliveryStatusPublicError_DA.Insert(deliveryStatusPublicError_Condition);
                                ErrorCount++;
                                throw new MyException(ResponseCodeEnum.DeliveryStatusPublicHaveP33AO3);
                            }
                        }


                        //檢查貨態
                        var P3Info = P3.Where(x => x.code_id == _info.DeliveryStatus).FirstOrDefault();

                        if (P3Info == null)
                        {
                            //查無此貨態
                            deliveryStatusPublicError_Condition.ErrMsg = ResponseCodeEnum.DeliveryStatusPublicNotFound.GetString();
                            deliveryStatusPublicError_Condition.Status = (int)ResponseCodeEnum.DeliveryStatusPublicNotFound;
                            await _DeliveryStatusPublicError_DA.Insert(deliveryStatusPublicError_Condition);
                            ErrorCount++;
                            throw new MyException(ResponseCodeEnum.DeliveryStatusPublicNotFound);
                        }
                        scan_item = P3Info.code_id;
                        if (P3Info.code_id.Equals("3")) //配達
                        {
                            var AOInfo = AO.Where(x => x.code_id == _info.DeliveryStatusCode).FirstOrDefault();

                            if (AOInfo == null)
                            {
                                //查無此貨態
                                deliveryStatusPublicError_Condition.ErrMsg = ResponseCodeEnum.DeliveryStatusPublicNotFound.GetString();
                                deliveryStatusPublicError_Condition.Status = (int)ResponseCodeEnum.DeliveryStatusPublicNotFound;
                                await _DeliveryStatusPublicError_DA.Insert(deliveryStatusPublicError_Condition);
                                ErrorCount++;
                                throw new MyException(ResponseCodeEnum.DeliveryStatusPublicNotFound);
                            }
                            arrive_option = AOInfo.code_id;
                        }

                        _ttDeliveryScanLog_Condition.check_number = checknumber;
                        //寫入DeliveryScanLog
                        var scanLogInsert = new ttDeliveryScanLog_Condition()
                        {
                            driver_code = DataSource,
                            check_number = checknumber,
                            scan_date = Scan_date,
                            scan_item = scan_item,
                            arrive_option = arrive_option,
                            cdate = DateTime.Now,
                            cuser = "fsejob"
                        };

                        await _ttDeliveryScanLog_DA.Insert(scanLogInsert);

                        SuccessCount++;
                    }
                    catch (MyException e)
                    {
                        if (!(e.ResponseCode == ResponseCodeEnum.DeliveryStatusPublicNotFound || e.ResponseCode == ResponseCodeEnum.DeliveryStatusPublicHaveP33AO3))
                        {
                            //未知錯誤
                            deliveryStatusPublicError_Condition.ErrMsg = e.Message;
                            deliveryStatusPublicError_Condition.Status = (int)ResponseCodeEnum.DeliveryStatusPublicError;
                            await _DeliveryStatusPublicError_DA.Insert(deliveryStatusPublicError_Condition);
                            ErrorCount++;
                        }
                    }
                }
                //更新 JOB ID
                info.StartId = lastId;
                await _ScheduleTask_DA.UpdateSetStartId(info);

            }

            return updateDeliveryStatusPublicToDeliveryScanLogJob_Res;





        }
    }
}
