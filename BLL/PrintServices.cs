﻿
using BLL.Model.FSExpressAPI.Model;
using BLL.Model.FSExpressAPI.Req.Consignment;
using BLL.Model.FSExpressAPI.Req.Print;
using BLL.Model.FSExpressAPI.Res.Consignment;
using BLL.Model.FSExpressAPI.Res.Print;
using BLL.Model.ScheduleAPI.Res.Consignment;
using Common;
using Common.SSRS;
using DAL.DA;
using DAL.JunFuTrans_DA;
using DAL.Model.Condition;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Transactions;
using static Common.Setting.EnumSetting;
using static Common.Setting.JobSetting;

namespace BLL
{
    public class PrintServices
    {

        private IMeasureWeightScanLog_DA _measureWeightScanLog_DA;
        private IPostSerial_DA _postSerial_DA;
        private IPost_DA _post_DA;
        private ItcDeliveryRequests_DA _tcDeliveryRequests_DA;
        private IReportPrintService _reportPrintService;
        private IttDeliveryScanLog_DA _ttDeliveryScanLog_DA;
        private IPostCollectOnDelivery_DA _postCollectOnDelivery_DA;
        private IorgArea_DA _orgArea_DA;
        private IMeasureScanLog_DA _MeasureScanLog_DA;

        public PrintServices(
             IMeasureWeightScanLog_DA measureWeightScanLog_DA,
              IPostSerial_DA postSerial_DA,
              IPost_DA post_DA,
              ItcDeliveryRequests_DA tcDeliveryRequests_DA,
              IReportPrintService reportPrintService,
              IttDeliveryScanLog_DA ttDeliveryScanLog_DA,
              IPostCollectOnDelivery_DA PostCollectOnDelivery_DA,
              IorgArea_DA orgArea_DA,
              IMeasureScanLog_DA MeasureScanLog_DA
            )
        {
            _measureWeightScanLog_DA = measureWeightScanLog_DA;
            _postSerial_DA = postSerial_DA;
            _post_DA = post_DA;
            _tcDeliveryRequests_DA = tcDeliveryRequests_DA;
            _reportPrintService = reportPrintService;
            _ttDeliveryScanLog_DA = ttDeliveryScanLog_DA;
            _postCollectOnDelivery_DA = PostCollectOnDelivery_DA;
            _orgArea_DA = orgArea_DA;
            _MeasureScanLog_DA = MeasureScanLog_DA;

        }


        /// <summary>
        /// 取得檢查碼
        /// </summary>
        /// <param name="postNumber"></param>
        /// <returns></returns>
        private static string GetPostNumberCheckCode(string postNumber)
        {
            string postNumberFull = postNumber;
            var evenDigit = false;
            int checkEvenSum = 0;
            int checkOddSum = 0;
            var checkCode = string.Empty;

            try
            {
                foreach (var digit in postNumberFull)
                {

                    if (evenDigit)
                    {
                        checkEvenSum = checkEvenSum + int.Parse(digit.ToString());
                    }
                    else
                    {
                        checkOddSum = checkOddSum + int.Parse(digit.ToString());
                    }
                    evenDigit = !evenDigit;
                }
                checkCode = ((10 - (checkEvenSum + checkOddSum * 3) % 10) % 10).ToString();
                return checkCode;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        /// <summary>
        /// 郵局標籤列印
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<PostLabel_Res> PostLabel(PostLabel_Req req, string Account)
        {
            PostLabel_Res postLabel_Res = new PostLabel_Res();

            //確定 checknumber
            var result = (await _tcDeliveryRequests_DA.GetInfoByCheckNumber(req.CheckNumber)).FirstOrDefault();

            if (result == null)
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            }

            //取得郵遞區號3碼

            if (string.IsNullOrEmpty(result.receive_city))
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            }
            if (string.IsNullOrEmpty(result.receive_city))
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            }

            var info = await _orgArea_DA.GetInfoZip3(result.receive_city, result.receive_area);

            if (info == null || info.Count() == 0)
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            }

            string zip3 = info.FirstOrDefault().ZIP3A;



            bool collection = false;
            string number = string.Empty;
            bool IsPrint = false;


            if (result.pieces.Value > 1)
            {
                //檢查是否列印過
                var PostInfo = await _postCollectOnDelivery_DA.GetInfoByrequest_idTop1(result.request_id);

                if (PostInfo == null)
                {
                    IsPrint = false;
                }
                else
                {
                    IsPrint = true;
                }
            }

            using (var _transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                //判斷是否為代收貨款
                if (result.collection_money != null && result.collection_money > 0 && !IsPrint)
                {

                    collection = true;

                    PostCollectOnDelivery_Condition postCollectOnDelivery_Condition = new PostCollectOnDelivery_Condition();
                    postCollectOnDelivery_Condition.request_id = (int)result.request_id;
                    postCollectOnDelivery_Condition.check_number = result.check_number;
                    postCollectOnDelivery_Condition.Weight = req.Weight;
                    await _postCollectOnDelivery_DA.Insert(postCollectOnDelivery_Condition);
                }
                else
                {

                    ////產郵局編號
                    //取號
                    PostSerialDiscern postSerialDiscern = PostSerialDiscern.Lv1;

                    if (int.Parse(req.Weight) > 400)
                    {
                        postSerialDiscern = PostSerialDiscern.Lv2;
                    }

                    PostInfo _postInfo = await _postSerial_DA.GetNumberByDiscern(postSerialDiscern);

                    string postNo = _postInfo.SerialNumber + _postInfo.CommodityCode + _postInfo.DiscernCode;

                    //算檢查碼
                    var CheckCode = GetPostNumberCheckCode(postNo + zip3 + "00");//00為郵局3+2 捨棄後面兩位固定
                    number = postNo + zip3 + "00" + CheckCode;

                    //建立郵局表
                    Post_Condition post_Condition = new Post_Condition();
                    post_Condition.PostId = number;
                    post_Condition.SerialNumber = _postInfo.SerialNumber;
                    post_Condition.CommodityCode = _postInfo.CommodityCode;
                    post_Condition.DiscernCode = _postInfo.DiscernCode;
                    post_Condition.Zip3 = zip3;
                    post_Condition.CheckCode = CheckCode;
                    post_Condition.request_id = (int)result.request_id;
                    post_Condition.check_number = result.check_number;
                    post_Condition.Weight = req.Weight;
                    await _post_DA.Insert(post_Condition);

                    //建立貨態
                    //存入貨態ttDeliveryScanLog scan_item = "7" 發送
                    await _ttDeliveryScanLog_DA.Insert(new ttDeliveryScanLog_Condition
                    {
                        driver_code = Account,
                        check_number = result.check_number,
                        scan_date = DateTime.Now,
                        scan_item = "7",
                        pieces = 1,
                        weight = 0,
                        runs = 1,
                        plates = 0,
                        Del_status = 0,
                        cdate = DateTime.Now,
                        tracking_number = number

                    });
                }

                //紀錄重量資訊
                MeasureWeightScanLog_Condition measureWeightScanLog_Condition = new MeasureWeightScanLog_Condition();
                measureWeightScanLog_Condition.DataSource = Account;
                measureWeightScanLog_Condition.Scancode = req.CheckNumber;
                measureWeightScanLog_Condition.Weight = req.Weight;
                await _measureWeightScanLog_DA.Add(measureWeightScanLog_Condition);

                //更新主表重量

                result.cbmWeight = double.Parse(req.Weight) / 1000;//公克轉公斤
                result.udate = DateTime.Now;
                await _tcDeliveryRequests_DA.Update(result);

                _transactionScope.Complete();

            }
            try
            {

                if (collection && !IsPrint)//是否為代收貨款
                {
                    NameValueCollection reportParams = new NameValueCollection
                    {
                        ["request_id"] = result.request_id.ToString(),
                    };
                    ReportExportType reportExportType = ReportExportType.PDF;
                    var pdfStream = _reportPrintService.GetNomalReport(reportParams, "LTCollectionMoneryLabel_PostOffice_NEW", reportExportType);
                    byte[] bytes;
                    using (var memoryStream = new MemoryStream())
                    {
                        pdfStream.CopyTo(memoryStream);
                        bytes = memoryStream.ToArray();
                    }
                    postLabel_Res.base64 = bytes;
                    postLabel_Res.Type = "2";
                    return postLabel_Res;
                }
                else
                {
                    NameValueCollection reportParams = new NameValueCollection
                    {
                        ["postId"] = number,
                    };
                    ReportExportType reportExportType = ReportExportType.PDF;

                    var pdfStream = _reportPrintService.GetNomalReport(reportParams, "LTReelLabelByIds_2_0_WeightPost", reportExportType);

                    byte[] bytes;
                    using (var memoryStream = new MemoryStream())
                    {
                        pdfStream.CopyTo(memoryStream);
                        bytes = memoryStream.ToArray();
                    }
                    postLabel_Res.base64 = bytes;
                    postLabel_Res.Type = "1";
                    return postLabel_Res;
                }

            }
            catch (Exception)
            {
                throw new MyException(ResponseCodeEnum.Error, number);
            }

        }
        /// <summary>
        /// 郵局標籤列印2
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<PostLabel_Res> PostLabel2(PostLabel_Req req, string Account)
        {
            PostLabel_Res postLabel_Res = new PostLabel_Res();

            //確定 checknumber
            var result = (await _tcDeliveryRequests_DA.GetInfoByCheckNumber(req.CheckNumber)).FirstOrDefault();

            if (result == null)
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            }

            var CBM = (await _MeasureScanLog_DA.GetInfoByCheckNumber(req.CheckNumber)).FirstOrDefault();
            
            int SUMCBM = 0;

            if (CBM != null)
            {
                if (string.IsNullOrEmpty(CBM.Width) || string.IsNullOrEmpty(CBM.Height) || string.IsNullOrEmpty(CBM.Length))
                {
                    SUMCBM = 0;
                }
                else
                {
                    SUMCBM = Convert.ToInt32(CBM.Height) + Convert.ToInt32(CBM.Length) + Convert.ToInt32(CBM.Width);
                }
            }
            
            

            //先判斷地址有無錯誤
            if (SUMCBM > 1550)
            {
                NameValueCollection reportParams = new NameValueCollection
                {
                   
                };
                ReportExportType reportExportType = ReportExportType.PDF;
                var pdfStream = _reportPrintService.GetNomalReport(reportParams, "LTReelLabelByIds_2_0_OverCBMPost", reportExportType);
                byte[] bytes;
                using (var memoryStream = new MemoryStream())
                {
                    pdfStream.CopyTo(memoryStream);
                    bytes = memoryStream.ToArray();
                }
                postLabel_Res.base64 = bytes;
                postLabel_Res.Type = "1";
                return postLabel_Res;
            }
            else if (double.Parse(req.Weight) > 20000)
            {
                NameValueCollection reportParams = new NameValueCollection
                {
                   
                };
                ReportExportType reportExportType = ReportExportType.PDF;
                var pdfStream = _reportPrintService.GetNomalReport(reportParams, "LTReelLabelByIds_2_0_OverWeightPost", reportExportType);
                byte[] bytes;
                using (var memoryStream = new MemoryStream())
                {
                    pdfStream.CopyTo(memoryStream);
                    bytes = memoryStream.ToArray();
                }
                postLabel_Res.base64 = bytes;
                postLabel_Res.Type = "1";
                return postLabel_Res;
            }
            //else if (req.CheckNumber.StartsWith("98") || req.CheckNumber.StartsWith("99") || req.CheckNumber.StartsWith("100") || req.CheckNumber.StartsWith("600"))
            //{
            //    NameValueCollection reportParams = new NameValueCollection
            //    {

            //    };
            //    ReportExportType reportExportType = ReportExportType.PDF;
            //    var pdfStream = _reportPrintService.GetNomalReport(reportParams, "NonTransPostAndSendBySelf", reportExportType);
            //    byte[] bytes;
            //    using (var memoryStream = new MemoryStream())
            //    {
            //        pdfStream.CopyTo(memoryStream);
            //        bytes = memoryStream.ToArray();
            //    }
            //    postLabel_Res.base64 = bytes;
            //    postLabel_Res.Type = "1";
            //    return postLabel_Res;
            //}
            else
            {

                //確定 checknumber
                //var result = (await _tcDeliveryRequests_DA.GetInfoByCheckNumber(req.CheckNumber)).FirstOrDefault();

                if (result == null)
                {
                    throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
                }

                //取得郵遞區號3碼

                if (string.IsNullOrEmpty(result.receive_city))
                {
                    throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
                }
                if (string.IsNullOrEmpty(result.receive_city))
                {
                    throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
                }

                var info = await _orgArea_DA.GetInfoZip3(result.receive_city, result.receive_area);

                if (info == null || info.Count() == 0)
                {
                    throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
                }

                string zip3 = info.FirstOrDefault().ZIP3A;



                bool collection = false;
                string number = string.Empty;
                bool IsPrint = false;


                if (result.pieces.Value > 1)
                {
                    //檢查是否列印過
                    var PostInfo = await _postCollectOnDelivery_DA.GetInfoByrequest_idTop1_2(result.request_id);

                    if (PostInfo == null)
                    {
                        IsPrint = false;
                    }
                    else
                    {
                        IsPrint = true;
                    }
                }

                using (var _transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    ////產郵局編號
                    //取號
                    PostSerialDiscern postSerialDiscern = PostSerialDiscern.Lv1;

                    if (int.Parse(req.Weight) > 400)
                    {
                        postSerialDiscern = PostSerialDiscern.Lv2;
                    }

                    //判斷是否為代收貨款
                    if (result.collection_money != null && result.collection_money > 0 && !IsPrint)
                    {

                        collection = true;
                        postSerialDiscern = PostSerialDiscern.CollectOnDelivery;
                        //PostCollectOnDelivery_Condition postCollectOnDelivery_Condition = new PostCollectOnDelivery_Condition();
                        //postCollectOnDelivery_Condition.request_id = (int)result.request_id;
                        //postCollectOnDelivery_Condition.check_number = result.check_number;
                        //postCollectOnDelivery_Condition.Weight = req.Weight;
                        //await _postCollectOnDelivery_DA.Insert(postCollectOnDelivery_Condition);
                    }

                    PostInfo _postInfo = await _postSerial_DA.GetNumberByDiscern(postSerialDiscern);

                    string postNo = _postInfo.SerialNumber + _postInfo.CommodityCode + _postInfo.DiscernCode;

                    //算檢查碼
                    var CheckCode = GetPostNumberCheckCode(postNo + zip3 + "00");//00為郵局3+2 捨棄後面兩位固定
                    number = postNo + zip3 + "00" + CheckCode;

                    //建立郵局表
                    Post_Condition post_Condition = new Post_Condition();
                    post_Condition.PostId = number;
                    post_Condition.SerialNumber = _postInfo.SerialNumber;
                    post_Condition.CommodityCode = _postInfo.CommodityCode;
                    post_Condition.DiscernCode = _postInfo.DiscernCode;
                    post_Condition.Zip3 = zip3;
                    post_Condition.CheckCode = CheckCode;
                    post_Condition.request_id = (int)result.request_id;
                    post_Condition.check_number = result.check_number;
                    post_Condition.Weight = req.Weight;
                    await _post_DA.Insert(post_Condition);

                    //建立貨態
                    //存入貨態ttDeliveryScanLog scan_item = "7" 發送
                    await _ttDeliveryScanLog_DA.Insert(new ttDeliveryScanLog_Condition
                    {
                        driver_code = Account,
                        check_number = result.check_number,
                        scan_date = DateTime.Now,
                        scan_item = "7",
                        pieces = 1,
                        weight = 0,
                        runs = 1,
                        plates = 0,
                        Del_status = 0,
                        cdate = DateTime.Now,
                        tracking_number = number

                    });


                    //紀錄重量資訊
                    MeasureWeightScanLog_Condition measureWeightScanLog_Condition = new MeasureWeightScanLog_Condition();
                    measureWeightScanLog_Condition.DataSource = Account;
                    measureWeightScanLog_Condition.Scancode = req.CheckNumber;
                    measureWeightScanLog_Condition.Weight = req.Weight;
                    await _measureWeightScanLog_DA.Add(measureWeightScanLog_Condition);

                    //更新主表重量

                    result.cbmWeight = double.Parse(req.Weight) / 1000;//公克轉公斤
                    result.udate = DateTime.Now;
                    await _tcDeliveryRequests_DA.Update(result);

                    _transactionScope.Complete();

                }
                try
                {
                    if (collection && !IsPrint)//是否為代收貨款
                    {
                        NameValueCollection reportParams = new NameValueCollection
                        {
                            ["PostId"] = number,
                        };
                        ReportExportType reportExportType = ReportExportType.PDF;
                        var pdfStream = _reportPrintService.GetNomalReport(reportParams, "LTCollectionMoneryLabel_PostOffice_Duo", reportExportType);
                        byte[] bytes;
                        using (var memoryStream = new MemoryStream())
                        {
                            pdfStream.CopyTo(memoryStream);
                            bytes = memoryStream.ToArray();
                        }
                        postLabel_Res.base64 = bytes;
                        postLabel_Res.Type = "2";
                        return postLabel_Res;
                    }
                    else
                    {
                        NameValueCollection reportParams = new NameValueCollection
                        {
                            ["postId"] = number,
                        };
                        ReportExportType reportExportType = ReportExportType.PDF;

                        var pdfStream = _reportPrintService.GetNomalReport(reportParams, "LTReelLabelByIds_2_0_WeightPost", reportExportType);

                        byte[] bytes;
                        using (var memoryStream = new MemoryStream())
                        {
                            pdfStream.CopyTo(memoryStream);
                            bytes = memoryStream.ToArray();
                        }
                        postLabel_Res.base64 = bytes;
                        postLabel_Res.Type = "1";
                        return postLabel_Res;
                    }

                }
                catch (Exception)
                {
                    throw new MyException(ResponseCodeEnum.Error, number);
                }

            }
        }
    }
}
