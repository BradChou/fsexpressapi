﻿using BLL.Model.AddressParsing.Model;
using DAL.JunFuAddress_DA;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BLL
{
    public class AddressServices_itri
    {
        private readonly IConfiguration _configuration;
        private IRoad_ZipCodeResult_DA _road_ZipCodeResult_DA;
        //
        // https://www.map8.zone/map8-api-docs/#geocoding-api
        //

        public AddressServices_itri
       (
          IConfiguration configuration,
          IRoad_ZipCodeResult_DA road_ZipCodeResult_DA
        )
        {
            _configuration = configuration;
            _road_ZipCodeResult_DA = road_ZipCodeResult_DA;
        }

        /// <summary>
        /// 清理髒東西
        /// </summary>
        /// <param name="fullAddr"></param>
        /// <returns></returns>
        public string ReplaceDirtyData(string fullAddr)
        {
            //清理髒東西
            fullAddr = fullAddr.Replace("（", "");
            fullAddr = fullAddr.Replace("）", "");
            fullAddr = fullAddr.Replace("(", "");
            fullAddr = fullAddr.Replace(")", "");
            fullAddr = fullAddr.Replace("(", "");
            fullAddr = fullAddr.Replace(")", "");
            fullAddr = fullAddr.Replace("<", "");
            fullAddr = fullAddr.Replace(">", "");
            fullAddr = fullAddr.Replace("＜", "");
            fullAddr = fullAddr.Replace("＞", "");
            fullAddr = fullAddr.Replace("，", "");
            fullAddr = fullAddr.Replace(",", "");
            fullAddr = fullAddr.Replace("'", "");
            fullAddr = fullAddr.Replace("--", "");
            fullAddr = fullAddr.Replace("/*", "");

            //錯字或符號
            fullAddr = fullAddr.Replace("号", "號");
            fullAddr = fullAddr.Replace("湾", "灣");
            fullAddr = fullAddr.Replace("云", "雲");
            fullAddr = fullAddr.Replace("楼", "樓");
            fullAddr = fullAddr.Replace("区", "區");
            fullAddr = fullAddr.Replace("巿", "市");
            fullAddr = fullAddr.Replace("郷", "鄉");
            fullAddr = fullAddr.Replace("ㄧ段", "一段");
            fullAddr = fullAddr.Replace("ㄧ路", "一路");
            fullAddr = fullAddr.Replace("ㄧ街", "一街");

            //全形字
            fullAddr = fullAddr.Replace("１", "1").Replace("２", "2").Replace("３", "3").Replace("４", "4").Replace("５", "5")
                                .Replace("６", "6").Replace("７", "7").Replace("８", "8").Replace("９", "9").Replace("０", "0");

            //一級或二級
            fullAddr = fullAddr.Replace("台北市", "臺北市");
            fullAddr = fullAddr.Replace("台北縣", "新北市");
            fullAddr = fullAddr.Replace("臺北縣", "新北市");
            fullAddr = fullAddr.Replace("桃園縣", "桃園市");
            fullAddr = fullAddr.Replace("台中市", "臺中市");
            fullAddr = fullAddr.Replace("台中縣", "臺中市");
            fullAddr = fullAddr.Replace("臺中縣", "臺中市");
            fullAddr = fullAddr.Replace("台南縣", "臺南市");
            fullAddr = fullAddr.Replace("臺南縣", "臺南市");
            fullAddr = fullAddr.Replace("高雄縣", "高雄市");
            fullAddr = fullAddr.Replace("台東縣", "臺東縣");
            fullAddr = fullAddr.Replace("台東市", "臺東市");

            fullAddr = fullAddr.Replace("台西鄉", "臺西鄉");
            fullAddr = fullAddr.Replace("霧台鄉", "霧臺鄉");
            fullAddr = fullAddr.Replace("樸子市", "朴子市");
            fullAddr = fullAddr.Replace("南莊鄉", "南庄鄉");
            fullAddr = fullAddr.Replace("臺中市後裡區", "臺中市后里區");
            fullAddr = fullAddr.Replace("臺中市後里區", "臺中市后里區");
            fullAddr = fullAddr.Replace("臺中市後里區", "臺中市后里區");
            fullAddr = fullAddr.Replace("臺中縣後裡鄉", "臺中市后里區");
            fullAddr = fullAddr.Replace("臺中縣後里鄉", "臺中市后里區");
            fullAddr = fullAddr.Replace("臺中縣後里鄉", "臺中市后里區");
            fullAddr = fullAddr.Replace("高雄縣三民鄉", "高雄市那瑪夏區");
            fullAddr = fullAddr.Replace("屏東縣霧台鄉", "屏東縣霧臺鄉");
            fullAddr = fullAddr.Replace("高雄市鳯山", "高雄市鳳山");
            fullAddr = fullAddr.Replace("屏東縣東港鄉", "屏東縣東港鎮");
            fullAddr = fullAddr.Replace("苗栗縣頭份鎮", "苗栗縣頭份市");

            //特定路段
            fullAddr = fullAddr.Replace("台灣大道", "臺灣大道");
            fullAddr = fullAddr.Replace("臺江大道", "台江大道");
            fullAddr = fullAddr.Replace("新臺五路", "新台五路");

            fullAddr = fullAddr.Replace("丁臺路", "丁台路");
            fullAddr = fullAddr.Replace("中臺路", "中台路");
            fullAddr = fullAddr.Replace("臺元", "台元");
            fullAddr = fullAddr.Replace("臺中路", "台中路");
            fullAddr = fullAddr.Replace("臺上路", "台上路");
            fullAddr = fullAddr.Replace("臺西南路", "台西南路");
            fullAddr = fullAddr.Replace("臺貿", "台貿");
            fullAddr = fullAddr.Replace("臺塑", "台塑");
            fullAddr = fullAddr.Replace("臺電", "台電");
            fullAddr = fullAddr.Replace("臺鳳", "台鳳");
            fullAddr = fullAddr.Replace("臺鋁", "台鋁");
            fullAddr = fullAddr.Replace("臺糖", "台糖");
            fullAddr = fullAddr.Replace("南臺", "南台");
            fullAddr = fullAddr.Replace("高臺", "高台");
            fullAddr = fullAddr.Replace("高臺", "高台");
            fullAddr = fullAddr.Replace("電臺", "電台");
            fullAddr = fullAddr.Replace("臺化", "台化");


            //清理多出來的省級
            string[] NeedClearData = { "台灣", "臺灣", "台灣省", "臺灣省" };

            string[] NeedClearData_sub = { "基隆", "台北" , "新北" , "桃園" , "新竹" ,
                                            "苗栗" , "台中" , "南投" , "彰化" , "雲林" ,
                                            "嘉義" , "台南" , "高雄" , "屏東" ,
                                            "宜蘭" , "花蓮" , "台東" ,
                                            "澎湖" , "金門" , "馬祖",
                                            "臺北", "臺中", "臺南", "臺東" };

            string tStr = "";
            for (int i = 0; i < NeedClearData.Length; i++)
            {
                for (int j = 0; j < NeedClearData_sub.Length; j++)
                {
                    tStr = NeedClearData[i] + NeedClearData_sub[j];

                    if (fullAddr.IndexOf(tStr) >= 0)
                    {
                        fullAddr = fullAddr.Replace(tStr, NeedClearData_sub[j]);
                    }
                }
            }


            return fullAddr;
        }

        public ParseAddress_itri_Model GetZipCodeAndSave_Main( string fullAddr, string Type, string ComeFrom, string CustomerCode)
        {

            string zipCode = "";
            ParseAddress_itri_Model paData = new ParseAddress_itri_Model();
            paData.InitialAddressPre = fullAddr;

            //清理髒東西
            fullAddr = ReplaceDirtyData(fullAddr);

            paData.Type = Type;
            paData.CustomerCode = CustomerCode;
            paData.ComeFrom = ComeFrom;

            try
            {
                paData.InitialAddress = fullAddr;

                string cityfilter = "市縣";
                string areafilter = "區"; //"區" "區鄉鎮市"
                string area1filter = "鄉鎮市";
                string roadfilter = "路街里巷村";

                // 處理縣市
                char[] anyOf = cityfilter.ToCharArray();
                int cityEnd = fullAddr.IndexOfAny(anyOf) + 1;
                string cityName = fullAddr.Substring(0, cityEnd);
                // 處理區鄉鎮市
                string parseII = fullAddr.Substring(cityEnd);
                anyOf = areafilter.ToCharArray();
                if (cityName.IndexOf('市') == -1)
                {
                    anyOf = area1filter.ToCharArray();
                }
                int areaEnd = parseII.IndexOfAny(anyOf) + 1;
                string areaName = parseII.Substring(0, areaEnd);

                //六都扣掉台北市有人喜歡用舊的二級地址
                //解不到 "區" 的時候 用鄉市鎮看看
                if (cityName.Equals("桃園市")
                    || cityName.Equals("臺中市") || cityName.Equals("臺南市")
                    || cityName.Equals("新北市") || cityName.Equals("高雄市"))
                {
                    if (areaName.Equals(""))
                    {
                        anyOf = area1filter.ToCharArray();
                        areaEnd = parseII.IndexOfAny(anyOf) + 1;
                        areaName = parseII.Substring(0, areaEnd);
                    }
                }

                // 處理路街段 循序從完整地址 取代一級二級
                string roadName = "";
                if (cityName != "")
                {
                    roadName = fullAddr.Replace(cityName, "");
                }
                if (areaName != "")
                {
                    roadName = roadName.Replace(areaName, "");
                }

                //移除一些 重複一級地址 的 怪地址 或是包含 , 的地址
                roadName = roadName.Replace(",", "");
                if (areaName.IndexOf(cityName) >= 0)
                {
                    areaName = areaName.Replace(cityName, "");
                }


                paData.CityName = cityName.Replace("台", "臺");
                paData.AreaName = areaName;

                //移除一些 包含 , 的一級二級地址
                paData.CityName = paData.CityName.Replace(",", "");
                paData.AreaName = paData.AreaName.Replace(",", ""); ;

                //郵遞區號當作沒看到
                if (Regex.Match(paData.CityName, @"\d+").Value != "")
                {
                    paData.CityName = paData.CityName.Replace(Regex.Match(paData.CityName, @"\d+").Value, "");
                }
                if (Regex.Match(paData.AreaName, @"\d+").Value != "")
                {
                    paData.AreaName = paData.AreaName.Replace(Regex.Match(paData.AreaName, @"\d+").Value, "");
                }
                if (Regex.Match(roadName, @"^\d+").Value != "")
                {
                    roadName = Regex.Replace(roadName, @"^\d+", "");
                }

                //六都 的二級地址都是 "區"
                if (paData.CityName.Equals("臺北市") || paData.CityName.Equals("桃園市")
                    || paData.CityName.Equals("臺中市") || paData.CityName.Equals("臺南市")
                    || paData.CityName.Equals("新北市") || paData.CityName.Equals("高雄市"))
                {
                    if (paData.AreaName.Length > 0)
                    {
                        if (!paData.AreaName.Substring(paData.AreaName.Length - 1).Equals("區"))
                        {
                            paData.AreaName = paData.AreaName.Substring(0, paData.AreaName.Length - 1) + "區";
                        }
                    }
                }
                //if (Regex.IsMatch(info, @"^\d+"))

                paData.RoadName = roadName;

                string roadData = paData.RoadName;

                try
                {
                    //沒有路段沒關係
                    //用一二級地址硬解
                    #region 沒有路段沒關係 用一二級地址硬解
                    // 將路段中有阿拉伯數字的先轉成中文數字
                    string roadChData = TranNumToChNum(roadData);

                    char[] roadArr = "巷弄號樓附".ToArray();
                    char[] unneUnit_Arr = "縣里村區市鄰路".ToArray();
                    char[] afterNO_unArr = "棟".ToArray();
                    int[] LaneArr = { 0, 3 };
                    int[] AlleyArr = { 0, 3 };
                    int[] AddressArr = { 0, 3 };
                    int[] FstArr = { 0, 3 };
                    int[] SndArr = { 0, 3 };
                    int FloorNo = -99;
                    string AddressOrigin = "";
                    string FloorOrigin = "";
                    paData.RoadName = roadChData;

                    roadName = _road_ZipCodeResult_DA.GetRoad(paData);
                    if (roadName != "" && roadName != null)
                    {
                        paData.RoadName = roadName;
                        // 去掉 Road 剩號碼段
                        roadData = roadChData.Replace(roadName, "");

                        string roadData_pre = roadData;
                        // 去掉  縣里村區市鄰路 等內容 剩號碼段
                        for (int un = 0; un < unneUnit_Arr.Length; un++)
                        {
                            if (roadData.Split(unneUnit_Arr[un]).Length > 0)
                            {
                                roadData = roadData.Substring(roadData.IndexOf(unneUnit_Arr[un]) + 1);
                            }
                        }
                        //去除之後變成空白
                        //表示門牌號碼裡面有髒東西 (縣里村區市鄰路)
                        //試著刪除髒東西
                        //刪除後可能會有誤差  EX. xx號之o  xx樓之o xx號附o 之類的
                        if (roadData == "")
                        {
                            roadData = roadData_pre;

                            // "號" / "樓" 後面的東西去掉
                            if (roadData.Length > 1
                                    && (roadData.IndexOf("號") > 0 || roadData.IndexOf("樓") > 0)
                                    && roadData.LastIndexOf("號") != roadData.Length - 1
                                    && roadData.LastIndexOf("樓") != roadData.Length - 1)
                            {
                                if (roadData.IndexOf("號") > 0)
                                {
                                    roadData = roadData.Substring(0, roadData.IndexOf("號") + 1);
                                }
                                if (roadData.IndexOf("樓") > 0)
                                {
                                    roadData = roadData.Substring(0, roadData.IndexOf("樓") + 1);
                                }

                                // 再試試看 去掉 縣里村區市鄰路 等內容 剩號碼段
                                for (int un = 0; un < unneUnit_Arr.Length; un++)
                                {
                                    if (roadData.Split(unneUnit_Arr[un]).Length > 0)
                                    {
                                        roadData = roadData.Substring(roadData.IndexOf(unneUnit_Arr[un]) + 1);
                                    }
                                }
                            }

                        }

                        // 如果路名少了號, 就將號字補在最後
                        if (roadData.IndexOf('號') == -1 && roadData.Length > 1)
                        {
                            roadData += "號";
                        }

                        // 將號碼中的中文數字轉換為阿拉伯數字
                        roadData = TranChtToNumber(roadData);
                        // 檢查路段中是否有巷
                        if (roadData.Split(roadArr[0]).Length > 0)
                        {
                            if (int.TryParse(roadData.Split(roadArr[0])[0], out LaneArr[0]) == true)
                            {
                                LaneArr[1] = Int32.Parse(roadData.Split(roadArr[0])[0]) % 2;
                                FstArr[0] = LaneArr[0];
                                FstArr[1] = LaneArr[1];
                            }
                            //去掉巷之後的字串
                            if (roadData.Length > roadData.IndexOf(roadArr[0]))
                            {
                                roadData = roadData.Substring(roadData.IndexOf(roadArr[0]) + 1);
                            }
                        }
                        // 檢查路段中是否有弄
                        if (roadData.Split(roadArr[1]).Length > 0)
                        {
                            if (int.TryParse(roadData.Split(roadArr[1])[0], out AlleyArr[0]) == true)
                            {
                                AlleyArr[1] = Int32.Parse(roadData.Split(roadArr[1])[0]) % 2;
                                SndArr[0] = AlleyArr[0];
                                SndArr[1] = AlleyArr[1];
                                if (FstArr[0] == 0)
                                {
                                    FstArr[0] = AlleyArr[0];
                                    FstArr[1] = AlleyArr[1];
                                }
                            }
                            //去掉弄之後的字串
                            if (roadData.Length > roadData.IndexOf(roadArr[1]))
                            {
                                roadData = roadData.Substring(roadData.IndexOf(roadArr[1]) + 1);
                            }
                        }
                        // 檢查路段中是否有號
                        if (roadData.Split(roadArr[2]).Length > 0)
                        {
                            AddressOrigin = roadData.Split(roadArr[2])[0];
                            AddressOrigin = AddressOrigin.Replace("之", "-");
                            if (int.TryParse(roadData.Split(roadArr[2])[0], out AddressArr[0]) == true)
                            {
                                AddressArr[1] = Int32.Parse(roadData.Split(roadArr[2])[0]) % 2;
                                if (FstArr[0] == 0)
                                {
                                    FstArr[0] = AddressArr[0];
                                    FstArr[1] = AddressArr[1];
                                }
                                if (SndArr[0] == 0)
                                {
                                    SndArr[0] = AddressArr[0];
                                    SndArr[1] = AddressArr[1];
                                }
                            }
                            else
                            {
                                // 判斷是否為幾之幾號
                                int v = 0;
                                if (roadData.IndexOf(roadArr[4]) == -1)
                                {
                                    // 沒有附號
                                    v = roadData.IndexOf(Regex.Replace(roadData.Split(roadArr[2])[0], "[0-9]", "").ToString());
                                }
                                else
                                {
                                    // 附字在號字前
                                    if (roadData.IndexOf(roadArr[4]) < roadData.IndexOf(roadArr[2]))
                                    {
                                        v = roadData.IndexOf(Regex.Replace(roadData.Split(roadArr[4])[0], "[0-9]", "").ToString());
                                    }
                                    else
                                    {
                                        v = roadData.IndexOf(Regex.Replace(roadData.Split(roadArr[2])[0], "[0-9]", "").ToString());
                                    }
                                }
                                AddressArr[0] = Int32.Parse(roadData.Substring(0, v));
                                AddressArr[1] = AddressArr[0] % 2;
                                if (FstArr[0] == 0)
                                {
                                    FstArr[0] = AddressArr[0];
                                    FstArr[1] = AddressArr[1];
                                }
                                if (SndArr[0] == 0)
                                {
                                    SndArr[0] = AddressArr[0];
                                    SndArr[1] = AddressArr[1];
                                }

                            }
                            //去掉號後的字串
                            if (roadData.Length > roadData.IndexOf(roadArr[2]))
                            {
                                roadData = roadData.Substring(roadData.IndexOf(roadArr[2]) + 1);
                            }
                        }

                        // 去掉棟等內容 剩樓層
                        for (int un1 = 0; un1 < afterNO_unArr.Length; un1++)
                        {
                            if (roadData.Split(afterNO_unArr[un1]).Length > 0)
                            {
                                roadData = roadData.Substring(roadData.IndexOf(afterNO_unArr[un1]) + 1);
                            }
                        }

                        // 檢查路段中是否有樓
                        if (roadData.Split(roadArr[3]).Length > 0 && roadData.Length > 0 && roadData.IndexOf('樓') > -1)
                        {
                            FloorOrigin = roadData.Split(roadArr[3])[0];
                            FloorOrigin = FloorOrigin.Replace("之", "-");
                            if (int.TryParse(roadData.Split(roadArr[3])[0], out FloorNo) == true)
                            {
                                // FloorNo = Int32.Parse(roadData.Split(roadArr[3])[0]) % 2;
                            }
                            else
                            {
                                // 判斷是否為幾之幾號
                                int v = roadData.IndexOf(Regex.Replace(roadData.Split(roadArr[3])[0], "[0-9]", "").ToString());
                                FloorNo = Int32.Parse(roadData.Substring(0, v));
                            }
                            //去掉號後的字串
                            if (roadData.Length > roadData.IndexOf(roadArr[3]))
                            {
                                roadData = roadData.Substring(roadData.IndexOf(roadArr[3]) + 1);
                            }
                        }

                        paData.LaneNo = LaneArr[0];
                        paData.LaneEven = LaneArr[1];
                        paData.AlleyNo = AlleyArr[0];
                        paData.AlleyEven = AlleyArr[1];
                        paData.AddressOrigin = AddressOrigin;
                        paData.AddressNo = AddressArr[0];
                        paData.AddressEven = AddressArr[1];
                        paData.FloorOrigin = FloorOrigin;
                        paData.FloorNo = FloorNo;
                        paData.FstNo = FstArr[0];
                        paData.FstEven = FstArr[1];
                        paData.SndNo = SndArr[0];
                        paData.SndEven = SndArr[1];
                    }

                    #endregion

                }
                catch (Exception ex2)
                {

                }

                //解郵遞區號
                paData = _road_ZipCodeResult_DA.GetZipCode(paData);
            }
            catch (Exception e)
            {
                //LogUtil.ErrorLog(e.ToString());
            }

            return paData;
        }
        public ParseAddress_itri_Model GetZipCodeAndSave(bool isSave , string fullAddr , string Type , string ComeFrom , string CustomerCode)
        {
            ParseAddress_itri_Model tModel = GetZipCodeAndSave_Main(fullAddr, Type, ComeFrom, CustomerCode);
           
            if (isSave.Equals(true))
            {
                _road_ZipCodeResult_DA.SaveAddress(tModel);
            }
            return tModel;
        }


        // 阿拉伯數字轉成中文字
        private string TranNumToChNum(string roadData)
        {
            string result;

            string addr = roadData;
            Match m;
            while ((m = Regex.Match(addr, "\\d+")).Success)
            {
                Int64 n = Int64.Parse(m.Value);
                string t = GetCountRefundInfoInChanese(n.ToString());
                    //EastAsiaNumericFormatter.FormatWithCulture("Ln", n,null, new CultureInfo("zh-TW"));

                //"L"-大寫，壹貳參... "Ln"-一二三... "Lc"-貨幣，同L

                addr = addr.Substring(0, m.Index) + t +
                       addr.Substring(m.Index + m.Value.Length);
            }
            result = addr;

            return result;
        }
        public string GetCountRefundInfoInChanese(string inputNum)
        {
            string[] intArr = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", };
            string[] strArr = { "零", "一", "二", "三", "四", "五", "六", "七", "八", "九", };
            string[] Chinese = { "", "十", "百", "千", "萬", "十", "百", "千", "億", "十", "百", "千", "兆", "十", "百", "千" };
            //金額
            //string[] Chinese = { "元", "十", "百", "千", "萬", "十", "百", "千", "億" };
            char[] tmpArr = inputNum.ToString().ToArray();
            string tmpVal = "";
            for (int i = 0; i < tmpArr.Length; i++)
            {
                tmpVal += strArr[tmpArr[i] - 48];//ASCII編碼 0為48
                tmpVal += Chinese[tmpArr.Length - 1 - i];//根據對應的位數插入對應的單位
            }

            if (tmpVal.Length > 0)
            {
                if (tmpVal.Substring(tmpVal.Length-1).Equals("零"))
                {
                    tmpVal = tmpVal.Substring(0, tmpVal.Length - 1);
                }
            }
            //tmpVal = tmpVal.Replace("零十", "");
            //tmpVal = tmpVal.Replace("零百", "");
            //tmpVal = tmpVal.Replace("零千", "");

            //單獨處理 10 ~ 19 號 轉換成國字有問題的 特殊案例
            // 15 -> 一十五 -> 十五
            if (inputNum.Length == 2)
            {
                if (inputNum.Substring(0,1).Equals("1"))
                {
                    if (tmpVal.Length >= 2)
                    {
                        if ( tmpVal.Substring(0,2).Equals("一十") )
                        {
                            tmpVal = tmpVal.Substring(1);
                        }
                    }
                }
            }
            return tmpVal;
        }
        // 中文數字轉成阿拉伯數字
        private string TranChtToNumber(string ChNum)
        {
            string result = "";

            string numberString = "零一二三四五六七八九";
            string unitString = "十百千萬億兆";
            bool numTouch = false;
            string ChtNum = "";

            char[] charArr = ChNum.Replace(" ", "").ToCharArray();
            if (string.IsNullOrEmpty(ChNum) || string.IsNullOrWhiteSpace(ChNum))
            {
                return "";
            }

            for (int i = 0; i < charArr.Length; i++)
            {
                // 不是中文數字的處理
                if (numberString.IndexOf(charArr[i]) == -1)
                {
                    // 若是單位文字則忽略不加進字串, 若不是且在數字中則返回數字
                    if (unitString.IndexOf(charArr[i]) == -1 && ChtNum.Length > 0)
                    {
                        if (ChtNum.Substring(ChtNum.Length - 1, 1) == "千")
                        {
                            ChtNum += "000";
                        }
                        else if (ChtNum.Substring(ChtNum.Length - 1, 1) == "百")
                        {
                            ChtNum += "00";
                        }
                        else if (ChtNum.Substring(ChtNum.Length - 1, 1) == "十")
                        {
                            ChtNum += "0";
                        }
                        ChtNum = ChtNum.Replace("千", "");
                        ChtNum = ChtNum.Replace("百", "");
                        ChtNum = ChtNum.Replace("十", "");
                        result += ChtNum;
                        result += charArr[i];
                        numTouch = false;
                        ChtNum = "";
                    }
                    else
                    {
                        // 碰到數字的第一個是十的處理方法
                        if (charArr[i] == '十')
                        {
                            if (numTouch == false)
                            {
                                // result += "1";
                                ChtNum = "1";
                                numTouch = true;
                            }
                            else if (unitString.IndexOf(charArr[i - 1]) != -1)
                            {
                                ChtNum += "1";
                            }
                            else
                            {
                                ChtNum += charArr[i];
                            }
                        }
                        else
                        {
                            if (unitString.IndexOf(charArr[i]) > 0)
                            {

                            }
                            else
                            {
                                ChtNum += charArr[i];
                            }
                            // result += charArr[i];
                        }
                    }
                }
                else
                {
                    ChtNum += numberString.IndexOf(charArr[i]).ToString();
                    //  result += numberString.IndexOf(charArr[i]).ToString();
                    numTouch = true;
                }
            }

            return result;
        }


        /// <summary>
        /// 非同步 超時就回傳空的class
        /// </summary>
        /// <param name="isSave"></param>
        /// <param name="fullAddr"></param>
        /// <param name="Type"></param>
        /// <param name="ComeFrom"></param>
        /// <param name="CustomerCode"></param>
        /// <returns></returns>
        public Map8 GetMap8_2AndSave_async(bool isSave, string fullAddr, string Type, string ComeFrom, string CustomerCode)
        {
            Map8 map8_all = new Map8();
            var tArr = new List<Task>();

            var t = Task.Factory.StartNew(() =>
            {
                return map8_all = GetMap8_2AndSave(isSave, fullAddr, Type, ComeFrom, CustomerCode);
            });
            tArr.Add(t);
            Task.WaitAll(tArr.ToArray(),2000);

            return map8_all;
        }

        public Map8 GetMap8_2AndSave(bool isSave, string fullAddr, string Type, string ComeFrom, string CustomerCode)
        {
            Map8 map8_all = new Map8();

            //保存舊地址
            string fullAddrPre = fullAddr;

            //Map8Result map8 = new Map8Result();
            //map8.Type_ = Type;
            //map8.CustomerCode = CustomerCode;
            //map8.ComeFrom = ComeFrom;
            //map8.InitialAddress = fullAddr;

            //清理髒東西
            fullAddr = ReplaceDirtyData(fullAddr);
            DateTime tNow = DateTime.Now; 


            var Map8Key = _configuration["Map8:Key"];
            var Map8RRL = _configuration["Map8:URL_2"];

            //打API
            var client = new RestClient(Map8RRL);
            var request = new RestRequest(Method.GET);
            request.AddParameter("address", fullAddr.Trim().Replace(" ", ""));
            request.AddParameter("key", Map8Key);
            var restResponse = client.Get<object>(request);

            if (restResponse.StatusCode == HttpStatusCode.OK)
            {
                map8_all = JsonConvert.DeserializeObject<Map8>(restResponse.Content);
                
                if (map8_all.results != null && map8_all.results.Count() > 0)
                {
                    foreach (Map8Result item in map8_all.results)
                    {
                        if (!string.IsNullOrEmpty(item.city))
                        {
                            /*
                            6	縣市
                            5	鄉鎮市區
                            4	路、路段
                            3	巷
                            2	弄
                            1	號、之號
                            0	無法定位
                            -1	同號，"之號"內插，如10-3及10-8，內插出10-5的坐標
                            -2	同單雙號內插，如 4號及10號內插成8號，或3號、11號內插出7號
                            -3	不同單雙號內插，即4號、11號內插6號
                            -4	同號、不同之號的代表點，如以10-3的坐標當做10-5的坐標
                            -5	同單雙號的代表點，如以 4號的坐標當做10號的坐標
                            -6	不同單雙號的代表點，如以 4號的坐標當做7號的坐標
                            -7	同樣路名的其中一點，其他狀況，以同路名的第一門牌坐標為代表
                            fuzzy	地址定位結果乃透過模糊搜尋結果而得
                            */

                            string levelstring = "1,2,3,4,5";
                            if (levelstring.IndexOf(item.level) >= 0)
                            {

                            }

                            item.Type_ = Type;
                            item.CustomerCode = CustomerCode;
                            item.ComeFrom = ComeFrom;
                            item.InitialAddressPre = fullAddrPre;
                            item.InitialAddress = fullAddr;
                            item.location_lng = item.geometry.location.lng;
                            item.location_lat = item.geometry.location.lat;
                            item.json = JsonConvert.SerializeObject(item);
                            item.CreateTime = tNow;
                            if (isSave.Equals(true))
                            {


                                ParseAddress_itri_Model tModel = GetZipCodeAndSave_Main(fullAddr, Type, ComeFrom, CustomerCode);
                                
                                item.ZipCode = tModel.ZipCode;
                                item.StationCode = tModel.StationCode;
                                item.StationName = tModel.StationName;
                                item.SalesDriverCode = tModel.SalesDriverCode;
                                item.MotorcycleDriverCode = tModel.MotorcycleDriverCode;
                                item.StackCode = tModel.StackCode;
                                item.SeqNO = tModel.SeqNO;
                                item.ShuttleStationCode = tModel.ShuttleStationCode;
                                _road_ZipCodeResult_DA.SaveAddress_Map8_2(item);
                            }

                        }
                    }
                }
            }

            return map8_all;

        }
    }
}
