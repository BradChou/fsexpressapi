﻿using BLL.Model.FSExpressAPI.Model;
using BLL.Model.FSExpressAPI.Req.Measure;
using BLL.Model.FSExpressAPI.Res.Measure;
using BLL.Model.ScheduleAPI.Req.Measure;
using BLL.Model.ScheduleAPI.Res.Measure;
using Common;
using DAL.Contract_DA;
using DAL.DA;
using DAL.Model.Condition;
using DAL.Model.Contract_Condition;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Transactions;
using static Common.Setting.EnumSetting;
using static Common.Setting.JobSetting;

namespace BLL
{
    public class MeasureServices
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _config;
        private IScheduleTask_DA _ScheduleTask_DA;
        private IMeasureScanLog_DA _MeasureScanLog_DA;
        private IMeasureScanLog_Filtered_DA _MeasureScanLog_Filtered_DA;
        private IMeasureScanLog_Exclude_DA _MeasureScanLog_Exclude_DA;
        private IMeasureScanLog_ErrLog_DA _MeasureScanLog_ErrLog_DA;
        private IttDeliveryScanLog_DA _ttDeliveryScanLog_DA;
        private IttDeliveryScanLog_ErrorScanLog_DA _ttDeliveryScanLog_ErrorScanLog_DA;
        private ItcDeliveryRequests_DA _tcDeliveryRequests_DA;
        private IMeasureImageTransferLog_DA _MeasureImageTransferLog_DA;
        private ItcDeliveryRequests_DA _tcDeliveryRequests;
        private ItbCustomers_DA _tbCustomers_DA;
        private Icbm_data_log_DA _cbm_data_log_DA;
        private ICBMDetailLog_DA _CBMDetailLog_DA;
        private ICBMDetailInfo_DA _CBMDetailInfo_DA;
        private ICBMDetail_DA _CBMDetail_DA;
        private IDailySettleTodoList_DA _DailySettleTodoList_DA;
        private string CustomerCode;

        private tbCustomers_Condition _CustomerInfo;

        private tcDeliveryRequests_Condition _tcDeliveryRequestsInfo;
        public MeasureServices(
            IHttpContextAccessor httpContextAccessor,
            IConfiguration config,
            IScheduleTask_DA ScheduleTask_DA,
            IMeasureScanLog_DA MeasureScanLog_DA,
            IMeasureScanLog_Filtered_DA MeasureScanLog_Filtered_DA,
            IMeasureScanLog_Exclude_DA MeasureScanLog_Exclude_DA,
            IMeasureScanLog_ErrLog_DA MeasureScanLog_ErrLog_DA,
            IttDeliveryScanLog_DA ttDeliveryScanLog_DA,
            IttDeliveryScanLog_ErrorScanLog_DA ttDeliveryScanLog_ErrorScanLog_DA,
            ItcDeliveryRequests_DA tcDeliveryRequests_DA,
            IMeasureImageTransferLog_DA MeasureImageTransferLog_DA,
            ItcDeliveryRequests_DA tcDeliveryRequests,
            ItbCustomers_DA tbCustomers_DA,
            Icbm_data_log_DA cbm_data_log_DA,
            ICBMDetailLog_DA CBMDetailLog_DA,
            ICBMDetailInfo_DA CBMDetailInfo_DA,
            ICBMDetail_DA CBMDetail_DA,
            IDailySettleTodoList_DA DailySettleTodoList_DA

            )
        {
            _httpContextAccessor = httpContextAccessor;
            _config = config;
            _ScheduleTask_DA = ScheduleTask_DA;
            _MeasureScanLog_DA = MeasureScanLog_DA;
            _MeasureScanLog_Filtered_DA = MeasureScanLog_Filtered_DA;
            _MeasureScanLog_Exclude_DA = MeasureScanLog_Exclude_DA;
            _MeasureScanLog_ErrLog_DA = MeasureScanLog_ErrLog_DA;
            _ttDeliveryScanLog_DA = ttDeliveryScanLog_DA;
            _ttDeliveryScanLog_ErrorScanLog_DA = ttDeliveryScanLog_ErrorScanLog_DA;
            _tcDeliveryRequests_DA = tcDeliveryRequests_DA;
            _MeasureImageTransferLog_DA = MeasureImageTransferLog_DA;
            _tcDeliveryRequests = tcDeliveryRequests;
            _tbCustomers_DA = tbCustomers_DA;
            _cbm_data_log_DA = cbm_data_log_DA;
            _CBMDetailLog_DA = CBMDetailLog_DA;
            _CBMDetailInfo_DA = CBMDetailInfo_DA;
            _CBMDetail_DA = CBMDetail_DA;
            _DailySettleTodoList_DA = DailySettleTodoList_DA;

            CustomerCode = _httpContextAccessor.HttpContext.Items["Account"] == null ? string.Empty : _httpContextAccessor.HttpContext.Items["Account"].ToString();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task InsertMesureScanLog(InsertShipmentSize_Req _Req)
        {
            MeasureScanLog_Condition _Condition = new MeasureScanLog_Condition();

            _Condition.DataSource = _Req.DataSource;
            _Condition.Scancode = _Req.Scancode;
            _Condition.Length = _Req.Length;
            _Condition.Width = _Req.Width;
            _Condition.Height = _Req.Height;
            _Condition.Size = _Req.Size;
            _Condition.Status = _Req.Status;
            _Condition.ScanTime = _Req.ScanTime;
            _Condition.Picture = _Req.Picture;

            await _MeasureScanLog_DA.Add(_Condition);

        }



        /// <summary>
        /// 過濾MesureScanLog至MeasureScanLog_Filtered(取狀態=OK)
        /// </summary>
        /// <returns></returns>
        public async Task<MesureScanLogFilterJob_Res> MesureScanLogFilter()
        {
            var result = new MesureScanLogFilterJob_Res() { NumToExclude = 0, NumToFiltered = 0 };
            var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.MesureScanLogFilterJob)).FirstOrDefault();
            var MeasureDriverCodeList = _config.GetSection("MeasureDriverCode").GetChildren().ToList();
            if (info == null)
            {
                throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            }

            var date = DateTime.Now;
            //取狀態為OK的資料
            var scanLogList = (await _MeasureScanLog_DA.GetInfoByDatetime(info.StartTime, date)).ToList();
            if (scanLogList == null)
            {
                throw new MyException(ResponseCodeEnum.Error);
            }
            if (scanLogList != null && scanLogList.Count() > 0)
            {
                try
                {
                    var lastTime = scanLogList.Max(p => p.cdate);
                    foreach (var scanLog in scanLogList)
                    {
                        //不管是什麼都給我來一筆
                        try
                        {
                            //判斷長寬高
                            int Length = int.Parse(scanLog.Length);
                            int Height = int.Parse(scanLog.Height);
                            int Width = int.Parse(scanLog.Width);

                            if (Length > 0 && Height > 0 && Width > 0)
                            {
                                CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();
                                cBMDetailLog_Condition.CheckNumber = scanLog.Scancode;
                                cBMDetailLog_Condition.ComeFrom = "2";//丈量專用袋代碼
                                cBMDetailLog_Condition.Length = int.Parse(scanLog.Length);
                                cBMDetailLog_Condition.Height = int.Parse(scanLog.Height);
                                cBMDetailLog_Condition.Width = int.Parse(scanLog.Width);
                                cBMDetailLog_Condition.CreateUser = MeasureDriverCodeList.FirstOrDefault(p => p.Key == scanLog.DataSource).Value;
                                await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

                            }


                        }
                        catch (Exception e)
                        {

                        }


                        switch (scanLog.Status)
                        {
                            //狀態為OK進入MeasureScanLog_Filtered,例外=>DataSource不存在
                            case "OK" when MeasureDriverCodeList.Any(p => p.Key == scanLog.DataSource):
                            case "NG(SizeError)" when MeasureDriverCodeList.Any(p => p.Key == scanLog.DataSource) && long.TryParse(scanLog.Scancode, out long tmp):
                                result.NumToFiltered++;
                                await _MeasureScanLog_Filtered_DA.Insert(new MeasureScanLog_Filtered_Condition
                                {
                                    MeasureScanLogId = scanLog.id,
                                    DataSource = scanLog.DataSource,
                                    Scancode = scanLog.Scancode,
                                    Length = scanLog.Length,
                                    Width = scanLog.Width,
                                    Height = scanLog.Height,
                                    Size = scanLog.Size,
                                    Status = scanLog.Status,
                                    ScanTime = scanLog.ScanTime,
                                    Picture = scanLog.Picture,
                                    cdate = DateTime.Now
                                });
                                break;
                            //其他進入MeasureScanLog_Exclude
                            default:
                                result.NumToExclude++;
                                await _MeasureScanLog_Exclude_DA.Insert(new MeasureScanLog_Exclude_Condition
                                {
                                    MeasureScanLogId = scanLog.id,
                                    DataSource = scanLog.DataSource,
                                    Scancode = scanLog.Scancode,
                                    Length = scanLog.Length,
                                    Width = scanLog.Width,
                                    Height = scanLog.Height,
                                    Size = scanLog.Size,
                                    Status = scanLog.Status,
                                    ScanTime = scanLog.ScanTime,
                                    Picture = scanLog.Picture,
                                    cdate = DateTime.Now
                                });
                                break;
                        }

                    }
                    info.StartTime = lastTime;
                    await _ScheduleTask_DA.UpdateSetStartDate(info);
                }
                catch (MyException e)
                {
                }
            }

            return result;
        }

        public async Task<UpdateMesureScanLogToMainTable_Res> UpdateMesureScanLogToMainTable()
        {
            var result = new UpdateMesureScanLogToMainTable_Res() { NumToScanLog = 0, NumToDelieveryRequest = 0, NumToExclude = 0 };
            var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.UpdateMesureScanLogToMainTable)).FirstOrDefault();
            var MeasureDriverCodeList = _config.GetSection("MeasureDriverCode").GetChildren().ToList();

            if (info == null)
            {
                throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            }
            var date = DateTime.Now;
            var scanLogList = (await _MeasureScanLog_Filtered_DA.GetInfoByDatetime(info.StartTime, date)).ToList();
            if (scanLogList == null)
            {
                throw new MyException(ResponseCodeEnum.Error);
            }
            if (scanLogList != null && scanLogList.Count() > 0)
            {
                try
                {
                    var lastTime = scanLogList.Max(p => p.cdate);
                    foreach (var scanLog in scanLogList)
                    {
                        //查詢checknumber若無則放進MeasureScanLog_Exclude
                        var updateConsignment = (await _tcDeliveryRequests_DA.GetInfoByCheckNumber(scanLog.Scancode)).LastOrDefault();
                        var scanExclude = new MeasureScanLog_Exclude_Condition();
                        var scanError = new MeasureScanLog_ErrLog_Condition();
                        var scanLogInsert = new ttDeliveryScanLog_Condition()
                        {
                            driver_code = MeasureDriverCodeList.FirstOrDefault(p => p.Key == scanLog.DataSource).Value,
                            check_number = scanLog.Scancode,
                            scan_date = scanLog.ScanTime,
                            scan_item = "7",
                            cdate = DateTime.Now
                        };
                        var scanLogErr = new ttDeliveryScanLog_ErrorScanLog_Condition();
                        //鎖貨態判讀
                        //查無單號
                        if (updateConsignment == null)
                        {
                            scanExclude = scanLog;
                            await _MeasureScanLog_Exclude_DA.Insert(scanExclude);
                            result.NumToExclude++;
                            scanLogErr = scanLogInsert;
                            scanLogErr.error_msg = "查不到此筆單號，請與發送站所進行確認。";
                            await _ttDeliveryScanLog_ErrorScanLog_DA.Insert(scanLogErr);
                            continue;
                        }
                        //正常配達
                        if (updateConsignment.latest_scan_item == "3" && updateConsignment.latest_arrive_option == "3")
                        {
                            var scanList = await _ttDeliveryScanLog_DA.GetInfoByCheckNumber(updateConsignment.check_number);
                            if (scanList.Count() > 0)
                            {
                                scanExclude = scanLog;
                                await _MeasureScanLog_Exclude_DA.Insert(scanExclude);
                                result.NumToExclude++;
                                scanLogErr = scanLogInsert;
                                scanLogErr.error_msg = "此筆已正常配達";
                                await _ttDeliveryScanLog_ErrorScanLog_DA.Insert(scanLogErr);
                                continue;
                            }
                        }
                        //已銷單
                        else if (updateConsignment.cancel_date != null)
                        {
                            scanExclude = scanLog;
                            await _MeasureScanLog_Exclude_DA.Insert(scanExclude);
                            result.NumToExclude++;
                            scanLogErr = scanLogInsert;
                            scanLogErr.error_msg = "此筆已銷單";
                            await _ttDeliveryScanLog_ErrorScanLog_DA.Insert(scanLogErr);
                            continue;
                        }
                        else
                        {
                            //存入貨態ttDeliveryScanLog scan_item = "7" 發送
                            await _ttDeliveryScanLog_DA.Insert(new ttDeliveryScanLog_Condition
                            {
                                //driver_code = _config[$"MeasureDriverCode:{scanLog.DataSource}"],
                                driver_code = MeasureDriverCodeList.FirstOrDefault(p => p.Key == scanLog.DataSource).Value,
                                check_number = scanLog.Scancode,
                                scan_date = scanLog.ScanTime,
                                scan_item = "7",
                                pieces = 1,
                                weight = 0,
                                runs = 1,
                                plates = 0,
                                Del_status = 0,
                                cdate = DateTime.Now

                            });
                            result.NumToScanLog++;
                        }
                        //3邊有一邊為0就不更新主表tcDeliveryRequests
                        if (Tool.IsZeroOrNothing(scanLog.Length) || Tool.IsZeroOrNothing(scanLog.Width) | Tool.IsZeroOrNothing(scanLog.Height))
                        {
                            continue;
                        }
                        try
                        {
                            //update主表tcDeliveryRequests
                            updateConsignment.cbmLength = Double.Parse(scanLog.Length);
                            updateConsignment.cbmWidth = Double.Parse(scanLog.Width);
                            updateConsignment.cbmHeight = Double.Parse(scanLog.Height);
                            updateConsignment.cbmCont = Double.Parse(scanLog.Size);
                            //updateConsignment.pic_path = Tool.StrTrimer(scanLog.Picture);
                            await _tcDeliveryRequests_DA.MeasureUpdate(updateConsignment);
                            result.NumToDelieveryRequest++;
                        }
                        catch (MyException e)
                        {
                            //錯誤進errlog表
                            scanError = scanLog;
                            scanError.cdate = DateTime.Now;
                            scanError.ErrMsg = e.Message;
                            await _MeasureScanLog_ErrLog_DA.Insert(scanError);
                        }
                    }
                    info.StartTime = lastTime;
                    await _ScheduleTask_DA.UpdateSetStartDate(info);
                }
                catch (MyException e)
                {
                }
            }
            return result;
        }

        //public async Task<UpdateMesureScanLogToMainTable_Res> MeasureErrDataReUpdateCycleJob()
        //public async Task<UploadMeasureImage_Res> UploadMeasureImage(UploadMeasureImage_Req req)
        public async Task<UploadMeasureImageStartTime_Res> UploadMeasureImageStartTIme(UploadMeasureImageStartTIme_Req req)
        {

            var result = new UploadMeasureImageStartTime_Res();
            var info = new ScheduleTask_Condition();
            switch (req.MeasureID)
            {
                case "29":
                    info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.UploadDaYaunCbmImageJob)).FirstOrDefault();
                    break;
                case "49":
                    info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.UploadTaiChungCbmImageJob)).FirstOrDefault();
                    break;
                default:
                    break;
            }

            if (info == null)
            {
                throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            }
            else
            {
                result.startTime = info.StartTime;
            }
            return result;
        }
        public async Task<UploadMeasureImageUpdateLog_Res> UploadMeasureImageUpdateLog(UploadMeasureImageUpdateLog_Req req)
        {
            var result = new UploadMeasureImageUpdateLog_Res();
            var info = new ScheduleTask_Condition();
            switch (req.DataSource)
            {
                case "29":
                    info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.UploadDaYaunCbmImageJob)).FirstOrDefault();
                    break;
                case "49":
                    info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.UploadTaiChungCbmImageJob)).FirstOrDefault();
                    break;
                default:
                    break;
            }
            try
            {
                await _MeasureImageTransferLog_DA.Insert(new MeasureImageTransferLog_Condition()
                {
                    DataSource = req.DataSource,
                    SourcePath = req.SourcePath,
                    DestPath = req.DestPath,
                    Message = req.Message,
                    Status = req.Status,
                });
                info.StartTime = req.lastWriteTime;
                await _ScheduleTask_DA.UpdateSetStartDate(info);
                var fileName = Path.GetFileName(req.DestPath);
                if (fileName == null || req.Status != "Success")
                {
                    return result;
                }
                else if (fileName.Length < 9)
                {
                    return result;
                }
                //update主表丈量機照片欄位;
                if (fileName.Substring(fileName.Length - 6) == "08.jpg" || fileName.Substring(fileName.Length - 8) == "08-1.jpg")
                {
                    var TaichungPath = @"TaiChungCbm\pictures\";
                    var DaYuanPath = @"DaYuanCbm\pictures\";
                    var TaichungColumn = @"catch_taichung_cbm_pic_path_from_S3";
                    var DaYuanColumn = @"catch_cbm_pic_path_from_S3";
                    var dateFolder = req.lastWriteTime.ToString("yyyyMMdd") + @"\";
                    var checkNumber = fileName.Substring(0, fileName.IndexOf("-"));
                    var consignmentUpdate = (await _tcDeliveryRequests_DA.GetInfoByCheckNumber(checkNumber)).FirstOrDefault();
                    if (consignmentUpdate == null)
                    {
                        return result;
                    }
                    switch (req.DataSource)
                    {
                        case "29":
                            await _tcDeliveryRequests_DA.MeasurePathUpdate(DaYuanColumn, DaYuanPath + dateFolder + fileName, consignmentUpdate.request_id);
                            break;
                        case "49":
                            await _tcDeliveryRequests_DA.MeasurePathUpdate(TaichungColumn, TaichungPath + dateFolder + fileName, consignmentUpdate.request_id);
                            break;
                        default:
                            break;
                    }
                }
                //update主表丈量機照片欄位(北轉08-2.jpg圖片放到台中欄位)
                if (fileName.Substring(fileName.Length - 8) == "08-2.jpg" && req.DataSource == "29")
                {
                    var DaYuanPath = @"DaYuanCbm\pictures\";
                    var TaichungColumn = @"catch_taichung_cbm_pic_path_from_S3";
                    var dateFolder = req.lastWriteTime.ToString("yyyyMMdd") + @"\";
                    var checkNumber = fileName.Substring(0, fileName.IndexOf("-"));
                    var consignmentUpdate = (await _tcDeliveryRequests_DA.GetInfoByCheckNumber(checkNumber)).FirstOrDefault();
                    if (consignmentUpdate == null)
                    {
                        return result;
                    }
                    await _tcDeliveryRequests_DA.MeasurePathUpdate(TaichungColumn, DaYuanPath + dateFolder + fileName, consignmentUpdate.request_id);
                }
            }
            catch (Exception)
            {

                throw;
            }

            return result;

            #region ftpUpload(停用)
            //if (ftpInfo.FirstOrDefault(p => p.Key == req.MeasureID + "Folder") == null || measureDirList.FirstOrDefault(p => p.Key == req.MeasureID) == null)
            //{
            //    throw new MyException(ResponseCodeEnum.MeasureIDNotFound);
            //}
            //uploadFolder = ftpRoot + ftpInfo.FirstOrDefault(p => p.Key == req.MeasureID + "Folder").Value.ToString();
            //localFolder = measureDirList.FirstOrDefault(p => p.Key == req.MeasureID).Value.ToString();

            ////localFolder = "C:\\Users\\Tim\\Desktop\\Work\\TEST\\PHOTO\\";
            //var utility = new FtpUtility();
            //utility.UserName = ftpUser;
            //utility.Password = ftpPassword;
            //utility.Path = uploadFolder;

            //if (info == null)
            //{
            //    throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            //}
            ////var dirList = new DirectoryInfo("C:\\Users\\Tim\\Desktop\\Work\\TEST\\PHOTO");
            //var dirList = new DirectoryInfo(localFolder);
            //if (!dirList.Exists)
            //{
            //    throw new MyException(ResponseCodeEnum.MeasureFolderNotFound);
            //}
            //var Files = dirList.GetFiles("*.jpg").Where(p => p.LastWriteTime > info.StartTime).OrderBy(p => p.LastWriteTime);


            //using (var client = new WebClient())
            //{
            //    client.Credentials = new NetworkCredential(ftpUser, ftpPassword);
            //    if (Files.Count() > 0)
            //    {
            //        foreach (var file in Files)
            //        {
            //            var filename = file.Name;
            //            utility.CreateFTPDirectory(uploadFolder + file.LastWriteTime.ToString("yyyyMMdd"));
            //            var ftpPath = Path.Combine(uploadFolder + DateTime.Now.ToString("yyyyMMdd"), filename);
            //            if (!utility.ListFiles().Contains(ftpPath))
            //            {
            //                try
            //                {
            //                    //var localPath = Path.Combine(sourcePath, filename);
            //                    client.UploadFile(ftpPath, WebRequestMethods.Ftp.UploadFile, file.FullName);
            //                }
            //                catch (Exception e)
            //                {
            //                    await _MeasureImageTransferLog_DA.Insert(new MeasureImageTransferLog_Condition()
            //                    {
            //                        DataSource = req.MeasureID,
            //                        SourcePath = file.FullName,
            //                        DestPath = ftpPath,
            //                        Status = "Fail",
            //                        Message = e.Message
            //                    });
            //                    continue;
            //                }
            //                await _MeasureImageTransferLog_DA.Insert(new MeasureImageTransferLog_Condition()
            //                {
            //                    DataSource = req.MeasureID,
            //                    SourcePath = file.FullName,
            //                    DestPath = ftpPath,
            //                    Status = "Success"
            //                });
            //            }
            //            else
            //            {
            //                await _MeasureImageTransferLog_DA.Insert(new MeasureImageTransferLog_Condition()
            //                {
            //                    DataSource = req.MeasureID,
            //                    SourcePath = file.FullName,
            //                    DestPath = ftpPath,
            //                    Status = "Fail",
            //                    Message = "檔案已存在"
            //                });
            //            }
            //            info.StartTime = file.LastWriteTime;
            //            await _ScheduleTask_DA.UpdateSetStartDate(info);
            //        }
            //    }
            //}
            #endregion

        }





        /// <summary>
        /// 新增丈量資訊
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task InsertCBMInfo(InsertCBMInfo_Req req)
        {
            try
            {
                CBMDetailLog_Condition cBMDetailLog_Condition = new CBMDetailLog_Condition();

                cBMDetailLog_Condition.CheckNumber = req.CheckNumber;
                cBMDetailLog_Condition.CBM = req.CBM;
                cBMDetailLog_Condition.ComeFrom = req.ComeFrom;
                cBMDetailLog_Condition.Length = req.Length;
                cBMDetailLog_Condition.Height = req.Height;
                cBMDetailLog_Condition.Width = req.Width;
                cBMDetailLog_Condition.CreateUser = CustomerCode;

                await _CBMDetailLog_DA.Insert(cBMDetailLog_Condition);

            }
            catch (Exception)
            {

                throw;
            }

        }
        /// <summary>
        /// 丈量查詢
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<GetShipmentSize_Res> GetShipmentSize(GetShipmentSize_Req req)
        {
            GetShipmentSize_Res data = new GetShipmentSize_Res();
            string checkNumber = req.CheckNumber;
            await CheckcheckNumber(checkNumber);
            //確認帳號
            await CheckCustomerCode();
            data.Height = _tcDeliveryRequestsInfo.cbmHeight.ToString();

            var cbmHeight = string.Empty;
            var cbmWidth = string.Empty;
            var cbmLength = string.Empty;
            var cbmCont = string.Empty;

            if (_tcDeliveryRequestsInfo.cbmHeight != null)
                cbmHeight = _tcDeliveryRequestsInfo.cbmHeight.HasValue ? _tcDeliveryRequestsInfo.cbmHeight.ToString() : string.Empty;
            if (_tcDeliveryRequestsInfo.cbmWidth != null)
                cbmWidth = _tcDeliveryRequestsInfo.cbmWidth.HasValue ? _tcDeliveryRequestsInfo.cbmWidth.ToString() : string.Empty;
            if (_tcDeliveryRequestsInfo.cbmLength != null)
                cbmLength = _tcDeliveryRequestsInfo.cbmLength.HasValue ? _tcDeliveryRequestsInfo.cbmLength.ToString() : string.Empty;
            if (_tcDeliveryRequestsInfo.cbmCont != null)
                cbmCont = _tcDeliveryRequestsInfo.cbmCont.HasValue ? _tcDeliveryRequestsInfo.cbmCont.ToString() : string.Empty;


            data.Height = cbmHeight;
            data.Width = cbmWidth;
            data.Length = cbmLength;
            data.Size = cbmCont;

            return data;
        }

        /// <summary>
        /// 確認帳號
        /// </summary>
        /// <returns></returns>
        private async Task CheckCustomerCode()
        {
            tbCustomers_Condition tbCustomers_Condition = new tbCustomers_Condition();
            //確認帳號
            var CustomerInfo = await _tbCustomers_DA.GetInfoByCustomerCode(CustomerCode);
            if (CustomerInfo == null)
            {
                throw new MyException(ResponseCodeEnum.CustomerNotFound);

            }
            _CustomerInfo = CustomerInfo;

            // return tbCustomers_Condition;
        }
        /// <summary>
        /// 確認託運單
        /// </summary>
        /// <returns></returns>
        private async Task CheckcheckNumber(string checkNumber)
        {
            tcDeliveryRequests_Condition tcDeliveryRequests_Condition = new tcDeliveryRequests_Condition();
            //檢查貨號 checknumber 

            var result = (await _tcDeliveryRequests.GetInfoByCheckNumber(checkNumber)).FirstOrDefault();

            if (result == null)
            {
                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
            }
            _tcDeliveryRequestsInfo = result;

        }
        /// <summary>
        /// 分類最後材積
        /// </summary>
        /// <returns></returns>
        public async Task<CBMDetailFilterJob_Res> CBMDetailFilter()
        {

            CBMDetailFilterJob_Res cBMDetailFilterJob_Res = new CBMDetailFilterJob_Res();

            try
            {

                int SuccessCount = 0;
                int ErrorCount = 0;


                //抓取上次最後執行
                //過濾
                var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.CBMDetailFilterJob)).FirstOrDefault();

                if (info == null)
                {
                    throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
                }


                var GetInfoById = await _CBMDetailLog_DA.GetInfoById(info.StartId);

                if (GetInfoById != null && GetInfoById.Count() > 0)
                {
                    var lastId = GetInfoById.Max(p => p.id);
                    var fId = GetInfoById.Min(p => p.id);
                    cBMDetailFilterJob_Res.StartId = fId;
                    cBMDetailFilterJob_Res.EndId = lastId;

                    foreach (var _GetInfoById in GetInfoById)
                    {

                        try
                        {
                            //找cheaknumber

                            string CheckNumber = _GetInfoById.CheckNumber;

                            //確認此訂單
                            var d = (await _tcDeliveryRequests.GetInfoByCheckNumber(CheckNumber)).FirstOrDefault();

                            //所有才積資訊
                            var CBMDetail = (await _CBMDetailLog_DA.GetInfoByCheckNumber(CheckNumber)).ToList();


                            if (d != null)
                            {
                                long RequestId = 0;



                                using (var _transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                                {

                                    try
                                    {
                                        RequestId = (long)d.request_id;
                                        //先清光光
                                        await _CBMDetail_DA.DELETE(CheckNumber);

                                        var count = d.pieces.HasValue ? d.pieces.Value : 0;
                                        //原來的比數


                                        //EDI
                                        var CBM_1 = CBMDetail.Where(x => x.ComeFrom.Equals("1")).ToList();
                                        //丈量 跟補發
                                        var CBM_23 = CBMDetail.Where(x => x.ComeFrom.Equals("2") || x.ComeFrom.Equals("3")).ToList();
                                        //補發
                                        //var CBM_3 = CBMDetail.Where(x => x.ComeFrom.Equals("3")).ToList();
                                        //袋裝綁
                                        var CBM_4 = CBMDetail.Where(x => x.ComeFrom.Equals("4")).ToList();
                                        //袋裝解
                                        var CBM_5 = CBMDetail.Where(x => x.ComeFrom.Equals("5")).ToList();

                                        List<CBMDetailModel> cBMs = new List<CBMDetailModel>();

                                        //優先看2 3
                                        if (CBM_23.Count() > 0)
                                        {

                                            //數據調整
                                            foreach (var _info in CBM_23)
                                            {
                                                CBMDetailModel cBM = new CBMDetailModel();
                                                cBM.id = _info.id;
                                                cBM.CBM = _info.CBM;
                                                cBM.ComeFrom = _info.ComeFrom;
                                                cBM.Width = _info.Width.HasValue ? _info.Width.Value : 0;
                                                cBM.Length = _info.Length.HasValue ? _info.Length.Value : 0;
                                                cBM.Height = _info.Height.HasValue ? _info.Height.Value : 0;
                                                cBM.CheckNumber = CheckNumber;

                                                if (cBM.Width == 0 || cBM.Length == 0 || cBM.Height == 0)
                                                {
                                                    switch (cBM.CBM)
                                                    {
                                                        case "S060":
                                                            cBM.ThreeSided = 600;
                                                            break;
                                                        case "S090":
                                                            cBM.ThreeSided = 900;
                                                            break;
                                                        case "S110":
                                                            cBM.ThreeSided = 1100;
                                                            break;
                                                        case "S120":
                                                            cBM.ThreeSided = 1200;
                                                            break;
                                                        case "S150":
                                                            cBM.ThreeSided = 1500;
                                                            break;

                                                        //袋裝另外算

                                                        default:
                                                            cBM.ThreeSided = 0;
                                                            break;

                                                    }
                                                }
                                                else
                                                {
                                                    cBM.ThreeSided = cBM.Width + cBM.Length + cBM.Height;
                                                }
                                                cBMs.Add(cBM);
                                            }
                                            //算數量
                                            var cBMsccc = cBMs.Count();


                                            if (cBMsccc == count)
                                            {

                                            }
                                            else if (cBMsccc > count)
                                            {
                                                //排序 抓數量
                                                cBMs = cBMs.OrderByDescending(x => x.ThreeSided).ThenByDescending(x => x.id).ToList().GetRange(0, count);
                                            }
                                            else if (cBMsccc < count)
                                            {
                                                //排序 補最大
                                                cBMs = cBMs.OrderByDescending(x => x.ThreeSided).ThenByDescending(x => x.id).ToList();
                                                var MAX = cBMs[0];
                                                List<CBMDetailModel> cBMs2 = new List<CBMDetailModel>();
                                                for (int i = 0; i < count - cBMsccc; i++)
                                                {
                                                    cBMs2.Add(MAX);
                                                }
                                                cBMs2.AddRange(cBMs);
                                                cBMs = cBMs2;
                                            }

                                        }
                                        else if (CBM_4.Count() > 0)
                                        {
                                            //先看有無袋裝
                                            //先看解綁最後時間
                                            if (CBM_5.Count() > 0 && CBM_4.Count() > 0)
                                            {
                                                var time = CBM_5.Max(x => x.CreateDate);
                                                //只看解綁以後
                                                CBM_4 = CBM_4.Where(x => x.CreateDate > time).ToList();
                                            }

                                            var cc4 = CBM_4.Count();

                                            if (cc4 == count)
                                            {
                                                //數據調整
                                                foreach (var _info in CBM_4)
                                                {
                                                    CBMDetailModel cBM = new CBMDetailModel();
                                                    cBM.id = _info.id;
                                                    cBM.CBM = _info.CBM;
                                                    cBM.ComeFrom = _info.ComeFrom;
                                                    cBM.Width = _info.Width.HasValue ? _info.Width.Value : 0;
                                                    cBM.Length = _info.Length.HasValue ? _info.Length.Value : 0;
                                                    cBM.Height = _info.Height.HasValue ? _info.Height.Value : 0;
                                                    cBM.CheckNumber = CheckNumber;

                                                    if (cBM.Width == 0 || cBM.Length == 0 || cBM.Height == 0)
                                                    {
                                                        switch (cBM.CBM)
                                                        {
                                                            case "S060":
                                                                cBM.ThreeSided = 600;
                                                                break;
                                                            case "S090":
                                                                cBM.ThreeSided = 900;
                                                                break;
                                                            case "S110":
                                                                cBM.ThreeSided = 1100;
                                                                break;
                                                            case "S120":
                                                                cBM.ThreeSided = 1200;
                                                                break;
                                                            case "S150":
                                                                cBM.ThreeSided = 1500;
                                                                break;
                                                            default: //都當代裝
                                                                cBM.ThreeSided = 601;
                                                                break;

                                                        }
                                                    }
                                                    else
                                                    {
                                                        cBM.ThreeSided = cBM.Width + cBM.Length + cBM.Height;
                                                    }
                                                    cBMs.Add(cBM);
                                                }
                                            }
                                            else if (cc4 > count)
                                            {
                                                //數據調整
                                                foreach (var _info in CBM_4)
                                                {
                                                    CBMDetailModel cBM = new CBMDetailModel();
                                                    cBM.id = _info.id;
                                                    cBM.CBM = _info.CBM;
                                                    cBM.ComeFrom = _info.ComeFrom;
                                                    cBM.Width = _info.Width.HasValue ? _info.Width.Value : 0;
                                                    cBM.Length = _info.Length.HasValue ? _info.Length.Value : 0;
                                                    cBM.Height = _info.Height.HasValue ? _info.Height.Value : 0;
                                                    cBM.CheckNumber = CheckNumber;

                                                    if (cBM.Width == 0 || cBM.Length == 0 || cBM.Height == 0)
                                                    {
                                                        switch (cBM.CBM)
                                                        {
                                                            case "S060":
                                                                cBM.ThreeSided = 600;
                                                                break;
                                                            case "S090":
                                                                cBM.ThreeSided = 900;
                                                                break;
                                                            case "S110":
                                                                cBM.ThreeSided = 1100;
                                                                break;
                                                            case "S120":
                                                                cBM.ThreeSided = 1200;
                                                                break;
                                                            case "S150":
                                                                cBM.ThreeSided = 1500;
                                                                break;
                                                            default: //都當代裝
                                                                cBM.ThreeSided = 601;
                                                                break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cBM.ThreeSided = cBM.Width + cBM.Length + cBM.Height;
                                                    }
                                                    cBMs.Add(cBM);
                                                }

                                                cBMs = cBMs.OrderByDescending(x => x.id).ToList().GetRange(0, count);
                                            }
                                            else if (cc4 < count)
                                            {
                                                //數據調整
                                                foreach (var _info in CBM_4)
                                                {
                                                    CBMDetailModel cBM = new CBMDetailModel();
                                                    cBM.id = _info.id;
                                                    cBM.CBM = _info.CBM;
                                                    cBM.ComeFrom = _info.ComeFrom;
                                                    cBM.Width = _info.Width.HasValue ? _info.Width.Value : 0;
                                                    cBM.Length = _info.Length.HasValue ? _info.Length.Value : 0;
                                                    cBM.Height = _info.Height.HasValue ? _info.Height.Value : 0;
                                                    cBM.CheckNumber = CheckNumber;

                                                    if (cBM.Width == 0 || cBM.Length == 0 || cBM.Height == 0)
                                                    {
                                                        switch (cBM.CBM)
                                                        {
                                                            case "S060":
                                                                cBM.ThreeSided = 600;
                                                                break;
                                                            case "S090":
                                                                cBM.ThreeSided = 900;
                                                                break;
                                                            case "S110":
                                                                cBM.ThreeSided = 1100;
                                                                break;
                                                            case "S120":
                                                                cBM.ThreeSided = 1200;
                                                                break;
                                                            case "S150":
                                                                cBM.ThreeSided = 1500;
                                                                break;
                                                            default: //都當代裝
                                                                cBM.ThreeSided = 601;
                                                                break;

                                                        }
                                                    }
                                                    else
                                                    {
                                                        cBM.ThreeSided = cBM.Width + cBM.Length + cBM.Height;
                                                    }
                                                    cBMs.Add(cBM);
                                                }
                                                //補預設S90
                                                for (int i = 0; i < count - cc4; i++)
                                                {
                                                    if (d.customer_code.Equals("F1300600002") || d.customer_code.Equals("F3500010002"))
                                                    {
                                                        CBMDetailModel cBM = new CBMDetailModel();
                                                        cBM.id = 0;
                                                        cBM.CBM = d.SpecCodeId;
                                                        cBM.ComeFrom = "0";
                                                        cBM.Width = 0;
                                                        cBM.Length = 0;
                                                        cBM.Height = 0;
                                                        cBM.CheckNumber = CheckNumber;
                                                        cBM.ThreeSided = 900;
                                                        cBMs.Add(cBM);
                                                    }
                                                    else
                                                    {
                                                        CBMDetailModel cBM = new CBMDetailModel();
                                                        cBM.id = 0;
                                                        cBM.CBM = "S090";
                                                        cBM.ComeFrom = "0";
                                                        cBM.Width = 0;
                                                        cBM.Length = 0;
                                                        cBM.Height = 0;
                                                        cBM.CheckNumber = CheckNumber;
                                                        cBM.ThreeSided = 900;
                                                        cBMs.Add(cBM);
                                                    }
                                                }
                                            }
                                        }
                                        else//都沒有 
                                        {
                                            //補預設S90
                                            for (int i = 0; i < count; i++)
                                            {
                                                if (d.customer_code.Equals("F1300600002") || d.customer_code.Equals("F3500010002"))
                                                {
                                                    CBMDetailModel cBM = new CBMDetailModel();
                                                    cBM.id = 0;
                                                    cBM.CBM = d.SpecCodeId;
                                                    cBM.ComeFrom = "0";
                                                    cBM.Width = 0;
                                                    cBM.Length = 0;
                                                    cBM.Height = 0;
                                                    cBM.CheckNumber = CheckNumber;
                                                    cBM.ThreeSided = 900;
                                                    cBMs.Add(cBM);
                                                }
                                                else
                                                {
                                                    CBMDetailModel cBM = new CBMDetailModel();
                                                    cBM.id = 0;
                                                    cBM.CBM = "S090";
                                                    cBM.ComeFrom = "0";
                                                    cBM.Width = 0;
                                                    cBM.Length = 0;
                                                    cBM.Height = 0;
                                                    cBM.CheckNumber = CheckNumber;
                                                    cBM.ThreeSided = 900;
                                                    cBMs.Add(cBM);
                                                }
                                            }
                                        }

                                        foreach (var _cbm in cBMs)
                                        {
                                            CBMDetail_Condition data = new CBMDetail_Condition();
                                            data.CBMDetailLogId = _cbm.id;
                                            data.ComeFrom = _cbm.ComeFrom;
                                            data.CBM = _cbm.CBM;
                                            data.CheckNumber = _cbm.CheckNumber;
                                            data.Length = _cbm.Length;
                                            data.Width = _cbm.Width;
                                            data.Height = _cbm.Height;

                                            await _CBMDetail_DA.Insert(data);
                                        }

                                        var CBMDetailInfo = await _CBMDetailInfo_DA.GetInfoByCheckNumber(CheckNumber);
                                        if (CBMDetailInfo == null)
                                        {
                                            CBMDetailInfo_Condition cBMDetailInfo_Condition = new CBMDetailInfo_Condition();
                                            cBMDetailInfo_Condition.CheckNumber = CheckNumber;
                                            cBMDetailInfo_Condition.Status = "1";
                                            await _CBMDetailInfo_DA.Insert(cBMDetailInfo_Condition);
                                        }
                                        else
                                        {
                                            CBMDetailInfo.UpdateDate = DateTime.Now;
                                            await _CBMDetailInfo_DA.Update(CBMDetailInfo);
                                        }
                                        _transactionScope.Complete();
                                    }
                                    catch (Exception e)
                                    {
                                        _transactionScope.Dispose();
                                        throw;
                                    }

                                }
                                //加入TODOLIST
                                if (RequestId > 0)
                                {
                                    await _DailySettleTodoList_DA.Insert(new DailySettleTodoList_Condition() { RequestId = RequestId, CheckNumber = CheckNumber, ActionType = "3" });
                                }
                            }

                            SuccessCount++;
                        }
                        catch (Exception e)
                        {
                            ErrorCount++;
                        }


                    }
                    info.StartId = lastId;
                    await _ScheduleTask_DA.UpdateSetStartId(info);

                    cBMDetailFilterJob_Res.SuccessCount = SuccessCount;
                    cBMDetailFilterJob_Res.ErrorCount = ErrorCount;
                }

            }
            catch (Exception ex)
            {

            }


            return cBMDetailFilterJob_Res;
        }


        //private async Task aaaa(List<string> CheckNumbers)
        //{
        //    try
        //    {
        //        //分類



        //        //確認此訂單
        //        var ds = (await _tcDeliveryRequests.GetInfoListByCheckNumber(CheckNumbers));

        //        //一次一萬筆
        //        var CBMDetailList = (await _CBMDetailLog_DA.GetInfoListByCheckNumber(CheckNumbers)).ToList();



        //        foreach (var d in ds)
        //        {
        //            if (d != null)
        //            {
        //                //找cheaknumber

        //                string CheckNumber = d.check_number;

        //                var count = d.pieces.HasValue ? d.pieces.Value : 0;
        //                //原來的比數


        //                var cbm = CBMDetailList.Where(x => x.CheckNumber.Equals(CheckNumber));


        //                //EDI
        //                var CBM_1 = cbm.Where(x => x.ComeFrom.Equals("1")).ToList();
        //                //丈量 跟補發
        //                var CBM_23 = cbm.Where(x => x.ComeFrom.Equals("2") || x.ComeFrom.Equals("3")).ToList();
        //                //補發
        //                //var CBM_3 = cbm.Where(x => x.ComeFrom.Equals("3")).ToList();
        //                //袋裝綁
        //                var CBM_4 = cbm.Where(x => x.ComeFrom.Equals("4")).ToList();
        //                //袋裝解
        //                var CBM_5 = cbm.Where(x => x.ComeFrom.Equals("5")).ToList();

        //                List<CBMDetailModel> cBMs = new List<CBMDetailModel>();

        //                //優先看2 3
        //                if (CBM_23.Count() > 0)
        //                {

        //                    //數據調整
        //                    foreach (var _info in CBM_23)
        //                    {
        //                        CBMDetailModel cBM = new CBMDetailModel();
        //                        cBM.id = _info.id;
        //                        cBM.CBM = _info.CBM;
        //                        cBM.ComeFrom = _info.ComeFrom;
        //                        cBM.Width = _info.Width.HasValue ? _info.Width.Value : 0;
        //                        cBM.Length = _info.Length.HasValue ? _info.Length.Value : 0;
        //                        cBM.Height = _info.Height.HasValue ? _info.Height.Value : 0;
        //                        cBM.CheckNumber = CheckNumber;

        //                        if (cBM.Width == 0 || cBM.Length == 0 || cBM.Height == 0)
        //                        {
        //                            switch (cBM.CBM)
        //                            {
        //                                case "S060":
        //                                    cBM.ThreeSided = 600;
        //                                    break;
        //                                case "S090":
        //                                    cBM.ThreeSided = 900;
        //                                    break;
        //                                case "S110":
        //                                    cBM.ThreeSided = 1100;
        //                                    break;
        //                                case "S120":
        //                                    cBM.ThreeSided = 1200;
        //                                    break;
        //                                case "S150":
        //                                    cBM.ThreeSided = 1500;
        //                                    break;

        //                                //袋裝另外算



        //                                default:
        //                                    cBM.ThreeSided = 0;
        //                                    break;

        //                            }
        //                        }
        //                        else
        //                        {
        //                            cBM.ThreeSided = cBM.Width + cBM.Length + cBM.Height;
        //                        }
        //                        cBMs.Add(cBM);
        //                    }
        //                    //算數量
        //                    var cBMsccc = cBMs.Count();


        //                    if (cBMsccc == count)
        //                    {

        //                    }
        //                    else if (cBMsccc > count)
        //                    {
        //                        //排序 抓數量

        //                        cBMs = cBMs.OrderByDescending(x => x.ThreeSided).ThenByDescending(x => x.id).ToList().GetRange(0, count);



        //                    }
        //                    else if (cBMsccc < count)
        //                    {
        //                        //排序 補最大

        //                        cBMs = cBMs.OrderByDescending(x => x.ThreeSided).ThenByDescending(x => x.id).ToList();

        //                        var MAX = cBMs[0];

        //                        List<CBMDetailModel> cBMs2 = new List<CBMDetailModel>();

        //                        for (int i = 0; i < count - cBMsccc; i++)
        //                        {
        //                            cBMs2.Add(MAX);
        //                        }

        //                        cBMs2.AddRange(cBMs);

        //                        cBMs = cBMs2;

        //                    }

        //                }
        //                else if (CBM_4.Count() > 0)
        //                {
        //                    //先看有無袋裝

        //                    //先看解綁最後時間
        //                    if (CBM_5.Count() > 0 && CBM_4.Count() > 0)
        //                    {
        //                        var time = CBM_5.Max(x => x.CreateDate);
        //                        //只看解綁以後
        //                        CBM_4 = CBM_4.Where(x => x.CreateDate > time).ToList();
        //                    }

        //                    var cc4 = CBM_4.Count();

        //                    if (cc4 == count)
        //                    {
        //                        //數據調整
        //                        foreach (var _info in CBM_4)
        //                        {
        //                            CBMDetailModel cBM = new CBMDetailModel();
        //                            cBM.id = _info.id;
        //                            cBM.CBM = _info.CBM;
        //                            cBM.ComeFrom = _info.ComeFrom;
        //                            cBM.Width = _info.Width.HasValue ? _info.Width.Value : 0;
        //                            cBM.Length = _info.Length.HasValue ? _info.Length.Value : 0;
        //                            cBM.Height = _info.Height.HasValue ? _info.Height.Value : 0;
        //                            cBM.CheckNumber = CheckNumber;

        //                            if (cBM.Width == 0 || cBM.Length == 0 || cBM.Height == 0)
        //                            {
        //                                switch (cBM.CBM)
        //                                {
        //                                    case "S060":
        //                                        cBM.ThreeSided = 600;
        //                                        break;
        //                                    case "S090":
        //                                        cBM.ThreeSided = 900;
        //                                        break;
        //                                    case "S110":
        //                                        cBM.ThreeSided = 1100;
        //                                        break;
        //                                    case "S120":
        //                                        cBM.ThreeSided = 1200;
        //                                        break;
        //                                    case "S150":
        //                                        cBM.ThreeSided = 1500;
        //                                        break;
        //                                    default: //都當代裝
        //                                        cBM.ThreeSided = 601;
        //                                        break;

        //                                }
        //                            }
        //                            else
        //                            {
        //                                cBM.ThreeSided = cBM.Width + cBM.Length + cBM.Height;
        //                            }
        //                            cBMs.Add(cBM);
        //                        }
        //                    }
        //                    else if (cc4 > count)
        //                    {
        //                        //數據調整
        //                        foreach (var _info in CBM_4)
        //                        {
        //                            CBMDetailModel cBM = new CBMDetailModel();
        //                            cBM.id = _info.id;
        //                            cBM.CBM = _info.CBM;
        //                            cBM.ComeFrom = _info.ComeFrom;
        //                            cBM.Width = _info.Width.HasValue ? _info.Width.Value : 0;
        //                            cBM.Length = _info.Length.HasValue ? _info.Length.Value : 0;
        //                            cBM.Height = _info.Height.HasValue ? _info.Height.Value : 0;
        //                            cBM.CheckNumber = CheckNumber;

        //                            if (cBM.Width == 0 || cBM.Length == 0 || cBM.Height == 0)
        //                            {
        //                                switch (cBM.CBM)
        //                                {
        //                                    case "S060":
        //                                        cBM.ThreeSided = 600;
        //                                        break;
        //                                    case "S090":
        //                                        cBM.ThreeSided = 900;
        //                                        break;
        //                                    case "S110":
        //                                        cBM.ThreeSided = 1100;
        //                                        break;
        //                                    case "S120":
        //                                        cBM.ThreeSided = 1200;
        //                                        break;
        //                                    case "S150":
        //                                        cBM.ThreeSided = 1500;
        //                                        break;
        //                                    default: //都當代裝
        //                                        cBM.ThreeSided = 601;
        //                                        break;

        //                                }
        //                            }
        //                            else
        //                            {
        //                                cBM.ThreeSided = cBM.Width + cBM.Length + cBM.Height;
        //                            }
        //                            cBMs.Add(cBM);
        //                        }

        //                        cBMs = cBMs.OrderByDescending(x => x.id).ToList().GetRange(0, count);
        //                    }
        //                    else if (cc4 < count)
        //                    {
        //                        //數據調整
        //                        foreach (var _info in CBM_4)
        //                        {
        //                            CBMDetailModel cBM = new CBMDetailModel();
        //                            cBM.id = _info.id;
        //                            cBM.CBM = _info.CBM;
        //                            cBM.ComeFrom = _info.ComeFrom;
        //                            cBM.Width = _info.Width.HasValue ? _info.Width.Value : 0;
        //                            cBM.Length = _info.Length.HasValue ? _info.Length.Value : 0;
        //                            cBM.Height = _info.Height.HasValue ? _info.Height.Value : 0;
        //                            cBM.CheckNumber = CheckNumber;

        //                            if (cBM.Width == 0 || cBM.Length == 0 || cBM.Height == 0)
        //                            {
        //                                switch (cBM.CBM)
        //                                {
        //                                    case "S060":
        //                                        cBM.ThreeSided = 600;
        //                                        break;
        //                                    case "S090":
        //                                        cBM.ThreeSided = 900;
        //                                        break;
        //                                    case "S110":
        //                                        cBM.ThreeSided = 1100;
        //                                        break;
        //                                    case "S120":
        //                                        cBM.ThreeSided = 1200;
        //                                        break;
        //                                    case "S150":
        //                                        cBM.ThreeSided = 1500;
        //                                        break;
        //                                    default: //都當代裝
        //                                        cBM.ThreeSided = 601;
        //                                        break;

        //                                }
        //                            }
        //                            else
        //                            {
        //                                cBM.ThreeSided = cBM.Width + cBM.Length + cBM.Height;
        //                            }
        //                            cBMs.Add(cBM);
        //                        }
        //                        //補預設S90
        //                        for (int i = 0; i < count - cc4; i++)
        //                        {
        //                            CBMDetailModel cBM = new CBMDetailModel();
        //                            cBM.id = 0;
        //                            cBM.CBM = "S090";
        //                            cBM.ComeFrom = "0";
        //                            cBM.Width = 0;
        //                            cBM.Length = 0;
        //                            cBM.Height = 0;
        //                            cBM.CheckNumber = CheckNumber;
        //                            cBM.ThreeSided = 900;
        //                            cBMs.Add(cBM);
        //                        }


        //                    }







        //                }
        //                else//都沒有 
        //                {
        //                    //補預設S90
        //                    for (int i = 0; i < count; i++)
        //                    {
        //                        CBMDetailModel cBM = new CBMDetailModel();
        //                        cBM.id = 0;
        //                        cBM.CBM = "S090";
        //                        cBM.ComeFrom = "0";
        //                        cBM.Width = 0;
        //                        cBM.Length = 0;
        //                        cBM.Height = 0;
        //                        cBM.CheckNumber = CheckNumber;
        //                        cBM.ThreeSided = 900;
        //                        cBMs.Add(cBM);
        //                    }

        //                }

        //                foreach (var _cbm in cBMs)
        //                {
        //                    CBMDetailB_Condition data = new CBMDetailB_Condition();
        //                    data.CBMDetailLogId = _cbm.id;
        //                    data.ComeFrom = _cbm.ComeFrom;
        //                    data.CBM = _cbm.CBM;
        //                    data.CheckNumber = _cbm.CheckNumber;
        //                    data.Length = _cbm.Length;
        //                    data.Width = _cbm.Width;
        //                    data.Height = _cbm.Height;

        //                    await _CBMDetailB_DA.Insert(data);
        //                }



        //            }
        //        }




        //    }
        //    catch (Exception e)
        //    {


        //    }

        //}


        ///// <summary>
        ///// 分類最後材積
        ///// </summary>
        ///// <returns></returns>
        //public async Task CBMDetailFilterB()
        //{

        //    try
        //    {

        //        // await aaaa("103102687881");

        //        var results = await _tcDeliveryRequests.GetInfoByArriveDate(DateTime.Now, DateTime.Now);
        //        // List<Task> ss = new List<Task>();

        //        var vs = results.Select(x => x.check_number).ToList();

        //        List<List<string>> listGroup = new List<List<string>>();
        //        int c = 1000;//為一組
        //        int jj = c;
        //        for (int i = 0; i < vs.Count; i += c)
        //        {
        //            List<string> cList = new List<string>();
        //            cList = vs.Take(jj).Skip(i).ToList();
        //            jj += c;
        //            listGroup.Add(cList);
        //        }
        //        int cc = 0;


        //        foreach (var list in listGroup)
        //        {
        //            await aaaa(list);
        //        }


        //        //    foreach (var _results in results)
        //        //{

        //        //    var CheckNumber = _results.check_number;


        //        //    // ss.Add(aa);

        //        //}
        //        //   Task.WaitAll(ss.ToArray());

        //        ////寫死 ID
        //        //var GetInfoById = await _CBMDetailLog_DA.GetInfoById(78318);

        //        //if (GetInfoById != null && GetInfoById.Count() > 0)
        //        //{
        //        //    var lastId = GetInfoById.Max(p => p.id);

        //        //    foreach (var _info in GetInfoById)
        //        //    {
        //        //        try
        //        //        {

        //        //            string CheckNumber = _info.CheckNumber;
        //        //            //是否為解綁
        //        //            bool NOCMB5 = true;

        //        //            if (_info.CBM != null && _info.CBM.Equals("5"))
        //        //            {
        //        //                NOCMB5 = false;
        //        //            }


        //        //            //確認單號數量與比數
        //        //            //確認此訂單
        //        //            var d = (await _tcDeliveryRequests.GetInfoByCheckNumber(CheckNumber)).FirstOrDefault();

        //        //            if (d != null)
        //        //            {

        //        //                List<CBMDetailModel> cBMs = new List<CBMDetailModel>();

        //        //                CBMDetailModel cBM = new CBMDetailModel();
        //        //                cBM.id = _info.id;
        //        //                cBM.CBM = _info.CBM;
        //        //                cBM.ComeFrom = _info.ComeFrom;
        //        //                cBM.Width = _info.Width.HasValue ? _info.Width.Value : 0;
        //        //                cBM.Length = _info.Length.HasValue ? _info.Length.Value : 0;
        //        //                cBM.Height = _info.Height.HasValue ? _info.Height.Value : 0;
        //        //                cBM.CheckNumber = CheckNumber;

        //        //                if (cBM.Width == 0 || cBM.Length == 0 || cBM.Height == 0)
        //        //                {
        //        //                    switch (cBM.CBM)
        //        //                    {
        //        //                        case "S060":
        //        //                            cBM.ThreeSided = 600;
        //        //                            break;
        //        //                        case "S090":
        //        //                            cBM.ThreeSided = 900;
        //        //                            break;
        //        //                        case "S110":
        //        //                            cBM.ThreeSided = 1100;
        //        //                            break;
        //        //                        case "S120":
        //        //                            cBM.ThreeSided = 1200;
        //        //                            break;
        //        //                        case "S150":
        //        //                            cBM.ThreeSided = 1500;
        //        //                            break;
        //        //                        default:
        //        //                            cBM.ThreeSided = 0;
        //        //                            break;

        //        //                    }
        //        //                    cBM.ThreeSided = 0;

        //        //                }
        //        //                else
        //        //                {
        //        //                    cBM.ThreeSided = cBM.Width + cBM.Length + cBM.Width;
        //        //                }
        //        //                //新的第一筆

        //        //                cBMs.Add(cBM);

        //        //                //原來的比數
        //        //                var CBMDetailList = (await _CBMDetail_DA.GetInfoByCheckNumber(CheckNumber)).ToList();

        //        //                if (CBMDetailList != null && CBMDetailList.Count() > 0)
        //        //                {
        //        //                    foreach (var _CBMDetailList in CBMDetailList)
        //        //                    {
        //        //                        CBMDetailModel _cBM = new CBMDetailModel();
        //        //                        _cBM.id = _CBMDetailList.CBMDetailLogId;
        //        //                        _cBM.CBM = _CBMDetailList.CBM;
        //        //                        _cBM.ComeFrom = _CBMDetailList.ComeFrom;
        //        //                        _cBM.Width = _CBMDetailList.Width.HasValue ? _CBMDetailList.Width.Value : 0;
        //        //                        _cBM.Length = _CBMDetailList.Length.HasValue ? _CBMDetailList.Length.Value : 0;
        //        //                        _cBM.Height = _CBMDetailList.Height.HasValue ? _CBMDetailList.Height.Value : 0;
        //        //                        _cBM.CheckNumber = CheckNumber;

        //        //                        if (_cBM.Width == 0 || _cBM.Length == 0 || _cBM.Height == 0)
        //        //                        {
        //        //                            switch (_cBM.CBM)
        //        //                            {
        //        //                                case "S060":
        //        //                                    _cBM.ThreeSided = 600;
        //        //                                    break;
        //        //                                case "S090":
        //        //                                    _cBM.ThreeSided = 900;
        //        //                                    break;
        //        //                                case "S110":
        //        //                                    _cBM.ThreeSided = 1100;
        //        //                                    break;
        //        //                                case "S120":
        //        //                                    _cBM.ThreeSided = 1200;
        //        //                                    break;
        //        //                                case "S150":
        //        //                                    _cBM.ThreeSided = 1500;
        //        //                                    break;
        //        //                                default:
        //        //                                    _cBM.ThreeSided = 0;
        //        //                                    break;

        //        //                            }
        //        //                            _cBM.ThreeSided = 0;

        //        //                        }
        //        //                        else
        //        //                        {
        //        //                            _cBM.ThreeSided = _cBM.Width + _cBM.Length + _cBM.Width;
        //        //                        }
        //        //                        cBMs.Add(_cBM);
        //        //                    }
        //        //                }



        //        //                //全部資料做比對

        //        //                int pieces = d.pieces.HasValue ? d.pieces.Value : 0;

        //        //                //預設

        //        //                List<CBMDetailModel> CBM_0 = new List<CBMDetailModel>();
        //        //                //補預設S90
        //        //                for (int i = 0; i < pieces; i++)
        //        //                {
        //        //                    CBMDetailModel _cBMS90 = new CBMDetailModel();
        //        //                    _cBMS90.id = 0;
        //        //                    _cBMS90.CBM = "S090";
        //        //                    _cBMS90.ComeFrom = "0";
        //        //                    _cBMS90.Width = 0;
        //        //                    _cBMS90.Length = 0;
        //        //                    _cBMS90.Height = 0;
        //        //                    _cBMS90.CheckNumber = CheckNumber;
        //        //                    CBM_0.Add(_cBMS90);
        //        //                }


        //        //                //找所有的丈量
        //        //                List<CBMDetailModel> ALLCBMIndo = new List<CBMDetailModel>();

        //        //                //丈量
        //        //                var CBM_2 = cBMs.Where(x => x.ComeFrom.Equals("2")).OrderByDescending(x => x.ThreeSided).ToList();
        //        //                //補發
        //        //                var CBM_3 = cBMs.Where(x => x.ComeFrom.Equals("3")).OrderByDescending(x => x.ThreeSided).ToList();
        //        //                //袋裝綁
        //        //                var CBM_4 = cBMs.Where(x => x.ComeFrom.Equals("4")).OrderByDescending(x => x.CBM).ToList();
        //        //                //袋裝解
        //        //                var CBM_5 = cBMs.Where(x => x.ComeFrom.Equals("5")).OrderByDescending(x => x.CBM).ToList();

        //        //                //先清光光
        //        //                await _CBMDetail_DA.DELETE(CheckNumber);


        //        //                //以下按照順序找-丈量優先
        //        //                if (CBM_2 != null && CBM_2.Any() && ALLCBMIndo.Count() != pieces)
        //        //                {
        //        //                    foreach (var _CBM_2 in CBM_2)
        //        //                    {
        //        //                        ALLCBMIndo.Add(_CBM_2);
        //        //                        if (ALLCBMIndo.Count() == pieces)
        //        //                        {
        //        //                            break;
        //        //                        }
        //        //                    }
        //        //                }
        //        //                //以下按照順序找-綁袋裝
        //        //                if (CBM_4 != null && CBM_4.Any() && ALLCBMIndo.Count() != pieces && NOCMB5)
        //        //                {
        //        //                    foreach (var _CBM_4 in CBM_4)
        //        //                    {
        //        //                        ALLCBMIndo.Add(_CBM_4);
        //        //                        if (ALLCBMIndo.Count() == pieces)
        //        //                        {
        //        //                            break;
        //        //                        }

        //        //                    }
        //        //                }

        //        //                //以下按照順序找-補發
        //        //                if (CBM_3 != null && CBM_3.Any() && ALLCBMIndo.Count() != pieces)
        //        //                {
        //        //                    foreach (var _CBM_3 in CBM_3)
        //        //                    {
        //        //                        ALLCBMIndo.Add(_CBM_3);
        //        //                        if (ALLCBMIndo.Count() == pieces)
        //        //                        {
        //        //                            break;
        //        //                        }

        //        //                    }
        //        //                }

        //        //                //以下按照順序找-補發
        //        //                if (CBM_0 != null && CBM_0.Any() && ALLCBMIndo.Count() != pieces)
        //        //                {
        //        //                    foreach (var _CBM_0 in CBM_0)
        //        //                    {
        //        //                        ALLCBMIndo.Add(_CBM_0);
        //        //                        if (ALLCBMIndo.Count() == pieces)
        //        //                        {
        //        //                            break;
        //        //                        }

        //        //                    }
        //        //                }

        //        //                while (pieces > 0)
        //        //                {
        //        //                    var _cbm = ALLCBMIndo[pieces - 1];

        //        //                    CBMDetail_Condition data = new CBMDetail_Condition();
        //        //                    data.CBMDetailLogId = _cbm.id;
        //        //                    data.ComeFrom = _cbm.ComeFrom;
        //        //                    data.CBM = _cbm.CBM;
        //        //                    data.CheckNumber = _cbm.CheckNumber;
        //        //                    data.Length = _cbm.Length;
        //        //                    data.Width = _cbm.Width;
        //        //                    data.Height = _cbm.Height;

        //        //                    await _CBMDetail_DA.Insert(data);
        //        //                    pieces--;
        //        //                }

        //        //                var CBMDetailInfo = await _CBMDetailInfo_DA.GetInfoByCheckNumber(CheckNumber);
        //        //                if (CBMDetailInfo == null)
        //        //                {
        //        //                    CBMDetailInfo_Condition cBMDetailInfo_Condition = new CBMDetailInfo_Condition();
        //        //                    cBMDetailInfo_Condition.CheckNumber = CheckNumber;
        //        //                    cBMDetailInfo_Condition.Status = "1";
        //        //                    await _CBMDetailInfo_DA.Insert(cBMDetailInfo_Condition);
        //        //                }
        //        //                else
        //        //                {
        //        //                    CBMDetailInfo.UpdateDate = DateTime.Now;
        //        //                    await _CBMDetailInfo_DA.Update(CBMDetailInfo);
        //        //                }

        //        //            }


        //        //        }
        //        //        catch (Exception e)
        //        //        {


        //        //        }
        //        //    }
        //        //}

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}


        /// <summary>
        /// 過濾MesureScanLog至cbm_data_log(取狀態!=OK)
        /// </summary>
        /// <returns></returns>
        public async Task<MesureScanLogAbnormalJob_Res> MesureScanLogAbnormal()
        {
            var result = new MesureScanLogAbnormalJob_Res() { NumToLogTable = 0 };
            var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.MesureScanLogAbnormalJob)).FirstOrDefault();
            var MeasureDriverCodeList = _config.GetSection("MeasureDriverCode").GetChildren().ToList();
            if (info == null)
            {
                throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            }

            var date = DateTime.Now;
            //取狀態不為OK的資料
            var scanLogList = (await _MeasureScanLog_DA.GetInfoByDatetime(info.StartTime, date)).ToList();
            if (scanLogList == null)
            {
                throw new MyException(ResponseCodeEnum.Error);
            }
            if (scanLogList != null && scanLogList.Count() > 0)
            {
                try
                {
                    var lastTime = scanLogList.Max(p => p.cdate);
                    foreach (var scanLog in scanLogList)
                    {
                        if (scanLog.Status != "OK" && MeasureDriverCodeList.Any(p => p.Key == scanLog.DataSource))
                        {
                            //若原本就有狀態為OK的資料 則不加入補救表(暫不使用) 2022/3/7
                            var existLogs = await _MeasureScanLog_DA.GetOKInfoByCheckNumber(scanLog.Scancode);
                            if (existLogs.ToList().Count > 0)
                            {
                                continue;
                            }
                            result.NumToLogTable++;
                            var strCbm = string.Empty;
                            var file1 = string.Empty;
                            var file2 = string.Empty;
                            var path1 = string.Empty;
                            var path2 = string.Empty;
                            List<string> phrase = new List<string>();
                            var datePath = string.Empty;
                            if (!string.IsNullOrEmpty(scanLog.Picture))
                            {
                                List<string> tmpFile = new List<string>();
                                try
                                {
                                    if (!string.IsNullOrEmpty(scanLog.Picture))
                                    {
                                        phrase = scanLog.Picture.Split('-').ToList();
                                        tmpFile.Add(phrase[0]);
                                        if (phrase.Count > 1)
                                        {
                                            for (int i = 1; i < phrase.Count; i++)
                                            {
                                                if (phrase[i].Length >= 6)
                                                {
                                                    tmpFile.Add(phrase[i]);
                                                    break;
                                                }
                                                tmpFile.Add(phrase[i]);
                                            }
                                            if (tmpFile.Count > 1)
                                            {
                                                datePath = ((DateTime)scanLog.ScanTime).Year.ToString().Substring(0, 2) + tmpFile[tmpFile.Count - 1].Substring(0, 6);
                                            }
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    throw;
                                }

                                switch (scanLog.DataSource)
                                {
                                    case "29":
                                        strCbm = "DaYuanCbm";
                                        tmpFile.AddRange(new List<string> { "08", "1" });
                                        file1 = string.Join('-', tmpFile);
                                        tmpFile.RemoveRange(tmpFile.Count - 1, 1);
                                        tmpFile.Add("2");
                                        file2 = string.Join('-', tmpFile);
                                        path1 = strCbm + '\\' + "pictures" + '\\' + datePath + '\\' + file1 + ".jpg";
                                        path2 = strCbm + '\\' + "pictures" + '\\' + datePath + '\\' + file2 + ".jpg";
                                        break;
                                    case "49":
                                        strCbm = "TaiChungCbm";
                                        tmpFile.Add("08");
                                        file1 = string.Join('-', tmpFile);
                                        path1 = strCbm + '\\' + "pictures" + '\\' + datePath + '\\' + file1 + ".jpg";
                                        break;
                                    default:
                                        break;
                                }
                            }


                            await _cbm_data_log_DA.Insert(new cbm_data_log_Condition
                            {
                                file_name = scanLog.id.ToString(),
                                check_number = scanLog.Scancode,
                                length = string.IsNullOrEmpty(scanLog.Length) ? null : double.Parse(scanLog.Length),
                                width = string.IsNullOrEmpty(scanLog.Width) ? null : double.Parse(scanLog.Width),
                                height = string.IsNullOrEmpty(scanLog.Height) ? null : double.Parse(scanLog.Height),
                                cbm = string.IsNullOrEmpty(scanLog.Size) ? null : double.Parse(scanLog.Size),
                                scan_result = scanLog.Status,
                                scan_time = scanLog.ScanTime,
                                s3_pic_uri = path1,
                                s3_pic_uri_2 = path2,
                                data_source = scanLog.DataSource,
                                is_log = false,
                                cdate = DateTime.Now
                            });
                        }
                    }
                    info.StartTime = lastTime;
                    await _ScheduleTask_DA.UpdateSetStartDate(info);
                }
                catch (MyException e)
                {
                }
            }

            return result;
        }



    }

}
