﻿using BLL.Model.AddressParsing.Res.Address;
using BLL.Model.FSExpressAPI.Model;
using BLL.Model.FSExpressAPI.Req.Consignment;
using BLL.Model.FSExpressAPI.Res.Consignment;
using BLL.Model.ScheduleAPI.Req.DailySettlement;
using BLL.Model.ScheduleAPI.Res.Consignment;
using Common;
using Common.Model;
using Common.SSRS;
using DAL;
using DAL.Contract_DA;
using DAL.DA;
using DAL.FSE01_DA;
using DAL.JunFuTrans_DA;
using DAL.Model.Condition;
using DAL.Model.Contract_Condition;
using DAL.Model.JunFuAddress_Condition;
using DAL.Model.JunFuTrans_Condition;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.International.Converters.TraditionalChineseToSimplifiedConverter;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Transactions;
using static Common.Setting.EnumSetting;
using static Common.Setting.JobSetting;



namespace BLL
{
    public class DailyMoneySettleServices
    {
        private readonly IConfiguration _configuration;
        private ItcDeliveryRequests_DA _tcDeliveryRequests;
        private IDailyMoneySettleDetailError_DA _DailyMoneySettleDetailError_DA;
        private IDailyMoneySettleDetail_DA _DailyMoneySettleDetail_DA;
        private IDailyMoneySettleDetail_Temp_DA _DailyMoneySettleDetail_Temp_DA;
        private IDailyCBMDetail_DA _DailyCBMDetail_DA;
        private IDailyCBMDetail_Temp_DA _DailyCBMDetail_Temp_DA;
        private IAreaType_DA _AreaType_DA;
        private IBagNobinding_DA _BagNobinding_DA;
        private ItbCustomers_DA _tbCustomers_DA;
        private ICBMDetail_DA _CBMDetail_DA;
        private ISpecialAreaAudit_DA _SpecialAreaAudit_DA;
        private ICBMAudit_DA _CBMAudit_DA;
        private ILogRecord_DA _LogRecord;
        private IProductValuationManage_DA _ProductValuationManage_DA;

        public DailyMoneySettleServices(
            IConfiguration configuration,
            ItcDeliveryRequests_DA tcDeliveryRequests,
            IDailyMoneySettleDetailError_DA DailyMoneySettleDetailError,
            IDailyMoneySettleDetail_DA DailyMoneySettleDetail,
            IDailyMoneySettleDetail_Temp_DA DailyMoneySettleDetail_Temp,
            IDailyCBMDetail_DA DailyCBMDetail_DA,
            IDailyCBMDetail_Temp_DA DailyCBMDetail_Temp,
            IAreaType_DA AreaType,
            IBagNobinding_DA BagNobinding,
            ItbCustomers_DA tbCustomers,
            ICBMDetail_DA CBMDetail,
            ISpecialAreaAudit_DA SpecialAreaAudit,
            ICBMAudit_DA CBMAudit,
            ILogRecord_DA LogRecord,
            IProductValuationManage_DA ProductValuationManage
            )
        {
            _configuration = configuration;
            _tcDeliveryRequests = tcDeliveryRequests;
            _DailyMoneySettleDetailError_DA = DailyMoneySettleDetailError;
            _DailyMoneySettleDetail_DA = DailyMoneySettleDetail;
            _DailyMoneySettleDetail_Temp_DA = DailyMoneySettleDetail_Temp;
            _DailyCBMDetail_DA = DailyCBMDetail_DA;
            _DailyCBMDetail_Temp_DA = DailyCBMDetail_Temp;
            _AreaType_DA = AreaType;
            _BagNobinding_DA = BagNobinding;
            _tbCustomers_DA = tbCustomers;
            _CBMDetail_DA = CBMDetail;
            _SpecialAreaAudit_DA = SpecialAreaAudit;
            _CBMAudit_DA = CBMAudit;
            _LogRecord = LogRecord;
            _ProductValuationManage_DA = ProductValuationManage;
        }


        public async Task InsertMoneySettleDetail_new(DailySettleToDoListModel DailySettleToDoList)
        {
            _LogRecord.Save("DailySettlement", "3.doInsertMoneySettleDetail_CBM", "start");

            List<DailySettleSingleToDo> NewDataList = new List<DailySettleSingleToDo>();
            List<DailySettleSingleToDo> UpdateDataList = new List<DailySettleSingleToDo>();

            NewDataList.AddRange(DailySettleToDoList.NewDataList);
            UpdateDataList.AddRange(DailySettleToDoList.UpdateDataList);


            List<string> RequestId = new List<string>();

            foreach (var item in NewDataList)
            {
                RequestId.Add(item.CheckNumber);
            }

            foreach (var item in UpdateDataList)
            {
                RequestId.Add(item.CheckNumber);
            }

            //List<Money_Settle_Detail_Condition> insertMoneySettleDetails = new List<Money_Settle_Detail_Condition>();

            //List<CBM_Detail_Condition> insertCBMDetails = new List<CBM_Detail_Condition>();

            DateTime date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM")).AddMonths(-1);

            var results = await _DailyMoneySettleDetail_DA.GetDailySettleData(RequestId);

            //抓蛋黃白區    
            var AllAreaType = await _AreaType_DA.GetAllAreaType();

            var AllCustomer = await _tbCustomers_DA.GetInfoAllCustomer();

            //抓材積資料，包含單筆多件
            var AllCBMInfo = (await _CBMDetail_DA.GetCBMInfo_2()).ToList();

            //抓捕特服區費用
            var AllSpecialAreaAudit = await _SpecialAreaAudit_DA.GetSpecialAreaAudit();

            //抓捕超材費用
            var AllCBMAudit = await _CBMAudit_DA.GetCBMAudit();
            //using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(10), TransactionScopeAsyncFlowOption.Enabled))
            //{


            //準備最後一起匯入的資料
            List<DailyMoneySettleDetail_Condition> FinalMoneySettleList = new List<DailyMoneySettleDetail_Condition>();

            long startId = _DailyMoneySettleDetail_DA.GetKey();

            //準備最後一起匯入的資料
            List<DailyCBMDetail_Condition> FinalCBMList = new List<DailyCBMDetail_Condition>();

            _LogRecord.Save("DailySettlement", "3.doInsertMoneySettleDetail_CBM", "foreach");

            foreach (var result in results)
            {
                //_LogRecord.Save("InsertMoneySettleDetail", "1", "");

                //修正B004
                //result.SpecCodeId = FixCBM(result.SpecCodeId);

                if (FinalCBMList.Count % 10000 == 500)
                {
                    _LogRecord.Save("DailySettlement", "3.doInsertMoneySettleDetail_CBM", "foreach-index-" + FinalCBMList.Count.ToString());
                }

                DailyMoneySettleDetail_Condition insertDailyMoneySettleDetail = new DailyMoneySettleDetail_Condition();

                // var AreaType =await _AreaType_DA.GetAreaType(result.receive_city, result.receive_area);
                var CustomerData = AllCustomer.Where(x => x.customer_code == result.customer_code).FirstOrDefault();

                var AreaType = AllAreaType.Where(x => (x.City == result.receive_city) & (x.Area == result.receive_area)).FirstOrDefault();

                var isNewCustomer = AllCustomer.Where(x => x.customer_code == result.customer_code).FirstOrDefault();

                var SpecialAreaAudit = AllSpecialAreaAudit.Where(x => x.CheckNumber == result.check_number).FirstOrDefault();

                var CBMAudit = AllCBMAudit.Where(x => x.CheckNumber == result.check_number).FirstOrDefault();
                try
                {
                    insertDailyMoneySettleDetail.RequestId = Convert.ToInt32(result.request_id);
                    insertDailyMoneySettleDetail.DeliveryType = string.IsNullOrEmpty(result.DeliveryType) ? "" : result.DeliveryType;
                    insertDailyMoneySettleDetail.CheckNumber = string.IsNullOrEmpty(result.check_number) ? "" : result.check_number;
                    insertDailyMoneySettleDetail.CreateDate = DateTime.Now;
                    insertDailyMoneySettleDetail.CustomerCode = string.IsNullOrEmpty(result.customer_code) ? "" : result.customer_code;
                    insertDailyMoneySettleDetail.SendStation = result.send_station_scode == null ? "" : result.send_station_scode;
                    insertDailyMoneySettleDetail.ArriveStation = result.area_arrive_code == null ? "" : result.area_arrive_code;
                    insertDailyMoneySettleDetail.City = result.receive_city == null ? "" : result.receive_city;
                    insertDailyMoneySettleDetail.Area = result.receive_area == null ? "" : result.receive_area;
                    insertDailyMoneySettleDetail.Address = result.receive_address == null ? "" : result.receive_address;
                    insertDailyMoneySettleDetail.Pieces = result.pieces == null ? 0 : Convert.ToInt32(result.pieces);
                    insertDailyMoneySettleDetail.ReceiptFlag = result.receipt_flag == null ? false : (bool)result.receipt_flag;
                    insertDailyMoneySettleDetail.CollectionMoney = result.collection_money == null ? 0 : Convert.ToInt32(result.collection_money);
                    insertDailyMoneySettleDetail.RoundTrip = result.round_trip == null ? false : (bool)result.round_trip;
                    insertDailyMoneySettleDetail.PrintDate = result.print_date;
                    insertDailyMoneySettleDetail.ShipDate = result.ship_date;
                    insertDailyMoneySettleDetail.OrderNumber = result.order_number == null ? "" : result.order_number;
                    insertDailyMoneySettleDetail.CustomerName = result.customer_name == null ? "" : result.customer_name;
                    insertDailyMoneySettleDetail.ReceiveContact = result.receive_contact == null ? "" : result.receive_contact;
                    insertDailyMoneySettleDetail.InvoiceDesc = result.invoice_desc == null ? "" : result.invoice_desc;
                    insertDailyMoneySettleDetail.ScanDate = result.scan_date;
                    insertDailyMoneySettleDetail.Valued = result.ProductValue == null ? 0 : (int)result.ProductValue;
                    insertDailyMoneySettleDetail.CreateUser = "ADMIN";

                    //是否為宜花東
                    var East = new List<string> { "宜蘭縣", "花蓮縣", "臺東縣", "台東縣", "宜蘭市", "花蓮市", "臺東市", "台東市" };
                    if (East.Contains(insertDailyMoneySettleDetail.City))
                    {
                        insertDailyMoneySettleDetail.isEast = true;
                    }
                    else
                    {
                        insertDailyMoneySettleDetail.isEast = false;
                    }


                    insertDailyMoneySettleDetail.CustomerStation = result.CustomerStation;

                    if (string.IsNullOrEmpty(result.ProductId))
                    {
                        //ProductId 預設要分是否為新舊合約客代
                        if (CustomerData.product_type == 1)
                        {
                            if (CustomerData.is_new_customer == true)
                            {
                                if (result.pieces > 1)
                                {
                                    insertDailyMoneySettleDetail.ProductId = "CM000030";
                                }
                                else
                                {
                                    insertDailyMoneySettleDetail.ProductId = "CM000030";
                                }
                            }
                            else
                            {
                                if (result.pieces > 1)
                                {
                                    insertDailyMoneySettleDetail.ProductId = "CM000036";
                                }
                                else
                                {
                                    insertDailyMoneySettleDetail.ProductId = "CS000035";
                                }
                            }

                        }
                        else if (CustomerData.product_type == 6)
                        {

                            if (result.pieces > 1)
                            {
                                insertDailyMoneySettleDetail.ProductId = "CM000043";
                            }
                            else
                            {
                                insertDailyMoneySettleDetail.ProductId = "CM000043";
                            }

                        }
                        else if (CustomerData.product_type == 2)
                        {

                            insertDailyMoneySettleDetail.ProductId = "PS000031";
                        }
                        else if (CustomerData.product_type == 3)
                        {
                            insertDailyMoneySettleDetail.ProductId = "PS000034";
                        }
                        else if (CustomerData.product_type == 4)
                        {

                            insertDailyMoneySettleDetail.ProductId = "CS000037";
                        }
                        else if (CustomerData.product_type == 5)
                        {
                            insertDailyMoneySettleDetail.ProductId = "CS000038";
                        }
                    }
                    else
                    {
                        if (result.ProductId == "CS000029")
                        {
                            result.ProductId = "CM000030";
                        }
                        insertDailyMoneySettleDetail.ProductId = result.ProductId;
                    }
                    //配達司機工號
                    insertDailyMoneySettleDetail.DriverCode = result.driver_code;
                    //配達司機所屬站所
                    insertDailyMoneySettleDetail.ArriveDriverStation = result.ArriveDriverStation;

                    insertDailyMoneySettleDetail.ReceiveDriverStation = result.ReceiveDriverStation;

                    insertDailyMoneySettleDetail.ReceiveDriverCode = result.ReceiveDriverCode;

                    //判斷是否補特服區費用
                    if (SpecialAreaAudit != null)
                    {
                        insertDailyMoneySettleDetail.SpecialAreaFee = SpecialAreaAudit.SpecialAreaFee;
                        insertDailyMoneySettleDetail.SpecialAreaId = SpecialAreaAudit.SpecialAreaId;
                    }
                    else
                    {
                        insertDailyMoneySettleDetail.SpecialAreaFee = result.SpecialAreaFee;
                        insertDailyMoneySettleDetail.SpecialAreaId = result.SpecialAreaId;
                    }


                    //重新跑特服區地址費用
                    //string Address = insertMoneySettleDetail.City + insertMoneySettleDetail.Area + insertMoneySettleDetail.Address;

                    //if (!string.IsNullOrEmpty(Address))
                    //{
                    //    var SpecialArea = GetAddressParsing(Address, insertMoneySettleDetail.CustomerCode, "1");
                    //    if (SpecialArea != null)
                    //    {
                    //        insertMoneySettleDetail.SpecialAreaFee = (int)SpecialArea.Fee;
                    //        insertMoneySettleDetail.SpecialAreaId = (int)SpecialArea.id;
                    //    }
                    //    else
                    //    {
                    //        insertMoneySettleDetail.SpecialAreaFee = result.SpecialAreaFee;
                    //        insertMoneySettleDetail.SpecialAreaId = result.SpecialAreaId;
                    //    }
                    //}
                    //else
                    //{
                    //    insertMoneySettleDetail.SpecialAreaFee = result.SpecialAreaFee;
                    //    insertMoneySettleDetail.SpecialAreaId = result.SpecialAreaId;
                    //}

                    if (AreaType == null)
                    {
                        insertDailyMoneySettleDetail.EggArea = "1";
                    }
                    else
                    {
                        insertDailyMoneySettleDetail.EggArea = AreaType.Type;
                    }

                    insertDailyMoneySettleDetail.Request_CreateDate = (DateTime)result.cdate;

                    insertDailyMoneySettleDetail.ArriveOption = result.ArriveOption;

                    insertDailyMoneySettleDetail.ScanLogCdate = result.ScanLogCdate;

                    //if(result.cbmLength !=null && result.cbmHeight !=null && result.cbmWidth !=null)
                    //{
                    //    double OverCBM = (double)result.cbmLength + (double)result.cbmHeight + (double)result.cbmWidth;

                    //    if (OverCBM > 1550)
                    //    {
                    //        OverCBM =  Math.Ceiling((OverCBM - 1550) /100);
                    //        insertMoneySettleDetail.OverCBM = Convert.ToInt32(OverCBM);
                    //    }
                    //}


                    //var insertId = _Money_Settle_Detail_DA.Insert(insertMoneySettleDetail);
                    startId += 1;
                    insertDailyMoneySettleDetail.id = startId;
                    var insertId = insertDailyMoneySettleDetail.id;
                    FinalMoneySettleList.Add(insertDailyMoneySettleDetail);

                    //CBMDetail_DA _CBMDetail_DA = new CBMDetail_DA();

                    //增加B004修正
                    var CBMInfo = AllCBMInfo.Where(x => (x.CheckNumber == result.check_number));
                    //var CBMInfo = AllCBMInfo.Where(x => (x.CheckNumber == result.check_number)).Select(x => { x.CBM = FixCBM(GetCBMTrans(x.CBM)); return x; });
                    //if (
                    //    result.check_number.Equals("202001709114"))
                    //{

                    //}


                    //判斷丈量機單筆多件抓到的數目是否與實際件數相等，多退少補
                    if ((result.pieces - CBMInfo.Count()) == 0)
                    {
                        int i = 1;
                        foreach (var item in CBMInfo)
                        {

                            if (item.Height.HasValue && item.Width.HasValue && item.Length.HasValue && string.IsNullOrEmpty(item.CBM))
                            {
                                if (result.customer_code.Equals("F1300600002") || result.customer_code.Equals("F3500010002"))
                                {
                                    item.CBM = CalculateCBM(item.Length.Value, item.Width.Value, item.Height.Value);
                                }
                                else
                                {
                                    item.CBM = CalculateCBMADD50(item.Length.Value, item.Width.Value, item.Height.Value);
                                }
                            }
                            else if (item.CBM.Equals("S060") && item.ComeFrom.Equals("0"))  //打單只有S060 改 S090

                            {
                                item.CBM = "S090";
                            }
                            DailyCBMDetail_Condition insertCBMDetail = new DailyCBMDetail_Condition();

                            //比較原始材積選大的
                            var Spec = result.SpecCodeId;
                            var checkBig = CheckCBMBig(Spec, GetCBMTrans(item.CBM));

                            //原始資料
                            insertCBMDetail.DetailID = insertId;
                            insertCBMDetail.SN = i;
                            insertCBMDetail.CreateDate = DateTime.Now;
                            insertCBMDetail.CreateUser = "ADMIN";

                            if (CBMAudit != null)
                            {
                                item.Length = CBMAudit.CbmSizeLength == null ? 0 : CBMAudit.CbmSizeLength;
                                item.Width = CBMAudit.CbmSizeWidth == null ? 0 : CBMAudit.CbmSizeWidth;
                                item.Height = CBMAudit.CbmSizeHeight == null ? 0 : CBMAudit.CbmSizeHeight;
                            }

                            insertCBMDetail.Length = item.Length == null ? 0 : item.Length;
                            insertCBMDetail.Width = item.Width == null ? 0 : item.Width;
                            insertCBMDetail.Height = item.Height == null ? 0 : item.Height;

                            //論才
                            if (CustomerData.product_type == 6)
                            {
                                var AllCBMList = await _ProductValuationManage_DA.GetProductValuateList(result.DeliveryType, result.customer_code, result.pieces);

                                var CBMList = AllCBMList.Where(x => (x.Spec == result.SpecCodeId));

                                var MaxCBM = await _ProductValuationManage_DA.GetMaxProductValuate(result.DeliveryType, result.customer_code, result.pieces);

                                int MeasureScanCuft = GetCBMCulft(insertCBMDetail.Length, insertCBMDetail.Width, insertCBMDetail.Height);

                                //var OverCuft = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);//用自己填入的材積 與 丈量機量出來的計算超材

                                if (string.IsNullOrEmpty(result.SpecCodeId) || CBMList.Count() == 0)
                                {
                                    insertCBMDetail.CBM = MaxCBM.Spec.ToString();

                                    insertCBMDetail.OverCBM = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);
                                }
                                else
                                {
                                    insertCBMDetail.CBM = result.SpecCodeId;

                                    insertCBMDetail.OverCBM = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);
                                }

                            }
                            else
                            {

                                //原始開單大
                                if (checkBig)
                                {
                                    insertCBMDetail.Length = 0;
                                    insertCBMDetail.Width = 0;
                                    insertCBMDetail.Height = 0;
                                    insertCBMDetail.CBM = Spec;

                                }
                                else
                                {

                                    if (item.CBM == null || CBMAudit != null)
                                    {
                                        //重新計算材積大小
                                        int? sum = insertCBMDetail.Length + insertCBMDetail.Width + insertCBMDetail.Height;
                                        insertCBMDetail.CBM = GetCBMSize(sum);
                                    }
                                    else
                                    {
                                        insertCBMDetail.CBM = GetCBMTrans(item.CBM);
                                    }

                                    //計算超材
                                    if (item.Length != null && item.Width != null && item.Height != null)
                                    {
                                        double OverCBM = (double)item.Length + (double)item.Width + (double)item.Height;
                                        insertCBMDetail.OverCBM = GetOverCBM(OverCBM);
                                    }

                                }
                            }

                            i += 1;

                            FinalCBMList.Add(insertCBMDetail);
                            //await _CBM_Detail_DA.Insert(insertCBMDetail);
                        }

                    }
                    else if ((result.pieces - CBMInfo.Count()) > 0)
                    {
                        int i = 1;
                        foreach (var item in CBMInfo)
                        {
                            if (item.Height.HasValue && item.Width.HasValue && item.Length.HasValue && string.IsNullOrEmpty(item.CBM))
                            {
                                if (result.customer_code.Equals("F1300600002") || result.customer_code.Equals("F3500010002"))
                                {
                                    item.CBM = CalculateCBM(item.Length.Value, item.Width.Value, item.Height.Value);
                                }
                                else
                                {
                                    item.CBM = CalculateCBMADD50(item.Length.Value, item.Width.Value, item.Height.Value);
                                }
                            }
                            else if (item.CBM.Equals("S060") && item.ComeFrom.Equals("0"))  //打單只有S060 改 S090
                            {
                                if (result.customer_code.Equals("F1300600002") || result.customer_code.Equals("F3500010002"))
                                {

                                }
                                else
                                {
                                    item.CBM = "S090";
                                }
                            }
                            DailyCBMDetail_Condition insertCBMDetail = new DailyCBMDetail_Condition();
                            //比較原始材積選大的
                            var Spec = result.SpecCodeId;
                            var checkBig = CheckCBMBig(Spec, GetCBMTrans(item.CBM));


                            //原始資料
                            insertCBMDetail.DetailID = insertId;
                            insertCBMDetail.SN = i;
                            insertCBMDetail.CreateDate = DateTime.Now;
                            insertCBMDetail.CreateUser = "ADMIN";

                            if (CBMAudit != null)
                            {
                                item.Length = CBMAudit.CbmSizeLength == null ? 0 : CBMAudit.CbmSizeLength;
                                item.Width = CBMAudit.CbmSizeWidth == null ? 0 : CBMAudit.CbmSizeWidth;
                                item.Height = CBMAudit.CbmSizeHeight == null ? 0 : CBMAudit.CbmSizeHeight;
                            }

                            insertCBMDetail.Length = item.Length == null ? 0 : item.Length;
                            insertCBMDetail.Width = item.Width == null ? 0 : item.Width;
                            insertCBMDetail.Height = item.Height == null ? 0 : item.Height;

                            //論才
                            if (CustomerData.product_type == 6)
                            {
                                var AllCBMList = await _ProductValuationManage_DA.GetProductValuateList(result.DeliveryType, result.customer_code, result.pieces);

                                var CBMList = AllCBMList.Where(x => (x.Spec == result.SpecCodeId));

                                var MaxCBM = await _ProductValuationManage_DA.GetMaxProductValuate(result.DeliveryType, result.customer_code, result.pieces);

                                int MeasureScanCuft = GetCBMCulft(insertCBMDetail.Length, insertCBMDetail.Width, insertCBMDetail.Height);

                                //var OverCuft = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);//用自己填入的材積 與 丈量機量出來的計算超材

                                if (string.IsNullOrEmpty(result.SpecCodeId) || CBMList.Count() == 0)
                                {
                                    insertCBMDetail.CBM = MaxCBM.Spec.ToString();

                                    insertCBMDetail.OverCBM = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);
                                }
                                else
                                {
                                    insertCBMDetail.CBM = result.SpecCodeId;

                                    insertCBMDetail.OverCBM = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);
                                }

                            }
                            else
                            {

                                //原始開單大
                                if (checkBig)
                                {
                                    insertCBMDetail.Length = 0;
                                    insertCBMDetail.Width = 0;
                                    insertCBMDetail.Height = 0;
                                    insertCBMDetail.CBM = Spec;

                                }
                                else
                                {

                                    if (item.CBM == null || CBMAudit != null)
                                    {
                                        int? sum = insertCBMDetail.Length + insertCBMDetail.Width + insertCBMDetail.Height;
                                        insertCBMDetail.CBM = GetCBMSize(sum);
                                    }
                                    else
                                    {
                                        insertCBMDetail.CBM = GetCBMTrans(item.CBM);
                                    }

                                    if (item.Length != null && item.Width != null && item.Height != null)
                                    {
                                        double OverCBM = (double)item.Length + (double)item.Width + (double)item.Height;
                                        insertCBMDetail.OverCBM = GetOverCBM(OverCBM);
                                    }
                                }
                            }


                            i += 1;

                            FinalCBMList.Add(insertCBMDetail);
                            //await _CBM_Detail_DA.Insert(insertCBMDetail);
                        }

                        //比較原始材積選大的
                        var OSpec = "S090";
                        var Spec2 = result.SpecCodeId;
                        var checkBig2 = CheckCBMBig(Spec2, OSpec);


                        for (int k = i; k <= result.pieces; k++)
                        {

                            if (checkBig2)
                            {
                                OSpec = Spec2;
                            }

                            DailyCBMDetail_Condition insertCBMDetail = new DailyCBMDetail_Condition();
                            insertCBMDetail.DetailID = insertId;
                            insertCBMDetail.SN = k;
                            insertCBMDetail.Length = 0;
                            insertCBMDetail.Width = 0;
                            insertCBMDetail.Height = 0;
                            insertCBMDetail.CreateDate = DateTime.Now;
                            insertCBMDetail.CreateUser = "ADMIN";
                            insertCBMDetail.CBM = OSpec;

                            FinalCBMList.Add(insertCBMDetail);
                            //await _CBM_Detail_DA.Insert(insertCBMDetail);
                        }

                    }
                    else
                    {
                        int i = 1;
                        foreach (var item in CBMInfo)
                        {
                            if (item.Height.HasValue && item.Width.HasValue && item.Length.HasValue && string.IsNullOrEmpty(item.CBM))
                            {
                                if (result.customer_code.Equals("F1300600002") || result.customer_code.Equals("F3500010002"))
                                {
                                    item.CBM = CalculateCBM(item.Length.Value, item.Width.Value, item.Height.Value);
                                }
                                else
                                {
                                    item.CBM = CalculateCBMADD50(item.Length.Value, item.Width.Value, item.Height.Value);
                                }
                            }
                            else if (item.CBM.Equals("S060") && item.ComeFrom.Equals("0"))  //打單只有S060 改 S090
                            {
                                if (result.customer_code.Equals("F1300600002") || result.customer_code.Equals("F3500010002"))
                                {

                                }
                                else
                                {
                                    item.CBM = "S090";
                                }
                            }

                            DailyCBMDetail_Condition insertCBMDetail = new DailyCBMDetail_Condition();

                            //比較原始材積選大的
                            var Spec = result.SpecCodeId;
                            var checkBig = CheckCBMBig(Spec, GetCBMTrans(item.CBM));


                            //原始資料
                            insertCBMDetail.DetailID = insertId;
                            insertCBMDetail.SN = i;
                            insertCBMDetail.CreateDate = DateTime.Now;
                            insertCBMDetail.CreateUser = "ADMIN";

                            if (CBMAudit != null)
                            {
                                item.Length = CBMAudit.CbmSizeLength == null ? 0 : CBMAudit.CbmSizeLength;
                                item.Width = CBMAudit.CbmSizeWidth == null ? 0 : CBMAudit.CbmSizeWidth;
                                item.Height = CBMAudit.CbmSizeHeight == null ? 0 : CBMAudit.CbmSizeHeight;
                            }

                            insertCBMDetail.Length = item.Length == null ? 0 : item.Length;
                            insertCBMDetail.Width = item.Width == null ? 0 : item.Width;
                            insertCBMDetail.Height = item.Height == null ? 0 : item.Height;

                            //論才
                            if (CustomerData.product_type == 6)
                            {
                                var AllCBMList = await _ProductValuationManage_DA.GetProductValuateList(result.DeliveryType, result.customer_code, result.pieces);

                                var CBMList = AllCBMList.Where(x => (x.Spec == result.SpecCodeId));

                                var MaxCBM = await _ProductValuationManage_DA.GetMaxProductValuate(result.DeliveryType, result.customer_code, result.pieces);

                                int MeasureScanCuft = GetCBMCulft(insertCBMDetail.Length, insertCBMDetail.Width, insertCBMDetail.Height);

                                //var OverCuft = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);//用自己填入的材積 與 丈量機量出來的計算超材

                                if (string.IsNullOrEmpty(result.SpecCodeId) || CBMList.Count() == 0)
                                {
                                    insertCBMDetail.CBM = MaxCBM.Spec.ToString();

                                    insertCBMDetail.OverCBM = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);
                                }
                                else
                                {
                                    insertCBMDetail.CBM = result.SpecCodeId;

                                    insertCBMDetail.OverCBM = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);
                                }

                            }
                            else
                            {

                                //原始開單大
                                if (checkBig)
                                {
                                    insertCBMDetail.Length = 0;
                                    insertCBMDetail.Width = 0;
                                    insertCBMDetail.Height = 0;
                                    insertCBMDetail.CBM = Spec;

                                }
                                else
                                {

                                    if (item.CBM == null || CBMAudit != null)
                                    {
                                        //重新計算材積大小
                                        int? sum = insertCBMDetail.Length + insertCBMDetail.Width + insertCBMDetail.Height;
                                        insertCBMDetail.CBM = GetCBMSize(sum);
                                    }
                                    else
                                    {
                                        insertCBMDetail.CBM = GetCBMTrans(item.CBM);
                                    }

                                    //計算超材
                                    if (item.Length != null && item.Width != null && item.Height != null)
                                    {
                                        double OverCBM = (double)item.Length + (double)item.Width + (double)item.Height;
                                        insertCBMDetail.OverCBM = GetOverCBM(OverCBM);
                                    }

                                }
                            }

                            i += 1;

                            FinalCBMList.Add(insertCBMDetail);
                            //await _CBM_Detail_DA.Insert(insertCBMDetail);

                            if (i - result.pieces == 1)
                            {
                                break;
                            }
                        }

                    }

                    //for (int i = 1; i <= result.pieces; i++)
                    //{
                    //    CBM_Detail_Condition insertCBMDetail = new CBM_Detail_Condition();
                    //    insertCBMDetail.DetailID = insertId;
                    //    insertCBMDetail.SN = i;
                    //    insertCBMDetail.Length = result.cbmLength == null ? 0 : (double)result.cbmLength;
                    //    insertCBMDetail.Width = result.cbmWidth == null ? 0 : (double)result.cbmWidth;
                    //    insertCBMDetail.Height = result.cbmHeight == null ? 0 : (double)result.cbmHeight;
                    //    insertCBMDetail.CreateDate = DateTime.Now;
                    //    insertCBMDetail.CreateUser = "ADMIN";

                    //    var BagNo = await _BagNobinding_DA.GetBagNo(result.request_id.ToString());

                    //    var invoicePattern = "B0[0-9]{1}";


                    //    if (BagNo != null )
                    //    {
                    //        if (Regex.Match(BagNo.BagNo, invoicePattern).Success)
                    //        {
                    //            insertCBMDetail.CBM = "B003";
                    //        }
                    //        else 
                    //        {
                    //        continue;
                    //        }

                    //    }
                    //    else
                    //    {
                    //        double sum = insertCBMDetail.Length + insertCBMDetail.Width + insertCBMDetail.Height;
                    //        if (sum < 1)
                    //        {
                    //            insertCBMDetail.CBM = "S090";
                    //        }
                    //        else if (sum <= 650)
                    //        {
                    //            insertCBMDetail.CBM = "S060";
                    //        }
                    //        else if (sum <= 950)
                    //        {
                    //            insertCBMDetail.CBM = "S090";
                    //        }
                    //        else if (sum <= 1150)
                    //        {
                    //            insertCBMDetail.CBM = "S110";
                    //        }
                    //        else if (sum <= 1250)
                    //        {
                    //            insertCBMDetail.CBM = "S120";
                    //        }
                    //        else
                    //        {
                    //            insertCBMDetail.CBM = "S150";
                    //        }
                    //    }
                    //    await _CBM_Detail_DA.Insert(insertCBMDetail);
                    //}                   

                }
                catch (Exception e)
                {
                    _LogRecord.Save("DailySettlement", "3.doInsertMoneySettleDetail_CB", "error-" + result.check_number);
                    DailySettleToDoList.ErrorDataList.Add(new DailySettleSingleToDo(result.request_id.ToString(), result.check_number, "4"));

                }

            }


            try
            {
                //List<DailyMoneySettleDetail_Temp_Condition> FinalMoneySettleList_Temp = FinalMoneySettleList.Cast<DailyMoneySettleDetail_Temp_Condition>().ToList();
                //List<DailyCBMDetail_Temp_Condition> FinalCBMList_Temp = FinalCBMList.Cast<DailyCBMDetail_Temp_Condition>().ToList();
                _DailyMoneySettleDetail_Temp_DA.Delete();
                _DailyCBMDetail_Temp_DA.Delete();



                _DailyMoneySettleDetail_DA.Insert(FinalMoneySettleList);


                List<DailyMoneySettleDetail_Temp_Condition> FinalMoneySettleList_Temp = new List<DailyMoneySettleDetail_Temp_Condition>();
                foreach (var item in FinalMoneySettleList)
                {
                    FinalMoneySettleList_Temp.Add(item);
                }

                _DailyMoneySettleDetail_Temp_DA.InsertTemp(FinalMoneySettleList_Temp);
                _LogRecord.Save("DailySettlement", "3.doInsertMoneySettleDetail_CBM", "MoneySettle -" + FinalMoneySettleList.Count.ToString());


                await _DailyCBMDetail_DA.Insert(FinalCBMList);

                List<DailyCBMDetail_Temp_Condition> FinalCBMList_Temp = new List<DailyCBMDetail_Temp_Condition>();
                foreach (var item in FinalCBMList)
                {
                    FinalCBMList_Temp.Add(item);
                }
                await _DailyCBMDetail_Temp_DA.InsertTemp(FinalCBMList_Temp);

                _LogRecord.Save("DailySettlement", "3.doInsertMoneySettleDetail_CBM", "CBM - " + FinalCBMList.Count.ToString());


                _LogRecord.Save("DailySettlement", "3.doInsertMoneySettleDetail_CBM", "end");


                //記錄錯誤
                List<DailyMoneySettleDetailError_Condition> ErrorList = new List<DailyMoneySettleDetailError_Condition>();

                foreach (var item in DailySettleToDoList.ErrorDataList)
                {
                    DailyMoneySettleDetailError_Condition Error = new DailyMoneySettleDetailError_Condition();

                    Error.RequestId = int.Parse(item.RequestId);
                    Error.CheckNumber = item.CheckNumber;
                    Error.CreateDate = DateTime.Now;

                    ErrorList.Add(Error);
                }
                _DailyMoneySettleDetailError_DA.Insert(ErrorList);

            }
            catch (Exception e)
            {
                _LogRecord.Save("DailySettlement", "3.doInsertMoneySettleDetail_CBM", "error - " + e.Message);
                //throw e;
            }



            //    _transactionScope.Complete();
            //}
        }

        public SpecialAreaManange GetAddressParsing(string Address, string CustomerCode, string ComeFrom, string Type = "1")
        {
            SpecialAreaManange Info = new SpecialAreaManange();
            var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetSpecialAreaDetail";

            //打API
            var client = new RestClient(AddressParsingURL);
            var request = new RestRequest(Method.POST);
            request.Timeout = 5000;
            request.ReadWriteTimeout = 5000;
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body

            var response = client.Post<ResData<SpecialAreaManange>>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                try
                {
                    Info = response.Data.Data;


                }
                catch (Exception e)
                {
                    throw e;

                }

            }
            else
            {
                //throw new Exception("地址解析有誤"); 
                Info = null;
            }

            return Info;
        }

        /// <summary>
        /// 取得CBM(長寬高加總換算)
        /// </summary>
        /// <param name="SumCBM"></param>
        /// <returns></returns>
        public string GetCBMSize(int? SumCBM)
        {
            string CBMSize = string.Empty;


            if (SumCBM < 1)
            {
                CBMSize = "S090";
            }
            else if (SumCBM <= 650)
            {
                CBMSize = "S060";
            }
            else if (SumCBM <= 950)
            {
                CBMSize = "S090";
            }
            else if (SumCBM <= 1150)
            {
                CBMSize = "S110";
            }
            else if (SumCBM <= 1250)
            {
                CBMSize = "S120";
            }
            else
            {
                CBMSize = "S150";
            }

            return CBMSize;
        }

        public int GetOverCBM(double OverCBM)
        {
            if (OverCBM > 1550)
            {
                int RetrunOverCBM = Convert.ToInt32(Math.Ceiling((OverCBM - 1550) / 100));
                return RetrunOverCBM;
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// 三邊換算CBM (mm)
        /// </summary>
        /// <param name="L"></param>
        /// <param name="W"></param>
        /// <param name="H"></param>
        /// <returns></returns>
        private string CalculateCBM(int L, int W, int H)
        {
            string all = "S060";
            int LWH = L + W + H;

            if (LWH != 0)
            {
                if (LWH <= 600) { all = "S060"; }
                if (LWH > 600 && LWH <= 900) { all = "S090"; }
                if (LWH > 900 && LWH <= 1100) { all = "S110"; }
                if (LWH > 1100 && LWH <= 1200) { all = "S120"; }
                if (LWH > 1200) { all = "S150"; }
            }
            return all;

        }

        /// <summary>
        /// 三邊換算CBM (mm) 多50處理
        /// </summary>
        /// <param name="L"></param>
        /// <param name="W"></param>
        /// <param name="H"></param>
        /// <returns></returns>
        private string CalculateCBMADD50(int L, int W, int H)
        {
            string all = "S060";
            int LWH = L + W + H;
            if (LWH != 0)
            {
                if (LWH <= 650) { all = "S060"; }
                if (LWH > 650 && LWH <= 950) { all = "S090"; }
                if (LWH > 950 && LWH <= 1150) { all = "S110"; }
                if (LWH > 1150 && LWH <= 1250) { all = "S120"; }
                if (LWH > 1250) { all = "S150"; }
            }
            return all;

        }

        /// <summary>
        /// 比較材積大小 左邊大於 =>True
        /// </summary>
        /// <param name="CBM1"></param>
        /// <param name="CBM2"></param>
        /// <returns></returns>
        private bool CheckCBMBig(string CBM1, string CBM2)
        {
            int w1 = 0;
            int w2 = 0;

            switch (CBM1)
            {
                case "b001":
                    w1 = 1;
                    break;
                case "b002":
                    w1 = 2;
                    break;
                case "b003":
                    w1 = 3;
                    break;
                case "b004":
                    w1 = 4;
                    break;
                case "S060":
                    w1 = 60;
                    break;
                case "S090":
                    w1 = 90;
                    break;
                case "S110":
                    w1 = 110;
                    break;
                case "S120":
                    w1 = 120;
                    break;
                case "S150":
                    w1 = 150;
                    break;
                default:
                    w1 = -1;
                    break;
            }

            switch (CBM2)
            {
                case "b001":
                    w2 = 1;
                    break;
                case "b002":
                    w2 = 2;
                    break;
                case "b003":
                    w2 = 3;
                    break;
                case "b004":
                    w2 = 4;
                    break;
                case "S060":
                    w2 = 60;
                    break;
                case "S090":
                    w2 = 90;
                    break;
                case "S110":
                    w2 = 110;
                    break;
                case "S120":
                    w2 = 120;
                    break;
                case "S150":
                    w2 = 150;
                    break;
                default:
                    w2 = -1;
                    break;
            }

            if (w1 == -1)
            {
                return false;
            }
            if (w2 == -1)
            {
                return true;
            }
            if (w1 > w2)
            {
                return true;
            }
            else
            {
                return false;

            }
        }

        //將有問題的CBM轉成正確的
        public string GetCBMTrans(string CBM)
        {

            var Pattern1 = "B0[0-9]{1}";
            var Pattern2 = "P0[0-9]{1}";
            var Pattern3 = "B[0-9]{1}";
            var Pattern4 = "O0[0-9]{1}";

            string FixCBM = string.Empty;

            if (Regex.Match(CBM, Pattern1).Success)
            {

                FixCBM = "B00" + CBM.Substring(2, 1);
            }
            else if (Regex.Match(CBM, Pattern2).Success)
            {
                FixCBM = "B00" + CBM.Substring(2, 1);
            }
            else if (Regex.Match(CBM, Pattern3).Success)
            {
                FixCBM = "X00" + CBM.Substring(1, 1);
            }
            else if (Regex.Match(CBM, Pattern4).Success)
            {
                FixCBM = "B00" + CBM.Substring(2, 1);
            }
            else
            {
                FixCBM = CBM;
            }

            return FixCBM;
        }

        /// <summary>
        /// CBM換算
        /// </summary>
        /// <param name="length"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public int GetCBMCulft(int? length, int? width, int? height)
        {

            int Culft = Convert.ToInt32(Math.Ceiling(
                                            (
                                                ((float)length) / 10.0 * ((float)width) / 10.0 * ((float)height) / 10.0
                                            ) / 27000.0
                                        ));
            return Culft;
        }
        /// <summary>
        /// 計算論件超材
        /// </summary>
        /// <param name="Spec"></param>
        /// <param name="MeasureScanCuft"></param>
        /// <returns></returns>
        public int GetOverCulft(string Spec, int MeasureScanCuft)
        {

            int SpecNumber = 0;

            int OverCuft = 0;

            //SpecNumber = Convert.ToInt32(Spec.Substring(3, 1));//客戶自己輸入的材積

            switch (Spec)
            {
                case "C001":
                    SpecNumber = 1;
                    break;
                case "C002":
                    SpecNumber = 2;
                    break;
                case "C003":
                    SpecNumber = 3;
                    break;
                case "C004":
                    SpecNumber = 4;
                    break;
                case "C005":
                    SpecNumber = 5;
                    break;
            }


            if (MeasureScanCuft > SpecNumber)
            {
                OverCuft = (MeasureScanCuft - SpecNumber); //丈量機量的比客戶自己填的大;
            }
            else if (SpecNumber > MeasureScanCuft)
            {
                OverCuft = 0; //客戶自己填的比丈量機大;
            }

            return OverCuft;
        }

        /// <summary>
        /// 修正B004，降階B003
        /// </summary>
        public string FixCBM(string spec)
        {
            return spec == "B004" ? "B003" : spec;
        }
    }
}
