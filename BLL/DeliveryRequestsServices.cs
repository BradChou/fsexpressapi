﻿using BLL.Model.ScheduleAPI.Res.DeliveryRequests;
using Common;
using Common.Model;
using DAL.DA;
using DAL.JunFuAddress_DA;
using DAL.Model.Condition;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;
using static Common.Setting.JobSetting;

namespace BLL
{
    public class DeliveryRequestsServices
    {
        private readonly IConfiguration _configuration;

        private ItcDeliveryRequests_DA _itcDeliveryRequests_DA;

        private IScheduleTask_DA _ScheduleTask_DA;

        private IAddressResult_DA _AddressResult_DA;

        private IMap8AddressDismantleV2_DA _Map8AddressDismantleV2_DA;


        private IUpdateSDMDJobLog_DA _UpdateSDMDJobLog_DA;
        public DeliveryRequestsServices
            (
            IConfiguration configuration,
             IScheduleTask_DA ScheduleTask_DA,
            ItcDeliveryRequests_DA itcDeliveryRequests_DA, IAddressResult_DA AddressResult_DA,
            IMap8AddressDismantleV2_DA Map8AddressDismantleV2_DA, IUpdateSDMDJobLog_DA UpdateSDMDJobLog_DA
            )
        {
            _configuration = configuration;
            _ScheduleTask_DA = ScheduleTask_DA;
            _itcDeliveryRequests_DA = itcDeliveryRequests_DA;
            _AddressResult_DA = AddressResult_DA;
            _Map8AddressDismantleV2_DA = Map8AddressDismantleV2_DA;
            _UpdateSDMDJobLog_DA = UpdateSDMDJobLog_DA;
        }


        /// <summary>
        ///  
        /// </summary>
        /// <returns></returns>
        public async Task DeliveryRequestsUpdateSDMDBYcheck_number(string check_number)
        {
            //檢查此貨號有沒有正常集貨

            var items = (await _itcDeliveryRequests_DA.GetInfoByCheckNumber(check_number));

            string CustomerCode = "F3500010002";

            foreach (var item in items)
            {
                if (item != null)
                {
                    try
                    {
                        string address = Tool.StrTrimer(item.receive_city + item.receive_area + item.receive_address);
                        string stationCode = string.Empty;
                        string stationName = string.Empty;
                        string salesDriverCode = string.Empty;
                        string motorcycleDriverCode = string.Empty;
                        string stackCode = string.Empty;
                        string shuttleStationCode = string.Empty;

                        string Map8city = string.Empty;
                        string Map8area = string.Empty;
                        string Map8name = string.Empty;

                        //地址解析

                        AddressParsingInfo _AddressParsingInfo = new AddressParsingInfo();
                        var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

                        //打API
                        var client = new RestClient(AddressParsingURL);
                        var request = new RestRequest(Method.POST);




                        request.RequestFormat = DataFormat.Json;
                        request.AddJsonBody(new { Address = address, type = "1", comeFrom = "6", customerCode = "F3500010002" }); // Anonymous type object is converted to Json body

                        try
                        {
                            var response = await client.PostAsync<ResData<AddressParsingInfo>>(request);

                            _AddressParsingInfo = response.Data == null ? null : response.Data;


                            if (_AddressParsingInfo != null)
                            {

                                stationCode = _AddressParsingInfo.StationCode;
                                salesDriverCode = _AddressParsingInfo.SalesDriverCode;
                                motorcycleDriverCode = _AddressParsingInfo.MotorcycleDriverCode;
                                shuttleStationCode = _AddressParsingInfo.ShuttleStationCode;
                                //紀錄結果

                                var di = await _Map8AddressDismantleV2_DA.GetInfoByAddr(_AddressParsingInfo.Address);

                                //記錄台灣圖霸解析

                                if (di != null)
                                {
                                    UpdateSDMDJobLog_Condition _c = new UpdateSDMDJobLog_Condition();

                                    _c.tcDeliveryRequestsId = item.request_id.ToString();
                                    _c.CustomerCode = CustomerCode;
                                    _c.Address = address;
                                    _c.ReceiveCode = stationCode;
                                    _c.ReceiveSD = salesDriverCode;
                                    _c.ReceiveMD = motorcycleDriverCode;
                                    _c.Map8City = di.city;
                                    _c.Map8Town = di.town;
                                    _c.Map8Name = di.name;

                                    await _UpdateSDMDJobLog_DA.Insert(_c);
                                }
                                //回寫主表
                                item.ReceiveCode = stationCode;
                                item.ReceiveSD = salesDriverCode;
                                item.ReceiveMD = motorcycleDriverCode;
                                item.ShuttleStationCode = shuttleStationCode;
                                item.udate = DateTime.Now;
                                item.uuser = "fsejob";
                                await _itcDeliveryRequests_DA.UpdateSDMD(item);

                            }
                        }
                        catch (Exception e)
                        {

                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
            }


         

        


        }

        /// <summary>
        ///  
        /// </summary>
        /// <returns></returns>
        public async Task<DeliveryRequestsUpdateSDMDJob_Res> DeliveryRequestsUpdateSDMD()
        {
            DeliveryRequestsUpdateSDMDJob_Res uploadPostOrderJob_Res = new DeliveryRequestsUpdateSDMDJob_Res();
            int SuccessCount = 0;
            try
            {

                //抓取上次最後執行
                //過濾
                var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.DeliveryRequestsUpdateSDMDJob)).FirstOrDefault();

                if (info == null)
                {
                    throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
                }
                var list = await _itcDeliveryRequests_DA.GetInfoListByrequestId(info.StartId.ToString());

                //先寫死客帶

                var CustomerCode = "F3500010002";

                list = list.Where(x => x.customer_code == CustomerCode);

                if (list != null && list.Any())
                {

                    var lastId = list.Max(p => p.request_id);

                    foreach (var item in list)
                    {
                        try
                        {
                            string address = Tool.StrTrimer(item.receive_city + item.receive_area + item.receive_address);
                            string stationCode = string.Empty;
                            string stationName = string.Empty;
                            string salesDriverCode = string.Empty;
                            string motorcycleDriverCode = string.Empty;
                            string stackCode = string.Empty;
                            string shuttleStationCode = string.Empty;

                            string Map8city = string.Empty;
                            string Map8area = string.Empty;
                            string Map8name = string.Empty;

                            //地址解析

                            AddressParsingInfo _AddressParsingInfo = new AddressParsingInfo();
                            var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

                            //打API
                            var client = new RestClient(AddressParsingURL);
                            var request = new RestRequest(Method.POST);




                            request.RequestFormat = DataFormat.Json;
                            request.AddJsonBody(new { Address = address, type = "1", comeFrom = "6", customerCode = "F3500010002" }); // Anonymous type object is converted to Json body

                            try
                            {
                                var response = await client.PostAsync<ResData<AddressParsingInfo>>(request);

                                _AddressParsingInfo = response.Data == null ? null : response.Data;


                                if (_AddressParsingInfo != null)
                                {

                                    stationCode = _AddressParsingInfo.StationCode;
                                    salesDriverCode = _AddressParsingInfo.SalesDriverCode;
                                    motorcycleDriverCode = _AddressParsingInfo.MotorcycleDriverCode;
                                    shuttleStationCode = _AddressParsingInfo.ShuttleStationCode;
                                    //紀錄結果

                                    var di = await _Map8AddressDismantleV2_DA.GetInfoByAddr(_AddressParsingInfo.Address);

                                    //記錄台灣圖霸解析

                                    if (di != null)
                                    {
                                        UpdateSDMDJobLog_Condition _c = new UpdateSDMDJobLog_Condition();

                                        _c.tcDeliveryRequestsId = item.request_id.ToString();
                                        _c.CustomerCode = CustomerCode;
                                        _c.Address = address;
                                        _c.ReceiveCode = stationCode;
                                        _c.ReceiveSD = salesDriverCode;
                                        _c.ReceiveMD = motorcycleDriverCode;
                                        _c.Map8City = di.city;
                                        _c.Map8Town = di.town;
                                        _c.Map8Name = di.name;

                                        await _UpdateSDMDJobLog_DA.Insert(_c);
                                    }
                                    //回寫主表
                                    item.ReceiveCode = stationCode;
                                    item.ReceiveSD = salesDriverCode;
                                    item.ReceiveMD = motorcycleDriverCode;
                                    item.ShuttleStationCode = shuttleStationCode;
                                    item.udate = DateTime.Now;
                                    item.uuser = "fsejob";
                                    await _itcDeliveryRequests_DA.UpdateSDMD(item);

                                }
                            }
                            catch (Exception e)
                            {

                            }
                        }
                        catch (Exception e)
                        {

                        }


                    }


                    info.StartId = long.Parse(lastId.ToString());
                    await _ScheduleTask_DA.UpdateSetStartId(info);
                }

                return uploadPostOrderJob_Res;
            }

            catch (MyException e)
            {
                return uploadPostOrderJob_Res;
            }

        }
    }
}
