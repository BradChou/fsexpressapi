﻿
using BLL.Model.FSExpressAPI.Req.Consignment;
using BLL.Model.FSExpressAPI.Res.Consignment;
using BLL.Model.ScheduleAPI.Res.Consignment;
using Common;
using DAL.DA;
using DAL.Model.Condition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;
using static Common.Setting.JobSetting;
using System.Security.Cryptography;
using System.Text;

namespace BLL
{
    public class AuthServices
    {
   
        private ItbAccounts_DA _tbAccounts_DA;



        public AuthServices(

            ItbAccounts_DA tbAccounts_DA
            )
        {

            _tbAccounts_DA = tbAccounts_DA;
        }

        public async Task<tbAccounts_Condition> GetAccountsInfo(string accountCode, string passWord)
        {

            MD5 md5 = MD5.Create();//建立一個MD5

            string md5passWord = GetMd5Hash(md5, passWord);

            return await _tbAccounts_DA.GetAccountsInfo(accountCode, md5passWord);

        }
        public static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }

}
