﻿using BLL.Model.AddressParsing.Req.Notification;
using BLL.Model.ScheduleAPI.Res.PostOffice;
using Common;
using DAL.Contract_DA;
using DAL.DA;
using DAL.Model.Condition;
using DAL.Model.Contract_Condition;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using static Common.Setting.EnumSetting;
using static Common.Setting.JobSetting;

namespace BLL
{
    public class NotificationServices
    {
  
        private readonly IConfiguration _configuration;
        private IScheduleTask_DA _ScheduleTask_DA;
        private INotification_DA _Notification_DA;
        private INotificationError_DA _NotificationError_DA;
        private ISMSInfo_DA _SMSInfo_DA;
        private ISMSUsing_DA _SMSUsing_DA;

        private ISMSFamilyNetlog_DA _SMSFamilyNetlog_DA;
        

        public NotificationServices
            (
            IConfiguration configuration,
            IScheduleTask_DA ScheduleTask_DA,
            INotification_DA Notification_DA,
             INotificationError_DA NotificationError_DA,
               ISMSInfo_DA SMSInfo_DA,
               ISMSUsing_DA SMSUsing_DA,
               ISMSFamilyNetlog_DA SMSFamilyNetlog_DA
            )
        {
            _configuration = configuration;
            _ScheduleTask_DA = ScheduleTask_DA;
            _Notification_DA = Notification_DA;
            _NotificationError_DA = NotificationError_DA;
            _SMSInfo_DA = SMSInfo_DA;
            _SMSUsing_DA = SMSUsing_DA;
            _SMSFamilyNetlog_DA = SMSFamilyNetlog_DA;
        }
        /// <summary>
        /// 全網加密方式
        /// </summary>
        /// <param name="data"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static string des64(string data, string key)
        {
            string iv = "1234567812345678";
            string m_key = key.PadRight(32, '0');

            byte[] encrypted;
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Encoding.UTF8.GetBytes(m_key);
                aesAlg.IV = Encoding.UTF8.GetBytes(iv);
                aesAlg.Mode = CipherMode.CBC;
                aesAlg.Padding = PaddingMode.PKCS7;
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(data);
                        }
                        encrypted = msEncrypt.ToArray();
                    }

                }

            }

            return Convert.ToBase64String(encrypted);



        }
        /// <summary>
        /// 寄簡訊
        /// </summary>
        /// <returns></returns>
        public async Task InsertSMS(SMSSend_Req req)
        {



            using (var _transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    SMSInfo_Condition sMSInfo_Condition = new SMSInfo_Condition();

                    sMSInfo_Condition.Status = "1";
                    sMSInfo_Condition.Title = req.Title;
                    sMSInfo_Condition.Content = req.Content;
                    sMSInfo_Condition.Mobile = req.Mobile;
                    sMSInfo_Condition.CreateUser = req.SendUser;
                    sMSInfo_Condition.UpdateUser = req.SendUser;
                    var Id = await _SMSInfo_DA.InsertReturnId(sMSInfo_Condition);

                    SMSUsing_Condition sMSUsing_Condition = new SMSUsing_Condition();
                    sMSUsing_Condition.SMSId = Id;
                    sMSUsing_Condition.ComeFrom = req.ComeFrom;
                    sMSUsing_Condition.CheckNumber = req.CheckNumber;
                    sMSUsing_Condition.CreateUser = req.SendUser;
                    await _SMSUsing_DA.Insert(sMSUsing_Condition);
                }
                catch (Exception)
                {

                    _transactionScope.Dispose();
                }

            

                _transactionScope.Complete();

            }
        }





        /// <summary>
        /// 寄送
        /// </summary>
        /// <returns></returns>
        public async Task SMSSend()
        {
            string APIURL = _configuration["SMS:APIURL"].ToString();
            string ContentType = _configuration["SMS:ContentType"].ToString();
            string Account = _configuration["SMS:Account"].ToString();
            string Password = _configuration["SMS:Password"].ToString();
            string NotifyType = _configuration["SMS:NotifyType"].ToString();
            var Pcode = des64(Account + DateTime.Now.ToString("yyyyMMddHHmmss"), Password);

            var SendList = await _SMSInfo_DA.GetNotSendList();

            foreach (var item in SendList)
            {
                DateTime StartTime = DateTime.Now;
                DateTime EndTime = DateTime.Now;
                SMSFamilyNetlog_Condition sMSFamilyNetlog_Condition = new SMSFamilyNetlog_Condition();

                try
                {

                    var guid = Guid.NewGuid().ToString("N");
                    string xml = string.Empty;

                    xml = xml + @"<?xml version=""1.0"" encoding=""utf-8""?>
<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">
  <soap12:Body>
    <SendSMSMessage xmlns=""http://tempuri.org/"">
      <request>&lt;DDSC&gt;
  &lt;Account&gt;" + Account + @"&lt;/Account&gt;
&lt;SubAccount&gt;&lt;/SubAccount&gt;
&lt;NotifyType&gt;" + NotifyType + @"&lt;/NotifyType&gt;
&lt;Pcode&gt;" + Pcode + @"&lt;/Pcode&gt;
&lt;Type&gt;C&lt;/Type&gt;
&lt;SeqNO&gt;" + guid + @"&lt;/SeqNO&gt;
&lt;ContactSet&gt;
&lt;Contact&gt;
&lt;ContactName&gt;&lt;/ContactName&gt;
&lt;ContactAddress&gt;" + item.Mobile + @"&lt;/ContactAddress&gt;
&lt;/Contact&gt;
&lt;/ContactSet&gt;
&lt;Title&gt;" + item.Title + @"&lt;/Title&gt;
&lt;Body&gt;" + item.Content + @"&lt;/Body&gt;
&lt;/DDSC&gt;</request>
    </SendSMSMessage>
  </soap12:Body>
</soap12:Envelope>"
        ;

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xml);
                    UTF8Encoding utf8 = new UTF8Encoding();

                    //Encoder utf8Encoder = Encoding.UTF8.GetEncoder();
                    //ASCIIEncoding encoding = new ASCIIEncoding();
                    string postData = doc.OuterXml;
                    sMSFamilyNetlog_Condition.RequestBody = postData;

                    byte[] data = utf8.GetBytes(postData);
                    // HTTP POST 請求物件
                    HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(APIURL);
                    httpWReq.Method = "POST";
                    httpWReq.ContentType = "application/soap+xml; charset=utf-8";
                    httpWReq.ContentLength = data.Length;
                    using (Stream stream = httpWReq.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                    //開始
                    StartTime = DateTime.Now;
                    HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                    //結束
                    EndTime = DateTime.Now;

                    string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();



                    var responseStringDecode = WebUtility.HtmlDecode(responseString);
                    sMSFamilyNetlog_Condition.ResponseBody = string.IsNullOrEmpty(responseString) ? string.Empty : responseString;

                    var str = XElement.Parse(responseStringDecode);
                    XNamespace ns = "http://tempuri.org/";
                    var StatusCode = str.Descendants(ns + "StatusCode");
                    var StatusDesc = str.Descendants(ns + "StatusDesc");
                    var RequestId = str.Descendants(ns + "RequestId");

                    // If you need a string result, use the .Value property:
                    var stringResult = StatusCode.First().Value;
                    var stringResult2 = StatusDesc.First().Value;
                    var stringResult3 = RequestId.First().Value;


                    sMSFamilyNetlog_Condition.StatusCode = string.IsNullOrEmpty(stringResult) ? string.Empty: stringResult;
                    sMSFamilyNetlog_Condition.StatusDesc = string.IsNullOrEmpty(stringResult2) ? string.Empty : stringResult2;
                    sMSFamilyNetlog_Condition.RequestId = string.IsNullOrEmpty(stringResult3) ? string.Empty : stringResult3;
            

                    if (stringResult.Equals("000"))
                    {
                        item.UpdateDate = DateTime.Now;
                        item.UpdateUser = "JOB";
                        item.Status = "2";
                        await _SMSInfo_DA.Update(item);
                    }
                    else
                    {
                        item.UpdateDate = DateTime.Now;
                        item.UpdateUser = "JOB";
                        item.Status = "99";
                        await _SMSInfo_DA.Update(item);
                    }
                }
                catch (Exception)
                {

                    item.UpdateDate = DateTime.Now;
                    item.UpdateUser = "JOB";
                    item.Status = "99";
                    await _SMSInfo_DA.Update(item);


                }



                sMSFamilyNetlog_Condition.StartTime = StartTime;
                sMSFamilyNetlog_Condition.EndTime = EndTime;

                await _SMSFamilyNetlog_DA.Insert(sMSFamilyNetlog_Condition);
            }

        }

        /// <summary>
        /// 寄送
        /// </summary>
        /// <returns></returns>
        public async Task Send()
        {
            //抓推送資訊
            var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.SendJob)).FirstOrDefault();

            if (info == null)
            {
                throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            }
            var GetInfoById = await _Notification_DA.GetInfoById(info.StartId);

            if (GetInfoById != null && GetInfoById.Count() > 0)
            {
                var lastId = GetInfoById.Max(p => p.id);

                foreach (var _info in GetInfoById)
                {
                    NotificationError_Condition notificationError_Condition = _info;

                    try
                    {
                        switch (_info.Type)
                        {
                            case 1: //notify
                                notify(_info);
                                break;

                            case 2: //Email


                                break;
                            default:
                                break;
                        }
                    }
                    catch (MyException e)
                    {
                        //未知錯誤
                        notificationError_Condition.ErrMsg = e.Message;
                        notificationError_Condition.Status = (int)ResponseCodeEnum.NotificationError;

                        await _NotificationError_DA.Insert(notificationError_Condition);
                        //ErrorCount++;
                    }
                }
                info.StartId = lastId;
                await _ScheduleTask_DA.UpdateSetStartId(info);

            }



        }

        private void notify(Notification_Condition data)
        {
            try
            {
                var _apiURL = _configuration["LineNotify:URL"];
                var _authorization = data.Recipient;
                var Subject = data.Subject;
                var Body = data.Body;

                var Message = "[" + Subject + "]" + " " + Body;

                //打API
                var client = new RestClient(_apiURL);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", _authorization);
                request.AddParameter("message", Message);
                var response = client.Post<object>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {

                    if (response.Data != null)
                    {

                    }
                    else
                    {

                    }
                }
                else
                {
                    throw new MyException(ResponseCodeEnum.LineNotifyError);
                }

            }
            catch (Exception e)
            {

                throw new MyException(ResponseCodeEnum.LineNotifyError);
            }

        }


    }
}
