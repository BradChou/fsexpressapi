﻿using BLL.Model.AddressParsing.Model;
using BLL.Model.AddressParsing.Req.Address;
using BLL.Model.AddressParsing.Res.Address;
using BLL.Model.FSExpressAPI.Model;
using Common;
using Common.Redis;
using DAL;
using DAL.Contract_DA;
using DAL.DA;
using DAL.JunFuAddress_DA;
using DAL.JunFuTrans_DA;
using DAL.Model.Contract_Condition;
using DAL.Model.JunFuAddress_Condition;
using DAL.Model.JunFuTrans_Condition;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.International.Converters.TraditionalChineseToSimplifiedConverter;
using Newtonsoft.Json;
using RestSharp;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;

namespace BLL
{
    public class AddressServices
    {
        private readonly IMemoryCache _memoryCache;

        private readonly IConfiguration _configuration;
        private IorgArea_DA _orgArea_DA;
        private IStoredProcedure_DA _storedProcedure_DA;
        private IAddressResult_DA _addressResult_DA;
        private IJunfuAddressDismantle_DA _junfuAddressDismantle_DA;
        private IMap8AddressDismantle_DA _map8AddressDismantle_DA;
        private IMap8AddressDismantleV2_DA _map8AddressDismantleV2_DA;
        private ISpecialAreaManage_DA _specialAreaManage_DA;


        private IAddressUsingLog_DA _addressUsingLog_DA;
        private ItbCustomers_DA _itbCustomers_DA;
        private IRoad_ZipCodeResult_DA _road_ZipCodeResult_DA;
        private ISpecialAreaResult_DA _specialAreaResult_DA;


        private AddressServices_itri _addressServices_Itri;

        private AddressResult_Condition addressResult_Condition = new AddressResult_Condition();

        private string PostZip3 = string.Empty;
        private string PostZip32 = string.Empty;
        private string PostZip33 = string.Empty;
        private string Address = string.Empty;
        private string City = string.Empty;
        private string Town = string.Empty;
        private string RoadName = string.Empty;
        private string Road = string.Empty;
        private string StationCode = string.Empty;
        private string StationName = string.Empty;
        private string SalesDriverCode = string.Empty;
        private string MotorcycleDriverCode = string.Empty;
        private string StackCode = string.Empty;
        private string ShuttleStationCode = string.Empty;
        private string SendSalesDriverCode = string.Empty;
        private string SendMotorcycleDriverCode = string.Empty;

        private GetDetail_Req _detail_Req = new GetDetail_Req();
        private orgArea_Condition JForgAreaOrgArea = new orgArea_Condition();
        private orgArea_Condition Map8orgAreaOrgArea = new orgArea_Condition();

        private List<orgArea_Condition> _orgAreaList = new List<orgArea_Condition>();
        private List<SpecialAreaManage_Condition> _specialAreaList = new List<SpecialAreaManage_Condition>();

        public AddressServices
         (
            IConfiguration configuration,
            IorgArea_DA orgArea_DA,
            IStoredProcedure_DA storedProcedure_DA,
            IAddressResult_DA addressResult_DA,
            IJunfuAddressDismantle_DA junfuAddressDismantle_DA,
            IMap8AddressDismantle_DA map8AddressDismantle_DA,
            IMap8AddressDismantleV2_DA map8AddressDismantleV2_DA,
            ISpecialAreaManage_DA specialAreaManage_DA,
            IAddressUsingLog_DA addressUsingLog_DA,
            ISpecialAreaResult_DA specialAreaResult_DA,
            ItbCustomers_DA tbCustomers_DA,
            AddressServices_itri addressServices_Itri,
            IRoad_ZipCodeResult_DA road_ZipCodeResult_DA,
            IMemoryCache memoryCache
          )
        {
            _configuration = configuration;
            _orgArea_DA = orgArea_DA;
            _storedProcedure_DA = storedProcedure_DA;
            _addressResult_DA = addressResult_DA;
            _junfuAddressDismantle_DA = junfuAddressDismantle_DA;
            _map8AddressDismantle_DA = map8AddressDismantle_DA;
            _map8AddressDismantleV2_DA = map8AddressDismantleV2_DA;
            _specialAreaManage_DA = specialAreaManage_DA;
            _specialAreaResult_DA = specialAreaResult_DA;
            _addressUsingLog_DA = addressUsingLog_DA;
            _itbCustomers_DA = tbCustomers_DA;
            _addressServices_Itri = addressServices_Itri;
            _road_ZipCodeResult_DA = road_ZipCodeResult_DA;

            _memoryCache = memoryCache;
        }
        private async Task<ParseAddress_itri_Model> Map8SDMD2(string Type, string ComeFrom, string CustomerCode)
        {

            //     var tModel = _addressServices_Itri.GetMap8_2AndSave(true, _detail_Req.Address, "", ComeFrom, CustomerCode);
            Map8 map8_all = new Map8();

            //Map8Result map8 = new Map8Result();
            //map8.Type_ = Type;
            //map8.CustomerCode = CustomerCode;
            //map8.ComeFrom = ComeFrom;
            //map8.InitialAddress = fullAddr;


            var fullAddrPre = _detail_Req.Address;

            var Map8V2 = await GetMap8ResultAPI2(fullAddrPre);

            if (CustomerCode.Equals("F3500010002"))
            {

            }
            else
            {
                if (fullAddrPre.IndexOf(Map8V2.town) > -1 && fullAddrPre.IndexOf("區") > -1)
                {
                    if (fullAddrPre.IndexOf(Map8V2.city) > -1)
                    {

                    }
                    else
                    {

                        Map8V2.city = fullAddrPre.Substring(0, 3);
                    }

                }
                else
                {
                    Map8V2.town = fullAddrPre.Substring(3, 3);
                    if (Map8V2.town.EndsWith("區") && Map8V2.town.Length == 3)
                    {
                        Map8V2.town = Map8V2.town.Substring(0, Map8V2.town.Length - 1);
                    }
                }
            }

            if (Map8V2 != null && !string.IsNullOrEmpty(Map8V2.city) && !string.IsNullOrEmpty(Map8V2.town) && !string.IsNullOrEmpty(Map8V2.name))
            {
                //清理髒東西
                //var fullAddr = _addressServices_Itri.ReplaceDirtyData(_detail_Req.Address);
                var fullAddr = Map8V2.city + Map8V2.town + Map8V2.name;
                ParseAddress_itri_Model tModel = _addressServices_Itri.GetZipCodeAndSave_Main(fullAddr, Type, ComeFrom, CustomerCode);

                if (!string.IsNullOrEmpty(tModel.ZipCode))
                {
                    switch (tModel.ZipCode.Length)
                    {
                        case 3:
                            PostZip3 = tModel.ZipCode;
                            break;
                        case 5:
                            PostZip32 = tModel.ZipCode;
                            break;
                        case 6:
                            PostZip33 = tModel.ZipCode;
                            break;
                        default:
                            break;
                    }
                    City = string.IsNullOrEmpty(City) ? Map8V2.city : City;
                    Town = string.IsNullOrEmpty(Town) ? Map8V2.town : Town;
                    Road = string.IsNullOrEmpty(Road) ? Map8V2.name : Road;

                    addressResult_Condition.Map8_city = Map8V2.city;
                    addressResult_Condition.Map8_town = Map8V2.town;
                    addressResult_Condition.Map8_road = Map8V2.name;
                }
                //Address = fullAddr.Trim().Replace(" ", "");
                //City = string.IsNullOrEmpty(City) ? Map8V2.city : City;
                //Town = string.IsNullOrEmpty(Town) ? Map8V2.town : Town;
                //StationCode = string.IsNullOrEmpty(tModel.StationCode) ? string.Empty : tModel.StationCode;
                //StationName = string.IsNullOrEmpty(tModel.StationName) ? string.Empty : tModel.StationName;
                //SalesDriverCode = string.IsNullOrEmpty(tModel.SalesDriverCode) ? string.Empty : tModel.SalesDriverCode;
                //MotorcycleDriverCode = string.IsNullOrEmpty(tModel.MotorcycleDriverCode) ? string.Empty : tModel.MotorcycleDriverCode;
                //StackCode = string.IsNullOrEmpty(tModel.StackCode) ? string.Empty : tModel.StackCode;
                //ShuttleStationCode = string.IsNullOrEmpty(tModel.ShuttleStationCode) ? string.Empty : tModel.ShuttleStationCode;

                //SendSalesDriverCode = string.IsNullOrEmpty(SendSalesDriverCode) ? tModel.SendSD : SendSalesDriverCode;
                //SendMotorcycleDriverCode = string.IsNullOrEmpty(SendMotorcycleDriverCode) ? tModel.SendMD : SendMotorcycleDriverCode;

                return tModel;
            }
            else
            {

                var fullAddr = _addressServices_Itri.ReplaceDirtyData(_detail_Req.Address);
                ParseAddress_itri_Model tModel = _addressServices_Itri.GetZipCodeAndSave_Main(fullAddr, Type, ComeFrom, CustomerCode);

                if (!string.IsNullOrEmpty(tModel.ZipCode))
                {
                    switch (tModel.ZipCode.Length)
                    {
                        case 3:
                            PostZip3 = tModel.ZipCode;
                            break;
                        case 5:
                            PostZip32 = tModel.ZipCode;
                            break;
                        case 6:
                            PostZip33 = tModel.ZipCode;
                            break;
                        default:
                            break;
                    }

                }

                return tModel;
            }








            //DateTime tNow = DateTime.Now;

            //var Map8Key = _configuration["Map8:Key"];
            //var Map8RRL = _configuration["Map8:URL_2"];

            ////打API
            //var client = new RestClient(Map8RRL);
            //var request = new RestRequest(Method.GET);
            //request.AddParameter("address", fullAddr.Trim().Replace(" ", ""));
            //request.AddParameter("key", Map8Key);
            //var restResponse = client.Get<object>(request);

            //if (restResponse.StatusCode == HttpStatusCode.OK)
            //{
            //    map8_all = JsonConvert.DeserializeObject<Map8>(restResponse.Content);

            //    if (map8_all.results != null && map8_all.results.Count() > 0)
            //    {


            //        var item = map8_all.results[0];

            //        //存API2結果



            //        if (!string.IsNullOrEmpty(item.city))
            //        {
            //            /*
            //            6	縣市
            //            5	鄉鎮市區
            //            4	路、路段
            //            3	巷
            //            2	弄
            //            1	號、之號
            //            0	無法定位
            //            -1	同號，"之號"內插，如10-3及10-8，內插出10-5的坐標
            //            -2	同單雙號內插，如 4號及10號內插成8號，或3號、11號內插出7號
            //            -3	不同單雙號內插，即4號、11號內插6號
            //            -4	同號、不同之號的代表點，如以10-3的坐標當做10-5的坐標
            //            -5	同單雙號的代表點，如以 4號的坐標當做10號的坐標
            //            -6	不同單雙號的代表點，如以 4號的坐標當做7號的坐標
            //            -7	同樣路名的其中一點，其他狀況，以同路名的第一門牌坐標為代表
            //            fuzzy	地址定位結果乃透過模糊搜尋結果而得
            //            */
            //            string levelstring = "123456";
            //            if (levelstring.IndexOf(item.level) >= 0)
            //            {

            //            }

            //            item.Type_ = Type;
            //            item.CustomerCode = CustomerCode;
            //            item.ComeFrom = ComeFrom;
            //            item.InitialAddress = fullAddr;
            //            item.location_lng = item.geometry.location.lng;
            //            item.location_lat = item.geometry.location.lat;
            //            item.json = JsonConvert.SerializeObject(item);
            //            item.CreateTime = tNow;


            //            ParseAddress_itri_Model tModel = _addressServices_Itri.GetZipCodeAndSave_Main(fullAddr, Type, ComeFrom, CustomerCode);

            //            item.ZipCode = tModel.ZipCode;
            //            item.StationCode = tModel.StationCode;
            //            item.StationName = tModel.StationName;
            //            item.SalesDriverCode = tModel.SalesDriverCode;
            //            item.MotorcycleDriverCode = tModel.MotorcycleDriverCode;
            //            item.StackCode = tModel.StackCode;
            //            item.SeqNO = tModel.SeqNO;
            //            item.InitialAddressPre = fullAddrPre;
            //            item.ShuttleStationCode = tModel.ShuttleStationCode;
            //            _road_ZipCodeResult_DA.SaveAddress_Map8_2(item);

            //            if (!string.IsNullOrEmpty(tModel.ZipCode))
            //            {
            //                switch (tModel.ZipCode.Length)
            //                {
            //                    case 3:
            //                        PostZip3 = tModel.ZipCode;
            //                        break;
            //                    case 5:
            //                        PostZip32 = tModel.ZipCode;
            //                        break;
            //                    case 6:
            //                        PostZip33 = tModel.ZipCode;
            //                        break;
            //                    default:
            //                        break;
            //                }

            //            }
            //            Address = fullAddr.Trim().Replace(" ", "");
            //            City = string.IsNullOrEmpty(City) ? item.city : City;
            //            Town = string.IsNullOrEmpty(Town) ? item.town : Town;
            //            StationCode = string.IsNullOrEmpty(tModel.StationCode) ? string.Empty : tModel.StationCode;
            //            StationName = string.IsNullOrEmpty(tModel.StationName) ? string.Empty : tModel.StationName;
            //            SalesDriverCode = string.IsNullOrEmpty(tModel.SalesDriverCode) ? string.Empty : tModel.SalesDriverCode;
            //            MotorcycleDriverCode = string.IsNullOrEmpty(tModel.MotorcycleDriverCode) ? string.Empty : tModel.MotorcycleDriverCode;
            //            StackCode = string.IsNullOrEmpty(tModel.StackCode) ? string.Empty : tModel.StackCode;
            //            ShuttleStationCode = string.IsNullOrEmpty(tModel.ShuttleStationCode) ? string.Empty : tModel.ShuttleStationCode;

            //            SendSalesDriverCode = string.IsNullOrEmpty(SendSalesDriverCode) ? tModel.SendSD : SendSalesDriverCode;
            //            SendMotorcycleDriverCode = string.IsNullOrEmpty(SendMotorcycleDriverCode) ? tModel.SendMD : SendMotorcycleDriverCode;
            //        }
            //    }

            //}
            //else
            //{
            //    string ERROR = restResponse.Content;

            //    if (string.IsNullOrEmpty(restResponse.Content))
            //    {
            //        ERROR = restResponse.StatusCode.ToString();
            //    }
            //    Map8Result map8Result = new Map8Result();
            //    map8Result.InitialAddress = _detail_Req.Address;
            //    map8Result.formatted_address = ERROR;
            //    map8Result.InitialAddressPre = fullAddrPre;
            //    _road_ZipCodeResult_DA.SaveAddress_Map8_2(map8Result);

            //}

            return null;
        }
        private async Task<orgArea_Condition> JFSDMD()
        {
            //Junfu拆地址
            var JunfuResult = await ParsingAddress(_detail_Req.Address);


            if (JunfuResult != null)
            {
                addressResult_Condition.JF_address = JunfuResult.address;
                addressResult_Condition.JF_city = JunfuResult.city;
                addressResult_Condition.JF_area = JunfuResult.area;
                addressResult_Condition.JF_village = JunfuResult.village;
                addressResult_Condition.JF_road = JunfuResult.road;
                addressResult_Condition.JF_lane = JunfuResult.lane;
                addressResult_Condition.JF_alley = JunfuResult.alley;
                addressResult_Condition.JF_sub_alley = JunfuResult.sub_alley;
                addressResult_Condition.JF_neig = JunfuResult.neig;
                addressResult_Condition.JF_no = JunfuResult.no;

                City = JunfuResult.city;
                Town = JunfuResult.area;
                //SDMD
                FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(_detail_Req.Address, JunfuResult.city, JunfuResult.area);

                var JForgArea = (await GetByFSEAddressEntity(fSEAddressEntity)).FirstOrDefault();

                if (JForgArea != null)
                {

                    City = string.IsNullOrEmpty(City) ? JForgArea.CITY : City;
                    Town = string.IsNullOrEmpty(Town) ? JForgArea.AREA : Town;
                    Road = string.IsNullOrEmpty(Road) ? JForgArea.ROAD : Road;
                    Address = City + Town + JunfuResult.address;
                    PostZip3 = string.IsNullOrEmpty(PostZip3) ? JForgArea.ZIP3A : PostZip3;
                    switch (JForgArea.ZIPCODE.Length)
                    {
                        case 5:
                            PostZip32 = string.IsNullOrEmpty(PostZip32) ? JForgArea.ZIPCODE : PostZip32;
                            break;
                        case 6:
                            PostZip33 = string.IsNullOrEmpty(PostZip33) ? JForgArea.ZIPCODE : PostZip33;
                            break;
                        default:
                            break;
                    }
                    StationCode = string.IsNullOrEmpty(StationCode) ? JForgArea.station_scode : StationCode;
                    StationName = string.IsNullOrEmpty(StationName) ? JForgArea.station_name : StationName;
                    SalesDriverCode = string.IsNullOrEmpty(SalesDriverCode) ? JForgArea.sd_no : SalesDriverCode;
                    MotorcycleDriverCode = string.IsNullOrEmpty(MotorcycleDriverCode) ? JForgArea.md_no : MotorcycleDriverCode;
                    StackCode = string.IsNullOrEmpty(StackCode) ? JForgArea.put_order : StackCode;
                    ShuttleStationCode = string.IsNullOrEmpty(ShuttleStationCode) ? JForgArea.ShuttleStationCode : ShuttleStationCode;
                    SendSalesDriverCode = string.IsNullOrEmpty(SendSalesDriverCode) ? JForgArea.SendSD : SendSalesDriverCode;
                    SendMotorcycleDriverCode = string.IsNullOrEmpty(SendMotorcycleDriverCode) ? JForgArea.SendMD : SendMotorcycleDriverCode;

                }
                return JForgArea;
            }
            return null;
        }
        private async Task<orgArea_Condition> Map8SDMD()
        {
            var Map8ResultTask = await GetMap8Result(_detail_Req.Address);
            if (Map8ResultTask != null && Map8ResultTask.Count() > 0)
            {
                //台灣圖霸拆地址
                var Map8ResultList = Map8ResultTask;

                if (Map8ResultList != null && Map8ResultList.Count() != 0)
                {
                    bool check = false;

                    var Map8Result = Map8ResultList[0];

                    //確認至少有 市 縣

                    if (_detail_Req.Address.IndexOf("市") > -1 || _detail_Req.Address.IndexOf("縣") > -1)
                    {
                        if (CheckMap8Result(Map8Result, _detail_Req.Address, 3)) //1=縣市 // 2 =鄉市鎮區 // 3 = 2不看鄉市鎮區 名字對就好
                        {
                            check = true;
                        }
                    }

                    if (check)//1=縣市 // 2 =鄉市鎮區
                    {
                        Address = _detail_Req.Address;
                        addressResult_Condition.Map8_formatted_address = Map8Result.formatted_address;
                        addressResult_Condition.Map8_city = Map8Result.city;
                        addressResult_Condition.Map8_town = Map8Result.town;
                        addressResult_Condition.Map8_village = Map8Result.village;
                        addressResult_Condition.Map8_lin = Map8Result.lin;
                        addressResult_Condition.Map8_road = Map8Result.road;
                        addressResult_Condition.Map8_hamlet = Map8Result.hamlet;
                        addressResult_Condition.Map8_lane = Map8Result.lane;
                        addressResult_Condition.Map8_alley = Map8Result.alley;
                        addressResult_Condition.Map8_lon = Map8Result.lon;
                        addressResult_Condition.Map8_num = Map8Result.num;
                        addressResult_Condition.Map8_floor = Map8Result.floor;
                        addressResult_Condition.Map8_numAttr = Map8Result.numAttr;

                        if (!string.IsNullOrEmpty(Map8Result.postcode3))
                        {
                            PostZip3 = Map8Result.postcode3;
                        }
                        if (!string.IsNullOrEmpty(Map8Result.postcode33))
                        {
                            PostZip33 = Map8Result.postcode33;
                        }
                        var Map8orgArea = await GetSDMDBYorgArea(Map8Result);
                        if (Map8orgArea != null)
                        {
                            City = string.IsNullOrEmpty(Map8orgArea.CITY) ? City : Map8orgArea.CITY;
                            Town = string.IsNullOrEmpty(Map8orgArea.AREA) ? Town : Map8orgArea.AREA;
                            Road = string.IsNullOrEmpty(Map8orgArea.ROAD) ? Road : Map8orgArea.ROAD;
                            PostZip3 = string.IsNullOrEmpty(Map8orgArea.ZIP3A) ? PostZip3 : Map8orgArea.ZIP3A;

                            var ZIPCODELength = string.IsNullOrEmpty(Map8orgArea.ZIPCODE) ? 0 : Map8orgArea.ZIPCODE.Length;

                            switch (ZIPCODELength)
                            {
                                case 5:
                                    PostZip32 = Map8orgArea.ZIPCODE;
                                    break;
                                case 6:
                                    PostZip33 = Map8orgArea.ZIPCODE;
                                    break;
                                default:
                                    break;
                            }

                            StationCode = string.IsNullOrEmpty(Map8orgArea.station_scode) ? StationCode : Map8orgArea.station_scode;
                            StationName = string.IsNullOrEmpty(Map8orgArea.station_name) ? StationName : Map8orgArea.station_name;
                            SalesDriverCode = string.IsNullOrEmpty(Map8orgArea.sd_no) ? SalesDriverCode : Map8orgArea.sd_no;
                            MotorcycleDriverCode = string.IsNullOrEmpty(Map8orgArea.md_no) ? MotorcycleDriverCode : Map8orgArea.md_no;
                            StackCode = string.IsNullOrEmpty(Map8orgArea.put_order) ? StackCode : Map8orgArea.put_order;
                        }
                        return Map8orgArea;
                    }

                }

            }
            else
            {


            }

            return null;
        }

        /// <summary>
        /// 詳細資訊
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<GetDetail_Res> GetDetail(GetDetail_Req req)
        {
            _detail_Req = req;
            //檢查客代
            var CustomersInfo = await _itbCustomers_DA.GetInfoByCustomerCode(_detail_Req.CustomerCode);
            if (CustomersInfo == null)
            {
                throw new MyException(ResponseCodeEnum.CustomerNotFound);
            }
            GetDetail_Res getDetail_Res = new GetDetail_Res();
            long AddressResultId = 0;

            AddressUsingLog_Condition addressUsingLog_Condition = new AddressUsingLog_Condition();
            addressUsingLog_Condition.ComeFrom = _detail_Req.ComeFrom;
            addressUsingLog_Condition.CustomerCode = _detail_Req.CustomerCode;
            addressResult_Condition.InitialAddress = _detail_Req.Address;

            var ExpiredDay = int.TryParse(_configuration["ExpiredDay"], out int d) ? d : 0;

            //測試與補救資料就不用看時間
            try
            {
                var st = (AddressParsingComeFromEnum)Enum.Parse(typeof(AddressParsingComeFromEnum), _detail_Req.ComeFrom, true);
                if (st == AddressParsingComeFromEnum.Test)
                {
                    ExpiredDay = 0;
                }
            }
            catch (Exception)
            {

            }
            DateTime Now = DateTime.Now;
            DateTime SNow = Now.AddDays(ExpiredDay * -1);

            //string orgArea_DA_All = _redis.StringGet("orgArea_DA_All");

            //if (string.IsNullOrEmpty(orgArea_DA_All))
            //{
            //    _orgAreaList = (await _orgArea_DA.All()).ToList();

            //}
            //else 
            //{
            //    var descJsonStu = JsonConvert.DeserializeObject<List<orgArea_Condition>>(orgArea_DA_All);//反序列化
            //    _orgAreaList = descJsonStu;
            //}


            var cacheKey = "orgArea_DA_All";
            //checks if cache entries exists
            if (!_memoryCache.TryGetValue(cacheKey, out _orgAreaList))
            {
                //calling the server
                _orgAreaList = (await _orgArea_DA.All()).ToList();
                //setting up cache options
                var cacheExpiryOptions = new MemoryCacheEntryOptions
                {
                    AbsoluteExpiration = DateTime.Now.AddSeconds(3600),   // 絕對過期時間：時間到後就會消失
                    Priority = CacheItemPriority.High,  // 優先權：資源吃緊時，會先釋放優先權較低的資料
                                                        //  SlidingExpiration = TimeSpan.FromSeconds(60) // 相對過期時間：每次讀取值後會重新計算過期時間

                }.RegisterPostEvictionCallback((key, value, reason, state) =>   // 資料消失時的回呼函式
                {
                    //logger.LogDebug($"快取鍵值: {key} 已消失。原因: {reason}");
                });
                //setting cache entries
                _memoryCache.Set(cacheKey, _orgAreaList, cacheExpiryOptions);
            }


            //   _orgAreaList = (await _orgArea_DA.All()).ToList();


            //時間一樣直接跑
            if (ExpiredDay == 0)
            {
                var JForgArea = JFSDMD();
                var Map8V2 = Map8SDMD2(req.Type, req.ComeFrom, req.CustomerCode);
                //  var Map8orgArea = Map8SDMD();
                JForgArea.Wait();
                Map8V2.Wait();
                //  Map8orgArea.Wait();
                var tModel = Map8V2.Result;
                if (tModel != null)
                {

                    Address = _detail_Req.Address.Trim().Replace(" ", "");
                    City = string.IsNullOrEmpty(tModel.CityName) ? City : tModel.CityName;
                    Town = string.IsNullOrEmpty(tModel.AreaName) ? Town : tModel.AreaName;
                    RoadName = string.IsNullOrEmpty(tModel.RoadName) ? RoadName : tModel.RoadName;
                    StationCode = string.IsNullOrEmpty(tModel.StationCode) ? StationCode : tModel.StationCode;
                    StationName = string.IsNullOrEmpty(tModel.StationName) ? StationName : tModel.StationName;
                    SalesDriverCode = string.IsNullOrEmpty(tModel.SalesDriverCode) ? SalesDriverCode : tModel.SalesDriverCode;
                    MotorcycleDriverCode = string.IsNullOrEmpty(tModel.MotorcycleDriverCode) ? MotorcycleDriverCode : tModel.MotorcycleDriverCode;
                    StackCode = string.IsNullOrEmpty(tModel.StackCode) ? StackCode : tModel.StackCode;
                    ShuttleStationCode = string.IsNullOrEmpty(tModel.ShuttleStationCode) ? ShuttleStationCode : tModel.ShuttleStationCode;
                    SendSalesDriverCode = string.IsNullOrEmpty(tModel.SendSD) ? SendSalesDriverCode : tModel.SendSD;
                    SendMotorcycleDriverCode = string.IsNullOrEmpty(tModel.SendMD) ? SendMotorcycleDriverCode : tModel.SendMD;
                }

            }
            else
            {


                var JForgArea = JFSDMD();
                var Map8V2 = Map8SDMD2(req.Type, req.ComeFrom, req.CustomerCode);
                //  var Map8orgArea = Map8SDMD();
                JForgArea.Wait();
                Map8V2.Wait();

                var tModel = Map8V2.Result;
                if (tModel != null)
                {
                    Address = _detail_Req.Address.Trim().Replace(" ", "");
                    City = string.IsNullOrEmpty(tModel.CityName) ? City : tModel.CityName;
                    Town = string.IsNullOrEmpty(tModel.AreaName) ? Town : tModel.AreaName;
                    StationCode = string.IsNullOrEmpty(tModel.StationCode) ? StationCode : tModel.StationCode;
                    StationName = string.IsNullOrEmpty(tModel.StationName) ? StationName : tModel.StationName;
                    SalesDriverCode = string.IsNullOrEmpty(tModel.SalesDriverCode) ? SalesDriverCode : tModel.SalesDriverCode;
                    MotorcycleDriverCode = string.IsNullOrEmpty(tModel.MotorcycleDriverCode) ? MotorcycleDriverCode : tModel.MotorcycleDriverCode;
                    StackCode = string.IsNullOrEmpty(tModel.StackCode) ? StackCode : tModel.StackCode;
                    ShuttleStationCode = string.IsNullOrEmpty(tModel.ShuttleStationCode) ? ShuttleStationCode : tModel.ShuttleStationCode;
                    SendSalesDriverCode = string.IsNullOrEmpty(tModel.SendSD) ? SendSalesDriverCode : tModel.SendSD;
                    SendMotorcycleDriverCode = string.IsNullOrEmpty(tModel.SendMD) ? SendMotorcycleDriverCode : tModel.SendMD;
                }
            }





            //找不到站所 有其他地址
            if (string.IsNullOrEmpty(StationCode) && !string.IsNullOrEmpty(City) && !string.IsNullOrEmpty(Town))
            {

                var OrgAreasList = _orgAreaList;

                var orgArea32sp = from orgArea
                                  in OrgAreasList
                                  where
                                  orgArea.ZIPCODE.Length == 5 &&
                                  orgArea.CITY.Equals(City) &&
                                  orgArea.AREA.Equals(Town)
                                  select orgArea;

                var one = orgArea32sp
                       .Select(m => new
                       {
                           ZIP3A = string.IsNullOrEmpty(m.ZIP3A) ? string.Empty : m.ZIP3A,
                           station_name = string.IsNullOrEmpty(m.station_name) ? string.Empty : m.station_name,
                           station_scode = string.IsNullOrEmpty(m.station_scode) ? string.Empty : m.station_scode,
                       })
                       .Distinct()
                       .ToList().FirstOrDefault();
                if (one != null)
                {
                    StationName = one.station_name;
                    StationCode = one.station_scode;
                    PostZip3 = one.ZIP3A;
                }
                else
                {
                    City = string.Empty;
                    Town = string.Empty;
                }

            }


            addressResult_Condition.PostZip3 = PostZip3;
            addressResult_Condition.PostZip32 = PostZip32;
            addressResult_Condition.PostZip33 = PostZip33;
            addressResult_Condition.StationCode = StationCode;
            addressResult_Condition.StationName = StationName;
            addressResult_Condition.SalesDriverCode = SalesDriverCode;
            addressResult_Condition.MotorcycleDriverCode = MotorcycleDriverCode;
            addressResult_Condition.StackCode = StackCode;
            addressResult_Condition.ShuttleStationCode = ShuttleStationCode;
            addressResult_Condition.SendSalesDriverCode = SendSalesDriverCode;
            addressResult_Condition.SendMotorcycleDriverCode = SendMotorcycleDriverCode;



            //新增解析結果
            long id = await _addressResult_DA.Insert(addressResult_Condition);
            AddressResultId = id;

            addressUsingLog_Condition.AddressResultId = AddressResultId;
            await _addressUsingLog_DA.Insert(addressUsingLog_Condition);

            getDetail_Res.Id = id;
            getDetail_Res.Address = Address;
            getDetail_Res.City = City;
            getDetail_Res.Town = Town;
            getDetail_Res.Road = Road;
            getDetail_Res.PostZip3 = PostZip3;
            getDetail_Res.PostZip32 = PostZip32;
            getDetail_Res.PostZip33 = PostZip33;
            getDetail_Res.StationCode = StationCode;
            getDetail_Res.StationName = StationName;
            getDetail_Res.SalesDriverCode = SalesDriverCode;
            getDetail_Res.MotorcycleDriverCode = MotorcycleDriverCode;
            getDetail_Res.StackCode = StackCode;
            getDetail_Res.ShuttleStationCode = ShuttleStationCode;
            getDetail_Res.SendSalesDriverCode = SendSalesDriverCode;
            getDetail_Res.SendMotorcycleDriverCode = SendMotorcycleDriverCode;

            return getDetail_Res;

        }

        /// <summary>
        /// 特服費
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<GetSpecialAreaDetail_Res> GetSpecialAreaDetail(GetSpecialAreaDetail_Req req)
        {
            GetSpecialAreaDetail_Res getDetail_Res = new GetSpecialAreaDetail_Res();

            SpecialAreaInfoModel specialAreaInfoModel = new SpecialAreaInfoModel();


            //清理髒東西
            var fullAddr = _addressServices_Itri.ReplaceDirtyData(req.Address);



            var cacheKey = "specialAreaManage_DA_All";
            //checks if cache entries exists
            if (!_memoryCache.TryGetValue(cacheKey, out _specialAreaList))
            {

                //calling the server
                _specialAreaList = await _specialAreaManage_DA.GetAll();
                //setting up cache options
                var cacheExpiryOptions = new MemoryCacheEntryOptions
                {
                    AbsoluteExpiration = DateTime.Now.AddSeconds(3600),   // 絕對過期時間：時間到後就會消失
                    Priority = CacheItemPriority.High,  // 優先權：資源吃緊時，會先釋放優先權較低的資料
                                                        //  SlidingExpiration = TimeSpan.FromSeconds(60) // 相對過期時間：每次讀取值後會重新計算過期時間

                }.RegisterPostEvictionCallback((key, value, reason, state) =>   // 資料消失時的回呼函式
                {
                    //logger.LogDebug($"快取鍵值: {key} 已消失。原因: {reason}");
                });
                //setting cache entries
                _memoryCache.Set(cacheKey, _specialAreaList, cacheExpiryOptions);
            }

            if (_specialAreaList == null || _specialAreaList.Count() == 0)
            {
                return getDetail_Res;
            }

            //城市
            string c = string.Empty;
            //鄉市鎮區
            string a = string.Empty;
            //路段
            string r = string.Empty;
            //剩餘路段
            string n = string.Empty;
            //號
            int? No = null;

            SpecialAreaManage_Condition Info = new SpecialAreaManage_Condition();


            List<Task> tasks = new List<Task>();

            var t1 = Task.Run(async () =>
            {
                try
                {
                    //拆地址
                    //Junfu拆地址
                    var JunfuResult = await ParsingAddress(fullAddr);


                    if (JunfuResult != null)
                    {
                        c = string.IsNullOrEmpty(JunfuResult.city) ? c : JunfuResult.city;
                        a = string.IsNullOrEmpty(JunfuResult.area) ? a : JunfuResult.area;
                        r = string.IsNullOrEmpty(JunfuResult.road) ? r : JunfuResult.road;
                        n = string.IsNullOrEmpty(JunfuResult.address) ? n : JunfuResult.address;
                    }
                }
                catch (Exception e)
                {


                }

            });

            tasks.Add(t1);

            //var t2 = Task.Run(async () =>
            //{

            //    try
            //    {
            //        var GetMap8ResultAPI2Result = await GetMap8ResultAPI2(fullAddr);
            //        if (GetMap8ResultAPI2Result != null)
            //        {
            //            //城市
            //            c = string.IsNullOrEmpty(GetMap8ResultAPI2Result.city) ? c : _addressServices_Itri.ReplaceDirtyData(GetMap8ResultAPI2Result.city);
            //            //鄉市鎮區
            //            a = string.IsNullOrEmpty(GetMap8ResultAPI2Result.town) ? a : _addressServices_Itri.ReplaceDirtyData(GetMap8ResultAPI2Result.town);
            //            //剩餘路段
            //            var add = fullAddr;
            //            if (!string.IsNullOrEmpty(c)) 
            //            {
            //                add = add.Replace(c, "");
            //            }
            //            if (!string.IsNullOrEmpty(a))
            //            {
            //                add = add.Replace(a, "");
            //            }

            //            n = string.IsNullOrEmpty(add) ? n : _addressServices_Itri.ReplaceDirtyData(add);  //台灣圖霸V2會吃掉
            //        }
            //    }
            //    catch (Exception e)
            //    {


            //    }



            //});

            //tasks.Add(t2);

            Task.WaitAll(tasks.ToArray());
            //連城市都找不到 直接看關鍵字
            if (string.IsNullOrEmpty(c))
            {

                var _list = _specialAreaList.Where(x => !string.IsNullOrEmpty(x.KeyWord));

                var _data = _list.Where(x => fullAddr.IndexOf(x.KeyWord) > -1);

                if (_data.Count() == 1)
                {
                    Info = _data.First();

                    c = Info.City;
                    a = Info.Area;
                    r = fullAddr;
                    n = fullAddr;
                }

            }
            else
            {
                //處理可能有重複的瑕疵

                if (!string.IsNullOrEmpty(c))
                {
                    n = n.Replace(c, "");
                }
                if (!string.IsNullOrEmpty(a))
                {
                    n = n.Replace(a, "");
                }

                n = n.Replace("-", "之");

                var data = _specialAreaList.Where(x => x.City.Equals(c) && x.Area.Equals(a)).ToList();

                Dictionary<SpecialAreaManage_Condition, int> keyValuePairs = new Dictionary<SpecialAreaManage_Condition, int>();

                //有東西
                if (data.Any())
                {

                    int Weights = 0;

                    foreach (var _item in data)
                    {
                        //巷弄號 有路 最高權重

                        if (!string.IsNullOrEmpty(_item.Road) && (_item.Lane != null || _item.Alley != null || (_item.NoStart != null && _item.NoEnd != null)))
                        {
                            if (a.IndexOf(_item.Area) > -1)
                            {
                                if (n.IndexOf(_item.Road) > -1)
                                {

                                    //如果地址有巷弄 DATA確沒有 跳過
                                    if (n.IndexOf("巷") > -1 && _item.Lane == null)
                                    {
                                        //錯了就直接跳
                                        continue;
                                    }
                                    if (n.IndexOf("弄") > -1 && _item.Alley == null)
                                    {
                                        //錯了就直接跳
                                        continue;
                                    }
                                    if (n.IndexOf("號") > -1)
                                    {

                                    }



                                    //有值就要對
                                    //巷
                                    if (_item.Lane != null)
                                    {
                                        if (n.IndexOf(_item.Lane.Value.ToString() + "巷") == -1)
                                        {
                                            //錯了就直接跳
                                            continue;
                                        }
                                    }

                                    //弄
                                    if (_item.Alley != null)
                                    {
                                        if (n.IndexOf(_item.Alley.Value.ToString() + "弄") == -1)
                                        {
                                            //錯了就直接跳
                                            continue;
                                        }
                                    }

                                    bool s = false;

                                    //號
                                    if (_item.NoStart != null && _item.NoEnd != null)
                                    {
                                        //控制幾個一數
                                        int t = 1;
                                        //單雙數 兩個兩個數
                                        if (_item.Even.Value != 0)
                                        {
                                            t = 2;
                                        }

                                        for (int i = _item.NoStart.Value; i <= _item.NoEnd.Value; i += t)
                                        {

                                            var check1 = n.IndexOf(i.ToString() + "號之");
                                            var check2 = n.IndexOf(i.ToString() + "號");
                                            var check3 = n.IndexOf(i.ToString() + "之");

                                            if (check1 > -1)
                                            {
                                                if (check1 == 0)
                                                {
                                                    s = true;
                                                    No = i;
                                                    break;
                                                }
                                                else
                                                {
                                                    var c1 = n.Substring(check1 - 1, 1);
                                                    if (!int.TryParse(c1, out int cc))
                                                    {
                                                        s = true;
                                                        No = i;
                                                        break;
                                                    }
                                                }
                                            }
                                            else if (check2 > -1 && n.IndexOf("之") == -1) //X號
                                            {
                                                if (check2 == 0)
                                                {
                                                    s = true;
                                                    No = i;
                                                    break;
                                                }
                                                else
                                                {
                                                    var c2 = n.Substring(check2 - 1, 1);
                                                    if (!int.TryParse(c2, out int cc))
                                                    {
                                                        s = true;
                                                        No = i;
                                                        break;
                                                    }

                                                }



                                            }
                                            else if (check3 > -1 && n.IndexOf("號") > -1) //X之O號
                                            {
                                                if (check2 == 0)
                                                {
                                                    s = true;
                                                    No = i;
                                                    break;
                                                }
                                                else
                                                {
                                                    var c3 = n.Substring(check3 - 1, 1);
                                                    if (!int.TryParse(c3, out int cc))
                                                    {
                                                        s = true;
                                                        No = i;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (!s)
                                        {
                                            continue;
                                        }
                                    }
                                    Weights = Weights + 8;
                                }
                            }
                        }

                        //有路 其他無

                        if (!string.IsNullOrEmpty(_item.Road) && _item.Lane == null && _item.Alley == null && _item.NoStart == null && _item.NoEnd == null)
                        {
                            if (a.IndexOf(_item.Area) > -1)
                            {
                                if (n.IndexOf(_item.Road) > -1)
                                {
                                    Weights = Weights + 4;
                                }
                            }
                        }

                        // 無路 
                        if (string.IsNullOrEmpty(_item.Road) && _item.Lane == null && _item.Alley == null && _item.NoStart == null && _item.NoEnd == null)
                        {
                            if (a.IndexOf(_item.Area) > -1)
                            {
                                Weights = Weights + 2;
                            }
                        }


                        keyValuePairs.Add(_item, Weights);
                        Weights = 0;
                    }
                    var Max = 0;

                    if (keyValuePairs.Any())
                    {
                        Max = keyValuePairs.Max(x => x.Value);
                    }


                    if (Max > 0)
                    {
                        keyValuePairs = keyValuePairs.Where(x => x.Value == Max).ToDictionary(x => x.Key, x => x.Value);

                        if (keyValuePairs.Count() == 1)
                        {
                            //唯一值不包含關鍵字
                            if (string.IsNullOrEmpty(keyValuePairs.First().Key.KeyWord))
                            {
                                Info = keyValuePairs.First().Key;

                            }
                            else
                            {
                                foreach (var item in keyValuePairs.Keys)
                                {
                                    if (!string.IsNullOrEmpty(item.KeyWord))
                                    {
                                        if (n.IndexOf(item.KeyWord) > -1)
                                        {
                                            Info = item;
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                        else //比權重最高的KeyWord
                        {



                            foreach (var item in keyValuePairs.Keys)
                            {
                                if (!string.IsNullOrEmpty(item.KeyWord))
                                {
                                    if (n.IndexOf(item.KeyWord) > -1)
                                    {
                                        Info = item;
                                        break;
                                    }
                                }
                            }

                            //如果還是沒有但是有無關鍵字的最高金額
                            var keyValuePairsNoKeyWord = keyValuePairs.Keys.Where(x => string.IsNullOrEmpty(x.KeyWord)).ToList();

                            if (keyValuePairsNoKeyWord != null && keyValuePairsNoKeyWord.Count() > 0)
                            {
                                Info = keyValuePairsNoKeyWord.OrderByDescending(x => x.Fee).FirstOrDefault();
                            }

                        }
                    }

                }
            }

            getDetail_Res.Address = c + a + n;

            getDetail_Res.City = c;

            getDetail_Res.Town = a;

            getDetail_Res.id = Info.id;
            getDetail_Res.Road = n;
            getDetail_Res.StationCode = Info.StationCode;
            getDetail_Res.SalesDriverCode = Info.SalesDriverCode;
            getDetail_Res.MotorcycleDriverCode = Info.MotorcycleDriverCode;
            getDetail_Res.StackCode = Info.StackCode;
            getDetail_Res.ShuttleStationCode = Info.ShuttleStationCode;
            getDetail_Res.Fee = (int)Info.Fee;
            getDetail_Res.TimeLimit = Info.TimeLimit.ToString();


            SpecialAreaResult_Condition specialAreaResult_Condition = new SpecialAreaResult_Condition();
            specialAreaResult_Condition.InitialAddress = req.Address;
            specialAreaResult_Condition.City = Info.City;
            specialAreaResult_Condition.Area = Info.Area;
            specialAreaResult_Condition.Road = Info.Road;
            specialAreaResult_Condition.Lane = Info.Lane;
            specialAreaResult_Condition.Alley = Info.Alley;
            specialAreaResult_Condition.No = No;
            specialAreaResult_Condition.SpecialAreaManageId = Info.id;
            specialAreaResult_Condition.KeyWord = Info.KeyWord;
            specialAreaResult_Condition.StationCode = Info.StationCode;
            specialAreaResult_Condition.SalesDriverCode = Info.SalesDriverCode;
            specialAreaResult_Condition.MotorcycleDriverCode = Info.MotorcycleDriverCode;
            specialAreaResult_Condition.StackCode = Info.StackCode;
            specialAreaResult_Condition.ShuttleStationCode = Info.ShuttleStationCode;
            specialAreaResult_Condition.Fee = Info.Fee;
            specialAreaResult_Condition.TimeLimit = Info.TimeLimit;

            await _specialAreaResult_DA.Insert(specialAreaResult_Condition);



            return getDetail_Res;

        }


        private async Task<List<Map8Result>> GetMap8Result(string Address)
        {

            List<Map8Result> _map8Result = new List<Map8Result>();
            //先找舊的
            var Map8ExpiredDay = int.TryParse(_configuration["Map8:ExpiredDay"], out int d) ? d : 0;

            DateTime Now = DateTime.Now;

            DateTime SNow = Now.AddDays(Map8ExpiredDay * -1);

            var map8AddressInfo = (await _map8AddressDismantle_DA.Search(Address, SNow, Now)).FirstOrDefault();

            if (map8AddressInfo != null && string.IsNullOrEmpty(map8AddressInfo.city))
            {

                Map8Result map8Result = new Map8Result();
                map8Result.formatted_address = map8AddressInfo.formatted_address;
                map8Result.doorplateID = map8AddressInfo.doorplateID;
                map8Result.postcode3 = map8AddressInfo.postcode3;
                map8Result.postcode33 = map8AddressInfo.postcode33;
                map8Result.city = map8AddressInfo.city;
                map8Result.town = map8AddressInfo.town;
                map8Result.village = map8AddressInfo.village;
                map8Result.lin = map8AddressInfo.lin;
                map8Result.road = map8AddressInfo.road;
                map8Result.hamlet = map8AddressInfo.hamlet;
                map8Result.lane = map8AddressInfo.lane;
                map8Result.alley = map8AddressInfo.alley;
                map8Result.lon = map8AddressInfo.lon;
                map8Result.num = map8AddressInfo.num;
                map8Result.floor = map8AddressInfo.floor;
                map8Result.numAttr = map8AddressInfo.numAttr;
                map8Result.residenceID = map8AddressInfo.residenceID;
                map8Result.compType = map8AddressInfo.compType;
                map8Result.compDate = map8AddressInfo.compDate;
                _map8Result.Add(map8Result);


            }
            else
            {
                var Map8Key = _configuration["Map8:Key"];
                var Map8RRL = _configuration["Map8:URL"];
                //打API
                var client = new RestClient(Map8RRL);
                var request = new RestRequest(Method.GET);
                request.AddParameter("query", Address.Trim().Replace(" ", ""));
                request.AddParameter("key", Map8Key);

                int ms = int.TryParse(_configuration["Map8:WaitMillisecondsTimeout"], out int m) ? m : 0;

                var Map8ResultTask = new List<Map8Result>();

                DateTime StartTime = DateTime.Now;
                DateTime EndTime = DateTime.Now;
                Map8 map8 = new Map8();

                var WaitTask = Task.Run(() =>
                {
                    try
                    {
                        StartTime = DateTime.Now;
                        var restResponse = client.Get<object>(request);
                        EndTime = DateTime.Now;
                        if (restResponse.StatusCode == HttpStatusCode.OK)
                        {
                            map8 = JsonConvert.DeserializeObject<Map8>(restResponse.Content);

                            if (map8.results != null && map8.results.Count() > 0)
                            {
                                int n = 1;
                                //紀錄
                                foreach (var item in map8.results)
                                {
                                    if (!string.IsNullOrEmpty(item.city))
                                    {
                                        Map8AddressDismantle_Condition map8AddressDismantle = new Map8AddressDismantle_Condition();

                                        map8AddressDismantle.o_address = Address;
                                        map8AddressDismantle.number = n;
                                        map8AddressDismantle.formatted_address = item.formatted_address;
                                        map8AddressDismantle.postcode3 = item.postcode3;
                                        map8AddressDismantle.postcode33 = item.postcode33;
                                        map8AddressDismantle.doorplateID = item.doorplateID;
                                        map8AddressDismantle.city = item.city;
                                        map8AddressDismantle.town = item.town;
                                        map8AddressDismantle.village = item.village;
                                        map8AddressDismantle.lin = item.lin;
                                        map8AddressDismantle.road = item.road;
                                        map8AddressDismantle.hamlet = item.hamlet;
                                        map8AddressDismantle.lane = item.lane;
                                        map8AddressDismantle.alley = item.alley;
                                        map8AddressDismantle.lon = item.lon;
                                        map8AddressDismantle.num = item.num;
                                        map8AddressDismantle.floor = item.floor;
                                        map8AddressDismantle.numAttr = item.numAttr;
                                        map8AddressDismantle.residenceID = item.residenceID;
                                        map8AddressDismantle.compType = item.compType;
                                        map8AddressDismantle.compDate = item.compDate;
                                        map8AddressDismantle.trxDate = item.trxDate;
                                        map8AddressDismantle.geomJson = JsonConvert.SerializeObject(item.geom);
                                        map8AddressDismantle.resultAnalysisJson = JsonConvert.SerializeObject(item.resultAnalysis);
                                        map8AddressDismantle.history = JsonConvert.SerializeObject(item.history);
                                        map8AddressDismantle.StartTime = StartTime;
                                        map8AddressDismantle.EndTime = EndTime;
                                        _map8AddressDismantle_DA.Insert(map8AddressDismantle);
                                        n++;

                                    }

                                }

                                _map8Result = map8.results;

                            }
                            else//抓上次的結果
                            {

                            }
                        }
                        else
                        {
                            Map8AddressDismantle_Condition map8AddressDismantle = new Map8AddressDismantle_Condition();
                            string ERROR = restResponse.Content;

                            if (string.IsNullOrEmpty(restResponse.Content))
                            {
                                ERROR = restResponse.StatusCode.ToString();
                            }

                            map8AddressDismantle.formatted_address = ERROR;
                            map8AddressDismantle.o_address = Address;
                            map8AddressDismantle.StartTime = StartTime;
                            map8AddressDismantle.EndTime = EndTime;
                            _map8AddressDismantle_DA.Insert(map8AddressDismantle);
                        }


                    }
                    catch (Exception e)
                    {
                        Map8AddressDismantle_Condition map8AddressDismantle = new Map8AddressDismantle_Condition();
                        map8AddressDismantle.o_address = Address;
                        map8AddressDismantle.formatted_address = e.Message;
                        map8AddressDismantle.StartTime = StartTime;
                        map8AddressDismantle.EndTime = EndTime;
                        _map8AddressDismantle_DA.Insert(map8AddressDismantle);

                    }
                }
                 ).Wait(ms);


            }

            //DateTime StartTime = DateTime.Now;
            //var response = client.Get<object>(request);
            //DateTime EndTime = DateTime.Now;

            //if (response.StatusCode == HttpStatusCode.OK)
            //{
            //    try
            //    {
            //        var json = JsonConvert.DeserializeObject<Map8>(response.Content);

            //        if (json.results != null && json.results.Count() > 0)
            //        {

            //            int n = 1;
            //            //紀錄
            //            foreach (var item in json.results)
            //            {
            //                Map8AddressDismantle_Condition map8AddressDismantle = new Map8AddressDismantle_Condition();

            //                map8AddressDismantle.o_address = Address;
            //                map8AddressDismantle.number = n;
            //                map8AddressDismantle.formatted_address = item.formatted_address;
            //                map8AddressDismantle.postcode3 = item.postcode3;
            //                map8AddressDismantle.postcode33 = item.postcode33;
            //                map8AddressDismantle.doorplateID = item.doorplateID;
            //                map8AddressDismantle.city = item.city;
            //                map8AddressDismantle.town = item.town;
            //                map8AddressDismantle.village = item.village;
            //                map8AddressDismantle.lin = item.lin;
            //                map8AddressDismantle.road = item.road;
            //                map8AddressDismantle.hamlet = item.hamlet;
            //                map8AddressDismantle.lane = item.lane;
            //                map8AddressDismantle.alley = item.alley;
            //                map8AddressDismantle.lon = item.lon;
            //                map8AddressDismantle.num = item.num;
            //                map8AddressDismantle.floor = item.floor;
            //                map8AddressDismantle.numAttr = item.numAttr;
            //                map8AddressDismantle.residenceID = item.residenceID;
            //                map8AddressDismantle.compType = item.compType;
            //                map8AddressDismantle.compDate = item.compDate;
            //                map8AddressDismantle.trxDate = item.trxDate;
            //                map8AddressDismantle.geomJson = JsonConvert.SerializeObject(item.geom);
            //                map8AddressDismantle.resultAnalysisJson = JsonConvert.SerializeObject(item.resultAnalysis);
            //                map8AddressDismantle.history = JsonConvert.SerializeObject(item.history);
            //                map8AddressDismantle.StartTime = StartTime;
            //                map8AddressDismantle.EndTime = EndTime;
            //                await _map8AddressDismantle_DA.Insert(map8AddressDismantle);
            //                n++;
            //            }

            //            _map8Result = json.results;

            //        }
            //        else
            //        {

            //        }
            //    }
            //    catch (Exception e)
            //    {


            //    }

            //}
            //else
            //{

            //}

            return _map8Result;
        }

        private async Task<Map8V2Result> GetMap8ResultAPI2(string Address)
        {

            Map8V2Result _map8V2Result = new Map8V2Result();
            //先找舊的
            var Map8ExpiredCacheSeconds = int.TryParse(_configuration["Map8:ExpiredCacheSeconds"], out int d) ? d : 0;


            DateTime StartTime = DateTime.Now;
            DateTime EndTime = DateTime.Now;


            var cacheKey = Address;
            //checks if cache entries exists
            if (!_memoryCache.TryGetValue(cacheKey, out _map8V2Result))
            {
                //calling the server
                var Map8Key = _configuration["Map8:Key"];
                var Map8RRL = _configuration["Map8:URL_2"];

                //打API
                var client = new RestClient(Map8RRL);
                client.Timeout = 2000;
                var request = new RestRequest(Method.GET);
                request.AddParameter("address", Address.Trim().Replace(" ", ""));
                request.AddParameter("key", Map8Key);
                StartTime = DateTime.Now;
                var restResponse = client.Get<object>(request);
                EndTime = DateTime.Now;
                if (restResponse.StatusCode == HttpStatusCode.OK)
                {
                    var map8v2 = JsonConvert.DeserializeObject<Map8V2>(restResponse.Content);

                    if (map8v2.results != null && map8v2.results.Count() > 0)
                    {

                        _map8V2Result = map8v2.results[0];

                        Map8AddressDismantleV2_Condition map8AddressDismantleV2 = new Map8AddressDismantleV2_Condition();


                        map8AddressDismantleV2.o_address = Address;
                        map8AddressDismantleV2.formatted_address = _map8V2Result.city + _map8V2Result.town + _map8V2Result.name;
                        map8AddressDismantleV2.place_id = _map8V2Result.place_id;
                        map8AddressDismantleV2.name = _map8V2Result.name;
                        map8AddressDismantleV2.city = _map8V2Result.city;
                        map8AddressDismantleV2.town = _map8V2Result.town;
                        map8AddressDismantleV2.type = _map8V2Result.type;
                        map8AddressDismantleV2.level = _map8V2Result.level;
                        map8AddressDismantleV2.likelihood = _map8V2Result.likelihood;
                        map8AddressDismantleV2.authoritative = _map8V2Result.authoritative;
                        map8AddressDismantleV2.geomJson = JsonConvert.SerializeObject(_map8V2Result.geometry);
                        map8AddressDismantleV2.StartTime = StartTime;
                        map8AddressDismantleV2.EndTime = EndTime;

                        await _map8AddressDismantleV2_DA.Insert(map8AddressDismantleV2);


                    }
                    else
                    {

                        return null;
                    }

                }
                else
                {

                    return null;
                }

                //setting up cache options
                var cacheExpiryOptions = new MemoryCacheEntryOptions
                {
                    AbsoluteExpiration = DateTime.Now.AddSeconds(Map8ExpiredCacheSeconds),   // 絕對過期時間：時間到後就會消失
                    Priority = CacheItemPriority.Low,  // 優先權：資源吃緊時，會先釋放優先權較低的資料

                }.RegisterPostEvictionCallback((key, value, reason, state) =>   // 資料消失時的回呼函式
                {
                    //logger.LogDebug($"快取鍵值: {key} 已消失。原因: {reason}");
                });
                //setting cache entries
                _memoryCache.Set(cacheKey, _map8V2Result, cacheExpiryOptions);
            }


            return _map8V2Result;
        }
        /// <summary>
        /// 確認收件人地址
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private async Task<LyAddrFormatReturnModel> ParsingAddress(string address)
        {
            LyAddrFormatReturnModel result = new LyAddrFormatReturnModel();
            try
            {

                var ExpiredDay = int.TryParse(_configuration["ExpiredDay"], out int d) ? d : 0;

                DateTime Now = DateTime.Now;
                DateTime SNow = Now.AddDays(ExpiredDay * -1);

                //查上次結果
                var info = await _junfuAddressDismantle_DA.Search(address, SNow, Now);

                if (info == null)
                {

                    string fulladdress = address;
                    //去除空白換行
                    fulladdress = Tool.StrTrimer(fulladdress);
                    //全形轉半形
                    fulladdress = Tool.ToDBC(fulladdress);
                    //去頭郵遞區號
                    fulladdress = Tool.ReplaceZipCode(fulladdress);
                    //簡轉繁體
                    string realAddress = ChineseConverter.Convert(fulladdress, ChineseConversionDirection.SimplifiedToTraditional);
                    //特殊狀況
                    realAddress = realAddress.Replace("樸子市", "朴子市");

                    realAddress = realAddress.Replace("南莊鄉", "南庄鄉");

                    realAddress = realAddress.Replace("臺積電", "台積電");

                    result = await _storedProcedure_DA.GetLY_AddrFormat(realAddress);

                    if (realAddress.IndexOf(result.city) == 0)
                    {
                        realAddress = realAddress.Replace(result.city, "");
                    }
                    else
                    {
                        //throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
                    }
                    if (realAddress.IndexOf(result.area) == 0)
                    {
                        realAddress = realAddress.Replace(result.area, "");
                    }
                    else
                    {
                        //throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
                    }
                    result.address = realAddress;

                    if (result == null)
                    {
                        //throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);

                    }
                    if (string.IsNullOrEmpty(result.city) || string.IsNullOrEmpty(result.area))
                    {
                        // throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
                    }

                    JunfuAddressDismantle_Condition junfuAddressDismantle_Condition = new JunfuAddressDismantle_Condition();
                    junfuAddressDismantle_Condition.o_address = address;
                    junfuAddressDismantle_Condition.status_code = result.status_code == null ? string.Empty : result.status_code.ToString();
                    junfuAddressDismantle_Condition.status_msg = result.status_msg;
                    junfuAddressDismantle_Condition.city = result.city;
                    junfuAddressDismantle_Condition.area = result.area;
                    junfuAddressDismantle_Condition.village = result.village;
                    junfuAddressDismantle_Condition.road = result.road;
                    junfuAddressDismantle_Condition.lane = result.lane;
                    junfuAddressDismantle_Condition.alley = result.alley;
                    junfuAddressDismantle_Condition.sub_alley = result.sub_alley;
                    junfuAddressDismantle_Condition.neig = result.neig;
                    junfuAddressDismantle_Condition.no = result.no;
                    junfuAddressDismantle_Condition.address = result.address;
                    await _junfuAddressDismantle_DA.Insert(junfuAddressDismantle_Condition);

                }
                else
                {
                    result.status_code = int.Parse(info.status_code);
                    result.status_msg = info.status_msg;
                    result.city = info.city;
                    result.area = info.area;
                    result.village = info.village;
                    result.road = info.road;
                    result.lane = info.lane;
                    result.alley = info.alley;
                    result.sub_alley = info.sub_alley;
                    result.neig = info.neig;
                    result.no = info.no;
                    result.address = info.address;


                }
                return result;

            }
            catch (Exception e)
            {
                return null;
                //throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
            }
            //return result;
        }


        private bool CheckMap8Result(Map8Result _map8Result, string _address, int lv)
        {

            bool check = true;

            //切換錯別字
            var address = _address;
            address = address.Replace("台", "臺");
            address = address.Replace("桃園縣", "桃園市");
            address = address.Replace("南莊", "南庄");
            switch (lv)
            {
                case 1:
                    if (address.IndexOf(_map8Result.city) == -1)
                    {
                        check = false;
                    }
                    break;
                case 2:
                    if (address.IndexOf(_map8Result.city) == -1)
                    {
                        check = false;
                    }
                    if (address.IndexOf(_map8Result.town) == -1)
                    {
                        check = false;
                    }
                    break;
                case 3://市區鄉鎮 可以不用打對
                    if (address.IndexOf(_map8Result.city) == -1)
                    {
                        check = false;
                    }
                    if (address.IndexOf(_map8Result.town.Replace("市", "").Replace("區", "").Replace("鄉", "").Replace("鎮", "")) == -1)
                    {
                        check = false;
                    }
                    break;
                default:
                    break;
            }




            return check;
        }

        private orgArea_Condition ParsingMap8SDMD(Map8Result _map8Result, string postcode)
        {
            //所有路段
            var OrgAreasList = _orgAreaList;

            List<orgArea_Condition> data = new List<orgArea_Condition>();

            string Map8_city = _map8Result.city;
            string Map8_town = _map8Result.town;

            string Map8_village = _map8Result.village;
            string Map8_road = _map8Result.road;
            string Map8_hamlet = _map8Result.hamlet;
            string Map8_lane = _map8Result.lane;
            //猜解台灣圖霸 配合地圖
            double lane = -1;
            double alley = -1;
            double num = -1;
            double numJhih = -1;
            double floor = -1;
            double floorJhih = -1;
            string strlane = _map8Result.lane;
            string stralley = _map8Result.alley;
            string strnum = _map8Result.num;
            string strfloor = _map8Result.floor;

            string sp = string.Empty;
            //string strJhih = string.Empty;
            //巷
            if (!string.IsNullOrEmpty(strlane))
            {
                try
                {
                    lane = double.Parse(strlane.Replace("巷", ""));
                }
                catch (Exception e)
                {


                }
            }

            //弄
            if (!string.IsNullOrEmpty(stralley))
            {
                try
                {
                    alley = double.Parse(stralley.Replace("弄", ""));
                }
                catch (Exception e)
                {

                }
            }

            //太複雜加個try
            try
            {
                //號
                if (!string.IsNullOrEmpty(strnum))
                {
                    try
                    {
                        //幾種規則
                        //XXXX1號
                        //OOOO2之3號
                        //118之11號之3

                        strnum = strnum.Replace("附", "之");//特殊用法

                        if (strnum.Substring(strnum.Length - 1, 1).Equals("號"))
                        {
                            var Bnum = double.TryParse(strnum.Replace("號", ""), out double TryParseNum);

                            if (Bnum)//成功 EX 8號
                            {
                                num = TryParseNum;
                            }
                            else //失敗 可能會有 "之"
                            {
                                if (strnum.IndexOf("之") > -1)//EX 6之1號
                                {
                                    var BnumJhih = double.TryParse(strnum.Split("之").First(), out double TryParsenNum);

                                    if (BnumJhih)
                                    {
                                        num = TryParsenNum;
                                        numJhih = double.Parse(strnum.Split("之").Last().Replace("號", ""));
                                    }
                                    else//EX   廍子坑69之203號
                                    {
                                        //先找  之  之前的數字
                                        var numFirst = strnum.Split("之").First();
                                        String value = Regex.Replace(numFirst, "[^0-9]", ""); //僅保留數字
                                        num = double.Parse(value);
                                        sp = numFirst.Replace(value, "");
                                        numJhih = double.Parse(strnum.Split("之").Last().Replace("號", ""));
                                    }
                                }
                                else  //清大東院5號 
                                {
                                    //先找  號  之前的數字
                                    var JhihFirst = strnum.Split("號").First();
                                    String value = Regex.Replace(JhihFirst, "[^0-9]", ""); //僅保留數字
                                    num = double.Parse(value);
                                    sp = JhihFirst.Replace(value, "");
                                }
                            }
                        }
                        else//EX 118之11號之3 5號之1
                        {
                            if (strnum.IndexOf("號之") > -1)
                            {
                                var BnumJhih = double.TryParse(strnum.Split("號之").First(), out double TryParsenNmJhih);

                                if (BnumJhih)
                                {
                                    num = TryParsenNmJhih;
                                }
                                else//暫時不管
                                {

                                }

                            }
                        }

                    }
                    catch (Exception)
                    {

                    }

                }

            }
            catch (Exception)
            {

            }


            //樓
            if (!string.IsNullOrEmpty(strfloor))
            {
                try
                {
                    if (strfloor.IndexOf("樓之") > -1)
                    {
                        floor = int.Parse(strfloor.Split("樓之").First());
                        floorJhih = int.Parse(strfloor.Split("樓之").Last());
                    }
                    else if (strfloor.IndexOf("樓") > -1)
                    {
                        floor = int.Parse(strfloor.Split("樓").First());
                    }
                }
                catch (Exception)
                {

                }

            }


            switch (postcode)
            {
                case "5":
                    string oroad = _map8Result.road;
                    string nroad = _map8Result.road;
                    //郵局路段3+2 中文數字 表示不一需要切換1段~10段 
                    if (oroad.IndexOf("段") > -1)
                    {
                        nroad = nroad.Replace("十段", "１０段");
                        nroad = nroad.Replace("九段", "９段");
                        nroad = nroad.Replace("八段", "８段");
                        nroad = nroad.Replace("七段", "７段");
                        nroad = nroad.Replace("六段", "６段");
                        nroad = nroad.Replace("五段", "５段");
                        nroad = nroad.Replace("四段", "４段");
                        nroad = nroad.Replace("三段", "３段");
                        nroad = nroad.Replace("二段", "２段");
                        nroad = nroad.Replace("一段", "１段");
                    }


                    // string postrode32 = nroad + _map8Result.hamlet;


                    string postrode32 = nroad + _map8Result.hamlet;
                    if (!string.IsNullOrEmpty(nroad) && !string.IsNullOrEmpty(Map8_hamlet))
                    {
                        postrode32 = nroad + Map8_hamlet + Map8_lane;
                    }
                    else if (!string.IsNullOrEmpty(nroad))
                    {
                        postrode32 = nroad + sp;
                    }


                    var orgArea32 = from orgArea
                                    in OrgAreasList
                                    where
                                    orgArea.ZIPCODE.Length == 5 &&
                                    orgArea.CITY.Equals(Map8_city) &&
                                    orgArea.AREA.Equals(Map8_town) &&
                                    orgArea.ROAD.Equals(postrode32)
                                    select orgArea;

                    data = orgArea32.ToList();

                    if (data.Count() == 0 && !string.IsNullOrEmpty(sp))
                    {
                        var orgArea32nosp = from orgArea
                                  in OrgAreasList
                                            where
                                            orgArea.ZIPCODE.Length == 5 &&
                                            orgArea.CITY.Equals(Map8_city) &&
                                            orgArea.AREA.Equals(Map8_town) &&
                                            orgArea.ROAD.Equals(nroad)
                                            select orgArea;

                        data = orgArea32nosp.ToList();
                    }




                    if (data.Count() == 0)
                    {
                        //沒有路 有三種 寫法
                        if (string.IsNullOrEmpty(nroad))
                        {
                            var orgArea32hamlet = from orgArea
                                    in OrgAreasList
                                                  where
                                                  orgArea.ZIPCODE.Length == 5 &&
                                                  orgArea.CITY.Equals(Map8_city) &&
                                                  orgArea.AREA.Equals(Map8_town) &&
                                                  orgArea.ROAD.Equals(Map8_hamlet)//巷
                                                  select orgArea;

                            data = orgArea32hamlet.ToList();

                            if (data.Count() == 0)
                            {
                                var orgArea32village = from orgArea
                                       in OrgAreasList
                                                       where
                                                       orgArea.ZIPCODE.Length == 5 &&
                                                       orgArea.CITY.Equals(Map8_city) &&
                                                       orgArea.AREA.Equals(Map8_town) &&
                                                       orgArea.ROAD.Equals(Map8_village + Map8_hamlet)//巷
                                                       select orgArea;

                                data = orgArea32village.ToList();

                                if (data.Count() == 0)
                                {
                                    var orgArea32sp = from orgArea
                                           in OrgAreasList
                                                      where
                                                      orgArea.ZIPCODE.Length == 5 &&
                                                      orgArea.CITY.Equals(Map8_city) &&
                                                      orgArea.AREA.Equals(Map8_town) &&
                                                      orgArea.ROAD.Equals(sp)//特殊
                                                      select orgArea;

                                    data = orgArea32sp.ToList();


                                }

                            }

                        }

                    }




                    break;
                case "6":


                    string postrode33 = Map8_road + _map8Result.hamlet;
                    if (!string.IsNullOrEmpty(Map8_road) && !string.IsNullOrEmpty(Map8_hamlet))
                    {
                        postrode32 = Map8_road + Map8_hamlet + Map8_lane;
                    }
                    else if (!string.IsNullOrEmpty(Map8_road))
                    {
                        postrode32 = Map8_road;
                    }
                    else if (!string.IsNullOrEmpty(Map8_village) && string.IsNullOrEmpty(Map8_road) && !string.IsNullOrEmpty(Map8_hamlet))
                    {
                        postrode32 = Map8_village + Map8_hamlet;
                    }
                    else
                    {
                        postrode32 = Map8_road;
                    }
                    //3+3
                    var orgArea33 = from orgArea
                                    in OrgAreasList
                                    where
                                    orgArea.ZIPCODE.Length == 6 &&
                                    orgArea.CITY.Equals(_map8Result.city) &&
                                    orgArea.AREA.Equals(_map8Result.town) &&
                                    orgArea.ROAD.Equals(postrode33)
                                    select orgArea;

                    data = orgArea33.ToList();
                    break;
                default:
                    break;

            }


            //檢查是否唯一一筆

            if (data.Count() == 0)
            {
                return null;
            }
            else if (data.Count() == 1)
            {
                return data.FirstOrDefault();
            }
            else
            {
                //如果有多筆 但是 結過都一樣 照樣過
                var one = data
                        .Select(m => new
                        {
                            ZIPCODE = string.IsNullOrEmpty(m.ZIPCODE) ? string.Empty : m.ZIPCODE,
                            ZIP3A = string.IsNullOrEmpty(m.ZIP3A) ? string.Empty : m.ZIP3A,
                            station_name = string.IsNullOrEmpty(m.station_name) ? string.Empty : m.station_name,
                            station_scode = string.IsNullOrEmpty(m.station_scode) ? string.Empty : m.station_scode,
                            sd_no = string.IsNullOrEmpty(m.sd_no) ? string.Empty : m.sd_no,
                            md_no = string.IsNullOrEmpty(m.md_no) ? string.Empty : m.md_no,
                            put_order = string.IsNullOrEmpty(m.put_order) ? string.Empty : m.put_order,
                        })
                        .Distinct()
                        .ToList();

                //只有一個結果(SDMD)
                if (one.Count() == 1)
                {
                    return data.FirstOrDefault();
                }
                else // 多個結果 要用其他去判斷
                {
                    //巷弄號之樓 配合郵局路段拆解


                    //判斷巷
                    if (lane > 0)
                    {
                        var datalane = data.Where(x => x.LANE == lane).ToList();

                        if (datalane.Count() == 1)
                        {
                            return datalane.FirstOrDefault();
                        }
                        else
                        {
                            //如果有多筆 但是 結過都一樣 照樣過
                            var datalane_one = datalane
                                    .Select(m => new
                                    {
                                        ZIPCODE = string.IsNullOrEmpty(m.ZIPCODE) ? string.Empty : m.ZIPCODE,
                                        ZIP3A = string.IsNullOrEmpty(m.ZIP3A) ? string.Empty : m.ZIP3A,
                                        station_name = string.IsNullOrEmpty(m.station_name) ? string.Empty : m.station_name,
                                        station_scode = string.IsNullOrEmpty(m.station_scode) ? string.Empty : m.station_scode,
                                        sd_no = string.IsNullOrEmpty(m.sd_no) ? string.Empty : m.sd_no,
                                        md_no = string.IsNullOrEmpty(m.md_no) ? string.Empty : m.md_no,
                                        put_order = string.IsNullOrEmpty(m.put_order) ? string.Empty : m.put_order,
                                    })
                                    .Distinct()
                                    .ToList();

                            //只有一個結果(SDMD)
                            if (datalane_one.Count() == 1)
                            {
                                return datalane.FirstOrDefault();
                            }
                        }
                    }
                    //判斷弄
                    if (alley > 0)
                    {
                        var dataalley = data.Where(x => x.ALLEY == alley).ToList();

                        if (dataalley.Count() == 1)
                        {
                            return dataalley.FirstOrDefault();
                        }
                        else
                        {
                            //如果有多筆 但是 結過都一樣 照樣過
                            var dataalley_one = dataalley
                                    .Select(m => new
                                    {
                                        ZIPCODE = string.IsNullOrEmpty(m.ZIPCODE) ? string.Empty : m.ZIPCODE,
                                        ZIP3A = string.IsNullOrEmpty(m.ZIP3A) ? string.Empty : m.ZIP3A,
                                        station_name = string.IsNullOrEmpty(m.station_name) ? string.Empty : m.station_name,
                                        station_scode = string.IsNullOrEmpty(m.station_scode) ? string.Empty : m.station_scode,
                                        sd_no = string.IsNullOrEmpty(m.sd_no) ? string.Empty : m.sd_no,
                                        md_no = string.IsNullOrEmpty(m.md_no) ? string.Empty : m.md_no,
                                        put_order = string.IsNullOrEmpty(m.put_order) ? string.Empty : m.put_order,
                                    })
                                    .Distinct()
                                    .ToList();

                            //只有一個結果(SDMD)
                            if (dataalley_one.Count() == 1)
                            {
                                return dataalley.FirstOrDefault();
                            }
                        }
                    }


                    //判斷號
                    if (num > 0)
                    {
                        //先判斷 郵局沒有EVEN 直接判段號碼

                        var o = data.Where(x => x.EVEN == 0);
                        if (o.Count() > 0)
                        {
                            var datanum_o = o.Where(x => x.NO_BGN <= num && x.NO_END >= num).ToList();

                            if (datanum_o.Count() == 1)
                            {
                                return datanum_o.FirstOrDefault();
                            }
                            else
                            {
                                //如果有多筆 但是 結過都一樣 照樣過
                                var datanum_one = datanum_o
                                        .Select(m => new
                                        {
                                            ZIPCODE = string.IsNullOrEmpty(m.ZIPCODE) ? string.Empty : m.ZIPCODE,
                                            ZIP3A = string.IsNullOrEmpty(m.ZIP3A) ? string.Empty : m.ZIP3A,
                                            station_name = string.IsNullOrEmpty(m.station_name) ? string.Empty : m.station_name,
                                            station_scode = string.IsNullOrEmpty(m.station_scode) ? string.Empty : m.station_scode,
                                            sd_no = string.IsNullOrEmpty(m.sd_no) ? string.Empty : m.sd_no,
                                            md_no = string.IsNullOrEmpty(m.md_no) ? string.Empty : m.md_no,
                                            put_order = string.IsNullOrEmpty(m.put_order) ? string.Empty : m.put_order,
                                        })
                                        .Distinct()
                                        .ToList();

                                //只有一個結果(SDMD)
                                if (datanum_one.Count() == 1)
                                {
                                    return datanum_o.FirstOrDefault();
                                }
                            }

                        }



                        //先判斷 基數偶數
                        //抓偶數機數
                        var findNo = int.Parse(num.ToString());
                        if ((findNo % 2) == 1)
                        {
                            var even = data.Where(x => x.EVEN == 1);
                            //只有一個結果(SDMD)
                            if (even.Count() == 1)
                            {
                                return even.FirstOrDefault();
                            }
                            else
                            {
                                //如果有多筆 但是 結過都一樣 照樣過
                                var datanum_even = even
                                        .Select(m => new
                                        {
                                            ZIPCODE = string.IsNullOrEmpty(m.ZIPCODE) ? string.Empty : m.ZIPCODE,
                                            ZIP3A = string.IsNullOrEmpty(m.ZIP3A) ? string.Empty : m.ZIP3A,
                                            station_name = string.IsNullOrEmpty(m.station_name) ? string.Empty : m.station_name,
                                            station_scode = string.IsNullOrEmpty(m.station_scode) ? string.Empty : m.station_scode,
                                            sd_no = string.IsNullOrEmpty(m.sd_no) ? string.Empty : m.sd_no,
                                            md_no = string.IsNullOrEmpty(m.md_no) ? string.Empty : m.md_no,
                                            put_order = string.IsNullOrEmpty(m.put_order) ? string.Empty : m.put_order,
                                        })
                                        .Distinct()
                                        .ToList();

                                //只有一個結果(SDMD)
                                if (datanum_even.Count() == 1)
                                {
                                    return even.FirstOrDefault();
                                }
                                else//如果有不一樣再來筆數字區間
                                {
                                    var datanum = even.Where(x => (x.NO_BGN <= num || x.NO_BGN == 0) && (x.NO_END >= num || x.NO_END == 0)).ToList();

                                    if (datanum.Count() == 1)
                                    {
                                        return datanum.FirstOrDefault();
                                    }
                                    else if (datanum.Count() == 0) //檢查特殊狀況 CMP_LABLE ='I'
                                    {
                                        var I = even.Where(x => x.LANE != 0 && x.NO_BGN == 0 && x.NO_END == 0);

                                        if (I.Count() == 1)
                                        {
                                            return I.FirstOrDefault();
                                        }
                                    }
                                    else if (datanum.Count() > 1)
                                    {
                                        //如果有多筆 但是 結過都一樣 照樣過
                                        var datanum_one = datanum
                                                .Select(m => new
                                                {
                                                    ZIPCODE = string.IsNullOrEmpty(m.ZIPCODE) ? string.Empty : m.ZIPCODE,
                                                    ZIP3A = string.IsNullOrEmpty(m.ZIP3A) ? string.Empty : m.ZIP3A,
                                                    station_name = string.IsNullOrEmpty(m.station_name) ? string.Empty : m.station_name,
                                                    station_scode = string.IsNullOrEmpty(m.station_scode) ? string.Empty : m.station_scode,
                                                    sd_no = string.IsNullOrEmpty(m.sd_no) ? string.Empty : m.sd_no,
                                                    md_no = string.IsNullOrEmpty(m.md_no) ? string.Empty : m.md_no,
                                                    put_order = string.IsNullOrEmpty(m.put_order) ? string.Empty : m.put_order,
                                                })
                                                .Distinct()
                                                .ToList();

                                        //只有一個結果(SDMD)
                                        if (datanum_one.Count() == 1)
                                        {
                                            return datanum.FirstOrDefault();
                                        }
                                        else//比到最後都沒有 選擇沒有巷弄的 
                                        {
                                            return datanum.Where(x => x.LANE == 0).FirstOrDefault();

                                        }
                                    }
                                }
                            }

                        }

                        if ((findNo % 2) == 0)
                        {
                            var old = data.Where(x => x.EVEN == 2);
                            //只有一個結果(SDMD)
                            if (old.Count() == 1)
                            {
                                return old.FirstOrDefault();
                            }
                            else
                            {
                                //如果有多筆 但是 結過都一樣 照樣過
                                var datanum_old = old
                                        .Select(m => new
                                        {
                                            ZIPCODE = string.IsNullOrEmpty(m.ZIPCODE) ? string.Empty : m.ZIPCODE,
                                            ZIP3A = string.IsNullOrEmpty(m.ZIP3A) ? string.Empty : m.ZIP3A,
                                            station_name = string.IsNullOrEmpty(m.station_name) ? string.Empty : m.station_name,
                                            station_scode = string.IsNullOrEmpty(m.station_scode) ? string.Empty : m.station_scode,
                                            sd_no = string.IsNullOrEmpty(m.sd_no) ? string.Empty : m.sd_no,
                                            md_no = string.IsNullOrEmpty(m.md_no) ? string.Empty : m.md_no,
                                            put_order = string.IsNullOrEmpty(m.put_order) ? string.Empty : m.put_order,
                                        })
                                        .Distinct()
                                        .ToList();

                                //只有一個結果(SDMD)
                                if (datanum_old.Count() == 1)
                                {
                                    return old.FirstOrDefault();
                                }
                                else//如果有不一樣再來筆數字區間
                                {
                                    var datanum = old.Where(x => (x.NO_BGN <= num || x.NO_BGN == 0) && (x.NO_END >= num || x.NO_END == 0)).ToList();

                                    if (datanum.Count() == 1)
                                    {
                                        return datanum.FirstOrDefault();
                                    }
                                    else if (datanum.Count() == 0) //檢查特殊狀況 CMP_LABLE ='I'
                                    {
                                        var I = old.Where(x => x.LANE != 0 && x.NO_BGN == 0 && x.NO_END == 0);

                                        if (I.Count() == 1)
                                        {
                                            return I.FirstOrDefault();
                                        }
                                    }
                                    else if (datanum.Count() > 1)
                                    {
                                        //如果有多筆 但是 結過都一樣 照樣過
                                        var datanum_one = datanum
                                                .Select(m => new
                                                {
                                                    ZIPCODE = string.IsNullOrEmpty(m.ZIPCODE) ? string.Empty : m.ZIPCODE,
                                                    ZIP3A = string.IsNullOrEmpty(m.ZIP3A) ? string.Empty : m.ZIP3A,
                                                    station_name = string.IsNullOrEmpty(m.station_name) ? string.Empty : m.station_name,
                                                    station_scode = string.IsNullOrEmpty(m.station_scode) ? string.Empty : m.station_scode,
                                                    sd_no = string.IsNullOrEmpty(m.sd_no) ? string.Empty : m.sd_no,
                                                    md_no = string.IsNullOrEmpty(m.md_no) ? string.Empty : m.md_no,
                                                    put_order = string.IsNullOrEmpty(m.put_order) ? string.Empty : m.put_order,
                                                })
                                                .Distinct()
                                                .ToList();

                                        //只有一個結果(SDMD)
                                        if (datanum_one.Count() == 1)
                                        {
                                            return datanum.FirstOrDefault();
                                        }
                                        else//比到最後都沒有 選擇沒有巷弄的 
                                        {
                                            return datanum.Where(x => x.LANE == 0).FirstOrDefault();

                                        }
                                    }
                                }
                            }

                        }

                    }

                }
            }



            return null;
        }



        /// <summary>
        /// 取SDMD
        /// </summary>
        /// <param name="fSEAddressEntity"></param>
        /// <returns></returns>
        private async Task<orgArea_Condition> GetSDMDBYorgArea(Map8Result _map8Result)
        {
            orgArea_Condition orgArea_Condition = new orgArea_Condition();



            //先找3+2
            var info32 = ParsingMap8SDMD(_map8Result, "5");

            if (info32 != null)
            {
                return info32;
            }

            //到這代表3+2沒找到
            var info33 = ParsingMap8SDMD(_map8Result, "6");

            if (info33 != null)
            {
                return info33;
            }

            return null;//null

        }


        /// <summary>
        /// 取SDMD
        /// </summary>
        /// <param name="fSEAddressEntity"></param>
        /// <returns></returns>
        private async Task<List<orgArea_Condition>> GetByFSEAddressEntity(FSEAddressEntity fSEAddressEntity)
        {
            try
            {

                var OrgAreasList = _orgAreaList;

                string postRoad = fSEAddressEntity.PostRoad;
                string postStreet = fSEAddressEntity.PostStreet;

                if (fSEAddressEntity.PostRoad != null && fSEAddressEntity.PostRoad.Length > 0 && fSEAddressEntity.PostRoad.Substring(fSEAddressEntity.PostRoad.Length - 1) == "里")
                {
                    postRoad = fSEAddressEntity.PostRoad[0..^1];
                }
                //排除所有NULL
                OrgAreasList = OrgAreasList.Where(x => !string.IsNullOrEmpty(x.CITY) && !string.IsNullOrEmpty(x.AREA) && !string.IsNullOrEmpty(x.ROAD) && !string.IsNullOrEmpty(x.EROAD)).ToList();

                var data = from orgArea in OrgAreasList where orgArea.CITY.Equals(fSEAddressEntity.City) && orgArea.AREA.Equals(fSEAddressEntity.District) && (orgArea.ROAD.Equals(postRoad) || orgArea.ROAD.Equals(postStreet)) select orgArea;

                List<orgArea_Condition> output = new List<orgArea_Condition>();

                List<orgArea_Condition> orgAreas = data.ToList();

                //if (orgAreas.Count == 0)
                //{
                //    var data1 = from orgArea in OrgAreasList where orgArea.CITY.Equals(fSEAddressEntity.City) && (orgArea.ROAD.Equals(fSEAddressEntity.PostRoad) || orgArea.ROAD.Equals(fSEAddressEntity.PostStreet)) select orgArea;

                //    orgAreas = data1.ToList();
                //}

                //if (orgAreas.Count == 0)
                //{
                //    var data1 = from orgArea in OrgAreasList where orgArea.CITY.Equals(fSEAddressEntity.City) && (orgArea.EROAD.Equals(fSEAddressEntity.PinYinRoad) || orgArea.EROAD.Equals(fSEAddressEntity.PinYinStreet)) select orgArea;

                //    orgAreas = data1.ToList();
                //}

                if (orgAreas.Count == 1)
                {
                    output = orgAreas;
                }
                else if (orgAreas.Count > 1)
                {
                    double findNo = 0;

                    try
                    {
                        findNo = Convert.ToDouble(fSEAddressEntity.FindNo);
                    }
                    catch
                    {

                    }

                    if (findNo == 0)
                    {
                        output.Add(orgAreas[0]);
                    }
                    else
                    {
                        bool even = true;

                        if ((findNo % 2) == 1)
                        {
                            even = false;
                        }

                        if (even)
                        {
                            var eachData = (from aa in orgAreas
                                            where aa.NO_BGN <= findNo && aa.NO_END >= findNo && aa.EVEN == 2
                                            select aa).ToList();

                            if (eachData.Count == 0)
                            {
                                eachData = (from aa in orgAreas
                                            where aa.NO_BGN <= findNo && aa.NO_END >= findNo && aa.EVEN == 0
                                            select aa).ToList();
                            }

                            if (eachData.Count == 0)
                            {
                                eachData = (from aa in orgAreas
                                            where aa.NO_BGN <= findNo && aa.NO_END >= findNo
                                            select aa).ToList();
                            }
                            if (eachData.Count == 0)
                            {
                                eachData = (from aa in orgAreas
                                            where aa.NO_BGN == findNo && aa.NO_END == 0
                                            select aa).ToList();
                            }

                            var eachData99 = (from aa in orgAreas
                                              where StationCode == "99"
                                              select aa).FirstOrDefault();

                            if (eachData99 != null)
                            {
                                output.Add(eachData99);
                            }


                            if (eachData.Count == 0)
                            {
                                output.Add(orgAreas[0]);
                            }
                            else
                            {
                                output.Add(eachData[0]);
                            }
                        }
                        else
                        {
                            var eachData = (from aa in orgAreas
                                            where aa.NO_BGN <= findNo && aa.NO_END >= findNo && aa.EVEN == 1
                                            select aa).ToList();

                            if (eachData.Count == 0)
                            {
                                eachData = (from aa in orgAreas
                                            where aa.NO_BGN <= findNo && aa.NO_END >= findNo && aa.EVEN == 0
                                            select aa).ToList();
                            }

                            if (eachData.Count == 0)
                            {
                                eachData = (from aa in orgAreas
                                            where aa.NO_BGN <= findNo && aa.NO_END >= findNo
                                            select aa).ToList();
                            }
                            if (eachData.Count == 0)
                            {
                                eachData = (from aa in orgAreas
                                            where aa.NO_BGN == findNo && aa.NO_END == 0
                                            select aa).ToList();
                            }


                            var eachData99 = (from aa in orgAreas
                                              where StationCode == "99"
                                              select aa).FirstOrDefault();

                            if (eachData99 != null)
                            {
                                output.Add(eachData99);
                            }


                            if (eachData.Count == 0)
                            {
                                output.Add(orgAreas[0]);
                            }
                            else
                            {
                                output.Add(eachData[0]);
                            }
                        }

                    }
                }

                return output;
            }
            catch (Exception)
            {

                return null;
            }

        }
    }
}
