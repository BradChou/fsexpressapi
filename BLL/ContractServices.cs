﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using DAL.Model.Contract_Condition;
using DAL.Model.Condition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Contract_DA;
using BLL.Model.ScheduleAPI.Res.Contract;
using DAL.DA;
using static Common.Setting.JobSetting;
using Common;
using static Common.Setting.EnumSetting;
using System.Transactions;
using DAL.FSE01_DA;
using DAL;

namespace BLL
{
    public class ContractServices
    {
        //private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _config;
        private IScheduleTask_DA _ScheduleTask_DA;
        private IMoneySettle_Detail_DA _MoneySettle_Detail_DA;
        private IProductManage_DA _ProductManage_DA;
        private IProductValuationManage_DA _ProductValuationManage_DA;
        private IProductValuationManageScope_DA _ProductValuationManageScope_DA;
        private ICustomerSettle_Fee_DA _CustomerSettle_Fee_DA;
        private ICBM_Detail_DA _CBM_Detail_DA;
        private IAdditionalFeeManage_DA _AdditionalFeeManage_DA;
        private IAdditionalFeeManageScope_DA _AdditionalFeeManageScope_DA;
        private ISpecialArea_DA _SpecialArea_DA;
        private IMoneySettleA_DA _MoneySettleA_DA;
        private IMoneySettleAScope_DA _MoneySettleAScope_DA;
        private IMoneySettleC_DA _MoneySettleC_DA;
        private IMoneySettleCScope_DA _MoneySettleCScope_DA;
        private ISettleA_Fee_DA _SettleA_Fee_DA;
        private ISettleB_Fee_DA _SettleB_Fee_DA;
        private ISettleC_Fee_DA _SettleC_Fee_DA;
        private ItbCustomers_DA _tbCustomers_DA;
        private ItbStation_DA _tbStation_DA;
        private IItemCodes_DA _itemCodes_DA;
        private ILogRecord_DA _LogRecord;
        private IDailyMoneySettleDetail_Temp_DA _DailyMoneySettleDetail_Temp_DA;
        private IDailyCBMDetail_Temp_DA _DailyCBMDetail_Temp_DA;
        private IDailyCustomerSettle_Fee_DA _DailyCustomerSettle_Fee_DA;
        private IDailySettleA_Fee_DA _DailySettleA_Fee_DA;
        private IDailySettleB_Fee_DA _DailySettleB_Fee_DA;
        private IDailySettleC_Fee_DA _DailySettleC_Fee_DA;
        private IDailyCustomerSettle_Fee_Temp_DA _DailyCustomerSettle_Fee_Temp_DA;
        private IDailySettleA_Fee_Temp_DA _DailySettleA_Fee_Temp_DA;
        private IDailySettleB_Fee_Temp_DA _DailySettleB_Fee_Temp_DA;
        private IDailySettleC_Fee_Temp_DA _DailySettleC_Fee_Temp_DA;
        private IStoredProcedure_DA _StoredProcedure_DA;
        private IDailyCalErrMsgLog_DA _DailyCalErrMsgLog_DA;

        public ContractServices(
            //IHttpContextAccessor httpContextAccessor,
            IConfiguration config,
            IScheduleTask_DA ScheduleTask_DA,
            IMoneySettle_Detail_DA MoneySettle_Detail_DA,
            IProductManage_DA productManage_DA,
            IProductValuationManage_DA productValuationManage_DA,
            IProductValuationManageScope_DA productValuationManageScope_DA,
            ICustomerSettle_Fee_DA customerSettle_Fee_DA,
            ICBM_Detail_DA cBM_Detail_DA,
            IAdditionalFeeManage_DA additionalFeeManage_DA,
            IAdditionalFeeManageScope_DA additionalFeeManageScope_DA,
            ISpecialArea_DA specialArea_DA,
            IMoneySettleA_DA moneySettleA_DA,
            IMoneySettleAScope_DA moneySettleAScope_DA,
            IMoneySettleC_DA moneySettleC_DA,
            IMoneySettleCScope_DA moneySettleCScope_DA,
            ISettleA_Fee_DA settleA_Fee_DA,
            ISettleB_Fee_DA settleB_Fee_DA,
            ISettleC_Fee_DA settleC_Fee_DA,
            ItbCustomers_DA tbCustomers_DA,
            ItbStation_DA tbStation_DA,
            IItemCodes_DA itemCodes_DA,
            ILogRecord_DA LogRecord,
            IDailyCustomerSettle_Fee_DA dailyCustomerSettle_Fee_DA,
            IDailySettleA_Fee_DA dailySettleA_Fee_DA,
            IDailySettleB_Fee_DA dailySettleB_Fee_DA,
            IDailySettleC_Fee_DA dailySettleC_Fee_DA,
            IDailyCustomerSettle_Fee_Temp_DA dailyCustomerSettle_Fee_Temp_DA,
            IDailySettleA_Fee_Temp_DA dailySettleA_Fee_Temp_DA,
            IDailySettleB_Fee_Temp_DA dailySettleB_Fee_Temp_DA,
            IDailySettleC_Fee_Temp_DA dailySettleC_Fee_Temp_DA,
            IDailyMoneySettleDetail_Temp_DA dailyMoneySettleDetail_Temp_DA,
            IDailyCBMDetail_Temp_DA dailyCBMDetail_Temp_DA,
            IStoredProcedure_DA storedProcedure_DA,
            IDailyCalErrMsgLog_DA dailyCalErrMsgLog_DA
            )
        {
            //_httpContextAccessor = httpContextAccessor;
            _config = config;
            _ScheduleTask_DA = ScheduleTask_DA;
            _MoneySettle_Detail_DA = MoneySettle_Detail_DA;
            _ProductManage_DA = productManage_DA;
            _ProductValuationManage_DA = productValuationManage_DA;
            _ProductValuationManageScope_DA = productValuationManageScope_DA;
            _CustomerSettle_Fee_DA = customerSettle_Fee_DA;
            _CBM_Detail_DA = cBM_Detail_DA;
            _AdditionalFeeManage_DA = additionalFeeManage_DA;
            _AdditionalFeeManageScope_DA = additionalFeeManageScope_DA;
            _SpecialArea_DA = specialArea_DA;
            _MoneySettleA_DA = moneySettleA_DA;
            _MoneySettleAScope_DA = moneySettleAScope_DA;
            _MoneySettleC_DA = moneySettleC_DA;
            _MoneySettleCScope_DA = moneySettleCScope_DA;
            _SettleA_Fee_DA = settleA_Fee_DA;
            _SettleB_Fee_DA = settleB_Fee_DA;
            _SettleC_Fee_DA = settleC_Fee_DA;
            _tbCustomers_DA = tbCustomers_DA;
            _tbStation_DA = tbStation_DA;
            _itemCodes_DA = itemCodes_DA;
            _LogRecord = LogRecord;
            _DailyCustomerSettle_Fee_DA = dailyCustomerSettle_Fee_DA;
            _DailySettleA_Fee_DA = dailySettleA_Fee_DA;
            _DailySettleB_Fee_DA = dailySettleB_Fee_DA;
            _DailySettleC_Fee_DA = dailySettleC_Fee_DA;
            _DailyCustomerSettle_Fee_Temp_DA = dailyCustomerSettle_Fee_Temp_DA;
            _DailySettleA_Fee_Temp_DA = dailySettleA_Fee_Temp_DA;
            _DailySettleB_Fee_Temp_DA = dailySettleB_Fee_Temp_DA;
            _DailySettleC_Fee_Temp_DA = dailySettleC_Fee_Temp_DA;
            _DailyMoneySettleDetail_Temp_DA = dailyMoneySettleDetail_Temp_DA;
            _DailyCBMDetail_Temp_DA = dailyCBMDetail_Temp_DA;
            _StoredProcedure_DA = storedProcedure_DA;
            _DailyCalErrMsgLog_DA = dailyCalErrMsgLog_DA;
        }
        /// <summary>
        /// 結算計算
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task SettleCaculate()
        {
            var result = new List<SettleCaculate_Res>();
            var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.SettleCaculateJob)).FirstOrDefault();
            if (info == null)
            {
                throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            }
            var detailData = await _MoneySettle_Detail_DA.GetInfoGreaterThanId(2542104);//2233702
            //var detailData = await _MoneySettle_Detail_DA.GetInfoInId(new int[] { 2522153 });//2522153
            //var detailData = await _MoneySettle_Detail_DA.GetInfoGreaterThanIdTEST();
            //var  detailData = await _MoneySettle_Detail_DA.GetInfoGreaterBetweenId(69498, 69596);
            var productList = await _ProductManage_DA.GetpublicAll();
            var valuationList = await _ProductValuationManage_DA.GetpublicAll();
            var valuationScopeList = await _ProductValuationManageScope_DA.GetpublicAll();
            var settleAList = await _MoneySettleA_DA.GetpublicAll();
            var settleAScopeList = await _MoneySettleAScope_DA.GetpublicAll();
            var settleCList = await _MoneySettleC_DA.GetpublicAll();
            var settleCScopeList = await _MoneySettleCScope_DA.GetpublicAll();
            var addtionList = await _AdditionalFeeManage_DA.GetPublicAll();
            var addtionScopeList = await _AdditionalFeeManageScope_DA.GetPublicAll();
            var specialList = await _SpecialArea_DA.GetPublicAll();
            var customrtList = await _tbCustomers_DA.GetAll();
            var stationList = await _tbStation_DA.GetAll();
            var insertCustomerSettle = new List<CustomerSettle_Fee_Condition>();
            var insertMoneySettleA = new List<SettleA_Fee_Condition>();
            var insertMoneySettleB = new List<SettleB_Fee_Condition>();
            var insertMoneySettleC = new List<SettleC_Fee_Condition>();
            var dateNow = DateTime.Now;
            var specList = (await _itemCodes_DA.GetSpecCodeId()).Select(x => x.CodeId).ToList();
            //List<string> itemDrop = new List<string> { "B004", "B005", "X001" };
            //foreach (var item in itemDrop)
            //{
            //    specList.Remove(item);
            //}
            var cbmDetailList = await _CBM_Detail_DA.GetInfoTotal(info.StartId);

            try
            {
                if (detailData != null)
                {
                    foreach (var detail in detailData)
                    {
                        info.StartId = detail.id;
                        info.UpdateDate = dateNow;
                        var custmorBill = new CustomerSettle_Fee_Condition();
                        var settleABill = new SettleA_Fee_Condition();
                        var settleBBill = new SettleB_Fee_Condition();
                        var settleCBill = new SettleC_Fee_Condition();
                        custmorBill.Detail_Id = detail.id;
                        custmorBill.CreateDate = dateNow;
                        settleABill.Detail_Id = detail.id;
                        settleABill.CreateDate = dateNow;
                        settleBBill.Detail_Id = detail.id;
                        settleBBill.CreateDate = dateNow;
                        settleCBill.Detail_Id = detail.id;
                        settleCBill.CreateDate = dateNow;
                        var product = productList.FirstOrDefault(x => x.ProductId == detail.ProductId);
                        if (product != null)
                        {
                            #region 客戶運費
                            //客代找產品價格
                            var customerValuationList = valuationList.Where(
                            x => x.ProductId == detail.ProductId && x.CustomerCode == detail.CustomerCode
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate.Value) >= detail.Request_CreateDate
                            && x.DeliveryType == detail.DeliveryType
                            ).ToList();
                            if (customerValuationList.Count() == 0)
                            {
                                customerValuationList = valuationList.Where(
                                x => x.ProductId == detail.ProductId && x.CustomerCode == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate.Value) >= detail.Request_CreateDate
                                && x.DeliveryType == detail.DeliveryType
                                ).ToList();
                            }
                            //加入沒有的項目公版
                            customerValuationList.AddRange(valuationList.Where(
                            x => x.ProductId == detail.ProductId && x.CustomerCode == "-1"
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate.Value) >= detail.Request_CreateDate
                            && x.DeliveryType == detail.DeliveryType && !customerValuationList.Select(x => x.Spec).Contains(x.Spec)
                            ).ToList());

                            var valuationAList = settleAList.Where(
                            x => x.ProductId == detail.ProductId && x.Station == detail.SendStation && x.CustomerCode == detail.CustomerCode
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                            && x.DeliveryType == detail.DeliveryType
                            ).ToList();
                            if (valuationAList.Count() == 0)
                            {
                                valuationAList = settleAList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1" && x.CustomerCode == detail.CustomerCode
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.DeliveryType == detail.DeliveryType
                                ).ToList();
                            }
                            if (valuationAList.Count() == 0)
                            {
                                valuationAList = settleAList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == detail.SendStation && x.CustomerCode == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.DeliveryType == detail.DeliveryType
                                ).ToList();
                            }
                            if (valuationAList.Count() == 0)
                            {
                                valuationAList = settleAList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1" && x.CustomerCode == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.DeliveryType == detail.DeliveryType
                                ).ToList();
                            }
                            //加入沒有的項目公版
                            valuationAList.AddRange(settleAList.Where(
                            x => x.ProductId == detail.ProductId && x.Station == "-1" && x.CustomerCode == "-1"
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                            && x.DeliveryType == detail.DeliveryType && !valuationAList.Select(x => x.Spec).Contains(x.Spec)
                            ).ToList());

                            //C段結算以司機站所看
                            var valuationCList = settleCList.Where(
                            x => x.ProductId == detail.ProductId && x.Station == detail.ArriveDriverStation
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                            && x.AreaType == "-1"
                            ).ToList();
                            if (valuationCList.Count() == 0)
                            {
                                valuationCList = settleCList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == detail.ArriveDriverStation
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.AreaType == detail.EggArea
                                ).ToList();
                            }
                            if (valuationCList.Count() == 0)
                            {
                                valuationCList = settleCList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.AreaType == detail.EggArea
                                ).ToList();
                            }
                            if (valuationCList.Count() == 0)
                            {
                                valuationCList = settleCList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.AreaType == "-1"
                                ).ToList();
                            }
                            //加入沒有的項目公版
                            valuationCList.AddRange(settleCList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.AreaType == "-1" && !valuationCList.Select(x => x.Spec).Contains(x.Spec)
                            ).ToList());

                            var detailCBM = new CBM_Detail_Condition();
                            var customerValuation = new ProductValuationManage_Condition();
                            var valuation = new ProductValuationManageScope_Condition();
                            var stationASettle = new MoneySettleA_Condition();
                            var stationA = new MoneySettleAScope_Condition();
                            var stationCSettle = new MoneySettleC_Condition();
                            var stationC = new MoneySettleCScope_Condition();
                            var customerAdditionList = new List<AdditionalFeeManage_Condition>();
                            var publicAdditionList = new List<AdditionalFeeManage_Condition>();
                            var detailCBMList = cbmDetailList.Where(x => x.DetailID == detail.id);//await _CBM_Detail_DA.GetInfoById(detail.id);
                            //以項目抓取價格
                            switch (product.PriceMode)
                            {
                                case "S":
                                    var detailItemS = detailCBMList.FirstOrDefault();
                                    if (!specList.Contains(detailItemS.CBM))
                                    {
                                        break;
                                    }
                                    custmorBill.SN = 1;
                                    settleABill.SN = 1;
                                    settleBBill.SN = 1;
                                    settleCBill.SN = 1;
                                    await SettleRowDataAsync(detail, customerValuationList
                                    , valuationScopeList, valuationAList, settleAScopeList, valuationCList, settleCScopeList
                                    , custmorBill, settleABill, settleBBill, settleCBill, addtionList, addtionScopeList
                                    , specialList, customrtList, stationList, detailItemS);
                                    insertCustomerSettle.Add(custmorBill);
                                    insertMoneySettleA.Add(settleABill);
                                    insertMoneySettleB.Add(settleBBill);
                                    insertMoneySettleC.Add(settleCBill);

                                    break;
                                case "M":
                                    //var detailCBMList = await _CBM_Detail_DA.GetInfoById(detail.id);
                                    foreach (var detailItem in detailCBMList)
                                    {
                                        if (!specList.Contains(detailItem.CBM))
                                        {
                                            continue;
                                        }
                                        var custmorBillM = new CustomerSettle_Fee_Condition();
                                        var settleABillM = new SettleA_Fee_Condition();
                                        var settleBBillM = new SettleB_Fee_Condition();
                                        var settleCBillM = new SettleC_Fee_Condition();
                                        custmorBillM.SN = detailItem.SN;
                                        settleABillM.SN = detailItem.SN;
                                        settleBBillM.SN = detailItem.SN;
                                        settleCBillM.SN = detailItem.SN;
                                        custmorBillM.Detail_Id = detail.id;
                                        custmorBillM.CreateDate = dateNow;
                                        settleABillM.Detail_Id = detail.id;
                                        settleABillM.CreateDate = dateNow;
                                        settleBBillM.Detail_Id = detail.id;
                                        settleBBillM.CreateDate = dateNow;
                                        settleCBillM.Detail_Id = detail.id;
                                        settleCBillM.CreateDate = dateNow;
                                        await SettleRowDataAsync(detail, customerValuationList
                                        , valuationScopeList, valuationAList, settleAScopeList, valuationCList, settleCScopeList
                                        , custmorBillM, settleABillM, settleBBillM, settleCBillM, addtionList, addtionScopeList
                                        , specialList, customrtList, stationList, detailItem);

                                        insertCustomerSettle.Add(custmorBillM);
                                        insertMoneySettleA.Add(settleABillM);
                                        insertMoneySettleB.Add(settleBBillM);
                                        insertMoneySettleC.Add(settleCBillM);

                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }

                    }
                    //多表Transaction
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _CustomerSettle_Fee_DA.InsertBatch(insertCustomerSettle);
                        _transactionScope.Complete();
                    }
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _SettleA_Fee_DA.InsertBatch(insertMoneySettleA);
                        _transactionScope.Complete();
                    }
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _SettleB_Fee_DA.InsertBatch(insertMoneySettleB);
                        _transactionScope.Complete();
                    }
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _SettleC_Fee_DA.InsertBatch(insertMoneySettleC);
                        _transactionScope.Complete();
                    }
                    await _ScheduleTask_DA.UpdateSetStartId(info);
                }

            }
            catch (Exception e)
            {

                throw e;
            }

            return;
        }

        public async Task SettleCaculate_new()
        {
            _LogRecord.Save("SettleCaculate_new", "1", "start");

            var result = new List<SettleCaculate_Res>();
            var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.SettleCaculateJob)).FirstOrDefault();
            if (info == null)
            {
                throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            }
            var cbmDetailList = await _CBM_Detail_DA.GetInfoTotal(2022112300100000000);
            var detailData = await _MoneySettle_Detail_DA.GetInfoGreaterThanId(2022112300100000000);//2022092900400000418
            //var detailData = await _MoneySettle_Detail_DA.GetInfoInId(new int[] { 1833069 });
            //var detailData = await _MoneySettle_Detail_DA.GetInfoGreaterThanIdTEST();
            //var detailData = await _MoneySettle_Detail_DA.GetInfoGreaterBetweenId(2022092900400000000, 2022092900400000418);
            var productList = await _ProductManage_DA.GetpublicAll();
            var valuationList = await _ProductValuationManage_DA.GetpublicAll();
            var valuationScopeList = await _ProductValuationManageScope_DA.GetpublicAll();
            var settleAList = await _MoneySettleA_DA.GetpublicAll();
            var settleAScopeList = await _MoneySettleAScope_DA.GetpublicAll();
            var settleCList = await _MoneySettleC_DA.GetpublicAll();
            var settleCScopeList = await _MoneySettleCScope_DA.GetpublicAll();
            var addtionList = await _AdditionalFeeManage_DA.GetPublicAll();
            var addtionScopeList = await _AdditionalFeeManageScope_DA.GetPublicAll();
            var specialList = await _SpecialArea_DA.GetPublicAll();
            var customrtList = await _tbCustomers_DA.GetAll();
            var stationList = await _tbStation_DA.GetAll();
            var insertCustomerSettle = new List<CustomerSettle_Fee_Condition>();
            var insertMoneySettleA = new List<SettleA_Fee_Condition>();
            var insertMoneySettleB = new List<SettleB_Fee_Condition>();
            var insertMoneySettleC = new List<SettleC_Fee_Condition>();
            var dateNow = DateTime.Now;
            var specList = (await _itemCodes_DA.GetSpecCodeId()).Select(x => x.CodeId).ToList();


            //List<string> itemDrop = new List<string> { "B004", "B005", "X001" };
            //foreach (var item in itemDrop)
            //{
            //    specList.Remove(item);
            //}

            _LogRecord.Save("SettleCaculate_new", "2", "");

            try
            {
                if (detailData != null)
                {
                    foreach (var detail in detailData)
                    {
                        if (insertCustomerSettle.Count % 5000 == 0)
                        {
                            _LogRecord.Save("SettleCaculate_new", "3", insertCustomerSettle.Count.ToString());
                        }

                        info.StartId = detail.id;
                        info.UpdateDate = dateNow;
                        var custmorBill = new CustomerSettle_Fee_Condition();
                        var settleABill = new SettleA_Fee_Condition();
                        var settleBBill = new SettleB_Fee_Condition();
                        var settleCBill = new SettleC_Fee_Condition();
                        custmorBill.Detail_Id = detail.id;
                        custmorBill.CreateDate = dateNow;
                        settleABill.Detail_Id = detail.id;
                        settleABill.CreateDate = dateNow;
                        settleBBill.Detail_Id = detail.id;
                        settleBBill.CreateDate = dateNow;
                        settleCBill.Detail_Id = detail.id;
                        settleCBill.CreateDate = dateNow;

                        //發送站以集貨司機站所判斷
                        detail.ReceiveDriverStation = String.IsNullOrEmpty(detail.ReceiveDriverStation) ? detail.SendStation : detail.ReceiveDriverStation;
                        if (detail.ProductId == "B004")
                        {
                            detail.ProductId = "B003";
                        }
                        var product = productList.FirstOrDefault(x => x.ProductId == detail.ProductId);
                        if (product != null)
                        {
                            #region 客戶運費
                            //客代找產品價格
                            var customerValuationList = valuationList.Where(
                            x => x.ProductId == detail.ProductId && x.CustomerCode == detail.CustomerCode
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate.Value) >= detail.Request_CreateDate
                            && x.DeliveryType == detail.DeliveryType
                            ).ToList();
                            if (customerValuationList.Count() == 0)
                            {
                                customerValuationList = valuationList.Where(
                                x => x.ProductId == detail.ProductId && x.CustomerCode == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate.Value) >= detail.Request_CreateDate
                                && x.DeliveryType == detail.DeliveryType
                                ).ToList();
                            }
                            //加入沒有的項目公版
                            customerValuationList.AddRange(valuationList.Where(
                            x => x.ProductId == detail.ProductId && x.CustomerCode == "-1"
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate.Value) >= detail.Request_CreateDate
                            && x.DeliveryType == detail.DeliveryType && !customerValuationList.Select(x => x.Spec).Contains(x.Spec)
                            ).ToList());

                            var valuationAList = settleAList.Where(
                            x => x.ProductId == detail.ProductId && x.Station == detail.ReceiveDriverStation && x.CustomerCode == detail.CustomerCode
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                            && x.DeliveryType == detail.DeliveryType
                            ).ToList();
                            if (valuationAList.Count() == 0)
                            {
                                valuationAList = settleAList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1" && x.CustomerCode == detail.CustomerCode
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.DeliveryType == detail.DeliveryType
                                ).ToList();
                            }
                            if (valuationAList.Count() == 0)
                            {
                                valuationAList = settleAList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == detail.ReceiveDriverStation && x.CustomerCode == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.DeliveryType == detail.DeliveryType
                                ).ToList();
                            }
                            if (valuationAList.Count() == 0)
                            {
                                valuationAList = settleAList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1" && x.CustomerCode == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.DeliveryType == detail.DeliveryType
                                ).ToList();
                            }
                            //加入沒有的項目公版
                            valuationAList.AddRange(settleAList.Where(
                            x => x.ProductId == detail.ProductId && x.Station == "-1" && x.CustomerCode == "-1"
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                            && x.DeliveryType == detail.DeliveryType && !valuationAList.Select(x => x.Spec).Contains(x.Spec)
                            ).ToList());

                            //C段結算以司機站所看
                            var valuationCList = settleCList.Where(
                            x => x.ProductId == detail.ProductId && x.Station == detail.ArriveDriverStation
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                            && x.AreaType == "-1"
                            ).ToList();
                            if (valuationCList.Count() == 0)
                            {
                                valuationCList = settleCList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == detail.ArriveDriverStation
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.AreaType == detail.EggArea
                                ).ToList();
                            }
                            if (valuationCList.Count() == 0)
                            {
                                valuationCList = settleCList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.AreaType == detail.EggArea
                                ).ToList();
                            }
                            if (valuationCList.Count() == 0)
                            {
                                valuationCList = settleCList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.AreaType == "-1"
                                ).ToList();
                            }
                            //加入沒有的項目公版
                            valuationCList.AddRange(settleCList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.AreaType == "-1" && !valuationCList.Select(x => x.Spec).Contains(x.Spec)
                            ).ToList());

                            var detailCBM = new CBM_Detail_Condition();
                            var customerValuation = new ProductValuationManage_Condition();
                            var valuation = new ProductValuationManageScope_Condition();
                            var stationASettle = new MoneySettleA_Condition();
                            var stationA = new MoneySettleAScope_Condition();
                            var stationCSettle = new MoneySettleC_Condition();
                            var stationC = new MoneySettleCScope_Condition();
                            var customerAdditionList = new List<AdditionalFeeManage_Condition>();
                            var publicAdditionList = new List<AdditionalFeeManage_Condition>();
                            var detailCBMList = cbmDetailList.Where(x => x.DetailID == detail.id).ToList();// await _CBM_Detail_DA.GetInfoById(detail.id);
                            //以項目抓取價格
                            switch (product.PriceMode)
                            {
                                case "S":
                                    var detailItemS = detailCBMList.FirstOrDefault();
                                    if (!specList.Contains(detailItemS.CBM))
                                    {
                                        continue;
                                    }
                                    custmorBill.SN = 1;
                                    settleABill.SN = 1;
                                    settleBBill.SN = 1;
                                    settleCBill.SN = 1;
                                    await SettleRowDataAsync(detail, customerValuationList
                                    , valuationScopeList, valuationAList, settleAScopeList, valuationCList, settleCScopeList
                                    , custmorBill, settleABill, settleBBill, settleCBill, addtionList, addtionScopeList
                                    , specialList, customrtList, stationList, detailItemS);
                                    insertCustomerSettle.Add(custmorBill);
                                    insertMoneySettleA.Add(settleABill);
                                    insertMoneySettleB.Add(settleBBill);
                                    insertMoneySettleC.Add(settleCBill);

                                    break;
                                case "M":
                                    //var detailCBMList = await _CBM_Detail_DA.GetInfoById(detail.id);
                                    foreach (var detailItem in detailCBMList)
                                    {
                                        if (!specList.Contains(detailItem.CBM))
                                        {
                                            continue;
                                        }
                                        var custmorBillM = new CustomerSettle_Fee_Condition();
                                        var settleABillM = new SettleA_Fee_Condition();
                                        var settleBBillM = new SettleB_Fee_Condition();
                                        var settleCBillM = new SettleC_Fee_Condition();
                                        custmorBillM.SN = detailItem.SN;
                                        settleABillM.SN = detailItem.SN;
                                        settleBBillM.SN = detailItem.SN;
                                        settleCBillM.SN = detailItem.SN;
                                        custmorBillM.Detail_Id = detail.id;
                                        custmorBillM.CreateDate = dateNow;
                                        settleABillM.Detail_Id = detail.id;
                                        settleABillM.CreateDate = dateNow;
                                        settleBBillM.Detail_Id = detail.id;
                                        settleBBillM.CreateDate = dateNow;
                                        settleCBillM.Detail_Id = detail.id;
                                        settleCBillM.CreateDate = dateNow;
                                        await SettleRowDataAsync(detail, customerValuationList
                                        , valuationScopeList, valuationAList, settleAScopeList, valuationCList, settleCScopeList
                                        , custmorBillM, settleABillM, settleBBillM, settleCBillM, addtionList, addtionScopeList
                                        , specialList, customrtList, stationList, detailItem);

                                        insertCustomerSettle.Add(custmorBillM);
                                        insertMoneySettleA.Add(settleABillM);
                                        insertMoneySettleB.Add(settleBBillM);
                                        insertMoneySettleC.Add(settleCBillM);

                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }

                    }

                    _LogRecord.Save("SettleCaculate_new", "4", "0.insertCustomerSettle");
                    //多表Transaction
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _CustomerSettle_Fee_DA.InsertBatch(insertCustomerSettle);
                        _transactionScope.Complete();
                    }
                    _LogRecord.Save("SettleCaculate_new", "4", "1.insertMoneySettleA");
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _SettleA_Fee_DA.InsertBatch(insertMoneySettleA);
                        _transactionScope.Complete();
                    }
                    _LogRecord.Save("SettleCaculate_new", "4", "2.insertMoneySettleB");
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _SettleB_Fee_DA.InsertBatch(insertMoneySettleB);
                        _transactionScope.Complete();
                    }
                    _LogRecord.Save("SettleCaculate_new", "4", "3.insertMoneySettleC");
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _SettleC_Fee_DA.InsertBatch(insertMoneySettleC);
                        _transactionScope.Complete();
                    }
                    _LogRecord.Save("SettleCaculate_new", "4", "4.UpdateSetStartId");

                    await _ScheduleTask_DA.UpdateSetStartId(info);

                    _LogRecord.Save("SettleCaculate_new", "5", "end");

                }

            }
            catch (Exception e)
            {
                _LogRecord.Save("DailySettleRowDataAsyncError", "1", e.Message);
                return;
            }

            return;
        }

        public async Task SettleCaculate_daily()
        {
            _LogRecord.Save("SettleCaculate_daily", "1", "start");

            var detailData = await _DailyMoneySettleDetail_Temp_DA.GetAll();
            //test
            //detailData = detailData.Where(x => x.id <= 2022110900200000005).ToList();
            var productList = await _ProductManage_DA.GetpublicAll();
            var valuationList = await _ProductValuationManage_DA.GetpublicAll();
            var valuationScopeList = await _ProductValuationManageScope_DA.GetpublicAll();
            var settleAList = await _MoneySettleA_DA.GetpublicAll();
            var settleAScopeList = await _MoneySettleAScope_DA.GetpublicAll();
            var settleCList = await _MoneySettleC_DA.GetpublicAll();
            var settleCScopeList = await _MoneySettleCScope_DA.GetpublicAll();
            var addtionList = await _AdditionalFeeManage_DA.GetPublicAll();
            var addtionScopeList = await _AdditionalFeeManageScope_DA.GetPublicAll();
            var specialList = await _SpecialArea_DA.GetPublicAll();
            var customrtList = await _tbCustomers_DA.GetAll();
            var stationList = await _tbStation_DA.GetAll();
            var insertCustomerSettle = new List<DailyCustomerSettle_Fee_Temp_Condition>();
            var insertMoneySettleA = new List<DailySettleA_Fee_Temp_Condition>();
            var insertMoneySettleB = new List<DailySettleB_Fee_Temp_Condition>();
            var insertMoneySettleC = new List<DailySettleC_Fee_Temp_Condition>();
            var dateNow = DateTime.Now;
            var specList = (await _itemCodes_DA.GetSpecCodeId()).Select(x => x.CodeId).ToList();
            var cbmDetailList = await _DailyCBMDetail_Temp_DA.GetInfoTotal();
            var customerNoFeeList = customrtList.Where(x => x.product_type == 4 || x.product_type == 5).ToList();
            var dailyCalErrMsgLogList = new List<DailyCalErrMsgLog_Condition>();
            //List<string> itemDrop = new List<string> { "B004", "B005", "X001" };
            //foreach (var item in itemDrop)
            //{
            //    specList.Remove(item);
            //}

            _LogRecord.Save("SettleCaculate_daily", "2", "");

            try
            {
                if (detailData != null)
                {
                    foreach (var detail in detailData)
                    {
                        if (insertCustomerSettle.Count % 5000 == 0)
                        {
                            _LogRecord.Save("SettleCaculate_daily", "3", insertCustomerSettle.Count.ToString());
                        }
                        var custmorBill = new DailyCustomerSettle_Fee_Temp_Condition();
                        var settleABill = new DailySettleA_Fee_Temp_Condition();
                        var settleBBill = new DailySettleB_Fee_Temp_Condition();
                        var settleCBill = new DailySettleC_Fee_Temp_Condition();
                        custmorBill.Detail_Id = detail.id;
                        custmorBill.CreateDate = dateNow;
                        settleABill.Detail_Id = detail.id;
                        settleABill.CreateDate = dateNow;
                        settleBBill.Detail_Id = detail.id;
                        settleBBill.CreateDate = dateNow;
                        settleCBill.Detail_Id = detail.id;
                        settleCBill.CreateDate = dateNow;

                        //發送站以集貨司機站所判斷
                        detail.ReceiveDriverStation = String.IsNullOrEmpty(detail.ReceiveDriverStation) ? detail.SendStation : detail.ReceiveDriverStation;

                        //排除product_type(4,5)和Checknumber 980開頭，不計算
                        var isNoFee = customerNoFeeList.Exists(x => x.customer_code == detail.CustomerCode);
                        if (isNoFee || detail.CheckNumber.StartsWith("980"))
                        {
                            //確認項次，沒有資料就給項次1，後面才跑得出來
                            var detailCBMList = cbmDetailList.Where(x => x.DetailID == detail.id);
                            detailCBMList = detailCBMList.Count() > 0 ?
                                            detailCBMList :
                                            new List<DailyCBMDetail_Temp_Condition>() { new DailyCBMDetail_Temp_Condition() { SN = 1 } };

                            foreach (var detailItem in detailCBMList)
                            {
                                var custmorBillM = new DailyCustomerSettle_Fee_Temp_Condition();
                                var settleABillM = new DailySettleA_Fee_Temp_Condition();
                                var settleBBillM = new DailySettleB_Fee_Temp_Condition();
                                var settleCBillM = new DailySettleC_Fee_Temp_Condition();

                                custmorBillM.SN = detailItem.SN;
                                settleABillM.SN = detailItem.SN;
                                settleBBillM.SN = detailItem.SN;
                                settleCBillM.SN = detailItem.SN;

                                custmorBillM.Detail_Id = detail.id;
                                custmorBillM.CreateDate = dateNow;
                                settleABillM.Detail_Id = detail.id;
                                settleABillM.CreateDate = dateNow;
                                settleBBillM.Detail_Id = detail.id;
                                settleBBillM.CreateDate = dateNow;
                                settleCBillM.Detail_Id = detail.id;
                                settleCBillM.CreateDate = dateNow;

                                #region 全部 0元
                                custmorBillM.DD_Fee = 0;
                                custmorBillM.EastOverCbm_Fee = 0;
                                custmorBillM.East_Fee = 0;
                                custmorBillM.OverCbm_Fee = 0;
                                custmorBillM.ReceiptFlag_Fee = 0;
                                custmorBillM.SpecialArea_Fee = 0;
                                custmorBillM.Valued_Fee = 0;
                                custmorBillM.Collection_Money = 0;
                                custmorBillM.Total = 0;

                                settleABillM.DD_Fee = 0;
                                settleABillM.OverCbm_Fee = 0;
                                settleABillM.ReceiptFlag_Fee = 0;
                                settleABillM.Valued_Fee = 0;
                                settleABillM.Collection_Money = 0;
                                settleABillM.Total = 0;

                                settleBBillM.DD_Fee = 0;
                                settleBBillM.EastOverCbm_Fee = 0;
                                settleBBillM.East_Fee = 0;
                                settleBBillM.OverCbm_Fee = 0;
                                settleBBillM.ReceiptFlag_Fee = 0;
                                settleBBillM.SpecialArea_Fee = 0;
                                settleBBillM.Valued_Fee = 0;
                                settleBBillM.Collection_Money = 0;
                                settleBBillM.Total = 0;

                                settleCBillM.DD_Fee = 0;
                                settleCBillM.OverCbm_Fee = 0;
                                settleCBillM.ReceiptFlag_Fee = 0;
                                settleCBillM.SpecialArea_Fee = 0;
                                settleCBillM.Collection_Money = 0;
                                settleCBillM.Total = 0;
                                #endregion

                                insertCustomerSettle.Add(custmorBillM);
                                insertMoneySettleA.Add(settleABillM);
                                insertMoneySettleB.Add(settleBBillM);
                                insertMoneySettleC.Add(settleCBillM);
                            }
                            continue;
                        }

                        var product = productList.FirstOrDefault(x => x.ProductId == detail.ProductId);
                        if (product != null)
                        {
                            #region 客戶運費
                            //客代找產品價格
                            var customerValuationList = valuationList.Where(
                            x => x.ProductId == detail.ProductId && x.CustomerCode == detail.CustomerCode
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate.Value) >= detail.Request_CreateDate
                            && x.DeliveryType == detail.DeliveryType
                            ).ToList();
                            if (customerValuationList.Count() == 0)
                            {
                                customerValuationList = valuationList.Where(
                                x => x.ProductId == detail.ProductId && x.CustomerCode == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate.Value) >= detail.Request_CreateDate
                                && x.DeliveryType == detail.DeliveryType
                                ).ToList();
                            }
                            //加入沒有的項目公版
                            customerValuationList.AddRange(valuationList.Where(
                            x => x.ProductId == detail.ProductId && x.CustomerCode == "-1"
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate.Value) >= detail.Request_CreateDate
                            && x.DeliveryType == detail.DeliveryType && !customerValuationList.Select(x => x.Spec).Contains(x.Spec)
                            ).ToList());

                            var valuationAList = settleAList.Where(
                            x => x.ProductId == detail.ProductId && x.Station == detail.ReceiveDriverStation && x.CustomerCode == detail.CustomerCode
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                            && x.DeliveryType == detail.DeliveryType
                            ).ToList();
                            if (valuationAList.Count() == 0)
                            {
                                valuationAList = settleAList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1" && x.CustomerCode == detail.CustomerCode
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.DeliveryType == detail.DeliveryType
                                ).ToList();
                            }
                            if (valuationAList.Count() == 0)
                            {
                                valuationAList = settleAList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == detail.ReceiveDriverStation && x.CustomerCode == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.DeliveryType == detail.DeliveryType
                                ).ToList();
                            }
                            if (valuationAList.Count() == 0)
                            {
                                valuationAList = settleAList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1" && x.CustomerCode == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.DeliveryType == detail.DeliveryType
                                ).ToList();
                            }
                            //加入沒有的項目公版
                            valuationAList.AddRange(settleAList.Where(
                            x => x.ProductId == detail.ProductId && x.Station == "-1" && x.CustomerCode == "-1"
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                            && x.DeliveryType == detail.DeliveryType && !valuationAList.Select(x => x.Spec).Contains(x.Spec)
                            ).ToList());

                            //C段結算以司機站所看
                            var valuationCList = settleCList.Where(
                            x => x.ProductId == detail.ProductId && x.Station == detail.ArriveDriverStation
                            && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                            && x.AreaType == "-1"
                            ).ToList();
                            if (valuationCList.Count() == 0)
                            {
                                valuationCList = settleCList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == detail.ArriveDriverStation
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.AreaType == detail.EggArea
                                ).ToList();
                            }
                            if (valuationCList.Count() == 0)
                            {
                                valuationCList = settleCList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.AreaType == detail.EggArea
                                ).ToList();
                            }
                            if (valuationCList.Count() == 0)
                            {
                                valuationCList = settleCList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.AreaType == "-1"
                                ).ToList();
                            }
                            //加入沒有的項目公版
                            valuationCList.AddRange(settleCList.Where(
                                x => x.ProductId == detail.ProductId && x.Station == "-1"
                                && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                && x.AreaType == "-1" && !valuationCList.Select(x => x.Spec).Contains(x.Spec)
                            ).ToList());

                            var detailCBM = new DailyCBMDetail_Temp_Condition();
                            var customerValuation = new ProductValuationManage_Condition();
                            var valuation = new ProductValuationManageScope_Condition();
                            var stationASettle = new MoneySettleA_Condition();
                            var stationA = new MoneySettleAScope_Condition();
                            var stationCSettle = new MoneySettleC_Condition();
                            var stationC = new MoneySettleCScope_Condition();
                            var customerAdditionList = new List<AdditionalFeeManage_Condition>();
                            var publicAdditionList = new List<AdditionalFeeManage_Condition>();
                            var detailCBMList = cbmDetailList.Where(x => x.DetailID == detail.id).ToList();// await _CBM_Detail_DA.GetInfoById(detail.id);
                            //以項目抓取價格
                            switch (product.PriceMode)
                            {
                                case "S":
                                    var detailItemS = detailCBMList.FirstOrDefault();
                                    if (detailItemS == null)
                                    {
                                        break;
                                    }
                                    if (!specList.Contains(detailItemS.CBM))
                                    {
                                        break;
                                    }
                                    custmorBill.SN = 1;
                                    settleABill.SN = 1;
                                    settleBBill.SN = 1;
                                    settleCBill.SN = 1;
                                    await DailySettleRowDataAsync(detail, customerValuationList
                                    , valuationScopeList, valuationAList, settleAScopeList, valuationCList, settleCScopeList
                                    , custmorBill, settleABill, settleBBill, settleCBill, addtionList, addtionScopeList
                                    , specialList, customrtList, stationList, detailItemS, dailyCalErrMsgLogList);
                                    insertCustomerSettle.Add(custmorBill);
                                    insertMoneySettleA.Add(settleABill);
                                    insertMoneySettleB.Add(settleBBill);
                                    insertMoneySettleC.Add(settleCBill);

                                    break;
                                case "M":
                                    //var detailCBMList = await _CBM_Detail_DA.GetInfoById(detail.id);
                                    foreach (var detailItem in detailCBMList)
                                    {
                                        if (!specList.Contains(detailItem.CBM))
                                        {
                                            continue;
                                        }
                                        var custmorBillM = new DailyCustomerSettle_Fee_Temp_Condition();
                                        var settleABillM = new DailySettleA_Fee_Temp_Condition();
                                        var settleBBillM = new DailySettleB_Fee_Temp_Condition();
                                        var settleCBillM = new DailySettleC_Fee_Temp_Condition();
                                        custmorBillM.SN = detailItem.SN;
                                        settleABillM.SN = detailItem.SN;
                                        settleBBillM.SN = detailItem.SN;
                                        settleCBillM.SN = detailItem.SN;
                                        custmorBillM.Detail_Id = detail.id;
                                        custmorBillM.CreateDate = dateNow;
                                        settleABillM.Detail_Id = detail.id;
                                        settleABillM.CreateDate = dateNow;
                                        settleBBillM.Detail_Id = detail.id;
                                        settleBBillM.CreateDate = dateNow;
                                        settleCBillM.Detail_Id = detail.id;
                                        settleCBillM.CreateDate = dateNow;
                                        await DailySettleRowDataAsync(detail, customerValuationList
                                        , valuationScopeList, valuationAList, settleAScopeList, valuationCList, settleCScopeList
                                        , custmorBillM, settleABillM, settleBBillM, settleCBillM, addtionList, addtionScopeList
                                        , specialList, customrtList, stationList, detailItem, dailyCalErrMsgLogList);

                                        insertCustomerSettle.Add(custmorBillM);
                                        insertMoneySettleA.Add(settleABillM);
                                        insertMoneySettleB.Add(settleBBillM);
                                        insertMoneySettleC.Add(settleCBillM);

                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }

                    }

                    _LogRecord.Save("SettleCaculate_daily", "4", "0.insertCustomerSettle");
                    //多表Transaction
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _DailyCustomerSettle_Fee_Temp_DA.InsertBatch(insertCustomerSettle);
                        _transactionScope.Complete();
                    }
                    _LogRecord.Save("SettleCaculate_daily", "4", "1.insertMoneySettleA");
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _DailySettleA_Fee_Temp_DA.InsertBatch(insertMoneySettleA);
                        _transactionScope.Complete();
                    }
                    _LogRecord.Save("SettleCaculate_daily", "4", "2.insertMoneySettleB");
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _DailySettleB_Fee_Temp_DA.InsertBatch(insertMoneySettleB);
                        _transactionScope.Complete();
                    }
                    _LogRecord.Save("SettleCaculate_daily", "4", "3.insertMoneySettleC");
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _DailySettleC_Fee_Temp_DA.InsertBatch(insertMoneySettleC);
                        _transactionScope.Complete();
                    }
                    _LogRecord.Save("SettleCaculate_daily", "4", "4.DailySettleCaculateUpadte");
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _StoredProcedure_DA.DailySettleCaculateUpadte();
                        _transactionScope.Complete();
                    }
                    _LogRecord.Save("SettleCaculate_daily", "4", "5.dailyCalErrMsgLogList");
                    using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromHours(5), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await _DailyCalErrMsgLog_DA.InsertBatch(dailyCalErrMsgLogList);
                        _transactionScope.Complete();
                    }

                    _LogRecord.Save("SettleCaculate_daily", "5", "end");

                }

            }
            catch (Exception e)
            {

                throw e;
            }

            return;
        }


        /// <summary>
        /// 單筆結算
        /// </summary>
        /// <param name="detail"></param>
        /// <param name="customerValuationList"></param>
        /// <param name="valuationScopeList"></param>
        /// <param name="valuationAList"></param>
        /// <param name="settleAScopeList"></param>
        /// <param name="valuationCList"></param>
        /// <param name="settleCScopeList"></param>
        /// <param name="custmorBill"></param>
        /// <param name="settleABill"></param>
        /// <param name="settleBBill"></param>
        /// <param name="settleCBill"></param>
        /// <param name="addtionList"></param>
        /// <param name="addtionScopeList"></param>
        /// <param name="specialList"></param>
        /// <returns></returns>
        async Task SettleRowDataAsync(MoneySettle_Detail_Condition detail, List<ProductValuationManage_Condition> customerValuationList
            , List<ProductValuationManageScope_Condition> valuationScopeList
            , List<MoneySettleA_Condition> valuationAList, List<MoneySettleAScope_Condition> settleAScopeList
            , List<MoneySettleC_Condition> valuationCList, List<MoneySettleCScope_Condition> settleCScopeList
            , CustomerSettle_Fee_Condition custmorBill, SettleA_Fee_Condition settleABill
            , SettleB_Fee_Condition settleBBill, SettleC_Fee_Condition settleCBill
            , List<AdditionalFeeManage_Condition> addtionList, List<AdditionalFeeManageScope_Condition> addtionScopeList
            , List<SpecialArea_Condition> specialList, List<tbCustomers_Condition> customrtList, List<tbStation_Condition> stationList
            , CBM_Detail_Condition detailCBM)
        {
            //var detailCBM = new CBM_Detail_Condition();
            var customerValuation = new ProductValuationManage_Condition();
            var valuation = new ProductValuationManageScope_Condition();
            var stationASettle = new MoneySettleA_Condition();
            var stationA = new MoneySettleAScope_Condition();
            var stationCSettle = new MoneySettleC_Condition();
            var stationC = new MoneySettleCScope_Condition();
            var customerAdditionList = new List<AdditionalFeeManage_Condition>();
            var publicAdditionList = new List<AdditionalFeeManage_Condition>();
            var cbmProduct = (await _ProductManage_DA.GetCBMProduct()).Select(x => x.ProductId).ToList();
            var overCBMInnerFee = 0;

            switch (detailCBM.CBM)
            {
                case "C001":
                case "C002":
                    overCBMInnerFee = 15;
                    break;
                case "C003":
                case "C004":
                    overCBMInnerFee = 20;
                    break;
                case "C005":
                    overCBMInnerFee = 25;
                    break;
                default:
                    break;
            }
            try
            {
                #region 客戶運價

                //detailCBM = (await _CBM_Detail_DA.GetInfoById(detail.id)).FirstOrDefault();
                customerValuation = customerValuationList.FirstOrDefault(x => x.Spec == detailCBM.CBM);
                if (customerValuation == null)
                {
                    _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no customerValuation");
                    return;
                }
                valuation = valuationScopeList.FirstOrDefault(x => x.ProductValuationManageId == customerValuation.id && x.StartNum <= custmorBill.SN && x.EndNum >= custmorBill.SN);
                if (customerValuation == null)
                {
                    _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no productValuation");
                    return;
                }
                stationASettle = valuationAList.FirstOrDefault(x => x.Spec == detailCBM.CBM);
                if (stationASettle == null)
                {
                    _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no stationASettle");
                    return;
                }
                stationA = settleAScopeList.FirstOrDefault(x => x.MoneySettleAId == stationASettle.id && x.StartNum <= custmorBill.SN && x.EndNum >= custmorBill.SN);
                if (stationA == null)
                {
                    _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no stationA");
                    return;
                }
                stationCSettle = valuationCList.FirstOrDefault(x => x.Spec == detailCBM.CBM);
                if (stationCSettle == null)
                {
                    _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no stationCSettle");
                    return;
                }
                stationC = settleCScopeList.FirstOrDefault(x => x.MoneySettleCId == stationCSettle.id && x.StartNum <= custmorBill.SN && x.EndNum >= custmorBill.SN);
                if (stationC == null)
                {
                    _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no stationC");
                    return;
                }

                var isNewCustomer = customrtList.FirstOrDefault(x => x.customer_code == detail.CustomerCode).is_new_customer == true ? true : false;
                var isOldOverCBMCustomer = customrtList.FirstOrDefault(x => x.customer_code == detail.CustomerCode).OldCustomerOverCBM == true ? true : false;
                var isNewSendStation = stationList.FirstOrDefault(x => x.station_scode == detail.ReceiveDriverStation).is_new_station == true ? true : false;
                //var isNewArriveStation = stationList.FirstOrDefault(x => x.station_scode == detail.ArriveStation).is_new_station == true ? true : false;
                //司機無站所問題
                var isNewArriveStation = true;
                if (!String.IsNullOrEmpty(detail.ArriveDriverStation))
                {
                    isNewArriveStation = stationList.FirstOrDefault(x => x.station_scode == detail.ArriveDriverStation).is_new_station == true ? true : false;
                }
                else if (!String.IsNullOrEmpty(detail.ArriveStation))
                {
                    isNewArriveStation = stationList.FirstOrDefault(x => x.station_scode == detail.ArriveStation).is_new_station == true ? true : false;
                }
                var isEast = detail.isEast == null || !cbmProduct.Contains(detail.ProductId) ? false : (bool)detail.isEast;


                var specialCustomerList = new List<string> { "F1300600002", "F3500010002" };

                custmorBill.DD_Fee = valuation.Valuation;
                settleABill.DD_Fee = custmorBill.DD_Fee - stationA.Valuation;
                //C段結算方式 1:單價 2:百分比
                //若是有新舊客代差異 以產品做區分
                if (stationCSettle.SettleType == "1")
                {
                    settleCBill.DD_Fee = stationC.Valuation;
                }
                else if (stationCSettle.SettleType == "2")
                {
                    settleCBill.DD_Fee = Convert.ToInt32(stationA.Valuation * Convert.ToDouble(stationC.Valuation / 100.0));
                }
                settleBBill.DD_Fee = custmorBill.DD_Fee - settleABill.DD_Fee - settleCBill.DD_Fee;
                #endregion
                //客代找增值服務
                if (custmorBill.SN == 1)
                {
                    customerAdditionList = addtionList.Where(
                    x => x.CustomerCode == detail.CustomerCode
                    && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                    ).ToList();
                    publicAdditionList = addtionList.Where(
                        x => x.CustomerCode == "-1"
                        && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                        ).ToList();
                    if (customerValuationList.Count() == 0)
                    {
                        customerAdditionList = publicAdditionList;
                    }
                    #region 代收貨款
                    //沒有代收就不用結算
                    if (detail.CollectionMoney == 0)
                    {
                        custmorBill.Collection_Money = 0;
                    }
                    else
                    {
                        var collectData = customerAdditionList.FirstOrDefault(x => x.Type == "1");
                        if (collectData == null)
                        {
                            collectData = publicAdditionList.FirstOrDefault(x => x.Type == "1");
                        }

                        var collectValue = addtionScopeList.FirstOrDefault(
                        x => x.AdditionalFeeManageId == collectData.id
                        && x.StartPrice <= detail.CollectionMoney && x.EndPrice >= detail.CollectionMoney
                        );
                        if (detail.CollectionMoney > 20000)
                        {
                            custmorBill.Collection_Money = -1;
                        }
                        else
                        {
                            custmorBill.Collection_Money = collectValue.Valuation;
                        }
                        #region ABC代收
                        //新舊客代差異 MOMO和蝦皮不計價
                        if (isNewCustomer)
                        {
                            switch (detail.CollectionMoney)
                            {
                                case <= 5000:
                                    settleBBill.Collection_Money = 10;
                                    settleCBill.Collection_Money = 5;
                                    break;
                                case > 5000 and <= 10000:
                                    settleBBill.Collection_Money = 15;
                                    settleCBill.Collection_Money = 5;
                                    break;
                                case > 10000 and <= 15000:
                                    settleBBill.Collection_Money = 20;
                                    settleCBill.Collection_Money = 5;
                                    break;
                                case > 15000 and <= 20000:
                                    settleBBill.Collection_Money = 25;
                                    settleCBill.Collection_Money = 5;
                                    break;

                                default:
                                    break;
                            }
                        }
                        else
                        {
                            switch (detail.CollectionMoney)
                            {
                                case <= 10000:
                                    settleBBill.Collection_Money = 10;
                                    settleCBill.Collection_Money = 0;
                                    break;
                                case > 10000 and <= 20000:
                                    settleBBill.Collection_Money = 40;
                                    settleCBill.Collection_Money = 0;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (specialCustomerList.Contains(detail.CustomerCode))
                        {
                            settleABill.Collection_Money = 0;
                        }
                        else
                        {
                            settleABill.Collection_Money = custmorBill.Collection_Money - settleBBill.Collection_Money - settleCBill.Collection_Money;
                        }
                        #endregion
                    }
                    #endregion
                    #region 回單
                    //沒回單不結算
                    if (detail.ReceiptFlag == false)
                    {
                        custmorBill.ReceiptFlag_Fee = 0;
                    }
                    else
                    {
                        var receiveData = customerAdditionList.FirstOrDefault(x => x.Type == "2");
                        if (receiveData == null)
                        {
                            receiveData = publicAdditionList.FirstOrDefault(x => x.Type == "2");
                        }
                        var receiveValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == receiveData.id);
                        custmorBill.ReceiptFlag_Fee = receiveValue.Valuation;
                        #region ABC回單
                        //新舊客代差異
                        if (isNewCustomer)
                        {
                            settleBBill.ReceiptFlag_Fee = 15;
                            settleCBill.ReceiptFlag_Fee = 15;
                            settleABill.ReceiptFlag_Fee = custmorBill.ReceiptFlag_Fee - settleBBill.ReceiptFlag_Fee - settleCBill.ReceiptFlag_Fee;

                        }
                        else
                        {
                            settleBBill.ReceiptFlag_Fee = custmorBill.ReceiptFlag_Fee;
                            settleABill.ReceiptFlag_Fee = 0;
                            settleCBill.ReceiptFlag_Fee = 0;
                        }
                        #endregion
                    }
                    #endregion
                    #region 保值費
                    if (detail.Valued <= 2000)
                    {
                        custmorBill.Valued_Fee = 0;
                    }
                    else
                    {
                        var valuedData = customerAdditionList.FirstOrDefault(x => x.Type == "3");
                        if (valuedData == null)
                        {
                            valuedData = publicAdditionList.FirstOrDefault(x => x.Type == "3");
                        }
                        var valuedValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == valuedData.id);
                        custmorBill.Valued_Fee = Convert.ToInt32(detail.Valued * Convert.ToDouble(valuedValue.Valuation / 100.0));
                        #region ABC保值
                        settleBBill.Valued_Fee = custmorBill.Valued_Fee;
                        #endregion
                    }
                    #endregion
                    #region 超才
                    //沒超才 或 特殊客代
                    if (detailCBM.OverCBM == 0 || specialCustomerList.Contains(detail.CustomerCode))
                    //if (detail.OverCBM == 0)
                    {
                        custmorBill.OverCbm_Fee = 0;
                        settleBBill.OverCbm_Fee = 0;
                        settleCBill.OverCbm_Fee = 0;
                        settleABill.OverCbm_Fee = 0;
                    }
                    /*
                    //論才超才(宜花東)
                    else if (cbmProduct.Contains(detail.ProductId) && isEast)
                    {


                        var overCBMData = customerAdditionList.FirstOrDefault(x => x.Type == "4" && x.ProductId == detail.ProductId && x.Spec == detailCBM.CBM);
                        if (overCBMData == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMData");
                            return;
                        }
                        var overCBMValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == overCBMData.id);
                        if (overCBMValue == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMValue");
                            return;
                        }
                        custmorBill.OverCbm_Fee = detailCBM.OverCBM * overCBMValue.Valuation;
                        overCBMInnerFee = detailCBM.OverCBM * overCBMInnerFee;
                        settleBBill.OverCbm_Fee = overCBMInnerFee / 2;
                        settleCBill.OverCbm_Fee = overCBMInnerFee - settleBBill.OverCbm_Fee;
                        settleABill.OverCbm_Fee = custmorBill.OverCbm_Fee - overCBMInnerFee;

                    }
                    */
                    //論才超才
                    else if (cbmProduct.Contains(detail.ProductId))
                    {
                        var overCBMData = customerAdditionList.FirstOrDefault(x => x.Type == "4" && x.ProductId == detail.ProductId && x.Spec == detailCBM.CBM);
                        if (overCBMData == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMData");
                            return;
                        }
                        var overCBMValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == overCBMData.id);
                        if (overCBMValue == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMValue");
                            return;
                        }
                        custmorBill.OverCbm_Fee = detailCBM.OverCBM * overCBMValue.Valuation;
                        overCBMInnerFee = detailCBM.OverCBM * overCBMInnerFee;
                        settleBBill.OverCbm_Fee = overCBMInnerFee / 2;
                        settleCBill.OverCbm_Fee = overCBMInnerFee - settleBBill.OverCbm_Fee;
                        settleABill.OverCbm_Fee = custmorBill.OverCbm_Fee - overCBMInnerFee;
                    }
                    else
                    {
                        if (isOldOverCBMCustomer)
                        {
                            custmorBill.OverCbm_Fee = 30;
                        }
                        else
                        {
                            custmorBill.OverCbm_Fee = detailCBM.OverCBM * 30;
                        }
                        //var overCBMData = customerAdditionList.FirstOrDefault(x => x.Type == "4");
                        //if (overCBMData == null)
                        //{
                        //    overCBMData = publicAdditionList.FirstOrDefault(x => x.Type == "4");
                        //}
                        //var overCBMValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == overCBMData.id);
                        //custmorBill.OverCbm_Fee = detail.OverCBM * overCBMValue.Valuation;

                        #region ABC超才
                        settleBBill.OverCbm_Fee = detailCBM.OverCBM * 10;
                        settleCBill.OverCbm_Fee = detailCBM.OverCBM * 10;
                        settleABill.OverCbm_Fee = custmorBill.OverCbm_Fee - settleBBill.OverCbm_Fee - settleCBill.OverCbm_Fee;
                        #endregion
                    }
                    #endregion
                    #region 特殊
                    if (detail.SpecialAreaFee == 0 && (detail.ArriveStation != "99" && detail.ArriveStation != "95"))
                    {
                        custmorBill.SpecialArea_Fee = 0;
                    }
                    else
                    {
                        if (isNewCustomer)
                        {
                            //var specialValue = specialList.FirstOrDefault(x => x.id == detail.SpecialArea);
                            //if (specialValue != null)
                            //{
                            //    custmorBill.SpecialArea_Fee = specialValue.Fee;
                            //}
                            custmorBill.SpecialArea_Fee = detail.SpecialAreaFee;
                        }
                        else
                        {
                            if (customerValuationList.Count() == 0)
                            {
                                customerAdditionList = publicAdditionList;
                            }
                            var specialAreaData = customerAdditionList.FirstOrDefault(x => x.Type == "5");
                            if (specialAreaData == null)
                            {
                                specialAreaData = publicAdditionList.FirstOrDefault(x => x.Type == "5");
                            }
                            var specialAreaValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == specialAreaData.id);
                            custmorBill.SpecialArea_Fee = specialAreaValue.Valuation;
                        }
                        #region ABC特殊
                        settleBBill.SpecialArea_Fee = Convert.ToInt32(Convert.ToDouble(custmorBill.SpecialArea_Fee) * 0.5);
                        settleCBill.SpecialArea_Fee = Convert.ToInt32(Convert.ToDouble(custmorBill.SpecialArea_Fee) * 0.5);
                        #endregion
                    }
                    #endregion
                    #region 宜花東 (論才才要，加入論S)
                    if (isEast)
                    {
                        // var eastData = customerAdditionList.FirstOrDefault(x => x.Type == "6" && x.ProductId == detail.ProductId && x.Spec == detailCBM.CBM);
                        var eastData = customerAdditionList.FirstOrDefault(x => x.Type == "6" && x.ProductId == detail.ProductId);
                        if (eastData == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no eastData");
                            return;
                        }
                        var eastValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == eastData.id);
                        if (eastValue == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no eastValue");
                            return;
                        }
                        custmorBill.East_Fee = eastValue.Valuation;
                        settleBBill.East_Fee = eastValue.Valuation;
                    }
                    #endregion
                }
                else //續件
                {
                    customerAdditionList = addtionList.Where(
                                   x => x.CustomerCode == detail.CustomerCode
                                   && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                   ).ToList();
                    publicAdditionList = addtionList.Where(
                        x => x.CustomerCode == "-1"
                        && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                        ).ToList();
                    #region 超才
                    if (detailCBM.OverCBM == 0)
                    {
                        custmorBill.OverCbm_Fee = 0;
                    }
                    /*
                    //論才超才(宜花東)
                    else if (cbmProduct.Contains(detail.ProductId) && isEast)
                    {
                        var overCBMData = customerAdditionList.FirstOrDefault(x => x.Type == "7" && x.ProductId == detail.ProductId && x.Spec == detailCBM.CBM);
                        if (overCBMData == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMData");
                            return;
                        }
                        var overCBMValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == overCBMData.id);
                        if (overCBMValue == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMValue");
                            return;
                        }
                        custmorBill.OverCbm_Fee = detailCBM.OverCBM * overCBMValue.Valuation;
                        overCBMInnerFee = detailCBM.OverCBM * overCBMInnerFee;
                        settleBBill.OverCbm_Fee = overCBMInnerFee / 2;
                        settleCBill.OverCbm_Fee = overCBMInnerFee - settleBBill.OverCbm_Fee;
                        settleABill.OverCbm_Fee = custmorBill.OverCbm_Fee - overCBMInnerFee;
                    }
                    */
                    //論才超才
                    else if (cbmProduct.Contains(detail.ProductId))
                    {
                        var overCBMData = customerAdditionList.FirstOrDefault(x => x.Type == "4" && x.ProductId == detail.ProductId && x.Spec == detailCBM.CBM);
                        if (overCBMData == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMData");
                            return;
                        }
                        var overCBMValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == overCBMData.id);
                        if (overCBMValue == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMValue");
                            return;
                        }
                        custmorBill.OverCbm_Fee = detailCBM.OverCBM * overCBMValue.Valuation;
                        settleBBill.OverCbm_Fee = custmorBill.OverCbm_Fee / 2;
                        settleCBill.OverCbm_Fee = custmorBill.OverCbm_Fee - settleBBill.OverCbm_Fee;
                        settleABill.OverCbm_Fee = 0;

                    }
                    else
                    {
                        if (isOldOverCBMCustomer)
                        {
                            custmorBill.OverCbm_Fee = 30;
                        }
                        else
                        {
                            custmorBill.OverCbm_Fee = detailCBM.OverCBM * 30;
                        }
                        #region ABC超才
                        settleBBill.OverCbm_Fee = detailCBM.OverCBM * 10;
                        settleCBill.OverCbm_Fee = detailCBM.OverCBM * 10;
                        settleABill.OverCbm_Fee = custmorBill.OverCbm_Fee - settleBBill.OverCbm_Fee - settleCBill.OverCbm_Fee;
                        #endregion
                    }
                    #endregion
                    #region 特服
                    //續件 舊客代 需要加入特服費
                    if (detail.SpecialAreaFee > 0 || (detail.ArriveStation == "99" || detail.ArriveStation == "95"))
                    {
                        if (!isNewCustomer)
                        {
                            var specialAreaData = customerAdditionList.FirstOrDefault(x => x.Type == "5");
                            if (specialAreaData == null)
                            {
                                specialAreaData = publicAdditionList.FirstOrDefault(x => x.Type == "5");
                            }
                            var specialAreaValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == specialAreaData.id);
                            custmorBill.SpecialArea_Fee = specialAreaValue.Valuation;
                        }
                        #region ABC特殊
                        settleBBill.SpecialArea_Fee = Convert.ToInt32(Convert.ToDouble(custmorBill.SpecialArea_Fee) * 0.5);
                        settleCBill.SpecialArea_Fee = Convert.ToInt32(Convert.ToDouble(custmorBill.SpecialArea_Fee) * 0.5);
                        #endregion
                    }

                    #endregion
                    #region 宜花東 (論才才要，加入論S)
                    if (isEast)
                    {
                        //var eastData = customerAdditionList.FirstOrDefault(x => x.Type == "6" && x.ProductId == detail.ProductId && x.Spec == detailCBM.CBM);
                        var eastData = customerAdditionList.FirstOrDefault(x => x.Type == "6" && x.ProductId == detail.ProductId);
                        if (eastData == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no eastData");
                            return;
                        }
                        var eastValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == eastData.id);
                        if (eastValue == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no eastValue");
                            return;
                        }
                        custmorBill.East_Fee = eastValue.Valuation;
                        settleBBill.East_Fee = eastValue.Valuation;
                    }
                    #endregion
                }
                custmorBill.Total = Convert.ToInt32(custmorBill.DD_Fee) + Convert.ToInt32(custmorBill.Collection_Money) + Convert.ToInt32(custmorBill.OverCbm_Fee) + Convert.ToInt32(custmorBill.ReceiptFlag_Fee) + Convert.ToInt32(custmorBill.Valued_Fee) + Convert.ToInt32(custmorBill.SpecialArea_Fee) + Convert.ToInt32(custmorBill.East_Fee) + Convert.ToInt32(custmorBill.EastOverCbm_Fee);
                settleABill.Total = Convert.ToInt32(settleABill.DD_Fee) + Convert.ToInt32(settleABill.Collection_Money) + Convert.ToInt32(settleABill.OverCbm_Fee) + Convert.ToInt32(settleABill.ReceiptFlag_Fee) + Convert.ToInt32(settleABill.Valued_Fee);//+ Convert.ToInt32(settleABill.SpecialArea_Fee)
                settleBBill.Total = Convert.ToInt32(settleBBill.DD_Fee) + Convert.ToInt32(settleBBill.Collection_Money) + Convert.ToInt32(settleBBill.OverCbm_Fee) + Convert.ToInt32(settleBBill.ReceiptFlag_Fee) + Convert.ToInt32(settleBBill.Valued_Fee) + Convert.ToInt32(settleBBill.SpecialArea_Fee) + Convert.ToInt32(settleBBill.East_Fee) + Convert.ToInt32(settleBBill.EastOverCbm_Fee);
                settleCBill.Total = Convert.ToInt32(settleCBill.DD_Fee) + Convert.ToInt32(settleCBill.Collection_Money) + Convert.ToInt32(settleCBill.OverCbm_Fee) + Convert.ToInt32(settleCBill.ReceiptFlag_Fee) + Convert.ToInt32(settleCBill.SpecialArea_Fee);//+ Convert.ToInt32(settleCBill.Valued_Fee)
            }
            catch (Exception e)
            {
                _LogRecord.Save("SettleRowDataAsync", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + "," + e.Message);
                //throw e;
            }
            return;
        }


        /// <summary>
        /// 單筆結算-日結
        /// </summary>
        /// <param name="detail"></param>
        /// <param name="customerValuationList"></param>
        /// <param name="valuationScopeList"></param>
        /// <param name="valuationAList"></param>
        /// <param name="settleAScopeList"></param>
        /// <param name="valuationCList"></param>
        /// <param name="settleCScopeList"></param>
        /// <param name="custmorBill"></param>
        /// <param name="settleABill"></param>
        /// <param name="settleBBill"></param>
        /// <param name="settleCBill"></param>
        /// <param name="addtionList"></param>
        /// <param name="addtionScopeList"></param>
        /// <param name="specialList"></param>
        /// <returns></returns>
        async Task DailySettleRowDataAsync(DailyMoneySettleDetail_Temp_Condition detail, List<ProductValuationManage_Condition> customerValuationList
            , List<ProductValuationManageScope_Condition> valuationScopeList
            , List<MoneySettleA_Condition> valuationAList, List<MoneySettleAScope_Condition> settleAScopeList
            , List<MoneySettleC_Condition> valuationCList, List<MoneySettleCScope_Condition> settleCScopeList
            , DailyCustomerSettle_Fee_Temp_Condition custmorBill, DailySettleA_Fee_Temp_Condition settleABill
            , DailySettleB_Fee_Temp_Condition settleBBill, DailySettleC_Fee_Temp_Condition settleCBill
            , List<AdditionalFeeManage_Condition> addtionList, List<AdditionalFeeManageScope_Condition> addtionScopeList
            , List<SpecialArea_Condition> specialList, List<tbCustomers_Condition> customrtList, List<tbStation_Condition> stationList
            , DailyCBMDetail_Temp_Condition detailCBM, List<DailyCalErrMsgLog_Condition> dailyCalErrMsgLogList)
        {
            //var detailCBM = new CBM_Detail_Condition();
            var customerValuation = new ProductValuationManage_Condition();
            var valuation = new ProductValuationManageScope_Condition();
            var stationASettle = new MoneySettleA_Condition();
            var stationA = new MoneySettleAScope_Condition();
            var stationCSettle = new MoneySettleC_Condition();
            var stationC = new MoneySettleCScope_Condition();
            var customerAdditionList = new List<AdditionalFeeManage_Condition>();
            var publicAdditionList = new List<AdditionalFeeManage_Condition>();
            var cbmProduct = (await _ProductManage_DA.GetCBMProduct()).Select(x => x.ProductId).ToList();
            var overCBMInnerFee = 0;

            switch (detailCBM.CBM)
            {
                case "C001":
                case "C002":
                    overCBMInnerFee = 15;
                    break;
                case "C003":
                case "C004":
                    overCBMInnerFee = 20;
                    break;
                case "C005":
                    overCBMInnerFee = 25;
                    break;
                default:
                    break;
            }
            try
            {
                #region 客戶運價

                //detailCBM = (await _CBM_Detail_DA.GetInfoById(detail.id)).FirstOrDefault();
                if (detailCBM.CBM=="B004")
                {
                    detailCBM.CBM = "B003";
                }
                customerValuation = customerValuationList.FirstOrDefault(x => x.Spec == detailCBM.CBM);
                if (customerValuation==null)
                {
                    customerValuation = customerValuationList[0];
                }


                if (customerValuation == null)
                {
                    _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no customerValuation");
                    dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no customerValuation" });
                    return;
                }
                valuation = valuationScopeList.FirstOrDefault(x => x.ProductValuationManageId == customerValuation.id && x.StartNum <= custmorBill.SN && x.EndNum >= custmorBill.SN);
                if (customerValuation == null)
                {
                    _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no productValuation");
                    dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no productValuation" });
                    return;
                }
                stationASettle = valuationAList.FirstOrDefault(x => x.Spec == detailCBM.CBM);
                if (stationASettle == null)
                {
                    stationASettle = valuationAList[0];
                }

                if (stationASettle == null)
                {
                    _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no stationASettle");
                    dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no stationASettle" });
                    return;
                }
                stationA = settleAScopeList.FirstOrDefault(x => x.MoneySettleAId == stationASettle.id && x.StartNum <= custmorBill.SN && x.EndNum >= custmorBill.SN);
                if (stationA == null)
                {
                    _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no stationA");
                    dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no stationA" });
                    return;
                }
                stationCSettle = valuationCList.FirstOrDefault(x => x.Spec == detailCBM.CBM);
                if (stationCSettle==null)
                {
                    stationCSettle = valuationCList[0];
                }
                if (stationCSettle == null)
                {
                    _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no stationCSettle");
                    dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no stationCSettle" });
                    return;
                }
                stationC = settleCScopeList.FirstOrDefault(x => x.MoneySettleCId == stationCSettle.id && x.StartNum <= custmorBill.SN && x.EndNum >= custmorBill.SN);
                if (stationC == null)
                {
                    _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no stationC");
                    dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no stationC" });
                    return;
                }

                var isNewCustomer = customrtList.FirstOrDefault(x => x.customer_code == detail.CustomerCode).is_new_customer == true ? true : false;
                var isOldOverCBMCustomer = customrtList.FirstOrDefault(x => x.customer_code == detail.CustomerCode).OldCustomerOverCBM == true ? true : false;
                var isNewSendStation = stationList.FirstOrDefault(x => x.station_scode == detail.ReceiveDriverStation).is_new_station == true ? true : false;
                //var isNewArriveStation = stationList.FirstOrDefault(x => x.station_scode == detail.ArriveStation).is_new_station == true ? true : false;
                //司機無站所問題
                var isNewArriveStation = true;
                if (!String.IsNullOrEmpty(detail.ArriveDriverStation))
                {
                    isNewArriveStation = stationList.FirstOrDefault(x => x.station_scode == detail.ArriveDriverStation).is_new_station == true ? true : false;
                }
                else if (!String.IsNullOrEmpty(detail.ArriveStation))
                {
                    isNewArriveStation = stationList.FirstOrDefault(x => x.station_scode == detail.ArriveStation).is_new_station == true ? true : false;
                }
                var isEast = detail.isEast == null || !cbmProduct.Contains(detail.ProductId) ? false : (bool)detail.isEast;


                var specialCustomerList = new List<string> { "F1300600002", "F3500010002" };



                custmorBill.DD_Fee = valuation.Valuation;
                settleABill.DD_Fee = custmorBill.DD_Fee - stationA.Valuation;
                //C段結算方式 1:單價 2:百分比
                //若是有新舊客代差異 以產品做區分
                if (stationCSettle.SettleType == "1")
                {
                    settleCBill.DD_Fee = stationC.Valuation;
                }
                else if (stationCSettle.SettleType == "2")
                {
                    settleCBill.DD_Fee = Convert.ToInt32(stationA.Valuation * Convert.ToDouble(stationC.Valuation / 100.0));
                }
                settleBBill.DD_Fee = custmorBill.DD_Fee - settleABill.DD_Fee - settleCBill.DD_Fee;
                #endregion
                //客代找增值服務
                if (custmorBill.SN == 1)
                {
                    customerAdditionList = addtionList.Where(
                    x => x.CustomerCode == detail.CustomerCode
                    && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                    ).ToList();
                    publicAdditionList = addtionList.Where(
                        x => x.CustomerCode == "-1"
                        && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                        ).ToList();
                    if (customerValuationList.Count() == 0)
                    {
                        customerAdditionList = publicAdditionList;
                    }
                    #region 代收貨款
                    //沒有代收就不用結算
                    if (detail.CollectionMoney == 0)
                    {
                        custmorBill.Collection_Money = 0;
                    }
                    else
                    {
                        var collectData = customerAdditionList.FirstOrDefault(x => x.Type == "1");
                        if (collectData == null)
                        {
                            collectData = publicAdditionList.FirstOrDefault(x => x.Type == "1");
                        }

                        var collectValue = addtionScopeList.FirstOrDefault(
                        x => x.AdditionalFeeManageId == collectData.id
                        && x.StartPrice <= detail.CollectionMoney && x.EndPrice >= detail.CollectionMoney
                        );
                        if (detail.CollectionMoney > 20000)
                        {
                            custmorBill.Collection_Money = -1;
                        }
                        else
                        {
                            custmorBill.Collection_Money = collectValue.Valuation;
                        }
                        #region ABC代收
                        //新舊客代差異 MOMO和蝦皮不計價
                        if (isNewCustomer)
                        {
                            switch (detail.CollectionMoney)
                            {
                                case <= 5000:
                                    settleBBill.Collection_Money = 10;
                                    settleCBill.Collection_Money = 5;
                                    break;
                                case > 5000 and <= 10000:
                                    settleBBill.Collection_Money = 15;
                                    settleCBill.Collection_Money = 5;
                                    break;
                                case > 10000 and <= 15000:
                                    settleBBill.Collection_Money = 20;
                                    settleCBill.Collection_Money = 5;
                                    break;
                                case > 15000 and <= 20000:
                                    settleBBill.Collection_Money = 25;
                                    settleCBill.Collection_Money = 5;
                                    break;

                                default:
                                    break;
                            }
                        }
                        else
                        {
                            switch (detail.CollectionMoney)
                            {
                                case <= 10000:
                                    settleBBill.Collection_Money = 10;
                                    settleCBill.Collection_Money = 0;
                                    break;
                                case > 10000 and <= 20000:
                                    settleBBill.Collection_Money = 40;
                                    settleCBill.Collection_Money = 0;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (specialCustomerList.Contains(detail.CustomerCode))
                        {
                            settleABill.Collection_Money = 0;
                        }
                        else
                        {
                            settleABill.Collection_Money = custmorBill.Collection_Money - settleBBill.Collection_Money - settleCBill.Collection_Money;
                        }
                        #endregion
                    }
                    #endregion
                    #region 回單
                    //沒回單不結算
                    if (detail.ReceiptFlag == false)
                    {
                        custmorBill.ReceiptFlag_Fee = 0;
                    }
                    else
                    {
                        var receiveData = customerAdditionList.FirstOrDefault(x => x.Type == "2");
                        if (receiveData == null)
                        {
                            receiveData = publicAdditionList.FirstOrDefault(x => x.Type == "2");
                        }
                        var receiveValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == receiveData.id);
                        custmorBill.ReceiptFlag_Fee = receiveValue.Valuation;
                        #region ABC回單
                        //新舊客代差異
                        if (isNewCustomer)
                        {
                            settleBBill.ReceiptFlag_Fee = 15;
                            settleCBill.ReceiptFlag_Fee = 15;
                            settleABill.ReceiptFlag_Fee = custmorBill.ReceiptFlag_Fee - settleBBill.ReceiptFlag_Fee - settleCBill.ReceiptFlag_Fee;

                        }
                        else
                        {
                            settleBBill.ReceiptFlag_Fee = custmorBill.ReceiptFlag_Fee;
                            settleABill.ReceiptFlag_Fee = 0;
                            settleCBill.ReceiptFlag_Fee = 0;
                        }
                        #endregion
                    }
                    #endregion
                    #region 保值費
                    //保值費在資料近來前就以計算
                    settleBBill.Valued_Fee = custmorBill.Valued_Fee = detail.Valued;

                    //if (detail.Valued <= 2000)
                    //{
                    //    custmorBill.Valued_Fee = 0;
                    //}
                    //else
                    //{
                    //    var valuedData = customerAdditionList.FirstOrDefault(x => x.Type == "3");
                    //    if (valuedData == null)
                    //    {
                    //        valuedData = publicAdditionList.FirstOrDefault(x => x.Type == "3");
                    //    }
                    //    var valuedValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == valuedData.id);
                    //    custmorBill.Valued_Fee = Convert.ToInt32(detail.Valued * Convert.ToDouble(valuedValue.Valuation / 100.0));
                    //    #region ABC保值
                    //    settleBBill.Valued_Fee = custmorBill.Valued_Fee;
                    //    #endregion
                    //}

                    #endregion
                    #region 超才
                    //沒超才 或 特殊客代
                    if (detailCBM.OverCBM == 0 || specialCustomerList.Contains(detail.CustomerCode))
                    //if (detail.OverCBM == 0)
                    {
                        custmorBill.OverCbm_Fee = 0;
                        settleBBill.OverCbm_Fee = 0;
                        settleCBill.OverCbm_Fee = 0;
                        settleABill.OverCbm_Fee = 0;
                    }
                    //論才超才(宜花東)
                    /*
                    else if (cbmProduct.Contains(detail.ProductId) && isEast)
                    {


                        var overCBMData = customerAdditionList.FirstOrDefault(x => x.Type == "7" && x.ProductId == detail.ProductId && x.Spec == detailCBM.CBM);
                        if (overCBMData == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMData");
                            dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no overCBMData" });
                            return;
                        }
                        var overCBMValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == overCBMData.id);
                        if (overCBMValue == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMValue");
                            dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no overCBMValue" });
                            return;
                        }
                        custmorBill.OverCbm_Fee = detailCBM.OverCBM * overCBMValue.Valuation;
                        overCBMInnerFee = detailCBM.OverCBM * overCBMInnerFee;
                        settleBBill.OverCbm_Fee = overCBMInnerFee / 2;
                        settleCBill.OverCbm_Fee = overCBMInnerFee - settleBBill.OverCbm_Fee;
                        settleABill.OverCbm_Fee = custmorBill.OverCbm_Fee - overCBMInnerFee;

                    }
                    */
                    //論才超才
                    else if (cbmProduct.Contains(detail.ProductId))
                    {
                        //var overCBMData = customerAdditionList.FirstOrDefault(x => x.Type == "4" && x.ProductId == detail.ProductId && x.Spec == detailCBM.CBM);
                        var overCBMData = customerAdditionList.FirstOrDefault(x => x.Type == "4" && x.ProductId == detail.ProductId);
                        if (overCBMData == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMData");
                            dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no overCBMData" });
                            return;
                        }
                        var overCBMValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == overCBMData.id);
                        if (overCBMValue == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMValue");
                            dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no overCBMValue" });
                            return;
                        }
                        custmorBill.OverCbm_Fee = detailCBM.OverCBM * overCBMValue.Valuation;
                        overCBMInnerFee = detailCBM.OverCBM * overCBMInnerFee;
                        settleBBill.OverCbm_Fee = overCBMInnerFee / 2;
                        settleCBill.OverCbm_Fee = overCBMInnerFee - settleBBill.OverCbm_Fee;
                        settleABill.OverCbm_Fee = custmorBill.OverCbm_Fee - overCBMInnerFee;
                    }
                    else
                    {
                        if (isOldOverCBMCustomer)
                        {
                            custmorBill.OverCbm_Fee = 30;
                        }
                        else
                        {
                            custmorBill.OverCbm_Fee = detailCBM.OverCBM * 30;
                        }
                        //var overCBMData = customerAdditionList.FirstOrDefault(x => x.Type == "4");
                        //if (overCBMData == null)
                        //{
                        //    overCBMData = publicAdditionList.FirstOrDefault(x => x.Type == "4");
                        //}
                        //var overCBMValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == overCBMData.id);
                        //custmorBill.OverCbm_Fee = detail.OverCBM * overCBMValue.Valuation;

                        #region ABC超才
                        settleBBill.OverCbm_Fee = detailCBM.OverCBM * 10;
                        settleCBill.OverCbm_Fee = detailCBM.OverCBM * 10;
                        settleABill.OverCbm_Fee = custmorBill.OverCbm_Fee - settleBBill.OverCbm_Fee - settleCBill.OverCbm_Fee;
                        #endregion
                    }
                    #endregion
                    #region 特殊
                    if (detail.SpecialAreaFee == 0 && (detail.ArriveStation != "99" && detail.ArriveStation != "95"))
                    {
                        custmorBill.SpecialArea_Fee = 0;
                    }
                    else
                    {
                        if (isNewCustomer)
                        {
                            //var specialValue = specialList.FirstOrDefault(x => x.id == detail.SpecialArea);
                            //if (specialValue != null)
                            //{
                            //    custmorBill.SpecialArea_Fee = specialValue.Fee;
                            //}
                            custmorBill.SpecialArea_Fee = detail.SpecialAreaFee;
                        }
                        else
                        {
                            if (customerValuationList.Count() == 0)
                            {
                                customerAdditionList = publicAdditionList;
                            }
                            var specialAreaData = customerAdditionList.FirstOrDefault(x => x.Type == "5");
                            if (specialAreaData == null)
                            {
                                specialAreaData = publicAdditionList.FirstOrDefault(x => x.Type == "5");
                            }
                            var specialAreaValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == specialAreaData.id);
                            custmorBill.SpecialArea_Fee = specialAreaValue.Valuation;
                        }
                        #region ABC特殊
                        settleBBill.SpecialArea_Fee = Convert.ToInt32(Convert.ToDouble(custmorBill.SpecialArea_Fee) * 0.5);
                        settleCBill.SpecialArea_Fee = Convert.ToInt32(Convert.ToDouble(custmorBill.SpecialArea_Fee) * 0.5);
                        #endregion
                    }
                    #endregion
                    #region 宜花東 (論才才要，加入論S)
                    if (isEast)
                    {
                        //var eastData = customerAdditionList.FirstOrDefault(x => x.Type == "6" && x.ProductId == detail.ProductId && x.Spec == detailCBM.CBM);
                        var eastData = customerAdditionList.FirstOrDefault(x => x.Type == "6" && x.ProductId == detail.ProductId);
                        if (eastData == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no eastData");
                            dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no eastData" });
                            return;
                        }
                        var eastValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == eastData.id);
                        if (eastValue == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no eastValue");
                            dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no eastValue" });
                            return;
                        }
                        custmorBill.East_Fee = eastValue.Valuation;
                        settleBBill.East_Fee = eastValue.Valuation;
                    }
                    #endregion
                }
                else //續件
                {
                    customerAdditionList = addtionList.Where(
                                   x => x.CustomerCode == detail.CustomerCode
                                   && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                                   ).ToList();
                    publicAdditionList = addtionList.Where(
                        x => x.CustomerCode == "-1"
                        && x.StartDate <= detail.Request_CreateDate && EndDateFix(x.EndDate) >= detail.Request_CreateDate
                        ).ToList();
                    #region 超才
                    if (detailCBM.OverCBM == 0)
                    {
                        custmorBill.OverCbm_Fee = 0;
                    }
                    /*
                    //論才超才(宜花東)
                    else if (cbmProduct.Contains(detail.ProductId) && isEast)
                    {
                        var overCBMData = customerAdditionList.FirstOrDefault(x => x.Type == "7" && x.ProductId == detail.ProductId && x.Spec == detailCBM.CBM);
                        if (overCBMData == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMData");
                            dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no overCBMData" });
                            return;
                        }
                        var overCBMValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == overCBMData.id);
                        if (overCBMValue == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMValue");
                            dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no overCBMValue" });
                            return;
                        }
                        custmorBill.OverCbm_Fee = detailCBM.OverCBM * overCBMValue.Valuation;
                        overCBMInnerFee = detailCBM.OverCBM * overCBMInnerFee;
                        settleBBill.OverCbm_Fee = overCBMInnerFee / 2;
                        settleCBill.OverCbm_Fee = overCBMInnerFee - settleBBill.OverCbm_Fee;
                        settleABill.OverCbm_Fee = custmorBill.OverCbm_Fee - overCBMInnerFee;
                    }
                    */
                    //論才超才
                    else if (cbmProduct.Contains(detail.ProductId))
                    {
                        var overCBMData = customerAdditionList.FirstOrDefault(x => x.Type == "4" && x.ProductId == detail.ProductId && x.Spec == detailCBM.CBM);
                        if (overCBMData == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMData");
                            dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no overCBMData" });
                            return;
                        }
                        var overCBMValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == overCBMData.id);
                        if (overCBMValue == null)
                        {
                            _LogRecord.Save("DailySettleRowDataAsyncError", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + ",no overCBMValue");
                            dailyCalErrMsgLogList.Add(new DailyCalErrMsgLog_Condition { DailyMoneySettleDetailId = detail.id, CheckNumber = detail.CheckNumber, ProductId = detail.ProductId.ToString(), CBM = detailCBM.CBM.ToString(), ErrMsg = "no overCBMValue" });
                            return;
                        }
                        custmorBill.OverCbm_Fee = detailCBM.OverCBM * overCBMValue.Valuation;
                        settleBBill.OverCbm_Fee = custmorBill.OverCbm_Fee / 2;
                        settleCBill.OverCbm_Fee = custmorBill.OverCbm_Fee - settleBBill.OverCbm_Fee;
                        settleABill.OverCbm_Fee = 0;

                    }
                    else
                    {
                        if (isOldOverCBMCustomer)
                        {
                            custmorBill.OverCbm_Fee = 30;
                        }
                        else
                        {
                            custmorBill.OverCbm_Fee = detailCBM.OverCBM * 30;
                        }
                        #region ABC超才
                        settleBBill.OverCbm_Fee = detailCBM.OverCBM * 10;
                        settleCBill.OverCbm_Fee = detailCBM.OverCBM * 10;
                        settleABill.OverCbm_Fee = custmorBill.OverCbm_Fee - settleBBill.OverCbm_Fee - settleCBill.OverCbm_Fee;
                        #endregion
                    }
                    #endregion
                    #region 特服
                    //續件 舊客代 需要加入特服費
                    if (detail.SpecialAreaFee > 0 || (detail.ArriveStation == "99" || detail.ArriveStation == "95"))
                    {
                        if (!isNewCustomer)
                        {
                            var specialAreaData = customerAdditionList.FirstOrDefault(x => x.Type == "5");
                            if (specialAreaData == null)
                            {
                                specialAreaData = publicAdditionList.FirstOrDefault(x => x.Type == "5");
                            }
                            var specialAreaValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == specialAreaData.id);
                            custmorBill.SpecialArea_Fee = specialAreaValue.Valuation;
                        }
                        #region ABC特殊
                        settleBBill.SpecialArea_Fee = Convert.ToInt32(Convert.ToDouble(custmorBill.SpecialArea_Fee) * 0.5);
                        settleCBill.SpecialArea_Fee = Convert.ToInt32(Convert.ToDouble(custmorBill.SpecialArea_Fee) * 0.5);
                        #endregion
                    }

                    #endregion
                    #region 宜花東 (論才才要，加入論S)
                    if (isEast)
                    {
                        //var eastData = customerAdditionList.FirstOrDefault(x => x.Type == "6" && x.ProductId == detail.ProductId && x.Spec == detailCBM.CBM);
                        var eastData = customerAdditionList.FirstOrDefault(x => x.Type == "6" && x.ProductId == detail.ProductId);
                        if (eastData != null)
                        {
                            var eastValue = addtionScopeList.FirstOrDefault(x => x.AdditionalFeeManageId == eastData.id);
                            if (eastValue != null)
                            {
                                custmorBill.East_Fee = eastValue.Valuation;
                                settleBBill.East_Fee = eastValue.Valuation;
                            }
                        }


                        

                    }
                    #endregion
                }
                custmorBill.Total = Convert.ToInt32(custmorBill.DD_Fee) + Convert.ToInt32(custmorBill.Collection_Money) + Convert.ToInt32(custmorBill.OverCbm_Fee) + Convert.ToInt32(custmorBill.ReceiptFlag_Fee) + Convert.ToInt32(custmorBill.Valued_Fee) + Convert.ToInt32(custmorBill.SpecialArea_Fee) + Convert.ToInt32(custmorBill.East_Fee) + Convert.ToInt32(custmorBill.EastOverCbm_Fee);
                settleABill.Total = Convert.ToInt32(settleABill.DD_Fee) + Convert.ToInt32(settleABill.Collection_Money) + Convert.ToInt32(settleABill.OverCbm_Fee) + Convert.ToInt32(settleABill.ReceiptFlag_Fee) + Convert.ToInt32(settleABill.Valued_Fee);//+ Convert.ToInt32(settleABill.SpecialArea_Fee)
                settleBBill.Total = Convert.ToInt32(settleBBill.DD_Fee) + Convert.ToInt32(settleBBill.Collection_Money) + Convert.ToInt32(settleBBill.OverCbm_Fee) + Convert.ToInt32(settleBBill.ReceiptFlag_Fee) + Convert.ToInt32(settleBBill.Valued_Fee) + Convert.ToInt32(settleBBill.SpecialArea_Fee) + Convert.ToInt32(settleBBill.East_Fee) + Convert.ToInt32(settleBBill.EastOverCbm_Fee);
                settleCBill.Total = Convert.ToInt32(settleCBill.DD_Fee) + Convert.ToInt32(settleCBill.Collection_Money) + Convert.ToInt32(settleCBill.OverCbm_Fee) + Convert.ToInt32(settleCBill.ReceiptFlag_Fee) + Convert.ToInt32(settleCBill.SpecialArea_Fee);//+ Convert.ToInt32(settleCBill.Valued_Fee)
            }
            catch (Exception e)
            {
                _LogRecord.Save("SettleRowDataAsync", "1", detail.id.ToString() + "," + detail.CheckNumber.ToString() + "," + detail.ProductId.ToString() + "," + detailCBM.CBM.ToString() + "," + e.Message);
                //throw e;
            }
            return;
        }


        DateTime EndDateFix(DateTime endDate)
        {
            var result = endDate.AddDays(1);
            return result;
        }
    }
}
