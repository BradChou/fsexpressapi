﻿using BLL.Model.AddressParsing.Res.Address;
using BLL.Model.FSExpressAPI.Model;
using BLL.Model.FSExpressAPI.Req.Consignment;
using BLL.Model.FSExpressAPI.Res.Consignment;
using BLL.Model.ScheduleAPI.Res.Consignment;
using Common;
using Common.Model;
using Common.SSRS;
using DAL;
using DAL.Contract_DA;
using DAL.DA;
using DAL.FSE01_DA;
using DAL.JunFuTrans_DA;
using DAL.Model.Condition;
using DAL.Model.Contract_Condition;
using DAL.Model.JunFuAddress_Condition;
using DAL.Model.JunFuTrans_Condition;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.International.Converters.TraditionalChineseToSimplifiedConverter;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Transactions;
using static Common.Setting.EnumSetting;
using static Common.Setting.JobSetting;



namespace BLL
{
    public class MoneySettleServices
    {
        private readonly IConfiguration _configuration;
        private ItcDeliveryRequests_DA _tcDeliveryRequests;
        private IMoney_Settle_Detail_DA _Money_Settle_Detail_DA;
        private ICBM_Detail_DA _CBM_Detail_DA;
        private IAreaType_DA _AreaType_DA;
        private IBagNobinding_DA _BagNobinding_DA;
        private ItbCustomers_DA _tbCustomers_DA;
        private ICBMDetail_DA _CBMDetail_DA;
        private ISpecialAreaAudit_DA _SpecialAreaAudit_DA;
        private ICBMAudit_DA _CBMAudit_DA;
        private ILogRecord_DA _LogRecord;
        private IProductValuationManage_DA _ProductValuationManage_DA;

        public MoneySettleServices(
            IConfiguration configuration,
            ItcDeliveryRequests_DA tcDeliveryRequests,
            IMoney_Settle_Detail_DA Money_Settle_Detail,
            ICBM_Detail_DA CBM_Detail,
            IAreaType_DA AreaType,
            IBagNobinding_DA BagNobinding,
            ItbCustomers_DA tbCustomers,
            ICBMDetail_DA CBMDetail,
            ISpecialAreaAudit_DA SpecialAreaAudit,
            ICBMAudit_DA CBMAudit,
            ILogRecord_DA LogRecord,
            IProductValuationManage_DA ProductValuationManage
            )
        {
            _configuration = configuration;
            _tcDeliveryRequests = tcDeliveryRequests;
            _Money_Settle_Detail_DA = Money_Settle_Detail;
            _CBM_Detail_DA = CBM_Detail;
            _AreaType_DA = AreaType;
            _BagNobinding_DA = BagNobinding;
            _tbCustomers_DA = tbCustomers;
            _CBMDetail_DA = CBMDetail;
            _SpecialAreaAudit_DA = SpecialAreaAudit;
            _CBMAudit_DA = CBMAudit; 
            _LogRecord = LogRecord;
            _ProductValuationManage_DA = ProductValuationManage;
        }

        public async Task InsertMoneySettleDetail()
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

            //List<Money_Settle_Detail_Condition> insertMoneySettleDetails = new List<Money_Settle_Detail_Condition>();

            //List<CBM_Detail_Condition> insertCBMDetails = new List<CBM_Detail_Condition>();

            DateTime date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM")).AddMonths(-1);

            var results = await _tcDeliveryRequests.GetInfoByArriveDate(date);

            
            sw.Reset();//碼表歸零

            sw.Start();//碼表開始計時
            
            //抓蛋黃白區    
            var AllAreaType = await _AreaType_DA.GetAllAreaType();

            var AllCustomer = await _tbCustomers_DA.GetInfoAllCustomer();
            
            //抓材積資料，包含單筆多件
            var AllCBMInfo = (await _CBMDetail_DA.GetCBMInfo()).ToList();
            
            //抓捕特服區費用
            var AllSpecialAreaAudit = await _SpecialAreaAudit_DA.GetSpecialAreaAudit();
            
            //抓捕超材費用
            var AllCBMAudit = await _CBMAudit_DA.GetCBMAudit();
            //using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(10), TransactionScopeAsyncFlowOption.Enabled))
            //{

            foreach (var result in results)
                {
                    Money_Settle_Detail_Condition insertMoneySettleDetail = new Money_Settle_Detail_Condition();

                // var AreaType =await _AreaType_DA.GetAreaType(result.receive_city, result.receive_area);

                 var AreaType = AllAreaType.Where(x => (x.City == result.receive_city) & (x.Area == result.receive_area)).FirstOrDefault();

                 var isNewCustomer = AllCustomer.Where(x => x.customer_code == result.customer_code).FirstOrDefault();

                 var SpecialAreaAudit = AllSpecialAreaAudit.Where(x => x.CheckNumber == result.check_number).FirstOrDefault();

                 var CBMAudit = AllCBMAudit.Where(x => x.CheckNumber == result.check_number).FirstOrDefault();
                    try
                    {
                        insertMoneySettleDetail.RequestId = Convert.ToInt32(result.request_id);
                        insertMoneySettleDetail.DeliveryType = string.IsNullOrEmpty(result.DeliveryType) ? "" : result.DeliveryType;
                        insertMoneySettleDetail.CheckNumber = string.IsNullOrEmpty(result.check_number) ? "" : result.check_number;
                        insertMoneySettleDetail.CreateDate = DateTime.Now;
                        insertMoneySettleDetail.CustomerCode = string.IsNullOrEmpty(result.customer_code) ? "" : result.customer_code;
                        insertMoneySettleDetail.SendStation = result.send_station_scode == null ? "" : result.send_station_scode;
                        insertMoneySettleDetail.ArriveStation = result.area_arrive_code == null ? "" : result.area_arrive_code;
                        insertMoneySettleDetail.City = result.receive_city == null ? "" : result.receive_city;
                        insertMoneySettleDetail.Area = result.receive_area == null ? "" : result.receive_area;
                        insertMoneySettleDetail.Address = result.receive_address == null ? "" : result.receive_address;
                        insertMoneySettleDetail.Pieces = result.pieces == null ? 0 : Convert.ToInt32(result.pieces);
                        insertMoneySettleDetail.ReceiptFlag = result.receipt_flag == null ? false : (bool)result.receipt_flag;
                        insertMoneySettleDetail.CollectionMoney = result.collection_money == null ? 0 : Convert.ToInt32(result.collection_money);
                        insertMoneySettleDetail.RoundTrip = result.round_trip == null ? false : (bool)result.round_trip;
                        insertMoneySettleDetail.PrintDate = result.print_date;
                        insertMoneySettleDetail.ShipDate = result.ship_date;
                        insertMoneySettleDetail.OrderNumber = result.order_number == null ? "" : result.order_number;
                        insertMoneySettleDetail.CustomerName = result.customer_name == null ? "" : result.customer_name;
                        insertMoneySettleDetail.ReceiveContact = result.receive_contact == null ? "" : result.receive_contact;
                        insertMoneySettleDetail.InvoiceDesc = result.invoice_desc == null ? "" : result.invoice_desc;
                        insertMoneySettleDetail.ScanDate = result.scan_date;
                        insertMoneySettleDetail.CreateUser = "ADMIN";

                        insertMoneySettleDetail.CustomerStation = result.CustomerStation;

                        if (string.IsNullOrEmpty(result.ProductId))
                        {
                            //ProductId 預設要分是否為新舊合約客代
                            if(isNewCustomer.is_new_customer == true)
                            {
                                if (result.pieces > 1)
                                {
                                    insertMoneySettleDetail.ProductId = "CM000030";
                                }
                                else
                                {
                                    insertMoneySettleDetail.ProductId = "CM000030";
                                }
                            }
                            else
                            {
                                if (result.pieces > 1)
                                {
                                    insertMoneySettleDetail.ProductId = "CM000036";
                                }
                                else
                                {
                                    insertMoneySettleDetail.ProductId = "CS000035";
                                }
                            }
                                                 
                        }
                        else
                        {
                            insertMoneySettleDetail.ProductId = result.ProductId;
                        }
                        //配達司機工號
                        insertMoneySettleDetail.DriverCode = result.driver_code;
                        //配達司機所屬站所
                        insertMoneySettleDetail.ArriveDriverStation = result.ArriveDriverStation;

                        insertMoneySettleDetail.ReceiveDriverStation = result.ReceiveDriverStation;

                        insertMoneySettleDetail.ReceiveDriverCode = result.ReceiveDriverCode;
                        
                        //判斷是否補特服區費用
                        if(SpecialAreaAudit != null)
                        {
                            insertMoneySettleDetail.SpecialAreaFee = SpecialAreaAudit.SpecialAreaId;
                            insertMoneySettleDetail.SpecialAreaId = SpecialAreaAudit.SpecialAreaFee;
                        }
                        else 
                        {
                            insertMoneySettleDetail.SpecialAreaFee = result.SpecialAreaFee;
                            insertMoneySettleDetail.SpecialAreaId = result.SpecialAreaId;
                        }

                
                    //重新跑特服區地址費用
                    //string Address = insertMoneySettleDetail.City + insertMoneySettleDetail.Area + insertMoneySettleDetail.Address;

                    //if (!string.IsNullOrEmpty(Address))
                    //{
                    //    var SpecialArea = GetAddressParsing(Address, insertMoneySettleDetail.CustomerCode, "1");
                    //    if (SpecialArea != null)
                    //    {
                    //        insertMoneySettleDetail.SpecialAreaFee = (int)SpecialArea.Fee;
                    //        insertMoneySettleDetail.SpecialAreaId = (int)SpecialArea.id;
                    //    }
                    //    else
                    //    {
                    //        insertMoneySettleDetail.SpecialAreaFee = result.SpecialAreaFee;
                    //        insertMoneySettleDetail.SpecialAreaId = result.SpecialAreaId;
                    //    }
                    //}
                    //else
                    //{
                    //    insertMoneySettleDetail.SpecialAreaFee = result.SpecialAreaFee;
                    //    insertMoneySettleDetail.SpecialAreaId = result.SpecialAreaId;
                    //}

                        if (AreaType == null)
                        {
                            insertMoneySettleDetail.EggArea = "1";
                        }
                        else
                        {
                            insertMoneySettleDetail.EggArea = AreaType.Type;
                        }

                        insertMoneySettleDetail.Request_CreateDate = (DateTime)result.cdate;

                        insertMoneySettleDetail.ArriveOption = result.ArriveOption;

                        insertMoneySettleDetail.ScanLogCdate = result.ScanLogCdate;

                        //if(result.cbmLength !=null && result.cbmHeight !=null && result.cbmWidth !=null)
                        //{
                        //    double OverCBM = (double)result.cbmLength + (double)result.cbmHeight + (double)result.cbmWidth;

                        //    if (OverCBM > 1550)
                        //    {
                        //        OverCBM =  Math.Ceiling((OverCBM - 1550) /100);
                        //        insertMoneySettleDetail.OverCBM = Convert.ToInt32(OverCBM);
                        //    }
                        //}

                        var insertId = _Money_Settle_Detail_DA.Insert(insertMoneySettleDetail);


                    //CBMDetail_DA _CBMDetail_DA = new CBMDetail_DA();

                    var CBMInfo = AllCBMInfo.Where(x => (x.CheckNumber == result.check_number));


                    //判斷丈量機單筆多件抓到的數目是否與實際件數相等，多退少補
                    if ((result.pieces - CBMInfo.Count()) == 0)
                    {
                        int i = 1;
                        foreach (var item in CBMInfo)
                        {
                            
                            CBM_Detail_Condition insertCBMDetail = new CBM_Detail_Condition();

                            if (CBMAudit != null)
                            {
                                item.Length = CBMAudit.CbmSizeLength == null ? 0 : CBMAudit.CbmSizeLength;
                                item.Width = CBMAudit.CbmSizeWidth == null ? 0 : CBMAudit.CbmSizeWidth;
                                item.Height = CBMAudit.CbmSizeHeight == null ? 0 : CBMAudit.CbmSizeHeight;
                            }

                            insertCBMDetail.Length = item.Length == null ? 0 : item.Length;
                            insertCBMDetail.Width = item.Width == null ? 0 : item.Width;
                            insertCBMDetail.Height = item.Height == null ? 0 : item.Height;

                            insertCBMDetail.DetailID = insertId;
                            insertCBMDetail.SN = i;
                            insertCBMDetail.CreateDate = DateTime.Now;
                            insertCBMDetail.CreateUser = "ADMIN";

                            
                            if (item.CBM == null || CBMAudit != null)
                            {
                                //重新計算材積大小
                                int? sum = insertCBMDetail.Length + insertCBMDetail.Width + insertCBMDetail.Height;
                                insertCBMDetail.CBM = GetCBMSize(sum);
                            }
                            else 
                            {
                                //代裝綁定的材積需要補個0
                                //var invoicePattern = "B0[0-9]{1}";

                                //if (Regex.Match(item.CBM, invoicePattern).Success)
                                //{
                                 
                                //    insertCBMDetail.CBM = "B00"+ item.CBM.Substring(2,1);
                                //}
                                //else
                                //{
                                //    insertCBMDetail.CBM = item.CBM; ;
                                //}

                                insertCBMDetail.CBM = GetCBMTrans(item.CBM);
                            }
                            //計算超材
                            if (item.Length != null && item.Width != null && item.Height != null)
                            {
                                double OverCBM = (double)item.Length + (double)item.Width + (double)item.Height;
                                insertCBMDetail.OverCBM = GetOverCBM(OverCBM);
                            }

                            i += 1;

                            await _CBM_Detail_DA.Insert(insertCBMDetail);
                        }

                    }
                    else if ((result.pieces - CBMInfo.Count()) > 0)
                    {
                        int i = 1;
                        foreach (var item in CBMInfo)
                        {
                            CBM_Detail_Condition insertCBMDetail = new CBM_Detail_Condition();
                            insertCBMDetail.DetailID = insertId;
                            insertCBMDetail.SN = i;
                            insertCBMDetail.CreateDate = DateTime.Now;
                            insertCBMDetail.CreateUser = "ADMIN";


                            if (CBMAudit != null)
                            {
                                item.Length = CBMAudit.CbmSizeLength == null ? 0 : CBMAudit.CbmSizeLength;
                                item.Width = CBMAudit.CbmSizeWidth == null ? 0 : CBMAudit.CbmSizeWidth;
                                item.Height = CBMAudit.CbmSizeHeight == null ? 0 : CBMAudit.CbmSizeHeight;
                            }

                            insertCBMDetail.Length = item.Length == null ? 0 : item.Length;
                            insertCBMDetail.Width = item.Width == null ? 0 : item.Width;
                            insertCBMDetail.Height = item.Height == null ? 0 : item.Height;



                            if (item.CBM == null || CBMAudit != null)
                            {
                                int? sum = insertCBMDetail.Length + insertCBMDetail.Width + insertCBMDetail.Height;
                                insertCBMDetail.CBM = GetCBMSize(sum);
                            }
                            else
                            {
                                //var invoicePattern = "B0[0-9]{1}";

                                //if (Regex.Match(item.CBM, invoicePattern).Success)
                                //{

                                //    insertCBMDetail.CBM = "B00" + item.CBM.Substring(2, 1);
                                //}
                                //else
                                //{
                                //    insertCBMDetail.CBM = item.CBM; ;
                                //}
                                insertCBMDetail.CBM = GetCBMTrans(item.CBM);
                            }

                            if (item.Length != null && item.Width != null && item.Height != null)
                            {
                                double OverCBM = (double)item.Length + (double)item.Width + (double)item.Height;
                                insertCBMDetail.OverCBM = GetOverCBM(OverCBM);
                            }

                            i += 1;

                            await _CBM_Detail_DA.Insert(insertCBMDetail);
                        }

                        for (int k = i; k <= result.pieces; k++)
                        {
                            CBM_Detail_Condition insertCBMDetail = new CBM_Detail_Condition();
                            insertCBMDetail.DetailID = insertId;
                            insertCBMDetail.SN = k;
                            insertCBMDetail.Length = 0;
                            insertCBMDetail.Width = 0;
                            insertCBMDetail.Height = 0;
                            insertCBMDetail.CreateDate = DateTime.Now;
                            insertCBMDetail.CreateUser = "ADMIN";
                            insertCBMDetail.CBM = "S090";

                            await _CBM_Detail_DA.Insert(insertCBMDetail);
                        }

                    }
                    else
                    {
                        int i = 1;
                        foreach (var item in CBMInfo)
                        {
                            CBM_Detail_Condition insertCBMDetail = new CBM_Detail_Condition();
                            insertCBMDetail.DetailID = insertId;
                            insertCBMDetail.SN = i;
                            insertCBMDetail.CreateDate = DateTime.Now;
                            insertCBMDetail.CreateUser = "ADMIN";

                            if (CBMAudit != null)
                            {
                                item.Length = CBMAudit.CbmSizeLength == null ? 0 : CBMAudit.CbmSizeLength;
                                item.Width = CBMAudit.CbmSizeWidth == null ? 0 : CBMAudit.CbmSizeWidth;
                                item.Height = CBMAudit.CbmSizeHeight == null ? 0 : CBMAudit.CbmSizeHeight;
                            }

                            insertCBMDetail.Length = item.Length == null ? 0 : item.Length;
                            insertCBMDetail.Width = item.Width == null ? 0 : item.Width;
                            insertCBMDetail.Height = item.Height == null ? 0 : item.Height;

                            if (item.CBM == null || CBMAudit != null)
                            {
                                int? sum = insertCBMDetail.Length + insertCBMDetail.Width + insertCBMDetail.Height;
                                insertCBMDetail.CBM = GetCBMSize(sum);
                            }
                            else
                            {
                                //var invoicePattern = "B0[0-9]{1}";

                                //if (Regex.Match(item.CBM, invoicePattern).Success)
                                //{

                                //    insertCBMDetail.CBM = "B00" + item.CBM.Substring(2, 1);
                                //}
                                //else
                                //{
                                //    insertCBMDetail.CBM = item.CBM; ;
                                //}
                                insertCBMDetail.CBM = GetCBMTrans(item.CBM);
                            }

                            if (item.Length != null && item.Width != null && item.Height != null)
                            {
                                double OverCBM = (double)item.Length + (double)item.Width + (double)item.Height;
                                insertCBMDetail.OverCBM = GetOverCBM(OverCBM);
                            }

                            i += 1;

                            await _CBM_Detail_DA.Insert(insertCBMDetail);

                            if (i - result.pieces == 1)
                            {
                                break;
                            }
                        }

                    }

                        //for (int i = 1; i <= result.pieces; i++)
                        //{
                        //    CBM_Detail_Condition insertCBMDetail = new CBM_Detail_Condition();
                        //    insertCBMDetail.DetailID = insertId;
                        //    insertCBMDetail.SN = i;
                        //    insertCBMDetail.Length = result.cbmLength == null ? 0 : (double)result.cbmLength;
                        //    insertCBMDetail.Width = result.cbmWidth == null ? 0 : (double)result.cbmWidth;
                        //    insertCBMDetail.Height = result.cbmHeight == null ? 0 : (double)result.cbmHeight;
                        //    insertCBMDetail.CreateDate = DateTime.Now;
                        //    insertCBMDetail.CreateUser = "ADMIN";
                            
                        //    var BagNo = await _BagNobinding_DA.GetBagNo(result.request_id.ToString());

                        //    var invoicePattern = "B0[0-9]{1}";
                        

                        //    if (BagNo != null )
                        //    {
                        //        if (Regex.Match(BagNo.BagNo, invoicePattern).Success)
                        //        {
                        //            insertCBMDetail.CBM = "B003";
                        //        }
                        //        else 
                        //        {
                        //        continue;
                        //        }
                                
                        //    }
                        //    else
                        //    {
                        //        double sum = insertCBMDetail.Length + insertCBMDetail.Width + insertCBMDetail.Height;
                        //        if (sum < 1)
                        //        {
                        //            insertCBMDetail.CBM = "S090";
                        //        }
                        //        else if (sum <= 650)
                        //        {
                        //            insertCBMDetail.CBM = "S060";
                        //        }
                        //        else if (sum <= 950)
                        //        {
                        //            insertCBMDetail.CBM = "S090";
                        //        }
                        //        else if (sum <= 1150)
                        //        {
                        //            insertCBMDetail.CBM = "S110";
                        //        }
                        //        else if (sum <= 1250)
                        //        {
                        //            insertCBMDetail.CBM = "S120";
                        //        }
                        //        else
                        //        {
                        //            insertCBMDetail.CBM = "S150";
                        //        }
                        //    }
                        //    await _CBM_Detail_DA.Insert(insertCBMDetail);
                        //}                   

                    }
                    catch (Exception e)
                    {
                        throw e;
                    }

                }
                try
                {
                    

                }
                catch (Exception e)
                {
                    sw.Stop();//碼錶停止

                    //印出所花費的總豪秒數

                    string result1 = sw.Elapsed.TotalMilliseconds.ToString();

                    throw e;
                }



            //    _transactionScope.Complete();
            //}
        }

        public async Task InsertMoneySettleDetail_new()
        {
            _LogRecord.Save("InsertMoneySettleDetail", "1", "");
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

            //List<Money_Settle_Detail_Condition> insertMoneySettleDetails = new List<Money_Settle_Detail_Condition>();

            //List<CBM_Detail_Condition> insertCBMDetails = new List<CBM_Detail_Condition>();

            DateTime date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM")).AddMonths(-1);

            var results = await _tcDeliveryRequests.GetInfoByArriveDate(DateTime.Now, DateTime.Now);

            
            sw.Reset();//碼表歸零

            sw.Start();//碼表開始計時
            
            //抓蛋黃白區    
            var AllAreaType = await _AreaType_DA.GetAllAreaType();

            var AllCustomer = await _tbCustomers_DA.GetInfoAllCustomer();
            
            //抓材積資料，包含單筆多件
            var AllCBMInfo = (await _CBMDetail_DA.GetCBMInfo()).ToList();
            
            //抓捕特服區費用
            var AllSpecialAreaAudit = await _SpecialAreaAudit_DA.GetSpecialAreaAudit();
            
            //抓捕超材費用
            var AllCBMAudit = await _CBMAudit_DA.GetCBMAudit();
            //using (var _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(10), TransactionScopeAsyncFlowOption.Enabled))
            //{

            
            //準備最後一起匯入的資料
            List<Money_Settle_Detail_Condition> FinalMoneySettleList = new List<Money_Settle_Detail_Condition>();

            long startId = _Money_Settle_Detail_DA.GetKey();

            //準備最後一起匯入的資料
            List<CBM_Detail_Condition> FinalCBMList = new List<CBM_Detail_Condition>();

            /*
            var query01 =
                        from a in results
                        join b in AllAreaType 
                        on new { City = a.receive_city, Area = a.receive_area} 
                           equals 
                           new { b.City, b.Area}
                        into gj
                        from newResult in gj.DefaultIfEmpty()
                        select new
                        {
                            a,
                            Type = newResult?.Type ?? string.Empty
                        };


            var query02 =
                        from a01 in query01
                        join b in AllCustomer
                        on a01.a.customer_code
                           equals
                           b.customer_code
                        into gj
                        from newResult in gj.DefaultIfEmpty()
                        select new
                        {
                            a01.a,
                            a01.Type,
                            is_new_customer = newResult?.is_new_customer ?? false
                        };

            var query03 =
                        from a01 in query02
                        join b in AllSpecialAreaAudit
                        on a01.a.check_number
                           equals
                           b.CheckNumber
                        into gj
                        from newResult in gj.DefaultIfEmpty()
                        select new
                        {
                            a01.a,
                            a01.Type,
                            a01.is_new_customer,
                            SpecialAreaId = newResult?.SpecialAreaId ?? null,
                            SpecialAreaFee = newResult?.SpecialAreaFee ?? null
                        };


            var query04 =
                        from a01 in query03
                        join b in AllCBMAudit
                        on a01.a.check_number
                           equals
                           b.CheckNumber
                        into gj
                        from newResult in gj.DefaultIfEmpty()
                        select new
                        {
                            a01.a,
                            a01.Type,
                            a01.is_new_customer,
                            a01.SpecialAreaId , 
                            a01.SpecialAreaFee ,
                            CbmSizeLength = newResult?.CbmSizeLength ?? null,
                            CbmSizeWidth = newResult?.CbmSizeWidth ?? null,
                            CbmSizeHeight = newResult?.CbmSizeHeight ?? null
                        };
            */

            _LogRecord.Save("InsertMoneySettleDetail", "2", "");

            foreach (var result in results)
            {
                //_LogRecord.Save("InsertMoneySettleDetail", "1", "");

                if (FinalCBMList.Count % 10000 == 0)
                {
                    _LogRecord.Save("InsertMoneySettleDetail", "3", FinalCBMList.Count.ToString());
                }

                Money_Settle_Detail_Condition insertMoneySettleDetail = new Money_Settle_Detail_Condition();

                // var AreaType =await _AreaType_DA.GetAreaType(result.receive_city, result.receive_area);

                var CustomerData = AllCustomer.Where(x => x.customer_code == result.customer_code).FirstOrDefault();

                var AreaType = AllAreaType.Where(x => (x.City == result.receive_city) & (x.Area == result.receive_area)).FirstOrDefault();

                var SpecialAreaAudit = AllSpecialAreaAudit.Where(x => x.CheckNumber == result.check_number).FirstOrDefault();

                 var CBMAudit = AllCBMAudit.Where(x => x.CheckNumber == result.check_number).FirstOrDefault();
                 try
                 {
                        insertMoneySettleDetail.RequestId = Convert.ToInt32(result.request_id);
                        insertMoneySettleDetail.DeliveryType = string.IsNullOrEmpty(result.DeliveryType) ? "" : result.DeliveryType;
                        insertMoneySettleDetail.CheckNumber = string.IsNullOrEmpty(result.check_number) ? "" : result.check_number;
                        insertMoneySettleDetail.CreateDate = DateTime.Now;
                        insertMoneySettleDetail.CustomerCode = string.IsNullOrEmpty(result.customer_code) ? "" : result.customer_code;
                        insertMoneySettleDetail.SendStation = result.send_station_scode == null ? "" : result.send_station_scode;
                        insertMoneySettleDetail.ArriveStation = result.area_arrive_code == null ? "" : result.area_arrive_code;
                        insertMoneySettleDetail.City = result.receive_city == null ? "" : result.receive_city;
                        insertMoneySettleDetail.Area = result.receive_area == null ? "" : result.receive_area;
                        insertMoneySettleDetail.Address = result.receive_address == null ? "" : result.receive_address;
                        insertMoneySettleDetail.Pieces = result.pieces == null ? 0 : Convert.ToInt32(result.pieces);
                        insertMoneySettleDetail.ReceiptFlag = result.receipt_flag == null ? false : (bool)result.receipt_flag;
                        insertMoneySettleDetail.CollectionMoney = result.collection_money == null ? 0 : Convert.ToInt32(result.collection_money);
                        insertMoneySettleDetail.RoundTrip = result.round_trip == null ? false : (bool)result.round_trip;
                        insertMoneySettleDetail.PrintDate = result.print_date;
                        insertMoneySettleDetail.ShipDate = result.ship_date;
                        insertMoneySettleDetail.OrderNumber = result.order_number == null ? "" : result.order_number;
                        insertMoneySettleDetail.CustomerName = result.customer_name == null ? "" : result.customer_name;
                        insertMoneySettleDetail.ReceiveContact = result.receive_contact == null ? "" : result.receive_contact;
                        insertMoneySettleDetail.InvoiceDesc = result.invoice_desc == null ? "" : result.invoice_desc;
                        insertMoneySettleDetail.ScanDate = result.scan_date;
                         insertMoneySettleDetail.Valued = result.ProductValue == null ? 0 : result.ProductValue;
                        insertMoneySettleDetail.CreateUser = "ADMIN";


                        //是否為宜花東
                        var East = new List<string>{"宜蘭縣","花蓮縣","臺東縣","台東縣", "宜蘭市", "花蓮市", "臺東市", "台東市" };
                        if(East.Contains(insertMoneySettleDetail.City))
                        {
                            insertMoneySettleDetail.isEast = true;
                        }
                        else
                        {
                            insertMoneySettleDetail.isEast = false;
                        }


                        insertMoneySettleDetail.CustomerStation = result.CustomerStation;

                        if (string.IsNullOrEmpty(result.ProductId))
                        {
                            //ProductId 預設要分是否為新舊合約客代

                            if (CustomerData.product_type == 1)
                            {
                                if (CustomerData.is_new_customer == true)
                                {
                                    if (result.pieces > 1)
                                    {
                                        insertMoneySettleDetail.ProductId = "CM000030";
                                    }
                                    else
                                    {
                                        insertMoneySettleDetail.ProductId = "CM000030";
                                    }
                                }
                                else
                                {
                                    if (result.pieces > 1)
                                    {
                                        insertMoneySettleDetail.ProductId = "CM000036";
                                    }
                                    else
                                    {
                                        insertMoneySettleDetail.ProductId = "CS000035";
                                    }
                                }

                            }
                            else if (CustomerData.product_type == 6)
                            {

                                if (result.pieces > 1)
                                {
                                    insertMoneySettleDetail.ProductId = "CM000043";
                                }
                                else
                                {
                                    insertMoneySettleDetail.ProductId = "CS000042";
                                }

                            }
                            else if (CustomerData.product_type == 2)
                            {

                                insertMoneySettleDetail.ProductId = "PS000031";
                            }
                            else if (CustomerData.product_type == 3)
                            {
                                insertMoneySettleDetail.ProductId = "PS000034";
                            }
                        }
                        else
                        {
                            insertMoneySettleDetail.ProductId = result.ProductId;
                        }
                        //配達司機工號
                        insertMoneySettleDetail.DriverCode = result.driver_code;
                        //配達司機所屬站所
                        insertMoneySettleDetail.ArriveDriverStation = result.ArriveDriverStation;

                        insertMoneySettleDetail.ReceiveDriverStation = result.ReceiveDriverStation;

                        insertMoneySettleDetail.ReceiveDriverCode = result.ReceiveDriverCode;
                        
                        //判斷是否補特服區費用
                        if(SpecialAreaAudit != null)
                        {
                            insertMoneySettleDetail.SpecialAreaFee = SpecialAreaAudit.SpecialAreaFee;
                            insertMoneySettleDetail.SpecialAreaId = SpecialAreaAudit.SpecialAreaId;
                        }
                        else 
                        {
                            insertMoneySettleDetail.SpecialAreaFee = result.SpecialAreaFee;
                            insertMoneySettleDetail.SpecialAreaId = result.SpecialAreaId;
                        }

                
                    //重新跑特服區地址費用
                    //string Address = insertMoneySettleDetail.City + insertMoneySettleDetail.Area + insertMoneySettleDetail.Address;

                    //if (!string.IsNullOrEmpty(Address))
                    //{
                    //    var SpecialArea = GetAddressParsing(Address, insertMoneySettleDetail.CustomerCode, "1");
                    //    if (SpecialArea != null)
                    //    {
                    //        insertMoneySettleDetail.SpecialAreaFee = (int)SpecialArea.Fee;
                    //        insertMoneySettleDetail.SpecialAreaId = (int)SpecialArea.id;
                    //    }
                    //    else
                    //    {
                    //        insertMoneySettleDetail.SpecialAreaFee = result.SpecialAreaFee;
                    //        insertMoneySettleDetail.SpecialAreaId = result.SpecialAreaId;
                    //    }
                    //}
                    //else
                    //{
                    //    insertMoneySettleDetail.SpecialAreaFee = result.SpecialAreaFee;
                    //    insertMoneySettleDetail.SpecialAreaId = result.SpecialAreaId;
                    //}

                        if (AreaType == null)
                        {
                            insertMoneySettleDetail.EggArea = "1";
                        }
                        else
                        {
                            insertMoneySettleDetail.EggArea = AreaType.Type;
                        }

                        insertMoneySettleDetail.Request_CreateDate = (DateTime)result.cdate;

                        insertMoneySettleDetail.ArriveOption = result.ArriveOption;

                        insertMoneySettleDetail.ScanLogCdate = result.ScanLogCdate;

                    //if(result.cbmLength !=null && result.cbmHeight !=null && result.cbmWidth !=null)
                    //{
                    //    double OverCBM = (double)result.cbmLength + (double)result.cbmHeight + (double)result.cbmWidth;

                    //    if (OverCBM > 1550)
                    //    {
                    //        OverCBM =  Math.Ceiling((OverCBM - 1550) /100);
                    //        insertMoneySettleDetail.OverCBM = Convert.ToInt32(OverCBM);
                    //    }
                    //}


                    //var insertId = _Money_Settle_Detail_DA.Insert(insertMoneySettleDetail);
                    startId += 1;
                    insertMoneySettleDetail.id = startId;
                    var insertId = insertMoneySettleDetail.id;
                    FinalMoneySettleList.Add(insertMoneySettleDetail);



                    //CBMDetail_DA _CBMDetail_DA = new CBMDetail_DA();

                    var CBMInfo = AllCBMInfo.Where(x => (x.CheckNumber == result.check_number));


                    //判斷丈量機單筆多件抓到的數目是否與實際件數相等，多退少補
                    if ((result.pieces - CBMInfo.Count()) == 0)
                    {
                        int i = 1;
                        foreach (var item in CBMInfo)
                        {

                            CBM_Detail_Condition insertCBMDetail = new CBM_Detail_Condition();

                            if (CBMAudit != null)
                            {
                                item.Length = CBMAudit.CbmSizeLength == null ? 0 : CBMAudit.CbmSizeLength;
                                item.Width = CBMAudit.CbmSizeWidth == null ? 0 : CBMAudit.CbmSizeWidth;
                                item.Height = CBMAudit.CbmSizeHeight == null ? 0 : CBMAudit.CbmSizeHeight;
                            }

                            insertCBMDetail.Length = item.Length == null ? 0 : item.Length;
                            insertCBMDetail.Width = item.Width == null ? 0 : item.Width;
                            insertCBMDetail.Height = item.Height == null ? 0 : item.Height;

                            insertCBMDetail.DetailID = insertId;
                            insertCBMDetail.SN = i;
                            insertCBMDetail.CreateDate = DateTime.Now;
                            insertCBMDetail.CreateUser = "ADMIN";

                            if(CustomerData.product_type == 6)
                            {
                                
                                var AllCBMList = await _ProductValuationManage_DA.GetProductValuateList(result.DeliveryType, result.customer_code, result.pieces);

                                var CBMList = AllCBMList.Where(x =>(x.Spec == result.SpecCodeId));
                       
                                var MaxCBM = await _ProductValuationManage_DA.GetMaxProductValuate(result.DeliveryType, result.customer_code, result.pieces);

                                int MeasureScanCuft = GetCBMCulft(insertCBMDetail.Length, insertCBMDetail.Width, insertCBMDetail.Height);

                                //var OverCuft = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);//用自己填入的材積 與 丈量機量出來的計算超材

                                if (string.IsNullOrEmpty(result.SpecCodeId) || CBMList.Count() == 0)
                                {
                                    insertCBMDetail.CBM = MaxCBM.Spec.ToString();

                                    insertCBMDetail.OverCBM = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);
                                }
                                else
                                {
                                    insertCBMDetail.CBM = result.SpecCodeId;

                                    insertCBMDetail.OverCBM = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);
                                }

                            }
                            else
                            {
                                if (item.CBM == null || CBMAudit != null)
                                {
                                    //重新計算材積大小
                                    int? sum = insertCBMDetail.Length + insertCBMDetail.Width + insertCBMDetail.Height;
                                    insertCBMDetail.CBM = GetCBMSize(sum);
                                }
                                else
                                {
                                    insertCBMDetail.CBM = GetCBMTrans(item.CBM);
                                }

                                //計算超材
                                if (item.Length != null && item.Width != null && item.Height != null)
                                {
                                    double OverCBM = (double)item.Length + (double)item.Width + (double)item.Height;
                                    insertCBMDetail.OverCBM = GetOverCBM(OverCBM);
                                }
                            }
                                
                            i += 1;

                            FinalCBMList.Add(insertCBMDetail);
                            //await _CBM_Detail_DA.Insert(insertCBMDetail);
                        }

                    }
                    else if ((result.pieces - CBMInfo.Count()) > 0)
                    {
                        int i = 1;
                        foreach (var item in CBMInfo)
                        {
                            CBM_Detail_Condition insertCBMDetail = new CBM_Detail_Condition();
                            insertCBMDetail.DetailID = insertId;
                            insertCBMDetail.SN = i;
                            insertCBMDetail.CreateDate = DateTime.Now;
                            insertCBMDetail.CreateUser = "ADMIN";


                            if (CBMAudit != null)
                            {
                                item.Length = CBMAudit.CbmSizeLength == null ? 0 : CBMAudit.CbmSizeLength;
                                item.Width = CBMAudit.CbmSizeWidth == null ? 0 : CBMAudit.CbmSizeWidth;
                                item.Height = CBMAudit.CbmSizeHeight == null ? 0 : CBMAudit.CbmSizeHeight;
                            }

                            insertCBMDetail.Length = item.Length == null ? 0 : item.Length;
                            insertCBMDetail.Width = item.Width == null ? 0 : item.Width;
                            insertCBMDetail.Height = item.Height == null ? 0 : item.Height;


                            if (CustomerData.product_type == 6)

                            {
                                var AllCBMList = await _ProductValuationManage_DA.GetProductValuateList(result.DeliveryType, result.customer_code, result.pieces);

                                var CBMList = AllCBMList.Where(x => (x.Spec == result.SpecCodeId));

                                var MaxCBM = await _ProductValuationManage_DA.GetMaxProductValuate(result.DeliveryType, result.customer_code, result.pieces);

                                int MeasureScanCuft = GetCBMCulft(insertCBMDetail.Length, insertCBMDetail.Width, insertCBMDetail.Height);

                                //var OverCuft = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);//用自己填入的材積 與 丈量機量出來的計算超材

                                if (string.IsNullOrEmpty(result.SpecCodeId) || CBMList.Count() == 0)
                                {
                                    insertCBMDetail.CBM = MaxCBM.Spec.ToString();

                                    insertCBMDetail.OverCBM = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);
                                }
                                else
                                {
                                    insertCBMDetail.CBM = result.SpecCodeId;

                                    insertCBMDetail.OverCBM = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);
                                }

                            }
                            else
                            {
                                if (item.CBM == null || CBMAudit != null)
                                {
                                    int? sum = insertCBMDetail.Length + insertCBMDetail.Width + insertCBMDetail.Height;
                                    insertCBMDetail.CBM = GetCBMSize(sum);
                                }
                                else
                                {
                                    insertCBMDetail.CBM = GetCBMTrans(item.CBM);
                                }

                                if (item.Length != null && item.Width != null && item.Height != null)
                                {
                                    double OverCBM = (double)item.Length + (double)item.Width + (double)item.Height;
                                    insertCBMDetail.OverCBM = GetOverCBM(OverCBM);
                                }
                            }
                            

                            i += 1;

                            FinalCBMList.Add(insertCBMDetail);
                            //await _CBM_Detail_DA.Insert(insertCBMDetail);
                        }

                        for (int k = i; k <= result.pieces; k++)
                        {
                            CBM_Detail_Condition insertCBMDetail = new CBM_Detail_Condition();
                            insertCBMDetail.DetailID = insertId;
                            insertCBMDetail.SN = k;
                            insertCBMDetail.Length = 0;
                            insertCBMDetail.Width = 0;
                            insertCBMDetail.Height = 0;
                            insertCBMDetail.CreateDate = DateTime.Now;
                            insertCBMDetail.CreateUser = "ADMIN";
                            insertCBMDetail.CBM = "S090";

                            FinalCBMList.Add(insertCBMDetail);
                            //await _CBM_Detail_DA.Insert(insertCBMDetail);
                        }

                    }
                    else
                    {
                        int i = 1;
                        foreach (var item in CBMInfo)
                        {
                            CBM_Detail_Condition insertCBMDetail = new CBM_Detail_Condition();
                            insertCBMDetail.DetailID = insertId;
                            insertCBMDetail.SN = i;
                            insertCBMDetail.CreateDate = DateTime.Now;
                            insertCBMDetail.CreateUser = "ADMIN";

                            if (CBMAudit != null)
                            {
                                item.Length = CBMAudit.CbmSizeLength == null ? 0 : CBMAudit.CbmSizeLength;
                                item.Width = CBMAudit.CbmSizeWidth == null ? 0 : CBMAudit.CbmSizeWidth;
                                item.Height = CBMAudit.CbmSizeHeight == null ? 0 : CBMAudit.CbmSizeHeight;
                            }

                            insertCBMDetail.Length = item.Length == null ? 0 : item.Length;
                            insertCBMDetail.Width = item.Width == null ? 0 : item.Width;
                            insertCBMDetail.Height = item.Height == null ? 0 : item.Height;



                            if (CustomerData.product_type == 6)
                            {
                                var AllCBMList = await _ProductValuationManage_DA.GetProductValuateList(result.DeliveryType, result.customer_code, result.pieces);

                                var CBMList = AllCBMList.Where(x => (x.Spec == result.SpecCodeId));

                                var MaxCBM = await _ProductValuationManage_DA.GetMaxProductValuate(result.DeliveryType, result.customer_code, result.pieces);

                                int MeasureScanCuft = GetCBMCulft(insertCBMDetail.Length, insertCBMDetail.Width, insertCBMDetail.Height);

                                //var OverCuft = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);//用自己填入的材積 與 丈量機量出來的計算超材

                                if (string.IsNullOrEmpty(result.SpecCodeId) || CBMList.Count() == 0)
                                {
                                    insertCBMDetail.CBM = MaxCBM.Spec.ToString();

                                    insertCBMDetail.OverCBM = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);
                                }
                                else
                                {
                                    insertCBMDetail.CBM = result.SpecCodeId;

                                    insertCBMDetail.OverCBM = GetOverCulft(insertCBMDetail.CBM, MeasureScanCuft);
                                }

                            }
                            else
                            {
                                if (item.CBM == null || CBMAudit != null)
                                {
                                    int? sum = insertCBMDetail.Length + insertCBMDetail.Width + insertCBMDetail.Height;
                                    insertCBMDetail.CBM = GetCBMSize(sum);
                                }
                                else
                                {
                                    insertCBMDetail.CBM = GetCBMTrans(item.CBM);
                                }

                                if (item.Length != null && item.Width != null && item.Height != null)
                                {
                                    double OverCBM = (double)item.Length + (double)item.Width + (double)item.Height;
                                    insertCBMDetail.OverCBM = GetOverCBM(OverCBM);
                                }
                            }                           

                            i += 1;

                            FinalCBMList.Add(insertCBMDetail);
                            //await _CBM_Detail_DA.Insert(insertCBMDetail);

                            if (i - result.pieces == 1)
                            {
                                break;
                            }
                        }

                    }

                    //for (int i = 1; i <= result.pieces; i++)
                    //{
                    //    CBM_Detail_Condition insertCBMDetail = new CBM_Detail_Condition();
                    //    insertCBMDetail.DetailID = insertId;
                    //    insertCBMDetail.SN = i;
                    //    insertCBMDetail.Length = result.cbmLength == null ? 0 : (double)result.cbmLength;
                    //    insertCBMDetail.Width = result.cbmWidth == null ? 0 : (double)result.cbmWidth;
                    //    insertCBMDetail.Height = result.cbmHeight == null ? 0 : (double)result.cbmHeight;
                    //    insertCBMDetail.CreateDate = DateTime.Now;
                    //    insertCBMDetail.CreateUser = "ADMIN";

                    //    var BagNo = await _BagNobinding_DA.GetBagNo(result.request_id.ToString());

                    //    var invoicePattern = "B0[0-9]{1}";


                    //    if (BagNo != null )
                    //    {
                    //        if (Regex.Match(BagNo.BagNo, invoicePattern).Success)
                    //        {
                    //            insertCBMDetail.CBM = "B003";
                    //        }
                    //        else 
                    //        {
                    //        continue;
                    //        }

                    //    }
                    //    else
                    //    {
                    //        double sum = insertCBMDetail.Length + insertCBMDetail.Width + insertCBMDetail.Height;
                    //        if (sum < 1)
                    //        {
                    //            insertCBMDetail.CBM = "S090";
                    //        }
                    //        else if (sum <= 650)
                    //        {
                    //            insertCBMDetail.CBM = "S060";
                    //        }
                    //        else if (sum <= 950)
                    //        {
                    //            insertCBMDetail.CBM = "S090";
                    //        }
                    //        else if (sum <= 1150)
                    //        {
                    //            insertCBMDetail.CBM = "S110";
                    //        }
                    //        else if (sum <= 1250)
                    //        {
                    //            insertCBMDetail.CBM = "S120";
                    //        }
                    //        else
                    //        {
                    //            insertCBMDetail.CBM = "S150";
                    //        }
                    //    }
                    //    await _CBM_Detail_DA.Insert(insertCBMDetail);
                    //}                   

                }
                 catch (Exception e)
                 {
                        throw e;
                 }

            }


            try
            {
                   
                _Money_Settle_Detail_DA.Insert(FinalMoneySettleList);
                _LogRecord.Save("InsertMoneySettleDetail", "4", "MoneySettle -" + FinalMoneySettleList.Count.ToString());
                await _CBM_Detail_DA.Insert(FinalCBMList);
                _LogRecord.Save("InsertMoneySettleDetail", "4", "CBM - " + FinalCBMList.Count.ToString());
                _LogRecord.Save("InsertMoneySettleDetail", "5", "");
            }
            catch (Exception e)
            {
                    sw.Stop();//碼錶停止

                    //印出所花費的總豪秒數

                    string result1 = sw.Elapsed.TotalMilliseconds.ToString();

                    throw e;
            }



            //    _transactionScope.Complete();
            //}
        }

        public SpecialAreaManange GetAddressParsing(string Address, string CustomerCode, string ComeFrom, string Type = "1")
        {
            SpecialAreaManange Info = new SpecialAreaManange();
            var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetSpecialAreaDetail";

            //打API
            var client = new RestClient(AddressParsingURL);
            var request = new RestRequest(Method.POST);
            request.Timeout = 5000;
            request.ReadWriteTimeout = 5000;
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body

            var response = client.Post<ResData<SpecialAreaManange>>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                try
                {
                    Info = response.Data.Data;


                }
                catch (Exception e)
                {
                    throw e;

                }

            }
            else
            {
                //throw new Exception("地址解析有誤"); 
                Info = null;
            }

            return Info;
        }

        public string GetCBMSize(int? SumCBM)
        {
            string CBMSize = string.Empty;

             
            if (SumCBM < 1)
            {
                CBMSize = "S090";
            }
            else if (SumCBM <= 650)
            {
                CBMSize = "S060";
            }
            else if (SumCBM <= 950)
            {
                CBMSize = "S090";
            }
            else if (SumCBM <= 1150)
            {
                CBMSize = "S110";
            }
            else if (SumCBM <= 1250)
            {
                CBMSize = "S120";
            }
            else
            {
                CBMSize = "S150";
            }

            return CBMSize;
        }

        public int GetOverCBM( double OverCBM)
        {
            if (OverCBM > 1550)
            {
                int RetrunOverCBM = Convert.ToInt32(Math.Ceiling((OverCBM - 1550) / 100));
                return RetrunOverCBM;
            }
            else
            {
                return 0;
            }
        }
        //將有問題的CBM轉成正確的
        public string GetCBMTrans(string CBM)
        {
            
            var Pattern1 = "B0[0-9]{1}";
            var Pattern2 = "P0[0-9]{1}";
            var Pattern3 = "B[0-9]{1}";
            var Pattern4 = "O0[0-9]{1}";

            string FixCBM = string.Empty;

            if (Regex.Match(CBM, Pattern1).Success)
            {

                FixCBM = "B00" + CBM.Substring(2, 1);
            }
            else if (Regex.Match(CBM, Pattern2).Success)
            {
                FixCBM = "B00" + CBM.Substring(2, 1);
            }
            else if (Regex.Match(CBM, Pattern3).Success)
            {
                FixCBM = "X00" + CBM.Substring(1, 1);
            }
            else if (Regex.Match(CBM, Pattern4).Success)
            {
                FixCBM = "B00" + CBM.Substring(2, 1);
            }
            else
            {
                FixCBM = CBM;
            }

            return FixCBM;
        }


        public int GetCBMCulft(int? length, int? width, int? height )

        {

            int Culft = Convert.ToInt32(Math.Ceiling(
                                            (
                                                ((float)length) / 10.0 * ((float)width) / 10.0 * ((float)height) / 10.0
                                            ) / 27000.0
                                        ));
            return Culft;
        }
        //計算論件超材
        public int GetOverCulft(string Spec, int MeasureScanCuft)
        {

            int SpecNumber = 0;

            int OverCuft = 0;

            //SpecNumber = Convert.ToInt32(Spec.Substring(3, 1));//客戶自己輸入的材積

            switch (Spec)
            {
                case "C001":
                    SpecNumber = 1;
                    break;
                case "C002":
                    SpecNumber = 2;
                    break;
                case "C003":
                    SpecNumber = 3;
                    break;
                case "C004":
                    SpecNumber = 4;
                    break;
                case "C005":
                    SpecNumber = 5;
                    break;
            }


            if (MeasureScanCuft > SpecNumber)
            {
                OverCuft = (MeasureScanCuft - SpecNumber); //丈量機量的比客戶自己填的大;
            }
            else if (SpecNumber > MeasureScanCuft)
            {
                OverCuft = 0; //客戶自己填的比丈量機大;
            }

            return OverCuft;
        }

    }
}
