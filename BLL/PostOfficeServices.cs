﻿using BLL.Model.FSExpressAPI.Model;
using BLL.Model.FSExpressAPI.Req.PostOffice;
using BLL.Model.FSExpressAPI.Res.PostOffice;
using BLL.Model.ScheduleAPI.Model;
using BLL.Model.ScheduleAPI.Res.PostOffice;
using Common;
using Common.Excel;
using Common.Model;
using DAL.DA;
using DAL.Model.Condition;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualBasic;
using ServicePostStatus;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using static Common.Setting.EnumSetting;
using static Common.Setting.JobSetting;
using static ServicePostStatus.PSTTP_MailQuerySoapClient;

namespace BLL
{
    public class PostOfficeServices
    {
        private readonly IConfiguration _configuration;
        private IPostXml_DA _PostXml_DA;
        private IScheduleTask_DA _ScheduleTask_DA;
        private IPostXmlParse_DA _PostXmlParse_DA;
        private IPostXmlParseError_DA _PostXmlParseError_DA;
        private IPostXmlParseExclude_DA _PostXmlParseExclude_DA;
        private IPostXmlParseFilter_DA _PostXmlParseFilter_DA;
        private IPostState_DA _PostState_DA;
        private IttDeliveryScanLog_DA _ttDeliveryScanLog_DA;
        private IPost_request_DA _Post_request_DA;
        private IPost_DA _post_DA;
        private ItcDeliveryRequests_DA _itcDeliveryRequests_DA;
        private IPostEod_DA _postEod_DA;
        private IPostEodParse_DA _postEodParse_DA;
        private IPostEodParseError_DA _postEodParseError_DA;
        private IPostEodParseExclude_DA _postEodParseExclude_DA;
        private IPostEodParseFilter_DA _postEodParseFilter_DA;
        private IPostStatus_DA _postStatus_DA;
        private IPostTransmission_DA _postTransmission_DA;
        private IPostFTPTransportLog_DA _postFTPTransportLog_DA;
        private IPostResultEod_DA _postResultEod_DA;
        private IPostIrregularEod_DA _postIrregularEod_DA;
        private INotification_DA _Notification_DA;
        private IScheduleAPILog_DA _ScheduleAPILog_DA;

        public PostOfficeServices
            (
            IConfiguration configuration,
            IScheduleTask_DA ScheduleTask_DA,
            IPostXml_DA PostXml_DA,
            IPostXmlParse_DA PostXmlParse_DA,
            IPostXmlParseError_DA PostXmlParseError_DA,
            IPostXmlParseExclude_DA PostXmlParseExclude_DA,
            IPostXmlParseFilter_DA PostXmlParseFilter_DA,
            IPostState_DA PostState_DA,
            IttDeliveryScanLog_DA ttDeliveryScanLog_DA,
            IPost_request_DA Post_request_DA,
            IPost_DA post_DA,
            ItcDeliveryRequests_DA itcDeliveryRequests_DA,
            IPostTransmission_DA postTransmission_DA,
            IPostEod_DA postEod_DA,
            IPostEodParse_DA postEodParse_DA,
            IPostEodParseError_DA postEodParseError_DA,
            IPostEodParseExclude_DA postEodParseExclude_DA,
            IPostEodParseFilter_DA postEodParseFilter_DA,
            IPostStatus_DA postStatus_DA,
            IPostFTPTransportLog_DA postFTPTransportLog_DA,
            IPostResultEod_DA postResultEod_DA,
            IPostIrregularEod_DA postIrregularEod_DA,
            INotification_DA Notification_DA,
            IScheduleAPILog_DA ScheduleAPILog_DA
            )
        {
            _configuration = configuration;
            _PostXml_DA = PostXml_DA;
            _ScheduleTask_DA = ScheduleTask_DA;
            _PostXmlParse_DA = PostXmlParse_DA;
            _PostXmlParseError_DA = PostXmlParseError_DA;
            _PostXmlParseExclude_DA = PostXmlParseExclude_DA;
            _PostXmlParseFilter_DA = PostXmlParseFilter_DA;
            _PostState_DA = PostState_DA;
            _ttDeliveryScanLog_DA = ttDeliveryScanLog_DA;
            _Post_request_DA = Post_request_DA;
            _post_DA = post_DA;
            _itcDeliveryRequests_DA = itcDeliveryRequests_DA;
            _postEod_DA = postEod_DA;
            _postEodParse_DA = postEodParse_DA;
            _postEodParseError_DA = postEodParseError_DA;
            _postEodParseExclude_DA = postEodParseExclude_DA;
            _postEodParseFilter_DA = postEodParseFilter_DA;
            _postStatus_DA = postStatus_DA;
            _postTransmission_DA = postTransmission_DA;
            _postFTPTransportLog_DA = postFTPTransportLog_DA;
            _postResultEod_DA = postResultEod_DA;
            _postIrregularEod_DA = postIrregularEod_DA;
            _Notification_DA = Notification_DA;
            _ScheduleAPILog_DA = ScheduleAPILog_DA;
        }

        /// <summary>
        /// 上傳FTP 給郵局的出貨檔JOB
        /// </summary>
        /// <returns></returns>
        public async Task<TransportPostFTPJob_Res> TransportPostFTP()
        {
            TransportPostFTPJob_Res transportPostFTPJob_Res = new TransportPostFTPJob_Res();

            int OrderCount = 0;
            int ResultCount = 0;
            int IrregularCount = 0;
            int StatusCount = 0;

            try
            {

                var PostOfficeFTPServer = _configuration["PostOffice:FTPServer"];

                #region 上傳出貨檔
                //帳密
                var PostOfficeOrderAccount = _configuration["PostOffice:Order:Account"];
                var PostOfficeOrderPassWord = _configuration["PostOffice:Order:PassWord"];
                //NAS 路徑
                var PostOfficeOrderNASPath = _configuration["PostOffice:Order:NASPath"];
                var PostOfficeOrderNASPathBak = _configuration["PostOffice:Order:NASPathBak"];
                //FTP 路徑
                var PostOfficeOrderFTPPath = _configuration["PostOffice:Order:FTPPath"];
                var PostOfficeOrderFTPPathBak = _configuration["PostOffice:Order:FTPPathBak"];
                //取得所有上傳檔案
                DirectoryInfo Orderdirs = new DirectoryInfo(PostOfficeOrderNASPath);
                IEnumerable<FileInfo> Orderfiles = Orderdirs.GetFiles("*.eod");

                if (Orderfiles != null && Orderfiles.Count() > 0)
                {
                    FtpUtility OrderftpClient = new FtpUtility(PostOfficeFTPServer + "/" + PostOfficeOrderFTPPath, PostOfficeOrderAccount, PostOfficeOrderPassWord);

                    foreach (var _file in Orderfiles)
                    {
                        //紀錄DB
                        PostFTPTransportLog_Condition postFTPTransportLog_Condition = new PostFTPTransportLog_Condition();
                        try
                        {
                            //上傳檔案
                            /* Create Object Instance */
                            string full_PostOfficeOrderFTPPath = Path.Combine(PostOfficeOrderFTPPath, _file.Name);
                            string full_PostOfficeOrderNASPath = Path.Combine(PostOfficeOrderNASPath, _file.Name);
                            string full_PostOfficeOrderNASPathBak = Path.Combine(PostOfficeOrderNASPathBak, _file.Name);

                            OrderftpClient.upload(full_PostOfficeOrderFTPPath, full_PostOfficeOrderNASPath);
                            //寫入DB
                            postFTPTransportLog_Condition.Name = _file.Name;
                            postFTPTransportLog_Condition.FTPPath = PostOfficeFTPServer + "/" + full_PostOfficeOrderFTPPath;
                            postFTPTransportLog_Condition.NASPath = full_PostOfficeOrderNASPath;
                            postFTPTransportLog_Condition.Type = (int)PostFTPTransportType.Order;
                            await _postFTPTransportLog_DA.Insert(postFTPTransportLog_Condition);

                            //移動檔案到NASBAK
                            //如果沒有資料夾就建立
                            if (!Directory.Exists(PostOfficeOrderNASPathBak))
                            {
                                Directory.CreateDirectory(PostOfficeOrderNASPathBak);
                            }
                            File.Move(full_PostOfficeOrderNASPath, full_PostOfficeOrderNASPathBak, true);
                            OrderCount++;
                        }
                        catch (Exception e)
                        {
                            postFTPTransportLog_Condition.FailMessage = e.Message;

                        }

                    }
                }
                #endregion



                #region 下載結果檔
                //帳密
                var PostOfficeResultAccount = _configuration["PostOffice:Result:Account"];
                var PostOfficeResultPassWord = _configuration["PostOffice:Result:PassWord"];
                //NAS 路徑
                var PostOfficeResultNASPath = _configuration["PostOffice:Result:NASPath"];
                var PostOfficeResultNASPathBak = _configuration["PostOffice:Result:NASPathBak"];
                //FTP 路徑
                var PostOfficeResultFTPPath = _configuration["PostOffice:Result:FTPPath"];
                var PostOfficeResultFTPPathBak = _configuration["PostOffice:Result:FTPPathBak"];


                //取得LIST
                FtpUtility ResultClient = new FtpUtility(PostOfficeFTPServer + "/" + PostOfficeResultFTPPath, PostOfficeResultAccount, PostOfficeResultPassWord);
                List<string> Resultfiles = ResultClient.GetPostFileNameList().Where(x => x.LastIndexOf(".eod") > 1).ToList();

                if (Resultfiles != null && Resultfiles.Count() > 0)
                {

                    foreach (var _file in Resultfiles)
                    {
                        //紀錄DB
                        PostFTPTransportLog_Condition postFTPTransportLog_Condition = new PostFTPTransportLog_Condition();
                        try
                        {
                            //上傳檔案
                            /* Create Object Instance */
                            string full_PostOfficeResultFTPPath = Path.Combine(PostOfficeResultFTPPath, _file);
                            string full_PostOfficeResultFTPPathBak = PostOfficeResultFTPPathBak + "/" + _file;
                            string full_PostOfficeResultNASPath = Path.Combine(PostOfficeResultNASPath, _file);
                            string full_PostOfficeResultNASPathBak = Path.Combine(PostOfficeResultNASPathBak, _file);

                            ResultClient.DownloadPostFile(full_PostOfficeResultFTPPath, full_PostOfficeResultNASPath);

                            //寫入DB

                            postFTPTransportLog_Condition.Name = _file;
                            postFTPTransportLog_Condition.FTPPath = PostOfficeFTPServer + "/" + full_PostOfficeResultFTPPath;
                            postFTPTransportLog_Condition.NASPath = full_PostOfficeResultNASPath;
                            postFTPTransportLog_Condition.Type = (int)PostFTPTransportType.Result;
                            await _postFTPTransportLog_DA.Insert(postFTPTransportLog_Condition);


                            //移動到Backup
                            ResultClient.RenamePostFile(full_PostOfficeResultFTPPath, full_PostOfficeResultFTPPathBak);

                            ResultCount++;
                        }
                        catch (Exception e)
                        {
                            postFTPTransportLog_Condition.FailMessage = e.Message;

                        }

                    }
                }
                #endregion



                #region 下載異常檔
                //帳密
                var PostOfficeIrregularAccount = _configuration["PostOffice:Irregular:Account"];
                var PostOfficeIrregularPassWord = _configuration["PostOffice:Irregular:PassWord"];
                //NAS 路徑
                var PostOfficeIrregularNASPath = _configuration["PostOffice:Irregular:NASPath"];
                var PostOfficeIrregularNASPathBak = _configuration["PostOffice:Irregular:NASPathBak"];
                //FTP 路徑
                var PostOfficeIrregularFTPPath = _configuration["PostOffice:Irregular:FTPPath"];
                var PostOfficeIrregularFTPPathBak = _configuration["PostOffice:Irregular:FTPPathBak"];

                //取得LIST
                FtpUtility IrregularClient = new FtpUtility(PostOfficeFTPServer + "/" + PostOfficeIrregularFTPPath, PostOfficeIrregularAccount, PostOfficeIrregularPassWord);
                List<string> Irregularfiles = IrregularClient.GetPostFileNameList().Where(x => x.LastIndexOf(".eod") > 1).ToList();
                if (Irregularfiles != null && Irregularfiles.Count() > 0)
                {

                    foreach (var _file in Irregularfiles)
                    {
                        //紀錄DB
                        PostFTPTransportLog_Condition postFTPTransportLog_Condition = new PostFTPTransportLog_Condition();
                        try
                        {
                            //上傳檔案
                            /* Create Object Instance */
                            string full_PostOfficeIrregularFTPPath = Path.Combine(PostOfficeIrregularFTPPath, _file);
                            string full_PostOfficeIrregularFTPPathBak = PostOfficeIrregularFTPPathBak + "/" + _file;
                            string full_PostOfficeIrregularNASPath = Path.Combine(PostOfficeIrregularNASPath, _file);
                            string full_PostOfficeIrregularNASPathBak = Path.Combine(PostOfficeIrregularNASPathBak, _file);

                            IrregularClient.DownloadPostFile(full_PostOfficeIrregularFTPPath, full_PostOfficeIrregularNASPath);

                            //寫入DB
                            postFTPTransportLog_Condition.Name = _file;
                            postFTPTransportLog_Condition.FTPPath = PostOfficeFTPServer + "/" + full_PostOfficeIrregularFTPPath;
                            postFTPTransportLog_Condition.NASPath = full_PostOfficeIrregularNASPath;
                            postFTPTransportLog_Condition.Type = (int)PostFTPTransportType.Irregular;
                            await _postFTPTransportLog_DA.Insert(postFTPTransportLog_Condition);

                            //移動到Backup
                            IrregularClient.RenamePostFile(full_PostOfficeIrregularFTPPath, full_PostOfficeIrregularFTPPathBak);

                            IrregularCount++;
                        }
                        catch (Exception e)
                        {
                            postFTPTransportLog_Condition.FailMessage = e.Message;

                        }

                    }
                }
                #endregion


                #region 下載貨態檔
                //帳密
                var PostOfficeStatusAccount = _configuration["PostOffice:Status:Account"];
                var PostOfficeStatusPassWord = _configuration["PostOffice:Status:PassWord"];
                //NAS 路徑
                var PostOfficeStatusNASPath = _configuration["PostOffice:Status:NASPath"];
                var PostOfficeStatusNASPathBak = _configuration["PostOffice:Status:NASPathBak"];
                //FTP 路徑
                var PostOfficeStatusFTPPath = _configuration["PostOffice:Status:FTPPath"];
                var PostOfficeStatusFTPPathBak = _configuration["PostOffice:Status:FTPPathBak"];
                //取得LIST
                FtpUtility StatusClient = new FtpUtility(PostOfficeFTPServer + "/" + PostOfficeStatusFTPPath, PostOfficeStatusAccount, PostOfficeStatusPassWord);
                List<string> Statusfiles = StatusClient.GetPostFileNameList().Where(x => x.LastIndexOf(".eod") > 1).ToList();
                if (Statusfiles != null && Statusfiles.Count() > 0)
                {

                    foreach (var _file in Statusfiles)
                    {
                        //紀錄DB
                        PostFTPTransportLog_Condition postFTPTransportLog_Condition = new PostFTPTransportLog_Condition();
                        try
                        {
                            //上傳檔案
                            /* Create Object Instance */
                            string full_PostOfficeStatusFTPPath = Path.Combine(PostOfficeStatusFTPPath, _file);
                            string full_PostOfficeStatusFTPPathBak = PostOfficeStatusFTPPathBak + "/" + _file;
                            string full_PostOfficeStatusNASPath = Path.Combine(PostOfficeStatusNASPath, _file);
                            string full_PostOfficeStatusNASPathBak = Path.Combine(PostOfficeStatusNASPathBak, _file);

                            StatusClient.DownloadPostFile(full_PostOfficeStatusFTPPath, full_PostOfficeStatusNASPath);

                            //寫入DB
                            postFTPTransportLog_Condition.Name = _file;
                            postFTPTransportLog_Condition.FTPPath = PostOfficeFTPServer + "/" + full_PostOfficeStatusFTPPath;
                            postFTPTransportLog_Condition.NASPath = full_PostOfficeStatusNASPath;
                            postFTPTransportLog_Condition.Type = (int)PostFTPTransportType.Status;
                            await _postFTPTransportLog_DA.Insert(postFTPTransportLog_Condition);

                            //移動到Backup
                            StatusClient.RenamePostFile(full_PostOfficeStatusFTPPath, full_PostOfficeStatusFTPPathBak);

                            StatusCount++;
                        }
                        catch (Exception e)
                        {
                            postFTPTransportLog_Condition.FailMessage = e.Message;
                        }
                    }
                }
                #endregion

            }
            catch (Exception e)
            {

                throw;
            }
            transportPostFTPJob_Res.OrderCount = OrderCount;
            transportPostFTPJob_Res.IrregularCount = IrregularCount;
            transportPostFTPJob_Res.ResultCount = ResultCount;
            transportPostFTPJob_Res.StatusCount = StatusCount;

            return transportPostFTPJob_Res;
        }

        //資料整理,避免|影響格式&若過長截斷字元
        private string Trans(string O, int len)
        {
            string strConverted = O;
            strConverted = Tool.StrTrimer(strConverted).Replace("|", "");

            strConverted = strConverted.Length > len ? strConverted.Substring(0, len) : strConverted;



            return strConverted;
        }

        /// <summary>
        ///  上傳FTP 給郵局的出貨檔JOB
        /// </summary>
        /// <returns></returns>
        public async Task<UploadPostOrderJob_Res> UploadPostOrderJob()
        {
            UploadPostOrderJob_Res uploadPostOrderJob_Res = new UploadPostOrderJob_Res();
            int SuccessCount = 0;
            try
            {

                //抓取上次最後執行
                //過濾
                var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.UploadPostOrderJob)).FirstOrDefault();

                if (info == null)
                {
                    throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
                }

                //抓DB
                List<PostTransmission_Condition> list = await _postTransmission_DA.GetTransferPostInfo(info.StartId);
                if (list != null && list.Count() > 0)
                {
                    var lastId = list.Max(p => p.id_forPost);

                    var EodUploadPath = _configuration["PostOffice:Order:NASPath"];
                    string data_cdate = DateTime.Now.ToString("yyyyMMddHHmm");
                    var PostCode = _configuration["PostOffice:PostCode"];
                    StringBuilder sb = new StringBuilder();

                    foreach (var item in list.OrderBy(x => x.id_forPost))
                    {
                        try
                        {
                            PostTransmission_Condition postTransmission_Condition = new PostTransmission_Condition();
                            postTransmission_Condition.id_forPost = item.id_forPost;
                            postTransmission_Condition.PostId = Trans(item.PostId, 20);
                            postTransmission_Condition.check_number = Trans(item.check_number, 30);
                            postTransmission_Condition.post_code = PostCode;
                            postTransmission_Condition.is_collection_money = Trans(item.is_collection_money, 1);
                            postTransmission_Condition.collection_money = string.IsNullOrEmpty(item.collection_money) ? "0" : Trans(item.collection_money, 6);
                            postTransmission_Condition.receive_name = Trans(item.receive_name, 30);
                            postTransmission_Condition.receive_tel = Trans(Regex.Replace(Strings.StrConv(item.receive_tel, VbStrConv.Narrow, 0), "[^0-9]", ""), 20);
                            postTransmission_Condition.zip3 = Trans(item.zip3, 6);
                            postTransmission_Condition.receive_address = Trans(item.receive_address, 120);
                            postTransmission_Condition.upload_date = DateTime.Now.ToString("yyyyMMddHHmmss");
                            postTransmission_Condition.bag_no = Trans(item.bag_no, 30);
                            postTransmission_Condition.bag_weight = Trans(item.bag_weight, 6);
                            postTransmission_Condition.serial_no = Trans(item.serial_no, 3);
                            postTransmission_Condition.weight = Trans(item.weight, 6);
                            postTransmission_Condition.container_no = Trans(item.container_no, 6);

                            await _postTransmission_DA.Insert(postTransmission_Condition);

                            //郵局要求格式
                            string detail =
                           postTransmission_Condition.PostId + "|" +
                           postTransmission_Condition.check_number + "|" +
                           postTransmission_Condition.post_code + "|" +
                           postTransmission_Condition.is_collection_money + "|" +
                           postTransmission_Condition.collection_money + "|" +
                           postTransmission_Condition.receive_name + "|" +
                           postTransmission_Condition.receive_tel + "|" +
                           postTransmission_Condition.zip3 + "|" +
                           postTransmission_Condition.receive_address + "|" +
                           postTransmission_Condition.upload_date + "|" +
                           postTransmission_Condition.bag_no + "|" +
                           postTransmission_Condition.bag_weight + "|" +
                           postTransmission_Condition.serial_no + "|" +
                           postTransmission_Condition.weight + "|";



                            sb.AppendLine(detail);

                            SuccessCount++;

                        }
                        catch (Exception e)
                        {

                        }
                    }



                    File.WriteAllText(EodUploadPath + @"\ORDER_" + PostCode + "_" + data_cdate + ".eod", sb.ToString());

                    info.StartId = lastId;
                    await _ScheduleTask_DA.UpdateSetStartId(info);
                }



                uploadPostOrderJob_Res.SuccessCount = SuccessCount;
                return uploadPostOrderJob_Res;

            }
            catch (MyException e)
            {
                return uploadPostOrderJob_Res;
            }

        }

        /// <summary>
        /// 抓郵局的FTP到NAS
        /// </summary>
        /// <returns></returns>
        public async Task<DownloadPostStatusFTPToNASJob_Res> DownloadPostStatusFTPToNAS()
        {
            DownloadPostStatusFTPToNASJob_Res DownloadPostStatusFTPToNASJob_Res = new DownloadPostStatusFTPToNASJob_Res();

            //抓取上次最後執行
            //抓取FTP
            //寫入DB


            return DownloadPostStatusFTPToNASJob_Res;

        }
        /// <summary>
        /// 抓郵局結果檔
        /// </summary>
        /// <returns></returns>
        public async Task<GetPostResultNASToDBJob_Res> GetPostResultNASToDB()
        {
            GetPostResultNASToDBJob_Res getPostFTPEODFile = new GetPostResultNASToDBJob_Res();
            var PostEodDownloadNASPath = _configuration["PostOffice:Result:NASPath"];
            var PostEodDownloadNASPathMove = _configuration["PostOffice:Result:NASPathBak"];
            int SuccessCount = 0;
            try
            {
                //取得所有檔案
                DirectoryInfo dirs = new DirectoryInfo(PostEodDownloadNASPath);
                IEnumerable<FileInfo> files = dirs.GetFiles("*.eod");

                foreach (var _file in files)
                {
                    string FullPath = _file.FullName;
                    string Name = _file.Name;
                    string MovePath = Path.Combine(PostEodDownloadNASPathMove, DateTime.Now.ToString("yyyyMM"));

                    //如果沒有資料夾就建立
                    if (!Directory.Exists(MovePath))
                    {
                        Directory.CreateDirectory(MovePath);
                    }
                    string FullMovePath = Path.Combine(MovePath, Name);
                    try
                    {
                        using (var r = new StreamReader(FullPath, true))
                        {
                            if (r.Peek() >= 0)
                                r.Read();

                            Encoding encoding = r.CurrentEncoding;
                            //讀取
                            var Text = System.IO.File.ReadAllText(FullPath, encoding);
                            //存所有狀況
                            //   postEod_Condition.EodContent = Text;

                            string[] temp = Text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);


                            foreach (var item in temp)
                            {
                                string[] temp2 = item.Split(new char[] { '|' });

                                if (temp2.Length != 5)
                                {
                                    //格式不符
                                    break;
                                }
                                string PostId = temp2[0];
                                string CheckNumber = temp2[1];
                                string OrderFileDate = temp2[2];
                                string StatusCode = temp2[3];
                                string StatusZH = temp2[4];

                                PostResultEod_Condition postResultEod_Condition = new PostResultEod_Condition();
                                postResultEod_Condition.EodName = Name;
                                postResultEod_Condition.EodPath = FullMovePath;
                                postResultEod_Condition.PostId = PostId;
                                postResultEod_Condition.CheckNumber = CheckNumber;
                                postResultEod_Condition.OrderFileDate = OrderFileDate;
                                postResultEod_Condition.StatusCode = StatusCode;
                                postResultEod_Condition.StatusZH = StatusZH;
                                await _postResultEod_DA.Insert(postResultEod_Condition);

                                SuccessCount++;
                            }
                        }
                        //移動
                        File.Move(FullPath, FullMovePath, true);
                        SuccessCount++;
                    }
                    catch (Exception e)
                    {
                        //throw;
                    }

                }
                getPostFTPEODFile.count = SuccessCount;
            }
            catch (Exception)
            {
                throw;
            }
            return getPostFTPEODFile;

        }

        /// <summary>
        /// 抓郵局異常檔
        /// </summary>
        /// <returns></returns>
        public async Task<GetPostIrregularNASToDBJob_Res> GetPostIrregularNASToDB()
        {
            GetPostIrregularNASToDBJob_Res getPostFTPEODFile = new GetPostIrregularNASToDBJob_Res();
            var PostEodDownloadNASPath = _configuration["PostOffice:Irregular:NASPath"];
            var PostEodDownloadNASPathMove = _configuration["PostOffice:Irregular:NASPathBak"];
            int SuccessCount = 0;
            try
            {
                //取得所有檔案
                DirectoryInfo dirs = new DirectoryInfo(PostEodDownloadNASPath);
                IEnumerable<FileInfo> files = dirs.GetFiles("*.eod");

                foreach (var _file in files)
                {
                    string FullPath = _file.FullName;
                    string Name = _file.Name;
                    string MovePath = Path.Combine(PostEodDownloadNASPathMove, DateTime.Now.ToString("yyyyMM"));

                    //如果沒有資料夾就建立
                    if (!Directory.Exists(MovePath))
                    {
                        Directory.CreateDirectory(MovePath);
                    }
                    string FullMovePath = Path.Combine(MovePath, Name);
                    try
                    {
                        using (var r = new StreamReader(FullPath, true))
                        {
                            if (r.Peek() >= 0)
                                r.Read();

                            Encoding encoding = r.CurrentEncoding;
                            //讀取
                            var Text = System.IO.File.ReadAllText(FullPath, encoding);
                            //存所有狀況
                            //   postEod_Condition.EodContent = Text;

                            string[] temp = Text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);


                            foreach (var item in temp)
                            {

                                try
                                {


                                    string[] temp2 = item.Split(new char[] { '|' });

                                    if (temp2.Length != 16)
                                    {
                                        //格式不符
                                        break;
                                    }
                                    string PostId = temp2[0];
                                    string check_number = temp2[1];
                                    string post_code = temp2[2];
                                    string is_collection_money = temp2[3];
                                    string collection_money = temp2[4];
                                    string receive_name = temp2[5];
                                    string receive_tel = temp2[6];
                                    string zip3 = temp2[7];
                                    string receive_address = temp2[8];
                                    string upload_date = temp2[9];
                                    string bag_no = temp2[10];
                                    string bag_weight = temp2[11];
                                    string serial_no = temp2[12];
                                    string weight = temp2[13];
                                    string container_no = temp2[14];
                                    string StatusCode = temp2[15];
                                    PostIrregularEod_Condition postIrregularEod_Condition = new PostIrregularEod_Condition();
                                    postIrregularEod_Condition.EodName = Name;
                                    postIrregularEod_Condition.EodPath = MovePath;
                                    postIrregularEod_Condition.PostId = PostId;
                                    postIrregularEod_Condition.check_number = check_number;
                                    postIrregularEod_Condition.post_code = post_code;
                                    postIrregularEod_Condition.is_collection_money = is_collection_money;
                                    postIrregularEod_Condition.collection_money = collection_money;
                                    postIrregularEod_Condition.receive_name = receive_name;
                                    postIrregularEod_Condition.receive_tel = receive_tel;
                                    postIrregularEod_Condition.zip3 = zip3;
                                    postIrregularEod_Condition.receive_address = receive_address;
                                    postIrregularEod_Condition.upload_date = upload_date;
                                    postIrregularEod_Condition.bag_no = bag_no;
                                    postIrregularEod_Condition.bag_weight = bag_weight;
                                    postIrregularEod_Condition.serial_no = serial_no;
                                    postIrregularEod_Condition.weight = weight;
                                    postIrregularEod_Condition.container_no = container_no;
                                    postIrregularEod_Condition.StatusCode = StatusCode;

                                    await _postIrregularEod_DA.Insert(postIrregularEod_Condition);

                                    SuccessCount++;
                                }
                                catch (Exception e)
                                {

                                    // throw;
                                }
                            }


                        }

                        //移動
                        File.Move(FullPath, FullMovePath, true);
                        SuccessCount++;
                    }
                    catch (Exception e)
                    {
                        // throw;
                    }

                }
                getPostFTPEODFile.count = SuccessCount;
            }
            catch (Exception e)
            {
                throw;
            }
            return getPostFTPEODFile;

        }


        /// <summary>
        /// 抓郵局貨態 NAS 到 DB
        /// </summary>
        /// <returns></returns>
        public async Task<GetPostStatusNASToDBJob_Res> GetPostStatusNASToDB()
        {
            GetPostStatusNASToDBJob_Res getPostFTPEODFile = new GetPostStatusNASToDBJob_Res();

            var PostEodDownloadNASPath = _configuration["PostOffice:Status:NASPath"];
            var PostEodDownloadNASPathMove = _configuration["PostOffice:Status:NASPathBak"];
            int SuccessCount = 0;

            try
            {

                //取得所有檔案
                DirectoryInfo dirs = new DirectoryInfo(PostEodDownloadNASPath);
                IEnumerable<FileInfo> files = dirs.GetFiles("*.eod");

                foreach (var _file in files)
                {
                    PostEod_Condition postEod_Condition = new PostEod_Condition();
                    string FullPath = _file.FullName;
                    string Name = _file.Name;

                    postEod_Condition.FileCreateTime = _file.CreationTime;
                    postEod_Condition.EodName = Name;

                    try
                    {
                        using (var r = new StreamReader(FullPath, true))
                        {
                            if (r.Peek() >= 0)
                                r.Read();

                            Encoding encoding = r.CurrentEncoding;

                            //讀取
                            var Text = System.IO.File.ReadAllText(FullPath, encoding);
                            //存所有狀況
                            postEod_Condition.EodContent = Text;

                        }

                        string MovePath = Path.Combine(PostEodDownloadNASPathMove, DateTime.Now.ToString("yyyyMM"));

                        //如果沒有資料夾就建立
                        if (!Directory.Exists(MovePath))
                        {
                            Directory.CreateDirectory(MovePath);
                        }

                        string FullMovePath = Path.Combine(MovePath, Name);


                        postEod_Condition.EodPath = FullMovePath;

                        //移動
                        File.Move(FullPath, FullMovePath, true);
                        SuccessCount++;
                    }
                    catch (Exception e)
                    {
                        postEod_Condition.FailMessage = e.Message;
                        postEod_Condition.Fail = true;
                        postEod_Condition.FailCount = 1;

                        //throw;
                    }
                    await _postEod_DA.Insert(postEod_Condition);
                }

                getPostFTPEODFile.count = SuccessCount;

            }
            catch (Exception)
            {

                throw;
            }


            return getPostFTPEODFile;

        }


        /// <summary>
        /// 解析郵局狀態JOB
        /// </summary>
        /// <returns></returns>
        public async Task<ParsingPostStatusJob_Res> ParsingPostStatus()
        {
            ParsingPostStatusJob_Res getPostFTPEODFile = new ParsingPostStatusJob_Res();

            int SuccessCount = 0;
            int ErrorCount = 0;
            //抓取上次最後執行
            //過濾
            var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.ParsingPostStatusJob)).FirstOrDefault();

            if (info == null)
            {
                throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            }

            var GetInfoByCdate = await _postEod_DA.GetInfoById(info.StartId);

            if (GetInfoByCdate != null && GetInfoByCdate.Count() > 0)
            {
                var lastId = GetInfoByCdate.Max(p => p.id);

                foreach (var posteodinfo in GetInfoByCdate)
                {
                    //拆解內容
                    try
                    {
                        //分行
                        string[] temp = posteodinfo.EodContent.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (var item in temp)
                        {
                            try
                            {
                                string[] temp2 = item.Split(new char[] { '|' });

                                if (temp2.Length != 6)
                                {
                                    //格式不符
                                    ErrorCount++;
                                }
                                string PostId = temp2[0];
                                string CheckNumber = temp2[1];
                                string PostStation = temp2[2];
                                string StatusDate = temp2[3];//yyyyMMddHHmmss
                                string StatusCode = temp2[4];
                                string StatusZH = temp2[5];

                                //確認時間格式
                                if (!DateTime.TryParseExact(StatusDate, "yyyyMMddHHmmss", null, System.Globalization.DateTimeStyles.None, out DateTime _statusDate))
                                {
                                    //格式不符
                                    ErrorCount++;
                                }

                                PostEodParse_Condition postEodParse_Condition = new PostEodParse_Condition();
                                postEodParse_Condition.PostEodId = posteodinfo.id;
                                postEodParse_Condition.PostId = PostId;
                                postEodParse_Condition.CheckNumber = CheckNumber;
                                postEodParse_Condition.PostStation = PostStation;
                                postEodParse_Condition.StatusCode = StatusCode;
                                postEodParse_Condition.StatusDate = _statusDate;
                                postEodParse_Condition.StatusZH = StatusZH;
                                await _postEodParse_DA.Insert(postEodParse_Condition);
                                SuccessCount++;
                            }
                            catch (Exception e)
                            {
                                //失敗 跳過
                                Notification_Condition notification_Condition = new Notification_Condition();
                                notification_Condition.Type = 1;
                                notification_Condition.Recipient = "郵局EOD分析";
                                notification_Condition.Subject = posteodinfo.EodName + "資料有誤";
                                notification_Condition.Body = item + "\r\n" + e.Message;
                                notification_Condition.CreateUser = "郵局EOD分析";
                                await _Notification_DA.Insert(notification_Condition);
                            }

                        }
                    }
                    catch (Exception e)
                    {
                        //拆解失敗

                        Notification_Condition notification_Condition = new Notification_Condition();
                        notification_Condition.Type = 1;
                        notification_Condition.Recipient = "郵局EOD分析";
                        notification_Condition.Subject = posteodinfo.EodName + "資料有誤";
                        notification_Condition.Body = e.Message;
                        notification_Condition.CreateUser = "郵局EOD分析";
                        await _Notification_DA.Insert(notification_Condition);
                    }


                }
                info.StartId = lastId;
                await _ScheduleTask_DA.UpdateSetStartId(info);
            }
            getPostFTPEODFile.SuccessCount = SuccessCount;
            getPostFTPEODFile.ErrorCount = ErrorCount;

            //寫入DB
            return getPostFTPEODFile;

        }

        /// <summary>
        /// 分類郵局狀態JOB
        /// </summary>
        /// <returns></returns>
        public async Task<FilterPostStatusJob_Res> FilterPostStatus()
        {
            FilterPostStatusJob_Res filterPostStatusJob_Res = new FilterPostStatusJob_Res();

            int SuccessCount = 0;
            int ErrorCount = 0;
            int ExcludeCount = 0;
            //抓取上次最後執行
            //過濾
            var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.FilterPostStatusJob)).FirstOrDefault();

            if (info == null)
            {
                throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            }

            var allPostStatus = await _postStatus_DA.All();

            var GetInfoByCdate = await _postEodParse_DA.GetInfoById(info.StartId);

            if (GetInfoByCdate != null && GetInfoByCdate.Count() > 0)
            {
                var lastId = GetInfoByCdate.Max(p => p.id);

                foreach (var StatusInfo in GetInfoByCdate.OrderBy(x => x.StatusDate))
                {
                    PostEodParseError_Condition postEodParseError_Condition = StatusInfo;
                    try
                    {
                        //檢查 貨態
                        var status = allPostStatus.Where(x => x.StatusCode.Equals(StatusInfo.StatusCode)).FirstOrDefault();
                        if (status == null)
                        {
                            //查無此貨態
                            postEodParseError_Condition.ErrorMessage = ResponseCodeEnum.PostStateNotFound.GetString();
                            postEodParseError_Condition.ErrorStatus = (int)ResponseCodeEnum.PostStateNotFound;
                            await _postEodParseError_DA.Insert(postEodParseError_Condition);
                            ErrorCount++;
                            throw new MyException(ResponseCodeEnum.PostNotFound);
                        }

                        //檢查POST 跟 CHECKNUMBER
                        var Dinfo = (await _post_DA.GetInfoBycheck_numberAndPostId(StatusInfo.CheckNumber, StatusInfo.PostId)).FirstOrDefault();

                        if (Dinfo == null)
                        {
                            //查無此單
                            postEodParseError_Condition.ErrorMessage = ResponseCodeEnum.PostNotFound.GetString();
                            postEodParseError_Condition.ErrorStatus = (int)ResponseCodeEnum.PostNotFound;
                            await _postEodParseError_DA.Insert(postEodParseError_Condition);
                            ErrorCount++;
                            throw new MyException(ResponseCodeEnum.PostNotFound);
                        }
                        //檢查郵局是否有重複貨態
                        var SInfo = await _postEodParseFilter_DA.SearchInfo(StatusInfo);
                        if (SInfo != null)
                        {
                            //此貨態重複
                            postEodParseError_Condition.ErrorMessage = ResponseCodeEnum.PostStateRepeat.GetString();
                            postEodParseError_Condition.ErrorStatus = (int)ResponseCodeEnum.PostStateRepeat;
                            await _postEodParseError_DA.Insert(postEodParseError_Condition);
                            ErrorCount++;
                            throw new MyException(ResponseCodeEnum.PostStateRepeat);
                        }
                        //排除用不到的貨態
                        if (string.IsNullOrEmpty(status.ScanItem) && string.IsNullOrEmpty(status.ArriveOption))
                        {
                            await _postEodParseExclude_DA.Insert(StatusInfo);
                            ExcludeCount++;
                            throw new MyException(ResponseCodeEnum.PostStateNotFound);
                        }
                        //確認資料無誤 寫入DB
                        await _postEodParseFilter_DA.Insert(StatusInfo);
                    }
                    catch (MyException e)
                    {
                        if (!(e.ResponseCode == ResponseCodeEnum.PostNotFound ||
                            e.ResponseCode == ResponseCodeEnum.PostStateNotFound ||
                            e.ResponseCode == ResponseCodeEnum.CheckNumberIsNULL ||
                            e.ResponseCode == ResponseCodeEnum.PostStateRepeat))
                        {
                            //未知錯誤
                            postEodParseError_Condition.ErrorMessage = e.Message;
                            postEodParseError_Condition.ErrorStatus = (int)ResponseCodeEnum.PostError;
                            await _postEodParseError_DA.Insert(postEodParseError_Condition);
                            ErrorCount++;
                        }
                    }
                }
                info.StartId = lastId;
                await _ScheduleTask_DA.UpdateSetStartId(info);
            }
            filterPostStatusJob_Res.SuccessCount = SuccessCount;
            filterPostStatusJob_Res.ErrorCount = ErrorCount;
            filterPostStatusJob_Res.ExcludeCount = ExcludeCount;

            return filterPostStatusJob_Res;

        }

        /// <summary>
        /// 寫入郵局貨態
        /// </summary>
        /// <returns></returns>
        public async Task<InsertPostStatusJob_Res> InsertPostStatus()

        {
            InsertPostStatusJob_Res InsertPostStatusJob_Res = new InsertPostStatusJob_Res();
            int SuccessCount = 0;
            int ErrorCount = 0;
            int ExcludeCount = 0;
            //抓取上次最後執行
            //過濾
            var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.InsertPostStatusJob)).FirstOrDefault();

            if (info == null)
            {
                throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            }

            var allPostStatus = await _postStatus_DA.All();

            var GetInfoByCdate = await _postEodParseFilter_DA.GetInfoById(info.StartId);

            if (GetInfoByCdate != null && GetInfoByCdate.Count() > 0)
            {
                var lastId = GetInfoByCdate.Max(p => p.id);
                var PostDriverCode = _configuration["PostOffice:PostDriverCode"];

                foreach (var StatusInfo in GetInfoByCdate.OrderBy(x => x.StatusDate))
                {
                    PostEodParseError_Condition postEodParseError_Condition = StatusInfo;
                    string CheckNumber = StatusInfo.CheckNumber;
                    string PostId = StatusInfo.PostId;
                    try
                    {
                        //檢查 貨態
                        var status = allPostStatus.Where(x => x.StatusCode.Equals(StatusInfo.StatusCode)).FirstOrDefault();
                        if (status == null)
                        {
                            //查無此貨態
                            postEodParseError_Condition.ErrorMessage = ResponseCodeEnum.PostStateNotFound.GetString();
                            postEodParseError_Condition.ErrorStatus = (int)ResponseCodeEnum.PostStateNotFound;
                            await _postEodParseError_DA.Insert(postEodParseError_Condition);
                            ErrorCount++;
                            throw new MyException(ResponseCodeEnum.PostNotFound);
                        }

                        string ScanItem = string.IsNullOrEmpty(status.ScanItem) ? string.Empty : status.ScanItem;
                        string ArriveOption = string.IsNullOrEmpty(status.ArriveOption) ? string.Empty : status.ArriveOption;
                        //檢查POST 跟 CHECKNUMBER
                        var Dinfo = (await _post_DA.GetInfoBycheck_numberAndPostId(CheckNumber, PostId)).FirstOrDefault();

                        if (Dinfo == null)
                        {
                            //查無此單
                            postEodParseError_Condition.ErrorMessage = ResponseCodeEnum.PostNotFound.GetString();
                            postEodParseError_Condition.ErrorStatus = (int)ResponseCodeEnum.PostNotFound;
                            await _postEodParseError_DA.Insert(postEodParseError_Condition);
                            ErrorCount++;
                            throw new MyException(ResponseCodeEnum.PostNotFound);
                        }
                        ////檢查郵局是否有重複貨態
                        //var SInfo = await _postEodParseFilter_DA.SearchInfo(StatusInfo);
                        //if (SInfo != null )
                        //{
                        //    //此貨態重複
                        //    postEodParseError_Condition.ErrorMessage = ResponseCodeEnum.PostStateRepeat.GetString();
                        //    postEodParseError_Condition.ErrorStatus = (int)ResponseCodeEnum.PostStateRepeat;
                        //    await _postEodParseError_DA.Insert(postEodParseError_Condition);
                        //    ErrorCount++;
                        //    throw new MyException(ResponseCodeEnum.PostStateRepeat);
                        //}

                        //檢查重複貨態
                        var DeliveryScanLogInfo = await _ttDeliveryScanLog_DA.GetInfoByCheckNumber(CheckNumber);

                        //相同貨態
                        DeliveryScanLogInfo = DeliveryScanLogInfo.Where(x => !string.IsNullOrEmpty(x.scan_item));
                        DeliveryScanLogInfo = DeliveryScanLogInfo.Where(x => x.scan_item.Equals(ScanItem));
                        //又相同時間
                        DeliveryScanLogInfo = DeliveryScanLogInfo.Where(x => x.scan_date != null);
                        var DInfo = DeliveryScanLogInfo.Where(x => x.scan_date == StatusInfo.StatusDate).FirstOrDefault();

                        if (DInfo != null)
                        {
                            //此貨態重複
                            postEodParseError_Condition.ErrorMessage = ResponseCodeEnum.PostStateRepeat.GetString();
                            postEodParseError_Condition.ErrorStatus = (int)ResponseCodeEnum.PostStateRepeat;
                            await _postEodParseError_DA.Insert(postEodParseError_Condition);
                            ErrorCount++;
                            throw new MyException(ResponseCodeEnum.PostStateRepeat);
                        }
                        //檢查是否有此貨態
                        //確認此訂單
                        var d = (await _itcDeliveryRequests_DA.GetInfoByCheckNumber(CheckNumber)).FirstOrDefault();

                        if (d == null)
                        {
                            //查無此單
                            postEodParseError_Condition.ErrorMessage = ResponseCodeEnum.CheckNumberIsNULL.GetString();
                            postEodParseError_Condition.ErrorStatus = (int)ResponseCodeEnum.CheckNumberIsNULL;
                            await _postEodParseError_DA.Insert(postEodParseError_Condition);
                            ErrorCount++;
                            throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
                        }
                        //排除用不到的貨態
                        if (string.IsNullOrEmpty(ScanItem) && string.IsNullOrEmpty(ArriveOption))
                        {
                            await _postEodParseExclude_DA.Insert(StatusInfo);
                            ExcludeCount++;
                            throw new MyException(ResponseCodeEnum.PostStateNotFound);
                        }


                        //判斷是否代收貨款
                        bool IsCollection = false;

                        //代收貨款有他自己的路
                        if (d.collection_money.HasValue)
                        {
                            if (d.collection_money.Value > 0)
                            {
                                IsCollection = true;
                            }
                        }
                        if (IsCollection)
                        {
                            if (StatusInfo.StatusCode.Equals("Q100")) //入賬
                            {
                                //先確認有無配達(舊資料)
                                var ds = (await _ttDeliveryScanLog_DA.GetInfoByCheckNumber(CheckNumber));

                                //  ds = ds.Where(x => !string.IsNullOrEmpty(x.scan_item) && !string.IsNullOrEmpty(x.arrive_option));

                                //查詢配達是否為最後一筆

                                var dinfo = ds.OrderBy(x => x.scan_date).LastOrDefault();



                                if (dinfo != null)
                                {
                                    dinfo.scan_item = string.IsNullOrEmpty(dinfo.scan_item) ? string.Empty : dinfo.scan_item;
                                    dinfo.scan_item = string.IsNullOrEmpty(dinfo.arrive_option) ? string.Empty : dinfo.arrive_option;

                                    if (!(dinfo.scan_item.Equals("3") && dinfo.arrive_option.Equals("3")))
                                    {


                                        //確認配達數量
                                        int c = 0;
                                        //找所有郵局ID 是否都配達
                                        var postList = await _post_DA.GetInfoBycheck_number(CheckNumber);

                                        foreach (var _post in postList)
                                        {
                                            //找貨態
                                            var PostEodParseFilter_DA = await _postEodParseFilter_DA.GetInfoByPostNo(_post.PostId);

                                            var PostEodParseFilter = PostEodParseFilter_DA.Where(x => x.StatusCode == StatusInfo.StatusCode).FirstOrDefault();

                                            if (PostEodParseFilter != null)
                                            {
                                                c++;
                                            }

                                        }

                                        // 正常配達數量等於物品數量才會新增
                                        if (d.pieces.Value == c)
                                        {
                                            var LastScanItem = await _ttDeliveryScanLog_DA.GetttDeliveryScanLogLastScanItem(CheckNumber);

                                            if (LastScanItem.Count() > 0)
                                            {

                                            }
                                            else
                                            {
                                                //寫入DeliveryScanLog
                                                var scanLogInsert = new ttDeliveryScanLog_Condition()
                                                {
                                                    driver_code = PostDriverCode,
                                                    check_number = CheckNumber,
                                                    scan_date = StatusInfo.StatusDate,
                                                    scan_item = ScanItem,
                                                    arrive_option = ArriveOption,
                                                    cdate = DateTime.Now,
                                                    cuser = "fsejob"
                                                };
                                                await _ttDeliveryScanLog_DA.Insert(scanLogInsert);
                                            }
                                            SuccessCount++;
                                        }
                                    }
                                }

                            }
                            else if (!StatusInfo.StatusCode.Equals("Q100") && !StatusInfo.StatusCode.Equals("I400") && !StatusInfo.StatusCode.Equals("I300") && !StatusInfo.StatusCode.Equals("I500")) //如果是非配達 入帳狀態值接寫入
                            {
                                var LastScanItem = await _ttDeliveryScanLog_DA.GetttDeliveryScanLogLastScanItem(CheckNumber);

                                if (LastScanItem.Count() > 0)
                                {

                                }
                                else
                                {
                                    //寫入DeliveryScanLog
                                    var scanLogInsert = new ttDeliveryScanLog_Condition()
                                    {
                                        driver_code = PostDriverCode,
                                        check_number = CheckNumber,
                                        scan_date = StatusInfo.StatusDate,
                                        scan_item = ScanItem,
                                        arrive_option = ArriveOption,
                                        cdate = DateTime.Now,
                                        cuser = "fsejob"
                                    };
                                    await _ttDeliveryScanLog_DA.Insert(scanLogInsert);
                                }
                                SuccessCount++;
                            }
                        }
                        else //非代收貨款 
                        {

                            //有什麼就直接寫什麼 除非是複數配達 2022.3.22 
                            if (d.pieces.HasValue && d.pieces.Value > 1 && ArriveOption.Equals("3") && ScanItem.Equals("3"))
                            {
                                //確認配達數量
                                int c = 0;
                                //找所有郵局ID 是否都配達
                                var postList = await _post_DA.GetInfoBycheck_number(CheckNumber);
                                foreach (var _post in postList)
                                {
                                    //找貨態
                                    var PostEodParseFilter_DA = await _postEodParseFilter_DA.GetInfoByPostNo(_post.PostId);

                                    var PostEodParseFilter = PostEodParseFilter_DA.Where(x => x.StatusCode == StatusInfo.StatusCode).FirstOrDefault();

                                    if (PostEodParseFilter != null)
                                    {
                                        c++;
                                    }

                                    // 正常配達數量等於物品數量才會新增
                                    if (d.pieces.Value == c)
                                    {
                                        var LastScanItem = await _ttDeliveryScanLog_DA.GetttDeliveryScanLogLastScanItem(CheckNumber);

                                        if (LastScanItem.Count() > 0)
                                        {

                                        }
                                        else
                                        {
                                            //寫入DeliveryScanLog
                                            var scanLogInsert = new ttDeliveryScanLog_Condition()
                                            {
                                                driver_code = PostDriverCode,
                                                check_number = CheckNumber,
                                                scan_date = StatusInfo.StatusDate,
                                                scan_item = ScanItem,
                                                arrive_option = ArriveOption,
                                                cdate = DateTime.Now,
                                                cuser = "fsejob"
                                            };
                                            await _ttDeliveryScanLog_DA.Insert(scanLogInsert);
                                        }
                                        SuccessCount++;
                                    }
                                }
                            }
                            else
                            {
                                var LastScanItem = await _ttDeliveryScanLog_DA.GetttDeliveryScanLogLastScanItem(CheckNumber);

                                if (LastScanItem.Count() > 0)
                                {

                                }
                                else
                                {
                                    //寫入DeliveryScanLog
                                    var scanLogInsert = new ttDeliveryScanLog_Condition()
                                    {
                                        driver_code = PostDriverCode,
                                        check_number = CheckNumber,
                                        scan_date = StatusInfo.StatusDate,
                                        scan_item = ScanItem,
                                        arrive_option = ArriveOption,
                                        cdate = DateTime.Now,
                                        cuser = "fsejob"
                                    };
                                    await _ttDeliveryScanLog_DA.Insert(scanLogInsert);
                                }
                                SuccessCount++;

                            }

                        }

                    }
                    catch (MyException e)
                    {
                        if (!(e.ResponseCode == ResponseCodeEnum.PostNotFound ||
                            e.ResponseCode == ResponseCodeEnum.PostStateNotFound ||
                            e.ResponseCode == ResponseCodeEnum.CheckNumberIsNULL ||
                            e.ResponseCode == ResponseCodeEnum.PostStateRepeat))
                        {
                            //未知錯誤
                            postEodParseError_Condition.ErrorMessage = e.Message;
                            postEodParseError_Condition.ErrorStatus = (int)ResponseCodeEnum.PostError;
                            await _postEodParseError_DA.Insert(postEodParseError_Condition);
                            ErrorCount++;
                        }
                    }
                }
                info.StartId = lastId;
                await _ScheduleTask_DA.UpdateSetStartId(info);
            }
            InsertPostStatusJob_Res.SuccessCount = SuccessCount;
            InsertPostStatusJob_Res.ErrorCount = ErrorCount;
            InsertPostStatusJob_Res.ExcludeCount = ExcludeCount;

            return InsertPostStatusJob_Res;

        }


        /// <summary>
        /// 抓郵局清單
        /// </summary>
        /// <returns></returns>
        public async Task<List<GetPostList_Res>> PostList(GetPostList_Req req)
        {
            List<GetPostList_Res> getPostList_Res = new List<GetPostList_Res>();

            var list = await _post_DA.GetPostInfoByCdate(req.StartTime.Value, req.EndTime.Value);

            foreach (var item in list)
            {
                item.Collection_money = item.Collection_money == null ? "0" : item.Collection_money;
            }

            return list;
        }


        /// <summary>
        /// 抓郵局清單
        /// </summary>
        /// <returns></returns>
        public async Task<List<GetPostStatus_Res>> GetPostStatus(GetPostStatus_Req req)
        {
            List<GetPostStatus_Res> getPostStatus_Res = new List<GetPostStatus_Res>();

            try
            {

                var list = await GetPostStatusList(req.PostId);
                if (list != null && list.Count() > 0)
                {
                    foreach (var item in list)
                    {
                        GetPostStatus_Res status = new GetPostStatus_Res();

                        status.StatusCode = item.StatusCode;
                        status.StatusZH = item.StatusZH;
                        status.StationCode = item.StationCode;
                        status.StationName = item.StationName;
                        status.Date = item.Date;
                        getPostStatus_Res.Add(status);
                    }

                }

                return getPostStatus_Res;
            }
            catch (Exception)
            {

                return getPostStatus_Res;

            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PostId"></param>
        /// <returns></returns>
        public async Task<List<PostStatusInfo>> GetPostStatusList(string PostId)
        {
            List<PostStatusInfo> _postStatusInfo = new List<PostStatusInfo>();

            PSTTP_MailQuerySoapClient pSTTP_MailQuerySoapClient = new PSTTP_MailQuerySoapClient(EndpointConfiguration.PSTTP_MailQuerySoap);

            var aa = await pSTTP_MailQuerySoapClient.MailQuerySingleAsync(PostId);

            //抓XML
            XDocument xDoc = XDocument.Parse(aa.Body.MailQuerySingleResult);
            XNamespace xmlns = xDoc.Root.Name.NamespaceName;
            XElement root = new XElement(xmlns + "ITEM");
            var items = xDoc.Descendants(root.Name);

            if (items != null && items.Count() > 0)
            {
                foreach (var _item in items)
                {
                    try
                    {
                        PostStatusInfo getPost = new PostStatusInfo();

                        var EVCODE = _item.Element(new XElement(xmlns + "EVCODE").Name);
                        var STATUS = _item.Element(new XElement(xmlns + "STATUS").Name);
                        var BRHNO = _item.Element(new XElement(xmlns + "BRHNO").Name);
                        var BRHNC = _item.Element(new XElement(xmlns + "BRHNC").Name);
                        var DATIME = _item.Element(new XElement(xmlns + "DATIME").Name);

                        getPost.StatusCode = EVCODE == null ? string.Empty : EVCODE.Value.Trim();
                        getPost.StatusZH = STATUS == null ? string.Empty : STATUS.Value.Trim();
                        getPost.StationCode = BRHNO == null ? string.Empty : BRHNO.Value.Trim();
                        getPost.StationName = BRHNC == null ? string.Empty : BRHNC.Value.Trim();
                        getPost.Date = DATIME == null ? null : DateTime.ParseExact(DATIME.Value.Trim(), "yyyyMMddHHmmss", null);

                        _postStatusInfo.Add(getPost);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            return _postStatusInfo;
        }

        /// <summary>
        /// 更新郵局貨態
        /// </summary>
        /// <returns></returns>
        public async Task<UpdatePostStatus_Res> UpdatePostStatus(UpdatePostStatus_Req req)
        {

            UpdatePostStatus_Res updatePostStatus_Res = new UpdatePostStatus_Res();

            List<PostStatusInfo> _postStatusInfo = new List<PostStatusInfo>();


            int SuccessCount = 0;
            try
            {
                //確認郵局
                //新的邏輯
                var PostInfo = await _post_DA.GetInfoByPostId(req.PostId.Trim());

                if (PostInfo == null)
                {
                    throw new MyException(ResponseCodeEnum.PostNotFound);
                }

                string check_number = PostInfo.check_number;

                //確認貨號
                var d = (await _itcDeliveryRequests_DA.GetInfoByCheckNumber(check_number)).FirstOrDefault();

                if (d == null)
                {
                    //查無此單

                    throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
                }
                //判斷是否代收貨款
                bool IsCollection = false;

                //代收貨款有他自己的路
                if (d.collection_money.HasValue)
                {
                    if (d.collection_money.Value > 0)
                    {
                        IsCollection = true;
                    }
                }

                _postStatusInfo = await GetPostStatusList(req.PostId);

                //補貨態
                if (_postStatusInfo != null && _postStatusInfo.Count() > 0)
                {
                    var PostDriverCode = _configuration["PostOffice:PostDriverCode"];
                    //郵局所有貨態
                    var PostStates = (await _PostState_DA.All()).Where(x => !string.IsNullOrEmpty(x.ScanItem));

                    //先確認有無配達(舊資料)
                    var ds = (await _ttDeliveryScanLog_DA.GetInfoByCheckNumber(check_number));

                    //先排序
                    _postStatusInfo = _postStatusInfo.OrderBy(x => x.Date).ToList();

                    foreach (var _info in _postStatusInfo)
                    {
                        if (IsCollection)
                        {
                            var dinfo = ds.OrderBy(x => x.scan_date).LastOrDefault();
                            //最後一筆不是配達
                            if (dinfo == null || string.IsNullOrEmpty(dinfo.scan_item) || string.IsNullOrEmpty(dinfo.arrive_option) || !(dinfo.scan_item.Equals("3") && dinfo.arrive_option.Equals("3")))
                            {
                                if (_info.StatusCode.Equals("Q1"))//入帳寫死
                                {
                                    //確認配達數量
                                    int c = 0;
                                    //找所有郵局ID 是否都配達
                                    var postList = await _post_DA.GetInfoBycheck_number(check_number);

                                    foreach (var _post in postList)
                                    {
                                        //找貨態
                                        var _GetPostStatusList = await GetPostStatusList(_post.PostId);
                                        var PostXmlParseFilter = _GetPostStatusList.Where(x =>
                                  x.StatusCode.IndexOf("I1") > -1 ||
                                  x.StatusCode.IndexOf("I2") > -1 ||
                                  x.StatusCode.IndexOf("I3") > -1 ||
                                  x.StatusCode.IndexOf("I4") > -1 ||
                                  x.StatusCode.IndexOf("I5") > -1 ||
                                  x.StatusCode.IndexOf("I6") > -1).FirstOrDefault();
                                        if (PostXmlParseFilter != null)
                                        {
                                            c++;
                                        }
                                    }
                                    //正常配達數量等於物品數量才會新增
                                    if (d.pieces == c)
                                    {
                                        var LastScanItem = await _ttDeliveryScanLog_DA.GetttDeliveryScanLogLastScanItem(check_number);

                                        if (LastScanItem.Count() > 0)
                                        {

                                        }
                                        else
                                        {
                                            //寫入DeliveryScanLog
                                            var scanLogInsert = new ttDeliveryScanLog_Condition()
                                            {
                                                driver_code = PostDriverCode,
                                                check_number = check_number,
                                                scan_date = _info.Date,
                                                scan_item = "3",//入帳寫死
                                                arrive_option = "3",//入帳寫死
                                                cdate = DateTime.Now,
                                                cuser = "fsejob"
                                            };
                                            await _ttDeliveryScanLog_DA.Insert(scanLogInsert);
                                        }
                                        SuccessCount++;
                                    }

                                }
                                else if (!_info.StatusCode.Equals("Q1") &&
                                      !_info.StatusCode.Equals("I1") &&
                                      !_info.StatusCode.Equals("I2") &&
                                      !_info.StatusCode.Equals("I3") &&
                                      !_info.StatusCode.Equals("I4") &&
                                      !_info.StatusCode.Equals("I5") &&
                                      !_info.StatusCode.Equals("I6")
                                      )
                                {

                                    var ScanItemInfo = PostStates.Where(x => x.PostStateNo == _info.StatusCode).FirstOrDefault();

                                    if (ScanItemInfo != null)
                                    {

                                        //檢查是否與現在有相同的掃描時間跟scan_item
                                        var checkRepeat = ds.Where(x => x.scan_date == _info.Date && x.scan_item == ScanItemInfo.ScanItem);

                                        if (checkRepeat == null || checkRepeat.Count() == 0)
                                        {
                                            var LastScanItem = await _ttDeliveryScanLog_DA.GetttDeliveryScanLogLastScanItem(check_number);

                                            if (LastScanItem.Count() > 0)
                                            {

                                            }
                                            else
                                            {
                                                //寫入DeliveryScanLog
                                                var scanLogInsert = new ttDeliveryScanLog_Condition()
                                                {
                                                    driver_code = PostDriverCode,
                                                    check_number = check_number,
                                                    scan_date = _info.Date,
                                                    scan_item = ScanItemInfo.ScanItem,
                                                    arrive_option = ScanItemInfo.ArriveOption,
                                                    cdate = DateTime.Now,
                                                    cuser = "fsejob"
                                                };
                                                await _ttDeliveryScanLog_DA.Insert(scanLogInsert);
                                            }
                                            SuccessCount++;
                                        }
                                    }
                                }
                            }

                        }
                        else
                        {
                            var ScanItemInfo = PostStates.Where(x => x.PostStateNo == _info.StatusCode).FirstOrDefault();

                            //需要的貨態
                            if (ScanItemInfo != null)
                            {
                                if (string.IsNullOrEmpty(ScanItemInfo.ArriveOption))
                                {
                                    ScanItemInfo.ArriveOption = string.Empty;
                                }
                                if (string.IsNullOrEmpty(ScanItemInfo.ScanItem))
                                {
                                    ScanItemInfo.ScanItem = string.Empty;
                                }

                                //檢查是否與現在有相同的掃描時間跟scan_item
                                var checkRepeat = ds.Where(x => x.scan_date == _info.Date && x.scan_item == ScanItemInfo.ScanItem);

                                if (checkRepeat == null || checkRepeat.Count() == 0)
                                {
                                    if (d.pieces.HasValue && d.pieces.Value > 1 && ScanItemInfo.ArriveOption.Equals("3") && ScanItemInfo.ScanItem.Equals("3"))
                                    {
                                        //確認配達數量
                                        int c = 0;
                                        //找所有郵局ID 是否都配達
                                        var postList = await _post_DA.GetInfoBycheck_number(check_number);

                                        foreach (var _post in postList)
                                        {
                                            //找貨態
                                            var _GetPostStatusList = await GetPostStatusList(req.PostId);
                                            var PostXmlParseFilter = _GetPostStatusList.Where(x => x.StationCode == _info.StatusCode).FirstOrDefault();
                                            if (PostXmlParseFilter != null)
                                            {
                                                c++;
                                            }
                                        }
                                        //正常配達數量等於物品數量才會新增
                                        if (d.pieces == c)
                                        {
                                            var LastScanItem = await _ttDeliveryScanLog_DA.GetttDeliveryScanLogLastScanItem(d.check_number);

                                            if (LastScanItem.Count() > 0)
                                            {

                                            }
                                            else
                                            {
                                                //寫入DeliveryScanLog
                                                var scanLogInsert = new ttDeliveryScanLog_Condition()
                                                {
                                                    driver_code = PostDriverCode,
                                                    check_number = check_number,
                                                    scan_date = _info.Date,
                                                    scan_item = ScanItemInfo.ScanItem,
                                                    arrive_option = ScanItemInfo.ArriveOption,
                                                    cdate = DateTime.Now,
                                                    cuser = "fsejob"
                                                };
                                                await _ttDeliveryScanLog_DA.Insert(scanLogInsert);
                                            }
                                            SuccessCount++;
                                        }
                                    }
                                    else
                                    {
                                        var LastScanItem = await _ttDeliveryScanLog_DA.GetttDeliveryScanLogLastScanItem(check_number);

                                        if (LastScanItem.Count() > 0)
                                        {

                                        }
                                        else
                                        {
                                            //寫入DeliveryScanLog
                                            var scanLogInsert = new ttDeliveryScanLog_Condition()
                                            {
                                                driver_code = PostDriverCode,
                                                check_number = check_number,
                                                scan_date = _info.Date,
                                                scan_item = ScanItemInfo.ScanItem,
                                                arrive_option = ScanItemInfo.ArriveOption,
                                                cdate = DateTime.Now,
                                                cuser = "fsejob"
                                            };
                                            await _ttDeliveryScanLog_DA.Insert(scanLogInsert);
                                        }
                                        SuccessCount++;
                                    }
                                }
                            }
                        }
                    }
                }
                updatePostStatus_Res.SuccessCount = SuccessCount;
                return updatePostStatus_Res;
            }
            catch (Exception)
            {

                return updatePostStatus_Res;

            }

        }

        /// <summary>
        /// 抓郵局的FTP
        /// </summary>
        /// <returns></returns>
        public async Task<UpdatePostStateToDeliveryScanLogJob_Res> UpdatePostStateToDeliveryScanLog()
        {
            UpdatePostStateToDeliveryScanLogJob_Res updatePostStateToDeliveryScanLogJob = new UpdatePostStateToDeliveryScanLogJob_Res();

            int SuccessCount = 0;
            int ErrorCount = 0;

            try
            {
                //過濾
                var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.UpdatePostStateToDeliveryScanLogJob)).FirstOrDefault();

                if (info == null)
                {
                    throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
                }

                var GetInfoById = await _PostXmlParseFilter_DA.GetInfoById(info.StartId);

                if (GetInfoById != null && GetInfoById.Count() > 0)
                {
                    var PostDriverCode = _configuration["PostOffice:PostDriverCode"];

                    //找尋所有貨態 
                    var PostStates = (await _PostState_DA.All()).Where(x => !string.IsNullOrEmpty(x.ScanItem));

                    var lastId = GetInfoById.Max(p => p.id);

                    foreach (var _info in GetInfoById)
                    {
                        PostXmlParseError_Condition postXmlParseError_Condition = _info;
                        PostXmlParseExclude_Condition postXmlParseExclude_Condition = _info;
                        try
                        {
                            string check_number = string.Empty;
                            //舊的邏輯
                            var Post_request = await _Post_request_DA.GetInfoByPostid(_info.PostNo.Trim());
                            //新的邏輯
                            var PostInfo = await _post_DA.GetInfoByPostId(_info.PostNo.Trim());

                            if (Post_request == null && PostInfo == null)
                            {
                                //查無此單
                                postXmlParseError_Condition.ErrMsg = ResponseCodeEnum.PostNotFound.GetString();
                                postXmlParseError_Condition.Status = (int)ResponseCodeEnum.PostNotFound;
                                await _PostXmlParseError_DA.Insert(postXmlParseError_Condition);
                                ErrorCount++;
                                throw new MyException(ResponseCodeEnum.PostNotFound);
                            }


                            if (Post_request == null)
                            {
                                check_number = PostInfo.check_number;
                            }
                            if (PostInfo == null)
                            {
                                check_number = Post_request.jf_check_number;
                            }

                            //確認此訂單
                            var d = (await _itcDeliveryRequests_DA.GetInfoByCheckNumber(check_number)).FirstOrDefault();

                            if (d == null)
                            {
                                //查無此單
                                postXmlParseError_Condition.ErrMsg = ResponseCodeEnum.CheckNumberIsNULL.GetString();
                                postXmlParseError_Condition.Status = (int)ResponseCodeEnum.CheckNumberIsNULL;
                                await _PostXmlParseError_DA.Insert(postXmlParseError_Condition);
                                ErrorCount++;
                                throw new MyException(ResponseCodeEnum.CheckNumberIsNULL);
                            }


                            var ScanItemInfo = PostStates.Where(x => x.PostStateNo == _info.PostStateNo).FirstOrDefault();

                            if (ScanItemInfo == null)
                            {
                                //查無此貨態
                                await _PostXmlParseExclude_DA.Insert(postXmlParseExclude_Condition);
                                ErrorCount++;
                                throw new MyException(ResponseCodeEnum.PostStateNotFound);
                            }

                            if (string.IsNullOrEmpty(ScanItemInfo.ArriveOption))
                            {
                                ScanItemInfo.ArriveOption = string.Empty;
                            }
                            if (string.IsNullOrEmpty(ScanItemInfo.ScanItem))
                            {
                                ScanItemInfo.ScanItem = string.Empty;
                            }
                            //判斷是否代收貨款
                            bool IsCollection = false;

                            //代收貨款有他自己的路
                            if (d.collection_money.HasValue)
                            {
                                if (d.collection_money.Value > 0)
                                {
                                    IsCollection = true;
                                }
                            }
                            //此段寫死 代收貨款需要入帳才配達
                            if (IsCollection)
                            {
                                if (_info.PostNo.Equals("Q100")) //入賬
                                {
                                    //先確認有無配達(舊資料)
                                    var ds = (await _ttDeliveryScanLog_DA.GetInfoByCheckNumber(check_number));

                                    ds = ds.Where(x => !string.IsNullOrEmpty(x.scan_item) && !string.IsNullOrEmpty(x.arrive_option));

                                    var dinfo = ds.Where(x => x.scan_item.Equals("3") && x.arrive_option.Equals("3")).FirstOrDefault();

                                    if (dinfo == null)
                                    {

                                        //確認配達數量
                                        int c = 0;
                                        //找所有郵局ID 是否都配達

                                        var postList = await _post_DA.GetInfoBycheck_number(check_number);

                                        foreach (var _post in postList)
                                        {
                                            //找貨態
                                            var PostXmlParseFilters = await _PostXmlParseFilter_DA.GetInfoByPostNo(_post.PostId);
                                            //寫死所有配達
                                            var PostXmlParseFilter = PostXmlParseFilters.Where(x => x.PostStateNo.Equals("I400") || x.PostStateNo.Equals("I300")).FirstOrDefault();
                                            if (PostXmlParseFilter != null)
                                            {
                                                c++;
                                            }
                                        }
                                        //正常配達數量等於物品數量才會新增
                                        if (d.pieces == c)
                                        {
                                            var LastScanItem = await _ttDeliveryScanLog_DA.GetttDeliveryScanLogLastScanItem(check_number);

                                            if (LastScanItem.Count() > 0)
                                            {

                                            }
                                            else
                                            {
                                                //寫入DeliveryScanLog
                                                var scanLogInsert = new ttDeliveryScanLog_Condition()
                                                {
                                                    driver_code = PostDriverCode,
                                                    check_number = check_number,
                                                    scan_date = _info.ProcessDateTime,
                                                    scan_item = ScanItemInfo.ScanItem,
                                                    arrive_option = ScanItemInfo.ArriveOption,
                                                    cdate = DateTime.Now,
                                                    cuser = "fsejob"
                                                };
                                                await _ttDeliveryScanLog_DA.Insert(scanLogInsert);
                                            }
                                            SuccessCount++;
                                        }
                                    }
                                }
                                else if (!_info.PostNo.Equals("Q100") && !_info.PostNo.Equals("I400") && !_info.PostNo.Equals("I300"))//如果是非配達 入帳狀態值接寫入
                                {
                                    var LastScanItem = await _ttDeliveryScanLog_DA.GetttDeliveryScanLogLastScanItem(check_number);

                                    if (LastScanItem.Count() > 0)
                                    {

                                    }
                                    else
                                    {
                                        //寫入DeliveryScanLog
                                        var scanLogInsert = new ttDeliveryScanLog_Condition()
                                        {
                                            driver_code = PostDriverCode,
                                            check_number = check_number,
                                            scan_date = _info.ProcessDateTime,
                                            scan_item = ScanItemInfo.ScanItem,
                                            arrive_option = ScanItemInfo.ArriveOption,
                                            cdate = DateTime.Now,
                                            cuser = "fsejob"
                                        };
                                        await _ttDeliveryScanLog_DA.Insert(scanLogInsert);
                                    }
                                    SuccessCount++;
                                }
                            }
                            else //非代收貨款 
                            {
                                //如果正常配達
                                if (ScanItemInfo.ArriveOption.Equals("3") && ScanItemInfo.ScanItem.Equals("3"))
                                {
                                    //多件處理方式
                                    if (d.pieces > 1)
                                    {
                                        //先確認有無配達(舊資料)
                                        var ds = (await _ttDeliveryScanLog_DA.GetInfoByCheckNumber(check_number));

                                        ds = ds.Where(x => !string.IsNullOrEmpty(x.scan_item) && !string.IsNullOrEmpty(x.arrive_option));

                                        var dinfo = ds.Where(x => x.scan_item.Equals("3") && x.arrive_option.Equals("3")).FirstOrDefault();

                                        if (dinfo == null)
                                        {
                                            //確認配達數量
                                            int c = 0;
                                            //找所有郵局ID 是否都配達

                                            var postList = await _post_DA.GetInfoBycheck_number(check_number);

                                            foreach (var _post in postList)
                                            {
                                                //找貨態
                                                var PostXmlParseFilters = await _PostXmlParseFilter_DA.GetInfoByPostNo(_post.PostId);

                                                var PostXmlParseFilter = PostXmlParseFilters.Where(x => x.PostStateNo == _info.PostStateNo).FirstOrDefault();

                                                if (PostXmlParseFilter != null)
                                                {
                                                    c++;
                                                }

                                            }
                                            //正常配達數量等於物品數量才會新增
                                            if (d.pieces == c)
                                            {
                                                var LastScanItem = await _ttDeliveryScanLog_DA.GetttDeliveryScanLogLastScanItem(d.check_number);

                                                if (LastScanItem.Count() > 0)
                                                {

                                                }
                                                else
                                                {
                                                    //寫入DeliveryScanLog
                                                    var scanLogInsert = new ttDeliveryScanLog_Condition()
                                                    {
                                                        driver_code = PostDriverCode,
                                                        check_number = check_number,
                                                        scan_date = _info.ProcessDateTime,
                                                        scan_item = ScanItemInfo.ScanItem,
                                                        arrive_option = ScanItemInfo.ArriveOption,
                                                        cdate = DateTime.Now,
                                                        cuser = "fsejob"
                                                    };
                                                    await _ttDeliveryScanLog_DA.Insert(scanLogInsert);
                                                }
                                                SuccessCount++;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var LastScanItem = await _ttDeliveryScanLog_DA.GetttDeliveryScanLogLastScanItem(d.check_number);

                                        if (LastScanItem.Count() > 0)
                                        {

                                        }
                                        else
                                        {
                                            //寫入DeliveryScanLog
                                            var scanLogInsert = new ttDeliveryScanLog_Condition()
                                            {
                                                driver_code = PostDriverCode,
                                                check_number = check_number,
                                                scan_date = _info.ProcessDateTime,
                                                scan_item = ScanItemInfo.ScanItem,
                                                arrive_option = ScanItemInfo.ArriveOption,
                                                cdate = DateTime.Now,
                                                cuser = "fsejob"
                                            };
                                            await _ttDeliveryScanLog_DA.Insert(scanLogInsert);
                                        }
                                        SuccessCount++;
                                    }

                                }
                                else
                                {
                                    var LastScanItem = await _ttDeliveryScanLog_DA.GetttDeliveryScanLogLastScanItem(d.check_number);

                                    if (LastScanItem.Count() > 0)
                                    {

                                    }
                                    else
                                    {
                                        //寫入DeliveryScanLog
                                        var scanLogInsert = new ttDeliveryScanLog_Condition()
                                        {
                                            driver_code = PostDriverCode,
                                            check_number = check_number,
                                            scan_date = _info.ProcessDateTime,
                                            scan_item = ScanItemInfo.ScanItem,
                                            arrive_option = ScanItemInfo.ArriveOption,
                                            cdate = DateTime.Now,
                                            cuser = "fsejob"
                                        };
                                        await _ttDeliveryScanLog_DA.Insert(scanLogInsert);
                                    }
                                    SuccessCount++;

                                }

                            }



                        }
                        catch (MyException e)
                        {

                            if (!(e.ResponseCode == ResponseCodeEnum.PostNotFound || e.ResponseCode == ResponseCodeEnum.PostStateNotFound || e.ResponseCode == ResponseCodeEnum.CheckNumberIsNULL))
                            {
                                //未知錯誤
                                postXmlParseError_Condition.ErrMsg = e.Message;
                                postXmlParseError_Condition.Status = (int)ResponseCodeEnum.PostError;
                                await _PostXmlParseError_DA.Insert(postXmlParseError_Condition);
                                ErrorCount++;
                            }

                        }
                    }
                    info.StartId = lastId;
                    await _ScheduleTask_DA.UpdateSetStartId(info);
                }

                updatePostStateToDeliveryScanLogJob.SuccessCount = SuccessCount;
                updatePostStateToDeliveryScanLogJob.ErrorCount = ErrorCount;
            }
            catch (Exception e)
            {
                throw;
            }


            return updatePostStateToDeliveryScanLogJob;
        }
        /// <summary>
        /// 抓郵局的FTP
        /// </summary>
        /// <returns></returns>
        public async Task<FilterPostStateJob_Res> FilterPostState()
        {
            FilterPostStateJob_Res filterPostStateJob_Res = new FilterPostStateJob_Res();

            int SuccessCount = 0;
            int ErrorCount = 0;
            int ExcludeCount = 0;
            try
            {
                //過濾
                var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.FilterPostStateJob)).FirstOrDefault();

                if (info == null)
                {
                    throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
                }

                var GetInfoById = await _PostXmlParse_DA.GetInfoById(info.StartId);

                if (GetInfoById != null && GetInfoById.Count() > 0)
                {
                    var lastId = GetInfoById.Max(p => p.id);

                    //找尋所有貨態 
                    var PostStates = (await _PostState_DA.All()).Where(x => !string.IsNullOrEmpty(x.ScanItem));

                    foreach (var _info in GetInfoById)
                    {

                        PostXmlParseError_Condition postXmlParseError_Condition = new PostXmlParseError_Condition();
                        PostXmlParseExclude_Condition postXmlParseExclude_Condition = new PostXmlParseExclude_Condition();
                        PostXmlParseFilter_Condition postXmlParseFilter_Condition = new PostXmlParseFilter_Condition();

                        postXmlParseError_Condition = _info;
                        postXmlParseExclude_Condition = _info;
                        postXmlParseFilter_Condition = _info;


                        try
                        {
                            string check_number = string.Empty;

                            //舊的邏輯
                            var Post_request = await _Post_request_DA.GetInfoByPostid(_info.PostNo.Trim());
                            //新的邏輯
                            var PostInfo = await _post_DA.GetInfoByPostId(_info.PostNo.Trim());

                            if (Post_request == null && PostInfo == null)
                            {
                                //查無此單
                                postXmlParseError_Condition.ErrMsg = ResponseCodeEnum.PostNotFound.GetString();
                                postXmlParseError_Condition.Status = (int)ResponseCodeEnum.PostNotFound;
                                await _PostXmlParseError_DA.Insert(postXmlParseError_Condition);
                                ErrorCount++;
                                throw new MyException(ResponseCodeEnum.PostNotFound);
                            }

                            var ScanItemInfo = PostStates.Where(x => x.PostStateNo == _info.PostStateNo).FirstOrDefault();

                            if (ScanItemInfo == null)
                            {
                                //查無此貨態
                                await _PostXmlParseExclude_DA.Insert(postXmlParseExclude_Condition);
                                ExcludeCount++;
                                throw new MyException(ResponseCodeEnum.PostStateNotFound);
                            }


                            var postXmlParseFilterInfo = await _PostXmlParseFilter_DA.SearchInfo(postXmlParseFilter_Condition);

                            if (postXmlParseFilterInfo != null)
                            {
                                //重複
                                postXmlParseError_Condition.ErrMsg = ResponseCodeEnum.PostStateRepeat.GetString();
                                postXmlParseError_Condition.Status = (int)ResponseCodeEnum.PostStateRepeat;
                                await _PostXmlParseError_DA.Insert(postXmlParseError_Condition);
                                ErrorCount++;
                                throw new MyException(ResponseCodeEnum.PostStateRepeat);
                            }


                            await _PostXmlParseFilter_DA.Insert(postXmlParseFilter_Condition);
                            SuccessCount++;

                        }
                        catch (MyException e)
                        {

                            if (!(e.ResponseCode == ResponseCodeEnum.PostNotFound ||
                                e.ResponseCode == ResponseCodeEnum.PostStateNotFound ||
                                e.ResponseCode == ResponseCodeEnum.PostStateRepeat
                                ))
                            {
                                //未知錯誤
                                postXmlParseError_Condition.ErrMsg = e.Message;
                                postXmlParseError_Condition.Status = (int)ResponseCodeEnum.PostError;
                                await _PostXmlParseError_DA.Insert(postXmlParseError_Condition);
                                ErrorCount++;
                            }


                        }
                    }

                    info.StartId = lastId;
                    await _ScheduleTask_DA.UpdateSetStartId(info);
                }
                filterPostStateJob_Res.SuccessCount = SuccessCount;
                filterPostStateJob_Res.ExcludeCount = ExcludeCount;
                filterPostStateJob_Res.ErrorCount = ErrorCount;

            }
            catch (Exception e)
            {
                throw;
            }



            return filterPostStateJob_Res;
        }

        /// <summary>
        /// 抓郵局的FTP
        /// </summary>
        /// <returns></returns>
        public async Task<ParsingPostXmlJob_Res> ParsingPostXml()
        {
            ParsingPostXmlJob_Res parsingPostXmlJob = new ParsingPostXmlJob_Res();

            int SuccessCount = 0;
            int ErrorCount = 0;
            //過濾
            var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.ParsingPostXmlJob)).FirstOrDefault();

            if (info == null)
            {
                throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            }

            var GetInfoByCdate = await _PostXml_DA.GetInfoById(info.StartId);

            if (GetInfoByCdate != null && GetInfoByCdate.Count() > 0)
            {
                var lastId = GetInfoByCdate.Max(p => p.id);

                foreach (var post in GetInfoByCdate)
                {
                    try
                    {
                        //解析XML
                        try
                        {
                            //抓XML
                            XDocument xDoc = XDocument.Parse(post.XmlContent);
                            //只需要Detail
                            var Detail = xDoc.Descendants("Detail").First();
                            //所有Row 建立 LIST
                            var Rows = Detail.Elements("Row");

                            foreach (var _Rows in Rows)
                            {
                                //檢查
                                if (_Rows.Element("DeliverDate") == null)
                                {
                                    throw new MyException(ResponseCodeEnum.PostXmlParsingError);
                                }
                                if (_Rows.Element("LamApNo") == null)
                                {
                                    throw new MyException(ResponseCodeEnum.PostXmlParsingError);
                                }
                                if (_Rows.Element("PostNo") == null)
                                {
                                    throw new MyException(ResponseCodeEnum.PostXmlParsingError);
                                }
                                if (_Rows.Element("PostStateNo") == null)
                                {
                                    throw new MyException(ResponseCodeEnum.PostXmlParsingError);
                                }
                                if (_Rows.Element("PostStateZH") == null)
                                {
                                    throw new MyException(ResponseCodeEnum.PostXmlParsingError);
                                }
                                if (_Rows.Element("ProcessDateTime") == null)
                                {
                                    throw new MyException(ResponseCodeEnum.PostXmlParsingError);
                                }

                                var PostNo = _Rows.Element("PostNo").Value;
                                var PostStateNo = _Rows.Element("PostStateNo").Value;
                                var PostStateZH = _Rows.Element("PostStateZH").Value;
                                var ProcessDateTime = _Rows.Element("ProcessDateTime").Value;//YYYYMMDDHHMMSS

                                PostXmlParse_Condition postXmlParse_Condition = new PostXmlParse_Condition();

                                postXmlParse_Condition.PostNo = PostNo;
                                postXmlParse_Condition.PostStateNo = PostStateNo;
                                postXmlParse_Condition.PostStateZH = PostStateZH;

                                CultureInfo provider = CultureInfo.InvariantCulture;

                                postXmlParse_Condition.ProcessDateTime = DateTime.ParseExact(ProcessDateTime, "yyyyMMddHHmmss", provider);
                                postXmlParse_Condition.PostNo = PostNo;

                                postXmlParse_Condition.PostXml_id = post.id;

                                await _PostXmlParse_DA.Insert(postXmlParse_Condition);
                                SuccessCount++;

                            }

                        }
                        catch (MyException e)
                        {
                            ErrorCount++;
                            // throw;
                        }

                        parsingPostXmlJob.SuccessCount = SuccessCount;
                        parsingPostXmlJob.ErrorCount = ErrorCount;
                    }
                    catch (Exception e)
                    {
                        throw;
                    }
                }
                info.StartId = lastId;
                await _ScheduleTask_DA.UpdateSetStartId(info);
            }

            return parsingPostXmlJob;
        }

        /// <summary>
        /// 抓郵局的FTP 有誤調整
        /// </summary>
        /// <returns></returns>
        public async Task<GetErrorPostFTPXMLFileJob_Res> GetErrorPostFTPXMLFile()
        {
            GetErrorPostFTPXMLFileJob_Res getErrorPostFTPXMLFile = new GetErrorPostFTPXMLFileJob_Res();
            getErrorPostFTPXMLFile.count = 0;

            var PostXmlDownloadPath = _configuration["PostOffice:PostXmlDownloadPath"];
            var Host = _configuration["PostOffice:Host"];
            var UserName = _configuration["PostOffice:UserName"];
            var Password = _configuration["PostOffice:Password"];
            var PostErrorLogPath = _configuration["PostOffice:PostErrorLogPath"];
            string path = Path.Combine(PostErrorLogPath, DateTime.Now.ToString("yyyyMMdd"));
            try
            {

                //過濾
                var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.GetErrorPostFTPXMLFileJob)).FirstOrDefault();

                if (info == null)
                {
                    throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
                }

                //取得錯誤解析的XML
                var Files = (await _PostXml_DA.GetErrorListById(info.StartId)).Where(x => x.Number < 5);
                /* Create Object Instance */
                FtpUtility ftpClient = new FtpUtility(Host, UserName, Password);


                if (Files != null && Files.Count() > 0)
                {

                    getErrorPostFTPXMLFile.count = Files.Count();

                    var lastId = Files.Max(p => p.id);

                    //下載
                    foreach (var _File in Files)
                    {
                        PostXml_Condition postXml_Condition = _File;
                        try
                        {
                            string timepath = _File.FileCreateTime.HasValue ? _File.FileCreateTime.Value.ToString("yyyyMMdd") : DateTime.Now.ToString("yyyyMMdd");
                            string filepath = Path.Combine(PostXmlDownloadPath, timepath);
                            //建立資料夾
                            if (!Directory.Exists(filepath))
                            {
                                Directory.CreateDirectory(filepath);
                            }
                            //完整檔案路徑
                            string fullfilepath = Path.Combine(filepath, _File.XmlName);
                            postXml_Condition.XmlName = _File.XmlName;
                            postXml_Condition.XmlPath = fullfilepath;
                            try
                            {
                                /* Download a File */
                                ftpClient.download(_File.XmlName, fullfilepath);
                                string readText = File.ReadAllText(fullfilepath);
                                postXml_Condition.XmlContent = File.ReadAllText(fullfilepath);
                                postXml_Condition.Fail = false;
                                postXml_Condition.Number++;

                                if (!File.Exists(fullfilepath))
                                {
                                    postXml_Condition.Fail = true;
                                    postXml_Condition.FailMessage = "找不到檔案,可能下載失敗";
                                    postXml_Condition.Number++;
                                }
                            }
                            catch (Exception e)
                            {
                                postXml_Condition.Fail = true;
                                postXml_Condition.FailMessage = e.Message;
                                postXml_Condition.Number++;
                            }
                        }
                        catch (Exception e)
                        {

                            postXml_Condition.Fail = true;
                            postXml_Condition.FailMessage = e.Message;
                            postXml_Condition.Number++;

                        }
                        //建立新的讓他用排成去跑順序
                        await _PostXml_DA.Insert(postXml_Condition);
                    }
                    info.StartId = lastId;
                    await _ScheduleTask_DA.UpdateSetStartId(info);
                }


            }
            catch (Exception e)
            {

                throw;
            }

            return getErrorPostFTPXMLFile;


        }

        /// <summary>
        /// 抓郵局的FTP
        /// </summary>
        /// <returns></returns>
        public async Task<GetPostFTPXMLFileJob_Res> GetPostFTPXMLFile()
        {
            GetPostFTPXMLFileJob_Res getPostFTPXMLFile = new GetPostFTPXMLFileJob_Res();
            getPostFTPXMLFile.count = 0;

            var PostXmlDownloadPath = _configuration["PostOffice:PostXmlDownloadPath"];
            var Host = _configuration["PostOffice:Host"];
            var UserName = _configuration["PostOffice:UserName"];
            var Password = _configuration["PostOffice:Password"];
            var PostErrorLogPath = _configuration["PostOffice:PostErrorLogPath"];
            string path = Path.Combine(PostErrorLogPath, DateTime.Now.ToString("yyyyMMdd"));

            //建立資料夾
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string Logpath = Path.Combine(path, "Errorlog.txt");


            List<string> list = new List<string>();

            try
            {
                /* Create Object Instance */
                FtpUtility ftpClient = new FtpUtility(Host, UserName, Password);

                var directory = ftpClient.directoryListSimple("");




                //過濾
                var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.GetPostFTPXMLFileJob)).FirstOrDefault();

                if (info == null)
                {
                    throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
                }
                var lastTime = info.StartTime;
                foreach (var _d in directory)
                {

                    if (!string.IsNullOrEmpty(_d))
                    {
                        /* Create Object Instance */
                        FtpUtility ftpClient2 = new FtpUtility(Host + "/" + _d, UserName, Password);

                        //只抓XML 附檔名
                        var Files = ftpClient2.GetFileInfos().Where(x => Path.GetExtension(x.FullPath).ToLower() == ".xml");


                        //郵局有可能有時間差所以排除重複的 預設抓三天內
                        var D = info.StartTime.AddDays(-3);


                        //時間去判斷上次抓到哪
                        Files = Files.Where(x => x.CreateTime > D).ToList();

                        if (Files != null && Files.Count() > 0)
                        {

                            getPostFTPXMLFile.count = Files.Count();

                            var last = Files.Max(p => p.CreateTime);

                            //下載
                            foreach (var _File in Files)
                            {

                                //確定是否抓過

                                var GetInfoByCdateAndName = (await _PostXml_DA.GetInfoByCdateAndName(D, _File.Name)).FirstOrDefault();
                                //沒抓過就抓
                                if (GetInfoByCdateAndName == null)
                                {

                                    PostXml_Condition postXml_Condition = new PostXml_Condition();
                                    try
                                    {
                                        string _file = Path.Combine(_d, _File.Name);
                                        string timepath = _File.CreateTime.HasValue ? _File.CreateTime.Value.ToString("yyyyMMdd") : DateTime.Now.ToString("yyyyMMdd");
                                        string filepath = Path.Combine(PostXmlDownloadPath, _d, timepath);
                                        //建立資料夾
                                        if (!Directory.Exists(filepath))
                                        {
                                            Directory.CreateDirectory(filepath);
                                        }
                                        //完整檔案路徑
                                        string fullfilepath = Path.Combine(filepath, _File.Name);
                                        postXml_Condition.FTPPath = _d;
                                        postXml_Condition.XmlName = _File.Name;
                                        postXml_Condition.XmlPath = fullfilepath;
                                        postXml_Condition.FileCreateTime = _File.CreateTime;
                                        try
                                        {
                                            /* Download a File */
                                            ftpClient.download(_file, fullfilepath);
                                            string readText = File.ReadAllText(fullfilepath);
                                            postXml_Condition.XmlContent = File.ReadAllText(fullfilepath);

                                            if (!File.Exists(fullfilepath))
                                            {
                                                postXml_Condition.Fail = true;
                                                postXml_Condition.FailMessage = "找不到檔案,可能下載失敗";
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            postXml_Condition.Fail = true;
                                            postXml_Condition.FailMessage = e.Message;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        list.Add(DateTime.Now.ToString());
                                        //throw;
                                        //還沒想到有啥錯誤
                                        list.Add("失敗Message :" + e.Message);
                                        list.Add("失敗StackTrace :" + e.StackTrace);
                                        list.Add("失敗Source :" + e.Source);
                                        postXml_Condition.Fail = true;
                                        postXml_Condition.FailMessage = e.Message;
                                        list.Add("----------------------------");
                                    }
                                    await _PostXml_DA.Insert(postXml_Condition);
                                }
                            }
                            if (DateTime.Compare(last.Value, lastTime) > 0)
                            {
                                lastTime = last.Value;
                            }
                        }
                    }

                }
                info.StartTime = lastTime;
                await _ScheduleTask_DA.UpdateSetStartDate(info);

            }
            catch (Exception e)
            {
                list.Add(DateTime.Now.ToString());
                list.Add("失敗Message :" + e.Message);
                list.Add("失敗StackTrace :" + e.StackTrace);
                list.Add("失敗Source :" + e.Source);
                //throw;
                list.Add("----------------------------");
            }



            File.AppendAllLines(Logpath, list);
            return getPostFTPXMLFile;
        }

        /// <summary>
        /// 郵局Q100報表
        /// </summary>
        /// <returns></returns>
        public async Task<CreateXlsxPostQ100ReportJob_Res> CreateXlsxPostQ100Report()
        {
            CreateXlsxPostQ100ReportJob_Res createXlsxPostQ100ReportJob = new CreateXlsxPostQ100ReportJob_Res();
            createXlsxPostQ100ReportJob.count = 0;
            ExcelParmModel<XlsxQ100ReportModel> excelParmModel = new ExcelParmModel<XlsxQ100ReportModel>();
            excelParmModel.TemplatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ExcelTemplate", "Q100ReportTemplate.xlsx");
            excelParmModel.SavePath = Path.Combine(_configuration["PostOffice:Report:Q100Path"], $"Q100-{DateTime.Now.ToString("yyyyMMdd")}.xlsx");
            excelParmModel.PassCell = new List<string>() { "Id" };
            excelParmModel.Identity.UserName = _configuration["PostOffice:Report:Username"];
            excelParmModel.Identity.Domain = _configuration["PostOffice:Report:Domain"];
            excelParmModel.Identity.Password = _configuration["PostOffice:Report:Password"];
            excelParmModel.Offset = 1;

            //取排程開始Id
            var scheduleTask_Condition = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.CreateXlsxPostQ100Report)).FirstOrDefault();

            if (scheduleTask_Condition == null)
            {
                throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            }

            //取報表資料資料
            excelParmModel.Data = (await _itcDeliveryRequests_DA.GetInfoByPostEodParseFilterId(scheduleTask_Condition.StartId)).ToList();

            //取得這次取資料最後StartId
            var endId = excelParmModel.Data.Max(x => x.Id);
            scheduleTask_Condition.StartId = endId;

            //寫入xlsx
            ExcelFile.Generate<XlsxQ100ReportModel>(excelParmModel);
            createXlsxPostQ100ReportJob.count = excelParmModel.Data.Count;

            //更新StartId
            await _ScheduleTask_DA.UpdateSetStartId(scheduleTask_Condition);

            return createXlsxPostQ100ReportJob;
        }
    }
}
