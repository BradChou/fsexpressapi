﻿using BLL.Model.ScheduleAPI.Res.DailySettle;
using Common;
using DAL.Contract_DA;
using DAL.DA;
using DAL.Model.Condition;
using DAL.Model.Contract_Condition;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;
using static Common.Setting.JobSetting;

namespace BLL
{
    public class DailySettleTodoListServices
    {
        private readonly IConfiguration _configuration;
        private IScheduleTask_DA _ScheduleTask_DA;
        private IDailySettleTodoList_DA _DailySettleTodoList_DA;
        private IDailySettleMainTemp_DA _DailySettleMainTemp_DA;
        private IDailySettleMain_DA _DailySettleMain_DA;
        private ItcDeliveryRequests_DA _tcDeliveryRequests_DA;
        private ISpecialAreaAudit_DA _SpecialAreaAudit_DA;

        public DailySettleTodoListServices(
             IConfiguration configuration,
             IScheduleTask_DA ScheduleTask_DA,
             IDailySettleTodoList_DA DailySettleTodoList_DA,
             IDailySettleMainTemp_DA DailySettleMainTemp_DA,
             IDailySettleMain_DA DailySettleMain_DA,
             ItcDeliveryRequests_DA tcDeliveryRequests_DA,
             ISpecialAreaAudit_DA SpecialAreaAudit_DA
            )
        {
            _configuration = configuration;
            _ScheduleTask_DA = ScheduleTask_DA;
            _DailySettleTodoList_DA = DailySettleTodoList_DA;
            _DailySettleMainTemp_DA = DailySettleMainTemp_DA;
            _DailySettleMain_DA = DailySettleMain_DA;
            _tcDeliveryRequests_DA = tcDeliveryRequests_DA;
            _SpecialAreaAudit_DA = SpecialAreaAudit_DA;
        }
        /// <summary>
        /// 抓取所有LIST 更新到DailySettleMainTemp
        /// </summary>
        /// <returns></returns>
        public async Task<DailySettleToDoListModel> TodoListUpdateMainTempJob(DailySettleToDoListModel data)
        {
            TodoListUpdateMainTempJob_Res todoListUpdateMainTempJob_Res = new TodoListUpdateMainTempJob_Res();

            ////抓上次更新位置

            ////抓未處理
            ////過濾
            //var info = (await _ScheduleTask_DA.GetInfoByTaskID(TaskCodeEnum.DailySettleTodoListJob)).FirstOrDefault();
            //if (info == null)
            //{
            //    throw new MyException(ResponseCodeEnum.ScheduleTaskNotFound);
            //}

            ////抓取所有更新

            //    List = await _DailySettleTodoList_DA.GetNODO();

            List<DailySettleSingleToDo> all = new List<DailySettleSingleToDo>();
            if (data == null)
            {
                return null;
            }
            var newList = data.NewDataList;
            var List = data.UpdateDataList;

            all.AddRange(data.NewDataList);
            all.AddRange(data.UpdateDataList);

            var _ListRequestId = all.Select(x => long.Parse(x.RequestId)).Distinct();


            //有改單
            var _L1 = List.Where(x => x.ActionType == "1").FirstOrDefault();
            //有追捕
            var _L2 = List.Where(x => x.ActionType == "2").FirstOrDefault();
            //有改材積
            var _L3 = List.Where(x => x.ActionType == "3").FirstOrDefault();

            List<DailySettleMainTemp_Condition> DailySettleMainTemps = new List<DailySettleMainTemp_Condition>();
            List<tcDeliveryRequests_Condition> tcDeliveryRequestss = new List<tcDeliveryRequests_Condition>();
            List<SpecialAreaAudit_Condition> SpecialAreaAudits = new List<SpecialAreaAudit_Condition>();

            //抓所有的TEMP
            //兩千一組
            List<List<long>> listGroup = new List<List<long>>();
            int c = 2000;//2000為一組
            int jj = c;
            for (int i = 0; i < _ListRequestId.Count(); i += c)
            {
                List<long> cList = new List<long>();
                cList = _ListRequestId.Take(jj).Skip(i).ToList();
                jj += c;
                var lll = await _DailySettleMainTemp_DA.GetListById(cList);
                var ddd = await _tcDeliveryRequests_DA.GetInfoListByrequestId(cList);
                var sss = await _SpecialAreaAudit_DA.GetListByRequestId(cList);
                if (lll.Any())
                {
                    DailySettleMainTemps.AddRange(lll);
                }
                if (ddd.Any())
                {
                    tcDeliveryRequestss.AddRange(ddd);
                }
                if (sss.Any())
                {
                    SpecialAreaAudits.AddRange(sss);
                }

                //listGroup.Add(cList);
            }

            List<DailySettleSingleToDo> NewDataList = new List<DailySettleSingleToDo>();

            List<DailySettleSingleToDo> UpdateDataList = new List<DailySettleSingleToDo>();


            List<DailySettleSingleToDo> ErrorDataList = new List<DailySettleSingleToDo>();

            //所有
            List<DailySettleMainTemp_Condition> allUpdate = new List<DailySettleMainTemp_Condition>();


            foreach (var item in all)
            {
                try
                {

                    switch (item.ActionType)
                    {
                        case "0"://新增
                            var d0 = tcDeliveryRequestss.Where(x => x.request_id.ToString() == item.RequestId).FirstOrDefault();
                            var t0 = DailySettleMainTemps.Where(x => x.RequestId.ToString() == item.RequestId).FirstOrDefault();
                            var s0 = SpecialAreaAudits.Where(x => x.RequestId == item.RequestId.ToString()).FirstOrDefault();

                            if (t0 == null)
                            {
                                throw new Exception();
                            }

                            if (d0 != null && t0 != null)
                            {
                                t0.ReviseUser = d0.uuser;
                                t0.ReviseDate = d0.udate;
                                t0.SendStationScode = d0.send_station_scode;
                                t0.SendSD = d0.SendSD;
                                t0.SendMD = d0.SendMD;
                                t0.CodeName = d0.subpoena_category;
                                t0.AreaArriveCode = d0.area_arrive_code;
                                t0.SendTel = d0.send_tel;
                                t0.SendContact = d0.send_contact;
                                t0.SendCity = d0.send_city;
                                t0.SendArea = d0.send_area;
                                t0.SendAddress = d0.send_address;
                                t0.ReceiveTel = d0.receive_tel1;
                                t0.ReceiveContact = d0.receive_contact;
                                t0.ReceiveCity = d0.receive_city;
                                t0.ReceiveArea = d0.receive_area;
                                t0.ReceiveAddress = d0.receive_address;
                                t0.TotalWeight = d0.cbmWeight.HasValue ? (decimal)d0.cbmWeight : null;
                                t0.SpecialAreaFlag = d0.SpecialAreaId > 0 ? true : false;
                                t0.SpecialAreaFee = d0.SpecialAreaFee;
                                t0.CollectionMoney = d0.collection_money;
                                t0.SpecialAreaFee = d0.SpecialAreaFee;
                                t0.SpecialAreaFee = d0.SpecialAreaFee;
                                t0.UpdateTime = DateTime.Now;
                                t0.UpdateDate = DateTime.Now.Date;
                                t0.DeliveryCreateDate = d0.cdate;
                                t0.DeliveryCreateUser = d0.cuser;
                                t0.ReceiptFlag = d0.receipt_flag;
                                t0.ReimbursementFee = d0.ReportFee;
                                t0.ReimbursementFlag = d0.ProductValue.HasValue ? (d0.ProductValue.Value > 0 ? true : false) : false;
                                t0.ReimbursementPremiumFee = d0.ProductValue;
                                t0.Pieces = d0.pieces;
                                t0.ShipDate = d0.ship_date;
                            }
                            if (d0 != null && t0 != null && s0 != null)
                            {
                                t0.SpecialAreaFlag = true;
                                t0.SpecialAreaFee = s0.SpecialAreaFee;
                                t0.UpdateTime = DateTime.Now;
                                t0.UpdateDate = DateTime.Now.Date;
                                //await _DailySettleMainTemp_DA.Update(t2);
                            }
                            allUpdate.Add(t0);

                            NewDataList.Add(item);

                            break;
                        case "1"://改單
                            var d = tcDeliveryRequestss.Where(x => x.request_id.ToString() == item.RequestId).FirstOrDefault();
                            var t = DailySettleMainTemps.Where(x => x.RequestId.ToString() == item.RequestId).FirstOrDefault();
                            if (t == null)
                            {
                                throw new Exception();
                            }

                            if (d != null && t != null)
                            {
                                t.ReviseUser = d.uuser;
                                t.ReviseDate = d.udate;
                                t.SendStationScode = d.send_station_scode;
                                t.SendSD = d.SendSD;
                                t.SendMD = d.SendMD;
                                t.CodeName = d.subpoena_category;
                                t.AreaArriveCode = d.area_arrive_code;
                                t.SendTel = d.send_tel;
                                t.SendContact = d.send_contact;
                                t.SendCity = d.send_city;
                                t.SendArea = d.send_area;
                                t.SendAddress = d.send_address;
                                t.ReceiveTel = d.receive_tel1;
                                t.ReceiveContact = d.receive_contact;
                                t.ReceiveCity = d.receive_city;
                                t.ReceiveArea = d.receive_area;
                                t.ReceiveAddress = d.receive_address;
                                t.TotalWeight = d.cbmWeight.HasValue ? (decimal)d.cbmWeight : null;
                                t.SpecialAreaFlag = d.SpecialAreaId > 0 ? true : false;
                                t.SpecialAreaFee = d.SpecialAreaFee;
                                t.CollectionMoney = d.collection_money;
                                t.SpecialAreaFee = d.SpecialAreaFee;
                                t.SpecialAreaFee = d.SpecialAreaFee;
                                t.UpdateTime = DateTime.Now;
                                t.UpdateDate = DateTime.Now.Date;
                                t.DeliveryCreateDate = d.cdate;
                                t.DeliveryCreateUser = d.cuser;
                                t.ReceiptFlag = d.receipt_flag;
                                t.ReimbursementFee = d.ReportFee;
                                t.ReimbursementFlag = d.ProductValue.HasValue ? (d.ProductValue.Value > 0 ? true : false) : false;
                                t.ReimbursementPremiumFee = d.ProductValue;
                                t.Pieces = d.pieces;
                                t.ShipDate = d.ship_date;
                                allUpdate.Add(t);
                                //await _DailySettleMainTemp_DA.Update(t);
                            }
                            UpdateDataList.Add(item);
                            break;
                        case "2"://追捕
                            var d2 = tcDeliveryRequestss.Where(x => x.request_id.ToString() == item.RequestId).FirstOrDefault();
                            var t2 = DailySettleMainTemps.Where(x => x.RequestId.ToString() == item.RequestId).FirstOrDefault();
                            var s2 = SpecialAreaAudits.Where(x => x.RequestId == item.RequestId.ToString()).FirstOrDefault();

                            if (t2 == null)
                            {
                                throw new Exception();
                            }

                            if (d2 != null && t2 != null && s2 != null)
                            {
                                t2.SpecialAreaFlag = true;
                                t2.SpecialAreaFee = s2.SpecialAreaFee;
                                t2.UpdateTime = DateTime.Now;
                                t2.UpdateDate = DateTime.Now.Date;
                                //await _DailySettleMainTemp_DA.Update(t2);
                                allUpdate.Add(t2);
                            }
                            UpdateDataList.Add(item);
                            break;
                        case "3"://補材積
                            var d3 = tcDeliveryRequestss.Where(x => x.request_id.ToString() == item.RequestId).FirstOrDefault();
                            var t3 = DailySettleMainTemps.Where(x => x.RequestId.ToString() == item.RequestId).FirstOrDefault();

                            if (t3 == null)
                            {
                                throw new Exception();
                            }

                            if (d3 != null && t3 != null)
                            {
                                //記得補就好
                                t3.UpdateTime = DateTime.Now;
                                t3.UpdateDate = DateTime.Now.Date;
                                //await _DailySettleMainTemp_DA.Update(t3);
                                allUpdate.Add(t3);
                            }
                            UpdateDataList.Add(item);
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception e)
                {
                    ErrorDataList.Add(item);
                }
            }

            //批次更新

            await _DailySettleMainTemp_DA.BUpdate(allUpdate);

            data.UpdateDataList = UpdateDataList;
            data.ErrorDataList = ErrorDataList;
            return data;
        }



    }
}
