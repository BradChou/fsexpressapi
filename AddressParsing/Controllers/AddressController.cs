﻿using BLL;
using BLL.Model.AddressParsing.Req.Address;
using BLL.Model.AddressParsing.Res.Address;
using BLL.Model.FSExpressAPI.Model;
using Common;
using Common.Excel;
using Common.Redis;
using DAL.DA;
using DAL.JunFuTrans_DA;
using DAL.Model.JunFuAddress_Condition;
using DAL.Model.JunFuTrans_Condition;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using static Common.Setting.EnumSetting;

namespace AddressParsing.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        private PostOfficeServices _postOfficeServices;
        private AddressServices _addressServices;
        private ItcDeliveryRequestsReSpecialArea_DA _tcDeliveryRequestsReSpecialArea_DA;
        private ItbCustomers_DA _tbCustomers_DA;
        private IorgArea_DA _orgArea_DA;

        public AddressController(AddressServices addressServices, IorgArea_DA orgArea_DA, PostOfficeServices postOfficeServices, ItbCustomers_DA tbCustomers_DA, ItcDeliveryRequestsReSpecialArea_DA tcDeliveryRequestsReSpecialArea_DA)
        {
            _addressServices = addressServices;
            _tbCustomers_DA = tbCustomers_DA;
            _orgArea_DA = orgArea_DA;
            _postOfficeServices = postOfficeServices;
            _tcDeliveryRequestsReSpecialArea_DA = tcDeliveryRequestsReSpecialArea_DA;
        }

        public class TestClass
        {

            public string a { get; set; }
            public string b { get; set; }

            public string c { get; set; }

            public string d { get; set; }

            public string e { get; set; }

            public string f { get; set; }
            public string g { get; set; }
            public string h { get; set; }
            public string i { get; set; }


        }



        [HttpPost("[action]")]
        public async Task<JsonResult> Excel()
        {
            var aaa = ExcelFile.ToObject<TestClass>("");



            using (var _transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {



                foreach (var _item in aaa)
                {
                    //    List<PostStatusInfo> fsdf = await _postOfficeServices.GetPostStatusList(_item.c.Trim());


                    //var info = fsdf.OrderBy(x => x.Date).LastOrDefault();


                    //lllll.Add(info);


                    try
                    {

                        var aaaa = await _tbCustomers_DA.GetInfo(_item.a);
                        if (aaaa != null)
                        {

                            aaaa.MasterCustomerCode = _item.c;
                            aaaa.MasterCustomerName = _item.d;

                            await _tbCustomers_DA.Update(aaaa);
                        }


                    }
                    catch (Exception e)
                    {

                        // throw;
                    }

                }
                _transactionScope.Complete();

            }
            // ExcelFile.SaveFile<PostStatusInfo>(lllll, @"");
            return new JsonResult(new { Code = 200, Message = "成功" });
        }
        //https://www.cnblogs.com/whuanle/p/13837153.html#%E5%AD%97%E7%AC%A6%E4%B8%B2string
        [HttpGet("Set")]
        public async Task<JsonResult> SetCache(string setkey, string setvalue)
        {

            var count = 100;
            var data = new List<string>();
            for (int i = 0; i < count; i++)
            {
                data.Add(i.ToString().PadLeft(6, '0'));
            }

            var aaa = await _orgArea_DA.All();
            string jsonData = JsonConvert.SerializeObject(aaa);
            //_redis.StringSet("orgArea_DA_All", jsonData, flags: CommandFlags.FireAndForget);

            //foreach (var entity in data)
            //{
            //    _redis.ListRightPush("A1", entity, flags: CommandFlags.FireAndForget);
            //}

            //   _redis.StringSet(setkey, setvalue, TimeSpan.FromSeconds(60),false, When.Always);



            string key = "key1";
            if (!string.IsNullOrEmpty(setkey))
                key = setkey;
            string value = DateTime.Now.ToLongTimeString();
            if (!string.IsNullOrEmpty(setvalue))
                value = setvalue;

            var options = new DistributedCacheEntryOptions()
           .SetSlidingExpiration(TimeSpan.FromSeconds(30));

            //  await _cache.SetStringAsync(key, value, options);
            return new JsonResult(new { Code = 200, Message = "设置缓存成功", Data = "key=" + key + "    value=" + value });
        }

        [HttpGet("Get")]
        public async Task<JsonResult> GetCache(string setkey)
        {



            //   string orgArea_DA_All = _redis.StringGet("orgArea_DA_All");

            //var descJsonStu = JsonConvert.DeserializeObject<List<orgArea_Condition>>(orgArea_DA_All);//反序列化


            //string orgArea_DA_All = _redis.StringGet("orgArea_DA_All");

            //var descJsonStu = JsonConvert.DeserializeObject<List<orgArea_Condition>>(orgArea_DA_All);//反序列化


            string key = "key1";
            if (!string.IsNullOrEmpty(setkey))
                key = setkey;
            //  var value = await _cache.GetStringAsync(key);
            return new JsonResult(new { Code = 200, Message = "设置缓存成功", Data = "" });
        }

        /// <summary>
        /// 詳細資訊
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetDetail(GetDetail_Req req)
        {

            //檢查參數
            #region 檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.Address))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.CustomerCode))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.ComeFrom))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            try
            {
                if (!Enum.IsDefined(typeof(AddressParsingTypeEnum), int.Parse(req.Type)))
                {
                    throw new MyException(ResponseCodeEnum.ParameterError);
                }

            }
            catch (Exception)
            {

                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.Type))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            try
            {
                if (!Enum.IsDefined(typeof(AddressParsingComeFromEnum), int.Parse(req.ComeFrom)))
                {
                    throw new MyException(ResponseCodeEnum.ParameterError);
                }

            }
            catch (Exception)
            {

                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            #endregion

            //去除空白換行
            req.Address = Tool.StrTrimer(req.Address);

            ResData<GetDetail_Res> resData = new ResData<GetDetail_Res>();
            GetDetail_Res getDetail_Res = await _addressServices.GetDetail(req);

            resData.Data = getDetail_Res;
            return Ok(resData);
        }



        /// <summary>
        /// 詳細資訊
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetSpecialAreaDetail(GetSpecialAreaDetail_Req req)
        {

            //檢查參數
            #region 檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.Address))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.CustomerCode))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.ComeFrom))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            try
            {
                if (!Enum.IsDefined(typeof(AddressParsingTypeEnum), int.Parse(req.Type)))
                {
                    throw new MyException(ResponseCodeEnum.ParameterError);
                }

            }
            catch (Exception)
            {

                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.Type))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            try
            {
                if (!Enum.IsDefined(typeof(AddressParsingComeFromEnum), int.Parse(req.ComeFrom)))
                {
                    throw new MyException(ResponseCodeEnum.ParameterError);
                }

            }
            catch (Exception)
            {

                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            #endregion

            //去除空白換行
            //    req.Address = Tool.StrTrimer(req.Address);

            ResData<GetSpecialAreaDetail_Res> resData = new ResData<GetSpecialAreaDetail_Res>();
            GetSpecialAreaDetail_Res getDetail_Res = await _addressServices.GetSpecialAreaDetail(req);

            resData.Data = getDetail_Res;
            return Ok(resData);
        }

        /// <summary>
        /// 詳細資訊
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<ActionResult<ResData>> GetSpecialAreaDetailRe()
        {

            var aaa = ExcelFile.ToObject<TestClass>(@"D:\123123\789.xlsx");
            //抓所有地址
            //var all = await _tcDeliveryRequestsReSpecialArea_DA.GetAll();

            ResData resData = new ResData();

            foreach (var item in aaa)
            {
                await _tcDeliveryRequestsReSpecialArea_DA.UpdateR(item.a,item.b,item.c);
            }

            ////重跑回寫
            //int c = 0;
            //foreach (var item in all)
            //{
            //    c++;
            //    GetSpecialAreaDetail_Req aaa = new GetSpecialAreaDetail_Req()
            //    {
            //        Address = item.receive_city + item.receive_area + item.receive_address,
            //        Type = "1",
            //        ComeFrom = "777",
            //        CustomerCode = "F2900570002"
            //    };


            //    GetSpecialAreaDetail_Res getDetail_Res = await _addressServices.GetSpecialAreaDetail(aaa);

            //    if (getDetail_Res != null) 
            //    {
            //        if (getDetail_Res.id == 0)
            //        {
            //            item.SpecialAreaIdRe = (int)getDetail_Res.id;
            //            item.SpecialAreaFeeRe = 0;
            //            item.CodeRe = "";
            //            item.SDRe = "";
            //            item.MDRe = "";

            //        }
            //        else 
            //        {
            //            item.SpecialAreaIdRe = (int)getDetail_Res.id;
            //            item.SpecialAreaFeeRe = getDetail_Res.Fee;
            //            item.CodeRe = getDetail_Res.StationCode;
            //            item.SDRe = getDetail_Res.SalesDriverCode;
            //            item.MDRe = getDetail_Res.MotorcycleDriverCode;
            //        }

            //    }


            //  await  _tcDeliveryRequestsReSpecialArea_DA.Update(item);

            //}








            return Ok(resData);
        }
        /// <summary>
        /// 詳細資訊
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<ActionResult<ResData>> GetToken()
        {

            ResData<string> resData = new ResData<string>();

            string key = "39fa557ca13feebg";

            string A = @"eyJ2IjoibXZpbmhvbjRPeGZ2Y2ZiWjNtZUVkdz09IiwidCI6Ijl1ZXEzT3ViVURleWt2WTVxV1R1QWx0MzNnZ0ZRSGxRa2NRcDFZa1FkSWs9IiwibSI6Ijc5OGIwMjljY2IxMDFiMjllYWYwNjdjOTYzODI2ZDM2Y2NmYzBhZjFmNjk2ZjIwM2VjMTdlMThmYzc1NDViYmIifQ==";


            string B = string.Empty;

            var unixtemp = DateTimeOffset.Now.ToUnixTimeSeconds().ToString();

            Byte[] bytesEncode = System.Text.Encoding.UTF8.GetBytes(unixtemp); //取得 UTF8 2進位 Byte
            B = Convert.ToBase64String(bytesEncode); // 轉換 Base64 索引表

             var C =     Security.HMACSHA256(A + "." + B, key);

            resData.Data = A + "." + B + "." + C;

            return Ok(resData);
        }

    }
}
