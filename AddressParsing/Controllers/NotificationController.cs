﻿using BLL;
using BLL.Model.AddressParsing.Req.Notification;
using Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;

namespace AddressParsing.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private NotificationServices _NotificationServices;
       
        public NotificationController(NotificationServices NotificationServices)
        {
            _NotificationServices = NotificationServices;
          
        }


        /// <summary>
        /// 詳細資訊
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> SMSSend(SMSSend_Req req)
        {

            //檢查參數
          
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (req.ComeFrom == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (req.Mobile == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (req.Content == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (req.SendUser == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            bool telcheck = Regex.IsMatch(req.Mobile, @"^09[0-9]{8}$");//規則:09開頭，後面接著8個0~9的數字，@是避免跳脫字元
                                                                       //isMatch回傳布林值T或F
            if (!telcheck)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            ResData resData = new ResData();
            await _NotificationServices.InsertSMS(req);

          
            return Ok(resData);
        }
    }
}
