﻿using BLL;
using BLL.Model.AddressParsing.Model;
using BLL.Model.AddressParsing.Req.Address;
using Common;
using DAL.Model.JunFuAddress_Condition;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;

namespace AddressParsing.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class Address_itriController : Controller
    {
        private AddressServices_itri _addressServices_itri;
        private IMemoryCache _cache;

        public Address_itriController(AddressServices_itri addressServices_itri , IMemoryCache memoryCache)
        {
            _addressServices_itri = addressServices_itri;
            _cache = memoryCache;
        }

        /// <summary>
        /// 間隔0.1秒  
        /// 待補充細節邏輯 
        /// ex. 排隊一直等
        /// </summary>
        /// <returns></returns>
        private bool checkNotBusy()
        {
            bool returnBool = true;
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                                .SetSlidingExpiration(TimeSpan.FromMinutes(3));

            string CACHE_KEY = "AddressType_itri";
            double GapSeconds = 0.1;
            DateTime NowTime = DateTime.Now;
            DateTime LastTime = NowTime;

            try
            {
                if (!_cache.TryGetValue(CACHE_KEY, out LastTime))
                {
                    LastTime = NowTime;
                    _cache.Set(CACHE_KEY, LastTime, cacheEntryOptions);
                }

                double DiffTime = new TimeSpan( NowTime.Ticks - LastTime.Ticks).TotalSeconds;

                if ( !NowTime.Equals(LastTime) && DiffTime < GapSeconds)
                {
                    int iKey = 1;
                    int iCount = 20;

                    while (iKey <= iCount && DiffTime < 0.1)
                    {
                        System.Threading.Thread.Sleep(50);
                        NowTime = DateTime.Now;
                        DiffTime = new TimeSpan(LastTime.Ticks - NowTime.Ticks).TotalSeconds;

                        iKey += 1;
                    }

                    NowTime = DateTime.Now;
                    _cache.Set(CACHE_KEY, NowTime, cacheEntryOptions);

                    returnBool = false;
                }
            }
            catch(Exception ex)
            {
                returnBool = false;
            }

            return returnBool;
        }


        [HttpPost("[action]")]
        public ParseAddress_itri_Model GetAddressRsult(GetAddressRsult_Req req)
        {
            /*
            bool isNotBusy = checkNotBusy();

            if (isNotBusy.Equals(false))
            {
                //太忙了 要自解
                if (1==1)
                {

                }
            }
            */

            ParseAddress_itri_Model returnModel = new ParseAddress_itri_Model();

            //檢查參數
            #region 檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.Address))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.CustomerCode))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.ComeFrom))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            try
            {
                if (!Enum.IsDefined(typeof(AddressParsingTypeEnum), int.Parse(req.Type)))
                {
                    throw new MyException(ResponseCodeEnum.ParameterError);
                }
            }
            catch (Exception)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.Type))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            try
            {
                if (!Enum.IsDefined(typeof(AddressParsingComeFromEnum), int.Parse(req.ComeFrom)))
                {
                    throw new MyException(ResponseCodeEnum.ParameterError);
                }

            }
            catch (Exception)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            #endregion

            //去除空白換行
            req.Address = Tool.StrTrimer(req.Address);

            returnModel = _addressServices_itri.GetZipCodeAndSave(req.isSave , req.Address , req.Type, req.ComeFrom , req.CustomerCode);

            return returnModel;
        }


        [HttpPost("[action]")]
        public Map8 GetAddressRsult_Map8_2(GetAddressRsult_Req req)
        {
            Map8 returnModel = new Map8();

            //檢查參數
            #region 檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.Address))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.CustomerCode))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.ComeFrom))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            try
            {
                if (!Enum.IsDefined(typeof(AddressParsingTypeEnum), int.Parse(req.Type)))
                {
                    throw new MyException(ResponseCodeEnum.ParameterError);
                }
            }
            catch (Exception)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (string.IsNullOrEmpty(req.Type))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            try
            {
                if (!Enum.IsDefined(typeof(AddressParsingComeFromEnum), int.Parse(req.ComeFrom)))
                {
                    throw new MyException(ResponseCodeEnum.ParameterError);
                }

            }
            catch (Exception)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            #endregion

            //去除空白換行 
            req.Address = Tool.StrTrimer(req.Address);

            returnModel = _addressServices_itri.GetMap8_2AndSave(req.isSave, req.Address, req.Type, req.ComeFrom, req.CustomerCode);
            //returnModel = _addressServices_itri.GetMap8_2AndSave_async(req.isSave, req.Address, req.Type, req.ComeFrom, req.CustomerCode);

            if (returnModel.status.Equals(""))
            {
                //old();
            }

            return returnModel;
        }

    }
}
