﻿using Common;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;

namespace AddressParsing.Handler
{
    public class CashExceptionHandler
    {
        private readonly RequestDelegate _next;
        public CashExceptionHandler(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                try
                {
                    await _next(context);
                }
                catch (MyException ex)
                {
                    ResData resData = new ResData();
                    resData.Status = ex.ResponseCode;
                    resData.Message = ex.Message;
                    context.Response.StatusCode = 200;
                    var serializerSettings = new JsonSerializerSettings
                    {
                        // 設定為駝峰命名
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    };
                    var result = JsonConvert.SerializeObject(resData, Formatting.None, serializerSettings);
                    //  var result = JsonConvert.SerializeObject(resData);
                    await context.Response.WriteAsync(result);
                }
            }
            catch (Exception e)//不知道為什麼沒吃到多補一層
            {

                ResData resData = new ResData();
                resData.Status = ResponseCodeEnum.Error;
                //resData.Message = e.Message;
                resData.Message = "系統異常";
                context.Response.StatusCode = 200;
                var serializerSettings = new JsonSerializerSettings
                {
                    // 設定為駝峰命名
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };
                var result = JsonConvert.SerializeObject(resData, Formatting.None, serializerSettings);
                // var result = JsonConvert.SerializeObject(resData);
                await context.Response.WriteAsync(result);
            }

        }
    }
}
