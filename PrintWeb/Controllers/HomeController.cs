﻿using Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OfficeOpenXml;
using PdfPrintingNet;
using PrintWeb.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;
using static PdfPrintingNet.PdfPrint;

namespace PrintWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;
        public HomeController(ILogger<HomeController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        public IActionResult Index()
        {
            return View();
        }
        public class GetPostList_Res
        {

            public string PostId { get; set; }

            public string SerialNumber { get; set; }
            public string CommodityCode { get; set; }
            public string DiscernCode { get; set; }
            public string Zip3 { get; set; }
            public string CheckCode { get; set; }
            public string check_number { get; set; }
            public string receive_contact { get; set; }
            public string receive_city { get; set; }
            public string receive_area { get; set; }
            public string receive_address { get; set; }
            public string Collection_money { get; set; }
            public string Weight { get; set; }
            public DateTime CreateTime { get; set; }
        }

        public class WeightList
        {

            public int c1 { get; set; }//小1
            public int c2 { get; set; }//小2
            public int c3 { get; set; }//小3
            public int c4 { get; set; }//小4
            public int c5 { get; set; }//裹1
            public int c6 { get; set; }//裹2
            public int c7 { get; set; }//裹3
            public int c8 { get; set; }//裹4
            public int c9 { get; set; }//超
            public int c10 { get; set; }//代收
        }

        public class Weight
        {

            public string Name { get; set; }
            public int Count { get; set; }

        }

        public ActionResult download2(string sdate, string edate)
        {

            sdate = sdate + ":00:00";

            edate = edate + ":00:00";

            DateTime dateTime = DateTime.Now;

            DateTime dateTime_S = DateTime.Parse(sdate);
            DateTime dateTime_E = DateTime.Parse(edate);

            if (DateTime.Compare(dateTime_S, dateTime_E) > 0)
            {

                TempData["message"] = "時間範圍寫反";
                return RedirectToAction("Report");

                //dateTime_S = DateTime.Parse(edate);
                // dateTime_E = DateTime.Parse(sdate);


            }

            string dateDiff = null;
            TimeSpan ts1 = new TimeSpan(dateTime_S.Ticks);
            TimeSpan ts2 = new TimeSpan(dateTime_E.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            //顯示時間  
            dateDiff = ts.Days.ToString() + "天"
                    + ts.Hours.ToString() + "小時"
                    + ts.Minutes.ToString() + "分鐘"
                    + ts.Seconds.ToString() + "秒";

            if (ts.Days > 30)
            {
                TempData["message"] = "時間範圍超過一個月";
                return RedirectToAction("Report");
            }

            string Account = _config["DataSource:Account"].ToString();
            string Password = _config["DataSource:Password"].ToString();
            string GetTokenURL = _config["GetTokenURL"].ToString();
            string GetPostListURL = _config["GetPostListURL"].ToString();

            //打API
            var client2 = new RestClient(GetTokenURL);
            var request2 = new RestRequest(Method.POST);
            request2.AddHeader("content-type", "application/json");
            request2.AddJsonBody(new { Account = Account, Password = Password });
            var response2 = client2.Execute(request2);

            var Token = JsonConvert.DeserializeObject<ResData<string>>(response2.Content);


            //打API
            var client = new RestClient(GetPostListURL);
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("Token", Token.Data);

            request.AddJsonBody(new { startTime = dateTime_S.ToString("yyyy-MM-dd HH") + ":00:00", endTime = dateTime_E.ToString("yyyy-MM-dd HH") + ":00:00" });
            var response = client.Execute(request);
            //打API

            //    var response = new RestRequest(Method.POST);
            //request.AddParameter("startTime", dateTime.ToString("yyyy-MM-dd") + " 18:00:00");//開始
            //request.AddParameter("endTime", dateTime.AddDays(-1).ToString("yyyy-MM-dd") + " 18:00:00");//結束
            //var response = client.Post<ResData<List<GetPostList_Res>>>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var keyResponse = JsonConvert.DeserializeObject<ResData<List<GetPostList_Res>>>(response.Content);

                WeightList weightList = new WeightList();
                weightList.c1 = 0;
                weightList.c2 = 0;
                weightList.c3 = 0;
                weightList.c4 = 0;
                weightList.c5 = 0;
                weightList.c6 = 0;
                weightList.c7 = 0;
                weightList.c8 = 0;
                weightList.c9 = 0;

                foreach (var item in keyResponse.Data)
                {
                    int c;

                    if (int.TryParse(item.Collection_money, out c))
                    {
                        if (c > 0)
                        {
                            weightList.c10++;
                        }
                        else
                        {
                            int w;
                            if (int.TryParse(item.Weight, out w))
                            {
                                if (w <= 100)
                                {
                                    weightList.c1++;
                                }
                                if (w > 100 && w <= 200)
                                {
                                    weightList.c2++;
                                }
                                if (w > 200 && w <= 300)
                                {
                                    weightList.c3++;
                                }
                                if (w > 300 && w <= 400)
                                {
                                    weightList.c4++;
                                }
                                if (w > 400 && w <= 5000)
                                {
                                    weightList.c5++;
                                }
                                if (w > 5000 && w <= 10000)
                                {
                                    weightList.c6++;
                                }
                                if (w > 10001 && w <= 15000)
                                {
                                    weightList.c7++;
                                }
                                if (w > 15000 && w <= 20000)
                                {
                                    weightList.c8++;
                                }
                                if (w > 20000)
                                {
                                    weightList.c9++;
                                }

                            }
                        }

                    }






                }

                List<Weight> wt = new List<Weight>();

                wt.Add(new Weight() { Name = "小1", Count = weightList.c1 });
                wt.Add(new Weight() { Name = "小2", Count = weightList.c2 });
                wt.Add(new Weight() { Name = "小3", Count = weightList.c3 });
                wt.Add(new Weight() { Name = "小4", Count = weightList.c4 });
                wt.Add(new Weight() { Name = "裹1", Count = weightList.c5 });
                wt.Add(new Weight() { Name = "裹2", Count = weightList.c6 });
                wt.Add(new Weight() { Name = "裹3", Count = weightList.c7 });
                wt.Add(new Weight() { Name = "裹4", Count = weightList.c8 });
                wt.Add(new Weight() { Name = "超", Count = weightList.c9 });
                wt.Add(new Weight() { Name = "代收", Count = weightList.c10 });



                var data18 = new List<GetPostList_Res>();
                var data78 = new List<GetPostList_Res>();
                var data74 = new List<GetPostList_Res>();


                foreach (var item in keyResponse.Data)
                {
                    if (string.IsNullOrEmpty(item.DiscernCode))
                    {
                        var DiscernCode = item.PostId.Substring(item.PostId.Length - 2, 2);

                        switch (DiscernCode)
                        {
                            case "18":
                                data18.Add(item);
                                break;

                            case "78":
                                data78.Add(item);
                                break;
                            case "74":
                                data74.Add(item);
                                break;
                            default:
                                break;
                        }

                    }
                    else
                    {
                        switch (item.DiscernCode)
                        {
                            case "18":
                                data18.Add(item);
                                break;
                            case "78":
                                data78.Add(item);
                                break;
                            case "74":
                                data74.Add(item);
                                break;
                            default:
                                break;
                        }
                    }
                }





                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                //Create a test file
                var fi = new FileInfo(@".\excel\PostEmpty.xlsx");
                //建立Excel
                ExcelPackage ep = new ExcelPackage(fi);
                var workbook = ep.Workbook;
                // var worksheet = workbook.Worksheets.First();

                //第一頁

                //建立第一個Sheet，後方為定義Sheet的名稱
                var sheet18 = ep.Workbook.Worksheets[0];
                var sheet78 = ep.Workbook.Worksheets[1];
                var sheet74 = ep.Workbook.Worksheets[2];
                var all = ep.Workbook.Worksheets[3];


                int col = 1;    //欄:直的，因為要從第1欄開始，所以初始為1

                //資料從第2列開始
                int row = 2;    //列:橫的
                foreach (var item in data18.OrderBy(x => x.PostId))
                {
                    col = 1;//每換一列，欄位要從1開始
                            //指定Sheet的欄與列(欄名列號ex.A1,B20，在這邊都是用數字)，將資料寫入
                    sheet18.Cells[row, col++].Value = "18";
                    sheet18.Cells[row, col++].Value = item.PostId;
                    sheet18.Cells[row, col++].Value = item.check_number;
                    sheet18.Cells[row, col++].Value = item.receive_contact;
                    sheet18.Cells[row, col++].Value = item.receive_city;
                    sheet18.Cells[row, col++].Value = item.receive_area;
                    sheet18.Cells[row, col++].Value = item.receive_address;
                    sheet18.Cells[row, col++].Value = item.Weight;
                    sheet18.Cells[row, col++].Value = item.CreateTime.ToString("yyyy/MM/dd HH:mm:ss");

                    row++;
                }
                col = 1;
                row = 2;
                //第二頁
                foreach (var item in data78.OrderBy(x => x.PostId))
                {
                    col = 1;//每換一列，欄位要從1開始
                            //指定Sheet的欄與列(欄名列號ex.A1,B20，在這邊都是用數字)，將資料寫入
                    sheet78.Cells[row, col++].Value = "78";
                    sheet78.Cells[row, col++].Value = item.PostId;
                    sheet78.Cells[row, col++].Value = item.check_number;
                    sheet78.Cells[row, col++].Value = item.receive_contact;
                    sheet78.Cells[row, col++].Value = item.receive_city;
                    sheet78.Cells[row, col++].Value = item.receive_area;
                    sheet78.Cells[row, col++].Value = item.receive_address;
                    sheet78.Cells[row, col++].Value = item.Weight;
                    sheet78.Cells[row, col++].Value = item.CreateTime.ToString("yyyy/MM/dd HH:mm:ss");

                    row++;
                }
                col = 1;
                row = 2;
                //第三頁
                foreach (var item in data74.OrderBy(x => x.PostId))
                {
                    col = 1;//每換一列，欄位要從1開始
                            //指定Sheet的欄與列(欄名列號ex.A1,B20，在這邊都是用數字)，將資料寫入
                    sheet74.Cells[row, col++].Value = "74";
                    sheet74.Cells[row, col++].Value = item.PostId;
                    sheet74.Cells[row, col++].Value = item.check_number;
                    sheet74.Cells[row, col++].Value = item.receive_contact;
                    sheet74.Cells[row, col++].Value = item.receive_city;
                    sheet74.Cells[row, col++].Value = item.receive_area;
                    sheet74.Cells[row, col++].Value = item.receive_address;
                    sheet74.Cells[row, col++].Value = item.Collection_money;
                    sheet74.Cells[row, col++].Value = item.Weight;
                    sheet74.Cells[row, col++].Value = item.CreateTime.ToString("yyyy/MM/dd HH:mm:ss");

                    row++;
                }
                col = 1;
                row = 1;
                //第四頁
                foreach (var item in wt)
                {
                    col = 1;//每換一列，欄位要從1開始
                            //指定Sheet的欄與列(欄名列號ex.A1,B20，在這邊都是用數字)，將資料寫入
                    all.Cells[row, col++].Value = item.Name;
                    all.Cells[row, col++].Value = item.Count;


                    row++;
                }




                //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                MemoryStream fileStream = new MemoryStream();
                ep.SaveAs(fileStream);
                ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤
                return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", dateTime.ToString("yyyyMMdd") + "清單" + ".xlsx");


                //MemoryStream stream = new MemoryStream();
                //StreamWriter writer = new StreamWriter(stream, Encoding.UTF8);
                //writer.WriteLine("郵局ID,全速配ID,重量(g),時間");
                //foreach (var item in keyResponse.Data)
                //{



                //    writer.WriteLine(item.PostId+ ","+item.check_number+ ","+item.Weight+ ","+item.CreateTime.ToString("yyyyMMddHHmmss"));
                //}




                //writer.Flush();
                //stream.Position = 0;

                //return File(stream, "text/csv", dateTime.ToString("yyyyMMdd")+"清單" + ".csv");
            }
            else
            {
                // _logger.LogInformation("檢查URL   " + _apiURL);
            }


            return null;


        }
        //public FileResult download()
        //{

        //    DateTime dateTime = DateTime.Now;

        //    DateTime dateTime_S = dateTime;
        //    DateTime dateTime_E = dateTime;


        //    if (dateTime.Hour < 12)
        //    {
        //        dateTime_S = dateTime_S.AddDays(-1);
        //    }
        //    else
        //    {
        //        dateTime_E = dateTime_E.AddDays(1);
        //    }


        //   // DateTime dateTime = DateTime.Now;
        //    string Account = _config["DataSource:Account"].ToString();
        //    string Password = _config["DataSource:Password"].ToString();
        //    string GetTokenURL = _config["GetTokenURL"].ToString();
        //    string GetPostListURL = _config["GetPostListURL"].ToString();

        //    //打API
        //    var client2 = new RestClient(GetTokenURL);
        //    var request2 = new RestRequest(Method.POST);
        //    request2.AddHeader("content-type", "application/json");
        //    request2.AddJsonBody(new { Account = Account, Password = Password });
        //    var response2 = client2.Execute(request2);

        //    var Token = JsonConvert.DeserializeObject<ResData<string>>(response2.Content);


        //    //打API
        //    var client = new RestClient(GetPostListURL);
        //    var request = new RestRequest(Method.POST);
        //    request.AddHeader("content-type", "application/json");
        //    request.AddHeader("Token", Token.Data);

        //    request.AddJsonBody(new { startTime = dateTime_S.ToString("yyyy-MM-dd") + " 12:00:00", endTime = dateTime_E.ToString("yyyy-MM-dd") + " 12:00:00" });
        //    var response = client.Execute(request);
        //    //打API

        //    //    var response = new RestRequest(Method.POST);
        //    //request.AddParameter("startTime", dateTime.ToString("yyyy-MM-dd") + " 18:00:00");//開始
        //    //request.AddParameter("endTime", dateTime.AddDays(-1).ToString("yyyy-MM-dd") + " 18:00:00");//結束
        //    //var response = client.Post<ResData<List<GetPostList_Res>>>(request);
        //    if (response.StatusCode == HttpStatusCode.OK)
        //    {
        //        var keyResponse = JsonConvert.DeserializeObject<ResData<List<GetPostList_Res>>>(response.Content);

        //        WeightList weightList = new WeightList();
        //        weightList.c1 = 0;
        //        weightList.c2 = 0;
        //        weightList.c3 = 0;
        //        weightList.c4 = 0;
        //        weightList.c5 = 0;
        //        weightList.c6 = 0;
        //        weightList.c7 = 0;
        //        weightList.c8 = 0;
        //        weightList.c9 = 0;


        //        MemoryStream stream = new MemoryStream();
        //        StreamWriter writer = new StreamWriter(stream, Encoding.UTF8);
        //        foreach (var item in keyResponse.Data)
        //        {
        //            int w;
        //            if (int.TryParse(item.Weight, out w))
        //            {
        //                if (w <= 100)
        //                {
        //                    weightList.c1++;
        //                }
        //                if (w > 100 && w <= 200)
        //                {
        //                    weightList.c2++;
        //                }
        //                if (w > 200 && w <= 300)
        //                {
        //                    weightList.c3++;
        //                }
        //                if (w > 300 && w <= 400)
        //                {
        //                    weightList.c4++;
        //                }
        //                if (w > 400 && w <= 5000)
        //                {
        //                    weightList.c5++;
        //                }
        //                if (w > 5000 && w <= 10000)
        //                {
        //                    weightList.c6++;
        //                }
        //                if (w > 10001 && w <= 15000)
        //                {
        //                    weightList.c7++;
        //                }
        //                if (w > 15000 && w <= 20000)
        //                {
        //                    weightList.c8++;
        //                }
        //                if (w > 20000)
        //                {
        //                    weightList.c9++;
        //                }

        //            }

        //        }


        //        List<Weight> wt = new List<Weight>();

        //        wt.Add(new Weight() { Name = "小1", Count = weightList.c1 });
        //        wt.Add(new Weight() { Name = "小2", Count = weightList.c2 });
        //        wt.Add(new Weight() { Name = "小3", Count = weightList.c3 });
        //        wt.Add(new Weight() { Name = "小4", Count = weightList.c4 });
        //        wt.Add(new Weight() { Name = "裹1", Count = weightList.c5 });
        //        wt.Add(new Weight() { Name = "裹2", Count = weightList.c6 });
        //        wt.Add(new Weight() { Name = "裹3", Count = weightList.c7 });
        //        wt.Add(new Weight() { Name = "裹4", Count = weightList.c8 });
        //        wt.Add(new Weight() { Name = "超", Count = weightList.c9});
        //        wt.Add(new Weight() { Name = "代收", Count = weightList.c10 });



        //        writer.Flush();
        //        stream.Position = 0;

        //        return File(stream, "text/csv", dateTime.ToString("yyyyMMdd") + "總數" + ".csv");
        //    }
        //    else
        //    {
        //        // _logger.LogInformation("檢查URL   " + _apiURL);
        //    }
        //    return null;
        //}

        public class PostLabel_Res
        {
            public string Type { get; set; }
            public byte[] base64 { get; set; }
        }
        public class checkcheck
        {

            public string CheckNumber { get; set; }//裹3
            public string Weight { get; set; }//裹4
        }

        [HttpPost]
        public ActionResult check2([FromBody] checkcheck check)
        {

            string CheckNumber = check.CheckNumber;

            string Weight = check.Weight;
            if (CheckNumber == null)
            {
                return Json(new { errorMsg = "託運單號有誤，請確認" });
                // throw new Exception();
            }
            if (CheckNumber.Length < 10)
            {
                return Json(new { errorMsg = "託運單號有誤，請確認" });
            }
            if (!long.TryParse(CheckNumber, out long c))
            {
                return Json(new { errorMsg = "託運單號有誤，請確認" });
            }
            if (Weight == null)
            {
                return Json(new { errorMsg = "重量有誤，請確認" });
                //  throw new Exception();
            }
            if (!long.TryParse(Weight, out long w))
            {
                return Json(new { errorMsg = "重量有誤，請確認" });
                //  throw new Exception();
            }

            string logpath = @"D:\\log\\";
            string logpathname = @"log.txt";
            string fullpath = logpath + DateTime.Now.ToString("yyyyMMdd") + "_" + logpathname;


            string Account = _config["DataSource:Account"].ToString();
            string Password = _config["DataSource:Password"].ToString();
            string PrintMachineName = _config["PrintMachineName"].ToString();
            string DotMatrixPrintMachineName = _config["DotMatrixPrintMachineName"].ToString();
            if (!Directory.Exists(logpath))
            {
                Directory.CreateDirectory(logpath);
            }
            if (!System.IO.File.Exists(fullpath))
            {
                var file = System.IO.File.Create(fullpath);
                file.Close();
            }
            //  System.IO.File.AppendAllText(fullpath, DateTime.Now.ToString() + "\r\n" + "-------------開始-----------" + "\r\n");

            string Printer3 = PrintMachineName;
            //   string semiUrl = "https://erpground.fs-express.com.tw/DeliveryRequest/PrintLTReelLabel2_0_post?ids={0}";
            string GetPostLabelURL = _config["GetPostLabelURL"].ToString();
            string GetTokenURL = _config["GetTokenURL"].ToString();

            //打API
            var client2 = new RestClient(GetTokenURL);
            var request2 = new RestRequest(Method.POST);
            request2.AddHeader("content-type", "application/json");
            request2.AddJsonBody(new { Account = Account, Password = Password });
            var response2 = client2.Execute(request2);

            var Token = JsonConvert.DeserializeObject<ResData<string>>(response2.Content);


            //打API
            var client = new RestClient(GetPostLabelURL);
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("Token", Token.Data);

            request.AddJsonBody(new { CheckNumber = CheckNumber, Weight = Weight });
            var response = client.Execute(request);

            try
            {
                var keyResponse = JsonConvert.DeserializeObject<ResData<PostLabel_Res>>(response.Content);

                if (keyResponse == null)
                {
                    throw new Exception();
                }

                if (keyResponse.Status != ResponseCodeEnum.Success)
                {
                    throw new Exception(keyResponse.Message);
                }

                byte[] bytesDownload = keyResponse.Data.base64;

                if (bytesDownload.Length > 0)
                {
                    switch (keyResponse.Data.Type)
                    {
                        case "1":
                            var pdfPrint = new PdfPrint("Winjet Inc.", "5b5d26533151575650245e5a412b57");
                            pdfPrint.PrinterName = Printer3;
                            pdfPrint.PageAutoSize = true;
                            pdfPrint.AllowCustomSize = true;

                            pdfPrint.Print(bytesDownload, "", true);

                            break;
                        case "2":

                            var pdfPrint2 = new PdfPrint("Winjet Inc.", "5b5d26533151575650245e5a412b57");
                            pdfPrint2.PrinterName = DotMatrixPrintMachineName;

                            pdfPrint2.AllowCustomSize = false;
                            pdfPrint2.CheckPrinterExistence = true;
                            pdfPrint2.Collate = false;
                            pdfPrint2.Copies = 0;
                            pdfPrint2.DuplexType = 0;
                            pdfPrint2.InverseForm = false;

                            pdfPrint2.IsContentCentered = true;
                            pdfPrint2.IsAutoRotate = true;

                            pdfPrint2.IsLandscape = false;
                            pdfPrint2.PageAutoSize = false;
                            pdfPrint2.Pages = string.Empty;


                            pdfPrint2.IsContentCentered = false;
                            pdfPrint2.IsAutoRotate = false;

                            pdfPrint2.IsLandscape = false;
                            pdfPrint2.PageAutoSize = true;
                            pdfPrint2.Pages = string.Empty;


                            var printerSettings = new PrinterSettings();

                            foreach (PaperSize aa in printerSettings.PaperSizes)
                            {
                                if (PaperKind.A4 == aa.Kind)
                                {
                                    pdfPrint2.PaperSize = aa;
                                }
                            }
                            pdfPrint2.PrintInColor = false;
                            pdfPrint2.RangeType = RangeTypes.EvenAndOdd;
                            pdfPrint2.SettingDialog = false;
                            pdfPrint2.UseOldPrintingSystem = false;

                            pdfPrint2.Rotation = PdfRotation.Rotate0;

                            pdfPrint2.Scale = ScaleTypes.Shrink;
                            pdfPrint2.Print(bytesDownload, "", true);

                            break;

                        default:
                            break;
                    }


                }

            }
            catch (Exception e)
            {
                System.IO.File.AppendAllText(fullpath, DateTime.Now.ToString() + "\r\n" + "CheckNumber : " + CheckNumber + "\r\n" + "失敗 :" + e.Message + "\r\n");
                // return Redirect("Error");
                return Json(new { errorMsg = "系統異常" });
            }
            return Json("");
        }


        [HttpPost]
        public ActionResult check(string CheckNumber, string Weight)
        {

            if (CheckNumber == null)
            {
                return Json(new { errorMsg = "託運單號有誤，請確認" });
                // throw new Exception();
            }
            if (CheckNumber.Length < 10)
            {
                return Json(new { errorMsg = "託運單號有誤，請確認" });
            }
            if (!long.TryParse(CheckNumber, out long c))
            {
                return Json(new { errorMsg = "託運單號有誤，請確認" });
            }
            if (Weight == null)
            {
                return Json(new { errorMsg = "重量有誤，請確認" });
                //  throw new Exception();
            }
            if (!long.TryParse(Weight, out long w))
            {
                return Json(new { errorMsg = "重量有誤，請確認" });
                //  throw new Exception();
            }

            string logpath = @"D:\\log\\";
            string logpathname = @"log.txt";
            string fullpath = logpath + DateTime.Now.ToString("yyyyMMdd") + "_" + logpathname;


            string Account = _config["DataSource:Account"].ToString();
            string Password = _config["DataSource:Password"].ToString();
            string PrintMachineName = _config["PrintMachineName"].ToString();
            string DotMatrixPrintMachineName = _config["DotMatrixPrintMachineName"].ToString();
            if (!Directory.Exists(logpath))
            {
                Directory.CreateDirectory(logpath);
            }
            if (!System.IO.File.Exists(fullpath))
            {
                var file = System.IO.File.Create(fullpath);
                file.Close();
            }
            //  System.IO.File.AppendAllText(fullpath, DateTime.Now.ToString() + "\r\n" + "-------------開始-----------" + "\r\n");

            string Printer3 = PrintMachineName;
            //   string semiUrl = "https://erpground.fs-express.com.tw/DeliveryRequest/PrintLTReelLabel2_0_post?ids={0}";
            string GetPostLabelURL = _config["GetPostLabelURL"].ToString();
            string GetTokenURL = _config["GetTokenURL"].ToString();

            //打API
            var client2 = new RestClient(GetTokenURL);
            var request2 = new RestRequest(Method.POST);
            request2.AddHeader("content-type", "application/json");
            request2.AddJsonBody(new { Account = Account, Password = Password });
            var response2 = client2.Execute(request2);

            var Token = JsonConvert.DeserializeObject<ResData<string>>(response2.Content);


            //打API
            var client = new RestClient(GetPostLabelURL);
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("Token", Token.Data);

            request.AddJsonBody(new { CheckNumber = CheckNumber, Weight = Weight });
            var response = client.Execute(request);

            try
            {
                var keyResponse = JsonConvert.DeserializeObject<ResData<PostLabel_Res>>(response.Content);

                if (keyResponse == null)
                {
                    throw new Exception();
                }

                if (keyResponse.Status != ResponseCodeEnum.Success)
                {
                    throw new Exception(keyResponse.Message);
                }

                byte[] bytesDownload = keyResponse.Data.base64;

                if (bytesDownload.Length > 0)
                {
                    switch (keyResponse.Data.Type)
                    {
                        case "1":
                            var pdfPrint = new PdfPrint("Winjet Inc.", "5b5d26533151575650245e5a412b57");
                            pdfPrint.PrinterName = Printer3;
                            pdfPrint.PageAutoSize = true;
                            pdfPrint.AllowCustomSize = true;

                            pdfPrint.Print(bytesDownload, "", true);

                            break;
                        case "2":

                            var pdfPrint2 = new PdfPrint("Winjet Inc.", "5b5d26533151575650245e5a412b57");
                            pdfPrint2.PrinterName = DotMatrixPrintMachineName;

                            pdfPrint2.AllowCustomSize = false;
                            pdfPrint2.CheckPrinterExistence = true;
                            pdfPrint2.Collate = false;
                            pdfPrint2.Copies = 0;
                            pdfPrint2.DuplexType = 0;
                            pdfPrint2.InverseForm = false;

                            pdfPrint2.IsContentCentered = true;
                            pdfPrint2.IsAutoRotate = true;

                            pdfPrint2.IsLandscape = false;
                            pdfPrint2.PageAutoSize = false;
                            pdfPrint2.Pages = string.Empty;
                        

                            pdfPrint2.IsContentCentered = false;
                            pdfPrint2.IsAutoRotate = false;

                            pdfPrint2.IsLandscape = false;
                            pdfPrint2.PageAutoSize = true;
                            pdfPrint2.Pages = string.Empty;


                            var printerSettings = new PrinterSettings();

                            foreach (PaperSize aa in printerSettings.PaperSizes)
                            {
                                if (PaperKind.A4 == aa.Kind)
                                {
                                    pdfPrint2.PaperSize = aa;
                                }
                            }
                            pdfPrint2.PrintInColor = false;
                            pdfPrint2.RangeType = RangeTypes.EvenAndOdd;
                            pdfPrint2.SettingDialog = false;
                            pdfPrint2.UseOldPrintingSystem = false;

                            pdfPrint2.Rotation = PdfRotation.Rotate0;

                            pdfPrint2.Scale = ScaleTypes.Shrink;
                            pdfPrint2.Print(bytesDownload, "", true);

                            break;

                        default:
                            break;
                    }


                }

            }
            catch (Exception e)
            {
                System.IO.File.AppendAllText(fullpath, DateTime.Now.ToString() + "\r\n" + "CheckNumber : " + CheckNumber + "\r\n" + "失敗 :" + e.Message + "\r\n");
                // return Redirect("Error");
                return Json(new { errorMsg = "系統異常" });
            }
            // System.IO.File.AppendAllText(fullpath, DateTime.Now.ToString() + "\r\n" + "--------結束-----" + "\r\n");
            return Json("");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            // return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            return View();
        }

        public IActionResult Report()
        {
            return View();
        }

    }
}
