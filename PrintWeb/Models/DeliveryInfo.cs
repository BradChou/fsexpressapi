﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintWeb.Models
{
    public class DeliveryInfo
    {
        public string CheckNumber { get; set; }
        public string Weight { get; set; }
    }
}
