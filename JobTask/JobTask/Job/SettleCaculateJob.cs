﻿using BLL.Model.ScheduleAPI.Res.Contract;
using Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;


namespace JobTask.Job
{
    [DisallowConcurrentExecution]
    class SettleCaculateJob : IJob
    {
        private readonly ILogger<SettleCaculateJob> _logger;
        private readonly IConfiguration _config;
        private readonly string _apikey;
        private readonly string _apiURL;
        public SettleCaculateJob(ILogger<SettleCaculateJob> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
            _apikey = _config["apikey"];
            _apiURL = _config["API:SettleCaculateJob"];
        }

        public Task Execute(IJobExecutionContext context)
        {



            //打API

            var client = new RestClient(_apiURL);
            var request = new RestRequest(Method.POST);
            request.AddHeader("apikey", _apikey);
            var response = client.Post<ResData<SettleCaculate_Res>>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                _logger.LogInformation($@"{DateTime.Now}");
                if (response.Data != null)
                {
                    _logger.LogInformation($"{response.Data.Data.SettleNum} rows have been caculated.\n");
                    
                }
                else
                {
                    _logger.LogInformation(response.ErrorMessage);
                }

            }
            else
            {
                _logger.LogInformation("Error.Please check URL   " + _apiURL);
            }




            return Task.CompletedTask;
        }
    }
}
