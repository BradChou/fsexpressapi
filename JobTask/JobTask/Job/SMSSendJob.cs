﻿using BLL.Model.ScheduleAPI.Res.PostOffice;
using Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JobTask.Job
{
    [DisallowConcurrentExecution]
    public class SMSSendJob : IJob
    {
        private readonly ILogger<SMSSendJob> _logger;
        private readonly IConfiguration _config;
        private readonly string _apikey;
        private readonly string _apiURL;
        public SMSSendJob(ILogger<SMSSendJob> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
            _apikey = _config["apikey"];
            _apiURL = _config["API:SMSSendJob"]; 
        }

        public Task Execute(IJobExecutionContext context)
        {

            //打API
            var client = new RestClient(_apiURL);
            var request = new RestRequest( Method.POST);
             request.AddHeader("apikey", _apikey);
            var response = client.Post<ResData>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
             //   _logger.LogInformation($@"{DateTime.Now}");
                if (response.Data != null)
                {
                   // _logger.LogInformation($@"更新!--{response.Data.Data.count}個");
                }
                else
                {
                    _logger.LogInformation(response.ErrorMessage);
                }
            }
            else 
            {
                _logger.LogInformation("檢查URL   "+ _apiURL);
            }

            return Task.CompletedTask;
        }
    }
}
