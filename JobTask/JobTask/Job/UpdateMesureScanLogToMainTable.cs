﻿using BLL.Model.ScheduleAPI.Res.Measure;
using Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JobTask.Job
{
    [DisallowConcurrentExecution]
    public class UpdateMesureScanLogToMainTable : IJob
    {
        private readonly ILogger<UpdateMesureScanLogToMainTable> _logger;
        private readonly IConfiguration _config;
        private readonly string _apikey;
        private readonly string _apiURL;
        public UpdateMesureScanLogToMainTable(ILogger<UpdateMesureScanLogToMainTable> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
            _apikey = _config["apikey"];
            _apiURL = _config["API:UpdateMesureScanLogToMainTable"]; 
        }

        public Task Execute(IJobExecutionContext context)
        {



            //打API

            var client = new RestClient(_apiURL);
            var request = new RestRequest( Method.POST);
             request.AddHeader("apikey", _apikey);
            var response = client.Post<ResData<UpdateMesureScanLogToMainTable_Res>>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                _logger.LogInformation($@"{DateTime.Now}");
                if (response.Data != null)
                {
                    _logger.LogInformation($"{response.Data.Data.NumToScanLog} rows have been insert into ttDeliveryScanLog successfully.\n");
                    _logger.LogInformation($"{response.Data.Data.NumToDelieveryRequest} rows have been insert into tcDeliveryRequests successfully.\n");
                    _logger.LogInformation($"{response.Data.Data.NumToExclude} rows have been insert into MeasureScanLog_Exclude successfully.");
                }
                else
                {
                    _logger.LogInformation(response.ErrorMessage);
                }

            }
            else 
            {
                _logger.LogInformation("Error.Please check URL   "+ _apiURL);
            }




            return Task.CompletedTask;
        }
    }
}
