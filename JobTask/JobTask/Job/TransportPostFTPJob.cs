﻿using BLL.Model.ScheduleAPI.Res.PostOffice;
using Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JobTask.Job
{
   
    [DisallowConcurrentExecution]
    public class TransportPostFTPJob : IJob
    {
        private readonly ILogger<TransportPostFTPJob> _logger;
        private readonly IConfiguration _config;
        private readonly string _apikey;
        private readonly string _apiURL;
        public TransportPostFTPJob(ILogger<TransportPostFTPJob> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
            _apikey = _config["apikey"];
            _apiURL = _config["API:TransportPostFTPJob"];
        }

        public Task Execute(IJobExecutionContext context)
        {

            //打API
            var client = new RestClient(_apiURL);
            var request = new RestRequest(Method.POST);
            request.AddHeader("apikey", _apikey);
            var response = client.Post<ResData<TransportPostFTPJob_Res>>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                _logger.LogInformation($@"{DateTime.Now}");
                if (response.Data != null)
                {
                    _logger.LogInformation($@"更新!--{response.Data.Data.StatusCount}個");
                    _logger.LogInformation($@"更新!--{response.Data.Data.IrregularCount}個");
                    _logger.LogInformation($@"更新!--{response.Data.Data.OrderCount}個");
                    _logger.LogInformation($@"更新!--{response.Data.Data.ResultCount}個");
                }
                else
                {
                    _logger.LogInformation(response.ErrorMessage);
                }
            }
            else
            {
                _logger.LogInformation("檢查URL   " + _apiURL);
            }

            return Task.CompletedTask;
        }
    }
}
