﻿using BLL.Model.ScheduleAPI.Res.Measure;
using Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
namespace JobTask.Job
{
    public class MesureScanLogAbnormalJob : IJob
    {
        private readonly ILogger<MesureScanLogAbnormalJob> _logger;
        private readonly IConfiguration _config;
        private readonly string _apikey;
        private readonly string _apiURL;
        public MesureScanLogAbnormalJob(ILogger<MesureScanLogAbnormalJob> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
            _apikey = _config["apikey"];
            _apiURL = _config["API:MesureScanLogAbnormalJob"];
        }

        public Task Execute(IJobExecutionContext context)
        {



            //打API

            var client = new RestClient(_apiURL);
            var request = new RestRequest(Method.POST);
            request.AddHeader("apikey", _apikey);
            var response = client.Post<ResData<MesureScanLogAbnormalJob_Res>>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                _logger.LogInformation($@"{DateTime.Now}");
                if (response.Data != null)
                {
                    _logger.LogInformation($"{response.Data.Data.NumToLogTable} rows have been insert into [cbm_data_log] successfully.\n");
                                   }
                else
                {
                    _logger.LogInformation(response.ErrorMessage);
                }

            }
            else
            {
                _logger.LogInformation("Error.Please check URL   " + _apiURL);
            }




            return Task.CompletedTask;
        }
    }
}
