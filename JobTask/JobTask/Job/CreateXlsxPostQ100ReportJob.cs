﻿using BLL.Model.ScheduleAPI.Res.PostOffice;
using Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobTask.Job
{
    [DisallowConcurrentExecution]
    public class CreateXlsxPostQ100ReportJob : IJob
    {
        private readonly ILogger<CreateXlsxPostQ100ReportJob> _logger;
        private readonly IConfiguration _config;
        private readonly string _apikey;
        private readonly string _apiURL;

        public CreateXlsxPostQ100ReportJob(ILogger<CreateXlsxPostQ100ReportJob> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
            _apikey = _config["apikey"];
            _apiURL = _config["API:CreateXlsxPostQ100ReportJob"];
        }

        public Task Execute(IJobExecutionContext context)
        {

            //打API
            var client = new RestClient(_apiURL);
            var request = new RestRequest(Method.POST);
            request.AddHeader("apikey", _apikey);
            var response = client.Post<ResData<CreateXlsxPostQ100ReportJob_Res>>(request);
            if (response.StatusCode != System.Net.HttpStatusCode.NotFound)
            {
                _logger.LogInformation($@"{DateTime.Now}");
                if (response.Data != null)
                {
                   
                    _logger.LogInformation($@"Q100產出報表!--{response.Data.Data.count}筆");

                    
                }
                else
                {
                    _logger.LogInformation(response.ErrorMessage);
                }
            }
            else
            {
                _logger.LogInformation("檢查URL   " + _apiURL);
            }

            return Task.CompletedTask;
        }

    }
}
