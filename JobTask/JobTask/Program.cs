﻿using JobTask.Job;
using Microsoft.Extensions.Hosting;
using Quartz;
using System;

namespace JobTask
{
    class Program
    {

        static void Main(string[] args)
        {
            // trigger async evaluation
            // RunProgram().GetAwaiter().GetResult();

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
         Host.CreateDefaultBuilder(args)
             .ConfigureServices((hostContext, services) =>
             {
                 services.AddQuartz(q =>
                 {
                     q.UseMicrosoftDependencyInjectionScopedJobFactory();

                     // Register the job, loading the schedule from configuration
                     //    q.AddJobAndTrigger<HelloWorldJob>(hostContext.Configuration);


                     
                     q.AddJobAndTrigger<UpdateDeliveryStatusPublicToDeliveryScanLogJob>(hostContext.Configuration);
                   //  q.AddJobAndTrigger<FilterDeliveryStatusPublicJob>(hostContext.Configuration);
                     q.AddJobAndTrigger<MesureScanLogFilterJob>(hostContext.Configuration);
                     q.AddJobAndTrigger<UpdateMesureScanLogToMainTable>(hostContext.Configuration);
                     q.AddJobAndTrigger<SendJob>(hostContext.Configuration);
                     q.AddJobAndTrigger<SMSSendJob>(hostContext.Configuration);

                     
                     q.AddJobAndTrigger<MesureScanLogAbnormalJob>(hostContext.Configuration);


                
                     q.AddJobAndTrigger<GetPostStatusNASToDBJob>(hostContext.Configuration);
                     q.AddJobAndTrigger<ParsingPostStatusJob>(hostContext.Configuration);
                     q.AddJobAndTrigger<FilterPostStatusJob>(hostContext.Configuration);
                     q.AddJobAndTrigger<InsertPostStatusJob>(hostContext.Configuration);
                     q.AddJobAndTrigger<UploadPostOrderJob>(hostContext.Configuration);
                     q.AddJobAndTrigger<GetPostResultNASToDBJob>(hostContext.Configuration);
                     q.AddJobAndTrigger<GetPostIrregularNASToDBJob>(hostContext.Configuration);
                     q.AddJobAndTrigger<TransportPostFTPJob>(hostContext.Configuration);
                     
                     q.AddJobAndTrigger<CreateXlsxPostQ100ReportJob>(hostContext.Configuration);
                     
                     q.AddJobAndTrigger<CBMDetailFilterJob>(hostContext.Configuration);

                     q.AddJobAndTrigger<DeliveryRequestsUpdateSDMDJob>(hostContext.Configuration);
                     
                     //  q.AddJobAndTrigger<SettleCaculateJob>(hostContext.Configuration);

                     // q.AddJobAndTrigger<InsertMoneySettleDetailJob>(hostContext.Configuration);


                     //舊郵局 目前不使用
                     // q.AddJobAndTrigger<GetErrorPostFTPXMLFileJob>(hostContext.Configuration);
                     //q.AddJobAndTrigger<GetPostFTPXMLFileJob>(hostContext.Configuration);
                     //q.AddJobAndTrigger<ParsingPostXmlJob>(hostContext.Configuration);
                     // q.AddJobAndTrigger<FilterPostStateJob>(hostContext.Configuration);
                     //   q.AddJobAndTrigger<UpdatePostStateToDeliveryScanLogJob>(hostContext.Configuration);

                 });

                 services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);
             });

    }
}
