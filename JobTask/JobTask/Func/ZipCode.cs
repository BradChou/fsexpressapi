﻿using Newtonsoft.Json;
using RestSharp;
using System.Data;
using System.Net;

namespace JobTask.Func
{
    class ZipCode
    {
        /// <summary>
        /// 取得地址郵遞區號
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static string GetZipCode(string address)
        {
            var zipcode = string.Empty;
            string apiURL = "https://zip5.5432.tw/zip5json.py";
            var client = new RestClient(apiURL);
            var request = new RestRequest("{adrs}", Method.GET);
            //request.AddParameter("adrs", address, ParameterType.UrlSegment);//處理可能出現的保留字
            request.AddParameter("adrs", address);
            var response = client.Get<object>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var json = JsonConvert.DeserializeObject<zipCode>(response.Content);
                //DataTable zipdata = JsonConvert.DeserializeObject<DataTable>("[" + response.Content.Trim() + "]");
                //zipcode = zipdata.Rows[0]["zipcode6"].ToString();
                zipcode = json.zipcode;
            }
            return zipcode;
        }
        class checkNumber
        {
            public string check_number { get; set; }
            public string Message { get; set; }

        }
        class zipCode
        {
            public string zipcode6 { get; set; }
            public string dataver6 { get; set; }
            public string adrs { get; set; }
            public string new_adrs6 { get; set; }
            public string new_adrs2 { get; set; }
            public string new_adrs6_2 { get; set; }
            public string new_adrs { get; set; }
            public string dataver { get; set; }
            public string zipcode { get; set; }

        }
        //public static string PostTest(GetDeliveryInfo_Req req)
        //{
        //    string contentType = "application/json"; //Content-Type
        //    var result = string.Empty;
        //    string apiURL = string.Format(@"https://apimaster.fs-express.com.tw/Consignment/GetDeliveryInfo");
        //    var client = new RestClient(apiURL);
        //    var request = new RestRequest(Method.POST);
        //    request.Timeout = 10000;
        //    //request.AddHeader("Cache-Control", "no-cache");          
        //    //request.AddParameter(contentType, content, ParameterType.RequestBody);
        //    //var body = "{  \"check_number\": \"string\"}";
        //    //request.AddParameter("application/json", body, ParameterType.RequestBody);
        //    //request.AddJsonBody(new { check_number = "foo" }); 
        //    request.RequestFormat = DataFormat.Json;
        //    //request.AddBody(new { A = "foo", B = "bar" }); // uses JsonSerializer
        //    //request.AddParameter("check_number",req.check_number); 
        //    //request.AddHeader("content-type", "application/x-www-form-urlencoded");

        //    request.AddBody(req);
        //    var response = client.Execute(request);
        //    if (response.StatusCode == HttpStatusCode.OK)
        //    {
        //        var json = JsonConvert.DeserializeObject<checkNumber>(response.Content);
        //        result = json.Message.ToString();
        //    }
        //    return result;
        //}
    }
}
