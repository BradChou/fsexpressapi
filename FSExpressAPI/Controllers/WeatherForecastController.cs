﻿using BLL.Model.FSExpressAPI.Req.Auth;
using Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;

namespace FSExpressAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        /// <summary>
        /// 郵局標籤列印
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
       
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> Test(GetToken_Req req)
        {



            ResData<string> resData = new ResData<string>();

            resData.Data = DateTime.Now.ToString("yyyyMMddHHmmssfffff");

            if (req.Account == "123") 
            {
                throw new MyException(ResponseCodeEnum.Error);
            }

            return resData;
        }
        }
}
