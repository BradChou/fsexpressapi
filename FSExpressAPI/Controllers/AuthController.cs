﻿using BLL.Model.FSExpressAPI.Model;
using BLL.Model.FSExpressAPI.Req.Auth;
using Common;
using FSExpressAPI.Attribute;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;
using BLL;


namespace FSExpressAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {

        private readonly IConfiguration _configuration;

        private readonly  AuthServices _authServices;


        public AuthController(IConfiguration configuration, AuthServices authServices)
        {
            _configuration = configuration;
            _authServices = authServices;

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetToken(GetToken_Req req)
        {

            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.Account))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.Password))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            //判斷蝴蝶帳密是否正確
            
            //合法帳號 先寫死
            List<AccountInfo> AccountInfos = new List<AccountInfo>();
           // AccountInfos.Add(new AccountInfo() { Account = "YHD001", Password = "4eb7a9f2g02k" });
            AccountInfos.Add(new AccountInfo() { Account = "PP091", Password = "1qaz@WSX" });
            AccountInfos.Add(new AccountInfo() { Account = "PP092", Password = "2wsx#EDC" });
            AccountInfos.Add(new AccountInfo() { Account = "FSE", Password = "Passw0rd" });

            AccountInfos.Add(new AccountInfo() { Account = "29A", Password = "29A29A" });
            AccountInfos.Add(new AccountInfo() { Account = "29B", Password = "29B29B" });
            AccountInfos.Add(new AccountInfo() { Account = "PP520", Password = "0000" });
            AccountInfos.Add(new AccountInfo() { Account = "PP920", Password = "0000" });

            //核對蝴蝶帳密
            var Accounts = await _authServices.GetAccountsInfo(req.Account, req.Password);


            var ContainsInfo = AccountInfos.Where(x => x.Account == req.Account && x.Password == req.Password).FirstOrDefault();

            if (Accounts ==null && ContainsInfo == null) 
            {
                throw new MyException(ResponseCodeEnum.WrongAccountOrPassword);
            }


            //    if (req.Account == "FSE" || req.Account == "YHD001")
            //{
            //    if (ContainsInfo == null)
            //    {
            //        throw new MyException(ResponseCodeEnum.AuthorizationUserError);
            //    }
            //}
            //else if (Accounts == null)
            //{
            //    throw new MyException(ResponseCodeEnum.WrongAccountOrPassword);
            //}



            ResData<string> resData = new ResData<string>();
            //bool isValid = _userService.IsValidUserInformation(data);

            var tokenString = GenerateJwtToken(req.Account);

            resData.Data = tokenString;

            return Ok(resData);

            // return BadRequest("Please pass the valid Username and Password");
        }



        /// <summary>
        /// Generate JWT Token after successful login.
        /// </summary>
        /// <param name="Account"></param>
        /// <returns></returns>
        private string GenerateJwtToken(string Account)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration["Jwt:key"]);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("Account", Account) }),
                Expires = DateTime.Now.AddMinutes(int.Parse(_configuration["Jwt:Extime"])),
                Issuer = _configuration["Jwt:Issuer"],
                //  Audience = _configuration["Jwt:Audience"],
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
