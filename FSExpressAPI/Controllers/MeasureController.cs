﻿using BLL;
using BLL.Model.FSExpressAPI.Req.Measure;
using BLL.Model.FSExpressAPI.Res.Measure;
using Common;
using DAL.Model.Condition;
using FSExpressAPI.Attribute;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;

namespace FSExpressAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MeasureController : ControllerBase
    {

        private MeasureServices _measureServices;

        public MeasureController(MeasureServices measureServices)
        {
            _measureServices = measureServices;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_Req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertShipmentSize")]
        public async Task<ActionResult<ResData>> InsertShipmentSize(InsertShipmentSize_Req _Req)
        {
            ResData resData = new ResData();
            // InsertShipmentSize_Res _Res = new InsertShipmentSize_Res();
            //  _Res.isSuccess = false;

            if (_Req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(_Req.DataSource))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(_Req.Status))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (_Req.ScanTime == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            try
            {
                await _measureServices.InsertMesureScanLog(_Req);
                return Ok(resData);
            }
            catch (Exception e)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
        }

        /// <summary>
        /// 丈量查詢
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetShipmentSize(GetShipmentSize_Req req)
        {
            ResData<GetShipmentSize_Res> resData = new ResData<GetShipmentSize_Res>();
            //檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.CheckNumber))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            resData.Data = await _measureServices.GetShipmentSize(req);
            return Ok(resData);
        }

        /// <summary>
        /// 新增丈量資訊
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> InsertCBMInfo(InsertCBMInfo_Req req)
        {
            ResData resData = new ResData();
            //檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            try
            {
                if (!Enum.IsDefined(typeof(CBMDetailLogComeFromEnum), int.Parse(req.ComeFrom)))
                {
                    throw new MyException(ResponseCodeEnum.ParameterError);
                }

            }
            catch (Exception)
            {

                throw new MyException(ResponseCodeEnum.ParameterError);
            }


            if (req.Height == null && req.Width == null && req.Length == null)
            {
                if (string.IsNullOrEmpty(req.CBM))
                {
                    throw new MyException(ResponseCodeEnum.ParameterError);
                }
                else
                {
                    if (!req.CBM.Equals("S060") &&
                    !req.CBM.Equals("S090") &&
                    !req.CBM.Equals("S110") &&
                    !req.CBM.Equals("S120") &&
                    !req.CBM.Equals("S150"))
                    {
                        throw new MyException(ResponseCodeEnum.ParameterError);
                    }
                }

            }
            else if (req.Height != null && req.Width != null && req.Length != null)
            {
                if (!string.IsNullOrEmpty(req.CBM))
                {
                    if (!req.CBM.Equals("S060") &&
                !req.CBM.Equals("S090") &&
                !req.CBM.Equals("S110") &&
                !req.CBM.Equals("S120") &&
                !req.CBM.Equals("S150"))
                    {
                        throw new MyException(ResponseCodeEnum.ParameterError);
                    }
                }
            }
            else
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            await _measureServices.InsertCBMInfo(req);
            return Ok(resData);
        }
    }
}
