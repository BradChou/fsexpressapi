﻿using BLL;
using BLL.Model.FSExpressAPI.Req.PostOffice;
using BLL.Model.FSExpressAPI.Res.PostOffice;
using Common;
using FSExpressAPI.Attribute;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServicePostStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;
using static ServicePostStatus.PSTTP_MailQuerySoapClient;

namespace FSExpressAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PostOfficeController : ControllerBase
    {
        private PostOfficeServices _postOfficeServices;


        public PostOfficeController(PostOfficeServices postOfficeServices)
        {
            _postOfficeServices = postOfficeServices;
        }

        /// <summary>
        /// 郵局清單
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetPostList(GetPostList_Req req)
        {

            //檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (req.StartTime == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (req.EndTime == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            ResData<List<GetPostList_Res>> resData = new ResData<List<GetPostList_Res>>();
            resData.Data = await _postOfficeServices.PostList(req);
            return Ok(resData);
        }

        /// <summary>
        /// 郵局貨態
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetPostStatus(GetPostStatus_Req req)
        {

            //檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (req.PostId == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
          
            ResData<List<GetPostStatus_Res>> resData = new ResData<List<GetPostStatus_Res>>();
            resData.Data = await _postOfficeServices.GetPostStatus(req);
            return Ok(resData);
        }

        /// <summary>
        /// 更新郵局所有貨態
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> UpdatePostStatus(UpdatePostStatus_Req req)
        {

            //檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (req.PostId == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            List<string> vs = new List<string>
            {
"03253330022174111004"

            };

            ResData<UpdatePostStatus_Res> resData = new ResData<UpdatePostStatus_Res>();

            foreach (var item in vs)
            {
                req.PostId = item;
                resData.Data = await _postOfficeServices.UpdatePostStatus(req);
            }
        //    resData.Data = await _postOfficeServices.UpdatePostStatus(req);
            
            
            return Ok(resData);
        }


    }
}
