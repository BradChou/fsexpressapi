﻿using BLL;
using BLL.Model.FSExpressAPI.Req.Print;
using BLL.Model.FSExpressAPI.Res.Print;
using Common;
using Common.Attribute;
using FSExpressAPI.Attribute;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;

namespace FSExpressAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PrintController : ControllerBase
    {
        private PrintServices _printServices;

        public PrintController(PrintServices printServices)
        {
            _printServices = printServices;
        }

        /// <summary>
        /// 郵局標籤列印
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetPostLabel(PostLabel_Req req)
        {
            //檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.CheckNumber))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.Weight))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            var Account = HttpContext.Items["Account"].ToString();

            ResData<PostLabel_Res> resData = new ResData<PostLabel_Res>();
            resData.Data = await _printServices.PostLabel(req, Account);
            return Ok(resData);
        }

        /// <summary>
        /// 郵局標籤列印2
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetPostLabel2(PostLabel_Req req)
        {
            //檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.CheckNumber))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.Weight))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            var Account = HttpContext.Items["Account"].ToString();

            ResData<PostLabel_Res> resData = new ResData<PostLabel_Res>();
            resData.Data = await _printServices.PostLabel2(req, Account);
            return Ok(resData);
        }


    }
}
