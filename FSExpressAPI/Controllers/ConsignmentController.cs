﻿
using BLL;
using BLL.Model.FSExpressAPI.Req.Consignment;
using BLL.Model.FSExpressAPI.Res.Consignment;
using Common;
using Common.Attribute;
using FSExpressAPI.Attribute;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;

namespace FSExpressAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ConsignmentController : ControllerBase
    {

        private ConsignmentServices _consignmentServices;


        public ConsignmentController(ConsignmentServices consignmentServices)
        {
            _consignmentServices = consignmentServices;
        }

        /// <summary>
        /// 新增託運單
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
      //  [Transactional]
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> InsertDelivery(InsertDelivery_Req req)
        {
            ResData<List<InsertDelivery_Res>> resData = new ResData<List<InsertDelivery_Res>>();

            //檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.ReceiveContact))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.ReceiveTel1))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.ReceiveAddress))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (req.PaymentMethod == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.SendContact))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.SendTel))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.SendAddress))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }

            if (req.CbmSize == null && req.MeasurementInfos == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (req.DeliveryMethod == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (req.receiptFlag == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (!string.IsNullOrEmpty(req.Arriveassigndate))
            {
                var b = DateTime.TryParseExact(req.Arriveassigndate, "yyyyMMdd", null, DateTimeStyles.None, out DateTime dateTime);
                if (!b)
                {
                    throw new MyException(ResponseCodeEnum.ParameterError);
                }
            }

            //數量要符合材積數量
            if (req.Pieces.HasValue && req.MeasurementInfos != null)
            {
                if (req.Pieces.Value != req.MeasurementInfos.Count())
                {
                    throw new MyException(ResponseCodeEnum.ParameterError);
                }
            }
            else if (req.Pieces.HasValue && req.MeasurementInfos == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            else if (!req.Pieces.HasValue && req.MeasurementInfos != null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            resData.Data = await _consignmentServices.InsertDelivery(req);
            return Ok(resData);
        }
        /// <summary>
        /// 新增託運單
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        //  [Transactional]
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> InsertDeliveryForBusiness(InsertDeliveryForBusiness_Req req)
        {
            ResData<List<InsertDeliveryForBusiness_Res>> resData = new ResData<List<InsertDeliveryForBusiness_Res>>();

            //檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.ReceiveContact))
            {
                throw new MyException(ResponseCodeEnum.Parameter_ReceiveContact_Error);
            }
            if (string.IsNullOrEmpty(req.ReceiveTel1))
            {
                throw new MyException(ResponseCodeEnum.Parameter_ReceiveTel1_Error);
            }
            if (string.IsNullOrEmpty(req.ReceiveAddress))
            {
                throw new MyException(ResponseCodeEnum.DeliveryReceiveAddressError);
            }
            if (req.PaymentMethod == null)
            {
                throw new MyException(ResponseCodeEnum.Parameter_PaymentMethod_Error);
            }

            if (req.CbmSize == null && req.MeasurementInfos == null)
            {
                throw new MyException(ResponseCodeEnum.Parameter_MeasurementInfos_Error);
            }
            if (req.DeliveryMethod == null)
            {
                throw new MyException(ResponseCodeEnum.Parameter_DeliveryMethod_Error);
            }
            if (req.receiptFlag == null)
            {
                throw new MyException(ResponseCodeEnum.Parameter_ReceiptFlag_Error);
            }

            if (!string.IsNullOrEmpty(req.Arriveassigndate))
            {
                var b = DateTime.TryParseExact(req.Arriveassigndate, "yyyyMMdd", null, DateTimeStyles.None, out DateTime dateTime);
                if (!b)
                {
                    throw new MyException(ResponseCodeEnum.Parameter_Arriveassigndate_Error);
                }
            }
            //數量要符合材積數量
            if (req.Pieces.HasValue && req.MeasurementInfos != null)
            {
                if (req.Pieces.Value != req.MeasurementInfos.Count())
                {
                    throw new MyException(ResponseCodeEnum.Parameter_PiecesMeasurementInfos);
                }
            }
            else if (req.Pieces.HasValue && req.MeasurementInfos == null)
            {
                throw new MyException(ResponseCodeEnum.Parameter_PiecesMeasurementInfos);
            }
            else if (!req.Pieces.HasValue && req.MeasurementInfos != null)
            {
                throw new MyException(ResponseCodeEnum.Parameter_PiecesMeasurementInfos);
            }


            resData.Data = await _consignmentServices.InsertDeliveryForBusiness(req);
            return Ok(resData);
        }
        /// <summary>
        /// 取得標籤(Base64)
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetTagImage(GetTagImage_Req req)
        {
            ResData<string> resData = new ResData<string>();
            resData.Data = await _consignmentServices.GetTagImage(req);
            return Ok(resData);
        }
        /// <summary>
        /// 退貨(逆物流)
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> ReverseDelivery(ReverseDelivery_Req req)
        {
            ResData<ReverseDelivery_Res> resData = new ResData<ReverseDelivery_Res>();
            resData.Data = await _consignmentServices.ReverseDelivery(req);
            return Ok(resData);
        }

        /// <summary>
        /// 貨態
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetDeliveryTracking(GetDeliveryTracking_Req req)
        {
            ResData<List<GetDeliveryTracking_Res>> resData = new ResData<List<GetDeliveryTracking_Res>>();
            resData.Data = await _consignmentServices.GetDeliveryTracking(req);
            return Ok(resData);
        }
        /// <summary>
        /// 取消託運單
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> CancelDelivery(CancelDelivery_Req req)
        {
            ResData resData = new ResData();
            await _consignmentServices.CancelDelivery(req);
            return Ok(resData);
        }


        /// <summary>
        /// 來回件
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> RoundShipment(RoundShipment_Req req)
        {
            ResData<RoundShipment_Res> resData = new ResData<RoundShipment_Res>();
            //檢查參數
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.CheckNumber))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            await _consignmentServices.RoundShipment(req);
            return Ok(resData);
        }

        /// <summary>
        /// 查詢託運單
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResData>> GetDeliveryInfo(GetDeliveryInfo_Req req)
        {
            ResData<GetDeliveryInfo_Res> resData = new ResData<GetDeliveryInfo_Res>();
            resData.Data = await _consignmentServices.GetDeliveryInfoBycheck_number(req.checkNumber);
            return Ok(resData);
        }

        /// <summary>
        /// 新增貨態
        /// </summary>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]

        public async Task<ActionResult<ResData>> InsertShipInfo(InsertShipInfo_Req req)
        {
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.checkNumber))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (req.Scan_date == null)
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.status))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            //if (string.IsNullOrEmpty(req.Status_delivery))
            //{
            //    throw new MyException(ResponseCodeEnum.ParameterError);
            //}
            if (string.IsNullOrEmpty(req.Frontline_worker))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            if (string.IsNullOrEmpty(req.Station_name))
            {
                throw new MyException(ResponseCodeEnum.ParameterError);
            }
            ResData resData = new ResData();

            await _consignmentServices.InsertShipInfo(req);

            return Ok(resData);
        }

        /// <summary>
        /// 產品代碼
        /// </summary>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]

        public async Task<ActionResult<ResData>> GetProduct()
        {

            ResData<List<GetProduct_Res>> resData = new ResData<List<GetProduct_Res>>();

            resData.Data = await _consignmentServices.GetProduct();

            return Ok(resData);
        }

        /// <summary>
        /// 丈量代碼
        /// </summary>
        /// <returns></returns>
        [AuthorizationAttribute]
        [HttpPost("[action]")]

        public async Task<ActionResult<ResData>> GetMeasurementInfo()
        {

            ResData<List<GetMeasurementInfo_Res>> resData = new ResData<List<GetMeasurementInfo_Res>>();

            resData.Data = await _consignmentServices.GetMeasurementInfo();


            return Ok(resData);
        }
    }
}
