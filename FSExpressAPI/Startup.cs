using BLL;
using Common;
using Common.SSRS;
using DAL;
using DAL.Context;
using DAL.Contract_DA;
using DAL.DA;
using DAL.FSE01_DA;
using DAL.JunFuAddress_DA;
using DAL.JunFuTrans_DA;
using FSExpressAPI.Handler;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace FSExpressAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //2022-04-12 �[�J cache
            services.AddMemoryCache();

            services.AddControllers();
            services.AddControllers().AddNewtonsoftJson();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "FSExpressAPI", Version = "v1" });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Token",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                            new string[] {}
                    }
                });



                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            #region Authentication
            services.AddAuthentication(option =>
            {
                option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = false,
                    ValidateLifetime = false,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["Jwt:Issuer"],
                    // ValidAudience = Configuration["Jwt:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"])) //Configuration["JwtToken:SecretKey"]
                };
            });
            #endregion


            services.AddHttpContextAccessor();

            //Common
            services.AddSingleton<DapperContext>();
            services.AddSingleton<DBconn>();
            services.AddScoped<IReportHelper, SsrsHelper>();

            //Services
            services.AddScoped<AddressServices>();
            services.AddScoped<AddressServices_itri>();
            services.AddScoped<AuthServices>();
            services.AddScoped<ConsignmentServices>();
            services.AddScoped<ContractServices>();

            services.AddScoped<DailyMoneySettleServices>();
            services.AddScoped<DailySettleTodoListServices>();

            services.AddScoped<DeliveryRequestsServices>();
            services.AddScoped<MeasureServices>();
            services.AddScoped<MoneySettleServices>();
            services.AddScoped<NotificationServices>();
            services.AddScoped<PostOfficeServices>();
            services.AddScoped<PrintServices>();
            services.AddScoped<IReportPrintService, ReportPrintService>();

            //DA
            services.AddScoped<IBagNobinding_DA, BagNobinding_DA>();
            services.AddScoped<Icbm_data_log_DA, cbm_data_log_DA>();
            services.AddScoped<ICBMDetailLog_DA, CBMDetailLog_DA>();
            services.AddScoped<ICBMDetailInfo_DA, CBMDetailInfo_DA>();
            services.AddScoped<ICBMDetail_DA, CBMDetail_DA>();
            services.AddScoped<Icheck_number_prehead_DA, check_number_prehead_DA>();
            services.AddScoped<IDeliveryStatusPublic_DA, DeliveryStatusPublic_DA>();
            services.AddScoped<IDeliveryStatusPublicError_DA, DeliveryStatusPublicError_DA>();
            services.AddScoped<IDeliveryStatusPublicFilter_DA, DeliveryStatusPublicFilter_DA>();
            services.AddScoped<IFSExpressAPILog_DA, FSExpressAPILog_DA>();
            services.AddScoped<IMeasureImageTransferLog_DA, MeasureImageTransferLog_DA>();
            services.AddScoped<IMeasureScanLog_DA, MeasureScanLog_DA>();
            services.AddScoped<IMeasureScanLog_ErrLog_DA, MeasureScanLog_ErrLog_DA>();
            services.AddScoped<IMeasureScanLog_Exclude_DA, MeasureScanLog_Exclude_DA>();
            services.AddScoped<IMeasureScanLog_Filtered_DA, MeasureScanLog_Filtered_DA>();
            services.AddScoped<IMeasureWeightScanLog_DA, MeasureWeightScanLog_DA>();
            services.AddScoped<INotification_DA, Notification_DA>();
            services.AddScoped<INotificationError_DA, NotificationError_DA>();
            services.AddScoped<Ipickup_request_for_apiuser_DA, pickup_request_for_apiuser_DA>();
            services.AddScoped<IPost_DA, Post_DA>();
            services.AddScoped<IPost_request_DA, Post_request_DA>();
            services.AddScoped<IPostCollectOnDelivery_DA, PostCollectOnDelivery_DA>();
            services.AddScoped<IPostEod_DA, PostEod_DA>();
            services.AddScoped<IPostEodParse_DA, PostEodParse_DA>();
            services.AddScoped<IPostEodParseExclude_DA, PostEodParseExclude_DA>();
            services.AddScoped<IPostEodParseError_DA, PostEodParseError_DA>();
            services.AddScoped<IPostEodParseFilter_DA, PostEodParseFilter_DA>();
            services.AddScoped<IPostFTPTransportLog_DA, PostFTPTransportLog_DA>();
            services.AddScoped<IPostResultEod_DA, PostResultEod_DA>();
            services.AddScoped<IPostIrregularEod_DA, PostIrregularEod_DA>();
            services.AddScoped<IPostSerial_DA, PostSerial_DA>();
            services.AddScoped<IPostState_DA, PostState_DA>();
            services.AddScoped<IPostStatus_DA, PostStatus_DA>();
            services.AddScoped<IPostTransmission_DA, PostTransmission_DA>();
            services.AddScoped<IPostXml_DA, PostXml_DA>();
            services.AddScoped<IPostXmlParse_DA, PostXmlParse_DA>();
            services.AddScoped<IPostXmlParseError_DA, PostXmlParseError_DA>();
            services.AddScoped<IPostXmlParseExclude_DA, PostXmlParseExclude_DA>();
            services.AddScoped<IPostXmlParseFilter_DA, PostXmlParseFilter_DA>();

            services.AddScoped<IScheduleAPILog_DA, ScheduleAPILog_DA>();
            services.AddScoped<IScheduleTask_DA, ScheduleTask_DA>();
            services.AddScoped<ItbAccounts_DA, tbAccounts_DA>();
            services.AddScoped<ItbCustomers_DA, tbCustomers_DA>();
            services.AddScoped<ItbItemCodes_DA, tbItemCodes_DA>();
            services.AddScoped<ItbStation_DA, tbStation_DA>();
            services.AddScoped<ItcDeliveryRequests_DA, tcDeliveryRequests_DA>();
            services.AddScoped<ItcDeliveryRequestsReSpecialArea_DA, tcDeliveryRequestsReSpecialArea_DA>();
            services.AddScoped<IttDeliveryRequestsRecord_DA, ttDeliveryRequestsRecord_DA>();
            services.AddScoped<IttDeliveryScanLog_DA, ttDeliveryScanLog_DA>();
            services.AddScoped<IttDeliveryScanLog_ErrorScanLog_DA, ttDeliveryScanLog_ErrorScanLog_DA>();
            services.AddScoped<IUpdateSDMDJobLog_DA, UpdateSDMDJobLog_DA>();

            //JunFuAddress_DA
            services.AddScoped<IAddressParsingAPILog_DA, AddressParsingAPILog_DA>();
            services.AddScoped<IAddressResult_DA, AddressResult_DA>();
            services.AddScoped<IAddressUsingLog_DA, AddressUsingLog_DA>();
            services.AddScoped<IJunfuAddressDismantle_DA, JunfuAddressDismantle_DA>();
            services.AddScoped<IMap8AddressDismantle_DA, Map8AddressDismantle_DA>();
            services.AddScoped<IMap8AddressDismantleV2_DA, Map8AddressDismantleV2_DA>();
            services.AddScoped<IRoad_ZipCodeResult_DA, Road_ZipCodeResult_DA>();
            services.AddScoped<ISpecialAreaResult_DA, SpecialAreaResult_DA>();

            //JunFuTrans_DA
            services.AddScoped<Icheck_number_sd_mapping_DA, check_number_sd_mapping_DA>();
            services.AddScoped<IorgArea_DA, orgArea_DA>();

            //ContractSystem
            services.AddScoped<IAdditionalFeeManage_DA, AdditionalFeeManage_DA>();
            services.AddScoped<IAdditionalFeeManageScope_DA, AdditionalFeeManageScope_DA>();
            services.AddScoped<IAreaType_DA, AreaType_DA>();
            services.AddScoped<ICBM_Detail_DA, CBM_Detail_DA>();
            services.AddScoped<ICBMAudit_DA, CBMAudit_DA>();
            services.AddScoped<ICustomerSettle_Fee_DA, CustomerSettle_Fee_DA>();
            services.AddScoped<IDailyCustomerSettle_Fee_DA, DailyCustomerSettle_Fee_DA>();
            services.AddScoped<IDailyCustomerSettle_Fee_Temp_DA, DailyCustomerSettle_Fee_Temp_DA>();
            services.AddScoped<IDailySettleA_Fee_DA, DailySettleA_Fee_DA>();
            services.AddScoped<IDailySettleA_Fee_Temp_DA, DailySettleA_Fee_Temp_DA>();
            services.AddScoped<IDailyMoneySettleDetailError_DA, DailyMoneySettleDetailError_DA>();
            services.AddScoped<IDailySettleB_Fee_DA, DailySettleB_Fee_DA>();
            services.AddScoped<IDailySettleB_Fee_Temp_DA, DailySettleB_Fee_Temp_DA>();
            services.AddScoped<IDailySettleC_Fee_DA, DailySettleC_Fee_DA>();
            services.AddScoped<IDailySettleC_Fee_Temp_DA, DailySettleC_Fee_Temp_DA>();
            services.AddScoped<IDailySettleTodoList_DA, DailySettleTodoList_DA>();
            services.AddScoped<IDailySettleMain_DA, DailySettleMain_DA>();
            services.AddScoped<IDailySettleMainTemp_DA, DailySettleMainTemp_DA>();
            services.AddScoped<IDailySettleTodoList_DA, DailySettleTodoList_DA>();
            services.AddScoped<IDailyMoneySettleDetail_DA, DailyMoneySettleDetail_DA>();
            services.AddScoped<IDailyMoneySettleDetail_Temp_DA, DailyMoneySettleDetail_Temp_DA>();
            services.AddScoped<IDailyCBMDetail_Temp_DA, DailyCBMDetail_Temp_DA>();
            services.AddScoped<IDailyCBMDetail_DA, DailyCBMDetail_DA>();
            services.AddScoped<IItemCodes_DA, ItemCodes_DA>();
            services.AddScoped<IMoneySettle_Detail_DA, MoneySettle_Detail_DA>();
            services.AddScoped<IMoneySettleA_DA, MoneySettleA_DA>();
            services.AddScoped<IMoneySettleAScope_DA, MoneySettleAScope_DA>();
            services.AddScoped<IMoneySettleC_DA, MoneySettleC_DA>();
            services.AddScoped<IMoneySettleCScope_DA, MoneySettleCScope_DA>();
            services.AddScoped<IProductManage_DA, ProductManage_DA>();
            services.AddScoped<IProductValuationManage_DA, ProductValuationManage_DA>();
            services.AddScoped<IProductValuationManageScope_DA, ProductValuationManageScope_DA>();
            services.AddScoped<ISettleA_Fee_DA, SettleA_Fee_DA>();
            services.AddScoped<ISettleB_Fee_DA, SettleB_Fee_DA>();
            services.AddScoped<ISettleC_Fee_DA, SettleC_Fee_DA>();
            services.AddScoped<ISMSFamilyNetlog_DA, SMSFamilyNetlog_DA>();
            services.AddScoped<ISMSInfo_DA, SMSInfo_DA>();
            services.AddScoped<ISMSUsing_DA, SMSUsing_DA>();
            services.AddScoped<ISpecialArea_DA, SpecialArea_DA>();
            services.AddScoped<ISpecialAreaAudit_DA, SpecialAreaAudit_DA>();
            services.AddScoped<ISpecialAreaManage_DA, SpecialAreaManage_DA>();

            //FSE01_DA
            services.AddScoped<IMoney_Settle_Detail_DA, Money_Settle_Detail_DA>();
            services.AddScoped<ILogRecord_DA, LogRecord>();

            //SP
            services.AddScoped<IStoredProcedure_DA, StoredProcedure_DA>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsEnvironment("local"))
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "FSExpressAPI v1"));
            }

            app.UseMiddleware<ApiLog>();
            app.UseMiddleware<CashExceptionHandler>();
            app.UseMiddleware<JWTMiddleware>();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });



        }
    }
}
