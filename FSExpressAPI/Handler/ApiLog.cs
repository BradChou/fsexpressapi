﻿using DAL.DA;
using DAL.Model.Condition;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSExpressAPI.Handler
{
    public class ApiLog
    {
        private readonly RequestDelegate _next;


        private readonly RecyclableMemoryStreamManager _recyclableMemoryStreamManager;

        public ApiLog(RequestDelegate next)
        {
            _next = next;
            _recyclableMemoryStreamManager = new RecyclableMemoryStreamManager();

        }
        public async Task InvokeAsync(HttpContext context, IFSExpressAPILog_DA _FSExpressAPILog_DA, IConfiguration configuration)
        {

            var id = GetLogId();

            context.Items["ApiLogId"] = id;

            DateTime startTime = DateTime.Now;

            //req
            context.Request.EnableBuffering();
            await using var requestStream = _recyclableMemoryStreamManager.GetStream();
            await context.Request.Body.CopyToAsync(requestStream);

            //res
            var originalBodyStream = context.Response.Body;
            await using var responseBody = _recyclableMemoryStreamManager.GetStream();
            context.Response.Body = responseBody;

            var Schema = context.Request.Scheme;

            var Host = context.Request.Host.ToUriComponent();

            var Path = context.Request.Path.HasValue ? context.Request.Path.Value : string.Empty;

            var QueryString = context.Request.QueryString.HasValue ? context.Request.QueryString.Value : string.Empty;

            var RequestHeader = GetHeaders(context.Request.Headers);

            var RequestBody = ReadStreamInChunks(requestStream);

            context.Request.Body.Position = 0;

            await _next(context);

            context.Response.Body.Seek(0, SeekOrigin.Begin);
            var responseBodyTxt = await new StreamReader(context.Response.Body).ReadToEndAsync();
            context.Response.Body.Seek(0, SeekOrigin.Begin);
            await responseBody.CopyToAsync(originalBodyStream);

            // var ResponseHeader = GetHeaders(context.Response.Headers);
            var ResponseBody = responseBodyTxt;
            //var ResponseStatus = context.Response.StatusCode;

            DateTime EndTime = DateTime.Now;

            //apilog
            FSExpressAPILog_Condition fSExpressAPILog_Condition = new FSExpressAPILog_Condition();

            fSExpressAPILog_Condition.Guid = id;
            fSExpressAPILog_Condition.Account = context.Items["Account"] == null ? string.Empty : (string)context.Items["Account"];
            fSExpressAPILog_Condition.RequestBody = RequestBody;
            fSExpressAPILog_Condition.ResponseBody = ResponseBody;
            fSExpressAPILog_Condition.RequestHeader = RequestHeader;
            fSExpressAPILog_Condition.Path = Path;
            fSExpressAPILog_Condition.QueryString = QueryString;
            fSExpressAPILog_Condition.StratTime = startTime;
            fSExpressAPILog_Condition.EndTime = EndTime;

            try
            {
                if ((!string.IsNullOrEmpty(ResponseBody)) && (!string.IsNullOrEmpty(RequestBody)))
                {
                    await _FSExpressAPILog_DA.Add(fSExpressAPILog_Condition);
                }
               
            }
            catch (Exception e)
            {

                string path = string.IsNullOrEmpty(configuration["ApiLogPath"]) ? @"C:\FSExpressAPILog\" : configuration["ApiLogPath"];

                string fileName = DateTime.Now.ToString("yyyyMMdd") + ".txt";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                if (!File.Exists(fileName))
                {
                    var file = File.Create(fileName);
                    file.Close();
                }
                File.AppendAllLines(path + fileName,
                    new List<string>
                    {
                    "--------Start----------",
                  "Time :" + DateTime.Now.ToString(),
                  "Message :"+  e.Message,
                  "StackTrace :"+  e.StackTrace,
                  "Source :"+  e.Source,
                  "Data :"+  JsonConvert.SerializeObject(fSExpressAPILog_Condition),

                    "--------End----------",
                    });
            }
        }


        private static string GetLogId()
        {

            var guid = Guid.NewGuid().ToString("N");

            return guid;
        }

        private static string GetHeaders(IHeaderDictionary headers)
        {
            var headerStr = new StringBuilder();
            foreach (var header in headers)
            {
                headerStr.Append($"{header.Key}: {header.Value}。");
            }

            return headerStr.ToString();
        }

        private static string ReadStreamInChunks(Stream stream)
        {
            const int readChunkBufferLength = 4096;
            stream.Seek(0, SeekOrigin.Begin);
            using var textWriter = new StringWriter();
            using var reader = new StreamReader(stream);
            var readChunk = new char[readChunkBufferLength];
            int readChunkLength;
            do
            {
                readChunkLength = reader.ReadBlock(readChunk, 0, readChunkBufferLength);
                textWriter.Write(readChunk, 0, readChunkLength);
            } while (readChunkLength > 0);
            return textWriter.ToString();
        }
    }
}
