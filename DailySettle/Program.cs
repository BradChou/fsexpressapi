using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using DAL.DA;
using Common;
using DailySettle.Common;
using DAL.FSE01_DA;
using DAL.Contract_DA;
using BLL;
using DAL.Context;
using DAL;
using Common.SSRS;

namespace DailySettle
{
    // https://stackoverflow.com/questions/70475830/how-to-use-dependency-injection-in-winforms

    static class Program
    {
        public static IConfiguration Configuration;

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //To register all default providers:
            //var host = Host.CreateDefaultBuilder(args).Build();
            var builder = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            Configuration = builder.Build();

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var host = CreateHostBuilder().Build();
            ServiceProvider = host.Services;

            Application.Run(ServiceProvider.GetRequiredService<Form1>());

            //Application.Run(new Form1());
        }


        public static IServiceProvider ServiceProvider { get; private set; }
        static IHostBuilder CreateHostBuilder()
        {
            return Host.CreateDefaultBuilder()
                .ConfigureServices((context, services) => {
                    services.AddSingleton<DBconn>();
                    services.AddScoped<DailySettleTodoListServices>();
                    services.AddScoped<DailyMoneySettleServices>();
                    services.AddScoped<ContractServices>();

                    
                    services.AddScoped<IScheduleTask_DA, ScheduleTask_DA>();
                    services.AddScoped<IDailySettleTodoList_DA, DailySettleTodoList_DA>();
                    services.AddScoped<IDailySettleMainTemp_DA, DailySettleMainTemp_DA>();
                    services.AddScoped<IDailySettleMain_DA, DailySettleMain_DA>();
                    services.AddScoped<ItcDeliveryRequests_DA, tcDeliveryRequests_DA>();
                    services.AddScoped<ISpecialAreaAudit_DA, SpecialAreaAudit_DA>();

                    services.AddScoped<IDailyMoneySettleDetail_DA, DailyMoneySettleDetail_DA>();
                    services.AddScoped<IDailyMoneySettleDetail_Temp_DA, DailyMoneySettleDetail_Temp_DA>();

                    services.AddScoped<IDailyCBMDetail_DA, DailyCBMDetail_DA>();
                    services.AddScoped<IDailyCBMDetail_Temp_DA, DailyCBMDetail_Temp_DA>();

                    services.AddScoped<IDailyMoneySettleDetailError_DA, DailyMoneySettleDetailError_DA>();

                    services.AddScoped<IDailyCalErrMsgLog_DA, DailyCalErrMsgLog_DA>();

                    services.AddScoped<IAreaType_DA, AreaType_DA>();
                    services.AddScoped<IBagNobinding_DA, BagNobinding_DA>();
                    services.AddScoped<ItbCustomers_DA, tbCustomers_DA>();
                    services.AddScoped<ICBMDetail_DA, CBMDetail_DA>();
                    services.AddScoped<ISpecialAreaAudit_DA, SpecialAreaAudit_DA>();
                    services.AddScoped<ICBMAudit_DA, CBMAudit_DA>();
                    services.AddScoped<IProductValuationManage_DA, ProductValuationManage_DA>();

                    services.AddScoped<IDailySettleMainAP_DA, DailySettleMainAP_DA>();
                    //services.AddScoped<DailySettlementMain>();
                    services.AddScoped<ILogRecord_DA, LogRecord>();



                    services.AddSingleton<DapperContext>();
                    services.AddSingleton<DBconn>();

                    //Services
                    services.AddScoped<AddressServices>();
                    services.AddScoped<AddressServices_itri>();
                    services.AddScoped<AuthServices>();
                    services.AddScoped<ConsignmentServices>();
                    services.AddScoped<ContractServices>();

                    services.AddScoped<DailyMoneySettleServices>();
                    services.AddScoped<DailySettleTodoListServices>();
                    services.AddScoped<DeliveryRequestsServices>();


                    services.AddScoped<MeasureServices>();
                    services.AddScoped<MoneySettleServices>();
                    services.AddScoped<NotificationServices>();
                    services.AddScoped<PostOfficeServices>();
                    services.AddScoped<PrintServices>();
                    services.AddScoped<IReportPrintService, ReportPrintService>();

                    services.AddScoped<IReportHelper, SsrsHelper>();



                    //DA


                    services.AddScoped<IBagNobinding_DA, BagNobinding_DA>();
                    services.AddScoped<Icbm_data_log_DA, cbm_data_log_DA>();
                    services.AddScoped<ICBMDetailLog_DA, CBMDetailLog_DA>();
                    services.AddScoped<ICBMDetailInfo_DA, CBMDetailInfo_DA>();
                    services.AddScoped<ICBMDetail_DA, CBMDetail_DA>();
                    services.AddScoped<Icheck_number_prehead_DA, check_number_prehead_DA>();
                    services.AddScoped<IDeliveryStatusPublic_DA, DeliveryStatusPublic_DA>();
                    services.AddScoped<IDeliveryStatusPublicError_DA, DeliveryStatusPublicError_DA>();
                    services.AddScoped<IDeliveryStatusPublicFilter_DA, DeliveryStatusPublicFilter_DA>();
                    services.AddScoped<IFSExpressAPILog_DA, FSExpressAPILog_DA>();
                    services.AddScoped<IMeasureImageTransferLog_DA, MeasureImageTransferLog_DA>();
                    services.AddScoped<IMeasureScanLog_DA, MeasureScanLog_DA>();
                    services.AddScoped<IMeasureScanLog_ErrLog_DA, MeasureScanLog_ErrLog_DA>();
                    services.AddScoped<IMeasureScanLog_Exclude_DA, MeasureScanLog_Exclude_DA>();
                    services.AddScoped<IMeasureScanLog_Filtered_DA, MeasureScanLog_Filtered_DA>();
                    services.AddScoped<IMeasureWeightScanLog_DA, MeasureWeightScanLog_DA>();
                    services.AddScoped<INotification_DA, Notification_DA>();
                    services.AddScoped<INotificationError_DA, NotificationError_DA>();
                    services.AddScoped<Ipickup_request_for_apiuser_DA, pickup_request_for_apiuser_DA>();
                    services.AddScoped<IPost_DA, Post_DA>();
                    services.AddScoped<IPost_request_DA, Post_request_DA>();
                    services.AddScoped<IPostCollectOnDelivery_DA, PostCollectOnDelivery_DA>();
                    services.AddScoped<IPostEod_DA, PostEod_DA>();
                    services.AddScoped<IPostEodParse_DA, PostEodParse_DA>();
                    services.AddScoped<IPostEodParseExclude_DA, PostEodParseExclude_DA>();
                    services.AddScoped<IPostEodParseError_DA, PostEodParseError_DA>();
                    services.AddScoped<IPostEodParseFilter_DA, PostEodParseFilter_DA>();
                    services.AddScoped<IPostFTPTransportLog_DA, PostFTPTransportLog_DA>();
                    services.AddScoped<IPostResultEod_DA, PostResultEod_DA>();
                    services.AddScoped<IPostIrregularEod_DA, PostIrregularEod_DA>();
                    services.AddScoped<IPostSerial_DA, PostSerial_DA>();
                    services.AddScoped<IPostState_DA, PostState_DA>();
                    services.AddScoped<IPostStatus_DA, PostStatus_DA>();
                    services.AddScoped<IPostTransmission_DA, PostTransmission_DA>();
                    services.AddScoped<IPostXml_DA, PostXml_DA>();
                    services.AddScoped<IPostXmlParse_DA, PostXmlParse_DA>();
                    services.AddScoped<IPostXmlParseError_DA, PostXmlParseError_DA>();
                    services.AddScoped<IPostXmlParseExclude_DA, PostXmlParseExclude_DA>();
                    services.AddScoped<IPostXmlParseFilter_DA, PostXmlParseFilter_DA>();
                    services.AddScoped<IScheduleAPILog_DA, ScheduleAPILog_DA>();
                    services.AddScoped<IScheduleTask_DA, ScheduleTask_DA>();
                    services.AddScoped<ItbAccounts_DA, tbAccounts_DA>();
                    services.AddScoped<ItbCustomers_DA, tbCustomers_DA>();
                    services.AddScoped<ItbItemCodes_DA, tbItemCodes_DA>();
                    services.AddScoped<ItbStation_DA, tbStation_DA>();
                    services.AddScoped<ItcDeliveryRequests_DA, tcDeliveryRequests_DA>();
                    services.AddScoped<IttDeliveryRequestsRecord_DA, ttDeliveryRequestsRecord_DA>();
                    services.AddScoped<IttDeliveryScanLog_DA, ttDeliveryScanLog_DA>();
                    services.AddScoped<IttDeliveryScanLog_ErrorScanLog_DA, ttDeliveryScanLog_ErrorScanLog_DA>();
                    services.AddScoped<IUpdateSDMDJobLog_DA, UpdateSDMDJobLog_DA>();
                    services.AddScoped<ILogRecord_DA, LogRecord>();

                    //ContractSystem
                    services.AddScoped<IAdditionalFeeManage_DA, AdditionalFeeManage_DA>();
                    services.AddScoped<IAdditionalFeeManageScope_DA, AdditionalFeeManageScope_DA>();
                    services.AddScoped<IAreaType_DA, AreaType_DA>();
                    services.AddScoped<ICBM_Detail_DA, CBM_Detail_DA>();
                    services.AddScoped<ICustomerSettle_Fee_DA, CustomerSettle_Fee_DA>();

                    services.AddScoped<IDailyCustomerSettle_Fee_DA, DailyCustomerSettle_Fee_DA>();
                    services.AddScoped<IDailyCustomerSettle_Fee_Temp_DA, DailyCustomerSettle_Fee_Temp_DA>();
                    services.AddScoped<IDailySettleA_Fee_DA, DailySettleA_Fee_DA>();
                    services.AddScoped<IDailySettleA_Fee_Temp_DA, DailySettleA_Fee_Temp_DA>();
                    services.AddScoped<IDailySettleB_Fee_DA, DailySettleB_Fee_DA>();
                    services.AddScoped<IDailySettleB_Fee_Temp_DA, DailySettleB_Fee_Temp_DA>();
                    services.AddScoped<IDailySettleC_Fee_DA, DailySettleC_Fee_DA>();
                    services.AddScoped<IDailySettleC_Fee_Temp_DA, DailySettleC_Fee_Temp_DA>();
                    services.AddScoped<IDailySettleMain_DA, DailySettleMain_DA>();
                    services.AddScoped<IDailySettleMainTemp_DA, DailySettleMainTemp_DA>();
                    services.AddScoped<IDailySettleTodoList_DA, DailySettleTodoList_DA>();
                    services.AddScoped<IDailyMoneySettleDetail_DA, DailyMoneySettleDetail_DA>();
                    services.AddScoped<IDailyMoneySettleDetail_Temp_DA, DailyMoneySettleDetail_Temp_DA>();
                    services.AddScoped<IDailyCBMDetail_Temp_DA, DailyCBMDetail_Temp_DA>();
                    services.AddScoped<IDailyCBMDetail_DA, DailyCBMDetail_DA>();
                    services.AddScoped<IItemCodes_DA, ItemCodes_DA>();
                    services.AddScoped<IMoneySettle_Detail_DA, MoneySettle_Detail_DA>();
                    services.AddScoped<IMoneySettleA_DA, MoneySettleA_DA>();
                    services.AddScoped<IMoneySettleAScope_DA, MoneySettleAScope_DA>();
                    services.AddScoped<IMoneySettleC_DA, MoneySettleC_DA>();
                    services.AddScoped<IMoneySettleCScope_DA, MoneySettleCScope_DA>();
                    services.AddScoped<IProductManage_DA, ProductManage_DA>();
                    services.AddScoped<IProductValuationManage_DA, ProductValuationManage_DA>();
                    services.AddScoped<IProductValuationManageScope_DA, ProductValuationManageScope_DA>();
                    services.AddScoped<ISettleA_Fee_DA, SettleA_Fee_DA>();
                    services.AddScoped<ISettleB_Fee_DA, SettleB_Fee_DA>();
                    services.AddScoped<ISettleC_Fee_DA, SettleC_Fee_DA>();
                    services.AddScoped<ISpecialArea_DA, SpecialArea_DA>();
                    services.AddScoped<ISpecialAreaManage_DA, SpecialAreaManage_DA>();
                    services.AddScoped<IMoney_Settle_Detail_DA, Money_Settle_Detail_DA>();
                    services.AddScoped<ISpecialAreaAudit_DA, SpecialAreaAudit_DA>();
                    services.AddScoped<ICBMAudit_DA, CBMAudit_DA>();
                    services.AddScoped<ILogRecord_DA, LogRecord>();
                    services.AddScoped<IDailyMoneySettleDetail_DA, DailyMoneySettleDetail_DA>();
                    services.AddScoped<IDailyMoneySettleDetail_Temp_DA, DailyMoneySettleDetail_Temp_DA>();
                    services.AddScoped<IDailyCBMDetail_DA, DailyCBMDetail_DA>();
                    services.AddScoped<IDailyCBMDetail_Temp_DA, DailyCBMDetail_Temp_DA>();


                    //SP
                    services.AddScoped<IStoredProcedure_DA, StoredProcedure_DA>();
                    services.AddScoped<ISMSInfo_DA, SMSInfo_DA>();
                    services.AddScoped<ISMSUsing_DA, SMSUsing_DA>();
                    services.AddScoped<ISMSFamilyNetlog_DA, SMSFamilyNetlog_DA>();
                    services.AddScoped<IItemCodes_DA, ItemCodes_DA>();
                    services.AddScoped<IUpdateSDMDJobLog_DA, UpdateSDMDJobLog_DA>();
                    services.AddScoped<IScheduleAPILog_DA, ScheduleAPILog_DA>();


                    services.AddScoped<IDailyCustomerSettle_Fee_DA, DailyCustomerSettle_Fee_DA>();
                    services.AddScoped<IDailySettleA_Fee_DA, DailySettleA_Fee_DA>();
                    services.AddScoped<IDailySettleB_Fee_DA, DailySettleB_Fee_DA>();
                    services.AddScoped<IDailySettleC_Fee_DA, DailySettleC_Fee_DA>();
                    services.AddScoped<IDailyCustomerSettle_Fee_Temp_DA, DailyCustomerSettle_Fee_Temp_DA>();
                    services.AddScoped<IDailySettleA_Fee_Temp_DA, DailySettleA_Fee_Temp_DA>();
                    services.AddScoped<IDailySettleB_Fee_Temp_DA, DailySettleB_Fee_Temp_DA>();
                    services.AddScoped<IDailySettleC_Fee_Temp_DA, DailySettleC_Fee_Temp_DA>();





                    services.AddTransient<Form1>();
                });
        }


    }
}
