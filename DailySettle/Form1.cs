﻿using DailySettle.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Extensions.Configuration;
using DAL.DA;
using DAL.Model.Condition;
using BLL;
using DAL.FSE01_DA;

namespace DailySettle
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //private IDailySettlementMain _DailySettlementMain;
        //public Form1(IDailySettleMain_DA dailySettleMain_DA, IDailySettlementMain dailySettlementMain)
        //{
        //    _DailySettleMain_DA = dailySettleMain_DA;
        //    _DailySettlementMain = dailySettlementMain;
        //    InitializeComponent();
        //}
        //public Form1( IDailySettlementMain dailySettlementMain)
        //{
        //    _DailySettlementMain = dailySettlementMain;
        //    InitializeComponent();
        //}



        private IDailySettleMainAP_DA _DailySettleMainAP_DA;
        private DailySettleTodoListServices _DailySettleTodoListServices;
        private DailyMoneySettleServices _DailyMoneySettleServices;
        private ILogRecord_DA _LogRecord;
        private ContractServices _ContractServices ;
        public Form1(IDailySettleMainAP_DA dailySettleMainAP_DA , 
                        DailySettleTodoListServices dailySettleTodoListServices ,
                        DailyMoneySettleServices dailyMoneySettleServices,
                        ContractServices contractServices ,
                        ILogRecord_DA logRecord)
        {
            _DailySettleMainAP_DA = dailySettleMainAP_DA;
            _DailySettleTodoListServices = dailySettleTodoListServices;
            _DailyMoneySettleServices = dailyMoneySettleServices;
            _ContractServices = contractServices;
            _LogRecord = logRecord;
            InitializeComponent();
        }
        private DateTime now;

        //日結排程時間
        private List<string> listDailySettlementHHMM = new List<string>();

        //日結工作表的截止計算時間 
        private DateTime LimitTime = DateTime.Now;

        //執行日結
        bool runDailySettlement = false;
        //執行新增資料就好
        bool runDailyInsert = false;

        //執行日結 到哪個步驟
        int iDailySettleStep = 0;
        //執行新增資料 到哪個步驟
        int iDailyInsertStep = 0;

        private void Form1_Load(object sender, EventArgs e)
        {

            //  https://stackoverflow.com/questions/65669920/adding-configuration-to-windows-forms-on-net-5-0
            //  https://stackoverflow.com/questions/71559139/inject-iconfiguration-to-windows-forms-on-net-6-0

            now = DateTime.Now;
            txtBox.AppendText("系統啟動時間 - " + now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n");

            BaseData();

            txtBox.AppendText("截止計算時間 - " + LimitTime.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");

        }

        int iStartKey = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            iStartKey += 1;

            if (iStartKey <= 3)
            {
                label1.Text = "訊息 : 倒數 " + (3 - iStartKey).ToString() + " 秒 , 排程即將自動開始";
                label1.ForeColor = Color.Black;
            }
            else
            {
                if (iStartKey % 600 == 0)
                {
                    txtBox.AppendText("現在時刻 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n");
                }
            }

            if (iStartKey == 3)
            {
                label1.Text = "";
                ControlSetStartAuto();
                Application.DoEvents();
                Main();
                //txtBox.AppendText("準備結束 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n");

                //SaveLog();
                //SaveNotify();
                //Application.DoEvents();
                //Application.Exit();
            }
        }

        //暫停自動倒數
        private void ControlSetStopAuto()
        {
            timer1.Enabled = false;
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
            //checkBox1.Enabled = true;
        }

        //恢復自動倒數
        private void ControlSetStartAuto()
        {
            timer1.Enabled = true;
            button1.Enabled = false;
            button2.Enabled = true;
            button3.Enabled = false;
            button4.Enabled = false;
            //checkBox1.Enabled = false;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //checkBox1.Checked = !checkBox1.Checked;
            iStartKey = 0;

            if (checkBox1.Checked == false)
            {
                label1.Text = "訊息 :請注意! 自動執行已停止";
                txtBox.AppendText("取消自動執行 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");
                iStartKey = 0;
                ControlSetStopAuto();
                return;
            }
            if (checkBox1.Checked == true)
            {
                label1.Text = "訊息 :請注意! 自動執行重新開始倒數";
                txtBox.AppendText("恢復自動執行 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");
                iStartKey = 0;
                ControlSetStartAuto();
                return;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 基礎設定資料
        /// </summary>
        private void BaseData()
        {
            var WorkSetting = Program.Configuration.GetSection("WorkSetting").Get<AppSettingConfig>();

            //DbWork.setConn(DbWork.CONN_TYPE.MAIN.ToString(), Program.Configuration.GetConnectionString("connstr"));
            //DbWork.setConn(DbWork.CONN_TYPE.LOG.ToString(), Program.Configuration.GetConnectionString("connstr_log"));
            //DbWork.setConn(DbWork.CONN_TYPE.FSE01.ToString(), Program.Configuration.GetConnectionString("connstr_fse01"));

            //日結排程時間
            listDailySettlementHHMM = WorkSetting.doDailySettlementHHMM;

            //日結工作表的截止計算時間 
            LimitTime = WorkSetting.LimitTime;
        }

        /// <summary>
        /// 主函式
        /// </summary>
        private void Main()
        {

            if ( listDailySettlementHHMM.Contains(now.ToString("HHmm")) )
            {
                runDailySettlement = true;
            }
            else
            {
                runDailyInsert = true;
            }

            DateTime thisNnow = DateTime.Now;
            if (runDailyInsert)
            {
                label1.Text = "訊息 : 準備開始 非日結排程 - " + thisNnow.ToString("yyyy-MM-dd HH:mm:ss");
                txtBox.AppendText("準備開始 非日結排程 - " + thisNnow.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");
                //_DailySettleMain_DA.doInsertDailyNewData(LimitTime);
                //txtBox.AppendText("結束 非日結排程 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");

                backgroundWorker1.ReportProgress(0);
                backgroundWorker1.RunWorkerAsync();
                //backgroundWorker1.CancelAsync();

            }

            if (runDailySettlement)
            {
                label1.Text = "訊息 : 準備開始 日結排程 - " + thisNnow.ToString("yyyy-MM-dd HH:mm:ss");
                txtBox.AppendText("準備開始 日結排程  - " + thisNnow.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");

                backgroundWorker1.ReportProgress(0);
                backgroundWorker1.RunWorkerAsync();
                //backgroundWorker1.CancelAsync();
            }
        }



        private  void backgroundWorker1_DoWorkAsync(object sender, DoWorkEventArgs e)
        {
            //BackgroundWorker worker = sender as BackgroundWorker;

            //for (int i = 1; i <= 10; i++)
            //{
            //    if (worker.CancellationPending == true)
            //    {
            //        e.Cancel = true;
            //        break;
            //    }
            //    else
            //    {
            //        // Perform a time consuming operation and report progress.
            //        System.Threading.Thread.Sleep(500);
            //        worker.ReportProgress(i * 10);
            //    }
            //}

            //新增資料
            if (runDailyInsert)
            {
                System.Threading.Thread.Sleep(1000);
                iDailyInsertStep = 1;
                backgroundWorker1.ReportProgress(1);
                _DailySettleMainAP_DA.doInsertDailyNewData(LimitTime);

            }
                
            //日結
            if (runDailySettlement)
            {
                System.Threading.Thread.Sleep(1000);
                iDailySettleStep = 1;
                backgroundWorker1.ReportProgress(1);
                _DailySettleMainAP_DA.doGetCalcList(LimitTime);

                _LogRecord.Save("DailySettlement", "1.doGetCalcList", "NewDataList - " +  _DailySettleMainAP_DA.dailySettleToDoListModel.NewDataList.Count.ToString());
                _LogRecord.Save("DailySettlement", "1.doGetCalcList", "UpdateDataList - " + _DailySettleMainAP_DA.dailySettleToDoListModel.UpdateDataList.Count.ToString());


                System.Threading.Thread.Sleep(1000);
                iDailySettleStep = 2;
                backgroundWorker1.ReportProgress(2);
                _DailySettleMainAP_DA.doUpdateDailyTempTable(LimitTime);

                var t1 = Task.Run(async () =>
                {

                    _DailySettleMainAP_DA.dailySettleToDoListModel = await _DailySettleTodoListServices.TodoListUpdateMainTempJob(_DailySettleMainAP_DA.dailySettleToDoListModel);
                });

                t1.Wait();

                _LogRecord.Save("DailySettlement", "2.doUpdateDailyTempTable", "end");


                System.Threading.Thread.Sleep(1000);
                iDailySettleStep = 3;
                backgroundWorker1.ReportProgress(3);
                //_DailySettleMainAP_DA.doInsertMoneySettleDetail_CBM(_DailySettleMainAP_DA.dailySettleToDoListModel);
                _LogRecord.Save("DailySettlement", "3.doInsertMoneySettleDetail_CBM", "start");

                var t2 = Task.Run(async () =>
                {
                    await _DailyMoneySettleServices.InsertMoneySettleDetail_new(_DailySettleMainAP_DA.dailySettleToDoListModel);
                });

                t2.Wait();
                _LogRecord.Save("DailySettlement", "3.doInsertMoneySettleDetail_CBM", "end");



                System.Threading.Thread.Sleep(1000);
                iDailySettleStep = 4;
                backgroundWorker1.ReportProgress(4);

                _LogRecord.Save("DailySettlement", "4.doCalcContractSettleDetail", "start");
                var t3 = Task.Run(async () =>
                {

                    await _ContractServices.SettleCaculate_daily();// (_DailySettleMainAP_DA.dailySettleToDoListModel);
                });

                t3.Wait();
                _LogRecord.Save("DailySettlement", "4.doCalcContractSettleDetail", "end");


                //_DailyMoneySettleServices
                //DailySettlementMain.doCalcContractSettleDetail(DailySettlementMain.toDoListModel);

                System.Threading.Thread.Sleep(1000);
                iDailySettleStep = 5;
                backgroundWorker1.ReportProgress(5);
                _DailySettleMainAP_DA.doUpdateTempToProd(_DailySettleMainAP_DA.dailySettleToDoListModel, LimitTime);


                System.Threading.Thread.Sleep(1000);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //resultLabel.Text = (e.ProgressPercentage.ToString() + "%");

            if (runDailyInsert == true)
            {
                if (iDailyInsertStep == 1)
                {
                    txtBox.AppendText("正在執行 新增 1.doInsertDailyNewData - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");
                }
            }

            if (runDailySettlement == true )
            {
                if (iDailySettleStep == 1)
                {
                    txtBox.AppendText("正在執行 日結 1.doGetCalcList - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");
                }
                else if (iDailySettleStep == 2 )
                {
                    txtBox.AppendText("NewDataList - " + _DailySettleMainAP_DA.dailySettleToDoListModel.NewDataList.Count.ToString() + "  \r\n\r\n");
                    txtBox.AppendText("UpdateDataList - " + _DailySettleMainAP_DA.dailySettleToDoListModel.UpdateDataList.Count.ToString() + "  \r\n\r\n");

                    txtBox.AppendText("正在執行 日結 2.doUpdateDailyTempTable - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");
                }
                else if (iDailySettleStep == 3)
                {
                    txtBox.AppendText("正在執行 日結 3.doInsertMoneySettleDetail_CBM - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");
                }
                else if (iDailySettleStep == 4)
                {
                    txtBox.AppendText("正在執行 日結 4.doCalcContractSettleDetail - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");
                }
                else if (iDailySettleStep == 5)
                {
                    txtBox.AppendText("正在執行 日結 5.doUpdateTempToProd - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");
                }
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //if (e.Cancelled == true)
            //{
            //    resultLabel.Text = "Canceled!";
            //}
            //else if (e.Error != null)
            //{
            //    resultLabel.Text = "Error: " + e.Error.Message;
            //}
            //else
            //{
            //    resultLabel.Text = "Done!";
            //}

            if (e.Cancelled == true)
            {
                txtBox.AppendText("非同步排程被主動取消強迫中止 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");
            }
            else if (e.Error != null)
            {
                txtBox.AppendText("非同步排程發生錯誤強迫中止 - " + e.Error.Message + " - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");
            }
            else
            {

                if (runDailyInsert == true)
                {
                    //runDailyInsert = false;
                    txtBox.AppendText("新增 非同步排程完成 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");
                }
                if (runDailySettlement == true)
                {
                    //runDailySettlement = false;
                    txtBox.AppendText("日結 非同步排程完成 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n\r\n");
                }

                if (runDailyInsert == true || runDailySettlement == true)
                {
                    SaveLog();
                    SaveNotify();
                    iDailySettleStep = 0;
                    iDailyInsertStep = 0;
                    runDailyInsert = false;
                    runDailySettlement = false;

                    Application.DoEvents();
                    Application.Exit();
                }

                //resultLabel.Text = "Done!";
            }

        }



        private void SaveLog()
        {
            try
            {
                if (!System.IO.Directory.Exists("Log"))
                {
                    System.IO.Directory.CreateDirectory("Log");
                }
                System.IO.File.WriteAllText("Log/" + DateTime.Now.ToString("yyyyMMddHHmm") 
                            + "_" + (runDailyInsert==true? "資料新增":"")
                            + "_" + (runDailySettlement == true ? "日結" : "")
                            + "_log.txt", txtBox.Text.ToString());
            }
            catch (Exception ex)
            {

            }

        }

        private void SaveNotify()
        {
            //一般新增只在指定的整點 播報
            if ((( DateTime.Now.Hour == 8 || DateTime.Now.Hour == 11 || DateTime.Now.Hour == 14 || DateTime.Now.Hour == 17 || DateTime.Now.Hour == 20 || DateTime.Now.Hour == 23)
                    && DateTime.Now.Minute < 25))
            {
                //紀錄並播報
                if (runDailyInsert)
                {
                    _DailySettleMainAP_DA.InsertNotifyLogRecord(
                                 new JobRecord("10000", "006", "OK", "日結工作(資料新增)", "\r\n處理完成 ; \r\n" + "系統啟動時間 - " + now.ToString("yyyy-MM-dd HH:mm:ss"), "", 1, "")
                             );
                }
            }
            else
            {
                //紀錄但不播報
                if (runDailyInsert)
                {
                    _DailySettleMainAP_DA.InsertNotifyLogRecord(
                                 new JobRecord("10000", "006", "OK", "日結工作(資料新增)", "\r\n處理完成 ; \r\n" + "系統啟動時間 - " + now.ToString("yyyy-MM-dd HH:mm:ss"), "", 0, "")
                             );
                }
            }

            //日結不論何時完成都要播報
            if (runDailySettlement)
            {
                _DailySettleMainAP_DA.InsertNotifyLogRecord(
                                 new JobRecord("10000", "007", "OK", "日結工作(日結)", "\r\n處理完成 ; \r\n" + "系統啟動時間 - " + now.ToString("yyyy-MM-dd HH:mm:ss"), "", 1, "")
                             );
            }

        }
    }
}
