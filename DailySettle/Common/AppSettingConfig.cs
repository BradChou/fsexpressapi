﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailySettle.Common
{
    public class AppSettingConfig
    {
        public string connstr { get; set; }
        public string connstr_log { get; set; }
        public string connstr_fse01 { get; set; }

        public string _doDailySettlementHHMM { get; set; }

        public List<string> doDailySettlementHHMM 
        {   get
            {
                return _doDailySettlementHHMM.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
        }

        private int _calcDailySettlementEndTime = 0;

        public int calcDailySettlementEndTime
        {
            get
            {
                return _calcDailySettlementEndTime;
            }
            set
            {
                _calcDailySettlementEndTime = int.Parse(value.ToString());
            }
        }

        public DateTime LimitTime
        {
            get
            {
                return DateTime.Now.AddMinutes(_calcDailySettlementEndTime);
            }
        }



    }
}
