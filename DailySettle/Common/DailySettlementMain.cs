﻿using BLL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DAL.DA;

namespace DailySettle.Common
{
    public class DailySettlementMain: IDailySettlementMain
    {

        //private static string _DailyTempTable = "";

        //public static string DailyTempTable
        //{
        //    get
        //    {
        //        return _DailyTempTable;
        //    }
        //    set
        //    {
        //        _DailyTempTable = value;
        //    }
        //}
        ///// <summary>
        ///// 日結排程 step0 建立日結暫存表 (空的)
        ///// </summary>
        ///// <returns></returns>
        //public static string doCreateDailyTempTable()
        //{
        //    DailyTempTable = "";
        //    return DailyTempTable;
        //}

        private DailySettleMain_DA _DailySettleMain_DA;


        public DailySettlementMain(DailySettleMain_DA DailySettleMain_DA)
        {
            _DailySettleMain_DA = DailySettleMain_DA;
        }

        private ToDoListModel _toDoListModel = new ToDoListModel();

        public ToDoListModel toDoListModel
        {
            get
            {
                return _toDoListModel;
            }
            set
            {
                _toDoListModel = value;
            }
        }

        /// <summary>
        /// 非日結排程 新增日結資料
        ///     step1. 新增至日結主表
        ///     step2. add to todolist
        /// </summary>
        public void doInsertDailyNewData()
        {
            _DailySettleMain_DA.Step0_Insert();
            LogRecord.Save("DailySettlement", "0.doInsertDailyNewData", "");
        }


        /// <summary>
        /// 日結排程 step1 取得本次要計算的日結排程的清單
        ///     step1. 從 todo 拉還沒作業的清單出來 ( 一般新增資料 + 更新資料 )
        ///     step2. (一般新增資料)  
        ///     step3. (要更新的資料) 
        /// </summary>
        /// <returns></returns>
        public ToDoListModel doGetCalcList(int iEndTime)
        {
            LogRecord.Save("DailySettlement", "1.doGetCalcList", "");
            return null;
        }

        /// <summary>
        /// 日結排程 step2 依照 ToDoListModel 循序 ( 新增/改單[一般/銷單/規格清洗/其他] )
        ///     step1. 新增到日結主表
        ///     step2. 拉前一步新增的資料 到 日結Temp表
        ///     step3. 拉準備要更新的資料到 日結Temp表
        ///     step4. 更新 日結Temp表
        /// </summary>
        public void doUpdateDailyTempTable(ToDoListModel todolist)
        {
            LogRecord.Save("DailySettlement", "2.doUpdateDailyTempTable", "");
        }

        /// <summary>
        /// 日結排程 step3 數據材積整理 - 基本資料整理(MoneySettleDetail)(一筆) + 
        ///                                 材積規格(CBM_Detail)(多筆)  to 日結Temp表 / 日結Detail Temp表
        /// </summary>
        public void doInsertMoneySettleDetail_CBM(ToDoListModel todolist)
        {
            LogRecord.Save("DailySettlement", "3.doInsertMoneySettleDetail_CBM", "");
        }

        /// <summary>
        /// 日結排程 step4 運費計算 to 日結Temp表 / 日結Detail Temp表
        /// </summary>
        public void doCalcContractSettleDetail(ToDoListModel todolist)
        {
            LogRecord.Save("DailySettlement", "4.doCalcContractSettleDetail", "");
        }

        /// <summary>
        /// 日結排程 step5  日結Temp表 -> 日結表 , 日結Detail Temp表 -> 日結Detail表
        ///     step1. 開 trans 更新 日結Temp表 -> 日結表 , 日結Detail Temp表 -> 日結Detail表
        ///     step2. 更新 todolist table 
        ///     step3. 更新 error check_number log table
        /// </summary>
        public void doUpdateTempToProd(ToDoListModel todolist)
        {
            LogRecord.Save("DailySettlement", "5.doUpdateTempToProd", "");
        }


        public void InsertRecord(JobRecord tobj)
        {
            try
            {
                using (var cn = new SqlConnection(DbWork.strConnLog))
                {


                    string strSQL = @"
INSERT INTO [dbo].[JobRecord]
           ([Type],[SubType],[Action],[Subject],[ContentBody],[LinkRelationData],[IsDoNotification],[MEMO])
     VALUES
           (@Type , @SubType , @Action , @Subject , @ContentBody , @LinkRelationData , @IsDoNotification , @MEMO  ) ";
                    cn.ExecuteScalar(strSQL, new
                    {
                        Type = tobj.Type,
                        SubType = tobj.SubType,
                        Action = tobj.Action,
                        Subject = tobj.Subject,
                        ContentBody = tobj.ContentBody,
                        LinkRelationData = tobj.LinkRelationData,
                        IsDoNotification = tobj.IsDoNotification,
                        MEMO = tobj.MEMO
                    });
                }
            }
            catch (Exception e)
            {

            }
        }
    }

    public interface IDailySettlementMain
    {
        public void doInsertDailyNewData();
    }
}
