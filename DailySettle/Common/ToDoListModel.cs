﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailySettle.Common
{
    public class ToDoListModel
    {
        /// <summary>
        /// 準備新增的 requestId or checkNumber
        /// </summary>
        public List<string> NewDataList = new List<string>();

        /// <summary>
        /// 準備更新的 requestId or checkNumber
        /// </summary>
        public List<string> UpdateDataList = new List<string>();

        /// <summary>
        /// 發生錯誤的 requestId or checkNumber
        /// </summary>
        public List<string> ErrorDataList = new List<string>();
    }


}
