﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DailySettle.Common
{
    public class LogRecord
    {

        public static void Save(string JobType, string DetailType, string Memo)
        {
            using (var _conn = new SqlConnection(DbWork.strConnFSE01))
            {
                try
                {
                    string sql =
                    @"  
                    INSERT into [dbo].[JobRecord] 
                    (
                      
	                  [JobType],
	                  [DetailType],
	                  [CreateTime],
	                  [Memo]

                    ) VALUES
                    (   
                      @JobType,
	                  @DetailType,
	                  getdate(),
	                  @Memo
                    )
                    ";
                    _conn.Execute(sql, new
                    {
                        JobType = JobType,
                        DetailType = DetailType,
                        Memo = Memo
                    });
                }
                catch (Exception e)
                {

                }


            }
        }

    }
}
