﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailySettle.Common
{
    public class JobRecord
    {
		public JobRecord(string strType, string strSubType, string strAction, string strSubject, string strContentBody,
				   string strLinkRelationData = "", int iIsDoNotification = 0, string strMEMO = "")
		{
			Type = strType;
			SubType = strSubType;
			Action = strAction;
			Subject = strSubject;
			ContentBody = strContentBody;
			LinkRelationData = strLinkRelationData;
			IsDoNotification = iIsDoNotification;
			MEMO = strMEMO;
		}
		public string Type = "";
		public string SubType = "";
		public string Action = "";
		public string Subject = "";
		public string ContentBody = "";
		public string LinkRelationData = "";
		public int IsDoNotification = 0;
		public string MEMO = "";
	}
}
