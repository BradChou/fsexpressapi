﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailySettle.Common
{
    public class DbWork
    {
        public static string strConnMain = "";
        public static string strConnFSE01 = "";
        public static string strConnLog = "";

        public enum CONN_TYPE
        {
            MAIN = 0,
            LOG = 1,
            FSE01 = 2
        }

        public static void setConn(string strConnType, string strConn)
        {
            if (strConnType.Equals(CONN_TYPE.MAIN.ToString()))
            {
                strConnMain = strConn;
            }
            else if (strConnType.Equals(CONN_TYPE.LOG.ToString()))
            {
                strConnLog = strConn;
            }
            else if (strConnType.Equals(CONN_TYPE.FSE01.ToString()))
            {
                strConnFSE01 = strConn;
            }
        }


    }
}
