﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.AddressParsing.Model
{
    public class ParseAddress_itri_Model
    {
        public ParseAddress_itri_Model()
        {
            CreateTime = DateTime.Now;
            IsArtificial = true;
            Active = true;
            Type = "9";
        }

        public string InitialAddressPre { get; set; }
        public string InitialAddress { get; set; }

        //郵遞區號
        public string ZipCode { get; set; }

        //縣市名
        public string CityName { get; set; }

        //區域名
        public string AreaName { get; set; }

        //路名
        public string RoadName { get; set; }

        //巷號
        public int LaneNo { get; set; }

        //巷號奇偶數
        // 0:偶數 1:奇數 2:沒有巷號
        public int LaneEven { get; set; }

        //弄號
        public int AlleyNo { get; set; }

        //弄號奇偶數
        // 0:偶數 1:奇數 2:沒有弄號
        public int AlleyEven { get; set; }

        //完整的門號, 含非數字
        //如: 萬華區東園街145-1號 > 145-1
        public string AddressOrigin { get; set; }
        //門號
        public int AddressNo { get; set; }
        //奇偶數判斷
        // 0:偶數 1:奇數 2:沒有門號
        public int AddressEven { get; set; }

        //原始註記樓層
        public string FloorOrigin { get; set; }

        //樓層
        public int FloorNo { get; set; }

        //第一個出現的號碼
        public int FstNo { get; set; }

        //第一個出現號碼的奇偶數
        public int FstEven { get; set; }

        //巷之後出現的第一個號碼
        public int SndNo { get; set; }

        //巷之後出現的第一個號碼的奇偶數
        public int SndEven { get; set; }

        public string StationCode { get; set; }

        public string StationName { get; set; }

        public string SalesDriverCode { get; set; }

        public string MotorcycleDriverCode { get; set; }

        public string StackCode { get; set; }

        public DateTime CreateTime { get; set; }

        public bool IsArtificial { get; set; }

        public bool Active { get; set; }

        public string Type { get; set; }

        public string ComeFrom { get; set; }

        public string CustomerCode { get; set; }

        public string SeqNO { get; set; }
        /// <summary>
        ///  接駁區代碼
        /// </summary>
        public string ShuttleStationCode { get; set; }


        public string SendSD { get; set; }
        public string SendMD { get; set; }
    }
}
