﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.AddressParsing.Model
{
    public class Map8
    {
        public List<string> html_attribution { get; set; }
        public List<Map8Result> results { get; set; }
        public QueryQuality queryQuality { get; set; }
        public string status { get; set; }
    }
    public class Geom
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }

    public class Geometry
    {
        public Location location;
    }
    public class Location
    {
        public decimal lat { get; set; }
        public decimal lng { get; set; }
    }

    public class ResultAnalysis
    {
        public List<int> statusCode { get; set; }
    }

    public class Map8Result
    {
        public string ZipCode { get; set; }
        public string formatted_address { get; set; }
        public string doorplateID { get; set; }
        public string postcode3 { get; set; }
        public string postcode33 { get; set; }
        public string city { get; set; }
        public string town { get; set; }
        public string village { get; set; }
        public string lin { get; set; }
        public string road { get; set; }
        public string hamlet { get; set; }
        public string lane { get; set; }
        public string alley { get; set; }
        public string lon { get; set; }
        public string num { get; set; }
        public string floor { get; set; }
        public string numAttr { get; set; }
        public string residenceID { get; set; }
        public string compType { get; set; }
        public string compDate { get; set; }
        public string trxDate { get; set; }
        public Geom geom { get; set; }
        public ResultAnalysis resultAnalysis { get; set; }
        public List<object> history { get; set; }



        public Geometry geometry { get; set; }
        public string id { get; set; }
        public string place_id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string level { get; set; }
        public decimal likelihood { get; set; }
        public string authoritative { get; set; }

        public decimal location_lat { get; set; }
        public decimal location_lng { get; set; }

        public string json { get; set; }
        public string InitialAddress { get; set; }
        public string InitialAddressPre { get; set; }
        public string StationCode { get; set; }

        public string StationName { get; set; }

        public string SalesDriverCode { get; set; }

        public string MotorcycleDriverCode { get; set; }

        public string StackCode { get; set; }
        /// <summary>
        ///  接駁區代碼
        /// </summary>
        public string ShuttleStationCode { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime EndTime { get; set; }

        public bool IsArtificial { get; set; }

        public bool Active { get; set; }

        public string Type_ { get; set; }

        public string ComeFrom { get; set; }

        public string CustomerCode { get; set; }

        public string SeqNO { get; set; }


    }

    public class QueryQuality
    {
        public List<int> statusCode { get; set; }
    }


}
