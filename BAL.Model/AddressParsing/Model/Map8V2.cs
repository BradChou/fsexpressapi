﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.AddressParsing.Model
{
    public class Map8V2
    {
        public string[] html_attribution { get; set; }
        public List<Map8V2Result> results { get; set; }
        public string status { get; set; }

    }

    public class Map8V2Result
    {
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string id { get; set; }
        public string place_id { get; set; }
        public string name { get; set; }
        public string city { get; set; }
        public string town { get; set; }
        public string type { get; set; }
        public string level { get; set; }
        public float likelihood { get; set; }
        public string authoritative { get; set; }


        public class Geometry
        {
            public Location location { get; set; }
        }

        public class Location
        {
            public float lat { get; set; }
            public float lng { get; set; }
        }
    }

   
}
