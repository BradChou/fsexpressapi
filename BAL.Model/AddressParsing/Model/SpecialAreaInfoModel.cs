﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.AddressParsing.Model
{
    public class SpecialAreaInfoModel
    {
        public string Address { get; set; }
        public string City { get; set; }
        public string Area { get; set; }
        public string Road { get; set; }
        public int? Alley { get; set; }
        public int? Lane { get; set; }
        public int? No { get; set; }
       
    }
}
