﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.AddressParsing.Req.Address
{
    public class GetSpecialAreaDetail_Req
    {
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 解析方式
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 來源
        /// </summary>
        public string ComeFrom { get; set; }
        /// <summary>
        /// 客戶代碼
        /// </summary>
        public string CustomerCode { get; set; }

    }
}
