﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.AddressParsing.Req.Notification
{
    public class SMSSend_Req
    {
        /// <summary>
        /// 來源
        /// </summary>
        public string ComeFrom { get; set; }
        /// <summary>
        /// 手機
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 標題
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 內容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 發送人
        /// </summary>
        public string SendUser { get; set; }
       
        /// <summary>
        /// 託運單號
        /// </summary>
        public string CheckNumber { get; set; }
    }
}
