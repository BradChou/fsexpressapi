﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.AddressParsing.Res.Address
{
    public class GetDetail_Res
    {
        /// <summary>
        /// 序號
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 鄉鎮區
        /// </summary>
        public string Town { get; set; }
        /// <summary>
        /// 路段
        /// </summary>
        public string Road { get; set; }
        /// <summary>
        /// 郵局三碼
        /// </summary>
        public string PostZip3 { get; set; }
        /// <summary>
        /// 郵局3+2
        /// </summary>
        public string PostZip32 { get; set; }
        /// <summary>
        /// 郵局3+3
        /// </summary>
        public string PostZip33 { get; set; }
        /// <summary>
        /// 站所代碼
        /// </summary>
        public string StationCode { get; set; }
        /// <summary>
        /// 站所名稱
        /// </summary>
        public string StationName { get; set; }
        /// <summary>
        /// 配送貨車司機
        /// </summary>
        public string SalesDriverCode { get; set; }
        /// <summary>
        /// 配送機車司機
        /// </summary>
        public string MotorcycleDriverCode { get; set; }
        /// <summary>
        ///  堆疊區
        /// </summary>
        public string StackCode { get; set; }

        /// <summary>
        ///  接駁區代碼
        /// </summary>
        public string ShuttleStationCode { get; set; }
        /// <summary>
        ///  集貨SD
        /// </summary>
        public string SendSalesDriverCode { get; set; }
        /// <summary>
        ///  集貨MD
        /// </summary>
        public string SendMotorcycleDriverCode { get; set; }





    }
}
