﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Model
{
    public class XlsxQ100ReportModel
    {
        /// <summary>
        /// PostEodParseFilterId
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 託運單號
        /// </summary>
        public string CheckNumber{get;set;}
        /// <summary>
        /// 郵局單號
        /// </summary>
        public string PostId { get; set; }
        /// <summary>
        /// 入帳日期
        /// </summary>
        public DateTime StatusDate { get; set; }
        /// <summary>
        /// 代收金額
        /// </summary>
        public int? Collection_money { get; set; }
    }
}
