﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Res.PostOffice
{
    public class GetPostFTPXMLFileJob_Res
    {
        public int count { get; set; }
        public GetPostFTPXMLFileJob_Res()
        {
            count = 0;
        }
    }
}
