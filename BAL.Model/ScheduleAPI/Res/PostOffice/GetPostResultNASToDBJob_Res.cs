﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Res.PostOffice
{
    public class GetPostResultNASToDBJob_Res
    {
        public int count { get; set; }
        public GetPostResultNASToDBJob_Res()
        {
            count = 0;
        }
    }

}
