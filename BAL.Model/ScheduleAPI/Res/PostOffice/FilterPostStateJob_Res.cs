﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Res.PostOffice
{
    public class FilterPostStateJob_Res
    {
        public int SuccessCount { get; set; }

        public int ExcludeCount { get; set; }
        public int ErrorCount { get; set; }

        public FilterPostStateJob_Res()
        {
            SuccessCount = 0;
            ErrorCount = 0;
            ExcludeCount = 0;
        }
    }
}
