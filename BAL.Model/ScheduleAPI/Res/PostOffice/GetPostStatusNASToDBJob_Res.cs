﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Res.PostOffice
{
    public class GetPostStatusNASToDBJob_Res
    {
        public int count { get; set; }
        public GetPostStatusNASToDBJob_Res()
        {
            count = 0;
        }
    }

}
