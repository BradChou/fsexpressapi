﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Res.PostOffice
{
    public class UploadPostOrderJob_Res
    {
        public int SuccessCount { get; set; }
        public UploadPostOrderJob_Res()
        {
            SuccessCount = 0;
        }
    }
}
