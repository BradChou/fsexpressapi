﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Res.PostOffice
{
    public class TransportPostFTPJob_Res
    {
        public int OrderCount { get; set; }
        public int ResultCount { get; set; }
        public int IrregularCount { get; set; }
        public int StatusCount { get; set; }

        public TransportPostFTPJob_Res()
        {
            OrderCount = 0;
            ResultCount = 0;
            IrregularCount = 0;
            StatusCount = 0;
        }
    }
}
