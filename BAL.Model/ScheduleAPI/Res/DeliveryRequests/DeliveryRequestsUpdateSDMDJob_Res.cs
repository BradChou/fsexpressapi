﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Res.DeliveryRequests
{
    public class DeliveryRequestsUpdateSDMDJob_Res
    {
        public int SuccessCount { get; set; }
        public int ErrorCount { get; set; }

        public DeliveryRequestsUpdateSDMDJob_Res()
        {
            SuccessCount = 0;
            ErrorCount = 0;
        }
    }
}
