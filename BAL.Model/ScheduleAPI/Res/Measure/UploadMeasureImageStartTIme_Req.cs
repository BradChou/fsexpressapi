﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Res.Measure
{
    public class UploadMeasureImageStartTime_Res
    {
        public DateTime startTime { get; set; }
    }
}
