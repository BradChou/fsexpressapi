﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Res.Measure
{
    public class UpdateMesureScanLogToMainTable_Res
    {
        public int NumToScanLog { get; set; }
        public int NumToDelieveryRequest { get; set; }
        public int NumToExclude { get; set; }
    }
}
