﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Res.Measure
{
    public class CBMDetailFilterJob_Res
    {
        public long StartId { get; set; }

        public long EndId { get; set; }

        public int SuccessCount { get; set; }
        public int ErrorCount { get; set; }

        public CBMDetailFilterJob_Res()
        {
            SuccessCount = 0;
            ErrorCount = 0;
            StartId = 0;
            EndId = 0;
        }
    }
}
