﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Res.Measure
{
    public class MesureScanLogFilterJob_Res
	{
        public int NumToExclude { get; set; }
        public int NumToFiltered { get; set; }
    }
}
