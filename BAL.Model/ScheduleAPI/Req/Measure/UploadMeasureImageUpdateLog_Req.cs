﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Req.Measure
{
    public class UploadMeasureImageUpdateLog_Req
    {
        public string DataSource { get; set; }
        public string SourcePath { get; set; }
        public string DestPath { get; set; }
        public string Status { get; set; }
        public DateTime cdate { get; set; }
        public string Message { get; set; }
        public DateTime lastWriteTime { get; set; }

        public UploadMeasureImageUpdateLog_Req()
        {
            cdate = DateTime.Now;
        }
    }
}
