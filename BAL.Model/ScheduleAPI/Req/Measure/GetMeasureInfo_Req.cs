﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Req.Measure
{
    public class GetMeasureInfo_Req
    {
        [Description("工作名稱")]
        public string taskName { get; set; }

        [Description("資料開始時間")]
        public DateTime cdateS { get; set; }
        [Description("資料結束時間")]
        public DateTime cdateE { get; set; }

    }
}
