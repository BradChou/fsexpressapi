﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.ScheduleAPI.Req.Measure
{
    public class GetScheduleTaskLog_Req
    {
        public string TaskName { get; set; }
        public string ApiName { get; set; }
    }
}
