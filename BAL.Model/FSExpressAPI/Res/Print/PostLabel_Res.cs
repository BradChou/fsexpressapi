﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Res.Print
{
    public class PostLabel_Res
    {
        public string Type { get; set; }
        public byte[] base64 { get; set; }
    }
}
