﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Res.PostOffice
{
    public class GetPostList_Res
    {
     
        public string PostId { get; set; }
        public string SerialNumber { get; set; }
        public string CommodityCode { get; set; }
        public string DiscernCode { get; set; }
        public string Zip3 { get; set; }
        public string CheckCode { get; set; }

        public string check_number { get; set; }
        public string receive_contact { get; set; }
        public string receive_city { get; set; }
        public string receive_area { get; set; }
        public string receive_address { get; set; }

        public string Collection_money { get; set; }
        public string Weight { get; set; }
        public DateTime CreateTime { get; set; }
       


    }
}
