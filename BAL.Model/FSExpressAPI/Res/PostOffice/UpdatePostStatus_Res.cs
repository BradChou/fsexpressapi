﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Res.PostOffice
{
    public class UpdatePostStatus_Res
    {
        public int SuccessCount { get; set; }
    

        public UpdatePostStatus_Res()
        {
            SuccessCount = 0;
           
        }
    }
}
