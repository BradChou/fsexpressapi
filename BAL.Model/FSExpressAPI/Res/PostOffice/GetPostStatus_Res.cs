﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Res.PostOffice
{
    public class GetPostStatus_Res
    {
        public string StatusCode { get; set; }
        public string StatusZH { get; set; }
        public string StationCode { get; set; }
        public string StationName { get; set; }
        public DateTime? Date { get; set; }
        
    }
}
