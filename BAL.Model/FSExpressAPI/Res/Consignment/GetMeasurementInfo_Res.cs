﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Res.Consignment
{
    public class GetMeasurementInfo_Res
    {
        public string MeasurementCode { get; set; }

        public string Memo { get; set; }
    }
}
