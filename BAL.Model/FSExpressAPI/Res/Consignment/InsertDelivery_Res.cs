﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Res.Consignment
{
    public class InsertDelivery_Res
    {

        public string CheckNumber { get; set; }
        public string SalesDriverCode { get; set; }
        public string MotorcycleDriverCode { get; set; }
        public string StackCode { get; set; }
        public string AreaCode { get; set; }
        public string SupplierCode { get; set; }
        /// <summary>
        ///  接駁區代碼
        /// </summary>
        public string ShuttleStationCode { get; set; }

        public string DeliveryType { get; set; }

        /// <summary>
        ///  集貨SD
        /// </summary>
        public string SendSalesDriverCode { get; set; }
        /// <summary>
        ///  集貨MD
        /// </summary>
        public string SendMotorcycleDriverCode { get; set; }
    }

}
