﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Res.Consignment
{
    public class GetDeliveryTracking_Res
    {
        public string State { get; set; }
        public string StateDetail { get; set; }
        public string HandleTime { get; set; }
        public string StationName { get; set; }
        public string DeliveryType { get; set; }
        public string ReturnCheckNumber { get; set; }
    }
}
