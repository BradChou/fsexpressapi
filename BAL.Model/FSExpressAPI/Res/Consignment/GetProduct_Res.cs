﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Res.Consignment
{
    public class GetProduct_Res
    {
        public string DeliveryType { get; set; }
        public string ProductId { get; set; }

        public string ProductName { get; set; }

        public List<SpecInfo> SpecList { get; set; }
    }

  
    }
