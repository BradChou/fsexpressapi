﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Res.Consignment
{
    public class ReverseDelivery_Res
    {
        public string CheckNumber { get; set; }
        public string SalesDriverCode { get; set; }
        public string MotorcycleDriverCode { get; set; }
        public string StackCode { get; set; }
        public string AreaCode { get; set; }
        public string SupplierCode { get; set; }
        public string DeliveryType { get; set; }

        public string SendSalesDriverCode { get; set; }

        public string SendMotorcycleDriverCode { get; set; }

    }



}
