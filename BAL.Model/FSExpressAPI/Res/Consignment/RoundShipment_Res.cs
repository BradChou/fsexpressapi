﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Res.Consignment
{
    public class RoundShipment_Res
    {
        public Forward Forward { get; set; }
        public Returned Returned { get; set; }
    }


    public class Forward
    {
        public string CheckNumber { get; set; }
        public string SalesDriverCode { get; set; }
        public string MotorcycleDriverCode { get; set; }
        public string StackCode { get; set; }
        public string AreaCode { get; set; }
        public string SupplierCode { get; set; }
    }

    public class Returned
    {
        public string CheckNumber { get; set; }
        public string SalesDriverCode { get; set; }
        public string MotorcycleDriverCode { get; set; }
        public string StackCode { get; set; }
        public string AreaCode { get; set; }
        public string SupplierCode { get; set; }
    }

}
