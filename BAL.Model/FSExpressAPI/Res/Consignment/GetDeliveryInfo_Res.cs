﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Res.Consignment
{
    public class GetDeliveryInfo_Res
    {

        public string orderNumber { get; set; }
        public DateTime? Ship_date { get; set; }
        public string Send_contact { get; set; }
        public string Send_tell { get; set; }
        public string Send_Address { get; set; }
        public string Area_arrive_code { get; set; }
        public string Receive_contact { get; set; }
        public string Receive_tell { get; set; }
        public string receive_address { get; set; }
        public string Arrive_assign_date { get; set; }
        public string Time_period { get; set; }
        public string pieces { get; set; }
        public string Collection_money { get; set; }
        public string Invoice_desc { get; set; }
        public string articlename { get; set; }


    }
}
