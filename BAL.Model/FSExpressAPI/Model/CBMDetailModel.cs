﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Model
{
    public class CBMDetailModel
    {
		public long id { get; set; }
		public string ComeFrom { get; set; }
		public string CheckNumber { get; set; }
		public string CBM { get; set; }
		public int Length { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
		public int ThreeSided { get; set; }
	}
}
