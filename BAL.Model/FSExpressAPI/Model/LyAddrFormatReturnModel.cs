﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Model
{
    public class LyAddrFormatReturnModel
    {
        public int? status_code { get; set; }
        public string status_msg { get; set; }
        public string city { get; set; }
        public string area { get; set; }
        public string village { get; set; }
        public string road { get; set; }
        public string lane { get; set; }
        public string alley { get; set; }
        public string sub_alley { get; set; }
        public string neig { get; set; }
        public string no { get; set; }
        public string address { get; set; }
    }
}
