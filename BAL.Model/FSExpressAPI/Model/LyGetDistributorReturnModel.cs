﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Model
{
   public class LyGetDistributorReturnModel
    {
        public string supplier_code { get; set; }
        public string supplier_name { get; set; }
        public string area_arrive_code { get; set; }
        public int? status_code { get; set; }
        public string status_msg { get; set; }
    }
}
