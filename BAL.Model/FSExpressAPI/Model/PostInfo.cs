﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Model
{
    public class PostInfo
    {
        public string PostId { get; set; }
        public string SerialNumber { get; set; }
        public string CommodityCode { get; set; }
        public string DiscernCode { get; set; }
        public string Zip3 { get; set; }
        public string CheckCode { get; set; }
    }
}
