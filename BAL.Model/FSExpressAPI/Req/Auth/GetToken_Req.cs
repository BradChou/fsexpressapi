﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Req.Auth
{
   public class GetToken_Req
    {
        public string Account { get; set; }
        public string Password { get; set; }
    }
}
