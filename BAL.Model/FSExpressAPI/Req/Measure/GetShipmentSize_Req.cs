﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Req.Measure
{
    public class GetShipmentSize_Req
    {
        public string CheckNumber { get; set; }
    }
}
