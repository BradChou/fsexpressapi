﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Req.Measure
{
   public class InsertCBMInfo_Req
    {
        public string ComeFrom { get; set; }
        public string CheckNumber { get; set; }
        public string CBM { get; set; }
        public int? Length { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
    
    }
}
