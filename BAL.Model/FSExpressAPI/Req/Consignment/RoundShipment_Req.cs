﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Req.Consignment
{
    public class RoundShipment_Req
    {
        public string CheckNumber { get; set; }
    }
}
