﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Req.Consignment
{
    public class InsertDeliveryForBusiness_Req
    {
        public string OrderNumber { get; set; }
        public string ReceiveContact { get; set; }
        public string ReceiveTel1 { get; set; }
        public string ReceiveTel2 { get; set; }
        public string ReceiveAddress { get; set; }
        public int Weight { get; set; }
        public int? PaymentMethod { get; set; }
        public int CollectionMoney { get; set; }
        public string SendContact { get; set; }
        public string SendTel { get; set; }
        public string SendAddress { get; set; }
        public int? CbmSize { get; set; }
        public string Arriveassigndate { get; set; }
        public int TimePeriod { get; set; }
        public string InvoiceDesc { get; set; }
        public string ArticleNumber { get; set; }
        public string SendPlatform { get; set; }
        public string ArticleName { get; set; }
        public int? DeliveryMethod { get; set; }
        public int? receiptFlag { get; set; }

        /// <summary>
        /// 數量
        /// </summary>
        public int? Pieces { get; set; }
        /// <summary>
        /// 貨物資訊
        /// </summary>
        public List<MeasurementInfo> MeasurementInfos { get; set; }
        /// <summary>
        /// 產品ID
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// 項目
        /// </summary>
        public string Spec { get; set; }
        /// <summary>
        /// 買保險
        /// </summary>
        public bool? BuyInsurance { get; set; }
        /// <summary>
        /// 報值費
        /// </summary>
        public int? Insurance { get; set; }
        public InsertDeliveryForBusiness_Req()
        {
            this.CollectionMoney = 0;
            this.Weight = 0;
            this.TimePeriod = 0;
            this.BuyInsurance = false;
            this.Insurance = 0;
        }
    }
}
