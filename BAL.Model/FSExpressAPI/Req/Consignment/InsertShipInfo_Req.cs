﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Req.Consignment
{
    public class InsertShipInfo_Req
    {
        public string checkNumber { get; set; }
        public DateTime? Scan_date { get; set; }
        public string status { get; set; }

        public string CompanyName { get; set; }
        public string Company_no { get; set; }
        public string Status_delivery { get; set; }
        public string Frontline_worker { get; set; }
        public string Station_name { get; set; }
        public string StationCode { get; set; }
        public string DataSource { get; set; }
    }
}
