﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Req.PostOffice
{
    public class UpdatePostStatus_Req
    {
        public string PostId { get; set; }
    }
}
