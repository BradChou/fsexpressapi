﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Req.PostOffice
{
    public class GetPostList_Req
    {

        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}
