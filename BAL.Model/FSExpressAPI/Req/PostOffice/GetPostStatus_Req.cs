﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model.FSExpressAPI.Req.PostOffice
{
    public class GetPostStatus_Req
    {
        public string PostId { get; set; }
    }
}
