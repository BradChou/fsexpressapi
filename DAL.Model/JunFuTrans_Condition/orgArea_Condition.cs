﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.JunFuTrans_Condition
{
    public class orgArea_Condition
    {
		public int id { get; set; }
		public string OFFICE { get; set; }
		public string ZIP3A { get; set; }
		public string ZIPCODE { get; set; }
		public string CITY { get; set; }
		public string AREA { get; set; }
		public string AREA1 { get; set; }
		public string ROAD { get; set; }
		public string SCOOP { get; set; }
		public double EVEN { get; set; }
		public string CMP_LABLE { get; set; }
		public double LANE { get; set; }
		public string LANE1 { get; set; }
		public double ALLEY { get; set; }
		public string ALLEY1 { get; set; }
		public double NO_BGN { get; set; }
		public string NO_BGN1 { get; set; }
		public double NO_END { get; set; }
		public string NO_END1 { get; set; }
		public string FLOOR { get; set; }
		public string FLOOR1 { get; set; }
		public string ROAD_NO { get; set; }
		public string ROAD1 { get; set; }
		public string EROAD { get; set; }
		public string RMK { get; set; }
		public string RMK1 { get; set; }
		public double ZIP3RMK { get; set; }
		public string ECITY { get; set; }
		public string EAREA { get; set; }
		public string UWORD { get; set; }
		public string ISN { get; set; }
		public string station_scode { get; set; }
		public string station_name { get; set; }
		public string md_no { get; set; }
		public string sd_no { get; set; }
		public string put_order { get; set; }
		//itri排序
		public string SeqNO { get; set; }
		public string SendSD { get; set; }
		public string SendMD { get; set; }


		/// <summary>
		///  接駁區代碼
		/// </summary>
		public string ShuttleStationCode { get; set; }

		
	}
}
