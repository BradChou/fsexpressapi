﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
	public class MoneySettleA_Condition
	{
		public long id { get; set; }
		public string CustomerCode { get; set; }
		public string Station { get; set; }
		public string ProductId { get; set; }
		public string Spec { get; set; }
		public string DeliveryType { get; set; }
		public bool IsActive { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime UpdateDate { get; set; }
		public string CreateUser { get; set; }
		public string UpdateUser { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
	}
}
