﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
	public class AdditionalFeeManageScope_Condition
	{
		public long id { get; set; }
		public long AdditionalFeeManageId { get; set; }
		public int StartPrice { get; set; }
		public int EndPrice { get; set; }
		public int Valuation { get; set; }
		public DateTime CreateDate { get; set; }
	}
}
