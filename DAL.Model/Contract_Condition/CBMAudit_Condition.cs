﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
    public class CBMAudit_Condition
    {
		public long id { get; set; }
		public string DriverCode { get; set; }
		public string DriverStation { get; set; }
		public string CheckNumber { get; set; }
		public string RequestId { get; set; }
		public int OriginCbmSizeLength { get; set; }
		public int OriginCbmSizeWidth { get; set; }
		public int OriginCbmSizeHeight { get; set; }
		public int? CbmSizeLength { get; set; }
		public int? CbmSizeWidth { get; set; }
		public int? CbmSizeHeight { get; set; }
		public string CheckPic_1 { get; set; }
		public string CheckPic_2 { get; set; }
		public string CheckPic_3 { get; set; }
		public int Status { get; set; }
		public DateTime CreateTime { get; set; }
		public string CreateUser { get; set; }
		public DateTime AuditTime { get; set; }
		public string AuditUser { get; set; }

	}
}
