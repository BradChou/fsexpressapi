﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
    public class ItemCodes_Conditions
    {
		public long id { get; set; }
		public string SystemType { get; set; }
		public string CodeType { get; set; }
		public string CodeId { get; set; }
		public string CodeName { get; set; }
		public int OrderBy { get; set; }
		public string Memo { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime UpdateDate { get; set; }
		public string CreateUser { get; set; }
		public string UpdateUser { get; set; }
	}
}
