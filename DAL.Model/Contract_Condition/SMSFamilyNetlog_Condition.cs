﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
    public class SMSFamilyNetlog_Condition
    {
		public long Id { get; set; }
		public string RequestBody { get; set; }
		public string ResponseBody { get; set; }
		public string RequestId { get; set; }
		public string StatusCode { get; set; }
		public string StatusDesc { get; set; }
		
		public DateTime StartTime { get; set; }
		public DateTime EndTime { get; set; }
		public DateTime CreateTime { get; set; }
	}
}
