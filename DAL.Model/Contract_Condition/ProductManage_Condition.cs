﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
    public class ProductManage_Condition
    {

        public string id { get; set; }

        /// <summary>
        /// 產品編號
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// 產品名稱
        /// </summary>
        public string Name { get; set; }
        public string PriceMode { get; set; }

        public string Type { get; set; }

        public string IsActive { get; set; }

        /// <summary>
        /// 創建者
        /// </summary>
        public string CreateUser { get; set; }



        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        public string UpdateUser { get; set; }

        public DateTime UpdateDate { get; set; }

    }
}
