﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
    public class SMSUsing_Condition
    {
        public long Id { get; set; }
        public long SMSId { get; set; }
        public string ComeFrom { get; set; }
        public string CheckNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }
    }

    public enum SMSUsingComeFromEnum
    {
        ButterflyWeb = 1,
        JunfuWebservice = 2,
        OLDOrangeAPI = 3,
        NEWOrangeAPI = 4,
        PublicAPI = 5,
        APPWebservice = 6,
        Itri = 55,
        Other = 99,
        Test = 777
    }
}
