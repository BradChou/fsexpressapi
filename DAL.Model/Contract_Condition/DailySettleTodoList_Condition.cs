﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
    public class DailySettleTodoList_Condition
    {
        public long Id { get; set; }
        public long RequestId { get; set; }
        public string CheckNumber { get; set; }
        public string ActionType { get; set; }
        public bool IsDo { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
