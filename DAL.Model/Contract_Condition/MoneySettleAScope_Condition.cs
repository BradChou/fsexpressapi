﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
    public class MoneySettleAScope_Condition
    {
		public long id { get; set; }
		public long MoneySettleAId { get; set; }
		public int StartNum { get; set; }
		public int EndNum { get; set; }
		public int Valuation { get; set; }
		public DateTime CreateDate { get; set; }
	}
}
