﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
	public class SettleA_Fee_Condition
	{
		public long id { get; set; }
		public long Detail_Id { get; set; }
		public int SN { get; set; }
		public int? DD_Fee { get; set; }
		public int? Collection_Money { get; set; }
		public int? ReceiptFlag_Fee { get; set; }
		public int? Valued_Fee { get; set; }
		public int? OverCbm_Fee { get; set; }
		public int? Total { get; set; }
		public string CreateUser { get; set; }
		public DateTime CreateDate { get; set; }
		public SettleA_Fee_Condition()
		{
			CreateUser = "ADMIN";
		}

	}
}
