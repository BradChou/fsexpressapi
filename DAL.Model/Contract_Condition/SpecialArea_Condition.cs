﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
	public class SpecialArea_Condition
	{
		public long id { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string Town { get; set; }
		public string Village { get; set; }
		public string Lin { get; set; }
		public string Road { get; set; }
		public string Hamlet { get; set; }
		public string Lane { get; set; }
		public string Alley { get; set; }
		public string Num { get; set; }
		public string KeyName { get; set; }
		public int ArriveCode { get; set; }
		public int Fee { get; set; }
		public string TimeLimit { get; set; }
		public string CreateUser { get; set; }
		public DateTime CreateDate { get; set; }
	}
}
