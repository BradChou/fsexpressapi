﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
    public class SpecialAreaAudit_Condition
    {
		public long id { get; set; }
		public string DriverCode { get; set; }
		public string DriverStation { get; set; }
		public string CheckNumber { get; set; }
		public string RequestId { get; set; }
		public string Addresss { get; set; }
		public int SpecialAreaFee { get; set; }
		public int SpecialAreaId { get; set; }
		public string ReasonMemo { get; set; }
		public int Status { get; set; }
		public DateTime CreateTime { get; set; }
		public string CreateUser { get; set; }
		public DateTime AuditTime { get; set; }
		public string AuditUser { get; set; }
		public string CheckPic_1 { get; set; }
		public string City { get; set; }
		public string Area { get; set; }
		public string Road { get; set; }
	}
}
