﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
	public class DailyCalErrMsgLog_Condition
    {
		public long id { get; set; }
		public long DailyMoneySettleDetailId { get; set; }
		public string CheckNumber { get; set; }
		public string ProductId { get; set; }
		public string CBM { get; set; }
		public string ErrMsg { get; set; }
		public DateTime CreateDate { get; set; }
		public DailyCalErrMsgLog_Condition()
		{
			CreateDate = DateTime.Now;
		}
	}
}
