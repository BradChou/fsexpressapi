﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
    public class SMSInfo_Condition
    {
		public long Id { get; set; }
		public string Mobile { get; set; }
		public string Title { get; set; }
		public string Content { get; set; }
		public string Status { get; set; }
		public DateTime CreateDate { get; set; }
		public string CreateUser { get; set; }
		public string UpdateUser { get; set; }
		public DateTime UpdateDate { get; set; }
	}
}
