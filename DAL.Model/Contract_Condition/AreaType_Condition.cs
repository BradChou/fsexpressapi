﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Contract_Condition
{
    public class AreaType_Condition
    {
        public long id { get; set; }
        public string City { get; set; }
        public string Area { get; set; }
        public string Type { get; set; }
    }
}
