﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.JunFuAddress_Condition
{
    public class Map8AddressDismantleV2_Condition
    {
		public long id { get; set; }
		public string o_address { get; set; }
		public string formatted_address { get; set; }
		public string place_id { get; set; }
		public string name { get; set; }
		public string city { get; set; }
		public string town { get; set; }
		public string type { get; set; }
		public string level { get; set; }
		public double likelihood { get; set; }
		public string authoritative { get; set; }
		public string geomJson { get; set; }
		public DateTime CreateTime { get; set; }
		public DateTime StartTime { get; set; }
		public DateTime EndTime { get; set; }
	}
}
