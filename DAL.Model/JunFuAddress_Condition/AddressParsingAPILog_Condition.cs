﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.JunFuAddress_Condition
{
    public class AddressParsingAPILog_Condition
    {
        public long id { get; set; }
        public string Guid { get; set; }
        public string Address { get; set; }
        public string CustomerCode { get; set; }
        public string RequestBody { get; set; }
        public string ResponseBody { get; set; }
        public string RequestHeader { get; set; }
        public string Path { get; set; }
        public string QueryString { get; set; }
        public DateTime StratTime { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
