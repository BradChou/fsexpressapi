﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.JunFuAddress_Condition
{
    public class AddressUsingLog_Condition
    {
        public long id { get; set; }
        public long AddressResultId { get; set; }
        public string ComeFrom { get; set; }
        public string CustomerCode { get; set; }
        public DateTime CreateTime { get; set; }
    }

    public enum AddressParsingTypeEnum
    {
        Lv1 = 1,
        Lv2 = 2,
        Lv3 = 3
    }
    public enum AddressParsingComeFromEnum
    {
        ButterflyWeb = 1,
        JunfuWebservice = 2,
        OLDOrangeAPI = 3,
        NEWOrangeAPI = 4,
        PublicAPI = 5,
        Job = 6,
        Itri = 55,
        Other = 99,
        Test = 777
    }

}
