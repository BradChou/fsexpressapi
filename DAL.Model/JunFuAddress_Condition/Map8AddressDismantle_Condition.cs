﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.JunFuAddress_Condition
{
    public class Map8AddressDismantle_Condition
    {
        public long id { get; set; }
        public string o_address { get; set; }
        public int? number { get; set; }
        public string formatted_address { get; set; }
        public string postcode3 { get; set; }
        public string postcode33 { get; set; }
        public string doorplateID { get; set; }
        public string city { get; set; }
        public string town { get; set; }
        public string village { get; set; }
  
        public string lin { get; set; }
        public string road { get; set; }
        public string hamlet { get; set; }
        public string lane { get; set; }
        public string alley { get; set; }
        public string lon { get; set; }
        public string num { get; set; }
        public string floor { get; set; }
        public string numAttr { get; set; }
        public string residenceID { get; set; }
        public string compType { get; set; }
        public string compDate { get; set; }
        public string trxDate { get; set; }
        public string geomJson { get; set; }
        public string resultAnalysisJson { get; set; }
        public string history { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }

        
    }
}
