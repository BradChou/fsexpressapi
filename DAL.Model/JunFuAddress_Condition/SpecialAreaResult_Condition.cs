﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.JunFuAddress_Condition
{
    public class SpecialAreaResult_Condition
    {
		public long id { get; set; }
		public string InitialAddress { get; set; }
		
		public string City { get; set; }
		public string Area { get; set; }
		public string Road { get; set; }
		public int? Lane { get; set; }
		public int? Alley { get; set; }
		public int? No { get; set; }
		public long SpecialAreaManageId { get; set; }
		public string KeyWord { get; set; }
		public string StationCode { get; set; }
		public string SalesDriverCode { get; set; }
		public string MotorcycleDriverCode { get; set; }
		public string StackCode { get; set; }
		public string ShuttleStationCode { get; set; }

		public double? Fee { get; set; }
		public int? TimeLimit { get; set; }
		public DateTime CreateTime { get; set; }
	}
}
