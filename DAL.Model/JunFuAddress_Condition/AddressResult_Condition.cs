﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.JunFuAddress_Condition
{
    public class AddressResult_Condition
    {
		public long id { get; set; }
		
		public string InitialAddress { get; set; }
		public string PostZip3 { get; set; }
		public string PostZip32 { get; set; }
		public string PostZip33 { get; set; }
		public string Map8_formatted_address { get; set; }
		public string Map8_city { get; set; }
		public string Map8_town { get; set; }
		public string Map8_village { get; set; }
		public string Map8_lin { get; set; }
		public string Map8_road { get; set; }
		public string Map8_hamlet { get; set; }
		public string Map8_lane { get; set; }
		public string Map8_alley { get; set; }
		public string Map8_lon { get; set; }
		public string Map8_num { get; set; }
		public string Map8_floor { get; set; }
		public string Map8_numAttr { get; set; }
		public string JF_address { get; set; }
		public string JF_city { get; set; }
		public string JF_area { get; set; }
		public string JF_village { get; set; }
		public string JF_road { get; set; }
		public string JF_lane { get; set; }
		public string JF_alley { get; set; }
		public string JF_sub_alley { get; set; }
		public string JF_neig { get; set; }
		public string JF_no { get; set; }
		public string StationCode { get; set; }
		public string StationName { get; set; }
		public string SalesDriverCode { get; set; }
		public string MotorcycleDriverCode { get; set; }
		public string StackCode { get; set; }
		/// <summary>
		///  接駁區代碼
		/// </summary>
		public string ShuttleStationCode { get; set; }

		/// <summary>
		///  集貨SD
		/// </summary>
		public string SendSalesDriverCode { get; set; }
		/// <summary>
		///  集貨MD
		/// </summary>
		public string SendMotorcycleDriverCode { get; set; }

		public DateTime CreateTime { get; set; }
	}
}
