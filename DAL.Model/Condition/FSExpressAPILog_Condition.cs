﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
	public class FSExpressAPILog_Condition
    {
		public long Id { get; set; }
		public string Guid { get; set; }
		public string Account { get; set; }
		public string RequestBody { get; set; }
		public string ResponseBody { get; set; }
		public string RequestHeader { get; set; }
		public string Path { get; set; }
		public string QueryString { get; set; }
		public DateTime StratTime { get; set; }
		public DateTime EndTime { get; set; }
		public DateTime CreateTime { get; set; }
	}
}
