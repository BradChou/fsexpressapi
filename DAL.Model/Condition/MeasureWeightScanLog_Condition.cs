﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
	public class MeasureWeightScanLog_Condition
	{
		public long id { get; set; }
		public string DataSource { get; set; }
		public string Scancode { get; set; }
		public string Weight { get; set; }
		public DateTime cdate { get; set; }
	}
}
