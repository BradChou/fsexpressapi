﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class DailyCBMDetail_Temp_Condition
    {
		public long id { get; set; }
		public long DetailID { get; set; }
		public int SN { get; set; }
		public string CBM { get; set; }
		public int? Length { get; set; }
		public int? Width { get; set; }
		public int? Height { get; set; }
		public DateTime CreateDate { get; set; }
		public string CreateUser { get; set; }
		public int OverCBM { get; set; }

		public static implicit operator DailyCBMDetail_Temp_Condition(DailyCBMDetail_Condition data)
		{
			return new DailyCBMDetail_Temp_Condition
			{
				id = data.id,
				DetailID = data.DetailID,
				SN = data.SN,
				CreateDate = data.CreateDate,
				CBM = data.CBM,
				CreateUser = data.CreateUser,
				Height = data.Height,
				Length = data.Length,
				Width = data.Width,
				OverCBM = data.OverCBM

			};
		}
	}

	
}
