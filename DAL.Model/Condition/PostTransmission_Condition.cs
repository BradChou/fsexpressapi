﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class PostTransmission_Condition
    {
        public long id { get; set; } //ID
        public long id_forPost { get; set; } //ID
        public string PostId { get; set; } //郵件編號
        public string check_number { get; set; } //客戶訂單編號
        public string post_code { get; set; } //客戶編號
        public string is_collection_money { get; set; } //是否代收貨款
        public string collection_money { get; set; } //代收貨價金額
        public string receive_name { get; set; } //收件人姓名
        public string receive_tel { get; set; } //收件人電話
        public string zip3 { get; set; }  //收件人郵遞區號
        public string receive_address { get; set; }  //收件人地址
        public string upload_date { get; set; } //傳檔日期
        public string bag_no { get; set; }  //包裝袋號
        public string bag_weight { get; set; }  //包裝袋重
        public string serial_no { get; set; }  // 郵件序號
        public string weight { get; set; }  //郵件重量
        public string container_no { get; set; }  //櫃體編號
    }
}
