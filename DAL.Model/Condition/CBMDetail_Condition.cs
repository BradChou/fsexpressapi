﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class CBMDetail_Condition
    {
		public long id { get; set; }
		public long CBMDetailLogId { get; set; }
		public string ComeFrom { get; set; }
		public string CheckNumber { get; set; }
		public string CBM { get; set; }
		public int? Length { get; set; }
		public int? Width { get; set; }
		public int? Height { get; set; }
		public DateTime CreateDate { get; set; }
	}
}
