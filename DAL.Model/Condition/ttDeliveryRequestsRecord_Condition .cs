﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class ttDeliveryRequestsRecord_Condition
	{
		public long id { get; set; }
		public DateTime create_date { get; set; }
		public string create_user { get; set; }
		public DateTime modify_date { get; set; }
		public string modify_user { get; set; }
		public DateTime delete_date { get; set; }
		public string delete_user { get; set; }
		public decimal request_id { get; set; }
		public string pricing_type { get; set; }
		public string customer_code { get; set; }
		public string check_number { get; set; }
		public string check_type { get; set; }
		public string order_number { get; set; }
		public string receive_customer_code { get; set; }
		public string subpoena_category { get; set; }
		public string receive_tel1 { get; set; }
		public string receive_tel1_ext { get; set; }
		public string receive_tel2 { get; set; }
		public string receive_contact { get; set; }
		public string receive_city { get; set; }
		public string receive_area { get; set; }
		public string receive_address { get; set; }
		public string area_arrive_code { get; set; }
		public bool receive_by_arrive_site_flag { get; set; }
		public string arrive_address { get; set; }
		public int pieces { get; set; }
		public int plates { get; set; }
		public int cbm { get; set; }
		public int collection_money { get; set; }
		public int arrive_to_pay_freight { get; set; }
		public int arrive_to_pay_append { get; set; }
		public string send_contact { get; set; }
		public string send_tel { get; set; }
		public string send_city { get; set; }
		public string send_area { get; set; }
		public string send_address { get; set; }
		public bool donate_invoice_flag { get; set; }
		public bool electronic_invoice_flag { get; set; }
		public string uniform_numbers { get; set; }
		public string arrive_mobile { get; set; }
		public string arrive_email { get; set; }
		public string invoice_memo { get; set; }
		public string invoice_desc { get; set; }
		public string product_category { get; set; }
		public string special_send { get; set; }
		public DateTime arrive_assign_date { get; set; }
		public string time_period { get; set; }
		public bool receipt_flag { get; set; }
		public bool pallet_recycling_flag { get; set; }
		public string supplier_code { get; set; }
		public string supplier_name { get; set; }
		public DateTime supplier_date { get; set; }
		public int receipt_numbe { get; set; }
		public int supplier_fee { get; set; }
		public int csection_fee { get; set; }
		public int remote_fee { get; set; }
		public int total_fee { get; set; }
		public DateTime print_date { get; set; }
		public bool print_flag { get; set; }
		public DateTime checkout_close_date { get; set; }
		public string sub_check_number { get; set; }
		public string import_randomCode { get; set; }
		public string close_randomCode { get; set; }
		public DateTime record_date { get; set; }
		public string record_action { get; set; }
		public string record_memo { get; set; }

	}
}
