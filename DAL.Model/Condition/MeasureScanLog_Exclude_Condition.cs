﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class MeasureScanLog_Exclude_Condition
    {
        public long id { get; set; }
        public long MeasureScanLogId { get; set; }
        public string DataSource { get; set; }
        public string Scancode { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string Size { get; set; }
        public string Status { get; set; }
        public DateTime? ScanTime { get; set; }
        public string Picture { get; set; }
        public DateTime cdate { get; set; }

        public static implicit operator MeasureScanLog_Exclude_Condition(MeasureScanLog_Filtered_Condition data)
        {
            return new MeasureScanLog_Exclude_Condition
            {
                MeasureScanLogId = data.MeasureScanLogId,
                DataSource = data.DataSource,
                Scancode = data.Scancode,
                Length = data.Length,
                Width = data.Width,
                Height = data.Height,
                Status = data.Status,
                Size = data.Size,
                ScanTime = data.ScanTime,
                Picture = data.Picture,
                cdate = DateTime.Now
            };
        }
    }
}
