﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class PostXmlParse_Condition
    {
		public long id { get; set; }
		public string PostNo { get; set; }
		public string PostStateNo { get; set; }
		public string PostStateZH { get; set; }
		public DateTime ProcessDateTime { get; set; }
		public long PostXml_id { get; set; }
		public DateTime CreateTime { get; set; }
	}
}
