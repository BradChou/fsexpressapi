﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class DailySettleToDoListModel
    {
        /// <summary>
        /// 準備新增的 checkNumber
        /// </summary>
        public List<DailySettleSingleToDo> NewDataList = new List<DailySettleSingleToDo>();

        /// <summary>
        /// 準備更新的 checkNumber
        /// </summary>
        public List<DailySettleSingleToDo> UpdateDataList = new List<DailySettleSingleToDo>();

        /// <summary>
        /// 發生錯誤的 checkNumber
        /// </summary>
        public List<DailySettleSingleToDo> ErrorDataList = new List<DailySettleSingleToDo>();

    }

    public class DailySettleSingleToDo
    {
        public DailySettleSingleToDo()
        {

        }

        public DailySettleSingleToDo(string requestid ,  string checknumber , string actiontype)
        {
            RequestId = requestid;
            CheckNumber = checknumber;
            ActionType = actiontype;
        }
        public string RequestId { get; set; }
        public string CheckNumber { get; set; }
        public string ActionType { get; set; }


    }


}
