﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
	public class pickup_request_for_apiuser_Condition
    {
		public int id { get; set; }
		public string check_number { get; set; }
		public string customer_code { get; set; }
		public int pieces { get; set; }
		public DateTime request_date { get; set; }
		public string send_city { get; set; }
		public string send_area { get; set; }
		public string send_road { get; set; }
		public string supplier_code { get; set; }
		public string send_tel { get; set; }
		public string sd { get; set; }
		public string md { get; set; }
		public string putorder { get; set; }
		public string reassign_md { get; set; }
		public string md_uuser { get; set; }
		public string reassign_sd { get; set; }

		/// <summary>
		///  接駁區代碼
		/// </summary>
		public string ShuttleStationCode { get; set; }

	}
}
