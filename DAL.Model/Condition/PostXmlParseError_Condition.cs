﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{

    public class PostXmlParseError_Condition
    {
        public long id { get; set; }
        public long PostXmlParseId { get; set; }
        public string PostNo { get; set; }
        public string PostStateNo { get; set; }
        public string PostStateZH { get; set; }
        public DateTime ProcessDateTime { get; set; }
        public long PostXml_id { get; set; }
        public DateTime CreateTime { get; set; }
        public string ErrMsg { get; set; }
        public int Number { get; set; }
        public int Status { get; set; }
        

        public PostXmlParseError_Condition()
        {
            this.Number = 1;
        }


        public static implicit operator PostXmlParseError_Condition(PostXmlParse_Condition data)
        {
            return new PostXmlParseError_Condition
            {
                PostXmlParseId = data.id,
                PostNo = data.PostNo,
                PostStateNo = data.PostStateNo,
                PostStateZH = data.PostStateZH,
                ProcessDateTime = data.ProcessDateTime,
                PostXml_id = data.PostXml_id
            };
        }

        public static implicit operator PostXmlParseError_Condition(PostXmlParseFilter_Condition data)
        {
            return new PostXmlParseError_Condition
            {
                PostXmlParseId = data.id,
                PostNo = data.PostNo,
                PostStateNo = data.PostStateNo,
                PostStateZH = data.PostStateZH,
                ProcessDateTime = data.ProcessDateTime,
                PostXml_id = data.PostXml_id
            };
        }
    }
}
