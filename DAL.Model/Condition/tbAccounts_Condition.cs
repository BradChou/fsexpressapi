﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class tbAccounts_Condition
    {
		public decimal account_id { get; set; }
		public string account_code { get; set; }
		public string password { get; set; }
		public bool active_flag { get; set; }
		public string head_code { get; set; }
		public string body_code { get; set; }
		public string master_code { get; set; }
		public string manager_type { get; set; }
		public string manager_unit { get; set; }
		public string job_title { get; set; }
		public string emp_code { get; set; }
		public string user_name { get; set; }
		public string user_email { get; set; }
		public string cuser { get; set; }
		public DateTime cdate { get; set; }
		public string uuser { get; set; }
		public DateTime udate { get; set; }
		public bool account_type { get; set; }
		public string customer_code { get; set; }
		public bool password_changed { get; set; }

	}
}
