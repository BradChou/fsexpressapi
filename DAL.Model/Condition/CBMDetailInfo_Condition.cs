﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class CBMDetailInfo_Condition
    {
		public long id { get; set; }
		public string CheckNumber { get; set; }
		public string Status { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime UpdateDate { get; set; }

	}
}
