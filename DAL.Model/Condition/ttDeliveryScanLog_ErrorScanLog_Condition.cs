﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class ttDeliveryScanLog_ErrorScanLog_Condition
    {
        public decimal log_id { get; set; }
        public string driver_code { get; set; }
        public string check_number { get; set; }
        public string scan_item { get; set; }
        public DateTime? scan_date { get; set; }
        public string area_arrive_code { get; set; }
        public string platform { get; set; }
        public string car_number { get; set; }
        public string sowage_rate { get; set; }
        public string area { get; set; }
        public string ship_mode { get; set; }
        public string goods_type { get; set; }
        public int? pieces { get; set; }
        public double? weight { get; set; }
        public int? runs { get; set; }
        public int? plates { get; set; }
        public string exception_option { get; set; }
        public string arrive_option { get; set; }
        public string sign_form_image { get; set; }
        public string sign_field_image { get; set; }
        public DateTime? deliveryupload { get; set; }
        public DateTime? photoupload { get; set; }
        public DateTime? cdate { get; set; }
        public string cuser { get; set; }
        public string tracking_number { get; set; }
        public int? Del_status { get; set; }
        public int? VoucherMoney { get; set; }
        public int? CashMoney { get; set; }
        public bool? write_off_type { get; set; }
        public string receive_option { get; set; }
        public string send_option { get; set; }
        public string delivery_option { get; set; }
        public string option_category { get; set; }
        public string error_msg { get; set; }
        public static implicit operator ttDeliveryScanLog_ErrorScanLog_Condition(ttDeliveryScanLog_Condition data)
        {
            return new ttDeliveryScanLog_ErrorScanLog_Condition()
            {
                driver_code = data.driver_code,
                check_number = data.check_number,
                scan_item = data.scan_item,
                scan_date = data.scan_date,
                area_arrive_code = data.area_arrive_code,
                platform = data.platform,
                car_number = data.car_number,
                sowage_rate = data.sowage_rate,
                area = data.area,
                ship_mode = data.ship_mode,
                goods_type = data.goods_type,
                pieces = data.pieces,
                weight = data.weight,
                runs = data.runs,
                plates = data.plates,
                exception_option = data.exception_option,
                arrive_option = data.arrive_option,
                sign_form_image = data.sign_form_image,
                sign_field_image = data.sign_field_image,
                deliveryupload = data.deliveryupload,
                photoupload = data.photoupload,
                cdate = DateTime.Now,
                cuser = data.cuser,
                tracking_number = data.tracking_number,
                Del_status = data.Del_status,
                VoucherMoney = data.VoucherMoney,
                CashMoney = data.CashMoney,
                write_off_type = data.write_off_type,
                receive_option = data.receive_option,
                send_option = data.send_option,
                delivery_option = data.delivery_option,
                option_category = data.option_category
            };
        }

    }
}
