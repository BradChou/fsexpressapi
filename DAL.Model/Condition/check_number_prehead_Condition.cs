﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class check_number_prehead_Condition
    {
        public int id { get; set; }
        public string product_type { get; set; }
        public string check_number_prehead { get; set; }
        public string description { get; set; }
    }
}
