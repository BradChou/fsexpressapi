﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class DeliveryStatusPublic_Condition
    {
        public long id { get; set; }
        public string DataSource { get; set; }
        public string CheckNumber { get; set; }
        public string DeliveryStatus { get; set; }
        
        public DateTime ScanDate { get; set; }
        public string CompanyName { get; set; }
        public string TaxIDNumber { get; set; }
        public string DeliveryStatusCode { get; set; }
        public string WorkerName { get; set; }
        public string StationCode { get; set; }
        public string StationName { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
