﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class Post_request_Condition
    {
        public decimal request_id { get; set; }
        public decimal jf_request_id { get; set; }
        public string jf_check_number { get; set; }
        public string post_number { get; set; }
        public string zipcode5 { get; set; }
        public string zipcode6 { get; set; }
        public string check_code { get; set; }
        public DateTime cdate { get; set; }
        public DateTime udate { get; set; }
        public string cuser { get; set; }
        public string uuser { get; set; }
    }
}
