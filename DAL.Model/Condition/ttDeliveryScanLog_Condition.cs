﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class ttDeliveryScanLog_Condition
    {
        public decimal log_id { get; set; }
        public string driver_code { get; set; }
        public string check_number { get; set; }
        public string scan_item { get; set; }
        public DateTime? scan_date { get; set; }
        public string area_arrive_code { get; set; }
        public string platform { get; set; }
        public string car_number { get; set; }
        public string sowage_rate { get; set; }
        public string area { get; set; }
        public string ship_mode { get; set; }
        public string goods_type { get; set; }
        public int? pieces { get; set; }
        public double? weight { get; set; }
        public int? runs { get; set; }
        public int? plates { get; set; }
        public string exception_option { get; set; }
        public string arrive_option { get; set; }
        public string sign_form_image { get; set; }
        public string sign_field_image { get; set; }
        public DateTime? deliveryupload { get; set; }
        public DateTime? photoupload { get; set; }
        public DateTime? cdate { get; set; }
        public string cuser { get; set; }
        public string tracking_number { get; set; }
        public int? Del_status { get; set; }
        public int? VoucherMoney { get; set; }
        public int? CashMoney { get; set; }
        public bool? write_off_type { get; set; }
        public string receive_option { get; set; }
        public string send_option { get; set; }
        public string delivery_option { get; set; }
        public string option_category { get; set; }
        public string station_name { get; set; }
        public string driver_station_name { get; set; }

        public string DeliveryType { get; set; }

        public string ReturnCheckNumber { get; set; }
    }
}
