﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class PostEod_Condition
    {
		public long id { get; set; }
		
		public string EodName { get; set; }
		public string EodPath { get; set; }
		public string EodContent { get; set; }
		public DateTime FileCreateTime { get; set; }
		public DateTime CreateTime { get; set; }
		public bool? Fail { get; set; }
		public string FailMessage { get; set; }
		public int FailCount { get; set; }

		public PostEod_Condition()
		{
			this.EodContent = string.Empty;
			this.Fail = false;
			this.FailMessage = string.Empty;
			this.FailCount = 0;
		}
	}
}
