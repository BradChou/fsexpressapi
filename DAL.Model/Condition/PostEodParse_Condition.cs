﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class PostEodParse_Condition
    {
        public long id { get; set; }
        public long PostEodId { get; set; }
        public string PostId { get; set; }
        public string CheckNumber { get; set; }
        public string PostStation { get; set; }
        public DateTime StatusDate { get; set; }
        public string StatusCode { get; set; }
        public string StatusZH { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
