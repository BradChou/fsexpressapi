﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
	public class MeasureScanLog_Condition
    {
		public long id { get; set; }
		public string DataSource { get; set; }
		public string Scancode { get; set; }
		public string Length { get; set; }
		public string Width { get; set; }
		public string Height { get; set; }
		public string Size { get; set; }
		public string Status { get; set; }
		public DateTime? ScanTime { get; set; }
		public string Picture { get; set; }
		public DateTime cdate { get; set; }
	}
}
