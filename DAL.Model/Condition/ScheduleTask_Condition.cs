﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class ScheduleTask_Condition
    {
        public long id { get; set; }
        public int TaskID { get; set; }

        public string TaskName { get; set; }
        public DateTime StartTime { get; set; }

        public long StartId { get; set; }
        public bool Enable { get; set; }
        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }
        
    }
}
