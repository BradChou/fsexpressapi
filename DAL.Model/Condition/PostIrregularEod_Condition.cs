﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class PostIrregularEod_Condition
    {
        public long id { get; set; }
        public string EodName { get; set; }
        public string EodPath { get; set; }
        public string PostId { get; set; }
        public string check_number { get; set; }
        public string post_code { get; set; }
        public string is_collection_money { get; set; }
        public string collection_money { get; set; }
        public string receive_name { get; set; }
        public string receive_tel { get; set; }
        public string zip3 { get; set; }
        public string receive_address { get; set; }
        public string upload_date { get; set; }
        public string bag_no { get; set; }
        public string bag_weight { get; set; }
        public string serial_no { get; set; }
        public string weight { get; set; }
        public string container_no { get; set; }
        public string StatusCode { get; set; }
        public DateTime CreateTime { get; set; }


    }
}
