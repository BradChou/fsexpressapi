﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class UpdateSDMDJobLog_Condition
    {
		public long Id { get; set; }
		public string tcDeliveryRequestsId { get; set; }
		public string CustomerCode { get; set; }
		public string Address { get; set; }
		public string ReceiveCode { get; set; }
		public string ReceiveSD { get; set; }
		public string ReceiveMD { get; set; }
		public string Map8City { get; set; }
		public string Map8Town { get; set; }
		public string Map8Name { get; set; }
		public DateTime CreateDate { get; set; }
	}
}
