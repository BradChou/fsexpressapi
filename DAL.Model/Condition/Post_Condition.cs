﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class Post_Condition
    {
        public long id { get; set; }
        public string PostId { get; set; }
        public string SerialNumber { get; set; }
        public string CommodityCode { get; set; }
        public string DiscernCode { get; set; }
        public string Zip3 { get; set; }
        public string CheckCode { get; set; }
        public long request_id { get; set; }
        public string check_number { get; set; }
        public DateTime CreateTime { get; set; }
        public string Weight { get; set; }
    }
}
