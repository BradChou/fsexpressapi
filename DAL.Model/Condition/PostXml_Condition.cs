﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class PostXml_Condition
    {
        public long id { get; set; }
        public string FTPPath { get; set; }
        
        public string XmlName { get; set; }
        public string XmlPath { get; set; }
        public string XmlContent { get; set; }

        public DateTime? FileCreateTime { get; set; }
        public DateTime CreateTime { get; set; }
        public bool Fail { get; set; }
        public string FailMessage { get; set; }

        public int Number { get; set; }

        public PostXml_Condition()
        {
            this.XmlContent = string.Empty;
            this.Fail = false;
            this.FailMessage = string.Empty;
            this.Number = 1;
        }
    }
}
