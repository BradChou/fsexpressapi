﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class PostStatus_Condition
    {
		public int id { get; set; }
		public string StatusCode { get; set; }
		public string StatusZH { get; set; }
		public string ScanItem { get; set; }
		public string ArriveOption { get; set; }
		public DateTime CreateTime { get; set; }
	}
}
