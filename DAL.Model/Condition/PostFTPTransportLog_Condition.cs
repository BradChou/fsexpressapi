﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class PostFTPTransportLog_Condition
    {
        public long id { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string FTPPath { get; set; }
        public string NASPath { get; set; }
        public DateTime CreateTime { get; set; }
        public string FailMessage { get; set; }

        public PostFTPTransportLog_Condition() 
        {
            FTPPath = string.Empty;
            NASPath = string.Empty;
            FailMessage = string.Empty;
        }
    }

    public enum PostFTPTransportType
    {
        Order = 1,
        Irregular = 2,
        Result = 3,
        Status = 4
    }

}
