﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
   public class DeliveryStatusPublicError_Condition
	{
		public long id { get; set; }
		public long DeliveryStatusPublicId { get; set; }
		public string DataSource { get; set; }
		public string CheckNumber { get; set; }
		public string DeliveryStatus { get; set; }
		public DateTime ScanDate { get; set; }
		public string CompanyName { get; set; }
		public string TaxIDNumber { get; set; }
		public string DeliveryStatusCode { get; set; }
		public string WorkerName { get; set; }
		public string StationCode { get; set; }
		public string StationName { get; set; }
		public DateTime CreateDate { get; set; }
		public int Status { get; set; }
		public string ErrMsg { get; set; }
		public int Number { get; set; }

		public DeliveryStatusPublicError_Condition()
		{
			this.Number = 1;
		}

		public static implicit operator DeliveryStatusPublicError_Condition(DeliveryStatusPublic_Condition data)
		{
			return new DeliveryStatusPublicError_Condition
			{
				DeliveryStatusPublicId = data.id,
				DataSource = data.DataSource,
				CheckNumber = data.CheckNumber,
				DeliveryStatus = data.DeliveryStatus,
				ScanDate = data.ScanDate,
				CompanyName = data.CompanyName,
				TaxIDNumber = data.TaxIDNumber,
				DeliveryStatusCode = data.DeliveryStatusCode,
				WorkerName = data.WorkerName,
				StationCode = data.StationCode,
				StationName = data.StationName,
				CreateDate = data.CreateDate
			};
		}
		public static implicit operator DeliveryStatusPublicError_Condition(DeliveryStatusPublicFilter_Condition data)
		{
			return new DeliveryStatusPublicError_Condition
			{
				DeliveryStatusPublicId = data.id,
				DataSource = data.DataSource,
				CheckNumber = data.CheckNumber,
				DeliveryStatus = data.DeliveryStatus,
				ScanDate = data.ScanDate,
				CompanyName = data.CompanyName,
				TaxIDNumber = data.TaxIDNumber,
				DeliveryStatusCode = data.DeliveryStatusCode,
				WorkerName = data.WorkerName,
				StationCode = data.StationCode,
				StationName = data.StationName,
				CreateDate = data.CreateDate
			};
		}
	}
}
