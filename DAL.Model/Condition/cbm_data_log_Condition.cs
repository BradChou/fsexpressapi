﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class cbm_data_log_Condition
    {
		public int id { get; set; }
		public string file_name { get; set; }
		public int? file_row { get; set; }
		public DateTime? cdate { get; set; }
		public string check_number { get; set; }
		public double? length { get; set; }
		public double? width { get; set; }
		public double? height { get; set; }
		public double? cbm { get; set; }
		public string s3_pic_uri { get; set; }
		public string s3_pic_uri_2 { get; set; }
		public DateTime? scan_time { get; set; }
		public string data_source { get; set; }
		public string scan_result { get; set; }
		public bool? is_log { get; set; }
		public string uuser { get; set; }
		public DateTime? udate { get; set; }
	}
}
