﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class PostEodParseExclude_Condition
    {
        public long id { get; set; }
        public long PostEodParseId { get; set; }
        public string PostId { get; set; }
        public string CheckNumber { get; set; }
        public string PostStation { get; set; }
        public DateTime StatusDate { get; set; }
        public string StatusCode { get; set; }
        public string StatusZH { get; set; }
        public DateTime CreateTime { get; set; }

        public static implicit operator PostEodParseExclude_Condition(PostEodParse_Condition data)
        {
            return new PostEodParseExclude_Condition
            {
                PostEodParseId = data.id,
                PostId = data.PostId,
                CheckNumber = data.CheckNumber,
                PostStation = data.PostStation,
                StatusDate = data.StatusDate,
                StatusCode = data.StatusCode,
                StatusZH = data.StatusZH
            };
        }
        public static implicit operator PostEodParseExclude_Condition(PostEodParseFilter_Condition data)
        {
            return new PostEodParseFilter_Condition
            {
                PostEodParseId = data.PostEodParseId,
                PostId = data.PostId,
                CheckNumber = data.CheckNumber,
                PostStation = data.PostStation,
                StatusDate = data.StatusDate,
                StatusCode = data.StatusCode,
                StatusZH = data.StatusZH
            };
        }
    }
}
