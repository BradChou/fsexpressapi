﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
	public class PostSerial_Condition
    {
		public long id { get; set; }
		public string Serial { get; set; }
		public string Customer { get; set; }
		public string Discern { get; set; }
		public DateTime UpdateTime { get; set; }
	}
}
