﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class tbCustomers_Condition
    {
        public decimal customer_id { get; set; }
        public string supplier_code { get; set; }
        public string supplier_id { get; set; }
        public string master_code { get; set; }
        public string second_id { get; set; }
        public string pricing_code { get; set; }
        public string second_code { get; set; }
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string customer_shortname { get; set; }
        public string customer_type { get; set; }
        public string uniform_numbers { get; set; }
        public string shipments_principal { get; set; }
        public string telephone { get; set; }
        public string fax { get; set; }
        public string shipments_city { get; set; }
        public string shipments_area { get; set; }
        public string shipments_road { get; set; }
        public string shipments_email { get; set; }
        public string stop_shipping_code { get; set; }
        public string stop_shipping_memo { get; set; }
        public DateTime contract_effect_date { get; set; }
        public DateTime contract_expired_date { get; set; }
        public string contract_content { get; set; }
        public DateTime tariffs_effect_date { get; set; }
        public int checkout_date { get; set; }
        public string tariffs_table { get; set; }
        public string business_people { get; set; }
        public string customer_hcode { get; set; }
        public string contact_1 { get; set; }
        public string email_1 { get; set; }
        public string contact_2 { get; set; }
        public string email_2 { get; set; }
        public string billing_special_needs { get; set; }
        public int ticket_period { get; set; }
        public int individual_fee { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public int contract_price { get; set; }
        public bool type { get; set; }
        public int delivery_Type { get; set; }
        public string pick_up_driver { get; set; }
        public string assigned_sd { get; set; }
        public string assigned_md { get; set; }
        public string invoice_city { get; set; }
        public string invoice_area { get; set; }
        public string invoice_road { get; set; }
        public int product_type { get; set; }
        public bool is_weekend_delivered { get; set; }
        public int weekend_delivery_fee { get; set; }
        public string pallet_exclusive_send_station { get; set; }
        public string bank { get; set; }
        public string bank_branch { get; set; }
        public string bank_account { get; set; }
        public bool return_address_flag { get; set; }
        public string sales_id { get; set; }
        public DateTime? sales_id_cdate { get; set; }
        public DateTime? sales_id_udate { get; set; }
        public int? multi_ship { get; set; }
        public string sale_station { get; set; }
        public DateTime? sale_station_cdate { get; set; }
        public DateTime? sale_station_udate { get; set; }
        public bool? is_new_customer { get; set; }

        public string MasterCustomerCode { get; set; }
        public string MasterCustomerName { get; set; }
        public bool? OldCustomerOverCBM { get; set; }


    }
}
