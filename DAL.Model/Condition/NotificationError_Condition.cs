﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class NotificationError_Condition
    {
        public long id { get; set; }
        public long NotificationId { get; set; }
        public int Type { get; set; }
        public string Recipient { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateUser { get; set; }
        public int PendingCount { get; set; }
        public int Status { get; set; }
        public string ErrMsg { get; set; }
        public DateTime UpdateTime { get; set; }

        public NotificationError_Condition()
        {
            this.PendingCount = 1;
        }


        public static implicit operator NotificationError_Condition(Notification_Condition data)
        {
            return new NotificationError_Condition
            {
                NotificationId = data.id,
                Type = data.Type,
                Recipient = data.Recipient,
                Subject = data.Subject,
                Body = data.Body,
                CreateUser = data.CreateUser
            };
        }
    }
}
