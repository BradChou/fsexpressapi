﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class MeasureImageTransferLog_Condition
    {
        public long id { get; set; }
        public string DataSource { get; set; }
        public string SourcePath { get; set; }
        public string DestPath { get; set; }
        public string Status { get; set; }
        public DateTime cdate { get; set; }
        public string Message { get; set; }

        public MeasureImageTransferLog_Condition()
        {
            cdate = DateTime.Now;
        }
    }
}
