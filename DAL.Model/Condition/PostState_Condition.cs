﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
  public   class PostState_Condition
    {
		public int id { get; set; }
		public string PostStateNo { get; set; }
		public string StateName { get; set; }
		public string StateDetail { get; set; }
		public string ScanItem { get; set; }
		public string ArriveOption { get; set; }
		public DateTime CreateTime { get; set; }
	}
}
