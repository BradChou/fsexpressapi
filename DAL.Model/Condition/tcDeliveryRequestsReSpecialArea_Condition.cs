﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
	public   class tcDeliveryRequestsReSpecialArea_Condition
    {
		public decimal request_id { get; set; }
		public string check_number { get; set; }
		public string customer_code { get; set; }
		public string receive_city { get; set; }
		public string receive_area { get; set; }
		public string receive_address { get; set; }
		public string send_city { get; set; }
		public string send_area { get; set; }
		public string send_address { get; set; }
		public string DeliveryType { get; set; }
		public DateTime cdate { get; set; }
		public int SpecialAreaId { get; set; }
		public int SpecialAreaFee { get; set; }
		public string SendCode { get; set; }
		public string SendSD { get; set; }
		public string SendMD { get; set; }
		public string ReceiveCode { get; set; }
		public string ReceiveSD { get; set; }
		public string ReceiveMD { get; set; }
		public int SpecialAreaIdRe { get; set; }
		public int SpecialAreaFeeRe { get; set; }
		public string CodeRe { get; set; }
		public string SDRe { get; set; }
		public string MDRe { get; set; }
	}
}
