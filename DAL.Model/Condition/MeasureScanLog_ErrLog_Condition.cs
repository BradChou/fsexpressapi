﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
	public class MeasureScanLog_ErrLog_Condition
    {
		public long id { get; set; }
		public long MeasureScanLog_FilteredId { get; set; }
		public string DataSource { get; set; }
		public string Scancode { get; set; }
		public string Length { get; set; }
		public string Width { get; set; }
		public string Height { get; set; }
		public string Size { get; set; }
		public string Status { get; set; }
		public DateTime? ScanTime { get; set; }
		public string Picture { get; set; }
		public DateTime cdate { get; set; }
		public string ErrMsg { get; set; }
		public int Number { get; set; }

		public MeasureScanLog_ErrLog_Condition()
        {
			this.Number = 1;
        }

		public static implicit operator MeasureScanLog_ErrLog_Condition(MeasureScanLog_Filtered_Condition data)
        {
			return new MeasureScanLog_ErrLog_Condition
			{
				MeasureScanLog_FilteredId = data.id,
				DataSource = data.DataSource,
				Scancode = data.Scancode,
				Length = data.Length,
				Width = data.Width,
				Height = data.Height,
				Status = data.Status,
				Size = data.Size,
				ScanTime = data.ScanTime,
				Picture = data.Picture,
				cdate = DateTime.Now
			};
        }
	}
}
