﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class PostResultEod_Condition
    {
		public long id { get; set; }
		public string EodName { get; set; }
		public string EodPath { get; set; }
		public string PostId { get; set; }
		public string CheckNumber { get; set; }
		public string OrderFileDate { get; set; }
		public string StatusCode { get; set; }
		public string StatusZH { get; set; }
		public DateTime CreateTime { get; set; }

		public PostResultEod_Condition() 
		{
			PostId = string.Empty;
			CheckNumber = string.Empty;
			OrderFileDate = string.Empty;
			StatusCode = string.Empty;
			StatusZH = string.Empty;

		}
	}
}
