﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class PostCollectOnDelivery_Condition
    {
        public long id { get; set; }
        public string PostId { get; set; }
        public long request_id { get; set; }
        public string check_number { get; set; }
        public string Weight { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
    }
}
