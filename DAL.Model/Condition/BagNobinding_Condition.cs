﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class BagNobinding_Condition
    {
		public long id { get; set; }
		public long RequestId { get; set; }
		public string CheckNumber { get; set; }
		public string BagNo { get; set; }
		public string DriverCode { get; set; }
		public bool IsBind { get; set; }
		public DateTime cdate { get; set; }
		public DateTime udate { get; set; }


	}
}
