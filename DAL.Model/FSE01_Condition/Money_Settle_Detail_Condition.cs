﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class Money_Settle_Detail_Condition
	{
		public long id { get; set; }
		public int RequestId { get; set; }
		public string DeliveryType { get; set; }
		public string CheckNumber { get; set; }
		public DateTime? CreateDate { get; set; }
		public string CustomerCode { get; set; }
		public string SendStation { get; set; }
		public string ArriveStation { get; set; }
		public string City { get; set; }
		public string Area { get; set; }
		public string Address { get; set; }
		public string ProductId { get; set; }
		public int Pieces { get; set; }
		public bool ReceiptFlag { get; set; }
		public int CollectionMoney { get; set; }
		public int? Valued { get; set; }
		public int OverCBM { get; set; }
		public bool RoundTrip { get; set; }
		public string EggArea { get; set; }
		public DateTime? PrintDate { get; set; }
		public DateTime? ShipDate { get; set; }
		public string OrderNumber { get; set; }
		public string CustomerName { get; set; }
		public string ReceiveContact { get; set; }
		public string InvoiceDesc { get; set; }
		public DateTime? ScanDate { get; set; }
		public string  CreateUser { get; set; }
		public DateTime? Request_CreateDate { get; set; }
		public int SpecialAreaFee { get; set; }
		public int SpecialAreaId { get; set; }
		public string DriverCode { get; set; }
		public string ArriveDriverStation { get; set; }
		public string CustomerStation { get; set; }
		public string ArriveOption { get; set; }
        public DateTime? ScanLogCdate { get; set; }
		public string ReceiveDriverStation { get; set; }
		public string ReceiveDriverCode { get; set; }
		public bool isEast { get; set; }
	}
}
