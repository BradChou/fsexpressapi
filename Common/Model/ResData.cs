﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Common.Setting.EnumSetting;

namespace Common
{
    //public class ResData
    //{
    //    public string Status { get; set; }
    //    public string Message { get; set; }
    //    public object Data { get; set; }
    //}
    public class ResData<TData> : ResData
    {

        public ResData()
            : base()
        {
        }
        public ResData(ResponseCodeEnum status)
            : base(status)
        {
        }
        public ResData(ResponseCodeEnum status, string msg)
            : base(status, msg)
        {
        }
        /// <summary>
        /// 結果
        /// </summary>
        public TData Data { get; set; }
    }
    public class ResData
    {
        private ResponseCodeEnum _status;
        private string _message;

        public ResData()
        {
            _status = ResponseCodeEnum.Success;
           
        }

        public ResData(ResponseCodeEnum status)
        {
            _status = status;
            _message = status.GetString();
   
        }

        public ResData(ResponseCodeEnum status, string msg)
        {
            _status = status;
            _message = msg;

        }
        /// <summary>
        /// 結果狀態 0成功
        /// </summary>
        public ResponseCodeEnum Status
        {
            get { return _status; }
            set { _status = value; }
        }


        /// <summary>
        /// 消息 用於調試
        /// </summary>
        public string Message
        {
            get
            {
                if (string.IsNullOrEmpty(_message))
                    return _status.GetString();
                else
                    return _message;
            }
            set { _message = value; }
        }

    }
}
