﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model
{
    public class ExcelParmModel<T> where T : class
    {
        /// <summary>
        /// 資料
        /// </summary>
        public List<T> Data { get; set; }
        /// <summary>
        /// 模板路徑
        /// </summary>
        public string TemplatePath { get; set; }
        /// <summary>
        /// 儲存路徑
        /// </summary>
        public string SavePath { get; set; }
        /// <summary>
        /// 開始寫入列
        /// </summary>
        public int Offset { get; set; }
        /// <summary>
        /// 跳過資料欄位
        /// </summary>
        public List<string> PassCell { get; set; }
        /// <summary>
        /// 身分，寫網路硬碟用
        /// </summary>
        public IdentityModel Identity { get; set; }

        public ExcelParmModel() { this.Identity = new IdentityModel(); }

    }
}
