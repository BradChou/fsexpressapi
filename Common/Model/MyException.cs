﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using static Common.Setting.EnumSetting;

namespace Common
{
   public class MyException : Exception, ISerializable
    {
        public ResponseCodeEnum ResponseCode { get; set; }

        public MyException(ResponseCodeEnum code)
            : this(code, code.GetString())
        {

        }


        public MyException(ResponseCodeEnum responseCode, string message)
            : this(responseCode, message, null)
        {
        }

        public MyException(ResponseCodeEnum responseCode, string message, Exception innerException)
            : base(message, innerException)
        {
            ResponseCode = responseCode;
        }
    }
}
