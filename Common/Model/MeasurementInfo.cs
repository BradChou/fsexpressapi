﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model
{
    public class MeasurementInfo
    {
        /// <summary>
        /// 材積(S060/S090/S110/S120/S150/B001/X001/N001)
        /// </summary>
        public string MeasurementCode { get; set; }
        /// <summary>
        /// 長(mm)
        /// </summary>
        public int? Length { get; set; }
        /// <summary>
        /// 寬(mm)
        /// </summary>
        public int? Width { get; set; }
        /// <summary>
        /// 高(mm)
        /// </summary>
      	public int? Height { get; set; }
        /// <summary>
        /// 重量(g)
        /// </summary>
        public int? Weight { get; set; }
    }
}
