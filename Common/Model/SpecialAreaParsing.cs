﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model
{
    public class SpecialAreaParsing
    {

        /// <summary>
        /// id
        /// </summary>
        public long id { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 鄉鎮區
        /// </summary>
        public string Town { get; set; }
        /// <summary>
        /// 路段
        /// </summary>
        public string Road { get; set; }
        /// <summary>
        /// 站所代碼
        /// </summary>
        public string StationCode { get; set; }

        /// <summary>
        /// 配送貨車司機
        /// </summary>
        public string SalesDriverCode { get; set; }
        /// <summary>
        /// 配送機車司機
        /// </summary>
        public string MotorcycleDriverCode { get; set; }
        /// <summary>
        ///  堆疊區
        /// </summary>
        public string StackCode { get; set; }

        /// <summary>
        ///  接駁區代碼
        /// </summary>
        public string ShuttleStationCode { get; set; }
        /// <summary>
        ///  價格
        /// </summary>
        public int Fee { get; set; }
        /// <summary>
        ///  時效
        /// </summary>
        public string TimeLimit { get; set; }
    }
}
