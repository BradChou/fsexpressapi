﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Common.SSRS
{
    public class SsrsHelper : IReportHelper
    {
        /// <summary>
        /// ReportRootUrl
        /// </summary>
        private string RootUrl { get; set; }

        /// <summary>
        /// FolderName
        /// </summary>
        private string FolderName { get; set; }

        private string LoginAccount { get; set; }

        private string Password { get; set; }

        private string ReportDomain { get; set; }

        private readonly IConfiguration _config;
     

        public SsrsHelper(IConfiguration configuration)
        {
            this._config = configuration;
            this.RootUrl = this._config["SSRSReport:Url"];
            this.ReportDomain = this._config["SSRSReport:Domain"];
            this.FolderName = this._config["SSRSReport:Folder"];
            this.LoginAccount =  this._config["SSRSReport:Account"];
            this.Password =  this._config["SSRSReport:Password"];
        }

        public Stream GetReport<TReportRequest>(TReportRequest request, string reportName)
        {
            var url = this.GetReportUrl(request, reportName);
            return this.Post(url);
        }

        private string GetReportUrl<TReportRequest>(TReportRequest reportRequest, string reportName)
        {
            var preUrl = string.Format("{0}?/{1}/{2}", this.RootUrl, this.FolderName, reportName);

            var queryString = this.GetQueryString(reportRequest);

            return string.Format("{0}&{1}&rs:Format=PDF", preUrl, queryString);
            //return "http://10.50.26.34/reportServer?/NineYi.OmniERP.PrintingService/Report_711Print&order_form_id=154479&batch_no=0&pick_no=0&database_name=omnimyecerp&rs:Format=PDF";
        }

        private string GetNomalReportUrl(NameValueCollection reportParams, string reportName, string exportType = "PDF", string imageType = "PNG")
        {
            var queryStringList = new List<string>();

            foreach (string key in reportParams.Keys)
            {
                var value = reportParams[key];

                if (value != null)
                {
                    queryStringList.Add(string.Format("{0}={1}", key, value));
                }
            }

            string queryString = string.Join("&", queryStringList);

            var preUrl = string.Format("{0}?/{1}/{2}", this.RootUrl, this.FolderName, reportName);

            string returnUrl = string.Format("{0}&{1}&rs:Format={2}", preUrl, queryString, exportType);

            if (exportType.ToUpper() == "IMAGE")
            {
                returnUrl = string.Format("{0}&rc:OutputFormat={1}", returnUrl, imageType);
            }

            return returnUrl;
        }

        private string GetSimpleReportUrl(string reportName, string exportType = "PDF", string imageType = "PNG")
        {
            var preUrl = string.Format("{0}?/{1}/{2}", this.RootUrl, this.FolderName, reportName);

            string returnUrl = string.Format("{0}&rs:Format={1}", preUrl, exportType);

            if (exportType.ToUpper() == "IMAGE")
            {
                returnUrl = string.Format("{0}&rc:OutputFormat={1}&rc:DpiX=300&rc:DpiY=300", returnUrl, imageType);
            }

            return returnUrl;
        }

        private string GetNomalReportUrl(Dictionary<string, string[]> reportParams, string reportName, string exportType = "PDF", string imageType = "PNG")
        {
            var queryStringList = new List<string>();

            foreach (string key in reportParams.Keys)
            {
                var values = reportParams[key];

                if (values != null)
                {
                    if (values.Length == 0)
                    {
                        queryStringList.Add(string.Format("{0}=", key));
                    }

                    foreach (string value in values)
                    {
                        queryStringList.Add(string.Format("{0}={1}", key, value));
                    }
                }
            }

            string queryString = string.Join("&", queryStringList);

            var preUrl = string.Format("{0}?/{1}/{2}", this.RootUrl, this.FolderName, reportName);

            string returnUrl = string.Format("{0}&{1}&rs:Format={2}", preUrl, queryString, exportType);

            if (exportType.ToUpper() == "IMAGE")
            {
                returnUrl = string.Format("{0}&rc:OutputFormat={1}", returnUrl, imageType);
            }

            return returnUrl;
        }

        private string GetQueryString<TReportRequest>(TReportRequest reportRequest)
        {
            var properties = reportRequest.GetType().GetProperties();
            var queryStringList = new List<string>();

            foreach (var property in properties)
            {
                var value = reportRequest.GetType().GetProperty(property.Name).GetValue(reportRequest);

                if (value != null)
                {
                    queryStringList.Add(string.Format("{0}={1}", property.Name, value));
                }
            }

            return string.Join("&", queryStringList);
        }

        /// <summary>
        /// post資料部分
        /// </summary>
        /// <param name="url"></param>
        /// <param name="queryString"></param>
        /// <returns></returns>
        private Stream PostInner(string url, string queryString)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; zh-TW; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1";
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.Credentials = new NetworkCredential(this.LoginAccount, this.Password);
            req.KeepAlive = true;
            req.Accept = "*/*";


            //取得postData
            byte[] byteData = Encoding.UTF8.GetBytes(queryString);
            req.ContentLength = byteData.Length;
            Stream requestStream = req.GetRequestStream();
            requestStream.Write(byteData, 0, byteData.Length);
            requestStream.Close();

            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            Stream dataStream = resp.GetResponseStream();

            return dataStream;
        }

        private Stream Post(string url, NameValueCollection reportParams)
        {
            var queryStringList = new List<string>();

            foreach (string key in reportParams.Keys)
            {
                var value = reportParams[key];

                if (value != null)
                {
                    queryStringList.Add(string.Format("{0}={1}", key, value));
                }
            }

            string queryString = string.Join("&", queryStringList);

            return PostInner(url, queryString);
        }

        private Stream Post(string url, Dictionary<string, string[]> reportParams)
        {
            var queryStringList = new List<string>();

            foreach (string key in reportParams.Keys)
            {
                var values = reportParams[key];

                if (values != null)
                {
                    if (values.Length == 0)
                    {
                        queryStringList.Add(string.Format("{0}=", key));
                    }

                    foreach (string value in values)
                    {
                        queryStringList.Add(string.Format("{0}={1}", key, value));
                    }
                }
            }

            string queryString = string.Join("&", queryStringList);

            return PostInner(url, queryString);
        }

        /// <summary>
        /// 執行 Api
        /// </summary>
        /// <param name="url">url</param>
        /// <returns>Content</returns>
        private Stream Post(string url)
        {
            WebRequest requset = WebRequest.Create(url);
            requset.Method = "POST";
            requset.Credentials = new NetworkCredential(this.LoginAccount, this.Password);
            requset.GetRequestStream();

            WebResponse response = requset.GetResponse();
            Stream dataStream = response.GetResponseStream();

            return dataStream;
        }

        /// <summary>
        /// 執行取得報表
        /// </summary>
        /// <param name="reportParams"></param>
        /// <param name="reportName"></param>
        /// <returns></returns>
        public Stream GetNomalReport(NameValueCollection reportParams, string reportName, string exportType)
        {
            var url = this.GetSimpleReportUrl(reportName, exportType);
            return this.Post(url, reportParams);
        }

        public Stream GetNomalReport(Dictionary<string, string[]> reportParams, string reportName, string exportType)
        {
            var url = this.GetSimpleReportUrl(reportName, exportType);
            return this.Post(url, reportParams);

        }

        public Stream GetNomalReport(NameValueCollection reportParams, string reportName, string exportType, string imageType)
        {
            var url = this.GetSimpleReportUrl(reportName, exportType, imageType);
            return this.Post(url, reportParams);
        }

        public Stream GetNomalReport(Dictionary<string, string[]> reportParams, string reportName, string exportType, string imageType)
        {
            var url = this.GetSimpleReportUrl(reportName, exportType, imageType);
            return this.Post(url, reportParams);
        }
    }
}
