﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.SSRS
{
    public interface IReportHelper
    {
        Stream GetReport<TReportRequest>(TReportRequest request, string reportName);

        Stream GetNomalReport(NameValueCollection reportParams, string reportName, string exportType);

        Stream GetNomalReport(Dictionary<string, string[]> reportParams, string reportName, string exportType);

        Stream GetNomalReport(NameValueCollection reportParams, string reportName, string exportType, string imageType);

        Stream GetNomalReport(Dictionary<string, string[]> reportParams, string reportName, string exportType, string imageType);
    }
}
