﻿using Common.Model;
using DAL.Model.JunFuAddress_Condition;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Tool
    {

        /// <summary>
        /// 地圖解析(API)
        /// </summary>
        /// <param name="Address"></param>
        /// <param name="CustomerCode"></param>
        /// <param name="ComeFrom"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public static async Task<AddressParsingInfo> GetAddressParsing(string Address, string CustomerCode, AddressParsingComeFromEnum ComeFrom, AddressParsingTypeEnum Type = AddressParsingTypeEnum.Lv1)
        {
            AddressParsingInfo Info = new AddressParsingInfo();
            var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

            //打API
            var client = new RestClient(AddressParsingURL);
            client.Timeout = 2000; // 2000 milliseconds == 2 seconds
            var request = new RestRequest(Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new { Address = StrTrimer(Address), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body

            try
            {
                var response = await client.PostAsync<ResData<AddressParsingInfo>>(request);
                return response.Data;
            }
            catch (Exception e)
            {
                return null;
            }


        }

        /// <summary>
        /// 特服地圖解析(API)
        /// </summary>
        /// <param name="Address"></param>
        /// <param name="CustomerCode"></param>
        /// <param name="ComeFrom"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public static async Task<SpecialAreaParsing> GetSpecialAreaParsing(string Address, string CustomerCode, AddressParsingComeFromEnum ComeFrom, AddressParsingTypeEnum Type = AddressParsingTypeEnum.Lv1)
        {
            SpecialAreaParsing Info = new SpecialAreaParsing();
            var AddressParsingURL = "http://map2.fs-express.com.tw/Address/GetSpecialAreaDetail";

            //打API
            var client = new RestClient(AddressParsingURL);
            client.Timeout = 2000; // 2000 milliseconds == 2 seconds
            var request = new RestRequest(Method.POST);

            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new { Address = StrTrimer(Address), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body

            try
            {
                var response = await client.PostAsync<ResData<SpecialAreaParsing>>(request);
                return response.Data;
            }
            catch (Exception e)
            {
                return null;
            }


        }
        /// <summary>
        ///  轉半形的函數(SBC case)
        /// </summary>
        /// <param name="input">輸入</param>
        /// <returns></returns>
        public static string ToDBC(string input)
        {
            char[] c = input.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }
                if (c[i] > 65280 && c[i] < 65375)
                    c[i] = (char)(c[i] - 65248);
            }
            return new string(c);
        }
        /// <summary>
        /// 去除空白換行符號
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string StrTrimer(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            if (str.Length == 0)
            {
                return string.Empty;
            }

            return str.Replace("\r\n", "").Replace("\n", "").Replace("\t", "").Replace("\r", "").Replace(" ", "").Replace("　", "").Trim();
        }

        public static bool IsZeroOrNothing(string str)
        {
            var result = false;
            if (StrTrimer(str) == string.Empty || StrTrimer(str) == "0")
            {
                result = true;
            }
            return result;
        }

        public static string ReplaceZipCode(string Address)
        {
            char[] replacecharArray = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

            return Address.TrimStart(replacecharArray);
        }
    }
}
