﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Common.Attribute
{
    public class TransactionalAttribute : System.Attribute, IFilterFactory
    {
        //make sure filter marked as not reusable
        public bool IsReusable => false;

        public IFilterMetadata CreateInstance(IServiceProvider serviceProvider)
        {
            return new TransactionalFilter();
        }

        private class TransactionalFilter : IActionFilter
        {
            private TransactionScope _transactionScope;

            public void OnActionExecuting(ActionExecutingContext context)
            {
               // _transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
                _transactionScope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(300), TransactionScopeAsyncFlowOption.Enabled);
            }

            public void OnActionExecuted(ActionExecutedContext context)
            {
                //if no exception were thrown
                if (context.Exception == null)
                    _transactionScope.Complete();
                _transactionScope.Dispose();
            }
        }
    }
}
