﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Common
{
    public class DBconn
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionStringJunFuReal;
        private readonly string _connectionStringJunFuTrans;
        private readonly string _connectionStringJunFuAddress;

        private readonly string _connectionStringFSE01;

        private readonly string _connectionStringContract;
        private readonly string _connectionStringLog;

        public DBconn(IConfiguration configuration)
        {
            SqlMapper.AddTypeMap(typeof(DateTime), System.Data.DbType.DateTime2);
            _configuration = configuration;
            _connectionStringJunFuReal = _configuration.GetConnectionString("JunFuRealConnection");
            _connectionStringJunFuTrans = _configuration.GetConnectionString("JunFuTransConnection");
            _connectionStringJunFuAddress = _configuration.GetConnectionString("JunFuAddressConnection");

            _connectionStringFSE01 = _configuration.GetConnectionString("FSE01Connection");

            _connectionStringContract = _configuration.GetConnectionString("ContractConnection");

            _connectionStringLog = _configuration.GetConnectionString("connstr_log");


        }
        public IDbConnection CreateJunFuRealConnection() => new SqlConnection(_connectionStringJunFuReal);
        public IDbConnection CreateJunFuTransConnection() => new SqlConnection(_connectionStringJunFuTrans);
        public IDbConnection CreateJunFuAddressConnection() => new SqlConnection(_connectionStringJunFuAddress);

        public IDbConnection CreateFSE01Connection() => new SqlConnection(_connectionStringFSE01);

        public IDbConnection CreateContractConnection() => new SqlConnection(_connectionStringContract);

        public IDbConnection CreateLogConnection() => new SqlConnection(_connectionStringLog);


    }
}
