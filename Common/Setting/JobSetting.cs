﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Setting
{
    public class JobSetting
    {
        public enum TaskCodeEnum
        {
            UpdateDeliveryStatusPublicToDeliveryScanLogJob = 1,
            MesureScanLogFilterJob = 2,
            UpdateMesureScanLogToMainTable = 3,
            MeasureErrDataReUpdateCycleJob = 4,
            SignImageAppUpload = 5,
            UploadDaYaunCbmImageJob = 6,
            UploadTaiChungCbmImageJob = 7,
            GetPostFTPXMLFileJob = 8,
            ParsingPostXmlJob = 9,
            FilterPostStateJob = 10,
            UpdatePostStateToDeliveryScanLogJob = 11,
            FilterDeliveryStatusPublicJob = 12,
            GetErrorPostFTPXMLFileJob = 13,
            SendJob = 14,
            DownloadPostStatusFTPToNASJob = 15,
            GetPostStatusNASToDBJob = 16,
            ParsingPostStatusJob = 17,
            FilterPostStatusJob = 18,
            InsertPostStatusJob = 19,
            UploadPostOrderJob = 20,
            MesureScanLogAbnormalJob = 21,
            SettleCaculateJob = 22,
            CBMDetailFilterJob = 23,


            DeliveryRequestsUpdateSDMDJob = 24,

            DailySettleTodoListJob = 25,

            CreateXlsxPostQ100Report = 26
        }
    }
}
