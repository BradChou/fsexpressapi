﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Setting
{
    public static class EnumSetting
    {
        public enum ResponseCodeEnum
        {
            #region 系統相關
            Success = 200,//成功
            Error = 500,//失敗
            #endregion

            #region API相關

            ParameterError = 10001,
            AuthorizationError = 10002,

            Parameter_ReceiveContact_Error=10003,
            Parameter_ReceiveTel1_Error = 10004,
            Parameter_ReceiveAddress_Error = 10005,
            Parameter_PaymentMethod_Error = 10006,
            Parameter_MeasurementInfos_Error = 10007,
            Parameter_DeliveryMethod_Error = 10008,
            Parameter_ReceiptFlag_Error = 10009,
            Parameter_Arriveassigndate_Error = 10010,
            Parameter_PiecesMeasurementInfos=10011,

            // Err = 10001,
            #endregion
            #region 託運單相關
            CheckNumberIsNULL = 20001,
            CheckNumberNotFoundInCustomerCode = 20002,
            HavingDeliveryItem = 20003,
            CheckNumberIsCanceled = 20004,

            #endregion
            #region 排程相關
            ScheduleTaskNotFound = 30001,
            MeasureIDNotFound = 30005,
            MeasureFolderNotFound = 30006,
            #endregion

            #region Auth相關
            AuthorizationUserError = 40001,
            WrongAccountOrPassword = 40002,

            #endregion

            #region POST相關
            PostError = 50001,
            PostNotFound = 50002,
            PostStateRepeat = 50003,
            PostStateNotFound = 50004,

            PostXmlParsingError = 50005,
            #endregion

            #region 公版貨態相關
            DeliveryStatusPublicError = 60001,
            DeliveryStatusPublicStateRepeat = 60002,
            DeliveryStatusPublicNotFound = 60003,
            DeliveryStatusPublicNotFoundCheckNumber = 60004,
            DeliveryStatusPublicHaveP33AO3 = 60005,

            #endregion
            #region LineNotify
            LineNotifyError = 70001,

            #endregion
            #region Notification
            NotificationError = 80001,

            #endregion
            #region 託運單相關
            DeliveryError = 90001,
            DeliverySendAddressError = 90002,
            DeliveryReceiveAddressError = 90003,
            DeliveryCollectionMoneyOver = 90004,
            DeliveryCollectionPaymentMethodError = 90005,
            DeliveryIsReturned = 90006,
            DeliveryReceiveTooFar = 90007,

            #endregion
            #region 報表相關
            ReportError = 100001,


            #endregion
            #region 客戶相關
            CustomerError = 110001,
            CustomerNotFound = 110002,

            #endregion


        }

        #region 方法
        public static string GetString(this ResponseCodeEnum res)
        {
            try
            {
                var type = res.GetType();
                string s = EnumStr.ResponseCodeEnumStr[res];
                return s;
            }
            catch (Exception e)
            {
                return "系統異常";
            }
        }

        #endregion
        class EnumStr
        {
            public static Dictionary<ResponseCodeEnum, string> ResponseCodeEnumStr = new Dictionary<ResponseCodeEnum, string>
            {
            {ResponseCodeEnum.Success,"成功"},
            {ResponseCodeEnum.Error,"失敗"},
            {ResponseCodeEnum.ParameterError,"參數異常"},
            {ResponseCodeEnum.CheckNumberIsNULL,"找不到託運單號"},
            {ResponseCodeEnum.ScheduleTaskNotFound,"找不到排程工作"},
            {ResponseCodeEnum.AuthorizationError,"Authorization異常"},
            {ResponseCodeEnum.AuthorizationUserError,"非合法使用者"},
            {ResponseCodeEnum.PostError,"郵局異常"},
            {ResponseCodeEnum.PostNotFound,"無郵局單號"},
            {ResponseCodeEnum.PostStateRepeat,"重複貨態"},
            {ResponseCodeEnum.PostStateNotFound,"找不到對應的貨態"},
            {ResponseCodeEnum.DeliveryStatusPublicError,"貨態異常"},
            {ResponseCodeEnum.DeliveryStatusPublicStateRepeat,"貨態重複" },
            {ResponseCodeEnum.DeliveryStatusPublicNotFound,"找不到對應貨態"},
            {ResponseCodeEnum.DeliveryStatusPublicNotFoundCheckNumber,"找不到貨號"},
            {ResponseCodeEnum.LineNotifyError,"Line推播異常"},
            {ResponseCodeEnum.NotificationError,"發送訊息異常"},
            {ResponseCodeEnum.CheckNumberNotFoundInCustomerCode,"貨號不屬於此客代"},
            {ResponseCodeEnum.HavingDeliveryItem,"此貨號已經有運送紀錄"},
            {ResponseCodeEnum.WrongAccountOrPassword,"帳密錯誤"},
            {ResponseCodeEnum.DeliveryStatusPublicHaveP33AO3,"已正常配交"},
            {ResponseCodeEnum.DeliveryError,"託運單異常"},
            {ResponseCodeEnum.DeliverySendAddressError,"託運單寄件人地址無法匹配"},
            {ResponseCodeEnum.DeliveryReceiveAddressError,"託運單收件人地址無法匹配"},
            {ResponseCodeEnum.DeliveryCollectionMoneyOver,"託運單超出代收款"},
            {ResponseCodeEnum.DeliveryCollectionPaymentMethodError,"無此貨款方式"},
            {ResponseCodeEnum.CheckNumberIsCanceled,"此貨號已經被取消"},
            {ResponseCodeEnum.ReportError,"報表異常"},
            {ResponseCodeEnum.CustomerError,"客戶異常"},
            {ResponseCodeEnum.CustomerNotFound,"找不到此客戶"},
            {ResponseCodeEnum.DeliveryIsReturned,"此單為逆物流"},
            {ResponseCodeEnum.DeliveryReceiveTooFar,"寄件地址不在服務範圍內！"},


             {ResponseCodeEnum.Parameter_ReceiveContact_Error,"ReceiveContact-參數異常"},
             {ResponseCodeEnum.Parameter_ReceiveTel1_Error,"ReceiveTel1-參數異常"},
             {ResponseCodeEnum.Parameter_ReceiveAddress_Error,"ReceiveAddress-參數異常"},
             {ResponseCodeEnum.Parameter_PaymentMethod_Error,"PaymentMethod-參數異常"},
             {ResponseCodeEnum.Parameter_MeasurementInfos_Error,"MeasurementInfos-參數異常"},
             {ResponseCodeEnum.Parameter_DeliveryMethod_Error,"DeliveryMethod-參數異常"},
             {ResponseCodeEnum.Parameter_ReceiptFlag_Error,"ReceiptFlag-參數異常"},
             {ResponseCodeEnum.Parameter_Arriveassigndate_Error,"Arriveassigndate-參數異常"},
             {ResponseCodeEnum.Parameter_PiecesMeasurementInfos,"Pieces與MeasurementInfos數量不一致-參數異常"}

            };
        }
    }

}
