﻿using Common.Model;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Common.Excel
{
    public class ExcelFile
    {

        public static void SaveFile<T>(List<T> Tdata, string path, int startX = 0, int startY = 0) where T : class
        {
            //IWorkbook Excel_WB = new XSSFWorkbook(new FileStream(path, FileMode.Open));

            XSSFWorkbook workbook = new XSSFWorkbook(path);
            XSSFSheet sheet = (XSSFSheet)workbook.GetSheetAt(0);


            var model = (T)typeof(T).GetConstructor(new System.Type[] { }).Invoke(new object[] { });//反射得到泛型类的实体
            PropertyInfo[] pro = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public);
            Type type = model.GetType();

    
            for (int i = startX; i < Tdata.Count(); i++)
            {
                var rows = sheet.CreateRow(i);
                int c = startY;
                foreach (PropertyInfo propertyInfo in pro)
                {
                    var aaa = propertyInfo.GetValue(Tdata[i], null).ToString();

                    rows.CreateCell(c).SetCellValue(aaa);

                //     sheet.GetRow(startX).GetCell(startY).SetCellValue(aaa);

                    c++;
                }

                c = startY;
            }

            using (FileStream sw = File.Create(path.Replace(".xlsx", "") + "15615616" + ".xlsx"))
            {
                workbook.Write(sw);
                // Excel_WB.Write(sw);
            }

        }

        /// <summary>
        /// 依模板產生Xlsx
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parm"></param>
        public static void Generate<T>(ExcelParmModel<T> parm) where T : class
        {
            using (FileStream fileStream = new FileStream(parm.TemplatePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                IWorkbook workbook = WorkbookFactory.Create(fileStream);
                ISheet sheet = workbook.GetSheetAt(0);
                List<ICell> TemplateCells = new List<ICell>();
                for (int i = 0; i < sheet.GetRow(parm.Offset).Cells.Count; i++)
                {
                    TemplateCells.Add(sheet.GetRow(parm.Offset).GetCell(i));
                }

                PropertyInfo[] properties = typeof(T).GetProperties().Where(x => !parm.PassCell.Any(y => y.Contains(x.Name))).ToArray<PropertyInfo>();
                foreach (var entity in parm.Data)
                {
                    sheet.CreateRow(parm.Offset);
                    int CellInRow = 0;
                    foreach (var property in properties)
                    {
                        ICell cell = sheet.GetRow(parm.Offset).CreateCell(CellInRow);
                        cell.CellStyle = TemplateCells[CellInRow].CellStyle;
                        cell.SetCellType(TemplateCells[CellInRow].CellType);
                        if (TemplateCells[CellInRow].CellType.Equals(CellType.Numeric))
                        {
                            cell.SetCellValue(Convert.ToDouble(property.GetValue(entity, null)));
                        }
                        else
                        {
                            cell.SetCellValue(Convert.ToString(property.GetValue(entity, null)));
                        }
                        CellInRow++;
                    }
                    parm.Offset++;
                }

                //是否模擬身分寫入
                if (parm.Identity.UserName != null) {

                    using (Impersonator impersonator = new Impersonator())
                    {
                        impersonator.Login(parm.Identity.UserName, parm.Identity.Domain, parm.Identity.Password);
                        System.Security.Principal.WindowsIdentity.RunImpersonated(impersonator.Identity.AccessToken, () =>
                        {
                            //做切換後身分的事情
                            using (FileStream fileOut = new FileStream(parm.SavePath, FileMode.Create))
                            {
                                workbook.Write(fileOut);
                            }
                        });
                    }
                    return;
                }

                using (FileStream fileOut = new FileStream(parm.SavePath, FileMode.Create))
                {
                    workbook.Write(fileOut);
                }
            }
        }

        public static List<T> ToObject<T>(string path, int startX = 0, int startY = 0,int Sheet=0) where T : class
        {
            var result = new List<T>();
            XSSFWorkbook workbook = new XSSFWorkbook(path);
            XSSFSheet sheet = (XSSFSheet)workbook.GetSheetAt(Sheet);

            for (int i = startX; i <= sheet.LastRowNum; i++)
            {

                XSSFRow row = (XSSFRow)sheet.GetRow(i);

                var model = (T)typeof(T).GetConstructor(new System.Type[] { }).Invoke(new object[] { });//反射得到泛型类的实体
                PropertyInfo[] pro = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public);
                Type type = model.GetType();

                int c = startY;
                foreach (PropertyInfo propertyInfo in pro)
                {
                    var data = row.GetCell(c) == null ? string.Empty : row.GetCell(c).ToString();

                    if (Convert.IsDBNull(data))
                    {
                        continue;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(data)))
                    {
                        var propertytype = propertyInfo.PropertyType;
                        if (propertytype == typeof(System.Nullable<DateTime>) || propertytype == typeof(DateTime))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToDateTime(data), null);
                        }
                        else if (propertytype == typeof(System.Nullable<decimal>) || propertytype == typeof(decimal))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToDecimal(data), null);
                        }
                        else if (propertytype == typeof(System.Nullable<int>) || propertytype == typeof(int))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToInt32(data), null);
                        }
                        else if (propertytype == typeof(System.Nullable<double>) || propertytype == typeof(double))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToDouble(data), null);
                        }
                        else if (propertytype == typeof(System.Nullable<bool>) || propertytype == typeof(bool))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToBoolean(data), null);
                        }
                        else
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, data, null);
                        }
                    }
                    c++;
                }
                c = startY;
                result.Add(model);

            }

            return result;
        }
        public static List<T> ToObject<T>(Stream is1, int startX = 0, int startY = 0) where T : class
        {
            var result = new List<T>();
            XSSFWorkbook workbook = new XSSFWorkbook(is1);
            XSSFSheet sheet = (XSSFSheet)workbook.GetSheetAt(0);

            for (int i = startX; i <= sheet.LastRowNum; i++)
            {

                XSSFRow row = (XSSFRow)sheet.GetRow(i);

                var model = (T)typeof(T).GetConstructor(new System.Type[] { }).Invoke(new object[] { });//反射得到泛型类的实体
                PropertyInfo[] pro = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public);
                Type type = model.GetType();

                int c = startY;
                foreach (PropertyInfo propertyInfo in pro)
                {
                    var data = row.GetCell(c) == null ? string.Empty : row.GetCell(c).ToString();

                    if (Convert.IsDBNull(data))
                    {
                        continue;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(data)))
                    {
                        var propertytype = propertyInfo.PropertyType;
                        if (propertytype == typeof(System.Nullable<DateTime>) || propertytype == typeof(DateTime))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToDateTime(data), null);
                        }
                        else if (propertytype == typeof(System.Nullable<decimal>) || propertytype == typeof(decimal))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToDecimal(data), null);
                        }
                        else if (propertytype == typeof(System.Nullable<int>) || propertytype == typeof(int))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToInt32(data), null);
                        }
                        else if (propertytype == typeof(System.Nullable<double>) || propertytype == typeof(double))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToDouble(data), null);
                        }
                        else if (propertytype == typeof(System.Nullable<bool>) || propertytype == typeof(bool))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToBoolean(data), null);
                        }
                        else
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, data, null);
                        }
                    }
                    c++;
                }
                c = startY;
                result.Add(model);

            }

            return result;
        }
        public static List<T> ToObject<T>(FileInfo file, int startX = 0, int startY = 0) where T : class
        {
            var result = new List<T>();
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFSheet sheet = (XSSFSheet)workbook.GetSheetAt(0);

            for (int i = startX; i <= sheet.LastRowNum; i++)
            {
                XSSFRow row = (XSSFRow)sheet.GetRow(i);

                var model = (T)typeof(T).GetConstructor(new System.Type[] { }).Invoke(new object[] { });//反射得到泛型类的实体
                PropertyInfo[] pro = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public);
                Type type = model.GetType();

                int c = startY;
                foreach (PropertyInfo propertyInfo in pro)
                {
                    var data = row.GetCell(c) == null ? string.Empty : row.GetCell(c).ToString();

                    if (Convert.IsDBNull(data))
                    {
                        continue;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(data)))
                    {
                        var propertytype = propertyInfo.PropertyType;
                        if (propertytype == typeof(System.Nullable<DateTime>) || propertytype == typeof(DateTime))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToDateTime(data), null);
                        }
                        else if (propertytype == typeof(System.Nullable<decimal>) || propertytype == typeof(decimal))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToDecimal(data), null);
                        }
                        else if (propertytype == typeof(System.Nullable<int>) || propertytype == typeof(int))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToInt32(data), null);
                        }
                        else if (propertytype == typeof(System.Nullable<double>) || propertytype == typeof(double))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToDouble(data), null);
                        }
                        else if (propertytype == typeof(System.Nullable<bool>) || propertytype == typeof(bool))
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, Convert.ToBoolean(data), null);
                        }
                        else
                        {
                            type.GetProperty(propertyInfo.Name).SetValue(model, data, null);
                        }
                    }
                    c++;
                }
                c = startY;
                result.Add(model);

            }

            return result;
        }


    }
}
