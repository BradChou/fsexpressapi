﻿using Microsoft.International.Converters.TraditionalChineseToSimplifiedConverter;
using Microsoft.VisualBasic;
using NPinyin;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Common
{
    public class FSEAddressEntity
    {
        private readonly char[] allowChar;

        private readonly string[] specialRoad = { "一心新村", "一村巷", "七星村河背", "七福新村", "九如新村", "二橋新村", "三友新村", "三爪子坑路正貿新村", "三爪子坑路民享新村", "三圳村莊內巷", "三圳村莊尾巷", "三圳村溪埔巷", "三成村小西路", "三成村中平巷", "三村一街", "三村二街", "三村路", "三村路仁德屯", "三村路合作新村", "上山村大山路", "上山村中山路", "士宏新村", "大方新村", "大同新村", "大庄村中山巷", "大庄村中橫巷", "大庄村田中路", "大庄村新生巷", "大利新村", "大坪村", "大坵村", "大南村茅仔埔", "大城村西平路", "大城村東平路", "大城村東勢路", "大埔村永安路", "大埔村和興路", "大埔路有勝一村", "大埔路有勝二村", "大埔路有勝新村", "大埔路東泰新村", "大埔路國宅新村", "大埔路慶安新村", "大德新村", "大橋新村", "小西村文昌路", "小西村成功路", "小西村竹西路", "山腳村大山路", "山腳村中山路", "干城二村", "干城三村", "中大新村", "中山東路３段文化新村", "中北新村", "中正新村", "中正路１段篤行三村", "中正路３段中正新村",  "中村巷", "中和街２段一村巷", "中柳村", "中原新村", "中原新村仁愛樓", "中原新村忍耐樓", "中原新村和平樓", "中盛村河背", "中船一村", "中船二村", "中港路五守新村", "中興西村", "中興東村", "中興路田園二村", "五俊村中正巷", "五俊村新生巷", "五福新村", "仁和新村", "仁美新村", "仁愛村", "仁愛村中央路", "仁愛村芳頂路", "仁愛村芳寮路", "仁愛新村", "仁義新村", "仁壽路忠勤新村", "仁德新村", "介壽村", "介壽新村", "介壽路慈恩二十五村", "元中村復興路", "內埔村南華路", "內埔村崁腳", "內埔村過坑", "內甕村瓦厝仔", "內甕村過溪仔", "六合二村", "六根村豐海路", "公正新村", "公誠新村", "公路新村", "公館村東平路", "化成路台貿二村", "天仁東村", "天成新村", "天行新村", "天星新村", "天香新村", "天寶新村", "太平村民生路", "太平村茅仔埔", "太武新村", "太極新村", "文村街", "斗中路七巧新村", "斗中路東青新村", "水尾村太平路", "王功村仁愛巷", "王功村海埔", "王功村新生巷", "王功村新復巷", "功學社新村", "北村路", "北門口光復新村一街", "北門口光復新村二街", "司法新村", "司馬村永安路", "司馬村同安路", "台貿十村", "台貿三村", "台電新村", "四村路", "四維村", "外埔村復興路", "正義新村", "正德新村", "民生村仁愛巷", "民生村新興巷", "民生新村", "民生路１段村頭巷", "民享村", "民俗文化村", "民族新村", "民靖村文昌路", "民靖村成功路", "民權路２段富貴新村", "永春村福德巷", "永隆三村", "永嘉新村", "永寧新村", "永樂新村", "永興村仁愛路", "永興村永安巷", "永興村光復巷", "永興村和平巷", "永興村復興巷", "永豐村復興", "玉山一村", "玉山二村", "玉光村中正路", "玉峰村斯馬庫斯", "瓦厝村太平路", "瓦厝村永安路", "瓦厝村新庄路", "瓦厝村溪林路", "田子村共榮巷", "田沃村", "田美村四灣", "田美村田美", "田頭村光明路", "甲中新村",  "石門新村", "立全新村", "光明新村"
                , "光復路台電新村", "光華新村", "光雄新村", "全家福新村", "全興村東興路", "吉星新村", "吉祥新村", "名村巷", "合作一村", "合意新村", "合群新村", "合德新村", "后沃村", "回窯新村", "圳寮村中橫巷", "圳寮村陸軍路", "圳寮路仁愛新村", "多良村瀧", "安村巷", "安康新村", "安樂新村", "成功村庄北巷", "成功新村", "池西村小池角", "池東村小池角", "百世新村", "竹元村竹西路",  "竹村一路", "竹村七路", "竹村二路", "竹村三路", "竹村五路", "竹村巷", "竹村路", "竹高新村", "竹湖村移民", "竹塘村光明路", "自立新村", "自助新村", "自治新村", "自勉新村", "自強新村", "西安村望安", "西村", "西村路", "西村農場", "西坪村望安", "西坵村", "西城村西平路", "西城村西勢路", "西城村東勢路", "西畔村新生巷", "西港村北勢路", "西港村西勢路", "利仁新村", "坑厝村新庄路", "坑厝村溪林路", "坑厝村蔡厝路", "宏昌新村", "宏德司法新村", "宏德新村", "尾厝村永安路", "快樂二村", "村中一巷", "村中二巷", "村中巷", "村中路", "村外路", "村市路", "村民巷", "村西巷", "村西路", "村東一巷", "村東路", "村南路", "秀園新村", "秀巒村斯馬庫斯", "育樂新村", "谷村巷", "赤崁漁港新村", "和平村西北巷", "和平新村", "和美村司公部", "和睦村公館", "和睦村司公部", "和興村公館", "定遠新村", "居安新村", "居敬新村", "居廣一村", "岡山路岡農新村", "岩竹村福德巷", "岳明新村", "幸福新村", "延平新村", "忠孝新村", "忠勇新村", "忠誠新村", "昌隆村中正路", "明中新村", "明義新村", "明德新村", "明德路明駝一村", "明燈路１段瑞柑新村", "枕山一村", "東安村望安", "東州村太平路", "東州村永安路", "東州村東巷", "東村八街", "東村十二街", "東村三街", "東村六街", "東村東江", "東村路", "東里村復興", "東城村東平路", "東振村中山巷", "東振村和平巷", "東泰二村", "東泰新村", "東崎路５段雪山一村", "東港村北勢路", "東港村東勢路", "東陽新村", "東興村共榮巷", "東興新村", "板里村", "松竹新村", "松村巷", "松旺新村", "武營路台緘新村", "玫瑰新村", "社團新村", "芳中村中央路", "芳中村光華巷", "芳中村和平路", "芳苑村仁愛巷", "芳苑村文明巷", "芳苑村永安巷", "芳苑村永興巷", "芳苑村光復巷", "芳苑村和平路", "芹壁村", "花商新村", "金陵一村", "金湖新村", "金龍新村", "長安新村", "長庚醫護新村", "長春村福德巷", "青帆村", "信義新村", "信義新村行政大樓", "信德新村", "前村東路", "前村路", "南山新村", "南江村東江", "南村一巷", "南村二巷", "南村路", "南亞新村", "南昌新村", "南河村", "南埔新村", "南富村四灣", "南華村中山巷", "南華村和平巷", "南陽路育樂新村", "南陽路南陽新村", "南陽路國民新村", "屏女新村", "屏山新村", "建平村二溪路草２段", "建平村文明巷", "建平村復興巷", "建平村新興巷", "建成新村", "建村街", "建村街陳厝巷", "建軍路建國新村", "建軍路凌雲新村", "建軍路蘭竹新村", "建國一村", "建國七村", "建國九村", "建國二十村"
                , "建國二村", "建國八村", "建國十一村", "建國十七村", "建國十九村", "建國十二村", "建國十六村", "建國三村", "建國五村", "建國六村", "建國四村", "建國新村", "建基路１段弘祥新村", "建華新村", "建業新村", "建興村榮華巷", "建興村興農路", "建興新村", "後寮村芳寮路", "思村路", "恆中新村", "柑園村庄北巷", "泉州村復興路", "津沙村", "美村南路", "美村巷", "美村街", "美村路", "美村路１段", "美村路２段", "美崙山新村", "致遠新村", "重德新村", "香村路", "凌雲一村", "凌雲二村", "家興新村", "展宇美村", "峨眉村河背", "振豐村榮華巷", "料羅新村", "泰山村中正路", "泰宏新村", "海光四村", "海埔新村", "珠螺村", "益民新村", "粉寮醒吾新村", "草湖村二溪路草２段", "草湖村仁愛路", "草湖村永興巷", "草湖村光華巷", "草湖村新興巷", "馬祖村", "馬祖新村", "高樹村同安路", "健行新村", "健康新村一街", "健康新村二街", "健康新村三街", "健康新村五街", "健康新村六街", "健康新村四街", "國光新村", "國校新村", "國泰新村", "國華新村", "崇實新村", "崙腳村中平巷", "崙腳村和平路", "張厝村溪埔巷", "教師新村", "梅村一路", "梅村路",  "淑德新村", "莒光三村", "莒光村", "莊敬新村", "通基新村", "陸光五村", "陸光六村", "陸航二村", "頂林新村", "頂部村中正巷", "頂部村芳頂路", "鳥嶼漁港新村", "博愛村西北巷", "博愛村新興巷", "博愛新村", "富台新村", "富村街", "富康一村", "富貴新村", "富義新村", "富樂新村", "復國新村", "復華新村", "復興村", "復興新村", "惠民二村", "惠民新村", "普仁一村", "普仁新村", "曾家村東興路", "游泳路慈光二十九村", "番路村瓦厝仔", "稅務新村", "紫雲村崁腳", "華忠新村", "華美新村", "華夏一村", "華夏一村市場", "華夏二村", "華夏新村", "華國新村", "華陽新村", "華豐街三村巷", "菜寮村中山路", "菜寮村中正路", "菜寮村西平路", "菜寮村東平路", "貿商九村", "逸境新村", "進村巷", "郵工新村", "鄉村世界", "開發新村", "陽明新村", "隆德新村", "雲林新村", "雲林新村一路", "雲林新村二路", "黃埔七村", "黃埔七村特一巷", "黃埔二村西一巷", "黃埔二村西二巷", "黃埔二村西五巷", "黃埔二村東四巷", "黃埔三村", "黃埔五村", "黃埔六村", "黃埔四村", "黃埔新村西一巷", "黃埔新村西二巷", "黃埔新村西三巷", "黃埔新村西五巷", "黃埔新村西六巷", "黃埔新村西四巷", "黃埔新村東一巷", "黃埔新村東一橫巷", "黃埔新村東二巷", "黃埔新村東二橫巷", "黃埔新村東三巷", "黃埔新村東五巷", "黃埔新村東六巷", "黃埔新村東四巷", "黃埔新村國校巷", "塘岐村", "塘興村坪頂", "塭豐村豐海路", "慈光九村", "慈光二十七村", "慈光十四村", "慈安六村", "慈安新村", "慈恩一村", "慈恩十一村", "慈恩十三村", "慈祥二村", "慈暉六村", "慈暉新村", "新村", "新村路", "新村路２段", "新南村興農路", "新港村復興路", "新廣村光明路", "新豐村和興路", "新寶村中央路", "新寶村海埔", "源泰新村", "溫泉村民生路", "溪州村永安路", "溪州村莊內巷"
                , "溪州村溪林路", "溪厝村中山巷", "溪厝村太平路", "溪厝村溪林路", "溪厝村溪埔巷", "溪厝村蔡厝路", "獅山村田美", "萬和村中正路", "萬富新村", "萬巒村中正路", "義仁村坪頂", "聖湖新村", "裕農新村", "路上村西北巷", "路平村和平巷", "農專新村", "達來村達發達旺巷", "電信一村", "僑興新村", "嘉女新村", "嘉工新村", "嘉中新村", "嘉惠二村", "嘉稅一村", "嘉義新村", "嘉農新村", "嘉警新村", "寧埔村移民", "實踐四村", "實踐新村", "榮友一村", "榮友新村", "榮民新村", "榮光村田中路", "榮光新村", "榮安一村", "榮安二村", "榮村", "漁工新村", "漁村", "漁村街", "福正村", "福安新村", "福沃村", "福金新村", "福榮村小西路", "福榮村新復巷", "福德新村", "福樂新村", "精忠一村", "精忠七村", "精忠三村", "精忠六村", "精忠新村", "綠園新村", "維新新村", "翠村街", "銀聯二村", "銘泰新村", "鳳翔新村", "廣福村中央路", "廣福村中正路", "廣福村興農路", "廣興村中正路", "影劇一村", "影劇三村", "影劇四村", "樂華村", "澎水新村", "憲光二村", "曉風新村", "橋仔村", "橋頭村復興路", "燄溫村豐海路", "篤行五村", "篤行六村", "縣府新村", "興仁村仁愛巷", "興村街", "興南村南華路", "興泰新村", "興華二村", "興業二村", "興業三村", "興業新村", "親仁新村", "錦村東二巷", "龍山村過坑", "龍岡路３段今日新村", "龍昇新村", "龍東路百寧新村", "龍東路慈光二十八村", "龍城新村", "龍新新村", "龍壽新村", "龍鳳新村", "龍關新村", "儲蓄新村", "勵志一村", "勵志二村", "勵志新村", "擎天一村", "聯和新村", "聯福新村", "臨海新村", "鍾山新村", "禮蘭新村", "舊村一街", "舊村二街", "舊村三街", "舊村五街", "舊村六街", "舊村四街", "舊眉村陸軍路", "蟠桃新村", "豐山村中山路", "豐山村仁愛街", "豐山村和平街", "豐山村忠孝街", "豐村", "豐坪村中山路", "豐南村復興", "豐裡村中山路", "豐裡村仁愛街", "豐裡村和平街", "豐裡村忠孝街", "豐德新村", "鎌村一街", "鎌村路", "觸口村過溪仔", "警光一村", "警光新村", "蘭陽新村", "鐵路一村", "鐵路二村", "鶯華新村", "鹽樹村中央路", "七里橋", "九里路", "八里大道", "八里堆", "三里巷", "三貂里頂坑", "三隆里光華路", "下里路", "下興里山下", "上埔里崎仔頭", "大里一街", "大里二街", "大里街", "大里路", "大里路中里巷", "大里路西里巷", "大里網", "大順里城仔頂", "大園里廟後", "大寮里光華路", "大興里城仔", "山下里山下", "山里路", "中正里中興街", "中里路", "中和里海口", "中龍里中正路", "中龍里中南街", "中龍里館前街", "五北里中山路", "五里路", "五南里中山路", "仁里一街", "仁里七街", "仁里九街", "仁里二街", "仁里八街", "仁里十二街", "仁里十三街", "仁里十四街", "仁里三街", "仁里五街", "仁里六街", "仁里四街", "仁里板", "仁里街", "仁里路", "仁善里三塊厝", "內阿里磅", "內島里中山路", "太麻里街", "水里一路", "水里二路", "水里三路", "水里五路", "水里六路", "水里路", "北太麻里", "北平里文明巷"
                , "北里巷", "北勢里北勢", "北興里北勢", "北龍里中正路", "北龍里館前街", "永興里文明巷", "永豐里城仔", "田心里崁腳", "白東里中山路", "后里路", "竹圍里崁腳", "西里巷", "西阿里山", "西阿里關", "西溪里後厝路", "利里武街", "巫里岸街", "牡丹里下坑", "里中路", "里仁路", "里北路", "里和路", "里東路", "里林西路", "里林西路文化巷", "里林西路文明巷", "里林西路明光巷", "里林西路明德南巷", "里林東路", "里林東路公路巷", "里林東路北光二巷", "里林東路明德南巷", "里林東路南光巷", "里林東路柯厝巷", "里林東路頂頭巷", "里林東路楊厝巷", "里林東路溪北巷", "里林東路興樹巷", "里林東路鐵路東巷", "里林東路鐵路南巷", "里金館", "里信路", "里南路", "里智路", "里港路", "里港路光復巷", "里順路", "里新路", "里義路", "里德路", "里龍", "里嶺路", "佳里興", "和平里劉厝", "房里路", "明里路", "東里一街", "東里七街", "東里九街", "東里二街", "東里八街", "東里十一街", "東里十二街", "東里十五街", "東里十四街", "東里十街", "東里三街", "東里五街", "東里四街", "東里村復興", "東里路", "東阿里山", "東勢里中正巷", "東勢里光復巷", "東勢里農場巷", "板里村", "金包里街", "金包里街舊市場", "長源里下坑", "長福里", "阿里荖", "阿里磅", "南八里", "南里街", "南阿里山", "南港里許厝港", "南興里三塊厝", "南龍里中正路", "南龍里中南街", "南龍里館前街", "科里", "重興里新厝", "香田里竹圍巷", "原斗里中正巷", "原斗里民生巷", "原斗里竹圍巷", "埔心里廟後", "埔頂里中心路", "宮后里", "海寶里海口", "高原里大庄", "啦里吧", "梅里路", "梅芳里太平路", "通西里中山路", "通西里漁港路", "通南里中山路", "通灣里中山路", "通灣里漁港路", "頂厝里二溪路", "頂厝里文明巷", "頂厝里光復巷", "魚行里頂坑", "蛤里味", "街尾里竹圍巷", "詔安里竹圍巷", "集里路", "新民里中心路", "新民里中正路", "新安里新社", "新里巷", "新英里北勢", "新厝里新厝", "新埔里中山路", "新富里北勢", "新華里崎仔頭", "新貴里北勢", "新勢里北勢", "新榮里北勢", "新豐里新社", "溪海里劉厝", "萬里加投", "萬里路", "萬興里二溪路", "萬興里農場巷", "聖德里大庄", "達里", "過溪里光華路", "嘉里一街", "嘉里一路", "嘉里二街", "嘉里二路", "嘉里三街", "嘉里三路", "嘉里四路", "嘉里路", "嘉里路機場", "網紗里坑內路", "廣福里中興街", "廣興里城仔", "德其里", "潮寮里光華路", "豐田里太平路", "豐里街", "鵝鑾里坑內路", "龔厝里後厝路", "灣寶里海口", "丁厝街", "七塊厝", "十一指厝", "十塊厝", "三厝西二巷", "三厝東一巷", "三厝街", "三間厝", "三塊厝", "下石厝路", "下面厝", "下厝", "下厝仔", "下厝坑", "下厝巷", "下厝街", "下厝路", "下新厝", "下新厝農場", "下縣厝子", "上厝", "上厝巷", "口厝路", "大舍東路公厝巷", "大厝", "大厝巷", "大湖路余厝巷", "大溪厝", "大溪路１段李厝巷", "山鶯路黃厝巷", "中山路３段厝仔巷", "中正三路邱厝巷"
                , "中正三路許厝巷", "中正三路曾厝巷", "中正三路鄭厝巷", "中厝巷", "中厝路", "五間厝", "五塊厝", "仁善里三塊厝", "內厝二路", "內厝八路", "內厝十一路", "內厝三路", "內厝子", "內厝五路", "內厝六路", "內厝街", "內厝路", "內甕村瓦厝仔", "六塊厝", "公厝", "公厝仔", "公厝北路", "天保厝", "文厝巷", "方厝寮", "水頭厝", "王厝", "王厝一橫巷", "王厝巷", "王厝路", "王厝寮", "丘厝", "仕隆路李厝巷", "仕隆路林厝巷", "仕隆路馬厝巷", "仕隆路陳厝巷", "占富厝", "民生路１段後厝巷", "玉屏路新厝巷", "瓦厝", "瓦厝子", "瓦厝村太平路", "瓦厝村永安路", "瓦厝村新庄路", "瓦厝村溪林路", "瓦厝巷", "瓦厝埔", "瓦厝區", "瓦厝街", "瓦厝路", "甘厝", "田厝一街", "田厝一路", "田厝二街", "田厝二路", "田厝三街", "田厝巷", "田厝街", "田厝路", "甲頭厝", "白鴿厝農場", "石頭厝", "石頭厝路", "后厝", "安家厝", "尖山路卓厝巷", "尖厝崙", "江西厝巷", "江厝仔", "江厝西巷", "江厝店", "江厝南巷", "江厝路", "竹高厝", "竹篙厝", "羊稠厝", "西厝路", "西湖街曾厝巷", "西溪里後厝路", "何厝東一街", "何厝東二街", "何厝街", "余厝巷", "吳仔厝", "吳厝一街", "吳厝二街", "吳厝三街", "吳厝五街", "吳厝六街", "吳厝巷", "吳厝街", "吳厝窠", "吳厝路", "呂厝巷", "呂厝路", "坑厝村新庄路", "坑厝村溪林路", "坑厝村蔡厝路", "宋厝巷", "尾厝", "尾厝村永安路", "尾厝路", "李厝路", "沈厝仔", "秀厝一街", "秀厝二街", "里林東路柯厝巷", "里林東路楊厝巷", "和平里劉厝", "和厝路１段", "和厝路２段", "周厝巷", "東厝路", "林厝巷", "林厝路", "板頭厝", "松山街尾厝巷", "社厝坑", "邱厝北一巷", "邱厝北二巷", "邱厝北四巷", "邱厝北巷", "邱厝南巷", "邱厝巷", "保長厝街", "前厝", "前厝路", "南昌路公厝巷", "南港里許厝港", "南勢厝路", "南靖厝", "南興里三塊厝", "型厝寮", "姚厝", "客厝", "客厝路", "建村街陳厝巷", "建樹路前厝巷", "後厝", "後厝一巷", "後厝二巷", "後厝子", "後厝仔", "後厝巷", "後厝路", "後壁厝", "挖仔厝", "施厝", "施厝巷", "施厝寮", "柯厝", "柯厝坑", "柯厝巷", "泉州厝", "泉厝路", "洋厝巷", "洪厝", "洪厝巷", "洪厝街", "洪厝寮", "紅土厝", "紅毛厝", "胡厝巷", "胡厝路", "胡厝寮", "茄投路田尾厝巷", "重興里新厝", "面前厝", "香山厝", "倒厝子", "原厝路", "厝子", "厝後巷", "厝後路", "厝勝路", "厝興路", "員鹿路４段鳳厝巷", "埔仔厝", "孫厝寮", "崁頭厝", "海方厝", "海豐厝", "海豐路新厝巷", "破瓦厝子", "草蓆厝", "馬厝", "馬頭厝", "馬麟厝", "珩厝", "張厝", "張厝一巷", "張厝二巷", "張厝三巷", "張厝四巷", "張厝村溪埔巷", "張厝巷", "梁厝", "梅子厝", "荷苞厝", "許厝", "許厝一街", "許厝二街", "許厝庄仔", "許厝巷", "許厝港", "許厝港路", "郭厝巷",  "陳厝坑", "陳厝坑路", "陳厝巷", "陳厝寮", "頂石厝路", "頂厝子", "頂厝里二溪路"
                , "新北大道", "頂厝里文明巷", "頂厝里光復巷", "頂厝巷", "頂厝路", "麥厝街", "彭厝路", "曾厝巷", "曾厝街", "港墘厝", "游厝庄", "湖仔厝", "番子厝", "番路村瓦厝仔", "菜公厝", "詔安厝", "順平厝", "黃厝", "黃厝巷", "黃厝街", "黃厝路", "媽祖厝", "新平路１段朱厝巷", "新厝子", "新厝仔", "新厝里新厝", "新厝巷", "新厝街", "新厝路", "楊厝", "楊厝一街", "楊厝二街", "楊厝巷", "楊厝路", "溫厝", "溪岸路新厝巷", "溪厝村中山巷", "溪厝村太平路", "溪厝村溪林路", "溪厝村溪埔巷", "溪厝村蔡厝路", "溪海里劉厝", "溪墘厝", "葉厝路", "董厝", "詹厝巷", "過溝中厝", "過溝頂厝", "過溝廈厝", "鄒厝崙", "雷厝", "對面厝", "廖厝巷", "福利路大厝巷", "管事厝", "管厝街", "鳳尾厝", "劉厝巷", "劉厝路", "廣溝厝", "德松路劉厝巷", "歐厝", "潮厝", "潮厝路", "澎湖厝", "潘厝巷", "潘厝橫巷", "蔡厝仔", "蔡厝巷", "蔡厝路", "橋頭路林厝巷", "盧厝挖", "磚雅厝", "興厝路", "蕃子厝", "蕃薯厝", "蕭厝坑", "賴厝", "賴厝東巷", "賴厝街", "錢厝坑", "頭前厝",  "謝厝巷", "謝厝寮", "舊厝子", "雙人厝", "顏厝巷", "顏厝寮", "羅厝路１段", "羅厝路２段", "羅厝路３段", "寶斗厝坑", "籃子厝", "籃厝", "蘇厝坑", "蘇厝寮", "龔厝里後厝路", "龔厝路", "三和二路一街","三和二路二街","中路北街","中路南街","中路街","中興路一街","中興路二街","六路一街","六路七街","六路九街","六路二街","六路八街","六路十一街","六路十七街","六路十九街","六路十二街","六路十八街","六路十三街","六路十五街","六路十六街","六路十四街","六路十街","六路三街","六路五街","六路六街","公園路二街","平山一路一街","平山一路二街","平山一路三街","平山一路四街","永興路一街","光華二路一街","光華二路二街","光華四路一街","光榮北路一街","光榮北路七街","光榮北路二街","光榮北路三街","光榮北路五街","光榮北路六街","光榮北路四街","光榮西路一街","光榮東路一街","光榮東路二街","光榮南路一街","光榮南路二街","光榮南路三街","光榮南路四街","同源路一街","同源路七街","同源路九街","同源路二街","同源路八街","同源路十街","同源路三街","同源路五街","同源路六街","同源路四街","成功二路一街","成功三路二街","成功三路三街","成功三路五街","成功三路四街","竹林路一街","竹林路二街","竹林路三街","西路街","車路街","車路頭街","府南路一街","府南路二街","東山路一街","東路街","東龍路後街","東舊路街","復興路一街","廣東路一街","廣東路二街","廣東路六街","廣東路四街","鐵路街","大街路","中街路","新街路","舊街一路","舊街二路","舊街路", "丁厝街","七塊厝","十一指厝","十塊厝","三厝西二巷","三厝東一巷","三厝街","三間厝","三塊厝","下石厝路","下面厝","下厝","下厝仔","下厝坑","下厝巷","下厝街","下厝路","下縣厝子","上厝","上厝巷","口厝路","大舍東路公厝巷","大厝","大厝巷","大湖路余厝巷","大溪厝","大溪路１段李厝巷","山鶯路黃厝巷"
                , "新厝子","新厝仔","新厝里新厝","新厝巷","新厝街","新厝路","楊厝一街","楊厝二街","楊厝巷","楊厝路","溫厝","溪岸路新厝巷","溪厝村中山巷","溪厝村太平路","溪厝村溪林路","溪厝村溪埔巷","溪厝村蔡厝路","溪海里劉厝","溪墘厝","葉厝路","董厝","詹厝巷","過溝中厝","過溝頂厝","過溝廈厝","鄒厝崙","雷厝","對面厝","廖厝巷","福利路大厝巷","管事厝","管厝街","鳳尾厝","劉厝","劉厝巷","劉厝路","廣溝厝","德松路劉厝巷","潮厝","潮厝路","澎湖厝","潘厝巷","潘厝橫巷","蔡厝仔","蔡厝巷","蔡厝路","橋頭路林厝巷","盧厝","盧厝挖","磚雅厝","興厝路","蕃子厝","蕃薯厝","蕭厝坑","賴厝東巷","賴厝街","錢厝坑","頭前厝","謝厝","謝厝巷","謝厝寮","舊厝子","雙人厝","顏厝巷","顏厝寮","魏厝","羅厝路１段","羅厝路２段","羅厝路３段","寶斗厝坑","籃子厝","籃厝","蘇厝坑","蘇厝寮","龔厝里後厝路","龔厝路", "中厝",   "盧厝","江厝", "石厝", "蔡厝", "劉厝", "新厝","內厝","賴厝" ,"蘇厝", "田厝", "蘇厝", "吳厝", "魏厝", "羅厝", "呂厝", "李厝", "林厝"};

        public FSEAddressEntity(string orgData)
        {
            this.Area = string.Empty;
            this.allowChar = new char[] { ' ', '一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '零', '○', '１', '２', '３', '４', '５', '６', '７', '８', '９', '０', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '之' };

            string realAddress = IntFomalAddress(orgData);

            string output = PassData(realAddress);

            this.No = 0;
            this.Floor = 0;

            this.OrgAddress = orgData;
            this.TransAddress = output;

            //修正內容
            SpecialTrans();
        }


        public FSEAddressEntity(string orgData, string city, string district)
        {
            this.Area = string.Empty;
            this.allowChar = new char[] { ' ', '一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '零', '○', '１', '２', '３', '４', '５', '６', '７', '８', '９', '０', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '之' };

            string realAddress = IntFomalAddress(orgData);

            string output = PassData(realAddress);

            this.No = 0;
            this.Floor = 0;

            this.City = (city == null) ? string.Empty : city.Trim();
            this.District = (district == null) ? string.Empty : district.Trim();

            this.OrgAddress = orgData;
            this.TransAddress = output;

            //修正內容
            SpecialTrans();
        }

        public string City { get; set; }

        public string District { get; set; }

        public string Area { get; set; }

        public string SubArea { get; set; }

        public string Road { get; set; }

        public string Section { get; set; }

        public string Line { get; set; }

        public string Alley { get; set; }

        public string SubAlley { get; set; }

        public string NoString { get; set; }

        public int No { get; set; }

        public string FloorString { get; set; }

        public int Floor { get; set; }

        public string Result { get; set; }

        public string FindNo { get; set; }

        public string OrgAddress { get; set; }

        public string TransAddress { get; set; }

        public string PostRoad { get; set; }

        public string PostStreet { get; set; }

        public string PinYinRoad { get; set; }

        public string PinYinStreet { get; set; }

        private void SpecialTrans()
        {
            if (this.City.Length > 0 && this.Road.Length >= 4 && this.Road.Substring(0, 4) != "新北大道")
            {
                string city = this.City[0..^1];

                //去除市
                if (this.PostRoad.StartsWith(city))
                {
                    this.PostRoad = this.PostRoad.Replace(city, "");
                    this.PostStreet = this.PostStreet.Replace(city, "");
                }
            }




            if (this.District.Length > 0)
            {
                string area = this.District[0..^1];
                //去除區
                if (this.PostRoad == "歸仁十一路" || this.PostRoad == "歸仁十二路" || this.PostRoad == "歸仁十三路" || this.PostRoad == "歸仁十四路" || this.PostRoad == "歸仁十五路" || this.PostRoad == "歸仁十六路" || this.PostRoad == "歸仁十八路") { }
                else if (this.PostRoad == "南和一街") { }
                else
                {
                    if (this.PostRoad.StartsWith(area) && this.PostRoad.Length > (area.Length + 2) && this.PostRoad.IndexOf('段') < 0)
                    {
                        this.PostRoad = this.PostRoad.Replace(area, "");
                        this.PostStreet = this.PostStreet.Replace(area, "");
                    }
                }
            }

            if (this.City == "桃園市桃園區縣")
            {
                this.City = "桃園市";
                this.Area = "桃園區";


            }

            if (this.City == "新北市板橋區縣")
            {
                this.City = "新北市";
                this.Area = "板橋區";

            }

            if (this.City == "嘉義縣" && this.District == "太保市" && this.PostRoad == "興業路")
            {
                this.PostRoad = "嘉太工業區興業路";

                this.PostStreet = "嘉太工業區興業路";
            }

            if (this.City == "高雄市" && this.District == "大樹區" && this.PostRoad == "小坪村")
            {
                this.PostRoad = "小坪路";

                this.PostStreet = "小坪路";
            }

            if (this.City == "彰化縣" && this.District == "福興鄉" && this.PostRoad == "廈粘村")
            {
                this.PostRoad = "廈粘街";

                this.PostStreet = "廈粘街";
            }

            if (this.City == "臺南市" && this.District == "仁德區" && this.Area == "田厝")
            {
                this.PostRoad = string.Format("{0}{1}", this.Area, this.PostRoad);
                this.PostStreet = string.Format("{0}{1}", this.Area, this.PostStreet);
            }

            if (this.City == "臺中市" && this.District == "豐原區" && this.Area == "鎌村")
            {
                this.PostRoad = string.Format("{0}{1}", this.Area, this.PostRoad);
                this.PostStreet = string.Format("{0}{1}", this.Area, this.PostStreet);
            }

            if (this.City == "臺中市" && this.District == "太平區" && this.PostRoad == "內湖路")
            {
                this.PostRoad = "內湖";
                this.PostStreet = "內湖";
            }


            if (this.City == "新北市" && SubArea == "荖阡坑路7鄰")
            {
                this.PostRoad = "荖阡坑路";
                this.PostStreet = "荖阡坑街";
            }


            if (this.City == "桃園市" && this.District == "龍潭區" && this.PostRoad == "富華街３林段")
            {
                this.PostRoad = "富華路三林段";
                this.PostStreet = "富華街三林段";
            }

            if (this.City == "臺中市" && this.District == "東區" && (this.PostRoad == "英一街" || this.PostRoad == "英二街" || this.PostRoad == "英三街" || this.PostRoad == "英五街" || this.PostRoad == "英六街" || this.PostRoad == "英七街" || this.PostRoad == "英八街" || this.PostRoad == "英九街" || this.PostRoad == "英十街" || this.PostRoad == "英十一街" || this.PostRoad == "英十二街" || this.PostRoad == "英十三街" || this.PostRoad == "英十五街" || this.PostRoad == "英十六街" || this.PostRoad == "英十七街"))
            {
                this.PostRoad = "東" + this.PostRoad;
                this.PostStreet = "東" + this.PostStreet;
            }

            if (this.City == "新北市" && this.Area == "板橋區" && this.PostRoad.Contains("民大道"))
            {
                this.PostRoad = "縣" + this.PostRoad;
                this.PostStreet = "縣" + this.PostStreet;
            }



        }

        private string PassData(string orgData)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            string output = orgData;

            try
            {
                if (output.Contains("公舘路") || output.Contains("瓦磘街")) { }
                else
                {
                    output = Strings.StrConv(orgData, VbStrConv.Narrow, 1028);
                }
            }
            catch
            {
                //dummy
            }

            //去除開通的數字
            output = TrimStartNum(output);

            //去除字串空白
            output = Regex.Replace(output, @"\s", "");

            //string json = JsonConvert.SerializeObject(cOutput);

            //Console.WriteLine(json);

            //取掉括號
            output = IniRoadString(output);

            //取得City
            string[] cityArray = { "縣", "市", "巿" };

            string tempOutput = output;

            string city = CutString(tempOutput, cityArray, ref tempOutput);

            if ((city.Length > 2 && city.Substring(city.Length - 2) == "新市") || city == "新市")
            {
                tempOutput = output;
            }
            else
            {
                this.City = city.Trim();
            }

            //取得District
            string[] districtArray = { "區", "鄉", "鎮", "市" };

            string dictrict = CutString(tempOutput, districtArray, ref tempOutput);

            if (dictrict == "工業區")
            {
                tempOutput = string.Format("{0}{1}", dictrict, tempOutput);
            }
            else
            {
                this.District = dictrict.Trim();
            }

            if (this.City == "彰化縣彰化市")
            {
                this.City = "彰化縣";
                this.District = "彰化市";
            }

            if (this.City == "彰化縣員林市")
            {
                this.City = "彰化縣";
                this.District = "員林市";
            }

            //去除重複的市區
            if (this.City != null && this.District != null && this.City.Length > 0 && this.District.Length > 0)
            {
                tempOutput = tempOutput.Replace(string.Format("{0}{1}", this.City, this.District), "");
            }

            //取得里
            string[] areaArray = { "村", "里", "寮", "厝" };

            string area = CutString(tempOutput, areaArray, ref tempOutput);

            if (area == "鐮村" || area == "三村" || area == "美村" || area == "南村" || area == "鎌村" || area == "富村" || area == "粉寮" || area == "和厝" || area == "後寮")
            {
                tempOutput = string.Format("{0}{1}", area, tempOutput);
            }
            else
            {
                this.Area = area.Trim();
            }

            if (tempOutput.Contains("華夏巷西一弄")) { this.Road = "華夏巷西一弄"; }

            string[] subAreaArray = { "鄰" };

            string subArea = CutString(tempOutput, subAreaArray, ref tempOutput);

            this.SubArea = subArea.Trim();

            //取得路
            string[] roadArray = { "路", "街", "大道", "巷", "寮", "崙子頂", "蒜頭", "埤角" };
            char[] cRoadArray = { '路', '街', '里', '村' };

            if (tempOutput.Contains("華夏巷西一弄")) { }
            else
            {
                string road = CutString(tempOutput, roadArray, ref tempOutput);



                this.Road = road.Trim();

                if (road.Length == 0 && output.IndexOfAny(cRoadArray) > 0)
                {
                    road = CutString(output, roadArray, ref tempOutput);
                    this.Road = road.Trim();
                }
            }





            //處理字體及特殊字
            this.Road = TransRoadSpecialCharacter(this.Road);

            //處理特別路
            string szTempRoad = string.Empty;
            bool specialRoad = HandleSpecialRoad(orgData, ref szTempRoad);
            if (specialRoad)
            {
                this.Road = szTempRoad;
                tempOutput = orgData.Substring(orgData.IndexOf(szTempRoad) + szTempRoad.Length);
            }

            if (output.Contains("桃園市桃園區縣府路") && this.Road == "府路")
            {
                this.PostRoad = "縣府路";
                this.PostStreet = "縣府街";
                this.Road = "縣府路";
            }

            //取得段
            string[] secArray = { "段" };

            string section = CutString(tempOutput, secArray, ref tempOutput);

            this.Section = section.Trim();

            //取得巷
            string[] lineArray = { "巷" };

            string line = CutString(tempOutput, lineArray, ref tempOutput);

            this.Line = line.Trim();

            //取得弄
            string[] alleyArray = { "弄" };

            string alley = CutString(tempOutput, alleyArray, ref tempOutput);

            this.Alley = alley.Trim();

            //取得衖
            string[] subAlleyArray = { "衖" };

            string subAlley = CutString(tempOutput, subAlleyArray, ref tempOutput);

            this.SubAlley = subAlley.Trim();

            //取得號
            string[] noArray = { "號", "号" };

            string noString = CutString(tempOutput, noArray, ref tempOutput);

            noString = GetNo(orgData);

            this.NoString = noString.Trim();



            //取得楼
            string[] floorArray = { "楼", "樓" };

            string floorString = CutString(tempOutput, floorArray, ref tempOutput);

            this.FloorString = floorString.Trim();


            //最後字串
            this.Result = tempOutput;

            if (this.NoString.Length == 0 && tempOutput.Length > 0)
            {
                string tail = GetTailNo(tempOutput);

                if (tail.Length > 0)
                {
                    this.NoString = tail;
                }
            }

            if (this.NoString.Length > 0)
            {
                this.NoString = TransNo(this.NoString);
            }

            //取得判斷字串
            if (this.Line.Length > 0)
            {
                this.FindNo = TransLine(this.Line.Trim()).Trim();
            }
            else
            {
                this.FindNo = TransNoToFind(this.NoString.Trim()).Trim();
            }

            if (this.Road.Length == 0 && this.FindNo.Length > 0)
            {
                this.Road = FindNonFormatRoad(output);
            }

            //this.FindNo = (this.Line.Length > 0) ? this.Line : this.NoString;

            this.PostRoad = this.Road.Trim();

            if (this.Section.Length > 0)
            {
                string transSection = TransSection(this.Section);
                this.PostRoad = string.Format("{0}{1}", this.Road.Trim(), transSection.Trim());
            }

            this.PostStreet = this.PostRoad;

            if (this.PostRoad.IndexOf("路") > 0)
            {
                this.PostStreet = this.PostRoad.Replace("路", "街");
            }
            else if (this.PostRoad.IndexOf("街") > 0)
            {
                this.PostStreet = this.PostRoad.Replace("街", "路");
            }

            if (this.PostRoad.Length == 0 && this.PostStreet.Length == 0 && this.Line.Length > 0)
            {
                this.PostStreet = this.Line;
                this.PostRoad = this.Line;
            }

            if (this.PostRoad.Length == 0 && this.PostStreet.Length == 0 && this.Alley.Length > 0)
            {
                this.PostStreet = this.Alley;
                this.PostRoad = this.Alley;
            }

            if (this.PostRoad.Length == 0 && this.PostStreet.Length == 0 && this.Area.Length > 0)
            {
                this.PostStreet = this.Area;
                this.PostRoad = this.Area;
            }

            //還原新市
            this.PostRoad = this.PostRoad.Replace("∞∞", "新市");
            this.PostStreet = this.PostStreet.Replace("∞∞", "新市");

            //HanyuPinyinOutputFormat hanyuPinyinOutputFormat = new HanyuPinyinOutputFormat();
            this.PinYinRoad = GetPinyinString(this.PostRoad);
            this.PinYinStreet = GetPinyinString(this.PostStreet);

            return output;
        }

        private string FindNonFormatRoad(string output)
        {
            string formatOutput = output;

            if (this.FindNo.Length > 0 && output.Length > 0 && output.IndexOf(this.FindNo) > 0)
            {
                formatOutput = output.Substring(0, output.IndexOf(this.FindNo));

                char[] cFindC = { '里', '村', '鄉', '鄰', '縣', '區' };

                if (formatOutput.LastIndexOfAny(cFindC) > 0)
                {
                    formatOutput = formatOutput.Substring(formatOutput.LastIndexOfAny(cFindC) + 1);
                }
            }
            return formatOutput;
        }

        private string IniRoadString(string orgString)
        {
            string output = orgString;

            output = IniRoadRealString(output, '(', ')');
            output = IniRoadRealString(output, '[', ']');
            output = IniRoadRealString(output, '{', '}');

            return output;
        }

        private string IniRoadRealString(string orgString, char start, char end)
        {
            string output = orgString;

            if (orgString.IndexOf(start) >= 0 && orgString.IndexOf(end) > 0 && orgString.IndexOf(end) > orgString.IndexOf(start))
            {
                string head = orgString.Substring(0, orgString.IndexOf(start));

                string tail = orgString.Substring(orgString.IndexOf(end) + 1);

                output = string.Format("{0}{1}", head, tail);
            }

            return output;
        }

        private string TrimStartNum(string org)
        {
            char[] caNum = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ' };

            string output = org.TrimStart(caNum);

            return output;
        }

        private string GetPinyinString(string org)
        {
            if (org.Length > 0)
            {

                string tempStr = org.Substring(0, org.Length - 1);

                try
                {
                    tempStr = Strings.StrConv(tempStr, VbStrConv.SimplifiedChinese, 2052);
                }
                catch
                {
                    //dummy
                }

                string output = Pinyin.GetPinyin(tempStr);

                output = output.Replace(" ", "");

                TextInfo myTI = new CultureInfo("en-US", false).TextInfo;

                output = myTI.ToTitleCase(output);


                if (org.IndexOf("路") > 0)
                {
                    output = string.Format("{0} Rd.", output);
                }
                else if (org.IndexOf("街") > 0)
                {
                    output = string.Format("{0} St.", output);
                }

                return output;
            }
            else
            {
                return org;
            }
        }


        private string TransRoadSpecialCharacter(string road)
        {
            string output = road;

            output = output.Replace("ㄧ", "一");
            output = output.Replace("玔", "圳");
            //output = output.Replace("工業", "工業區");
            output = output.Replace("９", "9");
            output = output.Replace("８", "8");
            output = output.Replace("７", "7");
            output = output.Replace("６", "6");
            output = output.Replace("５", "5");
            output = output.Replace("４", "4");
            output = output.Replace("３", "3");
            output = output.Replace("２", "2");
            output = output.Replace("１", "1");
            output = output.Replace("０", "0");


            output = output.Replace("99", "九十九");
            output = output.Replace("98", "九十八");
            output = output.Replace("97", "九十七");
            output = output.Replace("96", "九十六");
            output = output.Replace("95", "九十五");
            output = output.Replace("94", "九十四");
            output = output.Replace("93", "九十三");
            output = output.Replace("92", "九十二");
            output = output.Replace("91", "九十一");
            output = output.Replace("90", "九十");
            output = output.Replace("89", "八十九");
            output = output.Replace("88", "八十八");
            output = output.Replace("87", "八十七");
            output = output.Replace("86", "八十六");
            output = output.Replace("85", "八十五");
            output = output.Replace("84", "八十四");
            output = output.Replace("83", "八十三");
            output = output.Replace("82", "八十二");
            output = output.Replace("81", "八十一");
            output = output.Replace("80", "八十");
            output = output.Replace("79", "七十九");
            output = output.Replace("78", "七十八");
            output = output.Replace("77", "七十七");
            output = output.Replace("76", "七十六");
            output = output.Replace("75", "七十五");
            output = output.Replace("74", "七十四");
            output = output.Replace("73", "七十三");
            output = output.Replace("72", "七十二");
            output = output.Replace("71", "七十一");
            output = output.Replace("70", "七十");
            output = output.Replace("69", "六十九");
            output = output.Replace("68", "六十八");
            output = output.Replace("67", "六十七");
            output = output.Replace("66", "六十六");
            output = output.Replace("65", "六十五");
            output = output.Replace("64", "六十四");
            output = output.Replace("63", "六十三");
            output = output.Replace("62", "六十二");
            output = output.Replace("61", "六十一");
            output = output.Replace("60", "六十");
            output = output.Replace("59", "五十九");
            output = output.Replace("58", "五十八");
            output = output.Replace("57", "五十七");
            output = output.Replace("56", "五十六");
            output = output.Replace("55", "五十五");
            output = output.Replace("54", "五十四");
            output = output.Replace("53", "五十三");
            output = output.Replace("52", "五十二");
            output = output.Replace("51", "五十一");
            output = output.Replace("50", "五十");
            output = output.Replace("49", "四十九");
            output = output.Replace("48", "四十八");
            output = output.Replace("47", "四十七");
            output = output.Replace("46", "四十六");
            output = output.Replace("45", "四十五");
            output = output.Replace("44", "四十四");
            output = output.Replace("43", "四十三");
            output = output.Replace("42", "四十二");
            output = output.Replace("41", "四十一");
            output = output.Replace("40", "四十");
            output = output.Replace("39", "三十九");
            output = output.Replace("38", "三十八");
            output = output.Replace("37", "三十七");
            output = output.Replace("36", "三十六");
            output = output.Replace("35", "三十五");
            output = output.Replace("34", "三十四");
            output = output.Replace("33", "三十三");
            output = output.Replace("32", "三十二");
            output = output.Replace("31", "三十一");
            output = output.Replace("30", "三十");
            output = output.Replace("29", "二十九");
            output = output.Replace("28", "二十八");
            output = output.Replace("27", "二十七");
            output = output.Replace("26", "二十六");
            output = output.Replace("25", "二十五");
            output = output.Replace("24", "二十四");
            output = output.Replace("23", "二十三");
            output = output.Replace("22", "二十二");
            output = output.Replace("21", "二十一");
            output = output.Replace("20", "二十");
            output = output.Replace("19", "十九");
            output = output.Replace("18", "十八");
            output = output.Replace("17", "十七");
            output = output.Replace("16", "十六");
            output = output.Replace("15", "十五");
            output = output.Replace("14", "十四");
            output = output.Replace("13", "十三");
            output = output.Replace("12", "十二");
            output = output.Replace("11", "十一");
            output = output.Replace("10", "十");
            output = output.Replace("9", "九");
            output = output.Replace("8", "八");
            output = output.Replace("7", "七");
            output = output.Replace("6", "六");
            output = output.Replace("5", "五");
            output = output.Replace("4", "四");
            output = output.Replace("3", "三");
            output = output.Replace("2", "二");
            output = output.Replace("1", "一");

            output = output.Replace("台灣", "臺灣");
            output = output.Replace("新臺", "新台");
            output = output.Replace("政北", "市政北");
            output = output.Replace("市市政北", "市政北");
            output = output.Replace("縣市政北", "縣政北");
            output = output.Replace("頂社路", "頂社");
            output = output.Replace("尾社莊路", "尾社莊");
            output = output.Replace("永定村", "永定路");
            output = output.Replace("南臺街", "南台街");
            output = output.Replace("盬", "鹽");
            output = output.Replace("后莊", "后庄");
            output = output.Replace("頭莊", "頭庄");

            return output;
        }

        private string TransSection(string section)
        {
            string output = section;

            output = output.Replace("ㄧ", "１");
            output = output.Replace("一", "１");
            output = output.Replace("二", "２");
            output = output.Replace("三", "３");
            output = output.Replace("四", "４");
            output = output.Replace("五", "５");
            output = output.Replace("六", "６");
            output = output.Replace("七", "７");
            output = output.Replace("八", "８");
            output = output.Replace("九", "９");
            output = output.Replace("1", "１");
            output = output.Replace("2", "２");
            output = output.Replace("3", "３");
            output = output.Replace("4", "４");
            output = output.Replace("5", "５");
            output = output.Replace("6", "６");
            output = output.Replace("7", "７");
            output = output.Replace("8", "８");
            output = output.Replace("9", "９");

            return output;
        }

        private string TransLine(string inputString)
        {
            string output = inputString;

            output = output.Replace('之', '-');
            output = output.Replace('一', '1');
            output = output.Replace('二', '2');
            output = output.Replace('三', '3');
            output = output.Replace('四', '4');
            output = output.Replace('五', '5');
            output = output.Replace('六', '6');
            output = output.Replace('七', '7');
            output = output.Replace('八', '8');
            output = output.Replace('九', '9');
            output = output.Replace("十", "10");
            output = output.Replace("巷", "");

            return output;
        }

        private string TransNoToFind(string inputString)
        {
            string output = inputString;

            if (output.IndexOf('-') > 0)
            {
                output = output.Substring(0, output.IndexOf('-'));
            }

            return output;
        }

        private string TransNo(string inputString)
        {
            string output = inputString;

            try
            {
                output = Strings.StrConv(inputString, VbStrConv.Narrow, 0);
            }
            catch
            {
                //dummy
            }

            output = output.Replace('之', '-');
            output = output.Replace('一', '1');
            output = output.Replace('二', '2');
            output = output.Replace('三', '3');
            output = output.Replace('四', '4');
            output = output.Replace('五', '5');
            output = output.Replace('六', '6');
            output = output.Replace('七', '7');
            output = output.Replace('八', '8');
            output = output.Replace('九', '9');
            output = output.Replace("十", "10");

            return output.Trim();
        }

        private string GetTailNo(string inputString)
        {
            string output = string.Empty;

            string doString = inputString;

            List<char> putData = new List<char>();

            if (inputString.IndexOf('(') > 0)
            {
                doString = inputString.Substring(0, inputString.IndexOf('('));
            }

            char[] charArrayAddress = doString.ToCharArray();

            int index = doString.Length;

            for (int i = 0; i < index; i++)
            {
                if (allowChar.Contains(charArrayAddress[index - i - 1]))
                {
                    putData.Insert(0, charArrayAddress[index - i - 1]);
                }
                else
                {
                    output = new string(putData.ToArray());
                    break;
                }
            }

            if (output.Length == 0 && putData.Count > 0)
            {
                output = new string(putData.ToArray());
            }

            return output;
        }

        private string GetNo(string address)
        {
            char[] doChar = { '号', '號' };

            char[] charArrayAddress = address.ToCharArray();

            string output = string.Empty;

            List<char> putData = new List<char>();

            foreach (char eachChar in doChar)
            {
                if (address.IndexOf(eachChar) > 0)
                {
                    int startIndex = address.IndexOf(eachChar);

                    for (int i = 0; i < startIndex; i++)
                    {
                        if (allowChar.Contains(charArrayAddress[startIndex - i - 1]))
                        {
                            putData.Insert(0, charArrayAddress[startIndex - i - 1]);
                        }
                        else
                        {
                            output = new string(putData.ToArray());
                            break;
                        }

                    }
                }

            }


            return output;
        }

        private bool HandleSpecialRoad(string orgData, ref string realRoad)
        {
            bool output = false;

            foreach (string eachRoad in this.specialRoad)
            {
                if (orgData.IndexOf(eachRoad) > 0)
                {
                    realRoad = eachRoad;
                    output = true;
                    break;
                }
            }

            return output;
        }

        private string IntFomalAddress(string orgData)
        {
            string realAddress = ChineseConverter.Convert(orgData, ChineseConversionDirection.SimplifiedToTraditional);

            //修正南庄鄉
            realAddress = realAddress.Replace("南莊鄉", "南庄鄉");

            //處理新市

            if (realAddress.Contains("南市新市區"))
            {
                realAddress = realAddress.Replace("新市區", "新市區");
            }
            else
            {
                realAddress = realAddress.Replace("新市", "∞∞");
            }

            return realAddress;
        }

        private string CutString(string orgString, string[] strArray, ref string newString)
        {
            string output = string.Empty;
            newString = orgString;

            foreach (string each in strArray)
            {
                if (newString.IndexOf(each) > 0)
                {
                    string strFind = newString.Substring(0, newString.IndexOf(each) + each.Length);

                    if ((strFind.Length - each.Length) > 0 && strFind.Length < 8)
                    {
                        newString = newString.Substring(newString.IndexOf(each) + each.Length);

                        output = strFind;
                        break;

                    }
                }
            }

            return output;
        }
    }
}
