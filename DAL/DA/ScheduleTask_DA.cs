﻿using Common;
using DAL.Context;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Common.Setting.JobSetting;

namespace DAL.DA
{
    public class ScheduleTask_DA : IScheduleTask_DA
    {
        private readonly DBconn _dbconn;

        public ScheduleTask_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<IEnumerable<ScheduleTask_Condition>> GetInfoByTaskID(TaskCodeEnum TaskID)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[ScheduleTask]  WITH (NOLOCK) WHERE TaskID = @TaskID AND Enable = 1
                    ";
                var result = await _conn.QueryAsync<ScheduleTask_Condition>(sql, new { TaskID });
                return result;
            }
        }
        public async Task<ScheduleTask_Condition> GetInfoById(string id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[ScheduleTask]  WITH (NOLOCK) WHERE id = @id
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<ScheduleTask_Condition>(sql, new { id });
                return result;
            }
        }
        public async Task Add(ScheduleTask_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[ScheduleTask] VALUES ( @TaskID,@TaskName, @StartTime, @Enable, @CreateDate)
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
        public async Task UpdateSetStartDate(ScheduleTask_Condition data)
        {
            data.UpdateDate = DateTime.Now;

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                //將此處dattime mapping到datetime2
                SqlMapper.AddTypeMap(typeof(DateTime), System.Data.DbType.DateTime2);
                string sql =
                    @"  
Update [dbo].[ScheduleTask]
SET StartTime =@StartTime,UpdateDate=@UpdateDate
WHERE id = @id
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
        public async Task UpdateSetStartId(ScheduleTask_Condition data)
        {
            data.UpdateDate = DateTime.Now;
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {

                string sql =
                    @"  
Update [dbo].[ScheduleTask]
SET StartId =@StartId,UpdateDate=@UpdateDate
WHERE id = @id
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }
    public interface IScheduleTask_DA
    {
        Task Add(ScheduleTask_Condition data);
        Task UpdateSetStartDate(ScheduleTask_Condition data);
        Task UpdateSetStartId(ScheduleTask_Condition data);
        //Task Update(tcDeliveryRequests_Condition data);
        //Task Remove(tcDeliveryRequests_Condition data);
        Task<IEnumerable<ScheduleTask_Condition>> GetInfoByTaskID(TaskCodeEnum TaskID);
        Task<ScheduleTask_Condition> GetInfoById(string id);
        // Task<IEnumerable<tcDeliveryRequests_Condition>> All();
    }
}
