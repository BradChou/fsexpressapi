﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class tbAccounts_DA : ItbAccounts_DA
    {
        private readonly DBconn _dbconn;

        public tbAccounts_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<tbAccounts_Condition> GetAccountsInfo(string accountCode, string passWord)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tbAccounts]  WITH (NOLOCK) WHERE account_code = @accountCode and password = @passWord and active_flag = 1
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<tbAccounts_Condition>(sql, new { accountCode, passWord });
                return result;
            }
        }
    }
    public interface ItbAccounts_DA
    {
        Task<tbAccounts_Condition> GetAccountsInfo(string accountCode, string passWord);
    }


}
