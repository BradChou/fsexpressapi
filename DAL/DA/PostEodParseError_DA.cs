﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostEodParseError_DA : IPostEodParseError_DA
    {
        private readonly DBconn _dbconn;

        public PostEodParseError_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(PostEodParseError_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostEodParseError] 
                    (
                     [PostEodParseId]
                    ,[PostId]
                    ,[CheckNumber]
                    ,[PostStation]
                    ,[StatusDate]
                    ,[StatusCode]
                    ,[StatusZH]
,[ErrorStatus]
,[ErrorMessage]
,[Count]

                    ) VALUES
                    (   
                    @PostEodParseId,
                    @PostId,
                    @CheckNumber,
                    @PostStation,
                    @StatusDate,
                    @StatusCode,
                    @StatusZH,
@ErrorStatus,
@ErrorMessage,
@Count
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }
    public interface IPostEodParseError_DA
    {
        Task Insert(PostEodParseError_Condition data);

        //Task<PostEodParseError_Condition> SearchInfo(PostEodParseFilter_Condition data);
        //Task<IEnumerable<PostEodParseError_Condition>> GetInfoById(long id);

        //Task<IEnumerable<PostEodParseError_Condition>> GetInfoByPostNo(string PostNo);
        // Task<IEnumerable<PostEodParseError_Condition>> GetInfoByCdate(DateTime CreateTime);
    }
}
