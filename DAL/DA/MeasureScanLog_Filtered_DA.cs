﻿using Common;
using System;
using System.Collections.Generic;
using DAL.Model.Condition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DAL.DA
{
    public class MeasureScanLog_Filtered_DA : IMeasureScanLog_Filtered_DA
    {
        private readonly DBconn _dbconn;

        public MeasureScanLog_Filtered_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Insert(MeasureScanLog_Filtered_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[MeasureScanLog_Filtered]
                        (
                            [MeasureScanLogId]
                           ,[DataSource]
                           ,[Scancode]
                           ,[Length]
                           ,[Width]
                           ,[Height]
                           ,[Size]
                           ,[Status]
                           ,[ScanTime]
                           ,[Picture]
                           ,[cdate]
                        )
                        VALUES
                       (
                            @MeasureScanLogId
                           ,@DataSource
                           ,@Scancode
                           ,@Length
                           ,@Width
                           ,@Height
                           ,@Size
                           ,@Status
                           ,@ScanTime
                           ,@Picture
                           ,@cdate
                        )
                    ";
                var result = await _conn.QueryAsync<MeasureScanLog_Filtered_Condition>(sql, data);
                return;
            }
        }
        public async Task<MeasureScanLog_Filtered_Condition> GetInfoById(string id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MeasureScanLog_Filtered]  WITH (NOLOCK) WHERE id = @id
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<MeasureScanLog_Filtered_Condition>(sql, new { id });
                return result;
            }
        }
        public async Task<IEnumerable<MeasureScanLog_Filtered_Condition>> GetInfoByDatetime(DateTime cdateS, DateTime cdateE)
        {
            //將此處dattime mapping到datetime2
            SqlMapper.AddTypeMap(typeof(DateTime), System.Data.DbType.DateTime2);
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MeasureScanLog_Filtered]  WITH (NOLOCK) WHERE cdate > @cdateS AND cdate <= @cdateE
                    ";
                var result = await _conn.QueryAsync<MeasureScanLog_Filtered_Condition>(sql, new { cdateS, cdateE });
                return result;
            }
        }
    }
    public interface IMeasureScanLog_Filtered_DA
    { 
        Task Insert(MeasureScanLog_Filtered_Condition data);
        //Task Update(tcDeliveryRequests_Condition data);
        //Task Remove(tcDeliveryRequests_Condition data);
        Task<IEnumerable<MeasureScanLog_Filtered_Condition>> GetInfoByDatetime(DateTime cdateS, DateTime cdateE);
        Task<MeasureScanLog_Filtered_Condition> GetInfoById(string id);
        // Task<IEnumerable<tcDeliveryRequests_Condition>> All();
    }
}
