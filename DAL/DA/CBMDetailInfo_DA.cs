﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class CBMDetailInfo_DA : ICBMDetailInfo_DA
    {
        private readonly DBconn _dbconn;


        public CBMDetailInfo_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<CBMDetailInfo_Condition> GetInfoByCheckNumber(string CheckNumber)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[CBMDetailInfo]   WHERE CheckNumber = @CheckNumber 
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<CBMDetailInfo_Condition>(sql, new { CheckNumber });
                return result;
            }
        }

        public async Task Insert(CBMDetailInfo_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
INSERT INTO [dbo].[CBMDetailInfo]
           (
		[CheckNumber]
           ,[Status]
         
		   )
     VALUES
           (
		   @CheckNumber
           ,@Status
           
		   )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }

        }


        public async Task Update(CBMDetailInfo_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {

                string sql =
                    @"  
Update [dbo].[CBMDetailInfo]
SET 
Status =@Status,
UpdateDate=@UpdateDate
WHERE id = @id
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }
    public interface ICBMDetailInfo_DA
    {
        Task Update(CBMDetailInfo_Condition data);
        Task Insert(CBMDetailInfo_Condition data);

        Task<CBMDetailInfo_Condition> GetInfoByCheckNumber(string CheckNumber);


    }
}
