﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{

    public class DeliveryStatusPublic_DA : IDeliveryStatusPublic_DA
    {

        private readonly DBconn _dbconn;


        public DeliveryStatusPublic_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(DeliveryStatusPublic_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
INSERT INTO [dbo].[DeliveryStatusPublic]
           (
		   [DataSource]
           ,[CheckNumber]
           ,[DeliveryStatus]
           ,[ScanDate]
           ,[CompanyName]
           ,[TaxIDNumber]
           ,[DeliveryStatusCode]
           ,[WorkerName]
           ,[StationCode]
           ,[StationName]
         
		   )
     VALUES
           (
		    @DataSource
           ,@CheckNumber
            ,@DeliveryStatus
           ,@ScanDate
           ,@CompanyName
           ,@TaxIDNumber
           ,@DeliveryStatusCode
           ,@WorkerName
           ,@StationCode
           ,@StationName
         
		   )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }

        }
        public async Task<IEnumerable<DeliveryStatusPublic_Condition>> GetInfoByCdate(DateTime CreateDate)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[DeliveryStatusPublic]  WITH (NOLOCK) WHERE CreateDate > @CreateDate
                    ";
                var result = await _conn.QueryAsync<DeliveryStatusPublic_Condition>(sql, new { CreateDate });
                return result;
            }
        }

        public async Task<IEnumerable<DeliveryStatusPublic_Condition>> GetInfoById(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[DeliveryStatusPublic]  WITH (NOLOCK) WHERE id > @id
                    ";
                var result = await _conn.QueryAsync<DeliveryStatusPublic_Condition>(sql, new { id });
                return result;
            }
        }
    }
    public interface IDeliveryStatusPublic_DA
    {
        Task Insert(DeliveryStatusPublic_Condition data);
        Task<IEnumerable<DeliveryStatusPublic_Condition>> GetInfoByCdate(DateTime cdate);
        Task<IEnumerable<DeliveryStatusPublic_Condition>> GetInfoById(long id);
    }
}
