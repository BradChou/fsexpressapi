﻿using Common;
using DAL.Context;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class ttDeliveryScanLog_DA : IttDeliveryScanLog_DA
    {
        private readonly DBconn _dbconn;


        public ttDeliveryScanLog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Insert(ttDeliveryScanLog_Condition _condition)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[ttDeliveryScanLog]
           (
            [driver_code]
           ,[check_number]
           ,[scan_item]
           ,[scan_date]
           ,[area_arrive_code]
           ,[platform]
           ,[car_number]
           ,[sowage_rate]
           ,[area]
           ,[ship_mode]
           ,[goods_type]
           ,[pieces]
           ,[weight]
           ,[runs]
           ,[plates]
           ,[exception_option]
           ,[arrive_option]
           ,[sign_form_image]
           ,[sign_field_image]
           ,[deliveryupload]
           ,[photoupload]
           ,[cdate]
           ,[cuser]
           ,[tracking_number]
           ,[Del_status]
           ,[VoucherMoney]
           ,[CashMoney]
           ,[write_off_type]
           ,[receive_option]
           ,[send_option]
           ,[delivery_option]
           ,[option_category]
            )
     VALUES
           (
            @driver_code
           ,@check_number
           ,@scan_item
           ,@scan_date
           ,@area_arrive_code
           ,@platform
           ,@car_number
           ,@sowage_rate
           ,@area
           ,@ship_mode
           ,@goods_type
           ,@pieces
           ,@weight
           ,@runs
           ,@plates
           ,@exception_option
           ,@arrive_option
           ,@sign_form_image
           ,@sign_field_image
           ,@deliveryupload
           ,@photoupload
           ,@cdate
           ,@cuser
           ,@tracking_number
           ,@Del_status
           ,@VoucherMoney
           ,@CashMoney
           ,@write_off_type
           ,@receive_option
           ,@send_option
           ,@delivery_option
           ,@option_category
)
                    ";
                await _conn.ExecuteAsync(sql, _condition);
            }
            //  _dbconn.Dispose();
        }
        public async Task<IEnumerable<ttDeliveryScanLog_Condition>> GetInfoByCheckNumber(string check_number)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  select 
                    CASE WHEN S.station_name IS　NOT NULL THEN S.station_name ELSE 
                        CASE d.driver_code
                        WHEN 'Z010474' THEN (select top 1 station_name from tcDeliveryRequests d with(nolock) left join ttArriveSitesScattered a with(nolock) on d.send_area = a.post_area and d.send_city = a.post_city left join tbStation s with(nolock) on a.station_code = s.station_scode where check_number = @check_number)
                        ELSE S.station_name
                        END
                    END as driver_station_name ,
                    tc.DeliveryType,
                    tc.return_check_number,
                    * 
                    from ttDeliveryScanLog T WITH (NOLOCK)
                    Left Join tbDrivers D  with(nolock) on T.driver_code = D.driver_code
                    Left Join tbStation S  with(nolock) on S.station_scode = D.station
                    Left Join tcDeliveryRequests tc with(nolock) on T.check_number = tc.check_number
                    WHERE T.check_number = @check_number
                    ";
                var result = await _conn.QueryAsync<ttDeliveryScanLog_Condition>(sql, new { check_number });
                return result;
            }
        }

        public async Task<IEnumerable<ttDeliveryScanLog_Condition>> GetttDeliveryScanLogLastScanItem(string checkNumber)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @" SELECT TOP 10 *
                                       FROM [dbo].[ttDeliveryScanLog] a WITH (NOLOCK) 
                                      INNER JOIN [dbo].[tcDeliveryRequests] b WITH (NOLOCK) 
                                        on a.check_number=b.check_number
                                       WHERE a.check_number = @checkNumber
                                       and a.scan_item in (3,4)
                                       and a.arrive_option in ('3','8','49')
                                      
                    ";
                var result = await _conn.QueryAsync<ttDeliveryScanLog_Condition>(sql, new { checkNumber });
                return result;
            }
        }
    }




    public interface IttDeliveryScanLog_DA
    {
        Task Insert(ttDeliveryScanLog_Condition data);
        Task<IEnumerable<ttDeliveryScanLog_Condition>> GetInfoByCheckNumber(string check_number);

        Task<IEnumerable<ttDeliveryScanLog_Condition>> GetttDeliveryScanLogLastScanItem(string check_number);
    }
}
