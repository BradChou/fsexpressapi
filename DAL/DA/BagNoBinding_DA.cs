﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class BagNobinding_DA : IBagNobinding_DA
    {
        private readonly DBconn _dbconn;

        public BagNobinding_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }


        public async Task<BagNobinding_Condition> GetBagNo(string RequestId)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT BagNo
                        FROM [dbo].[BagNobinding] WITH (NOLOCK) 
                        WHERE RequestId = @RequestId
                        AND IsBind = 1
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<BagNobinding_Condition>(sql, new { RequestId });
                return result;
            }
        }
    }
    public interface IBagNobinding_DA
    {
        Task<BagNobinding_Condition> GetBagNo(string RequestsId);
    }
}
