﻿using Common;
using DAL.Context;
using DAL.Model.Condition;
using Dapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class ttDeliveryRequestsRecord_DA : IttDeliveryRequestsRecord_DA
    {
        private readonly DBconn _dbconn;

        public ttDeliveryRequestsRecord_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task CancelLog(string check_number, string customer_code, string request_id, string record_action)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @$"  
                        insert into ttDeliveryRequestsRecord (record_action,request_id,delete_user, customer_code, check_number, record_memo, create_date,modify_date,delete_date,record_date) 
                      values(@record_action, @request_id, 'skyeyes', @customer_code, @check_number ,'API銷單',GETDATE(),GETDATE(),GETDATE(),GETDATE())
                    ";
                await _conn.ExecuteAsync(sql, new { check_number, customer_code , request_id , record_action });
               
            }
        }

    }
}
public interface IttDeliveryRequestsRecord_DA
{
    Task CancelLog(string check_number, string customer_code, string request_id, string record_action);
}


