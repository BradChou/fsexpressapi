﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostEodParseFilter_DA : IPostEodParseFilter_DA
    {
        private readonly DBconn _dbconn;

        public PostEodParseFilter_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(PostEodParseFilter_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostEodParseFilter] 
                    (
                     [PostEodParseId]
                    ,[PostId]
                    ,[CheckNumber]
                    ,[PostStation]
                    ,[StatusDate]
                    ,[StatusCode]
                    ,[StatusZH]

                    ) VALUES
                    (   
                    @PostEodParseId,
                    @PostId,
                    @CheckNumber,
                    @PostStation,
                    @StatusDate,
                    @StatusCode,
                    @StatusZH
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }

        public async Task<PostEodParseFilter_Condition> SearchInfo(PostEodParseFilter_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostEodParseFilter] 
                        WHERE PostId = @PostId AND StatusDate =@StatusDate AND StatusCode = @StatusCode
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<PostEodParseFilter_Condition>(sql, data);
                return result;
            }
        }
        public async Task<IEnumerable<PostEodParseFilter_Condition>> GetInfoById(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostEodParseFilter]  WHERE id > @id
                    ";
                var result = await _conn.QueryAsync<PostEodParseFilter_Condition>(sql, new { id });
                return result;
            }
        }
        public async Task<IEnumerable<PostEodParseFilter_Condition>> GetInfoByPostNo(string PostId)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostEodParseFilter]  WHERE PostId = @PostId
                    ";
                var result = await _conn.QueryAsync<PostEodParseFilter_Condition>(sql, new { PostId });
                return result;
            }
        }
    }
    public interface IPostEodParseFilter_DA
    {
        Task Insert(PostEodParseFilter_Condition data);

        Task<PostEodParseFilter_Condition> SearchInfo(PostEodParseFilter_Condition data);
        Task<IEnumerable<PostEodParseFilter_Condition>> GetInfoById(long id);

        Task<IEnumerable<PostEodParseFilter_Condition>> GetInfoByPostNo(string PostNo);
        // Task<IEnumerable<PostXmlParse_Condition>> GetInfoByCdate(DateTime CreateTime);
    }
}
