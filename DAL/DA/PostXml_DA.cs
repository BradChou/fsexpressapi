﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostXml_DA : IPostXml_DA
    {
        private readonly DBconn _dbconn;

        public PostXml_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<IEnumerable<PostXml_Condition>> GetErrorListById(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostXml]  WITH (NOLOCK) WHERE id > @id AND Fail = 1
                    ";
                var result = await _conn.QueryAsync<PostXml_Condition>(sql, new { id });
                return result;
            }
        }

        public async Task<IEnumerable<PostXml_Condition>> GetInfoByCdate(DateTime CreateTime)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostXml]  WITH (NOLOCK) WHERE CreateTime > @CreateTime
                    ";
                var result = await _conn.QueryAsync<PostXml_Condition>(sql, new { CreateTime });
                return result;
            }
        }
        public async Task<IEnumerable<PostXml_Condition>> GetInfoByCdateAndName(DateTime CreateTime,string XmlName)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostXml]  WITH (NOLOCK) WHERE CreateTime > @CreateTime AND XmlName = @XmlName
                    ";
                var result = await _conn.QueryAsync<PostXml_Condition>(sql, new { CreateTime, XmlName });
                return result;
            }
        }

        public async Task<IEnumerable<PostXml_Condition>> GetInfoById(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostXml]  WITH (NOLOCK) WHERE id > @id AND Fail = 0
                    ";
                var result = await _conn.QueryAsync<PostXml_Condition>(sql, new { id });
                return result;
            }
        }

        public async Task Insert(PostXml_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostXml] 
                    (
[FTPPath],
                    [XmlName],
                    [XmlPath],
                    [XmlContent],
[FileCreateTime],
                    [Fail],
                    [FailMessage],
[Number]
                    ) VALUES
                    (   
@FTPPath,
                    @XmlName,
                    @XmlPath,
                    @XmlContent,
@FileCreateTime,
                    @Fail,
                    @FailMessage,
@Number
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }

        public async Task UpdateError(PostXml_Condition data)
        {
           
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {

                string sql =
                    @"  
Update [dbo].[PostXml]
SET 
XmlContent =@XmlContent,
Fail =@Fail,
FailMessage=@FailMessage,
Number=@Number
WHERE id = @id
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }

    public interface IPostXml_DA
    {
        Task Insert(PostXml_Condition data);
        Task<IEnumerable<PostXml_Condition>> GetInfoByCdate(DateTime CreateTime);
         Task<IEnumerable<PostXml_Condition>> GetInfoByCdateAndName(DateTime CreateTime,string XmlName);

        
        Task<IEnumerable<PostXml_Condition>> GetInfoById(long Id);

        Task<IEnumerable<PostXml_Condition>> GetErrorListById(long Id);

        //   Task UpdateError(PostXml_Condition data);
    }
}
