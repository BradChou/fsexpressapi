﻿using BLL.Model.FSExpressAPI.Res.PostOffice;
using BLL.Model.ScheduleAPI.Res.PostOffice;
using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostTransmission_DA : IPostTransmission_DA
    {
        private readonly DBconn _dbconn;

        public PostTransmission_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }



        public async Task<List<PostTransmission_Condition>> GetTransferPostInfo(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  


SELECT id AS id_forPost, 
P.PostId,
P.check_number, 
case 
when DR.collection_money  is null then   'N'
when DR.collection_money = 0 then   'N'
else 'Y'
end as 'is_collection_money',
isnull(DR.collection_money,0) 'collection_money',
DR.receive_contact as receive_name,
DR.receive_tel1 as receive_tel,
P.zip3,
DR.receive_city+ DR.receive_area+DR.receive_address 'receive_address',
P.weight,
DR.request_id
FROM [dbo].[Post] AS P
LEFT JOIN [dbo].[tcDeliveryRequests] AS DR ON P.request_id=DR.request_id

where P.id > @id
and IsDel=0
and Zip3 is not null

                    ";
                var result = (await _conn.QueryAsync<PostTransmission_Condition>(sql,new { id })).ToList();
                return result;
            }
        }


        public async Task Insert(List<PostTransmission_Condition> list)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql = @"INSERT INTO [postTransmission](
id_forPost,
PostId ,
check_number ,
post_code ,
is_collection_money ,
collection_money ,
receive_name ,
receive_tel ,
zip3 ,
receive_address ,
upload_date ,
bag_no ,
bag_weight ,
serial_no ,
weight ,
container_no 

) VALUES (
@id_forPost,
@PostId ,
@check_number ,
@post_code ,
@is_collection_money ,
@collection_money ,
@receive_name ,
@receive_tel ,
@zip3 ,
@receive_address ,
@upload_date ,
@bag_no ,
@bag_weight ,
@serial_no ,
@weight ,
@container_no 
)";
                _conn.Execute(sql, list);

            }

        }
        public async Task Insert(PostTransmission_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql = @"INSERT INTO [postTransmission](
id_forPost,
PostId ,
check_number ,
post_code ,
is_collection_money ,
collection_money ,
receive_name ,
receive_tel ,
zip3 ,
receive_address ,
upload_date ,
bag_no ,
bag_weight ,
serial_no ,
weight ,
container_no 

) VALUES (
@id_forPost,
@PostId ,
@check_number ,
@post_code ,
@is_collection_money ,
@collection_money ,
@receive_name ,
@receive_tel ,
@zip3 ,
@receive_address ,
@upload_date ,
@bag_no ,
@bag_weight ,
@serial_no ,
@weight ,
@container_no 
)";
                _conn.Execute(sql, data);

            }

        }
    }
    public interface IPostTransmission_DA
    {

        Task<List<PostTransmission_Condition>> GetTransferPostInfo(long id);
        Task Insert(List<PostTransmission_Condition> list);

        Task Insert(PostTransmission_Condition data);
    }




}
