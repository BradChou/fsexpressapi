﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using DAL.Model.Condition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DAL.DA
{
    public class CBMDetail_DA : ICBMDetail_DA
    {
        private readonly DBconn _dbconn;

        public async Task<IEnumerable<CBMDetail_Condition>> GetCBMInfo()
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
Declare @now datetime
Declare @sdate datetime 
Declare @edate datetime 


Set @now = format( getdate() , 'yyyyMMdd')
Set @edate = dateadd( day , 1 , @now )
Set @sdate = dateadd( month , -1 , @edate )


                    Select * 
                    from CBMDetail with(nolock)   
                    WHERE CheckNumber in (
										Select distinct check_number From ttDeliveryScanLog with(nolock)
										Where cdate >= '2022/11/01' 
										and cdate < '2022/12/01'    
										)    
           
                    ";
                var result = await _conn.QueryAsync<CBMDetail_Condition>(sql);
                return result;
            }

        }

        public async Task<IEnumerable<CBMDetail_Condition>> GetCBMInfo_2()
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
Declare @now datetime
Declare @sdate datetime 
Declare @edate datetime 

Set @now = format( getdate() , 'yyyyMMdd')
Set @edate = dateadd( day , 1 , @now )
Set @sdate = dateadd( month , -2 , @edate )

                    Select * 
                    from CBMDetail with(nolock)   
                    WHERE CheckNumber in (
										Select distinct check_number From ttDeliveryScanLog with(nolock)
										Where cdate >= @sdate 
										and cdate < @edate    
										)    
           
                    ";
                var result = await _conn.QueryAsync<CBMDetail_Condition>(sql);
                return result;
            }

        }       

        public CBMDetail_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(CBMDetail_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
INSERT INTO [dbo].[CBMDetail]
           (
	        [CBMDetailLogId]
           ,[ComeFrom]
           ,[CheckNumber]
           ,[CBM]
           ,[Length]
           ,[Width]
           ,[Height]
		   )
     VALUES
           (
		    @CBMDetailLogId
           ,@ComeFrom
           ,@CheckNumber
           ,@CBM
           ,@Length
           ,@Width
           ,@Height
		   )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }

        }


        public async Task DELETE(string CheckNumber)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {

                string sql =
                    @"  
DELETE FROM [dbo].[CBMDetail]
WHERE CheckNumber = @CheckNumber
                    ";
                await _conn.ExecuteAsync(sql, new { CheckNumber });

            }
        }

        public async Task<IEnumerable<CBMDetail_Condition>> GetInfoByCheckNumber(string CheckNumber)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[CBMDetail]  WHERE CheckNumber = @CheckNumber
                    ";
                var result = await _conn.QueryAsync<CBMDetail_Condition>(sql, new { CheckNumber });
                return result;
            }
        }
    }
    public interface ICBMDetail_DA
    {
        Task DELETE(string CheckNumber);
        Task Insert(CBMDetail_Condition data);

        Task<IEnumerable<CBMDetail_Condition>> GetInfoByCheckNumber(string CheckNumber);
        public Task<IEnumerable<CBMDetail_Condition>> GetCBMInfo();

        public Task<IEnumerable<CBMDetail_Condition>> GetCBMInfo_2();

    }
}
