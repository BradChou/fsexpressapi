﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostXmlParseError_DA : IPostXmlParseError_DA
    {
        private readonly DBconn _dbconn;

        public PostXmlParseError_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Insert(PostXmlParseError_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostXmlParseError] 
                    (
                    [PostXmlParseId],
                    [PostNo],
                    [PostStateNo],
                    [PostStateZH],
                    [ProcessDateTime],
                    [PostXml_id],
                    [Status],
                    [ErrMsg],
                    [Number]
                    ) VALUES
                    (   
                    @PostXmlParseId,
                    @PostNo,
                    @PostStateNo,
                    @PostStateZH,
                    @ProcessDateTime,
                    @PostXml_id,
                    @Status,
                    @ErrMsg,
                    @Number
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }
    public interface IPostXmlParseError_DA
    {
        Task Insert(PostXmlParseError_Condition data);
       // Task<IEnumerable<PostXmlParse_Condition>> GetInfoByCdate(DateTime CreateTime);
    }
}

