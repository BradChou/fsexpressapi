﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostFTPTransportLog_DA : IPostFTPTransportLog_DA
    {
        private readonly DBconn _dbconn;

        public PostFTPTransportLog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(PostFTPTransportLog_Condition data)
        {

            if (data==null) 
            {
            
            }
            if (string.IsNullOrEmpty(data.Name))
            { 
            
            }


            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostFTPTransportLog]
                    (

                 [Type]
           ,[Name]
           ,[FTPPath]
           ,[NASPath]

           ,[FailMessage]
                    ) VALUES
                    (   

                   @Type
                  ,@Name
                  ,@FTPPath
                  ,@NASPath
               
                  ,@FailMessage
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }
    public interface IPostFTPTransportLog_DA
    {
        Task Insert(PostFTPTransportLog_Condition data);
      
    }
}
