﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class tbItemCodes_DA : ItbItemCodes_DA
    {
        private readonly DBconn _dbconn;

        public tbItemCodes_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<IEnumerable<tbItemCodes_Condition>> GetInfosBycode_sclass(string code_sclass)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tbItemCodes]  WITH (NOLOCK)
                        WHERE code_sclass = @code_sclass 
                    ";
                var result = await _conn.QueryAsync<tbItemCodes_Condition>(sql,new { code_sclass });
                return result;
            }

        }
    }
    public interface ItbItemCodes_DA
    {
        Task<IEnumerable<tbItemCodes_Condition>> GetInfosBycode_sclass(string code_sclass);
    }


}
