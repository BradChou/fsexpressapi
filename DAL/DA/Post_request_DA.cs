﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class Post_request_DA : IPost_request_DA
    {
        private readonly DBconn _dbconn;

        public Post_request_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<Post_request_Condition> GetInfoByPostid(string Postid)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                        SELECT *
                        FROM [dbo].[Post_request]  WITH (NOLOCK)
                        WHERE [post_number]+'30022118'+[zipcode5]+[check_code] = @Postid 
                        ORDER BY [request_id] desc
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<Post_request_Condition>(sql, new { Postid });
                return result;
            }
        }
    }
    public interface IPost_request_DA
    {
        Task<Post_request_Condition> GetInfoByPostid(string Postid);

    }
}
