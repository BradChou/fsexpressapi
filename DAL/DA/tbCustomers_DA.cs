﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class tbCustomers_DA : ItbCustomers_DA
    {
        private readonly DBconn _dbconn;

        public tbCustomers_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<tbCustomers_Condition> GetInfoByCustomerCode(string customer_code)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tbCustomers] WITH (NOLOCK) WHERE customer_code =@customer_code AND stop_shipping_code=0
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<tbCustomers_Condition>(sql, new { customer_code });
                return result;
            }
        }

        public async Task<tbCustomers_Condition> GetInfo(string customer_code)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tbCustomers] WITH (NOLOCK) WHERE customer_code =@customer_code 
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<tbCustomers_Condition>(sql, new { customer_code });
                return result;
            }
        }

        public async Task Update(tbCustomers_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {

                string sql =
                    @"  
Update [dbo].[tbCustomers]
SET 
MasterCustomerCode =@MasterCustomerCode,
MasterCustomerName=@MasterCustomerName
WHERE customer_code = @customer_code
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
        public async Task<List<tbCustomers_Condition>> GetAll()
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tbCustomers] WITH (NOLOCK)
                    ";
                var result = (await _conn.QueryAsync<tbCustomers_Condition>(sql)).ToList();
                return result;
            }
        }

        public async Task<IEnumerable<tbCustomers_Condition>> GetInfoAllCustomer()
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tbCustomers] WITH (NOLOCK) 
                    ";
                var result = await _conn.QueryAsync<tbCustomers_Condition>(sql);
                return result;
            }
        }

    }
    public interface ItbCustomers_DA
    {
        Task<List<tbCustomers_Condition>> GetAll();
        Task<tbCustomers_Condition> GetInfoByCustomerCode(string customer_code);

        Task<tbCustomers_Condition> GetInfo(string customer_code);

        Task Update(tbCustomers_Condition data);
       
        Task<IEnumerable<tbCustomers_Condition>> GetInfoAllCustomer();
    }
}
