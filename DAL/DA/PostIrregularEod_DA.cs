﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
   public class PostIrregularEod_DA : IPostIrregularEod_DA
    {
        private readonly DBconn _dbconn;

        public PostIrregularEod_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(PostIrregularEod_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostIrregularEod] 
                    (
                 [EodName]
      ,[EodPath]
      ,[PostId]
      ,[check_number]
      ,[post_code]
      ,[is_collection_money]
      ,[collection_money]
      ,[receive_name]
      ,[receive_tel]
      ,[zip3]
      ,[receive_address]
      ,[upload_date]
      ,[bag_no]
      ,[bag_weight]
      ,[serial_no]
      ,[weight]
      ,[container_no]
      ,[StatusCode]

                    ) VALUES
                    (   
       @EodName
      ,@EodPath
      ,@PostId
      ,@check_number
      ,@post_code
      ,@is_collection_money
      ,@collection_money
      ,@receive_name
      ,@receive_tel
      ,@zip3
      ,@receive_address
      ,@upload_date
      ,@bag_no
      ,@bag_weight
      ,@serial_no
      ,@weight
      ,@container_no
      ,@StatusCode
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
        //public async Task<IEnumerable<PostIrregularEod_Condition>> GetInfoById(long id)
        //{
        //    using (var _conn = _dbconn.CreateJunFuRealConnection())
        //    {
        //        string sql =
        //            @"  SELECT *
        //                FROM [dbo].[PostIrregularEod] WHERE id > @id
        //            ";
        //        var result = await _conn.QueryAsync<PostIrregularEod_Condition>(sql, new { id });
        //        return result;
        //    }
        //}
    }
    public interface IPostIrregularEod_DA
    {
        Task Insert(PostIrregularEod_Condition data);

        //Task<PostIrregularEod_Condition> SearchInfo(PostIrregularEod_Condition data);
        // Task<IEnumerable<PostIrregularEod_Condition>> GetInfoById(long id);

        //Task<IEnumerable<PostIrregularEodFilter_Condition>> GetInfoByPostNo(string PostNo);
        // Task<IEnumerable<PostXmlParse_Condition>> GetInfoByCdate(DateTime CreateTime);
    }
}
