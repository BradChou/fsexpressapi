﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class pickup_request_for_apiuser_DA : Ipickup_request_for_apiuser_DA
    {
        private readonly DBconn _dbconn;

        public pickup_request_for_apiuser_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Insert(pickup_request_for_apiuser_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                 INSERT INTO [dbo].[pickup_request_for_apiuser]
           ([check_number]
           ,[customer_code]
           ,[pieces]
           ,[request_date]
           ,[send_city]
           ,[send_area]
           ,[send_road]
           ,[supplier_code]
,[ShuttleStationCode]
           ,[send_tel]
           ,[sd]
           ,[md]
           ,[putorder]
)
     VALUES
           (
@check_number
           ,@customer_code
           ,@pieces
           ,@request_date
           ,@send_city
           ,@send_area
           ,@send_road
           ,@supplier_code
,@ShuttleStationCode
           ,@send_tel
           ,@sd
           ,@md
           ,@putorder

)
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }
    public interface Ipickup_request_for_apiuser_DA
    {
        Task Insert(pickup_request_for_apiuser_Condition data);
    }
}

