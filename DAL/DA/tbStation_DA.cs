﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class tbStation_DA: ItbStation_DA
    {
        private readonly DBconn _dbconn;

        public tbStation_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<List<tbStation_Condition>> GetAll()
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tbStation] WITH (NOLOCK)
                    ";
                var result = (await _conn.QueryAsync<tbStation_Condition>(sql)).ToList();
                return result;
            }
        }
        public async Task<tbStation_Condition> GetInfoByStationCode(string station_scode)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tbStation] WITH (NOLOCK) WHERE station_scode =@station_scode
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<tbStation_Condition>(sql, new { station_scode });
                return result;
            }
        }
    }
    public interface ItbStation_DA
    {
        Task<List<tbStation_Condition>> GetAll();
        Task<tbStation_Condition> GetInfoByStationCode(string id);
    }
}
