﻿using Common;
using System;
using System.Collections.Generic;
using DAL.Model.Condition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DAL.DA
{
    public class MeasureScanLog_DA : IMeasureScanLog_DA
    {
        private readonly DBconn _dbconn;

        public MeasureScanLog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<IEnumerable<MeasureScanLog_Condition>> GetInfoByDatetime(DateTime cdateS, DateTime cdateE)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                //將此處dattime mapping到datetime2
                SqlMapper.AddTypeMap(typeof(DateTime), System.Data.DbType.DateTime2);
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MeasureScanLog]  WITH (NOLOCK) WHERE cdate > @cdateS AND cdate <= @cdateE
                    ";
                var result = await _conn.QueryAsync<MeasureScanLog_Condition>(sql, new { cdateS, cdateE });
                return result;
            }
        }
        public async Task<MeasureScanLog_Condition> GetInfoById(string id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MeasureScanLog]  WITH (NOLOCK) WHERE id = @id
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<MeasureScanLog_Condition>(sql, new { id });
                return result;
            }
        }
        public async Task<IEnumerable<MeasureScanLog_Condition>> GetInfoByCheckNumber(string checkNumber)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MeasureScanLog]  WITH (NOLOCK) WHERE Scancode = @checkNumber
                    ";
                var result = await _conn.QueryAsync<MeasureScanLog_Condition>(sql, new { checkNumber });
                return result;
            }
        }
        public async Task<IEnumerable<MeasureScanLog_Condition>> GetOKInfoByCheckNumber(string checkNumber)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MeasureScanLog]  WITH (NOLOCK) WHERE Scancode = @checkNumber AND Status = 'OK'
                    ";
                var result = await _conn.QueryAsync<MeasureScanLog_Condition>(sql, new { checkNumber });
                return result;
            }
        }
        public async Task Add(MeasureScanLog_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql = @"INSERT INTO [MeasureScanLog] (
	DataSource
    ,Scancode
	,Length
	,Width
	,Height
	,Size
	,STATUS
	,ScanTime
	,Picture
	
	)
VALUES (
	@DataSource
    ,@Scancode
	,@Length
	,@Width
	,@Height
	,@Size
	,@STATUS
	,@ScanTime
	,@Picture
	
	)";
                await _conn.ExecuteAsync(sql, data);
            }
        }
    }
    public interface IMeasureScanLog_DA
    { 
        Task Add(MeasureScanLog_Condition data);
        //Task Update(tcDeliveryRequests_Condition data);
        //Task Remove(tcDeliveryRequests_Condition data);
        Task<IEnumerable<MeasureScanLog_Condition>> GetInfoByDatetime(DateTime cdateS, DateTime cdateE);
        Task<MeasureScanLog_Condition> GetInfoById(string id);
        // Task<IEnumerable<tcDeliveryRequests_Condition>> All();
        Task<IEnumerable<MeasureScanLog_Condition>> GetInfoByCheckNumber(string checkNumber);
        Task<IEnumerable<MeasureScanLog_Condition>> GetOKInfoByCheckNumber(string checkNumber);
    }
}
