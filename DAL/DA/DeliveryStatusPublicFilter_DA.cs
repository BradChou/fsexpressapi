﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class DeliveryStatusPublicFilter_DA : IDeliveryStatusPublicFilter_DA
    {

        private readonly DBconn _dbconn;

        public DeliveryStatusPublicFilter_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<IEnumerable<DeliveryStatusPublicFilter_Condition>> GetInfoById(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[DeliveryStatusPublicFilter]  WITH (NOLOCK) WHERE id > @id
                    ";
                var result = await _conn.QueryAsync<DeliveryStatusPublicFilter_Condition>(sql, new { id });
                return result;
            }
        }

        public async Task Insert(DeliveryStatusPublicFilter_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[DeliveryStatusPublicFilter] 
                    (
                   [DeliveryStatusPublicId]
                  ,[DataSource]
                  ,[CheckNumber]
                  ,[DeliveryStatus]
                  ,[ScanDate]
                  ,[CompanyName]
                  ,[TaxIDNumber]
                  ,[DeliveryStatusCode]
                  ,[WorkerName]
                  ,[StationCode]
                  ,[StationName]

                    ) VALUES
                    (   
                  @DeliveryStatusPublicId
                 ,@DataSource
                 ,@CheckNumber
                 ,@DeliveryStatus
                 ,@ScanDate
                 ,@CompanyName
                 ,@TaxIDNumber
                 ,@DeliveryStatusCode
                 ,@WorkerName
                 ,@StationCode
                 ,@StationName

                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }

        }
        public async Task<DeliveryStatusPublicFilter_Condition> CheckInfoRepeat(DeliveryStatusPublicFilter_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[DeliveryStatusPublicFilter]  WITH (NOLOCK)
                        WHERE DataSource = @DataSource AND CheckNumber =@CheckNumber AND DeliveryStatus = @DeliveryStatus AND ScanDate = @ScanDate
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<DeliveryStatusPublicFilter_Condition>(sql, data);
                return result;
            }
        }
    }
    public interface IDeliveryStatusPublicFilter_DA
    {
        Task Insert(DeliveryStatusPublicFilter_Condition data);
        Task<IEnumerable<DeliveryStatusPublicFilter_Condition>> GetInfoById(long id);
        Task<DeliveryStatusPublicFilter_Condition> CheckInfoRepeat(DeliveryStatusPublicFilter_Condition data);
    }

}
