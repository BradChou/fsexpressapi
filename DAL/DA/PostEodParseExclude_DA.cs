﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostEodParseExclude_DA : IPostEodParseExclude_DA
    {
        private readonly DBconn _dbconn;

        public PostEodParseExclude_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(PostEodParseExclude_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostEodParseExclude] 
                    (
                     [PostEodParseId]
                    ,[PostId]
                    ,[CheckNumber]
                    ,[PostStation]
                    ,[StatusDate]
                    ,[StatusCode]
                    ,[StatusZH]

                    ) VALUES
                    (   
                    @PostEodParseId,
                    @PostId,
                    @CheckNumber,
                    @PostStation,
                    @StatusDate,
                    @StatusCode,
                    @StatusZH
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }
    public interface IPostEodParseExclude_DA
    {
        Task Insert(PostEodParseExclude_Condition data);

        //Task<PostEodParseFilter_Condition> SearchInfo(PostEodParseExclude_Condition data);
        //Task<IEnumerable<PostEodParseExclude_Condition>> GetInfoById(long id);

        //Task<IEnumerable<PostEodParseExclude_Condition>> GetInfoByPostNo(string PostNo);
        // Task<IEnumerable<PostEodParseExclude_Condition>> GetInfoByCdate(DateTime CreateTime);
    }
}
