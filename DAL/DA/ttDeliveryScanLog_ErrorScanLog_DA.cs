﻿using Common;
using DAL.Context;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class ttDeliveryScanLog_ErrorScanLog_DA : IttDeliveryScanLog_ErrorScanLog_DA
    {
        private readonly DBconn _dbconn;


        public ttDeliveryScanLog_ErrorScanLog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Insert(ttDeliveryScanLog_ErrorScanLog_Condition _condition)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[ttDeliveryScanLog_ErrorScanLog]
           (
            [driver_code]
           ,[check_number]
           ,[scan_item]
           ,[scan_date]
           ,[area_arrive_code]
           ,[platform]
           ,[car_number]
           ,[sowage_rate]
           ,[area]
           ,[ship_mode]
           ,[goods_type]
           ,[pieces]
           ,[weight]
           ,[runs]
           ,[plates]
           ,[exception_option]
           ,[arrive_option]
           ,[sign_form_image]
           ,[sign_field_image]
           ,[deliveryupload]
           ,[photoupload]
           ,[cdate]
           ,[cuser]
           ,[tracking_number]
           ,[Del_status]
           ,[VoucherMoney]
           ,[CashMoney]
           ,[write_off_type]
           ,[receive_option]
           ,[send_option]
           ,[delivery_option]
           ,[option_category]
           ,[error_msg]
            )
     VALUES
           (
            @driver_code
           ,@check_number
           ,@scan_item
           ,@scan_date
           ,@area_arrive_code
           ,@platform
           ,@car_number
           ,@sowage_rate
           ,@area
           ,@ship_mode
           ,@goods_type
           ,@pieces
           ,@weight
           ,@runs
           ,@plates
           ,@exception_option
           ,@arrive_option
           ,@sign_form_image
           ,@sign_field_image
           ,@deliveryupload
           ,@photoupload
           ,@cdate
           ,@cuser
           ,@tracking_number
           ,@Del_status
           ,@VoucherMoney
           ,@CashMoney
           ,@write_off_type
           ,@receive_option
           ,@send_option
           ,@delivery_option
           ,@option_category
           ,@error_msg
)
                    ";
                await _conn.ExecuteAsync(sql, _condition);
            }
            //  _dbconn.Dispose();
        }
        public async Task<IEnumerable<ttDeliveryScanLog_ErrorScanLog_Condition>> GetInfoByCheckNumber(string check_number)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[ttDeliveryScanLog_ErrorScanLog]  WITH (NOLOCK) WHERE check_number = @check_number
                    ";
                var result = await _conn.QueryAsync<ttDeliveryScanLog_ErrorScanLog_Condition>(sql, new { check_number });
                return result;
            }
        }
    }
    public interface IttDeliveryScanLog_ErrorScanLog_DA
    {
        Task Insert(ttDeliveryScanLog_ErrorScanLog_Condition data);
        Task<IEnumerable<ttDeliveryScanLog_ErrorScanLog_Condition>> GetInfoByCheckNumber(string check_number);
    }
}
