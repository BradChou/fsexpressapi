﻿using Common;
using System;
using System.Collections.Generic;
using DAL.Model.Condition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DAL.DA
{
    public class MeasureScanLog_ErrLog_DA : IMeasureScanLog_ErrLog_DA
    {
        private readonly DBconn _dbconn;

        public MeasureScanLog_ErrLog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Insert(MeasureScanLog_ErrLog_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[MeasureScanLog_ErrLog]
                        (
                            [MeasureScanLog_FilteredId]
                           ,[DataSource]
                           ,[Scancode]
                           ,[Length]
                           ,[Width]
                           ,[Height]
                           ,[Size]
                           ,[Status]
                           ,[ScanTime]
                           ,[Picture]
                           ,[cdate]
                           ,[ErrMsg]
                           ,[Number]
                        )
                        VALUES
                       (
                            @MeasureScanLog_FilteredId
                           ,@DataSource
                           ,@Scancode
                           ,@Length
                           ,@Width
                           ,@Height
                           ,@Size
                           ,@Status
                           ,@ScanTime
                           ,@Picture
                           ,@cdate
                           ,@ErrMsg
                           ,@Number
                        )
                    ";
                var result = await _conn.QueryAsync<MeasureScanLog_ErrLog_Condition>(sql, data);
                return;
            }
        }
        public async Task<MeasureScanLog_ErrLog_Condition> GetInfoById(string id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MeasureScanLog_ErrLog]  WITH (NOLOCK) WHERE id = @id
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<MeasureScanLog_ErrLog_Condition>(sql, new { id });
                return result;
            }
        }
        public async Task<IEnumerable<MeasureScanLog_ErrLog_Condition>> GetInfoByDatetime(DateTime cdateS, DateTime cdateE)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MeasureScanLog_ErrLog]  WITH (NOLOCK) WHERE cdate > @cdateS AND cdate <= @cdateE
                    ";
                var result = await _conn.QueryAsync<MeasureScanLog_ErrLog_Condition>(sql, new { cdateS, cdateE });
                return result;
            }
        }
    }
    public interface IMeasureScanLog_ErrLog_DA
    { 
        Task Insert(MeasureScanLog_ErrLog_Condition data);
        //Task Update(tcDeliveryRequests_Condition data);
        //Task Remove(tcDeliveryRequests_Condition data);
        Task<IEnumerable<MeasureScanLog_ErrLog_Condition>> GetInfoByDatetime(DateTime cdateS, DateTime cdateE);
        Task<MeasureScanLog_ErrLog_Condition> GetInfoById(string id);
        // Task<IEnumerable<tcDeliveryRequests_Condition>> All();
    }
}
