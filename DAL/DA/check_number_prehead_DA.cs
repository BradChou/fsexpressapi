﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class check_number_prehead_DA : Icheck_number_prehead_DA
    {
        private readonly DBconn _dbconn;

        public check_number_prehead_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }


        public async Task<check_number_prehead_Condition> GetInfoByProductType(string product_type)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[check_number_prehead] WITH (NOLOCK) WHERE product_type =@product_type 
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<check_number_prehead_Condition>(sql, new { product_type });
                return result;
            }
        }
    }
    public interface Icheck_number_prehead_DA
    {
        Task<check_number_prehead_Condition> GetInfoByProductType(string product_type);
    }
}
