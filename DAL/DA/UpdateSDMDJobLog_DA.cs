﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class UpdateSDMDJobLog_DA : IUpdateSDMDJobLog_DA
    {

        private readonly DBconn _dbconn;

        public UpdateSDMDJobLog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Insert(UpdateSDMDJobLog_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT INTO [dbo].[UpdateSDMDJobLog]
           ([tcDeliveryRequestsId]
           ,[CustomerCode]
,[Address]
           ,[ReceiveCode]
           ,[ReceiveSD]
           ,[ReceiveMD]
           ,[Map8City]
           ,[Map8Town]
           ,[Map8Name]
           )
     VALUES
           (@tcDeliveryRequestsId
           ,@CustomerCode
,@Address
           ,@ReceiveCode
           ,@ReceiveSD
           ,@ReceiveMD
           ,@Map8City
           ,@Map8Town
           ,@Map8Name
)

                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }

    }
    public interface IUpdateSDMDJobLog_DA
    {
        Task Insert(UpdateSDMDJobLog_Condition data);
    }
    }
