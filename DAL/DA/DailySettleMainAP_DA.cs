﻿using Common;
using System;
using System.Collections.Generic;
using DAL.Model.Condition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DAL.FSE01_DA;

namespace DAL.DA
{
    public class DailySettleMainAP_DA: IDailySettleMainAP_DA
    {
        private readonly DBconn _dbconn;
        private ILogRecord_DA _LogRecord;

        public DailySettleMainAP_DA(DBconn dBconn, ILogRecord_DA LogRecord)
        {
            _dbconn = dBconn;
            _LogRecord = LogRecord;
        }

        private DailySettleToDoListModel _dailySettleToDoListModel = new DailySettleToDoListModel();

        public DailySettleToDoListModel dailySettleToDoListModel
        {
            get
            {
                return _dailySettleToDoListModel;
            }
            set
            {
                _dailySettleToDoListModel = value;
            }
        }

        /// <summary>
        /// 非日結排程 新增日結資料
        ///     step1. 新增至日結主表
        ///     step2. add to todolist
        /// </summary>
        /// <param name="LimitTime"></param>
        public void doInsertDailyNewData(DateTime LimitTime)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                try
                {
                    string sql = @"DailySettle_Insert_Step0";
                    _conn.Execute(sql , new
                    {
                        LimitTime = LimitTime
                    },null,600,System.Data.CommandType.StoredProcedure);

                    _LogRecord.Save("DailySettlement", "1.doInsertDailyNewData", "");

                }
                catch(Exception ex)
                {
                    _LogRecord.Save("DailySettlement", "1.doInsertDailyNewData", ex.Message);
                }

            }
        }


        /// <summary>
        /// 日結排程 step1 取得本次要計算的日結排程的清單
        ///     step1. 從 todo 拉還沒作業的清單出來 ( 一般新增資料 + 更新資料 )
        ///     step2. (一般新增資料)  
        ///     step3. (要更新的資料) 
        /// </summary>
        /// <returns></returns>
        public void doGetCalcList(DateTime LimitTime)
        {
            _LogRecord.Save("DailySettlement", "1.doGetCalcList", "start");

            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                try
                {
                    string sql = @"DailySettle_Step1";
                    var result = _conn.Query<DailySettleSingleToDo>(sql,
                        new {
                            LimitTime = LimitTime ,
                            DataType = "new"
                        },null,true,null,System.Data.CommandType.StoredProcedure
                        );
                    dailySettleToDoListModel.NewDataList = (List<DailySettleSingleToDo>)result;
                }
                catch (Exception ex)
                {

                }


                try
                {
                    string sql2 = @"DailySettle_Step1";
                    var result2 = _conn.Query<DailySettleSingleToDo>(sql2,
                        new
                        {
                            LimitTime = LimitTime,
                            DataType = "update"
                        }, null, true, null, System.Data.CommandType.StoredProcedure
                        );
                    dailySettleToDoListModel.UpdateDataList = (List<DailySettleSingleToDo>)result2;
                }
                catch(Exception ex)
                {

                }

                dailySettleToDoListModel.ErrorDataList = new List<DailySettleSingleToDo>();

            }

            _LogRecord.Save("DailySettlement", "1.doGetCalcList", "end");
        }

        /// <summary>
        /// 日結排程 step2 依照 ToDoListModel 循序 ( 新增/改單[一般/銷單/規格清洗/其他] )
        ///     step1. 從日結主表搭配todolist表 拉資料 到 日結主表Temp
        ///     step2. 準備呼叫安格斯的程式 更新 日結主表Temp
        /// </summary>
        /// <param name="LimitTime"></param>
        public void doUpdateDailyTempTable(DateTime LimitTime)
        {
            _LogRecord.Save("DailySettlement", "2.doUpdateDailyTempTable", "start");

            //從日結主表搭配todolist表 拉資料 到 日結主表Temp
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                try
                {
                    string sql = @"exec [DailySettle_Step2] @LimitTime ";
                    _conn.Execute(sql,
                                        new
                                        {
                                            LimitTime = LimitTime
                                        }
                                    , null, 600);
                }
                catch (Exception ex)
                {

                }
            }

            // 準備呼叫安格斯的程式 更新 日結主表Temp 
            // 傳這個參數給 安格斯  dailySettleToDoListModel


            _LogRecord.Save("DailySettlement", "2.doUpdateDailyTempTable", "angus接力");

        }

        /// <summary>
        /// 日結排程 step3 數據材積整理 - 基本資料整理(MoneySettleDetail)(一筆) + 
        ///                                 材積規格(CBM_Detail)(多筆)  to 日結Temp表 / 日結Detail Temp表
        ///     step1. 呼叫雷門的程式
        /// </summary>
        public void doInsertMoneySettleDetail_CBM(DailySettleToDoListModel todolist)
        {
            // 呼叫雷門的程式
            // 傳這個參數給 雷門  dailySettleToDoListModel

            _LogRecord.Save("DailySettlement", "3.doInsertMoneySettleDetail_CBM", "");
        }

        /// <summary>
        /// 日結排程 step4 運費計算 to 日結Temp表 / 日結Detail Temp表
        ///     step1. 呼叫 yi cheng 的程式
        /// </summary>
        public void doCalcContractSettleDetail(DailySettleToDoListModel todolist)
        {
            // 呼叫 yi cheng 的程式
            // 傳這個參數給  yi cheng  dailySettleToDoListModel

            _LogRecord.Save("DailySettlement", "4.doCalcContractSettleDetail", "");
        }

        /// <summary>
        /// 日結排程 step5  日結Temp表 -> 日結表 , 日結Detail Temp表 -> 日結Detail表
        ///     step1. 開 trans 更新 日結Temp表 -> 日結表 , 日結Detail Temp表 -> 日結Detail表
        ///     step2. 更新 todolist table 
        ///     step3. 更新 error check_number log table
        /// </summary>
        public void doUpdateTempToProd(DailySettleToDoListModel todolist , DateTime LimitTime)
        {
            // temp系列的表 由誰更新到正式表
            // 合併在 step4 yi cheng 處理


            // 更新 todolist table 
            _LogRecord.Save("DailySettlement", "5.doUpdateTempToProd", "start");


            List<DailySettleSingleToDo> list = todolist.ErrorDataList;

            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                try
                {
                    string sql = @"DailySettle_Step5";
                    _conn.Execute(sql,
                                    new
                                    {
                                        LimitTime = LimitTime
                                    },null,600,System.Data.CommandType.StoredProcedure
                                    );
                }
                catch (Exception ex)
                {

                }

                while (list.Any())
                {
                    var data_part = list.Take(1000);
                    list = list.Skip(1000).ToList();
                    try
                    {
                        string sql = @"
INSERT INTO [dbo].[DailySettleErrorList]
           ([RequestId],[CheckNumber],[ActionType],[IsDo],[MEMO],[CreateDate],[UpdateDate])
     VALUES
           (@RequestId,
            @CheckNumber,
            @ActionType,
            '0',
            '',
            'admin' , 
            getdate())
";
                        _conn.Execute(sql, new{ data_part}, null, 600, System.Data.CommandType.Text
                                        );
                    }
                    catch (Exception ex)
                    {

                    }

                }



            }



            /*
            while (data.Any())
            {
                var data_part = data.Take(1000);
                animalIds = animalIds.Skip(1000).ToList();

            }
            */

            // 更新 error check_number log table


            _LogRecord.Save("DailySettlement", "5.doUpdateTempToProd", "");
        }



        public void InsertNotifyLogRecord(JobRecord tobj)
        {
            try
            {
                using (var _conn = _dbconn.CreateLogConnection())
                {


                    string strSQL = @"
INSERT INTO [dbo].[JobRecord]
           ([Type],[SubType],[Action],[Subject],[ContentBody],[LinkRelationData],[IsDoNotification],[MEMO])
     VALUES
           (@Type , @SubType , @Action , @Subject , @ContentBody , @LinkRelationData , @IsDoNotification , @MEMO  ) ";
                    _conn.ExecuteScalar(strSQL, new
                    {
                        Type = tobj.Type,
                        SubType = tobj.SubType,
                        Action = tobj.Action,
                        Subject = tobj.Subject,
                        ContentBody = tobj.ContentBody,
                        LinkRelationData = tobj.LinkRelationData,
                        IsDoNotification = tobj.IsDoNotification,
                        MEMO = tobj.MEMO
                    });
                }
            }
            catch (Exception e)
            {

            }
        }
    }

    public interface IDailySettleMainAP_DA
    {
        public void doInsertDailyNewData(DateTime LimitTime);

        public DailySettleToDoListModel dailySettleToDoListModel { get; set; }
        public void doGetCalcList(DateTime LimitTime);
        public void doUpdateDailyTempTable(DateTime LimitTime);
        public void doInsertMoneySettleDetail_CBM(DailySettleToDoListModel todolist);
        public void doCalcContractSettleDetail(DailySettleToDoListModel todolist);
        public void doUpdateTempToProd(DailySettleToDoListModel todolist, DateTime LimitTim);
        public void InsertNotifyLogRecord(JobRecord tobj);

    }

}
