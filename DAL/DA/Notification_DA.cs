﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class Notification_DA : INotification_DA
    {

        private readonly DBconn _dbconn;

        public Notification_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<IEnumerable<Notification_Condition>> GetInfoById(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[Notification]  WITH (NOLOCK) WHERE id > @id
                    ";
                var result = await _conn.QueryAsync<Notification_Condition>(sql, new { id });
                return result;
            }
        }

        public async Task Insert(Notification_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[Notification] 
                    (
       [Type]
      ,[Recipient]
      ,[Subject]
      ,[Body]
      ,[CreateUser]

                    ) VALUES
                    (   
       @Type
      ,@Recipient
      ,@Subject
      ,@Body
      ,@CreateUser

                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }

    public interface INotification_DA
    {
        Task Insert(Notification_Condition data);


        Task<IEnumerable<Notification_Condition>> GetInfoById(long id);




    }
}
