﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostEod_DA : IPostEod_DA
    {
        private readonly DBconn _dbconn;

        public PostEod_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<IEnumerable<PostEod_Condition>> GetErrorListById(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostEod]  WITH (NOLOCK) WHERE id > @id AND Fail = 1
                    ";
                var result = await _conn.QueryAsync<PostEod_Condition>(sql, new { id });
                return result;
            }
        }

        public async Task<IEnumerable<PostEod_Condition>> GetInfoByCdate(DateTime CreateTime)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostEod]  WITH (NOLOCK) WHERE CreateTime > @CreateTime
                    ";
                var result = await _conn.QueryAsync<PostEod_Condition>(sql, new { CreateTime });
                return result;
            }
        }
        public async Task<IEnumerable<PostEod_Condition>> GetInfoByCdateAndName(DateTime CreateTime, string EodName)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostEod]  WITH (NOLOCK) WHERE CreateTime > @CreateTime AND EodName = @EodName
                    ";
                var result = await _conn.QueryAsync<PostEod_Condition>(sql, new { CreateTime, EodName });
                return result;
            }
        }

        public async Task<IEnumerable<PostEod_Condition>> GetInfoById(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostEod]  WITH (NOLOCK) WHERE id > @id AND Fail = 0
                    ";
                var result = await _conn.QueryAsync<PostEod_Condition>(sql, new { id });
                return result;
            }
        }

        public async Task Insert(PostEod_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostEod] 
                    (

                    [EodName],
                    [EodPath],
                    [EodContent],
[FileCreateTime],
                    [Fail],
                    [FailMessage],
[FailCount]
                    ) VALUES
                    (   

                    @EodName,
                    @EodPath,
                    @EodContent,
@FileCreateTime,
                    @Fail,
                    @FailMessage,
@FailCount
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }

        public async Task UpdateError(PostEod_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {

                string sql =
                    @"  
Update [dbo].[PostEod]
SET 
EodContent =@EodContent,
Fail =@Fail,
FailMessage=@FailMessage,
FailCount=@FailCount
WHERE id = @id
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }

    public interface IPostEod_DA
    {
        Task Insert(PostEod_Condition data);
        Task<IEnumerable<PostEod_Condition>> GetInfoByCdate(DateTime CreateTime);
        Task<IEnumerable<PostEod_Condition>> GetInfoByCdateAndName(DateTime CreateTime, string EodName);


        Task<IEnumerable<PostEod_Condition>> GetInfoById(long Id);

        Task<IEnumerable<PostEod_Condition>> GetErrorListById(long Id);

        //   Task UpdateError(PostEod_Condition data);
    }
}
