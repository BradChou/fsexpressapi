﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostState_DA : IPostState_DA
    {
        private readonly DBconn _dbconn;

        public PostState_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<IEnumerable<PostState_Condition>> All()
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostState]  WITH (NOLOCK)
                    ";
                var result = await _conn.QueryAsync<PostState_Condition>(sql);
                return result;
            }
        }



    }
    public interface IPostState_DA
    {
        Task<IEnumerable<PostState_Condition>> All();

    }

}
