﻿using BLL.Model.FSExpressAPI.Res.PostOffice;
using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostCollectOnDelivery_DA : IPostCollectOnDelivery_DA
    {
        private readonly DBconn _dbconn;

        public PostCollectOnDelivery_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(PostCollectOnDelivery_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostCollectOnDelivery] 
                    (
                    [request_id],
                    [check_number],
                    [Weight]
                    ) VALUES
                    (   
                    @request_id,
                    @check_number,
                    @Weight
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }

        public async Task<PostCollectOnDelivery_Condition> GetInfoByrequest_idTop1(decimal request_id)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                try
                {
                    string sql =
                    @"  SELECT TOP (1) *
                        FROM [dbo].[PostCollectOnDelivery]  WITH (NOLOCK) WHERE request_id = @request_id
                        ORDER BY CreateTime desc
                    ";
                    var result = await _conn.QueryFirstOrDefaultAsync<PostCollectOnDelivery_Condition>(sql, new { request_id });
                    return result;
                }
                catch (Exception)
                {

                    return null;
                }

            }
        }

        public async Task<PostCollectOnDelivery_Condition> GetInfoByrequest_idTop1_2(decimal request_id)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                try
                {
                    string sql =
                    @"  SELECT TOP (1) *
                        FROM [dbo].[Post]  WITH (NOLOCK) WHERE request_id = @request_id
                        ORDER BY CreateTime desc
                    ";
                    var result = await _conn.QueryFirstOrDefaultAsync<PostCollectOnDelivery_Condition>(sql, new { request_id });
                    return result;
                }
                catch (Exception)
                {

                    return null;
                }

            }
        }

    }
    public interface IPostCollectOnDelivery_DA
    {

        Task Insert(PostCollectOnDelivery_Condition data);
        Task<PostCollectOnDelivery_Condition> GetInfoByrequest_idTop1(decimal request_id);
        Task<PostCollectOnDelivery_Condition> GetInfoByrequest_idTop1_2(decimal request_id);
    }
}

