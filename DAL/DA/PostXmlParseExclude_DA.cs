﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostXmlParseExclude_DA : IPostXmlParseExclude_DA
    {
        private readonly DBconn _dbconn;

        public PostXmlParseExclude_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Insert(PostXmlParseExclude_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostXmlParseExclude] 
                    (
                    [PostXmlParseId],
                    [PostNo],
                    [PostStateNo],
                    [PostStateZH],
                    [ProcessDateTime],
                    [PostXml_id]
                    ) VALUES
                    (   
                    @PostXmlParseId,
                    @PostNo,
                    @PostStateNo,
                    @PostStateZH,
                    @ProcessDateTime,
                    @PostXml_id
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }
    public interface IPostXmlParseExclude_DA
    {
        Task Insert(PostXmlParseExclude_Condition data);
       // Task<IEnumerable<PostXmlParse_Condition>> GetInfoByCdate(DateTime CreateTime);
    }
}

