﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostXmlParse_DA : IPostXmlParse_DA
    {
        private readonly DBconn _dbconn;

        public PostXmlParse_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Insert(PostXmlParse_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostXmlParse] 
                    (
                    [PostNo],
                    [PostStateNo],
                    [PostStateZH],
                    [ProcessDateTime],
                    [PostXml_id]
                    ) VALUES
                    (   
                    @PostNo,
                    @PostStateNo,
                    @PostStateZH,
                    @ProcessDateTime,
                    @PostXml_id
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
        public async Task<IEnumerable<PostXmlParse_Condition>> GetInfoByCdate(DateTime CreateTime)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostXmlParse]  WITH (NOLOCK) WHERE CreateTime > @CreateTime
                    ";
                var result = await _conn.QueryAsync<PostXmlParse_Condition>(sql, new { CreateTime });
                return result;
            }
        }

        public async Task<IEnumerable<PostXmlParse_Condition>> GetInfoById(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostXmlParse]  WITH (NOLOCK) WHERE id > @id
                    ";
                var result = await _conn.QueryAsync<PostXmlParse_Condition>(sql, new { id });
                return result;
            }
        }
    }
    public interface IPostXmlParse_DA
    {
        Task Insert(PostXmlParse_Condition data);
         Task<IEnumerable<PostXmlParse_Condition>> GetInfoByCdate(DateTime CreateTime);

        Task<IEnumerable<PostXmlParse_Condition>> GetInfoById(long id);
    }
}
