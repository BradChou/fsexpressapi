﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
   public class PostStatus_DA : IPostStatus_DA
    {
        private readonly DBconn _dbconn;

        public PostStatus_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<IEnumerable<PostStatus_Condition>> All()
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostStatus]  WITH (NOLOCK)
                    ";
                var result = await _conn.QueryAsync<PostStatus_Condition>(sql);
                return result;
            }
        }
    }
    public interface IPostStatus_DA
    {
        Task<IEnumerable<PostStatus_Condition>> All();

    }
}
