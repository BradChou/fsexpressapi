﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
   public class tcDeliveryRequestsReSpecialArea_DA: ItcDeliveryRequestsReSpecialArea_DA
    {
        private readonly DBconn _dbconn;

        public tcDeliveryRequestsReSpecialArea_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<IEnumerable<tcDeliveryRequestsReSpecialArea_Condition>> GetAll()
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tcDeliveryRequestsReSpecialArea]  WITH (NOLOCK)
                        WHERE customer_code <> 'F3500010002' AND [SpecialAreaIdRe] is null
                    ";
                var result = await _conn.QueryAsync<tcDeliveryRequestsReSpecialArea_Condition>(sql);
                return result;
            }
        }

        public async Task Update(tcDeliveryRequestsReSpecialArea_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {

                string sql =
                    @"  
Update [dbo].[tcDeliveryRequestsReSpecialArea]
SET 
SpecialAreaIdRe = @SpecialAreaIdRe,
SpecialAreaFeeRe = @SpecialAreaFeeRe,
CodeRe = @CodeRe,
SDRe = @SDRe,
MDRe = @MDRe
WHERE request_id = @request_id
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }

        public async Task UpdateR(string request_id, string SpecialAreaId, string SpecialAreaFee)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                try
                {
                    string sql =
                  @"  
Update [dbo].[tcDeliveryRequests]
SET 
SpecialAreaId = @SpecialAreaId,
SpecialAreaFee = @SpecialAreaFee,
udate=GETDATE()
WHERE request_id = @request_id
                    ";
                    await _conn.ExecuteAsync(sql, new { request_id , SpecialAreaId, SpecialAreaFee });
                }
                catch (Exception e)
                {

                    
                }
              

            }
        }
    }
    public interface ItcDeliveryRequestsReSpecialArea_DA
    {
        Task<IEnumerable<tcDeliveryRequestsReSpecialArea_Condition>> GetAll();
        Task UpdateR(string request_id, string SpecialAreaId, string SpecialAreaFee);

    }
}
