﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
  public  class DeliveryStatusPublicError_DA : IDeliveryStatusPublicError_DA
    {
        private readonly DBconn _dbconn;

        public DeliveryStatusPublicError_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Insert(DeliveryStatusPublicError_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[DeliveryStatusPublicError] 
                    (
                   [DeliveryStatusPublicId]
                  ,[DataSource]
                  ,[CheckNumber]
                  ,[DeliveryStatus]
                  ,[ScanDate]
                  ,[CompanyName]
                  ,[TaxIDNumber]
                  ,[DeliveryStatusCode]
                  ,[WorkerName]
                  ,[StationCode]
                  ,[StationName]
                  ,[Status]
                  ,[ErrMsg]
                  ,[Number]
                    ) VALUES
                    (   
                  @DeliveryStatusPublicId
                 ,@DataSource
                 ,@CheckNumber
                 ,@DeliveryStatus
                 ,@ScanDate
                 ,@CompanyName
                 ,@TaxIDNumber
                 ,@DeliveryStatusCode
                 ,@WorkerName
                 ,@StationCode
                 ,@StationName
                 ,@Status
                 ,@ErrMsg
                 ,@Number
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }

    public interface IDeliveryStatusPublicError_DA
    {
        Task Insert(DeliveryStatusPublicError_Condition data);
    }
}
