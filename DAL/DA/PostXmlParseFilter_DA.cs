﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostXmlParseFilter_DA : IPostXmlParseFilter_DA
    {
        private readonly DBconn _dbconn;

        public PostXmlParseFilter_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Insert(PostXmlParseFilter_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostXmlParseFilter] 
                    (
                    [PostXmlParseId],
                    [PostNo],
                    [PostStateNo],
                    [PostStateZH],
                    [ProcessDateTime],
                    [PostXml_id]
                    ) VALUES
                    (   
                    @PostXmlParseId,
                    @PostNo,
                    @PostStateNo,
                    @PostStateZH,
                    @ProcessDateTime,
                    @PostXml_id
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }

        public async Task<PostXmlParseFilter_Condition>  SearchInfo(PostXmlParseFilter_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostXmlParseFilter]  WITH (NOLOCK)
                        WHERE PostNo = @PostNo AND PostStateNo =@PostStateNo AND ProcessDateTime = @ProcessDateTime
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<PostXmlParseFilter_Condition>(sql, data);
                return result;
            }
        }

        public async Task<IEnumerable<PostXmlParseFilter_Condition>> GetInfoById(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostXmlParseFilter]  WITH (NOLOCK) WHERE id > @id
                    ";
                var result = await _conn.QueryAsync<PostXmlParseFilter_Condition>(sql, new { id });
                return result;
            }
        }

        public async Task<IEnumerable<PostXmlParseFilter_Condition>> GetInfoByPostNo(string PostNo)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostXmlParseFilter]  WITH (NOLOCK) WHERE PostNo = @PostNo
                    ";
                var result = await _conn.QueryAsync<PostXmlParseFilter_Condition>(sql, new { PostNo });
                return result;
            }
        }
    }
    public interface IPostXmlParseFilter_DA
    {
        Task Insert(PostXmlParseFilter_Condition data);

        Task<PostXmlParseFilter_Condition> SearchInfo(PostXmlParseFilter_Condition data);
        Task<IEnumerable<PostXmlParseFilter_Condition>> GetInfoById(long id);

        Task<IEnumerable<PostXmlParseFilter_Condition>> GetInfoByPostNo(string PostNo);
        // Task<IEnumerable<PostXmlParse_Condition>> GetInfoByCdate(DateTime CreateTime);
    }
}

