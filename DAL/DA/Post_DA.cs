﻿using BLL.Model.FSExpressAPI.Res.PostOffice;
using BLL.Model.ScheduleAPI.Res.PostOffice;
using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class Post_DA : IPost_DA
    {
        private readonly DBconn _dbconn;

        public Post_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(Post_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[Post] 
                    (
                    [PostId],
[SerialNumber],
[CommodityCode],
[DiscernCode],
[Zip3],
[CheckCode],
                    [request_id],
                    [check_number],
[Weight]
                    ) VALUES
                    (   
                    @PostId,
@SerialNumber,
@CommodityCode,
@DiscernCode,
@Zip3,
@CheckCode,
                    @request_id,
                    @check_number,
@Weight
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
        public async Task<Post_Condition> GetInfoByrequest_idTop1(decimal request_id)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {

                try
                {
                    string sql =
                    @"  SELECT TOP (1) *
                        FROM [dbo].[Post]  WITH (NOLOCK) WHERE request_id = @request_id AND IsDel=0
                        ORDER BY CreateTime desc
                    ";
                    var result = await _conn.QueryFirstOrDefaultAsync<Post_Condition>(sql, new { request_id });
                    return result;
                }
                catch (Exception)
                {

                    return null;
                }

            }
        }
        public async Task<Post_Condition> GetInfoByPostId(string PostId)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[Post]  WITH (NOLOCK) WHERE PostId = @PostId
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<Post_Condition>(sql, new { PostId });
                return result;
            }
        }

        public async Task<List<Post_Condition>> GetInfoByCdate(DateTime CreateTime_S, DateTime CreateTime_E)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
SELECT *
  FROM [dbo].[Post]
  WHERE CreateTime > @CreateTime_S AND CreateTime <@CreateTime_E
                    ";
                var result = (await _conn.QueryAsync<Post_Condition>(sql, new { CreateTime_S, CreateTime_E })).ToList();
                return result;
            }
        }

        public async Task<List<Post_Condition>> GetInfoBycheck_number(string check_number)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
SELECT *
  FROM [dbo].[Post] WITH (NOLOCK) WHERE check_number = @check_number AND IsDel=0
                    ";
                var result = (await _conn.QueryAsync<Post_Condition>(sql, new { check_number })).ToList();
                return result;
            }
        }
        public async Task<List<Post_Condition>> GetInfoBycheck_numberAndPostId(string check_number ,string PostId)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
SELECT *
  FROM [dbo].[Post] WITH (NOLOCK) WHERE check_number = @check_number AND IsDel=0 AND PostId = @PostId
                    ";
                var result = (await _conn.QueryAsync<Post_Condition>(sql, new { check_number, PostId })).ToList();
                return result;
            }
        }
        public async Task<List<GetPostList_Res>> GetPostInfoByCdate(DateTime CreateTime_S, DateTime CreateTime_E)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
SELECT P.* 
      ,[receive_contact]
      ,[receive_city]
      ,[receive_area]
      ,[receive_address]
      ,D.Collection_money
  FROM [dbo].[Post] AS P
  LEFT JOIN [dbo].[tcDeliveryRequests] AS D ON P.request_id=D.request_id
  WHERE CreateTime > @CreateTime_S AND CreateTime <@CreateTime_E AND P.IsDel=0
                    ";
                var result = (await _conn.QueryAsync<GetPostList_Res>(sql, new { CreateTime_S, CreateTime_E })).ToList();
                return result;
            }
        }


        //        public async Task Update(Post_Condition data)
        //        {
        //            using (var _conn = _dbconn.CreateJunFuRealConnection())
        //            {

        //                string sql =
        //                    @"  
        //Update [dbo].[Post]
        //SET PostId = @PostId,UpdateTime = @UpdateTime
        //WHERE id = @id
        //                    ";
        //                await _conn.ExecuteAsync(sql, data);

        //            }
        //        }        
    }
    public interface IPost_DA
    {
        Task<Post_Condition> GetInfoByrequest_idTop1(decimal request_id);
        Task<Post_Condition> GetInfoByPostId(string PostId);

        Task Insert(Post_Condition data);

        Task<List<Post_Condition>> GetInfoByCdate(DateTime CreateTime_S, DateTime CreateTime_E);
        Task<List<GetPostList_Res>> GetPostInfoByCdate(DateTime CreateTime_S, DateTime CreateTime_E);

        Task<List<Post_Condition>> GetInfoBycheck_number(string check_number);

        Task<List<Post_Condition>> GetInfoBycheck_numberAndPostId(string check_number, string PostId);
        // Task Update(Post_Condition data);
    }



    }
