﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class MeasureWeightScanLog_DA : IMeasureWeightScanLog_DA
    {

        private readonly DBconn _dbconn;

        public MeasureWeightScanLog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Add(MeasureWeightScanLog_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql = @"INSERT INTO [MeasureWeightScanLog] (
	[DataSource]
      ,[Scancode]
      ,[Weight]
	)
VALUES (
	@DataSource
      ,@Scancode
      ,@Weight
	
	)";
                await _conn.ExecuteAsync(sql, data);
            }
        }
    }

    public interface IMeasureWeightScanLog_DA
    {
        Task Add(MeasureWeightScanLog_Condition data);

    }
}
