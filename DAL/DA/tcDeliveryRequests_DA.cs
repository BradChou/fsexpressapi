﻿using BLL.Model.ScheduleAPI.Model;
using BLL.Model.ScheduleAPI.Req.DailySettlement;
using Common;
using DAL.Context;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class tcDeliveryRequests_DA : ItcDeliveryRequests_DA
    {
        private readonly DBconn _dbconn;

        public tcDeliveryRequests_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Update(tcDeliveryRequests_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                        UPDATE [dbo].[tcDeliveryRequests]
                        SET [pricing_type]              = @pricing_type
                        ,[customer_code]                = @customer_code
                        ,[check_number]                 = @check_number
                        ,[check_type]                   = check_type
                        ,[order_number]                 = @order_number
                        ,[receive_customer_code]        = @receive_customer_code
                        ,[subpoena_category]            = @subpoena_category
                        ,[receive_tel1]                 = receive_tel1
                        ,[receive_tel1_ext]             = @receive_tel1_ext
                        ,[receive_tel2]                 = @receive_tel2
                        ,[receive_contact]              = @receive_contact
                        ,[receive_zip]                  = @receive_zip
                        ,[receive_city]                 = @receive_city
                        ,[receive_area]                 = @receive_area
                        ,[receive_address]              = @receive_address
                        ,[area_arrive_code]             = @area_arrive_code
                        ,[receive_by_arrive_site_flag]  = @receive_by_arrive_site_flag
                        ,[arrive_address]               = @arrive_address
                        ,[pieces]                       = @pieces
                        ,[plates]                       = @plates
                        ,[cbm]                          = @cbm
                        ,[CbmSize]                      = @CbmSize
                        ,[collection_money]             = @collection_money
                        ,[arrive_to_pay_freight]        = @arrive_to_pay_freight
                        ,[arrive_to_pay_append]         = @arrive_to_pay_append
                        ,[send_contact]                 = @send_contact
                        ,[send_tel]                     = @send_tel
                        ,[send_zip]                     = @send_zip
                        ,[send_city]                    = @send_city
                        ,[send_area]                    = @send_area
                        ,[send_address]                 = @send_address
                        ,[donate_invoice_flag]          = @donate_invoice_flag
                        ,[electronic_invoice_flag]      = @electronic_invoice_flag
                        ,[uniform_numbers]              = @uniform_numbers
                        ,[arrive_mobile]                = @arrive_mobile
                        ,[arrive_email]                 = @arrive_email
                        ,[invoice_memo]                 = @invoice_memo
                        ,[invoice_desc]                 = @invoice_desc
                        ,[product_category]             = @product_category
                        ,[special_send]                 = @special_send
                        ,[arrive_assign_date]           = @arrive_assign_date
                        ,[time_period]                  = @time_period
                        ,[receipt_flag]                 = @receipt_flag
                        ,[pallet_recycling_flag]        = @pallet_recycling_flag
                        ,[Pallet_type]                  = @Pallet_type
                        ,[supplier_code]                = @supplier_code
                        ,[supplier_name]                = @supplier_name
                        ,[supplier_date]                = @supplier_date
                        ,[receipt_numbe]                = @receipt_numbe
                        ,[supplier_fee]                 = @supplier_fee
                        ,[csection_fee]                 = @csection_fee
                        ,[remote_fee]                   = @remote_fee
                        ,[total_fee]                    = @total_fee
                        ,[print_date]                   = @print_date
                        ,[print_flag]                   = @print_flag
                        ,[checkout_close_date]          = @checkout_close_date
                        ,[add_transfer]                 = @add_transfer
                        ,[sub_check_number]             = @sub_check_number
                        ,[import_randomCode]            = @import_randomCode
                        ,[close_randomCode]             = @close_randomCode
                        ,[turn_board]                   = @turn_board
                        ,[upstairs]                     = @upstairs
                        ,[difficult_delivery]           = @difficult_delivery
                        ,[turn_board_fee]               = @turn_board_fee
                        ,[upstairs_fee]                 = @upstairs_fee
                        ,[difficult_fee]                = @difficult_fee
                        ,[cancel_date]                  = @cancel_date
                        ,[cuser]                        = @cuser
                        ,[cdate]                        = @cdate
                        ,[uuser]                        = @uuser
                        ,[udate]                        = @udate
                        ,[HCTstatus]                    = @HCTstatus
                        ,[is_pallet]                    = @is_pallet
                        ,[pallet_request_id]            = @pallet_request_id           
                        ,[Less_than_truckload]          = @Less_than_truckload         
                        ,[Distributor]                  = @Distributor                 
                        ,[temperate]                    = @temperate                   
                        ,[DeliveryType]                 = @DeliveryType                
                        ,[cbmLength]                    = @cbmLength                   
                        ,[cbmWidth]                     = @cbmWidth                    
                        ,[cbmHeight]                    = @cbmHeight                   
                        ,[cbmWeight]                    = @cbmWeight                   
                        ,[cbmCont]                      = @cbmCont                     
                        ,[bagno]                        = @bagno                       
                        ,[ArticleNumber]                = @ArticleNumber               
                        ,[SendPlatform]                 = @SendPlatform                
                        ,[ArticleName]                  = @ArticleName                 
                        ,[round_trip]                   = @round_trip                  
                        ,[ship_date]                    = @ship_date                   
                        ,[VoucherMoney]                 = @VoucherMoney                
                        ,[CashMoney]                    = @CashMoney                   
                        ,[pick_up_good_type]            = @pick_up_good_type           
                        ,[latest_scan_date]             = @latest_scan_date            
                        ,[delivery_complete_date]       = @delivery_complete_date      
                        ,[pic_path]                     = @pic_path                    
                        ,[send_station_scode]           = @send_station_scode          
                        ,[Is_write_off]                 = @Is_write_off                
                        ,[return_check_number]          = @return_check_number        
                        ,[freight]                      = @freight                     
                        ,[latest_dest_date]             = @latest_dest_date            
                        ,[latest_delivery_date]         = @latest_delivery_date        
                        ,[latest_arrive_option_date]    = @latest_arrive_option_date   
                        ,[latest_dest_driver]           = @latest_dest_driver          
                        ,[latest_delivery_driver]       = @latest_delivery_driver      
                        ,[latest_arrive_option_driver]  = @latest_arrive_option_driver 
                        ,[latest_arrive_option]         = @latest_arrive_option        
                        ,[delivery_date]                = @delivery_date 
                        ,[roundtrip_checknumber]=@roundtrip_checknumber
                        WHERE [request_id]              = @request_id
                    ";
                await _conn.ExecuteAsync(sql, data);
                return;
            }
        }
        public async Task UpdateSDMD(tcDeliveryRequests_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                        UPDATE [dbo].[tcDeliveryRequests]
                        SET [ReceiveCode]            = @ReceiveCode
                        ,[ReceiveSD]                 = @ReceiveSD
                        ,[ReceiveMD]                 = @ReceiveMD
                        ,[ShuttleStationCode]        = @ShuttleStationCode
                       ,[udate] = @udate

                        WHERE [request_id]              = @request_id
                    ";
                await _conn.ExecuteAsync(sql, data);
                return;
            }
        }

        public async Task MeasureUpdate(tcDeliveryRequests_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                        UPDATE [dbo].[tcDeliveryRequests]
                        SET 
                        [cbmWidth]                     = @cbmWidth                   
                        ,[cbmLength]                    = @cbmLength                  
                        ,[cbmHeight]                    = @cbmHeight                   
                        ,[cbmWeight]                    = @cbmWeight    
                        ,[cbmCont]                      = @cbmCont    
                        WHERE [request_id]              = @request_id
                    ";
                await _conn.ExecuteAsync(sql, data);
                return;
            }
        }

        public async Task MeasurePathUpdate(string cbmPathColumn, string value, decimal request_id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @$"  
                        UPDATE [dbo].[tcDeliveryRequests]
                        SET 
                        [{cbmPathColumn}]                     = @value
                        WHERE [request_id]              = @request_id
                    ";
                await _conn.ExecuteAsync(sql, new { value, request_id });
                return;
            }
        }

        public async Task UpdateCheckNumber(decimal request_id, string check_number)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @$"  
                        UPDATE [dbo].[tcDeliveryRequests]
                        SET check_number = @check_number
                        WHERE request_id = @request_id
                    ";
                await _conn.ExecuteAsync(sql, new { request_id, check_number });
                return;
            }
        }

        public async Task CancelByCheckNumber(string check_number)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @$"  
                        UPDATE [dbo].[tcDeliveryRequests]
                        SET 
                        cancel_date                   =  GETDATE()
                        WHERE check_number              = @check_number
                    ";
                await _conn.ExecuteAsync(sql, new { check_number });
                return;
            }
        }

        public async Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoByCheckNumber(string check_number)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tcDeliveryRequests]  WITH (NOLOCK) WHERE check_number = @check_number
                    ";
                var result = await _conn.QueryAsync<tcDeliveryRequests_Condition>(sql, new { check_number });
                return result;
            }
        }

        public async Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoListByCheckNumber(List<string> check_number)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tcDeliveryRequests]  WITH (NOLOCK) WHERE check_number in @check_number
                    ";
                var result = await _conn.QueryAsync<tcDeliveryRequests_Condition>(sql, new { check_number });
                return result;
            }
        }

        public async Task<IEnumerable<XlsxQ100ReportModel>> GetInfoByPostEodParseFilterId(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  select a.id,a.CheckNumber,a.PostId,a.StatusDate,b.collection_money 
                        from PostEodParseFilter a WITH (NOLOCK)
                        join tcDeliveryRequests b on a.CheckNumber = b.check_number and a.StatusCode = 'Q100'
                        where id > @id
                    ";
                var result = await _conn.QueryAsync<XlsxQ100ReportModel>(sql, new { id });
                return result;
            }
        }

        public async Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoByCheckNumberWithoutCancel(string check_number)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tcDeliveryRequests]  WITH (NOLOCK) WHERE check_number = @check_number and cancel_date is null
                    ";
                var result = await _conn.QueryAsync<tcDeliveryRequests_Condition>(sql, new { check_number });
                return result;
            }
        }

        public async Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoByCheckNumberAndCustomerCode(string check_number, string customer_code)

        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tcDeliveryRequests]  WITH (NOLOCK) WHERE check_number = @check_number and customer_code = @customer_code
                    ";
                var result = await _conn.QueryAsync<tcDeliveryRequests_Condition>(sql, new { check_number, customer_code });
                return result;
            }
        }

        public async Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoByArriveDate(DateTime date)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"
                    select distinct
                      case when R.DeliveryType = 'R' then (
                        select 
                          B1.station_scode 
                        from 
                          ttArriveSitesScattered A1 With(Nolock) 
                          LEFT JOIN tbStation B1 with(nolock) on B1.station_scode = A1.station_code 
                        where 
                          A1.post_city = R.send_city 
                          and A1.post_area = R.send_area
                      ) else B.station_scode end as 'send_station', 
                      R.cancel_date, 
                      t.scan_date, 
                      T.arrive_option as ArriveOption,
                      R.ship_date, 
                      C.customer_name as customer_name, 
                      REPLACE (C.supplier_code, 'F', '') as CustomerStation, 
                      T.driver_code, 
                      D.station as ArriveDriverStation, 
                      T.cdate as ScanLogCdate,
                      ISNULL(e.driver_code,'') as ReceiveDriverCode,
                      ISNULL(e.station,'') as ReceiveDriverStation,
                      R.* 
                    FROM 
                      (
                        select 
                          * 
                        from 
                          (
                            select 
                              ROW_NUMBER() OVER (
                                PARTITION BY check_number 
                                ORDER BY 
                                  cdate desc
                              ) as ROW_ID, 
                              * 
                            from 
                              (
                                SELECT
			　                    a.arrive_option,
                                  a.cdate, 
                                  a.driver_code, 
                                  a.check_number, 
                                  a.scan_date 
                                FROM 
                                  [JunFuReal].[dbo].[ttDeliveryScanLog] AS a WITH (NOLOCK) 
                                  left join tcDeliveryRequests q with(nolock) on q.check_number = a.check_number 
                                  left join tbCustomers c with(nolock) on c.customer_code = q.customer_code 
                                WHERE 
                                  a.cdate >= '2022.09.01' 
                                  and a.cdate < '2022.10.01'                                  
                                  and q.cdate < '2022.10.01' 
                                  and scan_item = '3' 
                                  and a.check_number is not null 
                                  and a.check_number <> ''
                                  and a.check_number not like '980%' 
                                  and q.Less_than_truckload = '1'
                                  and q.cancel_date is null
                                  and q.check_number is not null
                                  and q.check_number <> ''
                                  and q.request_id is not null
                                 
                                  and (
                                    c.product_type = '1' 
                                    or c.product_type = '2' 
                                    or c.product_type = '3'
                                    or c.product_type = '6'
                                  )			  
                              ) H
                          ) as K 
                        where 
                          K.ROW_ID = 1
                      ) T 
                      LEFT JOIN (
                        Select 
                          cdate, 
                          check_number, 
                          cancel_date, 
                          supplier_code, 
                          DeliveryType, 
                          send_city, 
                          send_area, 
                          send_address, 
                          request_id, 
                          customer_code, 
                          send_station_scode, 
                          area_arrive_code, 
                          receive_city, 
                          receive_area, 
                          receive_address, 
                          pieces, 
                          receipt_flag, 
                          collection_money, 
                          round_trip, 
                          print_date, 
                          ship_date, 
                          order_number, 
                          receive_contact, 
                          invoice_desc, 
                          ProductId, 
                          cbmHeight, 
                          cbmLength, 
                          cbmWidth, 
                          SpecialAreaFee, 
                          SpecialAreaId 
                        From 
                          tcDeliveryRequests WITH (NOLOCK) 
                        Where 
                          cdate < '2022.10.01'                          
	                      and cancel_date is null 
                          and check_number is not null 
                          and request_id is not null 
	                      and check_number <> ''
                          and Less_than_truckload = '1'
                      ) R on R.check_number = T.check_number 
                      LEFT JOIN tbStation B WITH (NOLOCK) ON B.station_code = R.supplier_code 
                      LEFT JOIN tbCustomers C WITH(NOLOCK) ON C.customer_code = R.customer_code 
                      LEFT JOIN tbDrivers D WITH(NOLOCK) ON D.driver_code = T.driver_code 
                      left join ttDeliveryScanLog S with(nolock) on s.scan_date = r.ship_date and s.scan_item = '5' and s.check_number = r.check_number
					  LEFT JOIN tbDrivers e WITH(NOLOCK) ON e.driver_code = s.driver_code 
 
                      --order by t.cdate asc

                    ";
                var result = await _conn.QueryAsync<tcDeliveryRequests_Condition>(sql, new { date });
                return result;
            }
        }




        public async Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoByArriveDate(DateTime sdate , DateTime edate)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"
Declare @now datetime
Declare @sdate datetime 
Declare @edate datetime 

Set @now = format( getdate() , 'yyyyMM01')
Set @edate = @now
Set @sdate = dateadd( month , -1 , @edate )

/*
--Select @sdate , @edate , @now
*/

                    select distinct
                      case when R.DeliveryType = 'R' then (
                        select 
                          B1.station_scode 
                        from 
                          ttArriveSitesScattered A1 With(Nolock) 
                          LEFT JOIN tbStation B1 with(nolock) on B1.station_scode = A1.station_code 
                        where 
                          A1.post_city = R.send_city 
                          and A1.post_area = R.send_area
                      ) else B.station_scode end as 'send_station', 
                      R.cancel_date, 
                      t.scan_date, 
                      T.arrive_option as ArriveOption,
                      R.ship_date, 
                      C.customer_name as customer_name, 
                      REPLACE (C.supplier_code, 'F', '') as CustomerStation, 
                      T.driver_code, 
                      D.station as ArriveDriverStation, 
                      T.cdate as ScanLogCdate,
                      ISNULL(e.driver_code,'') as ReceiveDriverCode,
                      ISNULL(e.station,'') as ReceiveDriverStation,
                      R.* 
                    FROM 
                      (
                        select 
                          * 
                        from 
                          (
                            select 
                              ROW_NUMBER() OVER (
                                PARTITION BY check_number 
                                ORDER BY 
                                  cdate asc
                              ) as ROW_ID, 
                              * 
                            from 
                              (
                                SELECT
   　                    a.arrive_option,
                                  a.cdate, 
                                  a.driver_code, 
                                  a.check_number, 
                                  a.scan_date 
                                FROM 
                                  [JunFuReal].[dbo].[ttDeliveryScanLog] AS a WITH (NOLOCK) 
                                  left join tcDeliveryRequests q with(nolock) on q.check_number = a.check_number 
                                  left join tbCustomers c with(nolock) on c.customer_code = q.customer_code 
                                WHERE 
                                  a.cdate >= '2023/04/01' 
                                  and a.cdate < '2023/05/01'                                 
                                  and q.cdate < '2023/05/01'
                                  and scan_item = '5'
                                  and receive_option = '20'
                                  and a.check_number is not null 
                                  and a.check_number <> ''
                                  and a.check_number not like '980%' 
                                  and q.Less_than_truckload = '1'
                                  and q.cancel_date is null
                                  and q.check_number is not null
                                  and q.check_number <> ''
                                  and q.request_id is not null
                                 
                                  and (
                                   c.product_type = '1' 
                                    or c.product_type = '2' 
                                    or c.product_type = '3'
                                    or c.product_type = '6' 
                                  )     
                              ) H
                          ) as K 
                        where 
                          K.ROW_ID = 1
                          and scan_date > '2023/04/01' and scan_date < '2023/05/01'
                      ) T 
                      LEFT JOIN (
                        Select 
                          cdate, 
                          check_number, 
                          cancel_date, 
                          supplier_code, 
                          DeliveryType, 
                          send_city, 
                          send_area, 
                          send_address, 
                          request_id, 
                          customer_code, 
                          send_station_scode, 
                          area_arrive_code, 
                          receive_city, 
                          receive_area, 
                          receive_address, 
                          pieces, 
                          receipt_flag, 
                          collection_money, 
                          round_trip, 
                          print_date, 
                          ship_date, 
                          order_number, 
                          receive_contact, 
                          invoice_desc, 
                          ProductId, 
                          cbmHeight, 
                          cbmLength, 
                          cbmWidth, 
                          SpecialAreaFee, 
                          SpecialAreaId,
                          SpecCodeId
                        From 
                          tcDeliveryRequests WITH (NOLOCK) 
                        Where 
                          cdate < '2023/05/01'                         
                          and cancel_date is null 
                          and check_number is not null 
                          and request_id is not null 
                       and check_number <> ''
                          and Less_than_truckload = '1'
                      ) R on R.check_number = T.check_number 
                      LEFT JOIN tbStation B WITH (NOLOCK) ON B.station_code = R.supplier_code 
                      LEFT JOIN tbCustomers C WITH(NOLOCK) ON C.customer_code = R.customer_code 
                      LEFT JOIN tbDrivers D WITH(NOLOCK) ON D.driver_code = T.driver_code 
                      left join ttDeliveryScanLog S with(nolock) on s.scan_date = r.ship_date and s.scan_item = '5' and s.check_number = r.check_number
                      LEFT JOIN tbDrivers e WITH(NOLOCK) ON e.driver_code = s.driver_code

                    ";

                var result = await _conn.QueryAsync<tcDeliveryRequests_Condition>(sql,null, null, 500, null);

                return result;
            }
        }




        public async Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoByOrderNumberAndCustomerCode(string order_number, string customer_code)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tcDeliveryRequests]  WITH (NOLOCK) WHERE order_number = @order_number and customer_code = @customer_code
                    ";
                var result = await _conn.QueryAsync<tcDeliveryRequests_Condition>(sql, new { order_number, customer_code });
                return result;
            }
        }


        public async Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoListByrequestId(string request_id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tcDeliveryRequests]  WITH (NOLOCK) WHERE request_id > @request_id
                    ";
                var result = await _conn.QueryAsync<tcDeliveryRequests_Condition>(sql, new { request_id });
                return result;
            }
        }

        public async Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoListByrequestId(List<long> request_id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tcDeliveryRequests]  WITH (NOLOCK) WHERE request_id in @request_id
                    ";
                var result = await _conn.QueryAsync<tcDeliveryRequests_Condition>(sql, new { request_id });
                return result;
            }
        }
        public async Task<IEnumerable<tcDeliveryRequests_Condition>> GetDeliveryInfoByCheckNumber(string check_number)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tcDeliveryRequests] R WITH (NOLOCK)
                        LEFT JOIN [dbo].[ttDeliveryScanLog] S WITH (NOLOCK) ON S.check_number = R.check_number
                        WHERE R.check_number = @check_number 
                        and ((R.latest_scan_item = '5' and S.receive_option = '20') or 
                        (R.latest_scan_item = '6' or R.latest_scan_item = '7' or R.latest_scan_item = '1' or R.latest_scan_item = '2' or R.latest_scan_item = '3' or R.latest_scan_item = '4'))
                    ";
                var result = await _conn.QueryAsync<tcDeliveryRequests_Condition>(sql, new { check_number });
                return result;
            }
        }

        public async Task<tcDeliveryRequests_Condition> GetInfoByRequestId(string request_id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tcDeliveryRequests]  WITH (NOLOCK) WHERE request_id = @request_id
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<tcDeliveryRequests_Condition>(sql, new { request_id });
                return result;
            }
        }

        public async Task<decimal> InsertRetureId(tcDeliveryRequests_Condition data)
        {
            try
            {
                using (var _conn = _dbconn.CreateJunFuRealConnection())
                {
                    string sql =
                        @"  
                 INSERT INTO [dbo].[tcDeliveryRequests] (
	[pricing_type]
	,[customer_code]
	,[check_number]
	,[check_type]
	,[order_number]
	,[receive_customer_code]
	,[subpoena_category]
	,[receive_tel1]
	,[receive_tel1_ext]
	,[receive_tel2]
	,[receive_contact]
	,[receive_zip]
	,[receive_city]
	,[receive_area]
	,[receive_address]
	,[area_arrive_code]
	,[receive_by_arrive_site_flag]
	,[arrive_address]
	,[pieces]
	,[plates]
	,[cbm]
	,[CbmSize]
	,[collection_money]
	,[arrive_to_pay_freight]
	,[arrive_to_pay_append]
	,[send_contact]
	,[send_tel]
	,[send_zip]
	,[send_city]
	,[send_area]
	,[send_address]
	,[donate_invoice_flag]
	,[electronic_invoice_flag]
	,[uniform_numbers]
	,[arrive_mobile]
	,[arrive_email]
	,[invoice_memo]
	,[invoice_desc]
	,[product_category]
	,[special_send]
	,[arrive_assign_date]
	,[time_period]
	,[receipt_flag]
	,[pallet_recycling_flag]
	,[Pallet_type]
	,[supplier_code]
	,[supplier_name]
	,[supplier_date]
	,[receipt_numbe]
	,[supplier_fee]
	,[csection_fee]
	,[remote_fee]
	,[total_fee]
	,[print_date]
	,[print_flag]
	,[checkout_close_date]
	,[add_transfer]
	,[sub_check_number]
	,[import_randomCode]
	,[close_randomCode]
	,[turn_board]
	,[upstairs]
	,[difficult_delivery]
	,[turn_board_fee]
	,[upstairs_fee]
	,[difficult_fee]
	,[cancel_date]
	,[cuser]
	,[cdate]
	,[uuser]
	,[udate]
	,[HCTstatus]
	,[is_pallet]
	,[pallet_request_id]
	,[Less_than_truckload]
	,[Distributor]
	,[temperate]
	,[DeliveryType]
	,[cbmLength]
	,[cbmWidth]
	,[cbmHeight]
	,[cbmWeight]
	,[cbmCont]
	,[bagno]
	,[ArticleNumber]
	,[SendPlatform]
	,[ArticleName]
	,[round_trip]
	,[ship_date]
	,[VoucherMoney]
	,[CashMoney]
	,[pick_up_good_type]
	,[latest_scan_date]
	,[delivery_complete_date]
	,[pic_path]
	,[send_station_scode]
	,[Is_write_off]
	,[return_check_number]
	,[freight]
	,[latest_dest_date]
	,[latest_delivery_date]
	,[latest_arrive_option_date]
	,[latest_dest_driver]
	,[latest_delivery_driver]
	,[latest_arrive_option_driver]
	,[latest_arrive_option]
	,[delivery_date]
	,[payment_method]
	,[catch_cbm_pic_path_from_S3]
	,[catch_taichung_cbm_pic_path_from_S3]
	,[latest_scan_item]
	,[latest_scan_arrive_option]
	,[latest_scan_driver_code]
	,[latest_dest_exception_option]
	,[holiday_delivery]
	,[roundtrip_checknumber]
	,[closing_date]
	,[custom_label]
	,[latest_pick_up_scan_log_id]
	,[orange_r1_1_uuser]
	,[sign_paper_print_flag]
 ,[ProductId]
,[SpecCodeId]
,[ShuttleStationCode]
,[ProductValue],[SpecialAreaFee],[SpecialAreaId]
,[SendCode],[SendSD],[SendMD]
,[ReceiveCode],[ReceiveSD],[ReceiveMD],[ReportFee]
	)

VALUES (
	@pricing_type
	,@customer_code
	,@check_number
	,@check_type
	,@order_number
	,@receive_customer_code
	,@subpoena_category
	,@receive_tel1
	,@receive_tel1_ext
	,@receive_tel2
	,@receive_contact
	,@receive_zip
	,@receive_city
	,@receive_area
	,@receive_address
	,@area_arrive_code
	,@receive_by_arrive_site_flag
	,@arrive_address
	,@pieces
	,@plates
	,@cbm
	,@CbmSize
	,@collection_money
	,@arrive_to_pay_freight
	,@arrive_to_pay_append
	,@send_contact
	,@send_tel
	,@send_zip
	,@send_city
	,@send_area
	,@send_address
	,@donate_invoice_flag
	,@electronic_invoice_flag
	,@uniform_numbers
	,@arrive_mobile
	,@arrive_email
	,@invoice_memo
	,@invoice_desc
	,@product_category
	,@special_send
	,@arrive_assign_date
	,@time_period
	,@receipt_flag
	,@pallet_recycling_flag
	,@Pallet_type
	,@supplier_code
	,@supplier_name
	,@supplier_date
	,@receipt_numbe
	,@supplier_fee
	,@csection_fee
	,@remote_fee
	,@total_fee
	,@print_date
	,@print_flag
	,@checkout_close_date
	,@add_transfer
	,@sub_check_number
	,@import_randomCode
	,@close_randomCode
	,@turn_board
	,@upstairs
	,@difficult_delivery
	,@turn_board_fee
	,@upstairs_fee
	,@difficult_fee
	,@cancel_date
	,@cuser
	,@cdate
	,@uuser
	,@udate
	,@HCTstatus
	,@is_pallet
	,@pallet_request_id
	,@Less_than_truckload
	,@Distributor
	,@temperate
	,@DeliveryType
	,@cbmLength
	,@cbmWidth
	,@cbmHeight
	,@cbmWeight
	,@cbmCont
	,@bagno
	,@ArticleNumber
	,@SendPlatform
	,@ArticleName
	,@round_trip
	,@ship_date
	,@VoucherMoney
	,@CashMoney
	,@pick_up_good_type
	,@latest_scan_date
	,@delivery_complete_date
	,@pic_path
	,@send_station_scode
	,@Is_write_off
	,@return_check_number
	,@freight
	,@latest_dest_date
	,@latest_delivery_date
	,@latest_arrive_option_date
	,@latest_dest_driver
	,@latest_delivery_driver
	,@latest_arrive_option_driver
	,@latest_arrive_option
	,@delivery_date
	,@payment_method
	,@catch_cbm_pic_path_from_S3
	,@catch_taichung_cbm_pic_path_from_S3
	,@latest_scan_item
	,@latest_scan_arrive_option
	,@latest_scan_driver_code
	,@latest_dest_exception_option
	,@holiday_delivery
	,@roundtrip_checknumber
	,@closing_date
	,@custom_label
	,@latest_pick_up_scan_log_id
	,@orange_r1_1_uuser
	,@sign_paper_print_flag
 ,@ProductId
      ,@SpecCodeId
,@ShuttleStationCode
,@ProductValue
,@SpecialAreaFee,@SpecialAreaId
,@SendCode,@SendSD,@SendMD
,@ReceiveCode,@ReceiveSD,@ReceiveMD,@ReportFee
	)
SELECT @@IDENTITY 
                    ";
                    return await _conn.QueryFirstAsync<decimal>(sql, data);

                }
            }
            catch (Exception e)
            {

                throw;
            }
            
        }

        public async Task InsertAPIorigin(string customer_code,string origin)
        {
            try
            {
                using (var _conn = _dbconn.CreateJunFuRealConnection())
                {
                    string sql =
                        @"  
                 INSERT INTO [dbo].[APIorigin] (
	[customer_code]
	,[origin]
	,[cdate]
	)
VALUES (
	'"+customer_code+"',@origin,GETDATE())";
                    await _conn.ExecuteAsync(sql, new { origin });
                    //return await _conn.ExecuteAsync(sql, new { customer_code, origin });
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
public interface ItcDeliveryRequests_DA
{
    Task<decimal> InsertRetureId(tcDeliveryRequests_Condition data);
    //Task Update(tcDeliveryRequests_Condition data);
    //Task Remove(tcDeliveryRequests_Condition data);
    Task Update(tcDeliveryRequests_Condition data);
    Task UpdateSDMD(tcDeliveryRequests_Condition data);
    Task MeasureUpdate(tcDeliveryRequests_Condition data);
    Task MeasurePathUpdate(string cbmPathColumn, string value, decimal request_id);
    Task CancelByCheckNumber(string check_number);
    Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoByCheckNumber(string check_number);
    Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoListByCheckNumber(List<string> check_number);
    Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoListByrequestId(string request_id);
    Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoListByrequestId(List<long> request_id);
    Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoByArriveDate(DateTime date);
    Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoByArriveDate(DateTime sdate, DateTime edate);
    Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoByCheckNumberWithoutCancel(string check_number);
    Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoByCheckNumberAndCustomerCode(string check_number, string customer_code);
    Task<IEnumerable<tcDeliveryRequests_Condition>> GetInfoByOrderNumberAndCustomerCode(string order_number, string customer_code);
    Task<IEnumerable<tcDeliveryRequests_Condition>> GetDeliveryInfoByCheckNumber(string check_number);
    Task<tcDeliveryRequests_Condition> GetInfoByRequestId(string check_number);

    Task InsertAPIorigin(string customer_code,string origin);
    // Task<IEnumerable<tcDeliveryRequests_Condition>> All();

    Task<IEnumerable<XlsxQ100ReportModel>> GetInfoByPostEodParseFilterId(long id);
}


