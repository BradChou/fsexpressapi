﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class CBMDetailLog_DA : ICBMDetailLog_DA
    {
        private readonly DBconn _dbconn;


        public CBMDetailLog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<IEnumerable<CBMDetailLog_Condition>> GetInfoById(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[CBMDetailLog] WITH (NOLOCK) WHERE id > @id
                    ";
                var result = await _conn.QueryAsync<CBMDetailLog_Condition>(sql, new { id });
                return result;
            }
        }
        public async Task<IEnumerable<CBMDetailLog_Condition>> GetInfoByCheckNumber(string CheckNumber)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[CBMDetailLog] WITH (NOLOCK) WHERE CheckNumber = @CheckNumber
                    ";
                var result = await _conn.QueryAsync<CBMDetailLog_Condition>(sql, new { CheckNumber });
                return result;
            }
        }

        public async Task<IEnumerable<CBMDetailLog_Condition>> GetInfoListByCheckNumber(List<string> CheckNumber)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[CBMDetailLog] WITH (NOLOCK) WHERE CheckNumber in @CheckNumber
                    ";
                var result = await _conn.QueryAsync<CBMDetailLog_Condition>(sql, new { CheckNumber },null,1000*60*60);
                return result;
            }
        }
        public async Task Insert(CBMDetailLog_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
INSERT INTO [dbo].[CBMDetailLog]
           (
		   [ComeFrom]
           ,[CheckNumber]
           ,[CBM]
           ,[Length]
           ,[Width]
           ,[Height]
          
           ,[CreateUser]
         
		   )
     VALUES
           (
		    @ComeFrom
           ,@CheckNumber
           ,@CBM
           ,@Length
           ,@Width
           ,@Height
           ,@CreateUser
         
		   )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }

        }
    }
    public interface ICBMDetailLog_DA
    {
        Task<IEnumerable<CBMDetailLog_Condition>> GetInfoById(long id);

        Task<IEnumerable<CBMDetailLog_Condition>> GetInfoByCheckNumber(string CheckNumber);
        Task<IEnumerable<CBMDetailLog_Condition>> GetInfoListByCheckNumber(List<string> CheckNumber);
        Task Insert(CBMDetailLog_Condition data);

    }
}
