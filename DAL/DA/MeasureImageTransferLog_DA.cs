﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class MeasureImageTransferLog_DA : IMeasureImageTransferLog_DA
    {
        private readonly DBconn _dbconn;

        public MeasureImageTransferLog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(MeasureImageTransferLog_Condition _condition)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"INSERT INTO [dbo].[MeasureImageTransferLog]
                   ([DataSource]
                   ,[SourcePath]
                   ,[DestPath]
                   ,[Status]
                   ,[cdate]
                   ,[Message])
                    VALUES
                   (@DataSource
                   ,@SourcePath
                   ,@DestPath
                   ,@Status
                   ,@cdate
                   ,@Message)
                    ";
                await _conn.ExecuteAsync(sql, _condition);
            }
            //  _dbconn.Dispose();
        }

    }
    public interface IMeasureImageTransferLog_DA
    {
        Task Insert(MeasureImageTransferLog_Condition data);
    }
}
