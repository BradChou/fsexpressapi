﻿

using Common;
using DAL.Model.Condition;
using Dapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.DA
{
   public class PostEodParse_DA : IPostEodParse_DA
    {
        private readonly DBconn _dbconn;

        public PostEodParse_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(PostEodParse_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostEodParse] 
                    (
                    [PostEodId]
                    ,[PostId]
                    ,[CheckNumber]
                    ,[PostStation]
                    ,[StatusDate]
                    ,[StatusCode]
                    ,[StatusZH]

                    ) VALUES
                    (   
                    @PostEodId,
                    @PostId,
                    @CheckNumber,
                    @PostStation,
                    @StatusDate,
                    @StatusCode,
                    @StatusZH
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
        public async Task<IEnumerable<PostEodParse_Condition>> GetInfoById(long id)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[PostEodParse] WHERE id > @id
                    ";
                var result = await _conn.QueryAsync<PostEodParse_Condition>(sql, new { id });
                return result;
            }
        }
    }
    public interface IPostEodParse_DA
    {
        Task Insert(PostEodParse_Condition data);

        //Task<PostEodParseFilter_Condition> SearchInfo(PostEodParseFilter_Condition data);
        Task<IEnumerable<PostEodParse_Condition>> GetInfoById(long id);

        //Task<IEnumerable<PostEodParseFilter_Condition>> GetInfoByPostNo(string PostNo);
        // Task<IEnumerable<PostXmlParse_Condition>> GetInfoByCdate(DateTime CreateTime);
    }
}
