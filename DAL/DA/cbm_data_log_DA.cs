﻿using Common;
using System;
using System.Collections.Generic;
using DAL.Model.Condition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DAL.DA
{
    public class cbm_data_log_DA : Icbm_data_log_DA
    {
        private readonly DBconn _dbconn;

        public cbm_data_log_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(cbm_data_log_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[cbm_data_log] 
                    (
                   [file_name]
                  ,[file_row]
                  ,[cdate]
                  ,[check_number]
                  ,[length]
                  ,[width]
                  ,[height]
                  ,[cbm]
                  ,[s3_pic_uri]
                  ,[s3_pic_uri_2]
                  ,[scan_time]
                  ,[data_source]
                  ,[scan_result]
                  ,[is_log]
                  ,[uuser]
                  ,[udate]

                    ) VALUES
                    (   
                   @file_name
                  ,@file_row
                  ,@cdate
                  ,@check_number
                  ,@length
                  ,@width
                  ,@height
                  ,@cbm
                  ,@s3_pic_uri
                  ,@s3_pic_uri_2
                  ,@scan_time
                  ,@data_source
                  ,@scan_result
                  ,@is_log
                  ,@uuser
                  ,@udate
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }

        }
    }

    public interface Icbm_data_log_DA
    {
        public Task Insert(cbm_data_log_Condition data);
    }
}
