﻿using Common;
using DAL.Context;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Common.Setting.JobSetting;

namespace DAL.DA
{
    public class FSExpressAPILog_DA : IFSExpressAPILog_DA
    {
        private readonly DBconn _dbconn;

        public FSExpressAPILog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

       
        public async Task Add(FSExpressAPILog_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[FSExpressAPILog] (Guid, Account, RequestBody, ResponseBody, RequestHeader, Path, QueryString, StratTime, EndTime) VALUES ( @Guid, @Account, @RequestBody, @ResponseBody, @RequestHeader, @Path, @QueryString, @StratTime, @EndTime)
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
  
    }
    public interface IFSExpressAPILog_DA
    {
        Task Add(FSExpressAPILog_Condition data);
       
    }
}
