﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class ScheduleAPILog_DA : IScheduleAPILog_DA
    {
        private readonly DBconn _dbconn;

        public ScheduleAPILog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }


        public async Task Add(ScheduleAPILog_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[ScheduleAPILog] (Guid, Account, RequestBody, ResponseBody, RequestHeader, Path, QueryString, StratTime, EndTime) VALUES ( @Guid, @Account, @RequestBody, @ResponseBody, @RequestHeader, @Path, @QueryString, @StratTime, @EndTime)
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }
    public interface IScheduleAPILog_DA
    {
        Task Add(ScheduleAPILog_Condition data);

    }
}
