﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using DAL.Model.Condition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DAL.DA
{
    public class CBMDetailB_DA : ICBMDetailB_DA
    {
        private readonly DBconn _dbconn;

        public async Task<IEnumerable<CBMDetailB_Condition>> GetCBMInfo()
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
Declare @now datetime
Declare @sdate datetime 
Declare @edate datetime 

Set @now = format( getdate() , 'yyyyMM01')
Set @edate = @now
Set @sdate = dateadd( month , -1 , @edate )

                    Select * 
                    from CBMDetailB with(nolock)   
                    WHERE CheckNumber in (
										Select distinct check_number From ttDeliveryScanLog with(nolock)
										Where cdate >= @sdate 
										and cdate < @edate    
										)    
           
                    ";
                var result = await _conn.QueryAsync<CBMDetailB_Condition>(sql);
                return result;
            }

        }


        public CBMDetailB_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(CBMDetailB_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
INSERT INTO [dbo].[CBMDetailB]
           (
	        [CBMDetailLogId]
           ,[ComeFrom]
           ,[CheckNumber]
           ,[CBM]
           ,[Length]
           ,[Width]
           ,[Height]
		   )
     VALUES
           (
		    @CBMDetailLogId
           ,@ComeFrom
           ,@CheckNumber
           ,@CBM
           ,@Length
           ,@Width
           ,@Height
		   )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }

        }


        public async Task DELETE(string CheckNumber)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {

                string sql =
                    @"  
DELETE FROM [dbo].[CBMDetailB]
WHERE CheckNumber = @CheckNumber
                    ";
                await _conn.ExecuteAsync(sql, new { CheckNumber });

            }
        }

        public async Task<IEnumerable<CBMDetailB_Condition>> GetInfoByCheckNumber(string CheckNumber)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[CBMDetailB]  WHERE CheckNumber = @CheckNumber
                    ";
                var result = await _conn.QueryAsync<CBMDetailB_Condition>(sql, new { CheckNumber });
                return result;
            }
        }
    }
    public interface ICBMDetailB_DA
    {
        Task DELETE(string CheckNumber);
        Task Insert(CBMDetailB_Condition data);

        Task<IEnumerable<CBMDetailB_Condition>> GetInfoByCheckNumber(string CheckNumber);
        public Task<IEnumerable<CBMDetailB_Condition>> GetCBMInfo();
    }
}
