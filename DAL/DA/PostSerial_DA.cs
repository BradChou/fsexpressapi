﻿using BLL.Model.FSExpressAPI.Model;
using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class PostSerial_DA : IPostSerial_DA
    {
        private readonly DBconn _dbconn;

        public PostSerial_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<PostInfo> GetNumberByDiscern(PostSerialDiscern postSerialDiscern)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @" 
DECLARE　　@Serial VARCHAR(6) DECLARE　　@Customer VARCHAR(6) DECLARE　　@Discern VARCHAR(2)

--為1筆
SELECT TOP (1) @Serial = Serial
	,@Customer = Customer
	,@Discern = Discern
FROM [dbo].[PostSerial]
WHERE Discern = @postSerialDiscern

--字串變數
DECLARE @o_Serial VARCHAR(6) = @Serial
DECLARE @n_Serial VARCHAR(7)
--變數轉數字
DECLARE @o_Serialint INT = CAST(@o_Serial AS INT)
DECLARE @n_Serialint INT = CAST(@o_Serial + 1 AS INT)

SET @n_Serial = CAST(@n_Serialint AS VARCHAR(7))

--字串長度來判斷有沒有超過
DECLARE @o_SerialLen INT = LEN(@o_Serial)
DECLARE @n_SerialLen INT = LEN(@n_Serial)

--近位超過
IF (@n_SerialLen > @o_SerialLen)
BEGIN
	SET @n_Serial = CAST(SUBSTRING(@n_Serial, @n_SerialLen - @o_SerialLen + 1, @n_SerialLen) AS VARCHAR(7))
END

--判斷最後一筆不能為0
IF (CAST(@n_Serial AS INT) = 0)
BEGIN
	SET @n_Serial = '1'
END

--統一碼
--SET @n_Serial = Right('000000' + Cast(@n_Serial as varchar),@o_SerialLen)
SET @n_Serial = REPLICATE('0', @o_SerialLen - len(@n_Serial)) + @n_Serial

--更新
UPDATE [dbo].[PostSerial]
SET Serial = @n_Serial
	,UpdateTime = GETDATE()
WHERE Discern = @postSerialDiscern

--取號
SELECT @n_Serial AS SerialNumber, @Customer AS CommodityCode , @Discern AS DiscernCode
                    ";
                var result = await _conn.QueryFirstOrDefaultAsync<PostInfo>(sql, new { postSerialDiscern });
                return result;
            }
        }



    }
    public interface IPostSerial_DA
    {
        Task<PostInfo> GetNumberByDiscern(PostSerialDiscern postSerialDiscern);

    }

    public enum PostSerialDiscern
    {
        Lv1 = 18,
        Lv2 = 78,
        CollectOnDelivery = 74
    }

}
