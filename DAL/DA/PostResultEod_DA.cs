﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
   public class PostResultEod_DA : IPostResultEod_DA
    {
        private readonly DBconn _dbconn;

        public PostResultEod_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(PostResultEod_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                    INSERT [dbo].[PostResultEod] 
                    (
                   [EodName]
      ,[EodPath]
      ,[PostId]
      ,[CheckNumber]
      ,[OrderFileDate]
      ,[StatusCode]
      ,[StatusZH]

                    ) VALUES
                    (   
                   @EodName
                 ,@EodPath
                 ,@PostId
                 ,@CheckNumber
                 ,@OrderFileDate
                 ,@StatusCode
                 ,@StatusZH
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
        //public async Task<IEnumerable<PostResultEod_Condition>> GetInfoById(long id)
        //{
        //    using (var _conn = _dbconn.CreateJunFuRealConnection())
        //    {
        //        string sql =
        //            @"  SELECT *
        //                FROM [dbo].[PostResultEod] WHERE id > @id
        //            ";
        //        var result = await _conn.QueryAsync<PostResultEod_Condition>(sql, new { id });
        //        return result;
        //    }
        //}
    }
    public interface IPostResultEod_DA
    {
        Task Insert(PostResultEod_Condition data);

        //Task<PostResultEodFilter_Condition> SearchInfo(PostResultEodFilter_Condition data);
       // Task<IEnumerable<PostResultEod_Condition>> GetInfoById(long id);

        //Task<IEnumerable<PostResultEodFilter_Condition>> GetInfoByPostNo(string PostNo);
        // Task<IEnumerable<PostXmlParse_Condition>> GetInfoByCdate(DateTime CreateTime);
    }
}
