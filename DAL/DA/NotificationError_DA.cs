﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class NotificationError_DA : INotificationError_DA
    {
        private readonly DBconn _dbconn;

        public NotificationError_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Insert(NotificationError_Condition data)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
                   INSERT [dbo].[NotificationError] (
	[NotificationId]
	,[Type]
	,[Recipient]
	,[Subject]
	,[Body]
	,[CreateUser]
	,[PendingCount]
	,[Status]
	,[ErrMsg]
	)
VALUES (
	@NotificationId
	,@Type
	,@Recipient
	,@Subject
	,@Body
	,@CreateUser
	,@PendingCount
	,@Status
	,@ErrMsg
	)
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }

    public interface INotificationError_DA
    {
        Task Insert(NotificationError_Condition data);
    }
}
