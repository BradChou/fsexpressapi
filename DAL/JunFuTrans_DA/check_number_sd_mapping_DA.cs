﻿using Common;
using DAL.Model.JunFuTrans_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.JunFuTrans_DA
{
    public class check_number_sd_mapping_DA: Icheck_number_sd_mapping_DA
    {
        private readonly DBconn _dbconn;

        public check_number_sd_mapping_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(check_number_sd_mapping_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuTransConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[check_number_sd_mapping]
                        (
                        [check_number]
           ,[org_area_id]
           ,[md]
           ,[sd]
           ,[put_order]
           ,[request_id]
           ,[cdate]
           ,[udate]
                        )
                        VALUES
                       (
            @check_number
           ,@org_area_id
           ,@md
           ,@sd
           ,@put_order
           ,@request_id
           ,@cdate
           ,@udate
                        )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }

    }

    public interface Icheck_number_sd_mapping_DA
    {
        Task Insert(check_number_sd_mapping_Condition data);

    }
}
