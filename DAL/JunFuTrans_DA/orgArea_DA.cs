﻿using Common;
using DAL.Model.Condition;
using DAL.Model.JunFuTrans_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.JunFuTrans_DA
{
    public class orgArea_DA : IorgArea_DA
    {
        private readonly DBconn _dbconn;

        public orgArea_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<IEnumerable<orgArea_Condition>> All()
        {
            using (var _conn = _dbconn.CreateJunFuTransConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[orgArea]  WITH (NOLOCK)
                        WHERE CITY is not null AND AREA is not null AND ROAD is not null AND ZIPCODE is not null AND station_scode <> ''
                    ";
                var result = await _conn.QueryAsync<orgArea_Condition>(sql);
                return result;
            }
        }
        public async Task<IEnumerable<orgArea_Condition>> GetInfoZip3(string city ,string area)
        {
            using (var _conn = _dbconn.CreateJunFuTransConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[orgArea]  WITH (NOLOCK)
                        WHERE CITY = @city AND AREA =@area
                    ";
                var result = await _conn.QueryAsync<orgArea_Condition>(sql,new { city, area });
                return result;
            }
        }




    }
    public interface IorgArea_DA
    {
        Task<IEnumerable<orgArea_Condition>> All();
        Task<IEnumerable<orgArea_Condition>> GetInfoZip3(string city, string area);
    }



}
