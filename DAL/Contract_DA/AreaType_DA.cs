﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class AreaType_DA : IAreaType_DA
    {
        private readonly DBconn _dbconn;

        public AreaType_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<AreaType_Condition> GetAreaType(string City , string Area)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT Type
                        FROM [dbo].[AreaType] WITH (NOLOCK) WHERE city = @City and area = @Area
                    ";              

                var result = await _conn.QueryFirstOrDefaultAsync<AreaType_Condition>(sql, new { City, Area });
                return result;
            }
        }
        public async Task<IEnumerable<AreaType_Condition>> GetAllAreaType()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[AreaType] WITH (NOLOCK) 
                    ";

                var result = await _conn.QueryAsync<AreaType_Condition>(sql);
                return result;
            }
        }
    }
    public interface IAreaType_DA
    {
        Task<AreaType_Condition> GetAreaType(string City, string Area);
        Task<IEnumerable<AreaType_Condition>> GetAllAreaType();
    }

}
