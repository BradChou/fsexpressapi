﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class ProductValuationManageScope_DA: IProductValuationManageScope_DA
    {

        private readonly DBconn _dbconn;

        public ProductValuationManageScope_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<List<ProductValuationManageScope_Condition>> GetpublicAll()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[ProductValuationManageScope] WITH (NOLOCK)
                    ";
                var result = (await _conn.QueryAsync<ProductValuationManageScope_Condition>(sql)).ToList();
                return result;
            }
        }
    }
    public interface IProductValuationManageScope_DA
    {
        Task<List<ProductValuationManageScope_Condition>> GetpublicAll();
    }
}
