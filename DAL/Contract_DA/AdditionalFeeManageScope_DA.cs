﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class AdditionalFeeManageScope_DA : IAdditionalFeeManageScope_DA
    {
        private readonly DBconn _dbconn;

        public AdditionalFeeManageScope_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<List<AdditionalFeeManageScope_Condition>> GetPublicAll()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[AdditionalFeeManageScope] WITH (NOLOCK)
                    ";
                var result = (await _conn.QueryAsync<AdditionalFeeManageScope_Condition>(sql)).ToList();
                return result;
            }
        }
    }

    public interface IAdditionalFeeManageScope_DA
    {
        Task<List<AdditionalFeeManageScope_Condition>> GetPublicAll();
    }
}
