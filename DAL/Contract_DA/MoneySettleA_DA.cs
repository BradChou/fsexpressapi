﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class MoneySettleA_DA: IMoneySettleA_DA
    {
        private readonly DBconn _dbconn;

        public MoneySettleA_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<List<MoneySettleA_Condition>> GetpublicAll()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MoneySettleA] WITH (NOLOCK) WHERE IsActive = 1
                    ";
                var result = (await _conn.QueryAsync<MoneySettleA_Condition>(sql)).ToList();
                return result;
            }
        }
    }
    public interface IMoneySettleA_DA
    {
        Task<List<MoneySettleA_Condition>> GetpublicAll();
    }

}
