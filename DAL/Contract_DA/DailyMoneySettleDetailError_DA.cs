﻿using Common;
using System;
using System.Collections.Generic;
using DAL.Model.Condition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using BLL.Model.ScheduleAPI.Req.DailySettlement;

namespace DAL.DA
{
    public class DailyMoneySettleDetailError_DA : IDailyMoneySettleDetailError_DA
    {
        private readonly DBconn _dbconn;

        public DailyMoneySettleDetailError_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public void Insert(List<DailyMoneySettleDetailError_Condition> data)
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                string sql =
                    @"  
                    INSERT into [dbo].[DailyMoneySettleDetailError] 
                    (

                      [RequestId]
                      ,[CheckNumber]
                      ,[CreateDate]
                      
                    )
                    VALUES
                    (   
                      @RequestId
                      ,@CheckNumber
                      ,@CreateDate
                      
                    )
                    ";
                _conn.Execute(sql, data,null,500);
            }

        }
    }


    public interface IDailyMoneySettleDetailError_DA
    {
        
        public void Insert(List<DailyMoneySettleDetailError_Condition> data);
    }


}