﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DAL.Contract_DA
{
    public class MoneySettleCScope_DA : IMoneySettleCScope_DA
    {

        private readonly DBconn _dbconn;

        public MoneySettleCScope_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<List<MoneySettleCScope_Condition>> GetpublicAll()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MoneySettleCScope] WITH (NOLOCK)
                    ";
                var result = (await _conn.QueryAsync<MoneySettleCScope_Condition>(sql)).ToList();
                return result;
            }
        }
    }
    public interface IMoneySettleCScope_DA
    {
        Task<List<MoneySettleCScope_Condition>> GetpublicAll();
    }
}
