﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class SMSFamilyNetlog_DA : ISMSFamilyNetlog_DA
    {
        private readonly DBconn _dbconn;

        public SMSFamilyNetlog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(SMSFamilyNetlog_Condition insertData)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @" INSERT INTO [dbo].[SMSFamilyNetlog]
            (
            [RequestBody]
           ,[ResponseBody]
           ,[RequestId]
           ,[StatusCode]
           ,[StatusDesc]
           ,[StartTime]
           ,[EndTime]
         )
     VALUES
           (
            @RequestBody
           ,@ResponseBody
           ,@RequestId
           ,@StatusCode
           ,@StatusDesc
           ,@StartTime
           ,@EndTime
          )
                    ";

                await _conn.ExecuteAsync(sql, insertData);
            }
        }


    }
    public interface ISMSFamilyNetlog_DA
    {
        Task Insert(SMSFamilyNetlog_Condition data);
       
    }
}

