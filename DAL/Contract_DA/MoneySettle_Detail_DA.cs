﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class MoneySettle_Detail_DA: IMoneySettle_Detail_DA
    {
        private readonly DBconn _dbconn;

        public MoneySettle_Detail_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }


        public async Task<List<MoneySettle_Detail_Condition>> GetInfoGreaterThanId(long id)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MoneySettle_Detail] WITH (NOLOCK) WHERE id > @id 
                        AND ISNULL(SendStation,'') != '' --AND ISNULL(ArriveDriverStation,'') != '' 
                        --AND id not in
                        --(
                        --select a.id from [FSE01].[dbo].[MoneySettle_Detail] a with(nolock)
                        --left join [FSE01].[dbo].[CBM_Detail] b on a.id = b.DetailID
                        --where cbm is null OR ( ProductId = 'PS000032'AND cbm = 'B003')
                        --) AND CheckNumber not like '500%'
                        --AND ArriveDriverStation !=''
                    ";
                var result = (await _conn.QueryAsync<MoneySettle_Detail_Condition>(sql, new { id })).ToList();
                return result;
            }
        }
        public async Task<List<MoneySettle_Detail_Condition>> GetInfoGreaterBetweenId(long sId,long eId)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MoneySettle_Detail] WITH (NOLOCK) WHERE id >= @sId AND id <= @eId
                        AND ISNULL(SendStation,'') != '' AND ArriveDriverStation is not null
                    ";
                var result = (await _conn.QueryAsync<MoneySettle_Detail_Condition>(sql, new { sId, eId })).ToList();
                return result;
            }
        }
        public async Task<List<MoneySettle_Detail_Condition>> GetInfoInId(int[] ids)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MoneySettle_Detail] WITH (NOLOCK) WHERE id in @ids
                        AND ISNULL(SendStation,'') != '' AND ArriveDriverStation is not null
                    ";
                var result = (await _conn.QueryAsync<MoneySettle_Detail_Condition>(sql, new { ids })).ToList();
                return result;
            }
        }
            /// <summary>
            /// 測試 
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public async Task<List<MoneySettle_Detail_Condition>> GetInfoGreaterThanIdTEST()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"   select * from MoneySettle_Detail where id in(

'371454'
,'355297'
,'404676'
)";
                var result = (await _conn.QueryAsync<MoneySettle_Detail_Condition>(sql)).ToList();
                return result;
            }
        }
    }
    public interface IMoneySettle_Detail_DA
    {

        Task<List<MoneySettle_Detail_Condition>> GetInfoGreaterThanId(long id);
        Task<List<MoneySettle_Detail_Condition>> GetInfoGreaterBetweenId(long sId, long eId);
        Task<List<MoneySettle_Detail_Condition>> GetInfoGreaterThanIdTEST();
        Task<List<MoneySettle_Detail_Condition>> GetInfoInId(int[] ids);
    }
}
