﻿using Common;
using System;
using System.Collections.Generic;
using DAL.Model.Condition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using BLL.Model.ScheduleAPI.Req.DailySettlement;

namespace DAL.DA
{
    public class DailyMoneySettleDetail_DA : IDailyMoneySettleDetail_DA
    {
        private readonly DBconn _dbconn;

        public DailyMoneySettleDetail_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public void Insert(List<DailyMoneySettleDetail_Condition> data)
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                string sql =
                    @"  
                    INSERT into [dbo].[DailyMoneySettleDetail] 
                    (
                       [Id]
                      ,[RequestId]
                      ,[DeliveryType]
                      ,[CheckNumber]
                      ,[CreateDate]
                      ,[CustomerCode]
                      ,[SendStation]
                      ,[ArriveStation]
                      ,[City]
                      ,[Area]
                      ,[Address]
                      ,[ProductId]
                      ,[Pieces]
                      ,[ReceiptFlag]
                      ,[CollectionMoney]
                      ,[Valued]
                      ,[OverCBM]
                      ,[RoundTrip]
                      ,[SpecialAreaFee]
                      ,[EggArea]
                      ,[PrintDate]
                      ,[ShipDate]
                      ,[OrderNumber]
                      ,[CustomerName]
                      ,[ReceiveContact]
                      ,[InvoiceDesc]
                      ,[ScanDate]
                      ,[CreateUser]
                      ,[Request_CreateDate]
                      ,[SpecialAreaId]
                      ,[DriverCode]
                      ,[ArriveDriverStation]
                      ,[CustomerStation]
                      ,[ArriveOption]
                      ,[ScanLogCdate]
                      ,[ReceiveDriverStation]
                      ,[ReceiveDriverCode]
                      ,[isEast]
                    )
                    VALUES
                    (   
                       @Id
                      ,@RequestId
                      ,@DeliveryType
                      ,@CheckNumber
                      ,@CreateDate
                      ,@CustomerCode
                      ,@SendStation
                      ,@ArriveStation
                      ,@City
                      ,@Area
                      ,@Address
                      ,@ProductId
                      ,@Pieces
                      ,@ReceiptFlag
                      ,@CollectionMoney
                      ,@Valued
                      ,@OverCBM
                      ,@RoundTrip
                      ,@SpecialAreaFee
                      ,@EggArea
                      ,@PrintDate
                      ,@ShipDate
                      ,@OrderNumber
                      ,@CustomerName
                      ,@ReceiveContact
                      ,@InvoiceDesc
                      ,@ScanDate
                      ,@CreateUser
                      ,@Request_CreateDate
                      ,@SpecialAreaId
                      ,@DriverCode
                      ,@ArriveDriverStation
                      ,@CustomerStation
                      ,@ArriveOption
                      ,@ScanLogCdate 
                      ,@ReceiveDriverStation
                      ,@ReceiveDriverCode
                      ,@isEast
                    )
                    ";
                _conn.Execute(sql, data,null,500);
            }

        }

        public long GetKey()
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                string sql =
                    @"  
Declare @now nvarchar(8)
Declare @day1 nvarchar(100)
Declare @day2 nvarchar(100)

Declare @result nvarchar(19) 
Set @now = convert( nvarchar(8) , getdate() , 112 )  
Select @day1 = left(Max([id]), 8) , @day2 = left(Max([id]),11) From [DailyMoneySettleDetail]  

if ( @now = @day1 ) 
Begin 
	Set @result = cast( (cast(@day2 as bigint) + 1) as nvarchar(11) ) + '00000000'
End
else
Begin 
	Set @result = @now + '00100000000'
End

Select cast( @result as bigint) as id
                    ";
                
                var tResult = _conn.Query<Money_Settle_Detail_Condition>(sql).FirstOrDefault();

                return tResult.id;

            }

        }

        public async Task<IEnumerable<tcDeliveryRequests_Condition>> GetDailySettleData(List<string> data)
        {
            List<tcDeliveryRequests_Condition> result_all = new List<tcDeliveryRequests_Condition>();
            while (data.Any())
            {
                var data_part = data.Take(1000);
                data = data.Skip(1000).ToList();

                using (var _conn = _dbconn.CreateJunFuRealConnection())
                {
                    string sql =
                        @"
    Declare @now datetime
    Declare @sdate datetime 
    Declare @edate datetime 

    Set @now = format( getdate() , 'yyyyMM01')
    Set @edate = @now
    Set @sdate = dateadd( month , -1 , @edate )

    /*
    --Select @sdate , @edate , @now
    */

                        select distinct
                          case when R.DeliveryType = 'R' then (
                            select 
                              B1.station_scode 
                            from 
                              ttArriveSitesScattered A1 With(Nolock) 
                              LEFT JOIN tbStation B1 with(nolock) on B1.station_scode = A1.station_code 
                            where 
                              A1.post_city = R.send_city 
                              and A1.post_area = R.send_area
                          ) else B.station_scode end as 'send_station', 
                          R.cancel_date, 
                          t.scan_date, 
                          T.arrive_option as ArriveOption,
                          R.ship_date, 
                          C.customer_name as customer_name, 
                          REPLACE (C.supplier_code, 'F', '') as CustomerStation, 
                          T.driver_code, 
                          D.station as ArriveDriverStation, 
                          T.cdate as ScanLogCdate,
                          ISNULL(e.driver_code,'') as ReceiveDriverCode,
                          ISNULL(e.station,'') as ReceiveDriverStation,
                          R.* 
                        FROM 
                          (
                            select 
                              * 
                            from 
                              (
                                select 
                                  ROW_NUMBER() OVER (
                                    PARTITION BY check_number 
                                    ORDER BY 
                                      cdate desc
                                  ) as ROW_ID, 
                                  * 
                                from 
                                  (
                                    SELECT
   　                        a.arrive_option,
                                      a.cdate, 
                                      a.driver_code, 
                                      a.check_number, 
                                      a.scan_date 
                                    FROM 
                                      [JunFuReal].[dbo].[ttDeliveryScanLog] AS a WITH (NOLOCK) 
                                      left join tcDeliveryRequests q with(nolock) on q.check_number = a.check_number 
                                      left join tbCustomers c with(nolock) on c.customer_code = q.customer_code 
                                    WHERE 
                                      a.check_number in @CheckNumber
                                      and scan_item = '5'  
                                  ) H
                              ) as K 
                            where 
                              K.ROW_ID = 1
                          ) T 
                          LEFT JOIN (
                            Select 
                              cdate, 
                              check_number, 
                              cancel_date, 
                              supplier_code, 
                              DeliveryType, 
                              send_city, 
                              send_area, 
                              send_address, 
                              request_id, 
                              customer_code, 
                              send_station_scode, 
                              area_arrive_code, 
                              receive_city, 
                              receive_area, 
                              receive_address, 
                              pieces, 
                              receipt_flag, 
                              collection_money, 
                              round_trip, 
                              print_date, 
                              ship_date, 
                              order_number, 
                              receive_contact, 
                              invoice_desc, 
                              ProductId,
                              ProductValue,
                              cbmHeight, 
                              cbmLength, 
                              cbmWidth, 
                              SpecialAreaFee, 
                              SpecialAreaId,
                              SpecCodeId
                            From 
                              tcDeliveryRequests WITH (NOLOCK) 
                            Where 
                              1=1--cdate < '2022/10/01'                            
                           and cancel_date is null 
                              and check_number is not null 
                              and request_id is not null 
                           and check_number <> ''
                              and Less_than_truckload = '1'
                          ) R on R.check_number = T.check_number 
                          LEFT JOIN tbStation B WITH (NOLOCK) ON B.station_code = R.supplier_code 
                          LEFT JOIN tbCustomers C WITH(NOLOCK) ON C.customer_code = R.customer_code 
                          LEFT JOIN tbDrivers D WITH(NOLOCK) ON D.driver_code = T.driver_code 
                          left join ttDeliveryScanLog S with(nolock) on s.scan_date = r.ship_date and s.scan_item = '5' and s.check_number = r.check_number
           LEFT JOIN tbDrivers e WITH(NOLOCK) ON e.driver_code = s.driver_code
                        ";

                        var result = await _conn.QueryAsync<tcDeliveryRequests_Condition>(sql, new { CheckNumber = data_part }, null, 500, null);

                if (result_all != null)
                {
                    result_all.AddRange(result);
                }

                else
                {
                    foreach (tcDeliveryRequests_Condition item in result)
                        result_all.Add(item);
                }


            }

        }

        return result_all;

    }

    }


    public interface IDailyMoneySettleDetail_DA
    {
        
        public void Insert(List<DailyMoneySettleDetail_Condition> data);
        Task<IEnumerable<tcDeliveryRequests_Condition>> GetDailySettleData(List<string> data);
        public long GetKey();
    }


}