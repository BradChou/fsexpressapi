﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DAL.Contract_DA
{
    public class ProductValuationManage_DA : IProductValuationManage_DA
    {
        private readonly DBconn _dbconn;

        public ProductValuationManage_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<List<ProductValuationManage_Condition>> GetpublicAll()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[ProductValuationManage] WITH (NOLOCK) WHERE IsActive = 1
                    ";
                var result = (await _conn.QueryAsync<ProductValuationManage_Condition>(sql)).ToList();
                return result;
            }
        }

        public async Task<List<ProductValuationManage_Condition>> SearchInfo(ProductValuationManage_Condition data)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sqlQry = @"
SELECT *
  FROM [dbo].[ProductValuationManage]
WHERE 1 = 1
";
                if (!string.IsNullOrEmpty(data.CustomerCode))
                {
                    sqlQry += @"AND CustomerCode = @CustomerCode ";
                }
                if (!string.IsNullOrEmpty(data.Spec))
                {
                    sqlQry += @"AND Spec = @Spec ";
                }
                if (data.StartDate != null)
                {
                    sqlQry += @"AND EndDate > @StartDate ";
                }
                if (data.EndDate != null)
                {
                    sqlQry += @"AND StartDate < @EndDate ";
                }
                var result = (await _conn.QueryAsync<ProductValuationManage_Condition>(sqlQry, data)).ToList();
                return result;
            }
        }

        public async Task<ProductValuationManage_Condition> GetMaxProductValuate(string DeliveryType, string CustomerCode, int? pieces)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT top 1 *
                        FROM [dbo].[ProductValuationManage] WITH (NOLOCK) 
                        WHERE 
                        CustomerCode = @CustomerCode
                        AND DeliveryType = @deliverytype
                        AND ProductId like 'CM%' order by spec desc";

                var result = await _conn.QueryFirstOrDefaultAsync<ProductValuationManage_Condition>(sql, new { DeliveryType, CustomerCode, pieces });
                return result;
            }
        }

        public async Task<List<ProductValuationManage_Condition>> GetProductValuateList(string DeliveryType, string CustomerCode, int? pieces)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[ProductValuationManage] WITH (NOLOCK) 
                        WHERE 
                        CustomerCode = @CustomerCode
                        AND DeliveryType = @deliverytype
                        AND ProductId like 'CM%' order by spec desc";
               
                var result = (await _conn.QueryAsync<ProductValuationManage_Condition>(sql, new { DeliveryType, CustomerCode, pieces })).ToList();
                return result;
            }
        }
    }


    public interface IProductValuationManage_DA
    {
        Task<List<ProductValuationManage_Condition>> GetpublicAll();
        Task<List<ProductValuationManage_Condition>> SearchInfo(ProductValuationManage_Condition data);
        Task<ProductValuationManage_Condition> GetMaxProductValuate(string DeliveryType, string CustomerCode, int? pieces);
        Task<List<ProductValuationManage_Condition>> GetProductValuateList(string DeliveryType, string CustomerCode, int? pieces);

    }
}

