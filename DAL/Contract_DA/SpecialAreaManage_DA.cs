﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class SpecialAreaManage_DA : ISpecialAreaManage_DA
    {
        private readonly DBconn _dbconn;

        public SpecialAreaManage_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<List<SpecialAreaManage_Condition>> GetAll()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[SpecialAreaManage] WITH (NOLOCK)
                        WHERE City <> '' AND Area <> '' AND [IsActive] = 1
                    ";
                var result = (await _conn.QueryAsync<SpecialAreaManage_Condition>(sql)).ToList();
                return result;
            }
        }
    }
    public interface ISpecialAreaManage_DA
    {
        Task<List<SpecialAreaManage_Condition>> GetAll();
    }
}
