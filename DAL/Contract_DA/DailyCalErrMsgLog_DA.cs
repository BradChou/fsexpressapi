﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class DailyCalErrMsgLog_DA: IDailyCalErrMsgLog_DA
    {

        private readonly DBconn _dbconn;

        public DailyCalErrMsgLog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task InsertBatch(List<DailyCalErrMsgLog_Condition> insertList)
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                try
                {
                    string sql =
                    @"  
                    INSERT into [dbo].[DailyCalErrMsgLog]
                    (
                      
	                  [DailyMoneySettleDetailId]
                      ,[CheckNumber]
                      ,[ProductId]
                      ,[CBM]
                      ,[ErrMsg]
                      ,[CreateDate]

                    ) VALUES
                    (   
                      @DailyMoneySettleDetailId
                      ,@CheckNumber
                      ,@ProductId
                      ,@CBM
                      ,@ErrMsg
                      ,@CreateDate
                    )
                    ";
                    await _conn.ExecuteAsync(sql, insertList);
                }
                catch (Exception e)
                {

                }


            }
        }
    }

    public interface IDailyCalErrMsgLog_DA
    {
        public Task InsertBatch(List<DailyCalErrMsgLog_Condition> insertList);
    }

}
