﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class CBM_Detail_DA: ICBM_Detail_DA
    {
        private readonly DBconn _dbconn;

        public CBM_Detail_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<List<CBM_Detail_Condition>> GetInfoById(long id)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[CBM_Detail] WITH (NOLOCK) WHERE DetailId = @id 
                    ";
                var result = (await _conn.QueryAsync<CBM_Detail_Condition>(sql, new { id })).ToList();
                return result;
            }
        }

        public async Task<List<CBM_Detail_Condition>> GetInfoTotal(long id)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[CBM_Detail] WITH (NOLOCK) WHERE DetailId >= @id 
                    ";
                var result = (await _conn.QueryAsync<CBM_Detail_Condition>(sql, new { id })).ToList();
                return result;

            }
        }

        public async Task Insert(CBM_Detail_Condition data)
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                try
                {
                    string sql =
                    @"  
                    INSERT into [dbo].[CBM_Detail] 
                    (
                      
	                  [DetailID],
	                  [SN],
	                  [CBM],
	                  [Length],
	                  [Width],
	                  [Height],
	                  [CreateDate],
	                  [CreateUser],
                      [OverCBM]

                    ) VALUES
                    (   
                      @DetailID,
	                  @SN,
	                  @CBM,
	                  @Length,
	                  @Width,
	                  @Height,
	                  @CreateDate,
	                  @CreateUser,
                      @OverCBM
                    )
                    ";
                    await _conn.ExecuteAsync(sql, data);
                }
                catch(Exception e)
                {

                }
                

            }

        }

        public async Task Insert(List<CBM_Detail_Condition> data)
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                try
                {
                    string sql =
                    @"  
                    INSERT into [dbo].[CBM_Detail] 
                    (
                      
	                  [DetailID],
	                  [SN],
	                  [CBM],
	                  [Length],
	                  [Width],
	                  [Height],
	                  [CreateDate],
	                  [CreateUser],
                      [OverCBM]

                    ) VALUES
                    (   
                      @DetailID,
	                  @SN,
	                  @CBM,
	                  @Length,
	                  @Width,
	                  @Height,
	                  @CreateDate,
	                  @CreateUser,
                      @OverCBM
                    )
                    ";
                    await _conn.ExecuteAsync(sql, data,null,500);
                }
                catch (Exception e)
                {

                }


            }

        }

        public async Task ListInsert(List<CBM_Detail_Condition> data)
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                try
                {
                    string sql =
                    @"  
                    INSERT into [dbo].[CBM_Detail] 
                    (
                      
	                  [DetailID],
	                  [SN],
	                  [CBM],
	                  [Length],
	                  [Width],
	                  [Height],
	                  [CreateDate],
	                  [CreateUser],
                      [OverCBM]

                    ) VALUES
                    (   
                      @DetailID,
	                  @SN,
	                  @CBM,
	                  @Length,
	                  @Width,
	                  @Height,
	                  @CreateDate,
	                  @CreateUser,
                      @OverCBM
                    )
                    ";
                    await _conn.ExecuteAsync(sql, data);
                }
                catch (Exception e)
                {

                }


            }
        }

    }
    public interface ICBM_Detail_DA
    {
        Task<List<CBM_Detail_Condition>> GetInfoById(long id);

        Task<List<CBM_Detail_Condition>> GetInfoTotal(long id);

        public Task Insert(CBM_Detail_Condition data);

        public Task Insert(List<CBM_Detail_Condition> data);

    }
}
