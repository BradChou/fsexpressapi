﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DAL.Contract_DA
{
    public class MoneySettleAScope_DA: IMoneySettleAScope_DA
    {

        private readonly DBconn _dbconn;

        public MoneySettleAScope_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<List<MoneySettleAScope_Condition>> GetpublicAll()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MoneySettleAScope] WITH (NOLOCK)
                    ";
                var result = (await _conn.QueryAsync<MoneySettleAScope_Condition>(sql)).ToList();
                return result;
            }
        }
    }
    public interface IMoneySettleAScope_DA
    {
        Task<List<MoneySettleAScope_Condition>> GetpublicAll();
    }
}
