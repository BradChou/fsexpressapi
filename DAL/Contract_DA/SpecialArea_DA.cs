﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DAL.Contract_DA
{
    public class SpecialArea_DA: ISpecialArea_DA
    {
        private readonly DBconn _dbconn;

        public SpecialArea_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<List<SpecialArea_Condition>> GetPublicAll()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[SpecialArea] WITH (NOLOCK)
                    ";
                var result = (await _conn.QueryAsync<SpecialArea_Condition>(sql)).ToList();
                return result;
            }
        }

    }
    public interface ISpecialArea_DA
    {
        Task<List<SpecialArea_Condition>> GetPublicAll();
    }
}
