﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class ItemCodes_DA: IItemCodes_DA
    {
        private readonly DBconn _dbconn;

        public ItemCodes_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        /// <summary>
        /// GetSpecCodeId
        /// </summary>
        /// <returns></returns>
        public async Task<List<ItemCodes_Conditions>> GetSpecCodeId()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  select * from ItemCodes where CodeType = 'ProductSpec'
                    ";
                var result = (await _conn.QueryAsync<ItemCodes_Conditions>(sql)).ToList();
                return result;
            }
        }

        /// <summary>
        /// Get ItemCodes
        /// </summary>
        /// <returns></returns>
        public async Task<List<ItemCodes_Condition>> GetInfo(string CodeType, string SystemType = "FSEContractSystem")
        {
            string sqlQry = @"
SELECT *
  FROM [dbo].[ItemCodes] WITH (NOLOCK)
 WHERE CodeType = @CodeType AND SystemType = @SystemType
ORDER BY OrderBy
";

            using (var _conn = _dbconn.CreateContractConnection())
            {
                try
                {
                    return (await _conn.QueryAsync<ItemCodes_Condition>(sqlQry, new { CodeType, SystemType })).ToList();
                }
                catch (Exception e)
                {

                }
                return null;
            }
        }
    }
    public interface IItemCodes_DA
    {
        Task<List<ItemCodes_Conditions>> GetSpecCodeId();
        Task<List<ItemCodes_Condition>> GetInfo(string CodeType, string SystemType = "FSEContractSystem");
    }
}
