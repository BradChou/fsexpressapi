﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DAL.Contract_DA
{
    public class ProductManage_DA: IProductManage_DA
    {
        private readonly DBconn _dbconn;

        public ProductManage_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<List<ProductManage_Condition>> GetpublicAll()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[ProductManage] WITH (NOLOCK) WHERE IsActive = 1
                    ";
                var result = (await _conn.QueryAsync<ProductManage_Condition>(sql)).ToList();
                return result;
            }
        }
        public async Task<List<ProductManage_Condition>> GetCBMProduct()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT * 
                    FROM [FSE01].[dbo].[ProductManage] WITH (NOLOCK)  
                    where ProductId in('CS000042','CM000043','CM000030') AND IsActive = 1
                    ";
                var result = (await _conn.QueryAsync<ProductManage_Condition>(sql)).ToList();
                return result;
            }
        }
    }
    public interface IProductManage_DA
    {
        Task<List<ProductManage_Condition>> GetpublicAll();
        Task<List<ProductManage_Condition>> GetCBMProduct();
    }
}
