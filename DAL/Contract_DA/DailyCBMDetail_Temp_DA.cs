﻿using Common;
using DAL.Model.Condition;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class DailyCBMDetail_Temp_DA : IDailyCBMDetail_Temp_DA
    {
        private readonly DBconn _dbconn;

        public DailyCBMDetail_Temp_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<List<DailyCBMDetail_Temp_Condition>> GetInfoById(long id)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[CBM_Detail] WITH (NOLOCK) WHERE DetailId = @id 
                    ";
                var result = (await _conn.QueryAsync<DailyCBMDetail_Temp_Condition>(sql, new { id })).ToList();
                return result;
            }
        }

        public async Task<List<DailyCBMDetail_Temp_Condition>> GetInfoTotal()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[DailyCBMDetail_Temp] WITH (NOLOCK)
                    ";
                var result = (await _conn.QueryAsync<DailyCBMDetail_Temp_Condition>(sql)).ToList();
                return result;

            }
        }

        public async Task InsertTemp(List<DailyCBMDetail_Temp_Condition> data)
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                try
                {
                    string sql =
                    @"  
                    INSERT into [DailyCBMDetail_Temp] 
                    (
                      
	                  [DetailID],
	                  [SN],
	                  [CBM],
	                  [Length],
	                  [Width],
	                  [Height],
	                  [CreateDate],
	                  [CreateUser],
                      [OverCBM]

                    ) VALUES
                    (   
                      @DetailID,
	                  @SN,
	                  @CBM,
	                  @Length,
	                  @Width,
	                  @Height,
	                  @CreateDate,
	                  @CreateUser,
                      @OverCBM
                    )
                    ";
                    await _conn.ExecuteAsync(sql, data,null,500);
                }
                catch (Exception e)
                {

                }


            }

        }

        public void Delete()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  truncate table [dbo].[DailyCBMDetail_Temp]
                    ";
                _conn.Execute(sql);

            }
        }

    }
    public interface IDailyCBMDetail_Temp_DA
    {
        Task<List<DailyCBMDetail_Temp_Condition>> GetInfoById(long id);

        Task<List<DailyCBMDetail_Temp_Condition>> GetInfoTotal();

        public Task InsertTemp(List<DailyCBMDetail_Temp_Condition> data);

        public void Delete();

    }
}
