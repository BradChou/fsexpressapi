﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class DailySettleMain_DA : IDailySettleMain_DA
    {
        private readonly DBconn _dbconn;

        public DailySettleMain_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(DailySettleMain_Condition data)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @" INSERT INTO [dbo].[DailySettleMain]
           ([RequestId]
           ,[CheckNumber]
           ,[YYYYMMDD]
           ,[CreateStation]
           ,[CreateUser]
           ,[ReviseStation]
           ,[ReviseUser]
           ,[ReviseDate]
           ,[ShipDate]
           ,[SendStationScode]
           ,[SendBusinessStationCode]
           ,[SendBusinessStationBlockCode]
           ,[SendSD]
           ,[SendMD]
           ,[CarNumber]
           ,[NowSDNumber]
           ,[InputSource]
           ,[CommodityType]
           ,[CodeName]
           ,[CustomerCode]
           ,[CustomerSecondCode]
           ,[CustomerCheckCode]
           ,[AreaArriveCode]
           ,[ArriveBusinessStationCode]
           ,[SendTelAreacode]
           ,[SendTel]
           ,[SendContact]
           ,[SendCity]
           ,[SendArea]
           ,[SendAddress]
           ,[ReceiveTelAreacode]
           ,[ReceiveTel]
           ,[ReceiveContact]
           ,[ReceiveCity]
           ,[ReceiveArea]
           ,[ReceiveAddress]
           ,[TotalWeight]
           ,[Pieces]
           ,[HandWritingFee]
           ,[HandWritingFlag]
           ,[DD_Fee]
           ,[OrderFeeFlag]
           ,[OrderFee]
           ,[ShipFeeFlag]
           ,[ShipFee]
           ,[SendFeeFlag]
           ,[SendFee]
           ,[SpecialAreaFlag]
           ,[SpecialAreaFee]
           ,[IntermodalFlag]
           ,[IntermodalFee]
           ,[RefrigeratedDeliveryFlag]
           ,[RefrigeratedDeliveryFee]
           ,[HolidayDeliveryFlag]
           ,[HolidayDeliveryFee]
           ,[TimePeriodFlag]
           ,[TimePeriodFee]
           ,[BigThingsFlag]
           ,[BigThingsFee]
           ,[ReceiptFlag]
           ,[ReceiptFlagFee]
           ,[OtherSpecialFlag]
           ,[OtherSpecialFee]
           ,[CollectionMoney]
           ,[CollectionMoneyFlag]
           ,[CollectionMoneyFee]
           ,[ReimbursementFee]
           ,[ReimbursementFlag]
           ,[ReimbursementPremiumFee]
           ,[TotalFee]
           ,[AmountRequestedFee]
           ,[CombinedTransportFeeFlag]
           ,[CombinedTransportFee]
           ,[OtherPrepaidFee]
           ,[DispatchFee]
           ,[CollectionStationFee]
           ,[CollectionGoodsType]
           ,[PickupFeeType]
           ,[SpecialDeliveryType]
           ,[OrderNumber]
           ,[ReturnCheckNumber]
           ,[ReceiveZipCode]
           ,[ReceiveSpecialFeeFlag]
           ,[ReceiveCombinedTransportFeeFlag]
           ,[ReceiveSpecialFeeType]
           ,[SendZipCode]
           ,[SendCombinedTransportFeeFlag]
           ,[CountType]
           ,[CheckoutStatus]
           ,[BillOfficeCustomerMainCode]
           ,[BillOfficeCustomerSecondCode]
           ,[BillOfficeCustomerCheckCode]
           ,[IntegrateCustomerMainCode]
           ,[IntegrateCustomerSecondCode]
           ,[IntegrateCustomerCheckCode]
           ,[CheckoutNumber]
           ,[CheckoutDate]
           ,[CheckoutCdate]
           ,[ArrivalCollectMoneyType]
           ,[IsArrived]
           ,[CollectionAmountProcessingStatus]
           ,[CancelFlag]
           ,[ConsignmentTrackingFlag]
           ,[ConsignmentTrackingPaymentFlag]
           ,[ConsignmentEDIReplyFlag]
           ,[ConsignmentEDIPaymentReplyFlag]
           ,[ArrivalArea]
           ,[UpdateUser]
           ,[UpdateDate]
           ,[UpdateTime]
           ,[ProductId]
           ,[SpecCodeId]
           ,[OversCBM]
           ,[ASettleFee]
           ,[BSettleFee]
           ,[CSettleFee]
           ,[RoundtripFlag]
,[DeliveryCreateDate]
,[DeliveryCreateUser]
,[EastFee])
     VALUES
           (@RequestId
           ,@CheckNumber
           ,@YYYYMMDD
           ,@CreateStation
           ,@CreateUser
           ,@ReviseStation
           ,@ReviseUser
           ,@ReviseDate
           ,@ShipDate
           ,@SendStationScode
           ,@SendBusinessStationCode
           ,@SendBusinessStationBlockCode
           ,@SendSD
           ,@SendMD
           ,@CarNumber
           ,@NowSDNumber
           ,@InputSource
           ,@CommodityType
           ,@CodeName
           ,@CustomerCode
           ,@CustomerSecondCode
           ,@CustomerCheckCode
           ,@AreaArriveCode
           ,@ArriveBusinessStationCode
           ,@SendTelAreacode
           ,@SendTel
           ,@SendContact
           ,@SendCity
           ,@SendArea
           ,@SendAddress
           ,@ReceiveTelAreacode
           ,@ReceiveTel
           ,@ReceiveContact
           ,@ReceiveCity
           ,@ReceiveArea
           ,@ReceiveAddress
           ,@TotalWeight
           ,@Pieces
           ,@HandWritingFee
           ,@HandWritingFlag
           ,@DD_Fee
           ,@OrderFeeFlag
           ,@OrderFee
           ,@ShipFeeFlag
           ,@ShipFee
           ,@SendFeeFlag
           ,@SendFee
           ,@SpecialAreaFlag
           ,@SpecialAreaFee
           ,@IntermodalFlag
           ,@IntermodalFee
           ,@RefrigeratedDeliveryFlag
           ,@RefrigeratedDeliveryFee
           ,@HolidayDeliveryFlag
           ,@HolidayDeliveryFee
           ,@TimePeriodFlag
           ,@TimePeriodFee
           ,@BigThingsFlag
           ,@BigThingsFee
           ,@ReceiptFlag
           ,@ReceiptFlagFee
           ,@OtherSpecialFlag
           ,@OtherSpecialFee
           ,@CollectionMoney
           ,@CollectionMoneyFlag
           ,@CollectionMoneyFee
           ,@ReimbursementFee
           ,@ReimbursementFlag
           ,@ReimbursementPremiumFee
           ,@TotalFee
           ,@AmountRequestedFee
           ,@CombinedTransportFeeFlag
           ,@CombinedTransportFee
           ,@OtherPrepaidFee
           ,@DispatchFee
           ,@CollectionStationFee
           ,@CollectionGoodsType
           ,@PickupFeeType
           ,@SpecialDeliveryType
           ,@OrderNumber
           ,@ReturnCheckNumber
           ,@ReceiveZipCode
           ,@ReceiveSpecialFeeFlag
           ,@ReceiveCombinedTransportFeeFlag
           ,@ReceiveSpecialFeeType
           ,@SendZipCode
           ,@SendCombinedTransportFeeFlag
           ,@CountType
           ,@CheckoutStatus
           ,@BillOfficeCustomerMainCode
           ,@BillOfficeCustomerSecondCode
           ,@BillOfficeCustomerCheckCode
           ,@IntegrateCustomerMainCode
           ,@IntegrateCustomerSecondCode
           ,@IntegrateCustomerCheckCode
           ,@CheckoutNumber
           ,@CheckoutDate
           ,@CheckoutCdate
           ,@ArrivalCollectMoneyType
           ,@IsArrived
           ,@CollectionAmountProcessingStatus
           ,@CancelFlag
           ,@ConsignmentTrackingFlag
           ,@ConsignmentTrackingPaymentFlag
           ,@ConsignmentEDIReplyFlag
           ,@ConsignmentEDIPaymentReplyFlag
           ,@ArrivalArea
           ,@UpdateUser
           ,@UpdateDate
           ,@UpdateTime
           ,@ProductId
           ,@SpecCodeId
           ,@OversCBM
           ,@ASettleFee
           ,@BSettleFee
           ,@CSettleFee
           ,@RoundtripFlag
,@DeliveryCreateDate
,@DeliveryCreateUser
,@EastFee
		   )
                    ";

               await _conn.ExecuteAsync(sql, data);
            }
        }

        public async Task Update(DailySettleMain_Condition data)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  UPDATE  [dbo].[DailySettleMain]
  SET 
  [RequestId] = @RequestId, 
  [CheckNumber] = @CheckNumber, 
  [YYYYMMDD] = @YYYYMMDD, 
  [CreateStation] = @CreateStation, 
  [ReviseStation] = @ReviseStation, 
  [ReviseUser] = @ReviseUser, 
  [ReviseDate] = @ReviseDate, 
  [ShipDate] = @ShipDate, 
  [SendStationScode] = @SendStationScode, 
  [SendBusinessStationCode] = @SendBusinessStationCode, 
  [SendBusinessStationBlockCode] = @SendBusinessStationBlockCode, 
  [SendSD] = @SendSD, 
  [SendMD] = @SendMD, 
  [CarNumber] = @CarNumber, 
  [NowSDNumber] = @NowSDNumber, 
  [InputSource] = @InputSource, 
  [CommodityType] = @CommodityType, 
  [CodeName] = @CodeName, 
  [CustomerCode] = @CustomerCode, 
  [CustomerSecondCode] = @CustomerSecondCode, 
  [CustomerCheckCode] = @CustomerCheckCode, 
  [AreaArriveCode] = @AreaArriveCode, 
  [ArriveBusinessStationCode] = @ArriveBusinessStationCode, 
  [SendTelAreacode] = @SendTelAreacode, 
  [SendTel] = @SendTel, 
  [SendContact] = @SendContact, 
  [SendCity] = @SendCity, 
  [SendArea] = @SendArea, 
  [SendAddress] = @SendAddress, 
  [ReceiveTelAreacode] = @ReceiveTelAreacode, 
  [ReceiveTel] = @ReceiveTel, 
  [ReceiveContact] = @ReceiveContact, 
  [ReceiveCity] = @ReceiveCity, 
  [ReceiveArea] = @ReceiveArea, 
  [ReceiveAddress] = @ReceiveAddress, 
  [TotalWeight] = @TotalWeight, 
  [Pieces] = @Pieces, 
  [HandWritingFee] = @HandWritingFee, 
  [HandWritingFlag] = @HandWritingFlag, 
  [DD_Fee] = @DD_Fee, 
  [OrderFeeFlag] = @OrderFeeFlag, 
  [OrderFee] = @OrderFee, 
  [ShipFeeFlag] = @ShipFeeFlag, 
  [ShipFee] = @ShipFee, 
  [SendFeeFlag] = @SendFeeFlag, 
  [SendFee] = @SendFee, 
  [SpecialAreaFlag] = @SpecialAreaFlag, 
  [SpecialAreaFee] = @SpecialAreaFee, 
  [IntermodalFlag] = @IntermodalFlag, 
  [IntermodalFee] = @IntermodalFee, 
  [RefrigeratedDeliveryFlag] = @RefrigeratedDeliveryFlag, 
  [RefrigeratedDeliveryFee] = @RefrigeratedDeliveryFee, 
  [HolidayDeliveryFlag] = @HolidayDeliveryFlag, 
  [HolidayDeliveryFee] = @HolidayDeliveryFee, 
  [TimePeriodFlag] = @TimePeriodFlag, 
  [TimePeriodFee] = @TimePeriodFee, 
  [BigThingsFlag] = @BigThingsFlag, 
  [BigThingsFee] = @BigThingsFee, 
  [ReceiptFlag] = @ReceiptFlag, 
  [ReceiptFlagFee] = @ReceiptFlagFee, 
  [OtherSpecialFlag] = @OtherSpecialFlag, 
  [OtherSpecialFee] = @OtherSpecialFee, 
  [CollectionMoney] = @CollectionMoney, 
  [CollectionMoneyFlag] = @CollectionMoneyFlag, 
  [CollectionMoneyFee] = @CollectionMoneyFee, 
  [ReimbursementFee] = @ReimbursementFee, 
  [ReimbursementFlag] = @ReimbursementFlag, 
  [ReimbursementPremiumFee] = @ReimbursementPremiumFee, 
  [TotalFee] = @TotalFee, 
  [AmountRequestedFee] = @AmountRequestedFee, 
  [CombinedTransportFeeFlag] = @CombinedTransportFeeFlag, 
  [CombinedTransportFee] = @CombinedTransportFee, 
  [OtherPrepaidFee] = @OtherPrepaidFee, 
  [DispatchFee] = @DispatchFee, 
  [CollectionStationFee] = @CollectionStationFee, 
  [CollectionGoodsType] = @CollectionGoodsType, 
  [PickupFeeType] = @PickupFeeType, 
  [SpecialDeliveryType] = @SpecialDeliveryType, 
  [OrderNumber] = @OrderNumber, 
  [ReturnCheckNumber] = @ReturnCheckNumber, 
  [ReceiveZipCode] = @ReceiveZipCode, 
  [ReceiveSpecialFeeFlag] = @ReceiveSpecialFeeFlag, 
  [ReceiveCombinedTransportFeeFlag] = @ReceiveCombinedTransportFeeFlag, 
  [ReceiveSpecialFeeType] = @ReceiveSpecialFeeType, 
  [SendZipCode] = @SendZipCode, 
  [SendCombinedTransportFeeFlag] = @SendCombinedTransportFeeFlag, 
  [CountType] = @CountType, 
  [CheckoutStatus] = @CheckoutStatus, 
  [BillOfficeCustomerMainCode] = @BillOfficeCustomerMainCode, 
  [BillOfficeCustomerSecondCode] = @BillOfficeCustomerSecondCode, 
  [BillOfficeCustomerCheckCode] = @BillOfficeCustomerCheckCode, 
  [IntegrateCustomerMainCode] = @IntegrateCustomerMainCode, 
  [IntegrateCustomerSecondCode] = @IntegrateCustomerSecondCode, 
  [IntegrateCustomerCheckCode] = @IntegrateCustomerCheckCode, 
  [CheckoutNumber] = @CheckoutNumber, 
  [CheckoutDate] = @CheckoutDate, 
  [CheckoutCdate] = @CheckoutCdate, 
  [ArrivalCollectMoneyType] = @ArrivalCollectMoneyType, 
  [IsArrived] = @IsArrived, 
  [CollectionAmountProcessingStatus] = @CollectionAmountProcessingStatus, 
  [CancelFlag] = @CancelFlag, 
  [ConsignmentTrackingFlag] = @ConsignmentTrackingFlag, 
  [ConsignmentTrackingPaymentFlag] = @ConsignmentTrackingPaymentFlag, 
  [ConsignmentEDIReplyFlag] = @ConsignmentEDIReplyFlag, 
  [ConsignmentEDIPaymentReplyFlag] = @ConsignmentEDIPaymentReplyFlag, 
  [ArrivalArea] = @ArrivalArea, 
  [UpdateUser] = @UpdateUser, 
  [UpdateDate] = @UpdateDate, 
  [UpdateTime] = @UpdateTime, 
  [ProductId] = @ProductId, 
  [SpecCodeId] = @SpecCodeId, 
  [OversCBM] = @OversCBM, 
  [ASettleFee] = @ASettleFee, 
  [BSettleFee] = @BSettleFee, 
  [CSettleFee] = @CSettleFee, 
  [RoundtripFlag] = @RoundtripFlag,
[DeliveryCreateDate] = @DeliveryCreateDate,
[DeliveryCreateUser]=@DeliveryCreateUser,
[EastFee]=@EastFee
                        WHERE Id =@Id
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }


    }
    public interface IDailySettleMain_DA
    {
        Task Insert(DailySettleMain_Condition data);
        Task Update(DailySettleMain_Condition data);

    }
}
