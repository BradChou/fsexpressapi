﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class SpecialAreaAudit_DA : ISpecialAreaAudit_DA
    {
        private readonly DBconn _dbconn;

        public SpecialAreaAudit_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<IEnumerable<SpecialAreaAudit_Condition>> GetSpecialAreaAudit()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[SpecialAreaAudit] WITH (NOLOCK)
                        
                    ";
                var result = await _conn.QueryAsync<SpecialAreaAudit_Condition>(sql);
                return result;
            }
        }
        public async Task<IEnumerable<SpecialAreaAudit_Condition>> GetListByRequestId(List<long> RequestId)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[SpecialAreaAudit] WITH (NOLOCK)
                        WHERE RequestId in @RequestId AND Status = '1'
                        
                    ";
                var result = await _conn.QueryAsync<SpecialAreaAudit_Condition>(sql,new { RequestId });
                return result;
            }
        }
    }
    public interface ISpecialAreaAudit_DA
    {
        Task<IEnumerable<SpecialAreaAudit_Condition>> GetSpecialAreaAudit();
        Task<IEnumerable<SpecialAreaAudit_Condition>> GetListByRequestId(List<long> RequestId);
    }
}
