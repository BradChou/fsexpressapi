﻿using Common;
using System;
using System.Collections.Generic;
using DAL.Model.Condition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using BLL.Model.ScheduleAPI.Req.DailySettlement;

namespace DAL.DA
{
    public class DailyMoneySettleDetail_Temp_DA : IDailyMoneySettleDetail_Temp_DA
    {
        private readonly DBconn _dbconn;

        public DailyMoneySettleDetail_Temp_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public void InsertTemp(List<DailyMoneySettleDetail_Temp_Condition> data)
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                string sql =
                    @"  
                    INSERT into [dbo].[DailyMoneySettleDetail_Temp] 
                    (
                       [Id]
                      ,[RequestId]
                      ,[DeliveryType]
                      ,[CheckNumber]
                      ,[CreateDate]
                      ,[CustomerCode]
                      ,[SendStation]
                      ,[ArriveStation]
                      ,[City]
                      ,[Area]
                      ,[Address]
                      ,[ProductId]
                      ,[Pieces]
                      ,[ReceiptFlag]
                      ,[CollectionMoney]
                      ,[Valued]
                      ,[OverCBM]
                      ,[RoundTrip]
                      ,[SpecialAreaFee]
                      ,[EggArea]
                      ,[PrintDate]
                      ,[ShipDate]
                      ,[OrderNumber]
                      ,[CustomerName]
                      ,[ReceiveContact]
                      ,[InvoiceDesc]
                      ,[ScanDate]
                      ,[CreateUser]
                      ,[Request_CreateDate]
                      ,[SpecialAreaId]
                      ,[DriverCode]
                      ,[ArriveDriverStation]
                      ,[CustomerStation]
                      ,[ArriveOption]
                      ,[ScanLogCdate]
                      ,[ReceiveDriverStation]
                      ,[ReceiveDriverCode]
                      ,[isEast]
                    )
                    VALUES
                    (   
                       @Id
                      ,@RequestId
                      ,@DeliveryType
                      ,@CheckNumber
                      ,@CreateDate
                      ,@CustomerCode
                      ,@SendStation
                      ,@ArriveStation
                      ,@City
                      ,@Area
                      ,@Address
                      ,@ProductId
                      ,@Pieces
                      ,@ReceiptFlag
                      ,@CollectionMoney
                      ,@Valued
                      ,@OverCBM
                      ,@RoundTrip
                      ,@SpecialAreaFee
                      ,@EggArea
                      ,@PrintDate
                      ,@ShipDate
                      ,@OrderNumber
                      ,@CustomerName
                      ,@ReceiveContact
                      ,@InvoiceDesc
                      ,@ScanDate
                      ,@CreateUser
                      ,@Request_CreateDate
                      ,@SpecialAreaId
                      ,@DriverCode
                      ,@ArriveDriverStation
                      ,@CustomerStation
                      ,@ArriveOption
                      ,@ScanLogCdate 
                      ,@ReceiveDriverStation
                      ,@ReceiveDriverCode
                      ,@isEast
                    )
                    ";
                _conn.Execute(sql, data,null,500);
            }

        }

        public async Task<List<DailyMoneySettleDetail_Temp_Condition>> GetAll()
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                string sql =
                    @"  
                    select * from  [dbo].[DailyMoneySettleDetail_Temp] 
                    ";
                var result = (await _conn.QueryAsync<DailyMoneySettleDetail_Temp_Condition>(sql)).ToList();
                return result;
            }

        }

        public void Delete()
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                string sql =
                    @"  
                    truncate table [dbo].[DailyMoneySettleDetail_Temp]
                    ";
                _conn.Execute(sql);
            }

        }



    }


    public interface IDailyMoneySettleDetail_Temp_DA
    {

        public void InsertTemp(List<DailyMoneySettleDetail_Temp_Condition> data);
        public Task<List<DailyMoneySettleDetail_Temp_Condition>> GetAll();
        public void Delete();

    }


}