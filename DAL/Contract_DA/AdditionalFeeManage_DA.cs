﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DAL.Contract_DA
{
    public class AdditionalFeeManage_DA: IAdditionalFeeManage_DA
    {
        private readonly DBconn _dbconn;

        public AdditionalFeeManage_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<List<AdditionalFeeManage_Condition>> GetPublicAll()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[AdditionalFeeManage] WITH (NOLOCK) WHERE IsActive = 1 
                    ";
                var result = (await _conn.QueryAsync<AdditionalFeeManage_Condition>(sql)).ToList();
                return result;
            }
        }
        public async Task<List<AdditionalFeeManage_Condition>> GetInfoByCustomer(string customerCode)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[AdditionalFeeManage] WITH (NOLOCK) WHERE CustomerCode = @customerCode 
                    ";
                var result = (await _conn.QueryAsync<AdditionalFeeManage_Condition>(sql, new { customerCode })).ToList();
                return result;
            }
        }

    }

    public interface IAdditionalFeeManage_DA
    {
        Task<List<AdditionalFeeManage_Condition>> GetPublicAll();
        Task<List<AdditionalFeeManage_Condition>> GetInfoByCustomer(string customerCode);
    }
}
