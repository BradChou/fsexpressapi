﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class DailySettleTodoList_DA : IDailySettleTodoList_DA
    {
        private readonly DBconn _dbconn;

        public DailySettleTodoList_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task Insert(DailySettleTodoList_Condition data)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[DailySettleTodoList]
                           ([RequestId]
                           ,[CheckNumber]
                           ,[ActionType]
                           ,[IsDo]
                        )
                     VALUES
                           (@RequestId
                           ,@CheckNumber
                           ,@ActionType
                           ,@IsDo
                      )
                    ";
                try
                {
                    await _conn.ExecuteAsync(sql, data);
                }
                catch (Exception)
                {


                }



            }
        }

        public async Task<IEnumerable<DailySettleTodoList_Condition>> GetNODO()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"                        
SELECT *
FROM DailySettleTodoList
WHERE IsDo = 0

                    ";
                return await _conn.QueryAsync<DailySettleTodoList_Condition>(sql);

            }
        }


        public async Task<IEnumerable<DailySettleTodoList_Condition>> GetInfoById(long Id)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"                        
SELECT *
FROM DailySettleTodoList
WHERE Id > @Id

                    ";
                return await _conn.QueryAsync<DailySettleTodoList_Condition>(sql, new { Id });

            }
        }

        public async Task Update(List<long> Ids)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  UPDATE  [dbo].[DailySettleTodoList]
                        SET IsDo = 1
                        WHERE Id in @Id

                    ";
                await _conn.ExecuteAsync(sql, new { Ids });

            }
        }

        public async Task Update(long Id)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  UPDATE  [dbo].[DailySettleTodoList]
                        SET IsDo = 1
                        WHERE Id = @Id

                    ";
                await _conn.ExecuteAsync(sql, new { Id });

            }
        }


    }
    public interface IDailySettleTodoList_DA
    {
        Task Insert(DailySettleTodoList_Condition data);
        Task<IEnumerable<DailySettleTodoList_Condition>> GetNODO();
        Task<IEnumerable<DailySettleTodoList_Condition>> GetInfoById(long Id);
    }
}
