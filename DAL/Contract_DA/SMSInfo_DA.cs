﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class SMSInfo_DA : ISMSInfo_DA
    {
        private readonly DBconn _dbconn;

        public SMSInfo_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<long> InsertReturnId(SMSInfo_Condition insertData)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @" INSERT INTO [dbo].[SMSInfo]
           ([Mobile]
           ,[Title]
           ,[Content]
           ,[Status]
         
          
           ,[CreateUser]
           ,[UpdateUser])
OUTPUT Inserted.Id
     VALUES
           (@Mobile
           ,@Title
           ,@Content
           ,@Status
          
           ,@CreateUser
           ,@UpdateUser)
                    ";

                return await _conn.QueryFirstOrDefaultAsync<long>(sql, insertData);
            }
        }

        public async Task<List<SMSInfo_Condition>> GetNotSendList()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[SMSInfo]
                        WHERE Status =1
                    ";
                var result = (await _conn.QueryAsync<SMSInfo_Condition>(sql)).ToList();
                return result;
            }
        }
        public async Task Update(SMSInfo_Condition data)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  UPDATE  [dbo].[SMSInfo]
                        SET Status =@Status,UpdateDate=@UpdateDate,UpdateUser=@UpdateUser
                        WHERE Id =@Id
                    ";
                await _conn.ExecuteAsync(sql, data);
        
            }
        }

    }
    public interface ISMSInfo_DA
    {
        Task<long> InsertReturnId(SMSInfo_Condition insertData);
        Task<List<SMSInfo_Condition>> GetNotSendList();

        Task Update(SMSInfo_Condition insertData);
    }
}

