﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class MoneySettleC_DA : IMoneySettleC_DA
    {
        private readonly DBconn _dbconn;

        public MoneySettleC_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<List<MoneySettleC_Condition>> GetpublicAll()
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[MoneySettleC] WITH (NOLOCK) WHERE IsActive = 1
                    ";
                var result = (await _conn.QueryAsync<MoneySettleC_Condition>(sql)).ToList();
                return result;
            }
        }
    }
    public interface IMoneySettleC_DA
    {
        Task<List<MoneySettleC_Condition>> GetpublicAll();
    }

}
