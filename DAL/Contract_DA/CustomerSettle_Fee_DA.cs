﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class CustomerSettle_Fee_DA : ICustomerSettle_Fee_DA
    {
        private readonly DBconn _dbconn;

        public CustomerSettle_Fee_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }


        public async Task Insert(CustomerSettle_Fee_Condition insertData)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[CustomerSettle_Fee]
                           ([Detail_Id]
                           ,[DD_Fee]
                           ,[Collection_Money]
                           ,[ReceiptFlag_Fee]
                           ,[Valued_Fee]
                           ,[OverCbm_Fee]
                           ,[SpecialArea_Fee]
                           ,[East_Fee]
                           ,[Total])
                     VALUES
                           (@Detail_Id
                           ,@DD_Fee
                           ,@Collection_Money
                           ,@ReceiptFlag_Fee
                           ,@Valued_Fee
                           ,@OverCbm_Fee
                           ,@SpecialArea_Fee
                           ,@East_Fee
                           ,@Total)
                    ";
                await _conn.ExecuteAsync(sql, insertData);
                return;
            }
        }
        public async Task InsertBatch(List<CustomerSettle_Fee_Condition> insertList)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[CustomerSettle_Fee]
                           ([Detail_Id]
                           ,[SN]
                           ,[DD_Fee]
                           ,[Collection_Money]
                           ,[ReceiptFlag_Fee]
                           ,[Valued_Fee]
                           ,[OverCbm_Fee]
                           ,[SpecialArea_Fee]
                           ,[East_Fee]
                           ,[Total]
                           ,[CreateDate]
                           ,[CreateUser])
                     VALUES
                           (@Detail_Id
                           ,@SN
                           ,@DD_Fee
                           ,@Collection_Money
                           ,@ReceiptFlag_Fee
                           ,@Valued_Fee
                           ,@OverCbm_Fee
                           ,@SpecialArea_Fee
                           ,@East_Fee
                           ,@Total
                           ,@CreateDate
                           ,@CreateUser)
                    ";
                await _conn.ExecuteAsync(sql, insertList);
                return;
            }
        }
    }

    public interface ICustomerSettle_Fee_DA
    {
        Task Insert(CustomerSettle_Fee_Condition insertData);
        Task InsertBatch(List<CustomerSettle_Fee_Condition> insertList);
    }
}
