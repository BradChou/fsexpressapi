﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DAL.Contract_DA
{
    public class DailySettleB_Fee_Temp_DA : IDailySettleB_Fee_Temp_DA
    {

        private readonly DBconn _dbconn;

        public DailySettleB_Fee_Temp_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }


        public async Task Insert(DailySettleB_Fee_Temp_Condition insertData)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[DailySettleB_Fee_Temp]
                           ([Detail_Id]
                           ,[SN]
                           ,[DD_Fee]
                           ,[Collection_Money]
                           ,[ReceiptFlag_Fee]
                           ,[Valued_Fee]
                           ,[OverCbm_Fee]
                           ,[SpecialArea_Fee]
                           ,[East_Fee]
                           ,[Total]
                           ,[CreateDate]
                           ,[CreateUser])
                     VALUES
                           (@Detail_Id
                           ,@SN
                           ,@DD_Fee
                           ,@Collection_Money
                           ,@ReceiptFlag_Fee
                           ,@Valued_Fee
                           ,@OverCbm_Fee
                           ,@SpecialArea_Fee
                           ,@East_Fee
                           ,@Total
                           ,@CreateDate
                           ,@CreateUser)
                    ";
                await _conn.ExecuteAsync(sql, insertData);
                return;
            }
        }
        public async Task InsertBatch(List<DailySettleB_Fee_Temp_Condition> insertList)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[DailySettleB_Fee_Temp]
                           ([Detail_Id]
                           ,[SN]
                           ,[DD_Fee]
                           ,[Collection_Money]
                           ,[ReceiptFlag_Fee]
                           ,[Valued_Fee]
                           ,[OverCbm_Fee]
                           ,[SpecialArea_Fee]
                           ,[East_Fee]
                           ,[Total]
                           ,[CreateDate]
                           ,[CreateUser])
                     VALUES
                           (@Detail_Id
                           ,@SN
                           ,@DD_Fee
                           ,@Collection_Money
                           ,@ReceiptFlag_Fee
                           ,@Valued_Fee
                           ,@OverCbm_Fee
                           ,@SpecialArea_Fee
                           ,@East_Fee
                           ,@Total
                           ,@CreateDate
                           ,@CreateUser)
                    ";
                await _conn.ExecuteAsync(sql, insertList);
                return;
            }
        }
    }
    public interface IDailySettleB_Fee_Temp_DA
    {
        Task Insert(DailySettleB_Fee_Temp_Condition insertData);
        Task InsertBatch(List<DailySettleB_Fee_Temp_Condition> insertList);
    }
}
