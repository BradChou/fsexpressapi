﻿using Common;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using DAL.Model.Condition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DAL.DA
{
    public class DailyCBMDetail_DA : IDailyCBMDetail_DA
    {
        private readonly DBconn _dbconn;

        public DailyCBMDetail_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<IEnumerable<DailyCBMDetail_Condition>> GetCBMInfo()
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  
Declare @now datetime
Declare @sdate datetime 
Declare @edate datetime 

Set @now = format( getdate() , 'yyyyMM01')
Set @edate = @now
Set @sdate = dateadd( month , -1 , @edate )

                    Select * 
                    from CBMDetail with(nolock)   
                    WHERE CheckNumber in (
										Select distinct check_number From ttDeliveryScanLog with(nolock)
										Where cdate >= @sdate 
										and cdate < @edate    
										)    
           
                    ";
                var result = await _conn.QueryAsync<DailyCBMDetail_Condition>(sql);
                return result;
            }

        }


        //public DailyCBMDetail_Condition(DBconn dBconn)
        //{
        //    _dbconn = dBconn;
        //}
        public async Task Insert(List<DailyCBMDetail_Condition> data)
        {

            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                string sql =
                    @"  

                    INSERT into [DailyCBMDetail] 
                    (
                      
	                  [DetailID],
	                  [SN],
	                  [CBM],
	                  [Length],
	                  [Width],
	                  [Height],
	                  [CreateDate],
	                  [CreateUser],
                      [OverCBM]

                    ) VALUES
                    (   
                      @DetailID,
	                  @SN,
	                  @CBM,
	                  @Length,
	                  @Width,
	                  @Height,
	                  @CreateDate,
	                  @CreateUser,
                      @OverCBM
                    )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }

        }


        public async Task DELETE(string CheckNumber)
        {

            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {

                string sql =
                    @"  
DELETE FROM [dbo].[CBMDetail]
WHERE CheckNumber = @CheckNumber
                    ";
                await _conn.ExecuteAsync(sql, new { CheckNumber });

            }
        }

        public async Task<IEnumerable<DailyCBMDetail_Condition>> GetInfoByCheckNumber(string CheckNumber)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[CBMDetail]  WHERE CheckNumber = @CheckNumber
                    ";
                var result = await _conn.QueryAsync<DailyCBMDetail_Condition>(sql, new { CheckNumber });
                return result;
            }
        }
    }
    public interface IDailyCBMDetail_DA
    {
        Task DELETE(string CheckNumber);
        Task Insert(List<DailyCBMDetail_Condition> data);

        Task<IEnumerable<DailyCBMDetail_Condition>> GetInfoByCheckNumber(string CheckNumber);
        public Task<IEnumerable<DailyCBMDetail_Condition>> GetCBMInfo();
    }
}
