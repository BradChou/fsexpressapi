﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contract_DA
{
    public class SMSUsing_DA : ISMSUsing_DA
    {
        private readonly DBconn _dbconn;

        public SMSUsing_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(SMSUsing_Condition insertData)
        {
            using (var _conn = _dbconn.CreateContractConnection())
            {
                string sql =
                    @" INSERT INTO [dbo].[SMSUsing]
           (
            [SMSId]
           ,[ComeFrom]
           ,[CheckNumber]
           ,[CreateUser]
)
     VALUES
           (@SMSId
           ,@ComeFrom
           ,@CheckNumber
           ,@CreateUser
)
                    ";

                await _conn.ExecuteAsync(sql, insertData);
            }
        }




    }
    public interface ISMSUsing_DA
    {
        Task Insert(SMSUsing_Condition insertData);

    }
}

