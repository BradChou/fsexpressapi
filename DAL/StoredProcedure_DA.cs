﻿using BLL.Model.FSExpressAPI.Model;
using Common;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class StoredProcedure_DA: IStoredProcedure_DA
    {
        private readonly DBconn _dbconn;

        public StoredProcedure_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public async Task<LyAddrFormatReturnModel> GetLY_AddrFormat(string Address)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                return await _conn.QueryFirstOrDefaultAsync<LyAddrFormatReturnModel>("LY_AddrFormat", new { Address }, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<LyGetDistributorReturnModel> GetLY_GetDistributor(string receive_city,string receive_area)
        {
            using (var _conn = _dbconn.CreateJunFuRealConnection())
            {
                return await _conn.QueryFirstOrDefaultAsync<LyGetDistributorReturnModel>("LY_GetDistributor", new { receive_city , receive_area }, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task DailySettleCaculateUpadte()
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                await _conn.ExecuteAsync("sp_DailySettleCaculateUpadte",new { },commandType: CommandType.StoredProcedure);
            }
        }
    }
    public interface IStoredProcedure_DA
    {
        Task<LyAddrFormatReturnModel> GetLY_AddrFormat(string Address);
        Task<LyGetDistributorReturnModel> GetLY_GetDistributor(string receive_city, string receive_area);

        Task DailySettleCaculateUpadte();
    }
}
