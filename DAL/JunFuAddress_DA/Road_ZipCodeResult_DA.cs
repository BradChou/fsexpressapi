﻿using BLL.Model.AddressParsing.Model;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DAL.Model.JunFuTrans_Condition;

namespace DAL.JunFuAddress_DA
{
    public class Road_ZipCodeResult_DA : IRoad_ZipCodeResult_DA
    {
        private readonly DBconn _dbconn;

        public Road_ZipCodeResult_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public ParseAddress_itri_Model GetZipCode(ParseAddress_itri_Model paData)
        {
            orgArea_Condition result ;
            string CityVal = paData.CityName;
            string AreaVal = paData.AreaName;
            string RoadVal = paData.RoadName;
            int LaneNo = paData.LaneNo;
            int LaneEven = (paData.LaneEven + 2) % 2;
            if (LaneEven == 0) LaneEven = 2;
            int AlleyNo = paData.AlleyNo;
            int AlleyEven = (paData.AlleyEven + 2) % 2;
            if (AlleyEven == 0) AlleyEven = 2;
            int AddressNo = paData.AddressNo;
            int AddressEven = (paData.AddressEven + 2) % 2;
            if (AddressEven == 0) AddressEven = 2;
            int FloorNo = paData.FloorNo;
            int FstNo = paData.FstNo;
            int FstEven = (paData.FstEven + 2) % 2;
            if (FstEven == 0) FstEven = 2;
            int SndNo = paData.SndNo;
            int SndEven = (paData.SndEven + 2) % 2;
            if (SndEven == 0) SndEven = 2;

            string AddOrignal = paData.AddressOrigin;

            string sql = string.Format(@"
                SELECT TOP 1 ZIPCODE , station_scode , station_name , md_no , sd_no , put_order , SeqNO  ,ShuttleStationCode,SendSD,SendMD
                FROM 
                (
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'00' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE CITY='{0}' AND AREA='{1}' AND ROAD='{2}' ) TB1
                WHERE ( tb1.NO_BGN1='{4}' or tb1.NO_BGN='{4}' )
                UNION
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'01' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE CITY='{0}' AND AREA='{1}' AND ROAD='{2}' ) TB1
                WHERE TB1.LANE={3} AND ( tb1.NO_BGN1='{4}' or tb1.NO_BGN='{4}' )
                UNION
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'02' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE CITY='{0}' AND AREA='{1}' AND ROAD='{2}' ) TB1
                WHERE TB1.LANE={3} AND (EVEN={7} OR EVEN=0) 
                                    AND (
                                            ( tb1.NO_BGN1 < '{4}' AND tb1.NO_END1 >= '{4}') or ( tb1.NO_BGN < '{4}' AND tb1.NO_END >= '{4}') 
                                        )
                UNION
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'03' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE CITY='{0}' AND AREA='{1}' AND ROAD='{2}' ) TB1
                WHERE TB1.LANE={3}
                UNION
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'04' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE CITY='{0}' AND AREA='{1}' AND ROAD='{2}' ) TB1
                WHERE TB1.LANE={3} AND TB1.ALLEY={5} AND (NO_BGN<={6} 
                    AND NO_END>={6}) AND (EVEN={7} OR EVEN=0)
                UNION
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'05' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE CITY='{0}' AND AREA='{1}' AND ROAD='{2}' ) TB1
                WHERE  (
                                            ( tb1.NO_BGN1 < '{4}' AND tb1.NO_END1 >= '{4}') or ( tb1.NO_BGN < '{4}' AND tb1.NO_END >= '{4}') 
                                        )
                UNION
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'06' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE CITY='{0}' AND AREA='{1}' AND ROAD='{2}') TB1
                WHERE TB1.LANE={3} AND (NO_BGN<={11} AND NO_END>={11}) AND (EVEN={12} OR EVEN=0)
                UNION
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'07' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE CITY='{0}' AND AREA='{1}' AND ROAD='{2}' ) TB1
                WHERE TB1.ALLEY={5} AND (NO_BGN<={6} AND NO_END>={6}) 
                    AND (EVEN={7} OR EVEN=0)
                UNION
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'08' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE CITY='{0}' AND AREA='{1}' AND ROAD='{2}') TB1
                WHERE (NO_BGN<={8} AND NO_END>={8}) AND (EVEN={9} OR EVEN=0)
                UNION
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'09' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE CITY='{0}' AND AREA='{1}' AND ROAD='{2}' ) TB1
                WHERE (NO_BGN1='{4}') AND (FLOOR<={10} and FLOOR1>={10})
                UNION
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'10' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE CITY='{0}' AND AREA='{1}' AND ROAD='{2}' ) TB1
                WHERE (NO_BGN<={8} AND NO_END>={8}) AND (EVEN={9} OR EVEN=0)
                AND (FLOOR<={10} and FLOOR1>={10})
                UNION
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'11' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE CITY='{0}' AND AREA='{1}' AND ROAD='{2}') TB1
                UNION
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'13' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE ( ( CITY='{0}' AND AREA='{1}') ) ) TB1
                UNION
                SELECT TB1.ZIPCODE,TB1.ISN,station_scode , station_name , md_no , sd_no , put_order,'15' AS SeqNO ,ShuttleStationCode,SendSD,SendMD
                FROM (
                SELECT *
                FROM orgArea 
                WHERE ( ( CITY='{0}' AND AREA='{1}') or AREA1 = '{0}' ) and AREA1 in ('臺北市','台北市','基隆市','新竹市','嘉義市') AND ROAD='{2}') TB1
                ) TBA1 
                ORDER BY SeqNo ", CityVal, AreaVal, RoadVal, LaneNo,
                    AddressNo, AlleyNo, AddressNo, AddressEven, FstNo, FstEven, FloorNo, SndNo, SndEven);

            using (var conn = _dbconn.CreateJunFuTransConnection())
            {
                try
                {
                    result = conn.Query<orgArea_Condition>(sql).FirstOrDefault();

                    if (result != null) 
                    {
                        paData.ZipCode = result.ZIPCODE;
                        paData.StationCode = result.station_scode;
                        paData.StationName = result.station_name;
                        paData.MotorcycleDriverCode = result.md_no;
                        paData.SalesDriverCode = result.sd_no;
                        paData.StackCode = result.put_order;
                        paData.SeqNO = result.SeqNO;
                        paData.ShuttleStationCode = result.ShuttleStationCode;
                        paData.SendSD = result.SendSD;
                        paData.SendMD = result.SendMD;
                    }
                 
                }
                catch (Exception ex)
                {

                    //LogUtil.ErrorLog(e.ToString());
                    //throw e;
                }
            }
            return paData;
        }

        public string GetRoad(ParseAddress_itri_Model paData)
        {
            string result = "NONE";
            string fullRoad = paData.RoadName;
            string CityVal = paData.CityName;
            string AreaVal = paData.AreaName;

            string sql = string.Format(@"select top 1 TB11.roadName
                from (select DISTINCT TB1.ROAD as roadName from (
                select * from orgArea where  ( ( CITY='{0}' and area='{1}') or area1 = '{0}' )     ) tb1
                INNER JOIN (select N'{2}' as road1) TB2 
                ON TB2.road1 LIKE '%' + TB1.ROAD +'%') TB11 ORDER BY LEN(roadName) DESC", CityVal, AreaVal, fullRoad);
            using (var conn = _dbconn.CreateJunFuTransConnection())
            {
                try
                {
                    result = conn.Query<string>(sql).FirstOrDefault();
                }
                catch (Exception e)
                {
                    result = "";
                    //LogUtil.ErrorLog(e.ToString());
                    //throw e;
                }
            }
            return result;
        }

        public void SaveAddress(ParseAddress_itri_Model paData)
        {

            string sql = string.Format(@"
INSERT INTO [dbo].[AddressResult_itri]
	(   [InitialAddress],
		[ZipCode],[CityName],[AreaName],[RoadName],
		[LaneNo],[LaneEven],
		[AlleyNo],[AlleyEven],
		[AddressOrigin],[AddressNo],[AddressEven],
		[FloorOrigin],[FloorNo],
		[FstNo],[FstEven],
		[SndNo],[SndEven],
		[StationCode],[StationName],[SalesDriverCode],[MotorcycleDriverCode],[StackCode],[ShuttleStationCode],
		[CreateTime],[IsArtificial],[Active],[Type],[ComeFrom],[CustomerCode],[SeqNO]
		)
     VALUES
		(   @InitialAddress,
			@ZipCode,@CityName,@AreaName,@RoadName,
			@LaneNo,@LaneEven,
			@AlleyNo,@AlleyEven,
			@AddressOrigin,@AddressNo,@AddressEven,
			@FloorOrigin,@FloorNo,
			@FstNo,@FstEven,
			@SndNo,@SndEven,
			@StationCode,@StationName,@SalesDriverCode,@MotorcycleDriverCode,@StackCode,@ShuttleStationCode,
			@CreateTime,@IsArtificial,@Active,@Type,@ComeFrom,@CustomerCode,@SeqNO)
");
            using (var conn = _dbconn.CreateJunFuAddressConnection())
            {
                try
                {
                    conn.Execute(sql, paData);
                }
                catch (Exception e)
                {
                    //LogUtil.ErrorLog(e.ToString());
                    //throw e;
                }
            }

        }

        public void SaveAddress_Map8_2(Map8Result map8)
        {
            map8.EndTime = DateTime.Now;

            string sql = string.Format(@"
INSERT INTO [dbo].[AddressResult_Map8_2]
           ([InitialAddressPre],[InitialAddress],
            [ZipCode],[formatted_address],[id],[place_id],
			[name],[city],[town],[type],[level],[likelihood],[authoritative],
			[location_lat],[location_lng],
			[json],[StationCode],[StationName],
			[SalesDriverCode],[MotorcycleDriverCode],[StackCode],[ShuttleStationCode],[CreateTime],[EndTime],[EndTime_SQL],
			[IsArtificial],[Active],[Type_],[ComeFrom],[CustomerCode],[SeqNO])
     VALUES
	       (@InitialAddressPre,@InitialAddress,
            @ZipCode,@formatted_address,@id,@place_id,
			@name,@city,@town,@type,@level,@likelihood,@authoritative,
			@location_lat,@location_lng,
			@json,@StationCode,@StationName,
			@SalesDriverCode,@MotorcycleDriverCode,@StackCode,@ShuttleStationCode,@CreateTime,@EndTime,getdate(),
			@IsArtificial,@Active,@Type_,@ComeFrom,@CustomerCode,@SeqNO)
");
            using (var conn = _dbconn.CreateJunFuAddressConnection())
            {
                try
                {
                    conn.Execute(sql, map8);
                }
                catch (Exception e)
                {
                    //LogUtil.ErrorLog(e.ToString());
                    //throw e;
                }
            }

        }


    }

    public interface IRoad_ZipCodeResult_DA
    {
        ParseAddress_itri_Model GetZipCode(ParseAddress_itri_Model paData);

        string GetRoad(ParseAddress_itri_Model paData);

        void SaveAddress(ParseAddress_itri_Model paData);

        void SaveAddress_Map8_2(Map8Result paData);
    }
}