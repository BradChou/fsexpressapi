﻿using Common;
using DAL.Model.JunFuAddress_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.JunFuAddress_DA
{
   public class AddressUsingLog_DA : IAddressUsingLog_DA
    {
        private readonly DBconn _dbconn;

        public AddressUsingLog_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(AddressUsingLog_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuAddressConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[AddressUsingLog]
                        (
               [AddressResultId]
      ,[ComeFrom]
      ,[CustomerCode]
                        )
                        VALUES
                       (
       @AddressResultId
      ,@ComeFrom
      ,@CustomerCode
                        )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
    }
    public interface IAddressUsingLog_DA
    {
        Task Insert(AddressUsingLog_Condition data);

    }
}
