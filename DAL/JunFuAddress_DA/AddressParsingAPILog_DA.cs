﻿using Common;
using DAL.Model.JunFuAddress_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.JunFuAddress_DA
{
  public  class AddressParsingAPILog_DA : IAddressParsingAPILog_DA
    { 
    private readonly DBconn _dbconn;

    public AddressParsingAPILog_DA(DBconn dBconn)
    {
        _dbconn = dBconn;
    }


    public async Task Add(AddressParsingAPILog_Condition data)
    {

        using (var _conn = _dbconn.CreateJunFuAddressConnection())
        {
            string sql =
                @"  
                    INSERT [dbo].[AddressParsingAPILog] ( [Guid]
      ,[Address]
      ,[CustomerCode]
      ,[RequestBody]
      ,[ResponseBody]
      ,[RequestHeader]
      ,[Path]
      ,[QueryString]
      ,[StratTime]
      ,[EndTime]) VALUES ( 
       @Guid
      ,@Address
      ,@CustomerCode
      ,@RequestBody
      ,@ResponseBody
      ,@RequestHeader
      ,@Path
      ,@QueryString
      ,@StratTime
      ,@EndTime
)
                    ";
            await _conn.ExecuteAsync(sql, data);

        }
    }

}
public interface IAddressParsingAPILog_DA
    {
    Task Add(AddressParsingAPILog_Condition data);

}
}
