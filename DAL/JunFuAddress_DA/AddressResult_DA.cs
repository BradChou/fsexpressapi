﻿using Common;
using DAL.Model.JunFuAddress_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.JunFuAddress_DA
{
    public class AddressResult_DA : IAddressResult_DA
    {
        private readonly DBconn _dbconn;

        public AddressResult_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task<long> Insert(AddressResult_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuAddressConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[AddressResult]
                         (
                     [InitialAddress]
,[PostZip3]
,[PostZip32]
,[PostZip33]
           ,[Map8_formatted_address]
           ,[Map8_city]
           ,[Map8_town]
           ,[Map8_village]
           ,[Map8_lin]
           ,[Map8_road]
           ,[Map8_hamlet]
           ,[Map8_lane]
           ,[Map8_alley]
           ,[Map8_lon]
           ,[Map8_num]
           ,[Map8_floor]
           ,[Map8_numAttr]
           ,[JF_address]
           ,[JF_city]
           ,[JF_area]
           ,[JF_village]
           ,[JF_road]
           ,[JF_lane]
           ,[JF_alley]
           ,[JF_sub_alley]
           ,[JF_neig]
           ,[JF_no]
           ,[StationCode]
           ,[StationName]
           ,[SalesDriverCode]
           ,[MotorcycleDriverCode]
           ,[StackCode]
  ,[ShuttleStationCode]
 ,[SendSalesDriverCode]
 ,[SendMotorcycleDriverCode]

                        )
OUTPUT Inserted.id
                        VALUES
                       (
           @InitialAddress
,@PostZip3
,@PostZip32
,@PostZip33
           ,@Map8_formatted_address
           ,@Map8_city
           ,@Map8_town
,@Map8_village
           ,@Map8_lin
           ,@Map8_road
           ,@Map8_hamlet
           ,@Map8_lane
           ,@Map8_alley
           ,@Map8_lon
           ,@Map8_num
           ,@Map8_floor
           ,@Map8_numAttr
           ,@JF_address
           ,@JF_city
           ,@JF_area
           ,@JF_village
           ,@JF_road
           ,@JF_lane
           ,@JF_alley
           ,@JF_sub_alley
           ,@JF_neig
           ,@JF_no
           ,@StationCode
           ,@StationName
           ,@SalesDriverCode
           ,@MotorcycleDriverCode
           ,@StackCode
  ,@ShuttleStationCode
  ,@SendSalesDriverCode
  ,@SendMotorcycleDriverCode
         
                        )
                    ";
              return  await _conn.QueryFirstAsync<long>(sql, data);

                }
            }
        public async Task<IEnumerable<AddressResult_Condition>> Search(string address,DateTime sTime ,DateTime eTime)
        {
            using (var _conn = _dbconn.CreateJunFuAddressConnection())
            {
                string sql =
                    @" 
SELECT *
FROM [dbo].[AddressResult] WITH (NOLOCK)
WHERE [CreateTime] > @sTime AND [CreateTime] < @eTime AND [InitialAddress] = @address AND  ([StationCode] <> '' AND ( Map8_city <>'' OR JF_city <> ''))
ORDER BY CreateTime desc
                    ";
              return  await _conn.QueryAsync<AddressResult_Condition>(sql, new { address , sTime , eTime });

            }
        }

        public async Task<AddressResult_Condition> GetInfo(long id)
        {
            using (var _conn = _dbconn.CreateJunFuAddressConnection())
            {
                string sql =
                    @" 
SELECT *
FROM [dbo].[AddressResult] WITH (NOLOCK)
WHERE id=@id
                    ";
                return await _conn.QueryFirstOrDefaultAsync<AddressResult_Condition>(sql, new { id });

            }
        }
    }
    public interface IAddressResult_DA
    {
        Task<long> Insert(AddressResult_Condition data);
        Task<IEnumerable<AddressResult_Condition>> Search(string address, DateTime sTime, DateTime eTime);
        Task<AddressResult_Condition> GetInfo(long id);
    }
}
