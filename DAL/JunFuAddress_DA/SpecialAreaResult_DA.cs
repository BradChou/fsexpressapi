﻿using Common;
using DAL.Model.JunFuAddress_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.JunFuAddress_DA
{
    public class SpecialAreaResult_DA : ISpecialAreaResult_DA
    {
        private readonly DBconn _dbconn;

        public SpecialAreaResult_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(SpecialAreaResult_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuAddressConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[SpecialAreaResult]
                        (
[InitialAddress]
            ,[City]
           ,[Area]
           ,[Road]
           ,[Lane]
           ,[Alley]
           ,[No]
           ,[SpecialAreaManageId]
           ,[KeyWord]
           ,[StationCode]
           ,[SalesDriverCode]
           ,[MotorcycleDriverCode]
           ,[StackCode]
           ,[ShuttleStationCode]
           ,[Fee]
           ,[TimeLimit]
                        )
                        VALUES
                       (
@InitialAddress
            ,@City
           ,@Area
           ,@Road
           ,@Lane
           ,@Alley
           ,@No
           ,@SpecialAreaManageId
           ,@KeyWord
           ,@StationCode
           ,@SalesDriverCode
           ,@MotorcycleDriverCode
           ,@StackCode
           ,@ShuttleStationCode
 ,@Fee
 ,@TimeLimit

                        )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }

        
    }
    public interface ISpecialAreaResult_DA
    {
        Task Insert(SpecialAreaResult_Condition data);

    }
}
