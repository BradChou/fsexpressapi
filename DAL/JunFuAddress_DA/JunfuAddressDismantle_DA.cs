﻿using Common;
using DAL.Model.JunFuAddress_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.JunFuAddress_DA
{
    public class JunfuAddressDismantle_DA : IJunfuAddressDismantle_DA
    {
        private readonly DBconn _dbconn;

        public JunfuAddressDismantle_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(JunfuAddressDismantle_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuAddressConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[JunfuAddressDismantle]
                        (
                   [o_address]
           ,[status_code]
           ,[status_msg]
           ,[city]
           ,[area]
           ,[village]
           ,[road]
           ,[lane]
           ,[alley]
           ,[sub_alley]
           ,[neig]
           ,[no]
           ,[address]
                        )
                        VALUES
                       (
            @o_address
           ,@status_code
           ,@status_msg
           ,@city
           ,@area
           ,@village
           ,@road
           ,@lane
           ,@alley
           ,@sub_alley
           ,@neig
           ,@no
           ,@address
                        )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }
        public async Task<JunfuAddressDismantle_Condition> Search(string address, DateTime sTime, DateTime eTime)
        {
            using (var _conn = _dbconn.CreateJunFuAddressConnection())
            {
                string sql =
                    @" 
SELECT *
FROM [dbo].[JunfuAddressDismantle] WITH (NOLOCK)
WHERE [CreateTime] > @sTime AND [CreateTime] < @eTime AND [o_address] = @address 
ORDER BY CreateTime desc
                    ";
                return (await _conn.QueryAsync<JunfuAddressDismantle_Condition>(sql, new { address, sTime, eTime })).FirstOrDefault();

            }
        }

    }
    public interface IJunfuAddressDismantle_DA
    {
        Task Insert(JunfuAddressDismantle_Condition data);
        Task<JunfuAddressDismantle_Condition> Search(string address, DateTime sTime, DateTime eTime);
    }
}
