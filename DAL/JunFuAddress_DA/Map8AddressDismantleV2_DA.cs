﻿using Common;
using DAL.Model.JunFuAddress_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.JunFuAddress_DA
{
    public class Map8AddressDismantleV2_DA : IMap8AddressDismantleV2_DA
    {
        private readonly DBconn _dbconn;

        public Map8AddressDismantleV2_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(Map8AddressDismantleV2_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuAddressConnection())
            {
                string sql =
                    @" 
INSERT INTO [dbo].[Map8AddressDismantleV2]
           ([o_address]
           ,[formatted_address]
           ,[place_id]
           ,[name]
           ,[city]
           ,[town]
           ,[type]
           ,[level]
           ,[likelihood]
           ,[authoritative]
           ,[geomJson]
      
           ,[StartTime]
           ,[EndTime])
     VALUES
           (
            @o_address
           ,@formatted_address
           ,@place_id
           ,@name
           ,@city
           ,@town
           ,@type
           ,@level
           ,@likelihood
           ,@authoritative
           ,@geomJson
           
           ,@StartTime
           ,@EndTime
)
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }

        public async Task<Map8AddressDismantleV2_Condition> GetInfoByAddr(string o_address)
        {
            using (var _conn = _dbconn.CreateJunFuAddressConnection())
            {
                string sql =
                    @" 
SELECT *
FROM [dbo].[Map8AddressDismantleV2] WITH (NOLOCK)
WHERE o_address=@o_address
  ORDER BY id desc
                    ";
                return await _conn.QueryFirstOrDefaultAsync<Map8AddressDismantleV2_Condition>(sql, new { o_address });

            }
        }

    }
    public interface IMap8AddressDismantleV2_DA
    {
        Task Insert(Map8AddressDismantleV2_Condition data);
        Task<Map8AddressDismantleV2_Condition> GetInfoByAddr(string o_address);
    }
}
