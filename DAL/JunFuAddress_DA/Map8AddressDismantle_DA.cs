﻿using Common;
using DAL.Model.JunFuAddress_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.JunFuAddress_DA
{
    public class Map8AddressDismantle_DA : IMap8AddressDismantle_DA
    {
        private readonly DBconn _dbconn;

        public Map8AddressDismantle_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public async Task Insert(Map8AddressDismantle_Condition data)
        {
            using (var _conn = _dbconn.CreateJunFuAddressConnection())
            {
                string sql =
                    @"  INSERT INTO [dbo].[Map8AddressDismantle]
                        (
               [o_address]
           ,[number]
           ,[formatted_address]
    ,[postcode3]
      ,[postcode33]
           ,[doorplateID]
           ,[city]
           ,[town]
,[village]
           ,[lin]
           ,[road]
           ,[hamlet]
           ,[lane]
           ,[alley]
           ,[lon]
           ,[num]
           ,[floor]
           ,[numAttr]
           ,[residenceID]
           ,[compType]
           ,[compDate]
           ,[trxDate]
           ,[geomJson]
           ,[resultAnalysisJson]
           ,[history],[StartTime],[EndTime]
                        )
                        VALUES
                       (
            @o_address
           ,@number
           ,@formatted_address
    ,@postcode3
      ,@postcode33
           ,@doorplateID
           ,@city
           ,@town
,@village
           ,@lin
           ,@road
           ,@hamlet
           ,@lane
           ,@alley
           ,@lon
           ,@num
           ,@floor
           ,@numAttr
           ,@residenceID
           ,@compType
           ,@compDate
           ,@trxDate
           ,@geomJson
           ,@resultAnalysisJson
           ,@history,@StartTime,@EndTime
                        )
                    ";
                await _conn.ExecuteAsync(sql, data);

            }
        }

        public async Task<IEnumerable<Map8AddressDismantle_Condition>> Search(string address, DateTime sTime, DateTime eTime)
        {
            using (var _conn = _dbconn.CreateJunFuAddressConnection())
            {
                string sql =
                    @" 
SELECT *
FROM [dbo].[Map8AddressDismantle] WITH (NOLOCK)
WHERE [CreateTime] > @sTime AND [CreateTime] < @eTime AND [o_address] = @address AND [number]=1
ORDER BY CreateTime desc
                    ";
                return await _conn.QueryAsync<Map8AddressDismantle_Condition>(sql, new { address, sTime, eTime });

            }
        }
    }
    public interface IMap8AddressDismantle_DA
    {
        Task Insert(Map8AddressDismantle_Condition data);
        Task<IEnumerable<Map8AddressDismantle_Condition>> Search(string address, DateTime sTime, DateTime eTime);
    }
}
