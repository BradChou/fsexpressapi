﻿using Common;
using DAL.Model.Contract_Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DAL.FSE01_DA
{
    public class LogRecord : ILogRecord_DA
    {
        private readonly DBconn _dbconn;

        public LogRecord(DBconn dBconn)
        {
            _dbconn = dBconn;
        }

        public void Save(string JobType, string DetailType, string Memo)
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                try
                {
                    string sql =
                    @"  
                    INSERT into [dbo].[JobRecord] 
                    (
                      
	                  [JobType],
	                  [DetailType],
	                  [CreateTime],
	                  [Memo]

                    ) VALUES
                    (   
                      @JobType,
	                  @DetailType,
	                  getdate(),
	                  @Memo
                    )
                    ";
                    _conn.Execute(sql, new
                                        {
                                            JobType = JobType ,
                                            DetailType = DetailType ,
                                            Memo = Memo
                                        });
                }
                catch (Exception e)
                {

                }


            }
        }
    }

    public interface ILogRecord_DA
    {
        public void Save(string JobType, string DetailType , string Memo);


    }

}
