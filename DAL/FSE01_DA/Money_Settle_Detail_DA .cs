﻿using Common;
using System;
using System.Collections.Generic;
using DAL.Model.Condition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DAL.DA
{
    public class Money_Settle_Detail_DA : IMoney_Settle_Detail_DA
    {
        private readonly DBconn _dbconn;

        public Money_Settle_Detail_DA(DBconn dBconn)
        {
            _dbconn = dBconn;
        }
        public long Insert(Money_Settle_Detail_Condition data)
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                string sql =
                    @"  
                    INSERT into [dbo].[MoneySettle_Detail] 
                    (
                       [RequestId]
                      ,[DeliveryType]
                      ,[CheckNumber]
                      ,[CreateDate]
                      ,[CustomerCode]
                      ,[SendStation]
                      ,[ArriveStation]
                      ,[City]
                      ,[Area]
                      ,[Address]
                      ,[ProductId]
                      ,[Pieces]
                      ,[ReceiptFlag]
                      ,[CollectionMoney]
                      ,[Valued]
                      ,[OverCBM]
                      ,[RoundTrip]
                      ,[SpecialAreaFee]
                      ,[EggArea]
                      ,[PrintDate]
                      ,[ShipDate]
                      ,[OrderNumber]
                      ,[CustomerName]
                      ,[ReceiveContact]
                      ,[InvoiceDesc]
                      ,[ScanDate]
                      ,[CreateUser]
                      ,[Request_CreateDate]
                      ,[SpecialAreaId]
                      ,[DriverCode]
                      ,[ArriveDriverStation]
                      ,[CustomerStation]
                      ,[ArriveOption]
                      ,[ScanLogCdate]
                      ,[ReceiveDriverStation]
                      ,[ReceiveDriverCode]
                    )
                    OUTPUT Inserted.id
                    VALUES
                    (   
                       @RequestId
                      ,@DeliveryType
                      ,@CheckNumber
                      ,@CreateDate
                      ,@CustomerCode
                      ,@SendStation
                      ,@ArriveStation
                      ,@City
                      ,@Area
                      ,@Address
                      ,@ProductId
                      ,@Pieces
                      ,@ReceiptFlag
                      ,@CollectionMoney
                      ,@Valued
                      ,@OverCBM
                      ,@RoundTrip
                      ,@SpecialAreaFee
                      ,@EggArea
                      ,@PrintDate
                      ,@ShipDate
                      ,@OrderNumber
                      ,@CustomerName
                      ,@ReceiveContact
                      ,@InvoiceDesc
                      ,@ScanDate
                      ,@CreateUser
                      ,@Request_CreateDate
                      ,@SpecialAreaId
                      ,@DriverCode
                      ,@ArriveDriverStation
                      ,@CustomerStation
                      ,@ArriveOption
                      ,@ScanLogCdate 
                      ,@ReceiveDriverStation
                      ,@ReceiveDriverCode
                    )
                    ";
                
                
                    return _conn.QueryFirstOrDefault<long>(sql, data);
                
                
            }

        }

        public void Insert(List<Money_Settle_Detail_Condition> data)
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                string sql =
                    @"  
                    INSERT into [dbo].[MoneySettle_Detail] 
                    (
                       [id]
                      ,[RequestId]
                      ,[DeliveryType]
                      ,[CheckNumber]
                      ,[CreateDate]
                      ,[CustomerCode]
                      ,[SendStation]
                      ,[ArriveStation]
                      ,[City]
                      ,[Area]
                      ,[Address]
                      ,[ProductId]
                      ,[Pieces]
                      ,[ReceiptFlag]
                      ,[CollectionMoney]
                      ,[Valued]
                      ,[OverCBM]
                      ,[RoundTrip]
                      ,[SpecialAreaFee]
                      ,[EggArea]
                      ,[PrintDate]
                      ,[ShipDate]
                      ,[OrderNumber]
                      ,[CustomerName]
                      ,[ReceiveContact]
                      ,[InvoiceDesc]
                      ,[ScanDate]
                      ,[CreateUser]
                      ,[Request_CreateDate]
                      ,[SpecialAreaId]
                      ,[DriverCode]
                      ,[ArriveDriverStation]
                      ,[CustomerStation]
                      ,[ArriveOption]
                      ,[ScanLogCdate]
                      ,[ReceiveDriverStation]
                      ,[ReceiveDriverCode]
                      ,[isEast]
                    )
                    VALUES
                    (   
                       @id
                      ,@RequestId
                      ,@DeliveryType
                      ,@CheckNumber
                      ,@CreateDate
                      ,@CustomerCode
                      ,@SendStation
                      ,@ArriveStation
                      ,@City
                      ,@Area
                      ,@Address
                      ,@ProductId
                      ,@Pieces
                      ,@ReceiptFlag
                      ,@CollectionMoney
                      ,@Valued
                      ,@OverCBM
                      ,@RoundTrip
                      ,@SpecialAreaFee
                      ,@EggArea
                      ,@PrintDate
                      ,@ShipDate
                      ,@OrderNumber
                      ,@CustomerName
                      ,@ReceiveContact
                      ,@InvoiceDesc
                      ,@ScanDate
                      ,@CreateUser
                      ,@Request_CreateDate
                      ,@SpecialAreaId
                      ,@DriverCode
                      ,@ArriveDriverStation
                      ,@CustomerStation
                      ,@ArriveOption
                      ,@ScanLogCdate 
                      ,@ReceiveDriverStation
                      ,@ReceiveDriverCode
                      ,@isEast
                    )
                    ";
                _conn.Execute(sql, data,null,500);
            }

        }

        public long GetKey()
        {
            using (var _conn = _dbconn.CreateFSE01Connection())
            {
                string sql =
                    @"  
Declare @now nvarchar(8)
Declare @day1 nvarchar(100)
Declare @day2 nvarchar(100)

Declare @result nvarchar(19) 
Set @now = convert( nvarchar(8) , getdate() , 112 )  
Select @day1 = left(Max([id]), 8) , @day2 = left(Max([id]),11) From [MoneySettle_Detail]  

if ( @now = @day1 ) 
Begin 
	Set @result = cast( (cast(@day2 as bigint) + 1) as nvarchar(11) ) + '00000000'
End
else
Begin 
	Set @result = @now + '00100000000'
End

Select cast( @result as bigint) as id
                    ";
                
                var tResult = _conn.Query<Money_Settle_Detail_Condition>(sql).FirstOrDefault();

                return tResult.id;

            }

        }

    }


    public interface IMoney_Settle_Detail_DA
    {
        public long Insert(Money_Settle_Detail_Condition data);
        public void Insert(List<Money_Settle_Detail_Condition> data);
        public long GetKey();
    }


}